import datetime

from fabric.api import *
from fabric.operations import hide, run
from fabric.operations import local as lrun

env.roledefs['test'] = ['valerich@s1.inrooms.istomin.me:2233']


def test():
    env.hosts = ['test.inrooms.club:2233']
    env.use_ssh_config = True
    env.git_branch = 'release-test'
    env.project_path = '/opt/projects/inrooms_project/'
    env.python = env.project_path + 'bin/python'
    env.pip = env.project_path + 'bin/pip'
    env.app_path = env.project_path + 'lib/prj/inrooms_trade_offers/'
    env.app_front_path = env.project_path + 'lib/prj/front/'
    env.django_settings = 'config.settings.local'
    env.forward_agent = True


def prod():
    env.hosts = ['chn.inrooms.club:2233']
    env.use_ssh_config = True
    env.git_branch = 'release-prod'
    env.use_ssh_config = True
    env.project_path = '/opt/projects/inrooms_project/'
    env.python = env.project_path + 'bin/python'
    env.pip = env.project_path + 'bin/pip'
    env.app_path = env.project_path + 'lib/prj/inrooms_trade_offers/'
    env.app_front_path = env.project_path + 'lib/prj/front/'
    env.django_settings = 'config.settings.local'
    env.forward_agent = True


def localhost():
    env.hosts = ['localhost']
    env.path = '~/dev/django/inrooms_to_project/inrooms_trade_offers'
    env.virtualhost_path = env.path


# tasks

def run_test():
    localhost()
    result = lrun("cd %(path)s; git checkout %(git_branch)s;" % env, capture=False)
    if result.failed:
        abort('Error')
    result = lrun("git pull origin %(git_branch)s;" % env, capture=False)
    if result.failed:
        abort('Error')
    result = lrun("python manage.py test --parallel" % env, capture=False)
    if result.failed:
        abort('Error')


def deploy():
    # run_test()
    update_prj()
    make_front_data()
    install_requirements()
    migrate()
    collectstatic()
    server_init()
    restart_app()
    status_app()


def deploy_front():
    update_prj()
    make_front_data()
    collectstatic()
    restart_app()
    status_app()


def update_prj():
    print_current_task('Pull dev')
    run('cd %(app_path)s; git pull origin %(git_branch)s' % env, pty=True)


def make_front_data():
    run('cd %(app_front_path)s; rm -rf dist; npm install; gulp build;' % env, pty=True)


def install_requirements():
    print_current_task('Install Requirements')
    run('cd %(project_path)s/lib/prj/; %(pip)s install -r %(project_path)s/lib/prj/requirements/prod.txt' % env, pty=True)


def collectstatic():
    print_current_task('Collect Static')
    with hide('output'):
        run('cd %(app_path)s;  %(python)s manage.py collectstatic --noinput --settings %(django_settings)s' % env, pty=True)


def migrate():
    print_current_task('Migrate')
    run('cd %(app_path)s;  %(python)s manage.py migrate --settings %(django_settings)s' % env, pty=True)


def server_init():
    print_current_task('Server Init')
    run('cd %(app_path)s;  %(python)s manage.py server_init --settings %(django_settings)s' % env, pty=True)


def restart_app():
    print_current_task('Restart App')
    run('sudo supervisorctl restart inrooms celery-beat celery-w1 celery-w2', pty=True)


def start_app():
    print_current_task('Start App')
    run('sudo supervisorctl start inrooms celery-beat celery-w1 celery-w2', pty=True)


def stop_app():
    print_current_task('Stop App')
    run('sudo supervisorctl stop inrooms celery-beat celery-w1 celery-w2', pty=True)


def status_app():
    print_current_task('Project Status')
    run('sudo supervisorctl status', pty=True)


def backup():
    dump_db()
    media_archive()


def dump_db():
    print_current_task('Dump DB')
    now = datetime.datetime.now()
    run('pg_dump --dbname=postgresql://inrooms:inrooms@127.0.0.1:5432/inrooms | gzip > "%(project_path)stmp/sql_dumps/dump_' % env + str(now) + '.sql.gz"', pty=True)
    get('%(project_path)stmp/sql_dumps/dump_' % env + str(now) + '.sql.gz')


def media_archive():
    print_current_task('Media Archive')
    now = datetime.datetime.now()
    with hide('output'):
        run('tar -zcvf "%(project_path)stmp/media_archives/media-' % env + str(now) + '.tar.gz" "%(project_path)svar/www/media"' % env)
    get('%(project_path)stmp/media_archives/media-' % env + str(now) + '.tar.gz')


def print_current_task(task_name):
    print('-' * 80)
    print('- {}'.format(task_name))
    print('-' * 80)

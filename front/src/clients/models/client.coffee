backbone = require 'backbone'


class ClientModel extends backbone.Model

  defaults:
    kind: 1

  url: =>
    if @id
      "/api/clients/client/#{@id}/"
    else
      "/api/clients/client/"

  validation:
      inn: [
        {
          required: true,
          msg: 'Заполните ИНН'
        }]
      name: [
        {
          required: true,
          msg: 'Заполните название'
        }]

module.exports = ClientModel
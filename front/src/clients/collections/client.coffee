backbone = require 'backbone'

ClientModel = require '../models/client'


class ClientCollection extends backbone.Collection
  model: ClientModel
  url: "/api/clients/client/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ClientCollection
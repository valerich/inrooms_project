backbone = require 'backbone'

ClientFileModel = require '../models/client_file'


class ClientFileCollection extends backbone.Collection
  model: ClientFileModel

  initialize: (options) =>
    @client_id = options.client_id

  url: =>
    "/api/clients/client/#{@client_id}/files/"

#  parse: (response) =>
#    @count = response.count
#    return response.results

module.exports = ClientFileCollection
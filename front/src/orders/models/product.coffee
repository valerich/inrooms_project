backbone = require 'backbone'


class ProductModel extends backbone.Model

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    if @id
      "/api/orders/order/#{@order_id}/product_search/#{@id}/"
    else
      "/api/orders/order/#{@order_id}/product_search/"

module.exports = ProductModel
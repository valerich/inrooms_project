backbone = require 'backbone'


class OrderModel extends backbone.Model

  url: =>
    if @id
      "/api/orders/order/#{@id}/"
    else
      "/api/orders/order/"

module.exports = OrderModel
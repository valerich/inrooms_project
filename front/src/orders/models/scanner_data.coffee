backbone = require 'backbone'


class ScannerDataModel extends backbone.Model

  url: =>
    if @id
      "/api/orders/scanner_data/#{@id}/"
    else
      "/api/orders/scanner_data/"

module.exports = ScannerDataModel
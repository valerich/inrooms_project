_ = require 'underscore'
backbone = require 'backbone'


class OrderCreateInvoiceModel extends backbone.Model
  validation:
    provider: [
      {
        required: true,
        msg: 'Заполните поставщика'
      }
    ]


module.exports = OrderCreateInvoiceModel

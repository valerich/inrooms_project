_ = require 'underscore'
backbone = require 'backbone'


class HisotoryItemModel extends backbone.Model

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    if @id
      "/api/history/history_item/orders/order/#{@order_id}/#{@id}/"
    else
      "/api/history/history_item/orders/order/#{@order_id}/"


module.exports = HisotoryItemModel

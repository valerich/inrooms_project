_ = require 'underscore'
backbone = require 'backbone'


class OrderItemScannerDataModel extends backbone.Model
  defaults:
    series_count: 1
  validation:
    series_count: [
      {
        required: true,
        msg: 'Заполните количество серий'
      }, {
        pattern: 'digits',
        msg: 'Укажите корректное количество серий'
      }, {
        range: [1, 1000000000]
        msg: 'Количество не может быть меньше 1 и больше 1 000 000 000'
      }]

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    "/api/orders/order/#{@order_id}/items/create_on_scanner_data"


module.exports = OrderItemScannerDataModel

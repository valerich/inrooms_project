_ = require 'underscore'
backbone = require 'backbone'


class CategoryModel extends backbone.Model

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
#    if @id
#      "/api/orders/order/#{@order_id}/category/#{@id}/"
#    else
    "/api/orders/order/#{@order_id}/category/"


module.exports = CategoryModel

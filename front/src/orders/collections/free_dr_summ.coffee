backbone = require 'backbone'

FreeDRSummModel = require '../models/free_dr_summ'


class FreeDRSummCollection extends backbone.Collection
  model: FreeDRSummModel

  url: =>
    return "/api/orders/order/free_dr_summ/"


module.exports = FreeDRSummCollection
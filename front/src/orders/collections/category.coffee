backbone = require 'backbone'

CategoryModel = require '../models/category'


class CategoryCollection extends backbone.Collection
  model: CategoryModel

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    "/api/orders/order/#{@order_id}/category/"

  _prepareModel: (model, options) =>
      model.order_id = @order_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

module.exports = CategoryCollection
backbone = require 'backbone'

ScannerDataModel = require '../models/scanner_data'


class ScannerDataCollection extends backbone.Collection
  model: ScannerDataModel
  url: "/api/orders/scanner_data/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ScannerDataCollection
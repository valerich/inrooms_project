backbone = require 'backbone'

ProductModel = require '../models/product'


class ProductCollection extends backbone.Collection
  model: ProductModel
  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    "/api/orders/order/#{@order_id}/product_search/"

  _prepareModel: (model, options) =>
      model.order_id = @order_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ProductCollection
$ = require 'jquery'
_ = require 'underscore'
bus = require 'bus'
marionette = require 'backbone.marionette'


class FreeDRSummItemDetailView extends marionette.LayoutView
  template: require './templates/free_dr_summ_item_detail'

  initialize: (options) =>
    @data = options.data

  serializeData: =>
    s_data = super
    s_data['data'] = @data
    s_data



class FreeDRSummItemView extends marionette.ItemView
  template: require './templates/free_dr_summ_item'
  events:
      'click .free_dr_detail': 'onClickDetail'

  initialize: (options) =>
    @order_kind = options.order_kind

  onClickDetail: (event) =>
    event.preventDefault()
    client_id = $(event.target).data('client_id')

    $.ajax
      url: "/api/orders/order/free_dr_summ_detail/"
      data:
        "client": client_id,
        "order_kind": @order_kind
      type: 'get'
      success: (data) =>
        detailView = new FreeDRSummItemDetailView
          data: data

        bus.vent.trigger 'modal:show', detailView,
          title: "Детализация по свободному остатку - #{@model.get('client')['name']}"
          modal_size: 'lg'
      error: (data) =>
        helpers.modelErrorHandler @model, data


class FreeDRSummView extends marionette.CompositeView
  template: require './templates/free_dr_summ'
  childView: FreeDRSummItemView
  childViewContainer: '#list-d'

  initialize: (options) =>
    @client = options.client
    @order_kind = options.order_kind

  childViewOptions: (model, index) =>
    console.log 'childViewOptions'
    child_data = {
      order_kind: @order_kind,
      childIndex: index
    }
    return child_data

  doFilter: =>
    fetch_data = {
      'order_kind': @order_kind
    }
    if @client
      fetch_data['client'] = @client
    @collection.fetch({'data': fetch_data})

  serializeData: =>
    s_data = super
    s_data['order_kind'] = @order_kind
    s_data


module.exports = FreeDRSummView
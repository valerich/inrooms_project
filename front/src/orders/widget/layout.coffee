$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
complex = require 'complex'
marionette = require 'backbone.marionette'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
modelbinder = require 'backbone.modelbinder'


class OrderSummaryModel extends backbone.Model
  url: '/api/orders/order/summary/'


class LayoutView extends marionette.LayoutView
  show_header: false
  template: require './templates/layout'

  initialize: (options) =>
    @userCanViewOrderWidget = helpers.user_has_permission bus.cache.user, 'can_view_orders_widget2'
    @model = new OrderSummaryModel

    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    if @userCanViewOrderWidget
      @OrderWidgetUpdate()
      @binder.bind @model, @$el,
        aggreement_to_work: '.aggreement_to_work'
      @$('[data-toggle="tooltip"]').tooltip();

  serializeData: =>
    data = super
    data['userCanViewOrderWidget'] = @userCanViewOrderWidget
    data

  OrderWidgetUpdate: =>
    @model.fetch()

module.exports = LayoutView
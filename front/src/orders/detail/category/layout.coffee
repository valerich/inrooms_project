_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'
require 'jquery.inputmask'

CategoryCollection = require '../../collections/category'


class CategoryItemView extends marionette.ItemView
  template: require './templates/category_item'
  className: 'category_item'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @order_model = options.order_model

  onRender: =>
    @binder.bind @model, @$el,
      price_sum: '[name=price_sum]'
      name: '.category_name'


class CategoryCollectionView extends marionette.CollectionView
  childView: CategoryItemView
  className: 'category_list'

  initialize: (options) =>
    @order_model = options.order_model

  childViewOptions: (model, index) =>
    data = {
      order_model: @order_model,
      childIndex: index
    }
    return data


class SetCategoryView extends marionette.ItemView
  template: require './templates/set_category_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      'category_1_max_sum': '[name=category_1_max_sum]'

    @$('[name=category_1_max_sum]').inputmask("9{1,20}.9{1,2}")

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: "/api/orders/order/#{@model.order_id}/set_category/"
        type: 'post'
        data: {
          'category_1_max_sum': @model.get('category_1_max_sum')
        }
        success: =>
          bus.vent.trigger 'category:update'
          helpers.generate_notyfication('success', 'Сохранено')
          @destroy()
        error: (data) =>
          helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class SetCategoryModel extends backbone.Model

  initialize: (options) =>
    @order_id = options.order_id

  defaults:
    category_1_max_sum: "0.0"
  validation:
    category_1_max_sum: [
      {
        required: true,
        msg: 'Заполните сумму'
      }, {
        range: [0, 1000000000000000]
        msg: 'Количество не может быть меньше 0'
      }
    ]


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_order': 'onClickOrderSave'
    'click .set_categories': 'onClickSetCategories'

  regions:
    region_category_list: '.region_category_list'

  onClickOrderSave: =>
    backbone.history.navigate "/order/#{@model.id}/common/", trigger: true

  onClickSetCategories: =>
    setCategoryView = new SetCategoryView
      model: new SetCategoryModel
        order_id: @order_model.id

    bus.vent.trigger 'modal:show', setCategoryView,
      title: 'Распределение между категориями'

  fetchCategoryCollection: =>
    @category_collection.fetch(
      reset: true,
      data: {
        page_size: 100000
      }
    )

  initialize: (options) =>
    @order_model = options.model
    @listenTo(bus.vent, 'category:update', @fetchCategoryCollection)
    can_orders_edit = @order_model.get('can_edit')
    if can_orders_edit
      if @order_model.get('status_detail')['code'] == 'new'
        @canEdit = true
      else
        @canEdit = false
      if not @order_model.get('client_detail')['can_set_white_category']
        @canEdit = false
    else
      @canEdit = false

  onRender: =>
    @category_collection = new CategoryCollection
      order_id: @order_model.id

    @category_view = new CategoryCollectionView
      collection: @category_collection
      order_model: @order_model

    @region_category_list.show(@category_view)
    @fetchCategoryCollection()

  serializeData: =>
    result = super
    result['canEdit'] = @canEdit
    result


module.exports = LayoutView

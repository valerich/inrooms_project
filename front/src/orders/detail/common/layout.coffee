_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
bootbox = require 'bootbox'
store = require 'store'
helpers = require 'helpers'
#OrderCreateInvoiceModel = require '../../models/create_invoice'


#class CreateInvoiceView extends marionette.ItemView
#  template: require './templates/create_invoice_modal'
#  events:
#    'click [name=create]': 'onCreateBtnClick'
#  translated_fields:
#    provider: 'Поставщик'
#
#  initialize: (options)=>
#    @model = new OrderCreateInvoiceModel
#    @order_id = options.order_id
#    @binder = new modelbinder
#
#  onRender: =>
#    @binder.bind @model, @$el,
#      'provider': '[name=provider]'
#
#    helpers.initAjaxSelect2 @$('[name=provider]'),
#      url: "/api/clients/client/"
#      placeholder: 'Выберите поставщика'
#      minimumInputLength: -1
#      text_attr: 'name'
#      allowClear: true
#
#    backbone.Validation.bind @
#
#  onCreateBtnClick: (event) =>
#    event.preventDefault()
#    v = @model.validate()
#    if @model.isValid()
#      $.ajax
#          url: "/api/orders/order/" + @order_id + "/create_invoice/"
#          data:
#            "provider": @model.get('provider')
#          type: 'post'
#          success: (data) =>
#            helpers.generate_notyfication('success', 'Счет создан')
#            @destroy()
#          error: (data) =>
#            helpers.modelErrorHandler @model, data
#    else
#      error_str = _.values(v).join('<br>')
#      helpers.generate_notyfication('error', error_str)
#
#  @onBeforeDestroy: =>
#    @binder.unbind()


class ExportView extends marionette.ItemView
  template: require './templates/export_modal'
  events:
    'click [name=export]': 'onExportBtnClick'
  translated_fields:
    with_size_quantity: 'Включая количество размеров'
    delivery_year: 'Год поставки товара'
    with_number: 'Включая номер'
    with_date: 'Включая дату'

  initialize: (options)=>
    @model = new backbone.Model
      with_size_quantity: false
      with_number: true
      with_date: true
      delivery_year: new Date().getFullYear()
    @order_id = options.order_id
    @format = options.format
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      'delivery_year': '[name=delivery_year]'
      'with_size_quantity': '[name=with_size_quantity]'
      'with_number': '[name=with_number]'
      'with_date': '[name=with_date]'

    backbone.Validation.bind @

  onExportBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      url = "/orders/order/" + @order_id + "/export/?"
      if @model.get("with_size_quantity")
        url = url + "&with_size_quantity=" + @model.get("with_size_quantity")
      if @model.get("delivery_year")
        url = url + "&delivery_year=" + @model.get("delivery_year")
      if @model.get("with_number")
        url = url + "&with_number=" + @model.get("with_number")
      if @model.get("with_date")
        url = url + "&with_date=" + @model.get("with_date")
      url = url + "&as=" + @format
      sort_by = store.get("order_#{@order_id}_order_item_sort_by")
      if sort_by isnt undefined
        url = url + "&sort_by=" + sort_by
      window.open(url, '_blank')
      @destroy()
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()

  serializeData: =>
    @userCanExportOrderPdfWithSizeQuantity = helpers.user_has_permission bus.cache.user, 'can_export_order_pdf_with_size_quantity'
    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanExportOrderPdfWithSizeQuantity'] = @userCanExportOrderPdfWithSizeQuantity
    s_data


class ReReserveView extends marionette.LayoutView
  template: require './templates/re_reserve_modal'
  events:
    'click [name=re_reserve]': 'onReReserveBtnClick'

  initialize: (options)=>
    @order_id = options.order_id
    @status_code = options.status_code
    @errors_data = options.errors_data

  onReReserveBtnClick: (event) =>
    event.preventDefault()
    $.ajax
      url: "/api/orders/order/#{@order_id}/change_status/"
      type: 'post'
      data: {
        'code': @status_code,
        're_reserve_confirmed': true
      }
      success: =>
        helpers.generate_notyfication('success', 'Статус изменен')
        backbone.history.navigate "/order/", trigger: true
        @destroy()
      error: (data) =>
        helpers.modelErrorHandler @model, data

  serializeData: =>
    result = super
    result['order'] = @order_id
    result['errors_data'] = @errors_data
    result


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .print_order': 'onClickPrintOrder'
    'click .pdf_order': 'onClickPDFOrder'
    'click .change_status': 'onClickChangeStatus'
    'click .delete_order': 'onClickDeleteOrder'
    'click .exit_without_saving': 'onClickListLink'
    'click .create_invoice': 'onClickCreateInvoice'

  initialize: (options) =>
    @model.fetch()
    @canDelete = helpers.user_has_permission bus.cache.user, 'can_orders_delete'
    @binder = new modelbinder

  onClickCreateInvoice: =>
    message = "Вы действительно хотите создать счет на основании заказа?"
    bootbox.confirm message, (result)=>
      if result == true
        $target = $(event.target)
        $.ajax
          url: "/api/orders/order/" + @model.id + "/create_invoice/"
          type: 'post'
          success: (data) =>
            helpers.generate_notyfication('success', 'Счет создан')
          error: (data) =>
            helpers.modelErrorHandler @model, data

  onClickDeleteOrder: () =>
    if @canDelete
      bootbox.confirm 'Вы действительно хотите удалить заказ', (result)=>
        if result == true
          $.ajax
            url: "/api/orders/order/#{@model.id}/"
            type: 'delete'
            success: =>
              helpers.generate_notyfication('success', 'Заказ удален')
              backbone.history.navigate "/order/", trigger: true
            error: (data) =>
              helpers.modelErrorHandler @model, data

  onClickPrintOrder: (status_code) =>
    addExportView = new ExportView(
      order_id: @model.id
      format: 'html'
    )
    bus.vent.trigger 'modal:show', addExportView,
      title: 'Распечатать'

  onClickPDFOrder: (status_code) =>
    addExportView = new ExportView(
      order_id: @model.id
      format: 'pdf'
    )
    bus.vent.trigger 'modal:show', addExportView,
      title: 'Скачать в PDF'

  changeOrderStatus: (status_code) =>
    @model.save().done =>
        helpers.generate_notyfication('success', 'Сохранено')
        $.ajax
          url: "/api/orders/order/#{@model.id}/change_status/"
          type: 'post'
          data: {
            'code': status_code
          }
          success: =>
            helpers.generate_notyfication('success', 'Статус изменен')
            backbone.history.navigate "/order/", trigger: true
          error: (data) =>
            data_json = data.responseJSON
            if data_json['error_code'] == 'reserve_fail'
              newReReserveView = new ReReserveView(
                order_id: @model.id
                errors_data: data_json['data']
                status_code: status_code
              )
              bus.vent.trigger 'modal:show', newReReserveView,
                title: 'Проблемы с резервами'
            else
              helpers.modelErrorHandler @model, data

  onClickChangeStatus: (event) =>
    @model.save().done =>
        helpers.generate_notyfication('success', 'Сохранено')
        $target = $(event.target)
        new_status_code = $target.data('status_code')
        message = "Вы действительно хотите изменить статус заказа?"
        bootbox.confirm message, (result)=>
          if result == true
            $target = $(event.target)
            @changeOrderStatus(new_status_code)

  onClickListLink: (event) =>
    event.preventDefault()
    @model.save().done =>
      helpers.generate_notyfication('success', 'Сохранено')
      backbone.history.navigate '/order/', trigger: true

  serializeData: =>
    result = super
    result['user'] = bus.cache.user
    result['canDelete'] = @canDelete
    result

  onRender: =>
    @binder.bind @model, @$el,
      comment: '[name=comment]'

  onBeforeDestroy: =>
    @binder.unbind()

module.exports = LayoutView

_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
helpers = require 'helpers'
bus = require 'bus'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
      'click .save_order': 'onClickOrderSave'

  onClickOrderSave: =>
    if @canEdit
      @model.save().done =>
        helpers.generate_notyfication('success', 'Сохранено')
        backbone.history.navigate "/order/#{@model.id}/items/", trigger: true
    else
      backbone.history.navigate "/order/#{@model.id}/items/", trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @order_model = options.model
    @binder = new modelbinder
    can_orders_edit = helpers.user_has_permission bus.cache.user, 'can_orders_edit'
    if can_orders_edit
      if @order_model.get('status_detail')['code'] == 'new'
        @canEdit = true
      else
        @canEdit = false
    else
      @canEdit = false

  onRender: =>
    @binder.bind @model, @$el,
      client: '[name=client]'
      series_count: '[name=series_count]'
      comment: '[name=comment]'
      torg_12_category_1: '[name=torg_12_category_1]'
      torg_12_category_2: '[name=torg_12_category_2]'

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: 'Введите клиента'
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

  serializeData: =>
    result = super
    result['canEdit'] = @canEdit
    result


module.exports = LayoutView

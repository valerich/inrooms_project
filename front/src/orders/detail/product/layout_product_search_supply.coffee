$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require '../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
require 'magnific-popup'

ProductCollection = require '../../collections/product_supply'
ProductModel = require '../../models/product'
OrderItemModel = require '../../models/order_item'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/product_supply_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    console.log 'togglePanel'
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      main_collection: '[name=main_collection]'
      client_not_order: '[name=client_not_order]'
      sort_by: '.sort_list'

    helpers.initAjaxSelect2 @$('[name=main_collection]'),
      url: '/api/products/collection/'
      placeholder: "Выберите главную категорию"
      minimumInputLength: -1
      text_attr: 'full_name'
      allowClear: true


class ProductComplexItemView extends complex.ComplexItemView
  template: require './templates/product_supply_item'

  events:
    'click .create_order_item': 'onClickCreateOrderItem'
    'click .image-link': 'onClickImageLink'

  onClickImageLink: (event) =>
    image_data = @model.get('image')
    if image_data
      image = image_data['src']
    else
      image = '/static/img/no-image.jpg'
    $.magnificPopup.open
      type:'image'
      mainClass: 'mfp-fade'
      items: [{'src': image}]
      midClick: true

  initialize: (options)=>
    @binder = new modelbinder
    @order_model = options.order_model
    @order_item = new OrderItemModel
      order_id: @order_model.id
      product: @model.id
      series_count: @order_model.get('series_count')
    backbone.Validation.bind @, {
      model: @order_item
    }

  onRender: =>
    @binder.bind @order_item, @$el,
      series_count: '[name=series_count]'

  onClickCreateOrderItem: =>
    event.preventDefault()
    v = @order_item.validate()
    if @order_item.isValid()
      @order_item.save()
      .done =>
        helpers.generate_notyfication('success', 'Модель добавлена')
        bus.vent.trigger 'order:items:product_added', @order_item.get('product')
        bus.vent.trigger 'order:items:changed'
      .fail (fdata) ->
        helpers.modelErrorHandler @model, fdata
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class ProductComplexListView extends complex.ComplexCompositeView
  childView: ProductComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/product_supply_items'

  initialize: (options) =>
    @order_model = options.order_model

  childViewOptions: (model, index) =>
    child_data = {
      order_model: @order_model,
      childIndex: index
    }
    return child_data


class ProductComplexView extends complex.ComplexView
  template: require './templates/product_supply_complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout_product_supply_search'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @order_model = options.model
    @collection = new ProductCollection
      order_id: @order_model.id
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    urlParams["_client_id"] = @order_model.get('client')
    urlParams["page"] = 1

    @filter_model = new FilterModel(urlParams)

    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @filter_view = new FilterView
      model: @filter_model

    @list_view = new ProductComplexListView
      collection: @collection
      order_model: @order_model

    @complex_view = new ProductComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'products'

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @listenTo(bus.vent, 'order:items:product_added', @onProductAdded)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

  onProductAdded: (product_id) =>
    @collection.remove(product_id);


module.exports = LayoutView
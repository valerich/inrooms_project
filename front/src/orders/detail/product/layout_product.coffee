_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
data = require '../../data'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

OrderItemCollection = require '../../collections/order_item'
ProductModel = require '../../../products/models/product/'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/orderitem_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    console.log 'togglePanel'
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      sort_by: '.sort_list'


class ProductView extends marionette.ItemView
  template: require './templates/product_detail'

  events:
    "click .edit_product_price_toggle": "onClickEditPriceToggle"
    "click .save_new_price": "onClickSave"

  initialize: (options)=>
    @binder = new modelbinder
    @order_model = options.order_model
    @order_item_model = options.order_item_model

    fetching = @.model.fetch()
    fetching.done =>
      @.render()
    @.model.get('toggle_price_edit', false)

  onRender: =>
    @binder.bind @model, @$el,
      price: '[name=price]'

  onClickSave: =>
    new_price = @model.get('price')
    $.ajax
        url: "/api/orders/order/#{@order_model.id}/items/#{@order_item_model.id}/set_product_price/"
        data:
          "product_id": @model.id
          "price": new_price
        type: 'post'
        success: =>
          bus.vent.trigger 'order:items:changed'
          bus.vent.trigger 'product_item:update'
          @destroy()
        error: (data) =>
          helpers.modelErrorHandler @model, data

  onClickEditPriceToggle: =>
    toggle_price_edit = @.model.get('toggle_price_edit')
    if toggle_price_edit
      @$('[name=price]').addClass('form-white')
      @$('[name=price]').prop('disabled', true)
      @$('[name=save]').addClass('hidden')
      @.model.set('toggle_price_edit', false)
    else
      @$('[name=price]').removeClass('form-white')
      @$('[name=price]').prop('disabled', false)
      @$('[name=save]').removeClass('hidden')
      @.model.set('toggle_price_edit', true)



class OrderItemComplexItemView extends complex.ComplexItemView
  template: require './templates/orderitem_item'
  className: 'order_items_item'
  events:
    'click .remove': 'onClickRemove'
    'click .save': 'onClickSave'
    'click .image-link': 'onClickImageLink'
    'click .product_detail': 'onClickProductDetailLink'
  modelEvents:
    'change': 'setSeriesCount'

  onClickProductDetailLink: (event) =>
    productDetailView = new ProductView
      model: new ProductModel
       id: @model.get('product_detail')['id']
      order_model: @order_model
      order_item_model: @model

    bus.vent.trigger 'modal:show', productDetailView,
      title: 'Информация о товаре'

  onClickImageLink: (event) =>
    image_data = @model.get('product_detail')['image']
    if image_data
      image = image_data['src']
    else
      image = '/static/img/no-image.jpg'
    $.magnificPopup.open
      type:'image'
      mainClass: 'mfp-fade'
      items: [{'src': image}]
      midClick: true

  onClickSave: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Модель сохранена')
      bus.vent.trigger 'order:items:changed'
      @$('.btn-success').addClass('hidden')
    .fail (fdata) ->
      helpers.modelErrorHandler @model, fdata

  onClickRemove: =>
    bootbox.confirm "Вы действительно хотите удалить модель из заказа?", (result)=>
      if result == true
        @model.destroy()
        .done =>
          helpers.generate_notyfication('success', 'Удалено')
          bus.vent.trigger 'order:items:changed'
          bus.vent.trigger 'product_item:update'
        .fail (fdata) ->
          helpers.modelErrorHandler @model, fdata
          bus.vent.trigger 'order:items:changed'
          bus.vent.trigger 'product_item:update'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @order_model = options.order_model

  onRender: =>
    @setSeriesCount()
    @setWarehouseRemain()
    @binder.bind @model, @$el,
      price: '[name=price]'
      count_xxs: '[name=count_xxs]'
      count_xs: '[name=count_xs]'
      count_s: '[name=count_s]'
      count_m: '[name=count_m]'
      count_l: '[name=count_l]'
      count_xl: '[name=count_xl]'
      count_xxl: '[name=count_xxl]'
      count_xxxl: '[name=count_xxxl]'
      count_34: '[name=count_34]'
      count_35: '[name=count_35]'
      count_36: '[name=count_36]'
      count_37: '[name=count_37]'
      count_38: '[name=count_38]'
      count_39: '[name=count_39]'
      count_40: '[name=count_40]'
      count_41: '[name=count_41]'
      count_sum: '.count_sum'

    @listenTo @model, 'change', @onModelChange

  onModelChange: =>
    @setWarehouseRemain()
    keys = []
    _.each @model.changed, ( val, key ) =>
      if key != 'count_sum'
        keys.push(key)
    console.log keys
    if keys.length > 0
      @$('.btn-success').removeClass('hidden')
    else
      @$('.btn-success').addClass('hidden')

  setSeriesCount: =>
    series_type = @model.get('series_type')
    if series_type == 's'
      @$("#order_item_#{@model.id} .c_data").addClass('hidden')
      @$("#order_item_#{@model.id} .s_data").removeClass('hidden')
    else if series_type == 'c'
      @$("#order_item_#{@model.id} .s_data").addClass('hidden')
      @$("#order_item_#{@model.id} .c_data").removeClass('hidden')
#    warehouse_from_code = @order_model.get('delivery').get('warehouse_from_detail')['code']
    product_detail = @model.get('product_detail')
    if product_detail is undefined
      product_detail = {}
    remains = product_detail['remains']
    if remains is undefined
      remains = {}

    msk_warehouse_remains = remains['msk']
    if msk_warehouse_remains is undefined
      msk_warehouse_remains = {}
    showroom_warehouse_remains = remains['showroom']
    if showroom_warehouse_remains is undefined
      showroom_warehouse_remains = {}

    supply_data = @model.get('supply_data')
    if supply_data is undefined
      supply_data = {}

    count_sum = 0
    for size in [
              'xxs',
              'xs',
              's',
              'm',
              'l',
              'xl',
              'xxl',
              'xxxl',
              '34',
              '35',
              '36',
              '37',
              '38',
              '39',
              '40',
              '41',
    ]
      w_remain = 0

      msk_remain = msk_warehouse_remains[size]
      if msk_remain is undefined
        msk_remain = 0
      w_remain += msk_remain

      showroom_remain = showroom_warehouse_remains[size]
      if showroom_remain is undefined
        showroom_remain = 0
      w_remain += showroom_remain

      supply_data_item = supply_data[size]
      if supply_data_item is undefined
        supply_data_item = 0

      @$("#order_item_#{@model.id} .warehouse_from_remains .st_#{size} .warehouse_from_remain").html(w_remain + ' / ' + supply_data_item)
      count_sum = count_sum + parseInt(@model.get("count_#{size}"))
    @model.set("count_sum", count_sum)

  setWarehouseRemain: =>
    all_remains_data = @model.get('all_remains_data')
    if all_remains_data is undefined
      all_remains_data = {}

    for size in [
              'xxs',
              'xs',
              's',
              'm',
              'l',
              'xl',
              'xxl',
              'xxxl',
              '34',
              '35',
              '36',
              '37',
              '38',
              '39',
              '40',
              '41',
    ]
      all_remains_data_item = 0

      all_remains_data_item = all_remains_data[size]
      if all_remains_data_item is undefined
        all_remains_data_item = 0

      @$("#order_item_#{@model.id} .warehouse_from_remains2 .st_#{size} .warehouse_from_remain2").html(all_remains_data_item - @model.get("count_#{size}"))

  serializeData: =>
    result = super
    can_orders_edit = helpers.user_has_permission bus.cache.user, 'can_orders_edit'
    can_order_items_sizes_view = helpers.user_has_permission bus.cache.user, 'can_order_items_sizes_view'
    can_order_item_all_delete = helpers.user_has_permission bus.cache.user, 'can_order_item_all_delete'
    can_order_item_self_delete = helpers.user_has_permission bus.cache.user, 'can_order_item_self_delete'

    console.log bus.cache.user

    can_delete = false
    if can_order_item_all_delete
      can_delete = true
    else
      if can_order_item_self_delete and @model.get('creator') == bus.cache.user['id']
        can_delete = true

    if @order_model.get('status_detail')['code'] != 'new'
      can_delete = false
    result['canDeleteProducts'] = can_delete

    if can_orders_edit
      if @order_model.get('status_detail')['code'] == 'new'
        result['canEditProducts'] = true
      else
        result['canEditProducts'] = false
    else
      result['canEditProducts'] = false
    result['canViewOrderItemsSizes'] = can_order_items_sizes_view
    result


class OrderItemComplexListView extends complex.ComplexCompositeView
  childView: OrderItemComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/orderitem_items'

  initialize: (options) =>
    @order_model = options.order_model

  childViewOptions: (model, index) =>
    child_data = {
      order_model: @order_model,
      childIndex: index
    }
    return child_data


class OrderItemComplexView extends complex.ComplexView
  template: require './templates/orderitem_complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout_product'

  regions:
#    region_item_list: '.region_item_list'
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @order_model = options.model
    @collection = new OrderItemCollection
      order_id: @order_model.id
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)


    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    urlParams["_client_id"] = @order_model.get('client')
    urlParams["page"] = 1

    @filter_model = new FilterModel(urlParams)

    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @filter_view = new FilterView
      model: @filter_model

    @filter_model.set('sort_by', store.get("order_#{@order_model.id}_order_item_sort_by"))
    @list_view = new OrderItemComplexListView
      collection: @collection
      order_model: @order_model

    @complex_view = new OrderItemComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'order_items'

    @listenTo @filter_model, 'change', =>
      store.set("order_#{@order_model.id}_order_item_sort_by", @filter_model.get('sort_by'))
      @complex_view.doFilter()
    @listenTo(bus.vent, 'product_item:update', @complex_view.doFilter)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()


module.exports = LayoutView

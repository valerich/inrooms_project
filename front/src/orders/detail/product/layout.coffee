marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

ScannerDataLayout = require './layout_scanner_data'
ProductSearchLayout = require './layout_product_search'
ProductSearchSupplyLayout = require './layout_product_search_supply'
ProductListLayout = require './layout_product'
FreeDRSummCollection = require '../../collections/free_dr_summ'
FreeDRSummView = require '../../free_dr/layout'


class NavbarView extends marionette.ItemView
  template: require './templates/navbar'
  events:
    'click .tab_link': 'onClickTabLink'
  initialize: (options) =>
    @tab_name = options.tab_name
    @order_model = options.model
    can_orders_edit = @order_model.get('can_edit')
    if can_orders_edit
      if @order_model.get('status_detail')['code'] == 'new'
        @canEdit = true
      else
        @canEdit = false
    else
      @canEdit = false

  onRender: =>
    @$(".tab_link[data-tab_name=#{@tab_name}]").addClass('active')
  onClickTabLink: (event) =>
    event.preventDefault()
    tab_name = $(event.target).parent().data('tab_name')
    @$(".tab_link").removeClass('active')
    @$(".tab_link[data-tab_name=#{tab_name}]").addClass('active')
    @trigger 'click', tab_name

  serializeData: =>
    result = super
    result['canEdit'] = @canEdit
    result


class ProductSearchNavbarView extends marionette.ItemView
  template: require './templates/product_search_navbar'
  events:
    'click .product_search_tab_link': 'onClickTabLink'
  initialize: (options) =>
    @tab_name = options.tab_name
    @order_model = options.model

  onRender: =>
    @$(".product_search_tab_link[data-tab_name=#{@tab_name}]").addClass('active')
  onClickTabLink: (event) =>
    event.preventDefault()
    tab_name = $(event.target).parent().data('tab_name')
    @$(".product_search_tab_link").removeClass('active')
    @$(".product_search_tab_link[data-tab_name=#{tab_name}]").addClass('active')
    @trigger 'click', tab_name


class ProductSearchTabLayout extends marionette.LayoutView
  template: require './templates/layout_product_search_tab'
  className: 'order_detail_product_search_tab_layout'
  regions:
    region_tab: '.region_product_search_tab'
    region_navbar: '.region_product_search_navbar'

  initialize: (options) =>
    @tab_name = 'exist_product_search'
    @order_model = options.model

  renderTab: (tab_name) =>
    view_class = null
    if tab_name is 'exist_product_search'
      view_class = ProductSearchLayout
    if tab_name is 'supply_product_search'
      view_class = ProductSearchSupplyLayout
    if tab_name is 'scanner_data'
      view_class = ScannerDataLayout
    if view_class
      @tab_view = new view_class
        model: @order_model
      @region_tab.show(@tab_view)
    else
      @region_tab.empty()

  onRender: =>
    @renderTab(@tab_name)
    navbar_view = new ProductSearchNavbarView
      model: @order_model
      tab_name: @tab_name
    @region_navbar.show(navbar_view)

    @listenTo navbar_view, 'click', (tab_name) =>
      @tab_name = tab_name
      @renderTab(tab_name)


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'order_detail_product_layout'
  regions:
    region_tab: '.region_product_tab'
    region_navbar: '.region_product_navbar'

  events:
    'click .save_order': 'onClickOrderSave'

  onClickOrderSave: =>
    can_order_category_tab_view = helpers.user_has_permission bus.cache.user, 'can_order_category_tab_view'
    if can_order_category_tab_view
      backbone.history.navigate "/order/#{@model.id}/category/", trigger: true
    else
      backbone.history.navigate "/order/#{@model.id}/common/", trigger: true

  initialize: (options) =>
    @order_model = options.model
    if @order_model.get('items_count') == 0
      @tab_name = 'product_search'
    else
      @tab_name = 'product_list'

  renderTab: (tab_name) =>
    view_class = null
    if tab_name is 'product_search'
      view_class = ProductSearchTabLayout
    if tab_name is 'product_list'
      view_class = ProductListLayout
    if view_class
      @tab_view = new view_class
        model: @order_model
      @region_tab.show(@tab_view)
    else
      @region_tab.empty()
    @listenTo(bus.vent, 'order:items:changed', @onItemsChanged)

  onRender: =>
    @renderTab(@tab_name)
    navbar_view = new NavbarView
      model: @order_model
      tab_name: @tab_name
    @region_navbar.show(navbar_view)

    @listenTo navbar_view, 'click', (tab_name) =>
      @tab_name = tab_name
      @renderTab(tab_name)

    order_kind = @order_model.get('kind')

    if order_kind in [2, 3]
      free_dr_summ_collection = new FreeDRSummCollection

      @addRegion("region_free_dr_summ", ".region_free_dr_summ")

      @free_dr_summ_view = new FreeDRSummView
         collection: free_dr_summ_collection
         client: @order_model.get('client')
         order_kind: order_kind

      @region_free_dr_summ.show(@free_dr_summ_view)

    @renderSidebar()

    @sidebar_obj = @$('#fixed_sidebar')
    @offset = @sidebar_obj.offset()
    @topOffset = @offset.top
    @topPadding = 150

    $(window).bind('scroll', (ev) =>
      @scrollSidebar()
    )

  onItemsChanged: =>
    @order_model.fetch().done =>
      @renderSidebar()

  renderSidebar: =>
    @$('.amount_full').html(@order_model.get('amount_full'))
    @$('.partner').html(@order_model.get('client_detail')['name'])
    @$('.main_collection_percentage').html(@order_model.get('main_collection_percentage'))
    if @order_model.get('kind') in [2, 3]
      @free_dr_summ_view.doFilter()

  scrollSidebar: =>
    if $(window).scrollTop() > @topPadding
      @sidebar_obj.stop().animate({marginTop: $(window).scrollTop() - @topOffset - @topPadding}, 200)
    else
      @sidebar_obj.stop().animate({marginTop: 0}, 200)

module.exports = ObjectDetailLayout

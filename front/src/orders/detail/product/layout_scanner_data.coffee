$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require '../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

ScannerDataCollection = require '../../collections/scanner_data'
ScannerDataModel = require '../../models/scanner_data'
OrderItemScannerDataModel = require '../../models/order_item_scanner_data'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/scanner_data_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    console.log 'togglePanel'
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class ScannerDataComplexItemView extends complex.ComplexItemView
  template: require './templates/scanner_data_item'

  events:
    'click .create_order_item': 'onClickCreateOrderItem'

  initialize: (options)=>
    @binder = new modelbinder
    @order_model = options.order_model

    @order_item_scanner_data = new OrderItemScannerDataModel
      order_id: @order_model.id
      scanner_data_id: @model.id
      series_count: @order_model.get('series_count')
    backbone.Validation.bind @, {
      model: @order_item_scanner_data
    }

  onRender: =>
    @binder.bind @order_item_scanner_data, @$el,
      series_count: '[name=series_count]'

  onClickCreateOrderItem: =>
    event.preventDefault()
    v = @order_item_scanner_data.validate()
    if @order_item_scanner_data.isValid()
      @order_item_scanner_data.save()
      .done =>
        helpers.generate_notyfication('success', 'Модели добавлены')
        bus.vent.trigger 'order:items:scanner_data_added', @model.get('id')
        bus.vent.trigger 'order:items:changed'
      .fail (fdata) ->
        helpers.modelErrorHandler @model, fdata
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class ScannerDataComplexListView extends complex.ComplexCompositeView
  childView: ScannerDataComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/scanner_data_items'

  initialize: (options) =>
    @order_model = options.order_model

  childViewOptions: (model, index) =>
    child_data = {
      order_model: @order_model,
      childIndex: index
    }
    return child_data


class ScannerDataComplexView extends complex.ComplexView
  template: require './templates/scanner_data_complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout_scanner_data'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @order_model = options.model
    @collection = new ScannerDataCollection
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
    @filter_model = new FilterModel
    @list_view = new ScannerDataComplexListView
      collection: @collection
      order_model: @order_model

    @complex_view = new ScannerDataComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'scanner_data'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @listenTo(bus.vent, 'order:items:scanner_data_added', @onScannerDataAdded)


  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

  onScannerDataAdded: (scanner_data_id) =>
    @collection.remove(scanner_data_id);


module.exports = LayoutView
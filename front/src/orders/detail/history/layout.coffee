_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

HistoryItemCollection = require '../../collections/history_item'
HistoryItemModel = require '../../models/history_item'


class HistoryItemItemView extends marionette.ItemView
  template: require './templates/history_item_item'
  className: 'history_items_item'

#  onBeforeDestroy: =>
#    @binder.unbind()
#
#  initialize: (options)=>
#    @binder = new modelbinder
#    @order_model = options.order_model


class HistoryItemCollectionView extends marionette.CollectionView
  childView: HistoryItemItemView
  className: 'history_items_list'

  initialize: (options) =>
    @order_model = options.order_model

#  childViewOptions: (model, index) =>
#    data = {
#      order_model: @order_model,
#      childIndex: index
#    }
#    return data



class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  regions:
    region_item_list: '.region_item_list'

  initialize: (options) =>
    @order_model = options.model

  onRender: =>
    @history_item_collection = new HistoryItemCollection
      order_id: @order_model.id

    @history_item_view = new HistoryItemCollectionView
      collection: @history_item_collection
      order_model: @order_model

    @region_item_list.show(@history_item_view)

    @history_item_collection.fetch
      reset: true
      data:
        page_size: 1000

module.exports = LayoutView

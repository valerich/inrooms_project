marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

ClientLayout = require './client/layout'
CommonLayout = require './common/layout'
CategoryLayout = require './category/layout'
ItemsLayout = require './product/layout'
HistoryLayout = require './history/layout'
OrderModel = require '../models/order'


class NavbarView extends marionette.ItemView
  template: require './templates/navbar'
  events:
    'click .tab_link': 'onClickTabLink'
  initialize: (options) =>
    @tab_name = options.tab_name
  onRender: =>
    @$(".tab_link[data-tab_name=#{@tab_name}]").addClass('active')
  onClickTabLink: (event) =>
    event.preventDefault()
    tab_name = $(event.target).parent().data('tab_name')
    @$(".tab_link").removeClass('active')
    @$(".tab_link[data-tab_name=#{tab_name}]").addClass('active')
    @trigger 'click', tab_name
  serializeData: =>
    result = super
    can_order_category_tab_view = helpers.user_has_permission bus.cache.user, 'can_order_category_tab_view'
    can_order_history_tab_view = helpers.user_has_permission bus.cache.user, 'can_order_history_tab_view'
    result['user'] = bus.cache.user
    result['user']['can_order_category_tab_view'] = can_order_category_tab_view
    result['user']['can_order_history_tab_view'] = can_order_history_tab_view
    result


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'order_detail_layout'
  regions:
    region_tab: '.region_tab'
    region_navbar: '.region_navbar'
  events:
    'click .list_link': 'onClickListLink'

  onClickListLink: (event) =>
    event.preventDefault()
    backbone.history.navigate '/order/', trigger: true

#  onClickTabLink: (event) =>
#    event.preventDefault()
#    if @modelFetchDone
#      tab_name = $(event.target).parent().data('tab_name')
#      backbone.history.navigate "/order/#{@model.id}/#{tab_name}/", trigger: false
#      @renderTab(tab_name)

  initialize: (options) =>
    @tab_name = options.tab_name
    @binder = new modelbinder
    @model = new OrderModel
      id: options.id

  onBeforeDestroy: =>
    @binder.unbind()

  renderTab: (tab_name) =>
    can_order_category_tab_view = helpers.user_has_permission bus.cache.user, 'can_order_category_tab_view'
    can_order_history_tab_view = helpers.user_has_permission bus.cache.user, 'can_order_history_tab_view'

    view_class = null
    if tab_name is 'common'
      view_class = CommonLayout
    if tab_name is 'items'
      view_class = ItemsLayout
    if tab_name is 'category' and can_order_category_tab_view
      view_class = CategoryLayout
    if tab_name is 'client'
      view_class = ClientLayout
    if tab_name is 'history' and can_order_history_tab_view
      view_class = HistoryLayout
    if view_class
      tab_view = new view_class
        model: @model
      @region_tab.show(tab_view)
    else
      @region_tab.empty()

  onRender: =>
    @binder.bind @model, @$el,
      id: '.order-id'
      status_name: '.order-status'
    @model.fetch().done =>
      @model.set("status_name", @model.get("status_detail")['name'])
      bus.vent.trigger 'menu:active:set', null, 'order'
      @renderTab(@tab_name)
      navbar_view = new NavbarView
        model: @model
        tab_name: @tab_name
      @region_navbar.show(navbar_view)
      @listenTo navbar_view, 'click', (tab_name) =>
        @tab_name = tab_name
        backbone.history.navigate "/order/#{@model.id}/#{tab_name}/", trigger: false
        @renderTab(tab_name)

module.exports = ObjectDetailLayout

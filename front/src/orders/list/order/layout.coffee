$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'

OrderCollection = require '../../collections/order'
OrderModel = require '../../models/order'
FreeDRSummCollection = require '../../collections/free_dr_summ'
FreeDRSummView = require '../../free_dr/layout'


class FilterModel extends complex.FilterModel
  defaults:
    exclude_client_pp: 2
    page: 1
    page_size: '10'
    page_count: 1


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onClickAdd'

  onClickAdd: =>
    if @can_create != 2
      addNewOrderView = new NewOrderView
        model: new OrderModel
          kind: @order_kind

      bus.vent.trigger 'modal:show', addNewOrderView,
        title: 'Добавить новый заказ'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @order_kind = options.order_kind
    @binder = new modelbinder
    @setPermissions()

  setPermissions: =>
    can_orders_edit = helpers.user_has_permission bus.cache.user, 'can_orders_edit'
    @can_view_order_filter_client = helpers.user_has_permission bus.cache.user, 'can_view_order_filter_client'
    @can_view_order_filter_products = helpers.user_has_permission bus.cache.user, 'can_view_order_filter_products'
    @can_orders_main_create = helpers.user_has_permission bus.cache.user, 'can_orders_main_create'
    @can_orders_defect_create = helpers.user_has_permission bus.cache.user, 'can_orders_defect_create'
    @can_orders_ten_percent_create = helpers.user_has_permission bus.cache.user, 'can_orders_ten_percent_create'

    @can_create = false
    if @can_orders_main_create and @order_kind == 1
      @can_create = true
    if @can_orders_defect_create and @order_kind == 2
      @can_create = true
    if @can_orders_ten_percent_create and @order_kind == 3
      @can_create = true

  serializeData: =>
    result = super
    result['user'] = bus.cache.user
    result['user']['can_view_order_filter_client'] = @can_view_order_filter_client
    result['user']['can_view_order_filter_products'] = @can_view_order_filter_products
    result['user']['can_orders_main_create'] = @can_orders_main_create
    result['user']['can_orders_defect_create'] = @can_orders_defect_create
    result['user']['can_orders_ten_percent_create'] = @can_orders_ten_percent_create
    result['can_create'] = @can_create
    result['order_kind'] = @order_kind
    result

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      status: '[name=status]'
      items__product: '[name=items__product]'
      client: '[name=client]'
      exclude_client_pp: '[name=exclude_client_pp]'
      sort_by: '.sort_list'

    helpers.initAjaxSelect2 @$('[name=status]'),
      url: "/api/orders/status/"
      placeholder: 'Выберите статус'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true
    helpers.initAjaxSelect2 @$('[name=items__product]'),
      url: '/api/products/product/'
      placeholder: "Выберите модель"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"
    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: "Выберите клиента"
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1


class OrderComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class Order2ComplexItemView extends complex.ComplexItemView
  template: require './templates/item2'
  triggers:
    'click .panel': 'click'


class OrderComplexListView extends complex.ComplexCompositeView
  childView: OrderComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class Order2ComplexListView extends complex.ComplexCompositeView
  childView: Order2ComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items2'


class OrderComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2

  initialize: (options) =>
    @order_kind = options.order_kind
    super

  onRender: =>
    super
    if @order_kind in [2, 3]
      free_dr_summ_collection = new FreeDRSummCollection

      @addRegion("region_free_dr_summ", ".region_free_dr_summ")

      free_dr_summ_view = new FreeDRSummView
         collection: free_dr_summ_collection
         order_kind: @order_kind

      @region_free_dr_summ.show(free_dr_summ_view)

      free_dr_summ_view.doFilter()

  serializeData: =>
    result = super
    result['order_kind'] = @order_kind
    result


class NewOrderView extends marionette.ItemView
  template: require './templates/new_order_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    client: 'Client'

  initialize: =>
    @binder = new modelbinder
    @can_set_client_on_order_create = helpers.user_has_permission bus.cache.user, 'can_set_client_on_order_create'

  onRender: =>
    if @can_set_client_on_order_create
      @binder.bind @model, @$el,
        'client': '[name=client]'

      helpers.initAjaxSelect2 @$('[name=client]'),
        url: '/api/clients/client/'
        placeholder: "Выберите клиента"
        minimumInputLength: -1
        get_extra_search_params: () =>
          kind: 1

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          backbone.history.navigate "/order/#{model.get('id')}/", {trigger: true}
          helpers.generate_notyfication('success', 'Заказ создан')
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()

  serializeData: =>
    result = super
    result['user'] = bus.cache.user
    result['user']['can_set_client_on_order_create'] = @can_set_client_on_order_create
    result


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'
  events:
    'click .list_type': 'onClickListType'

  initialize: (options) =>
    @order_kind = options.order_kind
    @listenTo(bus.vent, 'order:added', @onOrderAdded)

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    @collection = new OrderCollection

    @filter_model = new FilterModel(urlParams)
    @filter_model.set('kind', @order_kind)
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @filter_view = new FilterView
      model: @filter_model
      order_kind: @order_kind

  onOrderAdded: =>
    @complex_view.doFilter()

  onClickListType: (event) =>
    $target = $(event.target)
    list_type = $target.data('list_type')
    @renderOrderList(list_type)

  onRender: =>
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @region_filter.show(@filter_view)

    list_type = store.get 'order_list_type'
    if !list_type
      list_type = 'list1'
    @renderOrderList(list_type)

  renderOrderList: (list_type) =>
    store.set 'order_list_type', list_type
    if list_type is 'list1'
      OrderView = OrderComplexListView
    if list_type is 'list2'
      OrderView = Order2ComplexListView
    @$(".list_type").removeClass('active')
    @$(".list_type[data-list_type=#{list_type}]").addClass('active')

    list_view = new OrderView
      collection: @collection

    @complex_view = new OrderComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'orders'
      order_kind: @order_kind

    @region_complex.show(@complex_view)

    @listenTo @complex_view, 'childview:click', (view, options) =>
      backbone.history.navigate "/order/#{view.model.id}/", {trigger: true}

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @complex_view.doFilter()


module.exports = LayoutView
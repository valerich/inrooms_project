$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'


OrderLayout = require './order/layout'


class NavbarView extends marionette.ItemView
  template: require './templates/navbar'
  events:
    'click .tab_list_link': 'onClickTabLink'
  initialize: (options) =>
    @tab_name = options.tab_name
  onRender: =>
    @$(".tab_list_link[data-tab_name=#{@tab_name}]").addClass('active')
  onClickTabLink: (event) =>
    event.preventDefault()
    tab_name = $(event.target).parent().data('tab_name')
    @$(".tab_list_link").removeClass('active')
    @$(".tab_list_link[data-tab_name=#{tab_name}]").addClass('active')
    @trigger 'click', tab_name


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_tab_data': '.region_tab_data'
    'region_list_navbar': '.region_list_navbar'

  initialize: (options) =>
    @tab_name = 'main'

  renderTab: (tab_name) =>
    view_class = null
    if tab_name is 'main'
      kind = 1
      view_class = OrderLayout
    if tab_name is 'defect'
      kind = 2
      view_class = OrderLayout
    if tab_name is 'ten_percent'
      kind = 3
      view_class = OrderLayout
    if view_class
      tab_view = new view_class
        model: @model
        order_kind: kind
      @region_tab_data.show(tab_view)
    else
      @region_tab_data.empty()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'order'
    navbar_view = new NavbarView
      tab_name: @tab_name
    @region_list_navbar.show(navbar_view)
    @renderTab(@tab_name)
    @listenTo navbar_view, 'click', (tab_name) =>
      @tab_name = tab_name
      @renderTab(tab_name)

module.exports = LayoutView
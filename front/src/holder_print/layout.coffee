backbone = require 'backbone'
marionette = require 'backbone.marionette'
bus = require 'bus'
modelbinder = require 'backbone.modelbinder'
helpers = require 'helpers'
require 'jquery.inputmask'


class PriceHolderView extends marionette.LayoutView
  template: require './templates/price_holder_template'


class ShowcaseHolderSetView extends marionette.LayoutView
  template: require './templates/showcase_holder_set_template'

  events:
    'click [name=print_holder]': 'onClickPrintHolder'

  onClickPrintHolder: (event) =>
    event.preventDefault()
    product1 = @model.get('product1')
    if product1 == undefined
      product1 = ''
    price1 = @model.get('price1')
    if price1 == undefined
      price1 = ''
    product2 = @model.get('product2')
    if product2 == undefined
      product2 = ''
    price2 = @model.get('price2')
    if price2 == undefined
      price2 = ''
    product3 = @model.get('product3')
    if product3 == undefined
      product3 = ''
    price3 = @model.get('price3')
    if price3 == undefined
      price3 = ''
    product4 = @model.get('product4')
    if product4 == undefined
      product4 = ''
    price4 = @model.get('price4')
    if price4 == undefined
      price4 = ''
    product5 = @model.get('product5')
    if product5 == undefined
      product5 = ''
    price5 = @model.get('price5')
    if price5 == undefined
      price5 = ''
    url = "/holders/showcase/?as=html&product1=#{product1}&price1=#{price1}&product2=#{product2}&price2=#{price2}&product3=#{product3}&price3=#{price3}&product4=#{product4}&price4=#{price4}&product5=#{product5}&price5=#{price5}"
    window.open(url, '_blank')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      product1: '[name=product1]'
      price1: '[name=price1]'
      product2: '[name=product2]'
      price2: '[name=price2]'
      product3: '[name=product3]'
      price3: '[name=price3]'
      product4: '[name=product4]'
      price4: '[name=price4]'
      product5: '[name=product5]'
      price5: '[name=price5]'


class ShowcaseHolderView extends marionette.LayoutView
  template: require './templates/showcase_holder_template'
  events:
    'click .set_products': 'onClickSetProducts'

  onClickSetProducts: (event) =>
    event.preventDefault()
    showcaseView = new ShowcaseHolderSetView
      model: new backbone.Model

    bus.vent.trigger 'modal:show', showcaseView,
        title: 'Ввод товаров для печати'
        modal_size: 'lg'


class LastChanceHolderView extends marionette.LayoutView
  template: require './templates/last_chance_holder_template'

  events:
    'click .print': 'onPrintClick'

  onPrintClick: (event) =>
    console.log 'onClickPrint'
    url = "/static/last_chance.pdf"
    window.open(url, '_blank')


class ModeHolderSetView extends marionette.LayoutView
  template: require './templates/mode_holder_set_template'
  events:
    'click [name=print_holder]': 'onClickPrintHolder'

  onClickPrintHolder: (event) =>
    event.preventDefault()
    url = "/holders/mode/?as=html&day_from=#{@model.get('day_from')}&day_to=#{@model.get('day_to')}&time_from=#{@model.get('time_from')}&time_to=#{@model.get('time_to')}"
    window.open(url, '_blank')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      day_from: '[name=day_from]'
      day_to: '[name=day_to]'
      time_from: '[name=time_from]'
      time_to: '[name=time_to]'

    @$('[name=time_from]').inputmask("99:99")
    @$('[name=time_to]').inputmask("99:99")


class ModeHolderView extends marionette.LayoutView
  template: require './templates/mode_holder_template'
  events:
    'click .set_data': 'onClickSetData'

  onClickSetData: (event) =>
    event.preventDefault()
    modeModel = new backbone.Model
    modeModel.set('day_from', 'понедельник')
    modeModel.set('day_to', 'воскресенье')
    modeModel.set('time_from', '09:00')
    modeModel.set('time_to', '20:00')
    modeView = new ModeHolderSetView
      model: modeModel

    bus.vent.trigger 'modal:show', modeView,
        title: 'Ввод режима работы'
        modal_size: 'lg'


class SalesHolderDetailView extends marionette.LayoutView
  template: require './templates/sales_holder_detail_template.hbs'
  events:
    'click .print_sales': 'onClickPrintSales'

  onClickPrintSales: (event) =>
    event.preventDefault()
    $target = $(event.target)
    num = $target.data('num')
    url = "/static/Sale#{num}.png"
    window.open(url, '_blank')
    @destroy()


class SaleHolderView extends marionette.LayoutView
  template: require './templates/sale_holder_template'
  events:
    'click .select_sales': 'onClickSelectSales'

  onClickSelectSales: (event) =>
    event.preventDefault()
    salesView = new SalesHolderDetailView

    bus.vent.trigger 'modal:show', salesView,
        title: 'Выбор холдера Sale'
        modal_size: 'lg'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_price_holder': '.region_price_holder'
    'region_showcase_holder': '.region_showcase_holder'
    'region_last_chance_holder': '.region_last_chance_holder'
    'region_mode_holder': '.region_mode_holder'
    'region_sale_holder': '.region_sale_holder'

  initialize: =>
    @price_holder_view = new PriceHolderView
    @showcase_holder_view = new ShowcaseHolderView
    @last_chance_holder_view = new LastChanceHolderView
    @mode_holder_view = new ModeHolderView
    @sale_holder_view = new SaleHolderView

    @userCanViewHolderPrint = helpers.user_has_permission bus.cache.user, 'can_holder_print_view'

  onRender: =>
    if @userCanViewHolderPrint
      bus.vent.trigger 'menu:active:set', null, 'holder_print'
      @region_price_holder.show(@price_holder_view)
      @region_showcase_holder.show(@showcase_holder_view)
      @region_last_chance_holder.show(@last_chance_holder_view)
      @region_mode_holder.show(@mode_holder_view)
      @region_sale_holder.show(@sale_holder_view)


module.exports = LayoutView
backbone = require 'backbone'

RelatedProductModel = require '../models/related_product'


class RelatedProductCollection extends backbone.Collection
  model: RelatedProductModel

  initialize: (options) =>
    @url = "/api/products/product/#{options.object_id}/related_products/"

module.exports = RelatedProductCollection
backbone = require 'backbone'

ProductModel = require '../models/product'


class ProductCollection extends backbone.Collection
  model: ProductModel
  url: "/api/products/product/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ProductCollection
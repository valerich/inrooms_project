backbone = require 'backbone'

FieldModel = require '../models/field'


class FieldCollection extends backbone.Collection
  model: FieldModel
  url: =>
    return "/api/products/product/#{@product_id}/productfield/"


module.exports = FieldCollection
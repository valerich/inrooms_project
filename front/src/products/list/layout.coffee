$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
bootbox = require 'bootbox'
require 'select2'
require 'backbone-validation'
require 'jquery.inputmask'

ProductCollection = require '../collections/product'
ProductModel = require '../models/product'
OfferModel = require '../../offers/models/offer'
ImageLayout = require '../detail/image/layout'
OfferView = require './offer_modal_view'
SizesBoard = require '../../components/sizes_board/layout'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onAddClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onAddClick: =>
    addNewProductView = new NewProductView
      product_kind: @product_kind
      model: new ProductModel
        kind: @product_kind

    bus.vent.trigger 'modal:show', addNewProductView,
      title: 'Добавить новую модель'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @product_kind = options.product_kind

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      main_collection: '[name=main_collection]'
      offer: '[name=offer]'
      is_active: '[name=is_active]'
      has_images: '[name=has_images]'
      has_remains: '[name=has_remains]'
      warehouse_id_remains: '[name=warehouse_id_remains]'
      warehouse_id_no_remains: '[name=warehouse_id_no_remains]'
      has_price: '[name=has_price]'
      min_price: '[name=min_price]'
      max_price: '[name=max_price]'
      brand: '[name=brand]'
      is_consistency_move: '[name=is_consistency_move]'
      seasonality: '[name=seasonality]'
      is_public: '[name=is_public]'

    helpers.initAjaxSelect2 @$('[name=main_collection]'),
      url: '/api/products/collection/'
      placeholder: "Выберите главную категорию"
      minimumInputLength: -1
      text_attr: 'full_name'
      allowClear: true
      get_extra_search_params: () =>
        kind: @product_kind

    helpers.initAjaxSelect2 @$('[name=brand]'),
      url: '/api/brands/brand/'
      placeholder: "Выберите бренд"
      minimumInputLength: -1
      allowClear: true

    helpers.initAjaxSelect2 @$('[name=offer]'),
      url: '/api/offers/offer-approved/'
      placeholder: "Выберите предложение"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"
    helpers.initAjaxSelect2 @$('[name=warehouse_id_remains]'),
      url: '/api/warehouses/warehouse/'
      placeholder: "Выберите склад"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return obj.name
    helpers.initAjaxSelect2 @$('[name=warehouse_id_no_remains]'),
      url: '/api/warehouses/warehouse/'
      placeholder: "Выберите склад"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return obj.name


class ProductComplexItemView extends marionette.LayoutView
  template: require './templates/item'

  events:
    'click .image-link': 'onImageLinkClick'
    'click .image-edit': 'onImageEditClick'
    'click .save': 'saveModel'
    'click .details': 'detailsClick'
    'click .offer-data': 'onClickOfferData'

  modelEvents:
    'change:is_active': 'setPriceAvailable'

  regions:
    'priceBoardRegion': '.price-board-region'

  setPriceAvailable: =>
    priceSelector = @$('[name=price]')

    if @model.get('is_active')
      priceSelector
      .prop('disabled', true)
      .prop('readonly', true)
      .addClass('form-white')
    else
      priceSelector
      .prop('disabled', false)
      .prop('readonly', false)
      .removeClass('form-white')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @product_kind = options.product_kind

  detailsClick: (event) =>
    event.preventDefault()
    @trigger 'show:details'

  fetching: =>
    @model.fetch()
    .done =>
      @.render()

  onClickOfferData: =>
    OfferDataView = new OfferView
      model: new OfferModel
        id: @model.get('offer')

    bus.vent.trigger 'modal:show', OfferDataView,
      title: "Модель создана из предложения №:#{@model.get('offer')}"

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    can_view_offers_link = helpers.user_has_permission bus.cache.user, 'can_view_offers_link_in_product_list_item'
    can_product_price_view = helpers.user_has_permission bus.cache.user, 'can_product_price_view'
    can_product_price_edit = helpers.user_has_permission bus.cache.user, 'can_product_price_edit'
    can_product_is_active_view = helpers.user_has_permission bus.cache.user, 'can_product_is_active_view'
    s_data['canViewOffersLink'] = can_view_offers_link
    s_data['userCanPriceView'] = can_product_price_view
    s_data['userCanPriceEdit'] = can_product_price_edit
    s_data['userCanIsActiveView'] = can_product_is_active_view
    s_data

#  onClickCreateOrder: =>
#    addNewProductView = new NewProductView
#      model: new NewProductModel
#        name: @model.get('name')
#        series_type: 'c'
#
#    bus.vent.trigger 'modal:show', addNewProductView,
#      title: 'Добавить новую модель'

  onImageLinkClick: =>
    $.magnificPopup.open({
      type:'image',
      gallery: {
        enabled: true
      },
      removalDelay: 300,
      mainClass: 'mfp-fade',
      items: @model.get('images'),
      gallery: {
        enabled: true
      },
    })

  onImageEditClick: =>
    editImageView = new ImageLayout
      model: @model
      button_position: 'bottom'

    bus.vent.trigger 'modal:show', editImageView,
      title: 'Изображения модели'
      modal_size: 'lg'

  saveModel: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Модель сохранена')
    .fail (data) =>
      helpers.modelErrorHandler @model, data

  onRender: =>
    @setPriceAvailable()

    @$('[data-toggle="tooltip"]').tooltip();
    @binder.bind @model, @$el,
      name: '[name=name]'
      article: '[name=article]'
      main_collection: "[name=main_collection]"
      is_active: '[name=is_active]'
      price: '[name=price]'
      color: '[name=color]'
      intag_weight: '[name=intag_weight]'
      seasonality: '[name=seasonality]'
      public_price: '[name=public_price]'
      public_old_price: '[name=public_old_price]'

    helpers.initAjaxSelect2 @$("[name=main_collection]"),
      url: '/api/products/collection/'
      placeholder: 'Введите название коллекции'
      minimumInputLength: -1
      text_attr: 'full_name'
      get_extra_search_params: () =>
        kind: @product_kind

    helpers.initAjaxSelect2 @$("[name=color]"),
      url: '/api/products/color/'
      placeholder: 'Введите цвет'
      minimumInputLength: -1
      text_attr: 'name'
      createSearchChoicePosition: 'bottom'
      createSearchChoice: (term) =>
        return {
          id: term + '_4create'
          text: term + ' (создать)'
        }
    @$('[name=color]').on 'change', (event)=>
      url = "/api/products/color/"
      if event.added
        if /_4create/.test(event.added.id.toString())
          $.ajax
            url: url
            type: "post"
            data:
              name: event.added.id.replace('_4create', '')
            success: (data) =>
              @model.set('color', data.id)
              @$('[name=color]').val(data.id).change()
              helpers.generate_notyfication('success', 'Значение создано')

    @$('[name=article]').inputmask("9999/999")
    @$('[name=price]').inputmask("9{1,20}.9{1,2}")

    # какую из серий отображать (цифровую или буквенную)
    seriesType = @model.get('main_collection_detail')['series_type']
    if @model.get('kind') == 1
      # вывод доски с ценами
      @priceBoardRegion.show new SizesBoard
        model: @model
        showLetterSizes: seriesType == 'c'
        showNumberSizes: seriesType == 's'
    else
      count = 0
      _.each @model.get('remains'), ( val, key ) =>
        if key in ['msk', 'showroom']
          _.each val, ( val, key ) =>
            count += val
      @$('.price-board-region').html('<div class="col-md-12"><span>Остаток: <b>' + count + '</b></span></div>')

    backbone.Validation.bind @


class ProductComplexListView extends complex.ComplexCompositeView
  childView: ProductComplexItemView
  childViewContainer: ".region_product_panel_items"
  template: require './templates/items'
  childEvents:
    'show:details': 'showDetails'

  showDetails: (childView) =>
    backbone.history.navigate "/product/#{childView.model.get('id')}/", trigger: true

  initialize: (options) =>
    @listenTo(bus.vent, 'product:images:update', @onProductUpdate)
    @product_kind = options.product_kind

  onProductUpdate: (model) =>
    item_view = @.children.findByModel(model)
    item_view.fetching()

  childViewOptions: (model, index) =>
    child_data = {
      product_kind: @product_kind,
      childIndex: index
    }
    return child_data


class ProductComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2


class NewProductView extends marionette.ItemView
  template: require './templates/new_product_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    name: 'Название'
    article: 'Артикул'
    main_collection: 'Главная коллекция'
    color: 'Цвет'
    brand: 'Бренд'

  initialize: (options) =>
    @binder = new modelbinder
    @product_kind = options.product_kind

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'
      article: '[name=article]'
      main_collection: '[name=main_collection]'
      color: '[name=color]'
      brand: '[name=brand]'
      seasonality: '[name=seasonality]'

    helpers.initAjaxSelect2 @$("[name=brand]"),
      url: '/api/brands/brand/'
      placeholder: 'Введите название бренда'
      minimumInputLength: -1

    helpers.initAjaxSelect2 @$("[name=main_collection]"),
      url: '/api/products/collection/'
      placeholder: 'Введите название коллекции'
      minimumInputLength: -1
      text_attr: 'full_name'
      get_extra_search_params: () =>
        kind: @product_kind

    helpers.initAjaxSelect2 @$("[name=color]"),
      url: '/api/products/color/'
      placeholder: 'Введите цвет'
      minimumInputLength: -1
      text_attr: 'name'
      createSearchChoicePosition: 'bottom'
      createSearchChoice: (term) =>
        return {
          id: term + '_4create'
          text: term + ' (создать)'
        }
    @$('[name=color]').on 'change', (event)=>
      url = "/api/products/color/"
      if event.added
        if /_4create/.test(event.added.id.toString())
          $.ajax
            url: url
            type: "post"
            data:
              name: event.added.id.replace('_4create', '')
            success: (data) =>
              @model.set('color', data.id)
              @$('[name=color]').val(data.id).change()
              helpers.generate_notyfication('success', 'Значение создано')

    @$('[name=article]').inputmask({"mask":"9999/999", "clearMaskOnLostFocus": true})

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Модель создана')
          bus.vent.trigger 'product:added'
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @listenTo(bus.vent, 'product:added', @onProductAdded)

    @product_kind = options.product_kind

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    urlParams['kind'] = @product_kind

    @filter_model = new FilterModel(urlParams)
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @collection = new ProductCollection
    @list_view = new ProductComplexListView
      collection: @collection
      product_kind: @product_kind

    @complex_view = new ProductComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'products'
      product_kind: @product_kind

    @filter_view = new FilterView
      model: @filter_model
      product_kind: @product_kind

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    if @product_kind == 2
      active_menu = 'shop_equipment'
    else
      if @product_kind == 3
        active_menu = 'promo'
      else
        if @product_kind == 4
          active_menu = 'lighting'
        else
          active_menu = 'product'
    bus.vent.trigger 'menu:active:set', null, active_menu
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

  onProductAdded: =>
    @complex_view.doFilter()

  serializeData: =>
    s_data = super
    if @product_kind == 1
      s_data['list_name'] = 'моделей'
    if @product_kind == 2
      s_data['list_name'] = 'торгового оборудования'
    if @product_kind == 3
      s_data['list_name'] = 'рекламной продукции'
    if @product_kind == 4
      s_data['list_name'] = 'осветительных приборов'
    s_data


module.exports = LayoutView
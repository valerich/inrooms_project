backbone = require 'backbone'
marionette = require 'backbone.marionette'


class OfferView extends marionette.ItemView
  template: require './templates/offer_modal'

  initialize: =>
    console.log @model
    fetching = @.model.fetch()

    fetching.done =>
      @.render()


module.exports = OfferView
backbone = require 'backbone'


class CommentModel extends backbone.Model

  initialize: (options) =>
    @object_id = options.object_id

  url: =>
    if @id
      "/api/comments/comment/products/product/#{@object_id}/#{@id}/"
    else
      "/api/comments/comment/products/product/#{@object_id}/"

  validation:
      body: [
        {
          required: true,
          msg: 'Заполните текст комментария'
        }]

module.exports = CommentModel
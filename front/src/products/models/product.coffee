backbone = require 'backbone'


class ProductModel extends backbone.Model

  url: =>
    if @id
      "/api/products/product/#{@id}/"
    else
      "/api/products/product/"

  validation:
      seasonality: [
        {
          required: true,
          msg: 'Заполните сезонность'
        }
      ]
      main_collection: [
        {
          required: true,
          msg: 'Заполните категорию'
        }]
      name: [
        {
          required: true,
          msg: 'Заполните название'
        }]
      color: [
        {
          required: true,
          msg: 'Заполните цвет'
        }]
      article: [
        {
          required: true,
          length: 8,
          msg: 'Заполните артикул'
        }]
      brand: [
        {
          required: true,
          msg: 'Заполните бренд'
        }]

module.exports = ProductModel
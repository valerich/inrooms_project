marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'

CommonLayout = require './common/layout'
FieldLayout = require './field/layout'
ImageLayout = require './image/layout'
CollectionLayout = require './collection/layout'
LabelLayout = require './labels/layout'
PublicLayout = require './public_data/layout'

ProductModel = require '../models/product'
CommentModel = require '../models/comment'
CommentCollection = require '../collections/comment'


class CommentComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/comment_item'


class CommentComplexListView extends complex.ComplexCompositeView
  childView: CommentComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/comment_items'


class CommentComplexView extends complex.ComplexView
  template: require './templates/comment_complex_layout'
  showSearch: false
  scroll_on_update: false
  d: 2


class NewCommentView extends marionette.ItemView
  template: require './templates/new_comment_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    body: 'Текст'

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      'body': '[name=body]'

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          @.trigger 'comment:product:created'
          helpers.generate_notyfication('success', 'Комментарий создан')
          @destroy()
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class ExportView extends marionette.ItemView
  template: require './templates/export_modal'
  events:
    'click [name=export]': 'onExportBtnClick'
  translated_fields:
    has_common: 'Основное'
    has_images: 'Изображения'
    has_description: 'Описание'
    has_fields: 'Характеристики'
    has_comments: 'Комментарии'

  initialize: (options)=>
    @model = new backbone.Model
      has_common: true
      has_images: true
      has_description: true
      has_fields: true
    @product_id = options.product_id
    @format = options.format
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      'has_common': '[name=has_common]'
      'has_images': '[name=has_images]'
      'has_description': '[name=has_description]'
      'has_fields': '[name=has_fields]'
      'has_comments': '[name=has_comments]'

    backbone.Validation.bind @

  onExportBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      url = "/products/product/" + @product_id + "/export/?"
      if @model.get("has_common")
        url = url + "&has_common=" + @model.get("has_common")
      if @model.get("has_images")
        url = url + "&has_images=" + @model.get("has_images")
      if @model.get("has_description")
        url = url + "&has_description=" + @model.get("has_description")
      if @model.get("has_fields")
        url = url + "&has_fields=" + @model.get("has_fields")
      if @model.get("has_comments")
        url = url + "&has_comments=" + @model.get("has_comments")
      url = url + "&as=" + @format
      window.open(url, '_blank')
      @destroy()
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class HeaderView extends marionette.ItemView
  template: require './templates/header'
  className: 'product_header_view'
  events:
    'click .save': 'saveModel'
    'click .pdf_export': 'onClickPdfExport'
    'click .docx_export': 'onClickDocxExport'
    'click .print': 'onClickPrint'
    'click .copy_characteristics': 'onCopyCharacteristicsClick'

  onCopyCharacteristicsClick: =>
    bootbox.confirm "Вы действительно скопировать уход, состав, страну из данной модели во все, связанные с ней через предложение?", (result)=>
      if result == true
        $.ajax
          url: "/api/products/product/#{@model.id}/copy_characteristics/"
          type: 'post'
          success: (data) =>
            helpers.generate_notyfication('success', 'Этикетки скопированы')
          error: (data) =>
            helpers.modelErrorHandler @model, data

  saveModel: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Предложение сохранено')
    .fail (data) ->
      helpers.modelErrorHandler @model, data
  onClickPdfExport: =>
    addExportView = new ExportView(
      product_id: @model.id
      format: 'pdf'
    )
    bus.vent.trigger 'modal:show', addExportView,
      title: 'Скачать в PDF'
  onClickDocxExport: =>
    addExportView = new ExportView(
      product_id: @model.id
      format: 'docx'
    )
    bus.vent.trigger 'modal:show', addExportView,
      title: 'Скачать в DOCX'
  onClickPrint: =>
    addExportView = new ExportView(
      product_id: @model.id
      format: 'html'
    )
    bus.vent.trigger 'modal:show', addExportView,
      title: 'Распечатать'
  initialize: =>
    @binder = new modelbinder
  onBeforeDestroy: =>
    @binder.unbind()
  onRender: =>
    @$('[data-rel="tooltip"]').tooltip()
    @$('[data-toggle="tooltip"]').tooltip()


class NavbarView extends marionette.ItemView
  template: require './templates/navbar'
  events:
    'click .tab_link': 'onClickTabLink'
  initialize: (options) =>
    @tab_name = options.tab_name
  onRender: =>
    @$(".tab_link[data-tab_name=#{@tab_name}]").addClass('active')
  onClickTabLink: (event) =>
    event.preventDefault()
    tab_name = $(event.target).parent().data('tab_name')
    @$(".tab_link").removeClass('active')
    @$(".tab_link[data-tab_name=#{tab_name}]").addClass('active')
    @trigger 'click', tab_name

  serializeData: =>
    s_data = super
    userCanViewProductPublicTabFilter = helpers.user_has_permission bus.cache.user, 'can_product_public_tab_view'
    s_data['userCanViewProductPublicTabFilter'] = userCanViewProductPublicTabFilter
    return s_data


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'product_detail_layout'
  regions:
    region_tab: '.region_tab'
    region_header: '.region_header'
    region_navbar: '.region_navbar'
    region_comments: '.region_comments'
  events:
    'click .list_link': 'onClickListLink'
    'click .add_comment': 'onClickAddComment'

  onClickAddComment: (event) =>
    addNewCommentView = new NewCommentView(
      model: new CommentModel(
        object_id: @model.id
      )
    )

    bus.vent.trigger 'modal:show', addNewCommentView,
      title: 'Добавить новый Комментарий'

    @listenTo addNewCommentView, "comment:product:created", @onCommentCreated

  onClickListLink: (event) =>
    event.preventDefault()
    backbone.history.navigate "/#{@active_menu}/", trigger: true

  onClickTabLink: (event) =>
    event.preventDefault()
    if @modelFetchDone
      tab_name = $(event.target).parent().data('tab_name')
      backbone.history.navigate "/product/#{@model.id}/#{tab_name}/", trigger: false
      @renderTab(tab_name)

  initialize: (options) =>
    @tab_name = options.tab_name
    @binder = new modelbinder
    @model = new ProductModel(id: options.id)
    @header_view = new HeaderView
      model: @model

    @comment_collection = new CommentCollection
      object_id: @model.id
    @comment_list_view = new CommentComplexListView
      collection: @comment_collection

    @comment_complex_view = new CommentComplexView
      collection: @comment_collection
      list_view: @comment_list_view
      name: 'comments'

#    @filter_view = new FilterView
#      model: @filter_model
#
#    @listenTo @filter_model, 'change', =>
#      @complex_view.doFilter()

  onCommentCreated: =>
    console.log 'onCommentCreated'
    @comment_complex_view.doFilter()

  onBeforeDestroy: =>
    @binder.unbind()

  renderTab: (tab_name) =>
    view_class = null
    if tab_name is 'common'
      view_class = CommonLayout
    if tab_name is 'field'
      view_class = FieldLayout
    if tab_name is 'image'
      view_class = ImageLayout
    if tab_name is 'collection'
      view_class = CollectionLayout
    if tab_name is 'label'
      view_class = LabelLayout
    if tab_name is 'public_data'
      view_class = PublicLayout

    if view_class
      tab_view = new view_class
        model: @model
      @region_tab.show(tab_view)
    else
      @region_tab.empty()

  onRender: =>
    @binder.bind @model, @$el,
      name: '.name'
      id: '.id'

    @active_menu = 'product'

    @model.fetch().done =>
      @renderTab(@tab_name)

      navbar_view = new NavbarView
        model: @model
        tab_name: @tab_name
      @region_navbar.show(navbar_view)

      @listenTo navbar_view, 'click', (tab_name) =>
        @tab_name = tab_name
        backbone.history.navigate "/product/#{@model.id}/#{tab_name}/", trigger: false
        @renderTab(tab_name)

      @region_header.show(@header_view)
      @region_comments.show(@comment_complex_view)
      @comment_complex_view.doFilter()

      @product_kind = @model.get('kind')
      if @product_kind == 2
          @active_menu = 'shop_equipment'
        else
          if @product_kind == 3
            @active_menu = 'promo'
          else
            if @product_kind == 4
              @active_menu = 'lighting'

    bus.vent.trigger 'menu:active:set', null, @active_menu

module.exports = ObjectDetailLayout

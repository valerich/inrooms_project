marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
bootbox = require 'bootbox'
complex = require 'complex'
require 'bootbox'
FieldListView = require './field'

#FieldModel = require '../../models/field'
#FieldCollection = require '../../collections/field'


class FieldModel extends backbone.Model
  url: =>
    return "/api/products/productfield/#{@id}/"

class FieldCollection extends backbone.Collection
  model: FieldModel
  url: =>
    return "/api/products/product/#{@product_id}/productfield/"

  parse: (response)=>
    @count = response.count
    return response.results


class TemplateCollection extends backbone.Collection
  url: '/api/products/template/'
  parse: (response)=>
    @count = response.count
    return response.results


class TemplateComplexView extends complex.ComplexView
  template: require './templates/intag_template_layout'
  showSearch: true
  page_size: '10'
  d: 1
  show_page_list: false
  page_size_list: []


class IntagCollection extends backbone.Collection
  url: '/api/products/intag/'
  parse: (response)=>
    @count = response.count
    return response.results


class IntagItemView extends marionette.ItemView
  template: require './templates/intag_item'
  triggers:
    'click': 'click'
  initialize: (options) =>
    @name = options.name
    @functional_description = options.functional_description
  onRender: =>
    @$el.addClass "#{@name}_item"


class IntagComplexView extends complex.ComplexView
  template: require './templates/intag_template_layout'
  showSearch: true
  page_size: '10'
  d: 1
  show_page_list: false
  page_size_list: []


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  className: 'product_field_layout'
  show_alert_key: 'show_product_field_alert'
  regions:
    'region_template_list': '.region_template_list'
    'region_intag_list': '.region_intag_list'
    'region_productfield_list': '.region_productfield_list'
  events:
    'click .tab_alert>button': 'closeAlert'

  closeAlert: =>
    store.set(@show_alert_key, false)
    @$('.tab_alert').addClass('hide')

  serializeData: =>
    result = super
    if store.get(@show_alert_key) is undefined
      store.set(@show_alert_key, true)
    result.show_alert = store.get(@show_alert_key) is true
    return result

  initialize: =>
    @template_collection = new TemplateCollection
    @template_list_view = new TemplateComplexView
      collection: @template_collection
      name: 'intag_template_list'

    @intag_collection = new IntagCollection()
    @intag_list_view = new IntagComplexView
      collection: @intag_collection
      name: 'intag_list'
      item_view: IntagItemView

    @field_collection = new FieldCollection
    @field_collection.product_id = @model.id
    @field_list_view = new FieldListView
      collection: @field_collection
      model: @model

    @listenTo @template_list_view, 'childview:click', (view, options) =>
      bootbox.confirm "Применить шаблон \"#{view.model.get('name')}\"?", (result)=>
        if result == true
          $.ajax
            url: "/api/products/template/apply/"
            type: 'post'
            data:
              template: view.model.id
              product: @model.id
            success: =>
              helpers.generate_notyfication('success', "Шаблон применен")
              @field_collection.fetch()

    @listenTo @intag_list_view, 'childview:click', (view, options) =>
      $.ajax
        url: "/api/products/intag/apply/"
        type: 'post'
        data:
          intag: view.model.id
          product: @model.id
        success: (response) =>
          @field_collection.fetch()

  onRender: =>
    @region_template_list.show(@template_list_view)
    @template_list_view.doFilter()

    @region_intag_list.show(@intag_list_view)
    @intag_list_view.doFilter()

    @region_productfield_list.show(@field_list_view)
    @field_collection.fetch()

module.exports = LayoutView

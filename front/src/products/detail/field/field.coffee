marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
#toastr = require 'toastr'
complex = require 'complex'
bootbox = require 'bootbox'
template_intag_category = require './templates/intag_category'


class FieldItemView extends marionette.ItemView
  template: require './templates/field_item'
  className: 'field_item'
  events:
    'click .remove': 'onClickRemove'
    'click .clear': 'onClickClear'
  onClickRemove: (event) =>
    event.preventDefault()
    intag = @model.get 'intag'
    name = intag.name
    bootbox.confirm "Удалить характеристику #{name}?", (result)=>
      if result == true
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')
  onClickClear: (event) =>
    event.preventDefault()
    $.ajax
      url: "/api/products/productfield/#{@model.id}/clear/"
      type: 'post'
      success: =>
        @model.fetch().done =>
          helpers.generate_notyfication('success', 'Очищено')
          @render()

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  initChoiceSelect2: =>
    intag = @model.get('intag')
    choices_options = {
      placeholder: 'Выберите значения'
      minimumInputLength: -1
      maximumSelectionSize: 10
      createSearchChoicePosition: 'bottom'
      createSearchChoice: (term) =>
        if intag.kind is 2
          if _.isNaN(parseInt(term))
            return null
          if !_.isNull(intag.min_value) and intag.min_value > parseFloat(term)
            return null
          if !_.isNull(intag.max_value) and intag.max_value < parseFloat(term)
            return null

        selected_unit = @$('[name=unit]').val()
        if selected_unit
          selected_unit = parseInt(selected_unit)
          unit = _.findWhere(intag.unit, id: selected_unit)
          if unit
            term += ' ' + unit.unit_name

        return {
        id: term + '_4create'
        text: term + ' (создать)'
        }

      query: (options) ->
        term = options.term.toLowerCase()
        results = []
        page_size = 10
        $.ajax
          url: "/api/products/intag/#{intag.id}/choices/"
          dataType: 'json'
          data:
            search: term
            page_size: page_size
            page: options.page
          success: (response) =>
            results = response.results
            for obj in results
              obj.text = obj.value
            more = ( options.page + 1 ) * page_size < response.count
            options.callback
              results: results
              more: more
    }
    if intag.is_many
      choices_options.tags = []
      choices_options.multiple = true
    else
      choices_options.multiple = false
      choices_options.allowClear = true

    @$('[name=choices]').select2 choices_options

    intag_choices = @model.get('intag_choices')
    if !_.isEmpty(intag_choices)
      if intag.is_many
        initial_data = []
        for choice in intag_choices
          initial_data.push({
            text: choice.value
            id: choice.id
          })
      else
        initial_data = {
          text: intag_choices[0].value
          id: intag_choices[0].id
        }
      @$('[name=choices]').select2('data', initial_data)

    @$('[name=choices]').on 'change', (event)=>
      url = "/api/products/productfield/#{@model.id}/choice/"
      if event.added
        if /_4create/.test(event.added.id.toString())
          $.ajax
            url: url
            type: "put"
            data:
              choice: event.added.id.replace('_4create', '')
            success: =>
              helpers.generate_notyfication('success', "Значение создано")
              @model.fetch().done @render
        else
          $.ajax
            url: url
            type: "post"
            data:
              choice: event.added.id
            success: =>
              if intag.is_many
                helpers.generate_notyfication('success', "Значение добавлено")
              else
                helpers.generate_notyfication('success', "Значение изменено")


      else if event.removed
        if _.isNumber(event.removed.id)
          $.ajax
            url: url
            type: "delete"
            data:
              choice: event.removed.id
            success: =>
              helpers.generate_notyfication('success', "Значение удалено")

  onRender: =>
    console.log @model
    intag = @model.get('intag')
    console.log intag
    @$el.attr 'data-intag_weight', intag.weight
    @initChoiceSelect2()


class FieldListView extends marionette.CompositeView
  template: require './templates/field_list'
  childViewContainer: '.childViewContainer'
  childView: FieldItemView
  className: 'field_list'
  events:
    'submit .search_form': 'onSubmitSearchForm'
    'click .clear_search': 'onClearSearch'
    'click .clear_all_fields': 'clearAllFields'
    'click .remove_all_fields': 'removeAllFields'

  initialize: (options) =>
    @model = options.model

#    $.ajax
#      url: "/api/products/product/#{@model.id}/productfield/update/"
#      type: 'post'
#      dataType: 'json'
#      contentType: 'application/json'
#      data: JSON.stringify
#      success: (response) =>
#        @collection.fetch().done =>
#          helpers.generate_notyfication('success', success_message)
#          @render()


  clearAllFields: =>
    bootbox.confirm "Очистить все характеристики?", (result)=>
      if result == true
        $.ajax
          url: "/api/products/product/#{@model.id}/productfield/clear/"
          type: 'post'
          success: (response) =>
            @collection.fetch().done =>
              helpers.generate_notyfication('success', 'Характеристики очищены')
              @render()
  removeAllFields: =>
    bootbox.confirm "Удалить все характеристики?", (result)=>
      if result == true
        $.ajax
          url: "/api/products/product/#{@model.id}/productfield/remove/"
          type: 'post'
          success: (response) =>
            @trigger 'reset'
            @collection.fetch().done =>
              helpers.generate_notyfication('success', 'Характеристики удалены')
              @render()

  hideIntagCategory: =>
    @$('.intag_category').each (index, el) =>
      if $(el).find('.field_item').not('.hide').length > 0
        $(el).removeClass('hide')
      else
        $(el).addClass('hide')


  onSubmitSearchForm: (event) =>
    event.preventDefault()
    search = @$('[name=search]').val()
    if search
      @children.each (view) =>
        intag = view.model.get('intag')
        if intag.name.indexOf(search) is -1
          view.$el.addClass('hide')
        else
          view.$el.removeClass('hide')
    else
      @children.each (view) =>
        view.$el.removeClass('hide')
    @hideIntagCategory()

  onClearSearch: =>
    @$('[name=search]').val('')
    @$('.search_form').submit()

  onRender: =>
    @$('.icon').tooltip()

  attachHtml: (collectionView, childView, index) =>
    collectionView._insertAfter(childView)

  _insertAfter: (childView) =>
    intagcategory_dict = bus.cache.intagcategory_dict
    intag = childView.model.get('intag')
    category_obj = intagcategory_dict[intag.category]
    if !category_obj
      category_obj =
        weight: 0
        id: '0'
        name: 'Общее'
    $el = @$(@childViewContainer)
    $category = $el.find("[data-category_id=#{category_obj.id}]")
    if $category.length is 0
      $category = $(template_intag_category(category_obj))
      $category.find('.toggle_intag_item_list').on 'click', (event)=>
        event.preventDefault()
        $category.find('.toggle_intag_item_list').toggleClass('hide_list')
        $category.find('.intag_item_list').toggleClass('hide')

      weight = category_obj.weight
      prev_weight = null
      $el.find('[data-weight]').each (index, tmp_el) =>
        tmp_weight = parseInt($(tmp_el).data('weight'))
        if prev_weight is null
          if weight < tmp_weight
            prev_weight = tmp_weight
        else
          if weight < tmp_weight < prev_weight
            prev_weight = tmp_weight
      if prev_weight
        $category.insertAfter($el.find("[data-weight=#{prev_weight}]"))
      else
        $category.prependTo($el)


    $list = $category.find('.intag_item_list')
    $list.append(childView.el)
    $items = $list.find('.field_item')
    $items.sort (a, b) =>
      a = parseInt($(a).data "intag_weight")
      b = parseInt($(b).data "intag_weight")
      if a > b
        return -1
      else if a < b
        return 1
      else
        return 0
    $items.detach().appendTo($list)


module.exports = FieldListView

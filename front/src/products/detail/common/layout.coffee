marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
#toastr = require 'toastr'
complex = require 'complex'
#sweetalert = require 'sweetalert'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .tab_alert>button': 'closeAlert'

  show_alert_key: 'show_product_common_alert'
  closeAlert: =>
    store.set(@show_alert_key, false)
    @$('.tab_alert').addClass('hide')

  serializeData: =>
    result = super
    if store.get(@show_alert_key) is undefined
      store.set(@show_alert_key, true)
    result.show_alert = store.get(@show_alert_key) is true
    return result

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    console.log 'CommonView'
    @binder.bind @model, @$el,
      name: '[name=name]'
      article: '[name=article]'
      main_collection: '[name=main_collection]'
      links: '[name=links]'
#      description: '[name=description]'

    helpers.initAjaxSelect2 @$('[name=main_collection]'),
      url: '/api/products/collection/'
      placeholder: 'Введите название коллекции'
      allowClear: true
      minimumInputLength: -1
      text_attr: 'full_name'

#    @$('[name=description]').summernote
#      onChange: (description) =>
#        @model.set 'description', description


module.exports = LayoutView

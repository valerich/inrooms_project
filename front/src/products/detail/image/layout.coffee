marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
#toastr = require 'toastr'
complex = require 'complex'
#sweetalert = require 'sweetalert'
Sortable = require 'sortablejs'
require 'blueimp-file-upload'
require 'magnific-popup'
require 'cropper'

class ImageModel extends backbone.Model
  initialize: =>
    @on 'change', (model, options) =>
      if @changed.visual_order isnt undefined or options.xhr
        return
      @save({patch: true}).done =>
        if @changed.visual_order is undefined
          helpers.generate_notyfication('success', 'Сохранено')
  url: =>
    return "/api/products/image/#{@id}/"

class ImageCollection extends backbone.Collection
  model: ImageModel

  parse: (response) =>
    @count = response.count
    return response.results


class ImageCropperItemView extends marionette.ItemView
  template: require './templates/modal_cropper'
  events:
    'click [name=create]': 'onCreateBtnClick'

  initialize: (options) =>
    @product_model = options.product_model

  onRender: =>
    @$('.image_data').cropper
      responsive: false
      width: 1000
      height: 1000

  onCreateBtnClick: (event) =>
    event.preventDefault()
    data = @$('.image_data').cropper('getCroppedCanvas').toDataURL('image/jpeg')
    $.ajax
        url: '/api/products/image/' + @model.id + '/reload/'
        type: "post"
        data:
          image: data
        success: (data) =>
          helpers.generate_notyfication('success', 'Значение создано')
          bus.vent.trigger 'product:images:cropped', @product_model
          @destroy()


class ImageItemView extends marionette.ItemView
  template: require './templates/image_item'
  className: 'image_item'
  events:
    'click .image-delete': 'onClickRemove'
    'click .image-link': 'onClickImageLink'

  onClickRemove: =>
    @model.destroy().done =>
      helpers.generate_notyfication('success', 'Удалено')
      bus.vent.trigger 'product:images:update', @product_model

  onClickImageLink: (event) =>
    imageCropperView = new ImageCropperItemView
      model: @model
      product_model: @product_model

    bus.vent.trigger 'modal2:show', imageCropperView,
      title: 'Редактирование изображения'

#    $.magnificPopup.open
#      type:'image'
#      mainClass: 'mfp-fade'
#      items: [{'src': @model.get('image')}]
#      midClick: true

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @product_model = options.product_model

  onRender: (options) =>
    @$el.data('model_id', @model.id)


class ImageCollectionView extends marionette.CollectionView
  childView: ImageItemView
  className: 'image_list'

  initialize: (options) =>
    @product_model = options.product_model

  childViewOptions: (model, index) =>
    data = {
      product_model: @product_model,
      childIndex: index
    }
    return data

  onRender: =>
    @sortable = new Sortable @el,
      onSort: @onSort
      handle: '.image'

  onSort: (event) =>
    data = []
    @$el.children().each (index, el) =>
      model_id = $(el).data('model_id')
      data.push({
        id: model_id
        visual_order: index
      })
      model = @collection.find id: model_id
      model.set 'visual_order', index
    if data
      $.ajax
        type: 'post'
        url: '/api/products/image-order/'
        contentType: 'application/json'
        data: JSON.stringify(data)
        success: =>
          helpers.generate_notyfication('success', 'Порядок сохранен')
          bus.vent.trigger 'product:images:update', @product_model


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  events:
    'click .tab_alert>button': 'closeAlert'
  show_alert_key: 'show_product_image_alert'
  regions:
    region_image_list: '.region_image_list'
  className: 'image_layout'

  closeAlert: =>
    store.set(@show_alert_key, false)
    @$('.tab_alert').addClass('hide')

  serializeData: =>
    result = super
    if store.get(@show_alert_key) is undefined
      store.set(@show_alert_key, true)
    result.show_alert = store.get(@show_alert_key) is true
    result.button_position = @button_position
    return result

  initialize: (options) =>
    @image_collection = new ImageCollection
    @image_collection.url = "/api/products/product/#{@model.id}/image/"

    @image_view = new ImageCollectionView
      collection: @image_collection
      product_model: @model

    if options.button_position isnt undefined
      @button_position = options.button_position
    else
      @button_position = 'top'

  onRender: =>
    @region_image_list.show(@image_view)
    @image_collection.fetch()

    @$('.fileupload').fileupload
      url: "/api/products/product/#{@model.id}/image-upload/"
      dataType: 'json'
      done: (e, response) =>
        @image_collection.fetch()
        bus.vent.trigger 'product:images:update', @model

    @listenTo(bus.vent, 'product:images:cropped', @onImagesCropped)

  onImagesCropped: (model) =>
    console.log('onImagesCropped')
    @image_collection.reset()
    @image_collection.fetch()
    bus.vent.trigger 'product:images:update', @model


module.exports = LayoutView

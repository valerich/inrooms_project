marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
#toastr = require 'toastr'
complex = require 'complex'
care_item_select2_item_template = require '../../../care/templates/select2_item'



class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .tab_alert>button': 'closeAlert'

  show_alert_key: 'show_product_common_alert'
  closeAlert: =>
    store.set(@show_alert_key, false)
    @$('.tab_alert').addClass('hide')

  serializeData: =>
    result = super
    if store.get(@show_alert_key) is undefined
      store.set(@show_alert_key, true)
    result.show_alert = store.get(@show_alert_key) is true
    return result

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    console.log 'CommonView'
    @binder.bind @model, @$el,
      consistency_value1: '[name=consistency_value1]'
      consistency_value2: '[name=consistency_value2]'
      consistency_value3: '[name=consistency_value3]'
      consistency_value4: '[name=consistency_value4]'
      consistency_type1: '[name=consistency_type1]'
      consistency_type2: '[name=consistency_type2]'
      consistency_type3: '[name=consistency_type3]'
      consistency_type4: '[name=consistency_type4]'
      brand: '[name=brand]'
      country: '[name=country]'
      wash: '[name=wash]'
      bleach: '[name=bleach]'
      drying: '[name=drying]'
      ironing: '[name=ironing]'
      professional_care: '[name=professional_care]'
      tnved_code: '[name=tnved_code]'

    helpers.initAjaxSelect2 @$('[name=tnved_code]'),
      url: '/api/products/tnved_code/'
      placeholder: 'Введите название ТНВЭД'
      allowClear: true
      minimumInputLength: -1
      text_attr: (obj) =>
        aa = "#{obj.name}."
        if obj['material_detail']
          aa += " #{obj.material_detail.name}"
        return aa

    helpers.initAjaxSelect2 @$('[name=consistency_type1]'),
      url: '/api/consistency/consistency_item/'
      placeholder: 'Введите название состава'
      allowClear: true
      minimumInputLength: -1

    helpers.initAjaxSelect2 @$('[name=consistency_type2]'),
      url: '/api/consistency/consistency_item/'
      placeholder: 'Введите название состава'
      allowClear: true
      minimumInputLength: -1

    helpers.initAjaxSelect2 @$('[name=consistency_type3]'),
      url: '/api/consistency/consistency_item/'
      placeholder: 'Введите название состава'
      allowClear: true
      minimumInputLength: -1

    helpers.initAjaxSelect2 @$('[name=consistency_type4]'),
      url: '/api/consistency/consistency_item/'
      placeholder: 'Введите название состава'
      allowClear: true
      minimumInputLength: -1

    helpers.initAjaxSelect2 @$('[name=brand]'),
      url: '/api/brands/brand/'
      placeholder: 'Введите название бренда'
      allowClear: true
      minimumInputLength: -1

    helpers.initAjaxSelect2 @$('[name=country]'),
      url: '/api/products/country/'
      placeholder: 'Введите название страны'
      allowClear: true
      minimumInputLength: -1

    helpers.initAjaxSelect2 @$('[name=wash]'),
      url: '/api/care/care_item/'
      placeholder: 'Введите выберите параметры стирки'
      allowClear: true
      minimumInputLength: -1
      text_attr: (product) =>
       care_item_select2_item_template(product)
      get_extra_search_params: () =>
        kind: 1
        page_size: 100
      formatResult: (product) =>
        care_item_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    helpers.initAjaxSelect2 @$('[name=bleach]'),
      url: '/api/care/care_item/'
      placeholder: 'Введите выберите параметры отбеливания'
      allowClear: true
      minimumInputLength: -1
      text_attr: (product) =>
       care_item_select2_item_template(product)
      get_extra_search_params: () =>
        kind: 2
        page_size: 100
      formatResult: (product) =>
        care_item_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    helpers.initAjaxSelect2 @$('[name=drying]'),
      url: '/api/care/care_item/'
      placeholder: 'Введите выберите параметры сушки'
      allowClear: true
      minimumInputLength: -1
      text_attr: (product) =>
       care_item_select2_item_template(product)
      get_extra_search_params: () =>
        kind: 3
        page_size: 100
      formatResult: (product) =>
        care_item_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    helpers.initAjaxSelect2 @$('[name=ironing]'),
      url: '/api/care/care_item/'
      placeholder: 'Введите выберите параметры глажения'
      allowClear: true
      minimumInputLength: -1
      text_attr: (product) =>
       care_item_select2_item_template(product)
      get_extra_search_params: () =>
        kind: 4
        page_size: 100
      formatResult: (product) =>
        care_item_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    helpers.initAjaxSelect2 @$('[name=professional_care]'),
      url: '/api/care/care_item/'
      placeholder: 'Введите выберите параметры профессионального ухода'
      allowClear: true
      minimumInputLength: -1
      text_attr: (product) =>
       care_item_select2_item_template(product)
      get_extra_search_params: () =>
        kind: 5
      formatResult: (product) =>
        care_item_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup


module.exports = LayoutView

marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'

RelatedProductCollection = require '../../collections/related_product'


class RelatedProductComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/related_product_item'


class RelatedProductComplexListView extends complex.ComplexCompositeView
  childView: RelatedProductComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/related_product_items'


class RelatedProductComplexView extends complex.ComplexView
  template: require './templates/related_product_complex_layout'
  showSearch: false
  scroll_on_update: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .set_related_products': 'onClickSetRelatedProducts'

  regions:
    'regino_related_products': '.region_related_products'

  serializeData: =>
    result = super
    return result

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

    @related_product_collection = new RelatedProductCollection
      object_id: @model.id
    @related_product_list_view = new RelatedProductComplexListView
      collection: @related_product_collection

    @related_product_complex_view = new RelatedProductComplexView
      collection: @related_product_collection
      list_view: @related_product_list_view
      name: 'related_product'

  onRender: =>
    @binder.bind @model, @$el,
      is_public: '[name=is_public]'
      description: '[name=description]'
      business_name: '[name=business_name]'
      public_price: '[name=public_price]'
      public_old_price: '[name=public_old_price]'

    @regino_related_products.show(@related_product_complex_view)
    @related_product_complex_view.doFilter()

  onClickSetRelatedProducts: (event) =>
    bootbox.confirm "Вы действительно хотите перепривязать товары?", (result)=>
      if result == true
         $.ajax
          url: "/api/products/product/#{@model.id}/set_related_products/"
          type: 'post'
          success: =>
            helpers.generate_notyfication('success', 'Готово')
            @related_product_complex_view.doFilter()
          error: (data) =>
            helpers.modelErrorHandler @model, data


module.exports = LayoutView

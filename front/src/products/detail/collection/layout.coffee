marionette = require 'backbone.marionette'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
#toastr = require 'toastr'
complex = require 'complex'
#sweetalert = require 'sweetalert'

class ProductCollectionCollection extends backbone.Collection
  parse: (response) =>
    @count = response.count
    return response.results

class ProductCollectionItemView extends marionette.ItemView
  className: 'collection_item'
  template: require './templates/collection_item'
  onRender: =>
    @$("[name=active][data-id=#{@model.id}]").on 'click', @clickActive

  onClickName: (event) =>
    event.preventDefault()
    $target = $(event.target)
    $target.toggleClass('folded')

    folded = $target.hasClass('folded')
    if folded
      @$('.branch').addClass('hide')
    else
      @$('.branch').removeClass('hide')

  addFoldingEvent: =>
    @$('.foldable').eq(0).on('click', @onClickName)
    @$('.foldable').eq(0).trigger('click')

  clickActive: (event)=>
    $.ajax
      url: @model.collection.url
      type: 'post'
      contentType: 'application/json'
      data: JSON.stringify
        collection_id: @model.id
      success: =>
        helpers.generate_notyfication('success', 'Сохранено')

class ProductCollectionView extends marionette.CollectionView
  childView: ProductCollectionItemView

class LayoutView extends marionette.LayoutView
  className: 'product_collection_layout'
  show_alert_key: 'show_product_collection_alert'
  template: require './templates/layout'
  collection_view_list: []
  events:
    'click .tab_alert>button': 'closeAlert'
    'click .fold_all': 'foldAll'
    'click .unfold_all': 'unfoldAll'
  regions:
    'region_collection_list': '.region_collection_list'

  foldAll: (event) =>
    event.preventDefault()
    for view in @collection_view_list
      $el = view.$('.foldable').eq(0)
      if $el.hasClass('folded')
        $el.trigger 'click'

  unfoldAll: (event) =>
    event.preventDefault()
    for view in @collection_view_list
      $el = view.$('.foldable').eq(0)
      if !$el.hasClass('folded')
        $el.trigger 'click'

  closeAlert: =>
    store.set(@show_alert_key, false)
    @$('.tab_alert').addClass('hide')

  serializeData: =>
    result = super
    if store.get(@show_alert_key) is undefined
      store.set(@show_alert_key, true)
    result.show_alert = store.get(@show_alert_key) is true
    return result

  initialize: =>
    @collection = new ProductCollectionCollection()
    @collection.url = "/api/products/product/#{@model.id}/collection/"

  renderCollection: =>
    if @$('.region_collection_list').length is 0
      return @onRenderCollection()
    rendered = true
    for model in @collection.models
      if !model.get 'rendered'
        $el = null # Элемент, в который будет вставляться вьюхха
        rendered = false
        parent = model.get 'parent'
        if parent
          $el = @$(".branch_#{parent}")
        else
          $el = @$('.region_collection_list')

        if $el and $el.length > 0
          view = new ProductCollectionItemView
            model: model
            collection: @collection
            product_collection: @collection
          view.render()
          $el.append(view.el)
          @collection_view_list.push(view)
          model.set 'rendered', true

    if rendered
      @onRenderCollection()
    else
      @renderCollection()

  onRenderCollection: =>
    for view in @collection_view_list
      if  view.$el.find('.collection_item').length > 0
        $name = view.$el.find('.name').eq(0)
        $name.addClass 'foldable'
        view.addFoldingEvent()

  onRender: =>
    @collection.fetch().then @renderCollection

module.exports = LayoutView

backbone = require 'backbone'

WarehouseModel = require '../models/warehouse'


class WarehouseCollection extends backbone.Collection
  model: WarehouseModel
  url: "/api/warehouses/warehouse/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = WarehouseCollection
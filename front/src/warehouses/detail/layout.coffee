$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'

helpers = require 'helpers'

WarehouseModel = require '../models/warehouse'


class ItemView extends marionette.ItemView
  template: require './templates/item'
  translated_fields:
    name: 'Название'

  events:
    'click .save': 'onClickSave'

  initialize: =>
    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'
      is_active: '[name=is_active]'
      code: '.code_field'

    backbone.Validation.bind @

  onClickSave: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'
  events:
    'click .to_list': 'goToList'
    'click .breadcrumb_list': 'goToList'

  initialize: (options)=>
    @binder = new modelbinder
    @user = new WarehouseModel
      id: options.id

    @item_view = new ItemView
      model: @user

  goToList: (event) =>
    event.preventDefault()
    backbone.history.navigate '/warehouse/', trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'warehouse'
    @region_item.show(@item_view)
    @user.fetch()
    @binder.bind @user, @$el,
      name: '.breadcrumb_name'


module.exports = LayoutView
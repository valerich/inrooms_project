backbone = require 'backbone'


class WarehouseModel extends backbone.Model

  url: =>
    if @id
      "/api/warehouses/warehouse/#{@id}/"
    else
      "/api/warehouses/warehouse/"

  validation:
      name: [
        {
          required: true,
          msg: 'Заполните название'
        }]
      code: [
        {
          required: true,
          msg: 'Заполните код'
        }]

module.exports = WarehouseModel
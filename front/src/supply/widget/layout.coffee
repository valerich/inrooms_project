$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
complex = require 'complex'
marionette = require 'backbone.marionette'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
modelbinder = require 'backbone.modelbinder'


class SupplySummaryModel extends backbone.Model
  url: '/api/supply/supply/summary/'


class LayoutView extends marionette.LayoutView
  show_header: false
  template: require './templates/layout'

  events:
    'click .region_set_price': 'onClickRegionToSetPrice'

  initialize: (options) =>
    @userCanViewSupply = helpers.user_has_permission bus.cache.user, 'can_view_supply_widget'
    @listenTo(bus.vent, 'supply:widget:update', @SupplyWidgetUpdate)
    @model = new SupplySummaryModel
    if options.show_header
      @show_header = options.show_header

    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    if @userCanViewSupply
      @SupplyWidgetUpdate()
      @binder.bind @model, @$el,
        to_set_prices: '.to_set_prices'
      @$('[data-toggle="tooltip"]').tooltip();

  onClickRegionToSetPrice: =>
    backbone.history.navigate 'supply/?to_set_prices=2', trigger: true

  serializeData: =>
    data = super
    data['show_header'] = @show_header
    data['userCanViewSupply'] = @userCanViewSupply
    data

  SupplyWidgetUpdate: =>
    @model.fetch()

module.exports = LayoutView
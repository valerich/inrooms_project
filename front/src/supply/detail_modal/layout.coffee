marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

SupplyModel = require '../models/supply'
ItemsView = require './items/layout'


class SupplyView extends marionette.ItemView
  template: require './templates/supply'


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'order_detail_layout'
  regions:
    region_supply: '.region_supply'
    region_items: '.region_items'

  initialize: (options) =>
    console.log options.id
    @tab_name = options.tab_name
    @binder = new modelbinder
    @model = new SupplyModel(id: options.id)

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>

    @model.fetch().done =>

      supply_view = new SupplyView
        model: @model
      @region_supply.show(supply_view)

      items_view = new ItemsView
        model: @model
      @region_items.show(items_view)

module.exports = ObjectDetailLayout
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

SupplyItemCollection = require '../../collections/supply_item'
SupplyItemModel = require '../../models/supply_item'


class SupplyItemItemView extends marionette.ItemView
  template: require './templates/item'
  className: 'supply_items_item'
  ui:
    'markup': '[name=markup]'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @supply_model = options.supply_model

  onShowMarkup: =>
    price = @model.get('price')
    cost_price = @model.get('cost_price')
    productDetail = @model.get('product_detail')

    # подсчитыаем себестоимость, если возможно
    if not _.isNaN(price) and _.isNumber(price) and price > 0 and cost_price
      calcMarkup = (((price - cost_price) / cost_price) * 100).toFixed(2)
    else
      calcMarkup = 'Неизвестно'

    @ui.markup.val calcMarkup

  onRender: =>
    @setSeriesCount()
    @binder.bind @model, @$el,
      price:
        selector: '[name=price]'
      count_xxs: '[name=count_xxs]'
      count_xs: '[name=count_xs]'
      count_s: '[name=count_s]'
      count_m: '[name=count_m]'
      count_l: '[name=count_l]'
      count_xl: '[name=count_xl]'
      count_xxl: '[name=count_xxl]'
      count_xxxl: '[name=count_xxxl]'
      count_34: '[name=count_34]'
      count_35: '[name=count_35]'
      count_36: '[name=count_36]'
      count_37: '[name=count_37]'
      count_38: '[name=count_38]'
      count_39: '[name=count_39]'
      count_40: '[name=count_40]'
      count_41: '[name=count_41]'
      count_sum: '.count_sum'

    @onShowMarkup()

  setSeriesCount: =>
    series_type = @model.get('series_type')
    if series_type == 's'
      @$("#order_item_#{@model.id} .c_data").addClass('hidden')
      @$("#order_item_#{@model.id} .s_data").removeClass('hidden')
    else if series_type == 'c'
      @$("#order_item_#{@model.id} .s_data").addClass('hidden')
      @$("#order_item_#{@model.id} .c_data").removeClass('hidden')
    warehouse_from_code = @supply_model.get('delivery_warehouse_from_detail')['code']
    product_detail = @model.get('product_detail')
    if product_detail is undefined
      product_detail = {}
    remains = product_detail['remains']
    if remains is undefined
      remains = {}
    warehouse_remains = remains[warehouse_from_code]
    if warehouse_remains is undefined
      warehouse_remains = {}
    count_sum = 0
    for size in [
              'xxs',
              'xs',
              's',
              'm',
              'l',
              'xl',
              'xxl',
              'xxxl',
              '34',
              '35',
              '36',
              '37',
              '38',
              '39',
              '40',
              '41',
    ]
      w_remain = warehouse_remains[size]
      if w_remain is undefined
        w_remain = 0
      @$("#order_item_#{@model.id} .warehouse_from_remains .st_#{size} .warehouse_from_remain").html(w_remain)
      count_sum = count_sum + parseInt(@model.get("count_#{size}"))
    @model.set("count_sum", count_sum)


class SupplyItemCollectionView extends marionette.CollectionView
  childView: SupplyItemItemView
  className: 'order_items_list'

  initialize: (options) =>
    @supply_model = options.supply_model

  childViewOptions: (model, index) =>
    data = {
      supply_model: @supply_model,
      childIndex: index
    }
    return data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  regions:
    region_item_list: '.region_item_list'

  fetchOrderItemCollection: =>
    @supplyitem_collection.fetch
      reset: true
      data: { page_size: 10000}

  initialize: (options) =>
    @supply_model = options.model

  onRender: =>
    @supplyitem_collection = new SupplyItemCollection
      supply_id: @supply_model.id

    @supplyitem_view = new SupplyItemCollectionView
      collection: @supplyitem_collection
      supply_model: @supply_model

    @region_item_list.show(@supplyitem_view)

    @supplyitem_collection.fetch
      reset: true
      data: { page_size: 10000}


module.exports = LayoutView

backbone = require 'backbone'

SupplyItemModel = require '../models/supply_item'


class SupplyItemCollection extends backbone.Collection
  model: SupplyItemModel

  initialize: (options) =>
    @supply_id = options.supply_id

  url: =>
    "/api/supply/supply/#{@supply_id}/items/"

  _prepareModel: (model, options) =>
      model.supply_id = @supply_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = SupplyItemCollection
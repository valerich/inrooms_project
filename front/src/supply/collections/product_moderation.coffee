backbone = require 'backbone'

ProductModel = require '../models/product_moderation'


class ProductCollection extends backbone.Collection
  model: ProductModel

  initialize: (options) =>
    @supply_id = options.supply_id

  url: =>
    "/api/supply/supply/#{@supply_id}/product_moderation/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ProductCollection
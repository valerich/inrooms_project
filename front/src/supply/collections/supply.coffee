backbone = require 'backbone'

SupplyModel = require '../models/supply'


class SupplyCollection extends backbone.Collection
  model: SupplyModel
  url: "/api/supply/supply/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = SupplyCollection
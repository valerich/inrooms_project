backbone = require 'backbone'

LuggageSpaceItemModel = require '../models/luggagespace_item'


class LuggageSpaceItemCollection extends backbone.Collection
  model: LuggageSpaceItemModel

  initialize: (options) =>
    @supply_id = options.supply_id

  url: =>
    "/api/supply/supply/#{@supply_id}/luggage_spaces/"

  _prepareModel: (model, options) =>
      model.supply_id = @supply_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = LuggageSpaceItemCollection

_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'
require '../../models/information'
require 'jquery.inputmask'

SupplyItemCollection = require '../../collections/supply_item'
SupplyItemModel = require '../../models/supply_item'
product_select2_item_template = require '../../../products/templates/select2_item'


class SupplyItemItemView extends marionette.ItemView
  template: require './templates/supplyitem_item'
  className: 'supply_items_item'
  events:
    'click .remove': 'onClickRemove'
    'click .save': 'onClickSave'
  modelEvents:
    'change': 'setSeriesCount'
  ui:
    'markup': '[name=markup]'

  onClickSave: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Модель сохранена')
      bus.vent.trigger 'product_item:update_info'
    .fail (data) ->
      helpers.modelErrorHandler @model, data

  onClickRemove: =>
    bootbox.confirm "Вы действительно хотите удалить модель из поставки?", (result)=>
      if result == true
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')
          bus.vent.trigger 'product_item:update_info'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @supply_model = options.supply_model

    @listenTo @model, 'change:price', @onShowMarkup

  onShowMarkup: =>
    price = @model.get('price')
    cost_price = @model.get('cost_price')
    productDetail = @model.get('product_detail')

    # подсчитыаем себестоимость, если возможно
    if not _.isNaN(price) and _.isNumber(price) and price > 0 and cost_price
      calcMarkup = (((price - cost_price) / cost_price) * 100).toFixed(2)
    else
      calcMarkup = 'Неизвестно'

    @ui.markup.val calcMarkup

  onRender: =>
    @setSeriesCount()
    @binder.bind @model, @$el,
      price:
        selector: '[name=price]'
      count_xxs: '[name=count_xxs]'
      count_xs: '[name=count_xs]'
      count_s: '[name=count_s]'
      count_m: '[name=count_m]'
      count_l: '[name=count_l]'
      count_xl: '[name=count_xl]'
      count_xxl: '[name=count_xxl]'
      count_xxxl: '[name=count_xxxl]'
      count_34: '[name=count_34]'
      count_35: '[name=count_35]'
      count_36: '[name=count_36]'
      count_37: '[name=count_37]'
      count_38: '[name=count_38]'
      count_39: '[name=count_39]'
      count_40: '[name=count_40]'
      count_41: '[name=count_41]'
      count_sum: '.count_sum'
    , changeTriggers: '': 'change keyup'

    @$('[name=price]').inputmask("9{1,20}.9{1,2}")

    @onShowMarkup()



  setSeriesCount: =>
    series_type = @model.get('series_type')
    if series_type == 's'
      @$("#order_item_#{@model.id} .c_data").addClass('hidden')
      @$("#order_item_#{@model.id} .s_data").removeClass('hidden')
    else if series_type == 'c'
      @$("#order_item_#{@model.id} .s_data").addClass('hidden')
      @$("#order_item_#{@model.id} .c_data").removeClass('hidden')
    warehouse_from_code = @supply_model.get('delivery_warehouse_from_detail')['code']
    product_detail = @model.get('product_detail')
    if product_detail is undefined
      product_detail = {}
    remains = product_detail['remains']
    if remains is undefined
      remains = {}
    warehouse_remains = remains[warehouse_from_code]
    if warehouse_remains is undefined
      warehouse_remains = {}
    count_sum = 0
    for size in [
              'xxs',
              'xs',
              's',
              'm',
              'l',
              'xl',
              'xxl',
              'xxxl',
              '34',
              '35',
              '36',
              '37',
              '38',
              '39',
              '40',
              '41',
    ]
      w_remain = warehouse_remains[size]
      if w_remain is undefined
        w_remain = 0
      @$("#order_item_#{@model.id} .warehouse_from_remains .st_#{size} .warehouse_from_remain").html(w_remain)
      count_sum = count_sum + parseInt(@model.get("count_#{size}"))
    @model.set("count_sum", count_sum)

  serializeData: =>
    result = super
    if @supply_model.get('status_detail')['code'] == 'new'
      result['canEditProducts'] = true
    else
      result['canEditProducts'] = false
    if @supply_model.get('status_detail')['code'] == 'set_prices'
      result['canEditPrice'] = true
    else
      result['canEditPrice'] = false
    result


class NewSupplyItemView extends marionette.ItemView
  template: require './templates/new_supplyitem'
  events:
    'click [name=add]': 'onAddBtnClick'

  initialize: (options)=>
    @binder = new modelbinder
    @warehouse_code = options.warehouse_code
    @items_product_ids = options.items_product_ids
    @supply_model = options.supply_model

  onRender: =>
    @binder.bind @model, @$el,
      product: '[name=product]'
    helpers.initAjaxSelect2 @$('[name=product]'),
      url: '/api/products/product/'
      placeholder: 'Введите название товара'
      get_extra_search_params: () =>
        warehouse_remains: @warehouse_code
        exclude_ids: @items_product_ids.join()
        brand: @supply_model.get('delivery_warehouse_to_detail')['brand_detail']['id']
      minimumInputLength: -1
      text_attr: (obj) =>
        "№:#{obj.id}. #{obj.name} / #{obj.color_detail.name}"
      formatResult: (product) =>
        product_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    backbone.Validation.bind @

  onAddBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save()
      .done =>
        helpers.generate_notyfication('success', 'Модель добавлена')
        bus.vent.trigger 'supply:items:changed'
        bus.vent.trigger 'product_item:update_info'
      .fail (data) ->
        helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class SupplyItemCollectionView extends marionette.CollectionView
  childView: SupplyItemItemView
  className: 'supply_items_list'

  initialize: (options) =>
    @supply_model = options.supply_model

  childViewOptions: (model, index) =>
    data = {
      supply_model: @supply_model,
      childIndex: index
    }
    return data


class InformationModel extends backbone.Model

  initialize: (options) =>
    @supply_id = options.supply_id

  url: =>
    "/api/supply/supply/#{@supply_id}/detail_info/"


    
class InformationItemView extends marionette.ItemView
  template: require './templates/information'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_supply': 'onClickSupplySave'

  regions:
    region_item_list: '.region_item_list'
    region_item_add: '.region_item_add'
    information_block: '.information_block'

  onClickSupplySave: =>
    for item in @supplyitem_collection.models
#      if item.changedAttributes()
        item.save().done =>
          helpers.generate_notyfication('success', 'Модель сохранена')
    status_code = @supply_model.get('status_detail')['code']
    if status_code == 'set_prices'
      backbone.history.navigate "/supply/#{@model.id}/product_moderation/", trigger: true
    else
      backbone.history.navigate "/supply/#{@model.id}/delivery/", trigger: true

  serializeData: =>
    result = super

    if @supply_model.get('status_detail')['code'] == 'new'
      result['canEditProducts'] = true
    else
      result['canEditProducts'] = false
    return result

  fetchSupplyItemCollection: =>
    @supplyitem_collection.fetch(
      reset: true,
      data: {
        page_size: 100000
      }
    ).done =>
      @addSupplyItemForm()

  initialize: (options) =>
    @supply_model = options.model
    @listenTo bus.vent, 'product_item:update', @fetchSupplyItemCollection
    @listenTo bus.vent, 'product_item:update_info', @updateInformation

  updateInformation: =>
    # обновление информации о перемещениях
    information_model = new InformationModel
      supply_id: @supply_model.id
    information_model.fetch()
    .done =>
      @information_block.show new InformationItemView model: information_model
    
  addSupplyItemForm: =>
    items_product_ids = []
    for item in @supplyitem_collection.models
      items_product_ids.push item.get('product')

    if @new_supplyitem_view
      @new_supplyitem_view.destroy()

    @new_supplyitem_view = new NewSupplyItemView
      model: new SupplyItemModel
        supply_id: @supply_model.id
      warehouse_code: @supply_model.get('delivery_warehouse_from_detail')['code']
      items_product_ids: items_product_ids
      supply_model: @supply_model

    @region_item_add.show(@new_supplyitem_view)

  onRender: =>
    @supplyitem_collection = new SupplyItemCollection
      supply_id: @supply_model.id

    @supplyitem_view = new SupplyItemCollectionView
      collection: @supplyitem_collection
      supply_model: @supply_model

    @region_item_list.show(@supplyitem_view)

    @listenTo(bus.vent, 'supply:items:changed', @fetchSupplyItemCollection)
    @fetchSupplyItemCollection()
    @updateInformation()


module.exports = LayoutView

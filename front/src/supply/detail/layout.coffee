marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

CommonLayout = require './common/layout'
DeliveryLayout = require './delivery/layout'
ItemsLayout = require './product/layout'
PaymentLayout = require './payment/layout'
ProductModerationLayout = require './product_moderation/layout'

supply_models = require '../models/supply2'


class NavbarView extends marionette.ItemView
  template: require './templates/navbar'
  events:
    'click .tab_link': 'onClickTabLink'
  initialize: (options) =>
    @tab_name = options.tab_name
  onRender: =>
    @$(".tab_link[data-tab_name=#{@tab_name}]").addClass('active')
  onClickTabLink: (event) =>
    event.preventDefault()
    tab_name = $(event.target).parent().data('tab_name')
    @$(".tab_link").removeClass('active')
    @$(".tab_link[data-tab_name=#{tab_name}]").addClass('active')
    @trigger 'click', tab_name
  serializeData: =>
    result = super
    can_supply_moderate_product = helpers.user_has_permission bus.cache.user, 'can_supply_moderate_product'
    result['user'] = bus.cache.user
    result['user']['can_product_moderation_rab_view'] = can_supply_moderate_product
    result


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'supply_detail_layout'
  regions:
    region_tab: '.region_tab'
    region_navbar: '.region_navbar'
  events:
    'click .list_link': 'onClickListLink'

  onClickListLink: (event) =>
    event.preventDefault()
    backbone.history.navigate '/supply/', trigger: true

  onClickTabLink: (event) =>
    event.preventDefault()
    if @modelFetchDone
      tab_name = $(event.target).parent().data('tab_name')
      backbone.history.navigate "/supply/#{@model.id}/#{tab_name}/", trigger: false
      @renderTab(tab_name)

  initialize: (options) =>
    @tab_name = options.tab_name
    @binder = new modelbinder
    @model = supply_models.SupplyModel.findOrCreate(id: options.id)

    @listenTo(bus.vent, 'supply:delivery:luggagespace:change', @changeLuggageSpace)

  changeLuggageSpace: (data)=>
    @model.get('delivery').set('luggagespace_set', data)

  onBeforeDestroy: =>
    @binder.unbind()

  renderTab: (tab_name) =>
    view_class = null
    if tab_name is 'common'
      view_class = CommonLayout
    if tab_name is 'items'
      view_class = ItemsLayout
    if tab_name is 'payment'
      view_class = PaymentLayout
    if tab_name is 'delivery'
      view_class = DeliveryLayout
    if tab_name is 'product_moderation'
      view_class = ProductModerationLayout
    if view_class
      tab_view = new view_class
        model: @model
      @region_tab.show(tab_view)
    else
      @region_tab.empty()

  onRender: =>
    @binder.bind @model, @$el,
      id: '.supply-id'
      status_name: '.supply-status'
      number_1c: '.number_1c'
    @model.fetch().done =>
      @model.set("status_name", @model.get("status_detail")['name'])
      bus.vent.trigger 'menu:active:set', null, 'supply'
      @renderTab(@tab_name)
      navbar_view = new NavbarView
        model: @model
        tab_name: @tab_name
      @region_navbar.show(navbar_view)
      @listenTo navbar_view, 'click', (tab_name) =>
        @tab_name = tab_name
        backbone.history.navigate "/supply/#{@model.id}/#{tab_name}/", trigger: false
        @renderTab(tab_name)

module.exports = ObjectDetailLayout

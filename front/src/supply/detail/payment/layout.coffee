_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
helpers = require 'helpers'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_supply': 'onClickSupplySave'

  translated_fields:
    payment_account: 'Счет'
    payment_method: 'Тип оплаты'
    amount_full: 'Cтоимость'
    amount_paid: 'Оплаченно в валюте'
    amount_paid_rur: 'Оплаченно в рублях'
    description: 'Описание'

  onClickSupplySave: =>
    @model.save  {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
          backbone.history.navigate "/supply/#{model.id}/product_moderation/", trigger: true
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      payment_account: '[name=payment_account]'
      payment_method: '[name=payment_method]'
      amount_full: '[name=amount_full]'
      amount_paid: '[name=amount_paid]'
      amount_paid_rur: '[name=amount_paid_rur]'
      description: '[name=description]'

    helpers.initAjaxSelect2 @$('[name=payment_method]'),
      url: '/api/payments/payment_method/'
      placeholder: 'Введите название типа оплаты'
      allowClear: true
      minimumInputLength: -1
      text_attr: 'name'

    helpers.initAjaxSelect2 @$('[name=payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        currency: 'USD'
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}. #{obj.currency_detail.name} / #{obj.user_detail.name}"

    @$('[name=description]').summernote
      onChange: (description) =>
        @model.set 'description', description

  serializeData: =>
    data = super
    data['can_change_delivery_payment'] = @model.get('status_detail')['code'] == 'sent_to_warehouse'
    data


module.exports = LayoutView

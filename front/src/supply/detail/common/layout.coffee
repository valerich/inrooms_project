_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bootbox = require 'bootbox'
store = require 'store'
helpers = require 'helpers'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .change_status': 'onClickChangeStatus'
    'click .export_xls': 'onClickExportXLSX'

  initialize: =>
    @model.fetch()

  onClickExportXLSX: =>
    event.preventDefault()
    url = "/supply/supply/" + @model.id + "/export/?"
    window.open(url, '_blank')

  changeOrderStatus: (status_code) =>
    $.ajax
      url: "/api/supply/supply/#{@model.id}/change_status/"
      type: 'post'
      data: {
        'code': status_code
      }
      success: =>
        helpers.generate_notyfication('success', 'Статус изменен')
        backbone.history.navigate "/supply/", trigger: true
      error: (data) =>
        helpers.modelErrorHandler @model, data

  onClickChangeStatus: (event) =>
    $target = $(event.target)
    new_status_code = $target.data('status_code')
    if new_status_code == 'delivery_waiting' and !@model.get('prices_assigned')
      message = "Вы должны установить цены товаров"
      bootbox.alert message, =>
          backbone.history.navigate "/supply/#{@model.id}/items/", trigger: true
    else
      if new_status_code == 'sent_to_warehouse' and @model.get('has_non_moderated_products')
        message = "Не все товары прошли модерацию"
        bootbox.confirm message, (result) =>
          if result == true
            $target = $(event.target)
            @changeOrderStatus(new_status_code)
          else
            backbone.history.navigate "/supply/#{@model.id}/items/", trigger: true
      else
        message = "Вы действительно хотите изменить статус заказа?"
        bootbox.confirm message, (result)=>
          if result == true
            $target = $(event.target)
            @changeOrderStatus(new_status_code)

module.exports = LayoutView

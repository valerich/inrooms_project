marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
require "backbone-relational"

supply_models = require '../../models/supply2'
LuggageSpaceView = require './luggagespace/layout'
LuggageSpaceItemCollection = require '../../collections/luggagespace_item'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_supply': 'onClickSupplySave'

  regions:
    luggagespace_region: '.luggagespace_region'
    region_luggagespace_add: '.region_luggagespace_add'

  translated_fields:
    delivery_type: 'Тип доставки'
    delivery_amount_full: 'Стоимость доставки ($)'
    delivery_number: 'Номер поставки'
    delivery_date: 'Дата доставки'

  onClickSupplySave: =>
    @model.save().done =>

    @model.save  {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Поставка сохранена')
          backbone.history.navigate "/supply/#{model.id}/payment/", trigger: true
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
        delivery_type: '[name=delivery_type]'
        delivery_amount_full: '[name=delivery_amount_full]'
        delivery_number: '[name=delivery_number]'
        delivery_date:
          selector: '[name=delivery_date]'
          converter: helpers.null_converter
        _all_weight: '.all_weight'
#        delivery_warehouse_to: '[name=delivery_warehouse_to]'
    helpers.initAjaxSelect2 @$('[name=delivery_type]'),
        url: '/api/supply/delivery_type/'
        placeholder: 'Введите тип доставки'
        allowClear: true
        minimumInputLength: -1
        text_attr: 'name'

#    helpers.initAjaxSelect2 @$('[name=delivery_warehouse_to]'),
#        url: '/api/warehouses/warehouse/'
#        placeholder: 'Выберите склад поставки'
#        minimumInputLength: -1
#        text_attr: 'name'
#        get_extra_search_params: () =>
#          to_supply: 2

    @$('[name=delivery_date]').datepicker
      autoclose: true

    @luggagespace_collection = new LuggageSpaceItemCollection
      supply_id: @model.id
#
    @luggagespace_view = new LuggageSpaceView
      collection: @luggagespace_collection
      model: @model

    @luggagespace_region.show(@luggagespace_view)

  serializeData: =>
    result = super
    result['delivery_warehouse_from_name'] = @model.get('delivery_warehouse_from_detail')['name']
    result['delivery_warehouse_to_name'] = @model.get('delivery_warehouse_to_detail')['name']
    result


module.exports = LayoutView

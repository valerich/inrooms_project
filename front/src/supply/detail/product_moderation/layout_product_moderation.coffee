_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

ProductModerationCollection = require '../../collections/product_moderation'
ImageLayout = require '../../../products/detail/image/layout'


class FilterModel extends complex.FilterModel
  defaults:
    is_active: '3'


class FilterView extends marionette.ItemView
  template: require './templates/product_moderation_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    console.log 'togglePanel'
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      is_active: '[name=is_active]'


class ProductModerationComplexItemView extends marionette.ItemView
  template: require './templates/product_moderation_item'
  className: 'product_moderation_item'

  events:
    'click .image-link': 'onImageLinkClick'
    'click .image-edit': 'onImageEditClick'
    'click .save': 'saveModel'

  initialize: (options)=>
    @binder = new modelbinder
    @supply_model = options.supply_model
    @model.supply_id = @supply_model.id

  fetching: =>
    @model.fetch()
    .done =>
      @.render()

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'
      article: '[name=article]'
      main_collection: "[name=main_collection]"
      is_active: '[name=is_active2]'
      seasonality: '[name=seasonality]'
      color: '[name=color]'

    helpers.initAjaxSelect2 @$("[name=main_collection]"),
      url: '/api/products/collection/'
      placeholder: 'Введите название коллекции'
      minimumInputLength: -1
      text_attr: 'full_name'
      get_extra_search_params: () =>
        kind: @product_kind

    helpers.initAjaxSelect2 @$("[name=color]"),
      url: '/api/products/color/'
      placeholder: 'Введите цвет'
      minimumInputLength: -1
      text_attr: 'name'
      createSearchChoicePosition: 'bottom'
      createSearchChoice: (term) =>
        return {
          id: term + '_4create'
          text: term + ' (создать)'
        }
    @$('[name=color]').on 'change', (event)=>
      url = "/api/products/color/"
      if event.added
        if /_4create/.test(event.added.id.toString())
          $.ajax
            url: url
            type: "post"
            data:
              name: event.added.id.replace('_4create', '')
            success: (data) =>
              @model.set('color', data.id)
              @$('[name=color]').val(data.id).change()
              helpers.generate_notyfication('success', 'Значение создано')

    @$('[name=article]').inputmask("9999/999")
    backbone.Validation.bind @

  onImageLinkClick: =>
    $.magnificPopup.open({
      type:'image',
      gallery: {
        enabled: true
      },
      removalDelay: 300,
      mainClass: 'mfp-fade',
      items: @model.get('images'),
      gallery: {
        enabled: true
      },
    })

  onImageEditClick: =>
    editImageView = new ImageLayout
      model: @model
      button_position: 'bottom'

    bus.vent.trigger 'modal:show', editImageView,
      title: 'Изображения модели'
      modal_size: 'lg'

  saveModel: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Модель сохранена')
    .fail (data) =>
      helpers.modelErrorHandler @model, data


class ProductModerationComplexListView extends complex.ComplexCompositeView
  childView: ProductModerationComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/product_moderation_items'

  initialize: (options) =>
    @listenTo(bus.vent, 'product:images:update', @onProductUpdate)
    @supply_model = options.supply_model

  childViewOptions: (model, index) =>
    data = {
      supply_model: @supply_model,
      childIndex: index
    }
    return data

  onProductUpdate: (model) =>
    item_view = @.children.findByModel(model)
    item_view.fetching()


class ProductModerationComplexView extends complex.ComplexView
  template: require './templates/product_moderation_complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout_product_moderation'

  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @supply_model = options.model
    @collection = new ProductModerationCollection
      supply_id: @supply_model.id
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
    @filter_model = new FilterModel
    @list_view = new ProductModerationComplexListView
      collection: @collection
      supply_model: @supply_model

    @complex_view = new ProductModerationComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'supply_items'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @listenTo(bus.vent, 'product_item:update', @complex_view.doFilter)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()


module.exports = LayoutView

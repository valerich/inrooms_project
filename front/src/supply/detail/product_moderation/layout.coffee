marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

ProductModerationListLayout = require './layout_product_moderation'


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'supply_detail_product_moderation_layout'
  regions:
    region_data: '.region_product_moderations_data'

  initialize: (options) =>
    @tab_name = 'product_moderation'
    @supply_model = options.model

  onRender: =>
    @tab_view = new ProductModerationListLayout
      model: @supply_model
    @region_data.show(@tab_view)

module.exports = ObjectDetailLayout

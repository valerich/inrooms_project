$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'

SupplyCollection = require '../collections/supply'
SupplyModel = require '../models/supply'
SupplyWidgetLayout = require '../widget/layout'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onClickAdd'

  onClickAdd: =>
    addNewSupplyView = new NewSupplyView
      model: new SupplyModel

    bus.vent.trigger 'modal:show', addNewSupplyView,
      title: 'Добавить новую поставку'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      status: '[name=status]'
      product: '[name=product]'
      to_set_prices: '[name=to_set_prices]'
      sort_by: '.sort_list'
      delivery_warehouse_to: '[name=delivery_warehouse_to]'

    helpers.initAjaxSelect2 @$('[name=delivery_warehouse_to]'),
      url: '/api/warehouses/warehouse/'
      placeholder: 'Выберите склад поставки'
      minimumInputLength: -1
      text_attr: 'name'
      get_extra_search_params: () =>
        to_supply: 2

    helpers.initAjaxSelect2 @$('[name=status]'),
      url: "/api/supply/status/"
      placeholder: 'Выберите статус'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true
    helpers.initAjaxSelect2 @$('[name=product]'),
      url: '/api/products/product/'
      placeholder: "Выберите модель"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"


class NewSupplyView extends marionette.ItemView
  template: require './templates/new_supply_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    delivery_warehouse_to: 'На склад'

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      delivery_warehouse_to: '[name=delivery_warehouse_to]'

    helpers.initAjaxSelect2 @$('[name=delivery_warehouse_to]'),
      url: '/api/warehouses/warehouse/'
      placeholder: 'Выберите склад поставки'
      minimumInputLength: -1
      text_attr: 'name'
      get_extra_search_params: () =>
        to_supply: 2

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Поставка создана')
          backbone.history.navigate "/supply/#{model.id}/", {trigger: true}
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class SupplyComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class Supply2ComplexItemView extends complex.ComplexItemView
  template: require './templates/item2'
  triggers:
    'click .panel': 'click'


class SupplyComplexListView extends complex.ComplexCompositeView
  childView: SupplyComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class Supply2ComplexListView extends complex.ComplexCompositeView
  childView: Supply2ComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items2'


class SupplyComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'
    'region_widgets': '.region_widgets'
  events:
    'click .list_type': 'onClickListType'

  initialize: (options) =>
    @listenTo(bus.vent, 'supply:added', @onSupplyAdded)

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    @collection = new SupplyCollection

    @filter_model = new FilterModel(urlParams)
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @filter_view = new FilterView
      model: @filter_model

    @widgets_view = new SupplyWidgetLayout

  onSupplyAdded: =>
    @complex_view.doFilter()

  onClickListType: (event) =>
    $target = $(event.target)
    list_type = $target.data('list_type')
    @renderSupplyList(list_type)

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'supply'
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @region_filter.show(@filter_view)
#    @region_widgets.show(@widgets_view)

    list_type = store.get 'supply_list_type'
    if !list_type
      list_type = 'list1'
    @renderSupplyList(list_type)

    @region_widgets.show(@widgets_view)

  renderSupplyList: (list_type) =>
    store.set 'supply_list_type', list_type
    if list_type is 'list1'
      SupplyView = SupplyComplexListView
    if list_type is 'list2'
      SupplyView = Supply2ComplexListView
    @$(".list_type").removeClass('active')
    @$(".list_type[data-list_type=#{list_type}]").addClass('active')

    list_view = new SupplyView
      collection: @collection

    @complex_view = new SupplyComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'supply'

    @region_complex.show(@complex_view)

    @listenTo @complex_view, 'childview:click', (view, options) =>
      backbone.history.navigate "/supply/#{view.model.id}/", {trigger: true}

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @complex_view.doFilter()

  serializeData: =>
    serialized_data = super
    serialized_data['list_name'] = 'Перемещения'
    serialized_data

module.exports = LayoutView
backbone = require 'backbone'


class SupplyModel extends backbone.Model

  url: =>
    if @id
      "/api/supply/supply/#{@id}/"
    else
      "/api/supply/supply/"

module.exports = SupplyModel
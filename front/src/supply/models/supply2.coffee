_ = require 'underscore'
backbone = require 'backbone'
require "backbone-relational"

SupplyModel = backbone.RelationalModel.extend
    urlRoot: "/api/supply/supply/"
    relations: [
        {
            type: backbone.HasOne,
            key: 'delivery',
            relatedModel: DeliveryModel,
#            reverseRelation: {
#              key: 'order',
#              includeInJSON : false
#            }
        }
    ]


DeliveryModel = backbone.RelationalModel.extend
    relations: [
        {
            type: backbone.HasMany,
            key: 'luggagespace_set',
            relatedModel: LuggageSpaceModel,
            collectionType: LuggageSpaceCollection,
            collectionKey: false,
#            reverseRelation: {
#              key: 'delivery',
#              includeInJSON : false
#            }
        }
    ]
    initialize: =>
        console.log @, 'Delivery Initialized'

LuggageSpaceModel = backbone.RelationalModel.extend
    idAttribute: 'ls_id'
    defaults:
      count: 1
      weight: 0
    validation:
      count: [
        {
          required: true,
          msg: 'Заполните количество'
        }, {
          pattern: 'digits',
          msg: 'Укажите корректное количество'
        }, {
          range: [1, 1000]
          msg: 'Количество не может быть меньше 1 и больше 1000'
        }]
      weight: [
        {
          required: true,
          msg: 'Заполните вес'
        }, {
          range: [1, 99999999]
          msg: 'Вес не может быть меньше 0.01 и больше 99999999.99'
        }
      ]
    initialize: =>
        console.log @, 'Luggagespace Initialized'


LuggageSpaceCollection = backbone.Collection.extend
    model: LuggageSpaceModel


SupplyModel.setup()
DeliveryModel.setup()
LuggageSpaceModel.setup()


module.exports = {
    SupplyModel: SupplyModel,
    DeliveryModel: DeliveryModel,
    LuggageSpaceModel: LuggageSpaceModel,
    LuggageSpaceCollection: LuggageSpaceCollection,
}

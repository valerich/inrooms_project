Backbone = require 'backbone'
bus = require 'bus'


class InformationModel extends Backbone.Model
  urlRoot: '/api/supply/supply/info/'

bus.reqres.setHandler 'supply_information', ->
  promise = Backbone.$.Deferred()
  informationModel = new InformationModel()

  informationModel.fetch
    success: ->
      promise.resolve informationModel
    error: (data) ->
      promise.reject data
  promise


module.exports = InformationModel
_ = require 'underscore'
backbone = require 'backbone'


class LuggageSpaceItemModel extends backbone.Model
  defaults:
    count: 1
    weight: 0
  validation:
    count: [
      {
        required: true,
        msg: 'Заполните количество'
      }, {
        pattern: 'digits',
        msg: 'Укажите корректное количество'
      }, {
        range: [1, 1000]
        msg: 'Количество не может быть меньше 1 и больше 1000'
      }]
    weight: [
      {
        required: true,
        msg: 'Заполните вес'
      }, {
        range: [1, 99999999]
        msg: 'Вес не может быть меньше 0.01 и больше 99999999.99'
      }
    ]

  initialize: (options) =>
    @supply_id = options.supply_id

  url: =>
    if @id
      "/api/supply/supply/#{@supply_id}/luggage_spaces/#{@id}/"
    else
      "/api/supply/supply/#{@supply_id}/luggage_spaces/"

module.exports = LuggageSpaceItemModel

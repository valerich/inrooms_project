backbone = require 'backbone'


class ProductModel extends backbone.Model

  initialize: (options) =>
    @supply_id = options.supply_id

  url: =>
    if @id
      "/api/supply/supply/#{@supply_id}/product_moderation/#{@id}/"
    else
      "/api/supply/supply/#{@supply_id}/product_moderation/"

module.exports = ProductModel
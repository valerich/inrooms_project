_ = require 'underscore'
backbone = require 'backbone'


class SupplyItemModel extends backbone.Model
  defaults:
    quantity: 1
  validation:
    quantity: [
      {
        required: true,
        msg: 'Заполните количество'
      }, {
        pattern: 'digits',
        msg: 'Укажите корректное количество'
      }, {
        range: [1, 1000000000]
        msg: 'Количество не может быть меньше 1 и больше 1 000 000 000'
      }]
    product: [
      {
        required: true,
        msg: 'Заполните модель'
      }
    ]

  initialize: (options) =>
    @supply_id = options.supply_id

  url: =>
    if @id
      "/api/supply/supply/#{@supply_id}/items/#{@id}/"
    else
      "/api/supply/supply/#{@supply_id}/items/"


module.exports = SupplyItemModel

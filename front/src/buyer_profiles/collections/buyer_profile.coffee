backbone = require 'backbone'

BuyerProfileModel = require '../models/buyer_profile'


class BuyerProfileCollection extends backbone.Collection
  model: BuyerProfileModel
  url: "/api/buyers/buyer_profile/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = BuyerProfileCollection
backbone = require 'backbone'

OrderItemModel = require '../models/order_item'


class OrderItemCollection extends backbone.Collection
  model: OrderItemModel
  url: "/api/orders/order_b2c/cheque_1c/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = OrderItemCollection
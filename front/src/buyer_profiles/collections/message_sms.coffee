backbone = require 'backbone'

MessageSMSModel = require '../models/message_sms'


class MessageSMSCollection extends backbone.Collection
  model: MessageSMSModel
  url: "/api/sms/message_sms/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = MessageSMSCollection
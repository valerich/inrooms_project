$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'
complex = require 'complex'
helpers = require 'helpers'
require 'jquery.inputmask'


class ItemView extends marionette.CompositeView
  template: require './templates/item'
  translated_fields:
    first_name: 'Имя'
    middle_name: 'Отчество'
    last_name: 'Фамилия'
    birthday: 'Дата рожения'
    phone: 'Телефон'
    email: 'Email'
    subscription_sms: 'Подписка на новости по смс'
    city: 'Город'
    profession: 'Род деятельности'
    card: 'Карта постоянного клиента'

  events:
    'click .save': 'onClickSave'

  initialize: =>
    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      first_name: '[name=first_name]'
      middle_name: '[name=middle_name]'
      last_name: '[name=last_name]'
      birthday: '[name=birthday]'
      phone: '[name=phone]'
      email: '[name=email]'
      subscription_sms: '[name=subscription_sms]'
      city: '[name=city]'
      profession: '[name=profession]'
      card: '[name=card]'
      gender: '[name=gender]'

    @$('[name=phone]').inputmask("(+7 (999) 999-99-99)|(+\\9\\99.999.999999)")
    @$('[name=birthday]').inputmask("99.99.9999")

    helpers.initAjaxSelect2 @$('[name=card]'),
      url: '/api/buyers/buyer_card/'
      placeholder: 'Введите номер карты'
      allowClear: true
      minimumInputLength: -1
      text_attr: 'number'
      get_extra_search_params: () =>
        has_buyer_profile: '3'

    backbone.Validation.bind @

  onClickSave: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'

  initialize: (options)=>
    @user = options.model

    @item_view = new ItemView
      model: @user

  onRender: =>
    @region_item.show(@item_view)


module.exports = LayoutView
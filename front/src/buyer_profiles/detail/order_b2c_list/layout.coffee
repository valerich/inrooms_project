$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'

OrderItemCollection = require '../../collections/order_item'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @buyer_model = options.buyer_model

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'

  serializeData: =>
    s_data = super
    s_data['order_b2c_amount_sum'] = @buyer_model.get('order_b2c_amount_sum')
    s_data


class OrderComplexItemView extends complex.ComplexItemView
  template: require './templates/item'

  onRender: =>
    @$('[data-toggle="tooltip"]').tooltip();


class OrderComplexListView extends complex.ComplexCompositeView
  childView: OrderComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class OrderComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>

    @buyer_model = options.model

    @collection = new OrderItemCollection

    @filter_model = new FilterModel()
    @filter_model.set('buyer_card', @model.get('card'))
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @filter_view = new FilterView
      model: @filter_model
      buyer_model: @buyer_model

  onRender: =>
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @region_filter.show(@filter_view)

    view = new OrderComplexListView
      collection: @collection

    @complex_view = new OrderComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: view
      name: 'orders'

    @region_complex.show(@complex_view)

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @complex_view.doFilter()

module.exports = LayoutView
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
require 'jquery.inputmask'
bootbox = require 'bootbox'

BuyerProfileCollection = require '../collections/buyer_profile'
BuyerProfileModel = require '../models/buyer_profile'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onAddClick'
    'click .export_btn': 'onClickExport'

  onClickExport: (event) =>
    bootbox.confirm 'Вы действительно хотите выгрузить список профилей в xls?', (result)=>
      if result == true
        url = '/api/buyers/buyer_profile/?as=xls&'
        url = url + $.param(@model.toJSON())
        window.open(url, '_blank')

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onAddClick: =>
    addNewView = new NewBuyerProfileView
      model: new BuyerProfileModel

    bus.vent.trigger 'modal:show', addNewView,
      title: 'Добавить новую анкету клиента'
      modal_size: 'lg'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      card__client: '[name=card__client]'

    helpers.initAjaxSelect2 @$('[name=card__client]'),
      url: "/api/clients/client/"
      placeholder: 'Выберите партнера'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

  serializeData: =>
    @userCanViewClientFilter = helpers.user_has_permission bus.cache.user, 'can_view_all_buyer_profiles'

    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanViewClientFilter'] = @userCanViewClientFilter
    s_data


class BuyerProfileComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class BuyerProfileComplexListView extends complex.ComplexCompositeView
  childView: BuyerProfileComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class BuyerProfileComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class NewBuyerProfileView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    first_name: 'Имя'
    middle_name: 'Отчество'
    last_name: 'Фамилия'
    birthday: 'Дата рожения'
    phone: 'Телефон'
    email: 'Email'
    subscription_sms: 'Подписка на новости по смс'
    city: 'Город'
    profession: 'Род деятельности'
    card: 'Карта постоянного клиента'

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      first_name: '[name=first_name]'
      middle_name: '[name=middle_name]'
      last_name: '[name=last_name]'
      birthday: '[name=birthday]'
      phone: '[name=phone]'
      email: '[name=email]'
      subscription_sms: '[name=subscription_sms]'
      city: '[name=city]'
      profession: '[name=profession]'
      card: '[name=card]'
      gender: '[name=gender]'

    @$('[name=phone]').inputmask("(+7 (999) 999-99-99)|(+\\9\\99.999.999999)")
    @$('[name=birthday]').inputmask("99.99.9999")

    helpers.initAjaxSelect2 @$('[name=card]'),
      url: '/api/buyers/buyer_card/'
      placeholder: 'Введите номер карты'
      allowClear: true
      minimumInputLength: -1
      text_attr: 'number'
      get_extra_search_params: () =>
        has_buyer_profile: '3'

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Создан')
          @destroy()
          backbone.history.navigate "/buyer_profile/#{model.id}/", trigger: true
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @collection = new BuyerProfileCollection
    @filter_model = new FilterModel
    @list_view = new BuyerProfileComplexListView
      collection: @collection

    @complex_view = new BuyerProfileComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'buyer_profiles'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'buyer_profile'
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

    @listenTo @complex_view.list_view, 'childview:click', (view)=>
      backbone.history.navigate "/buyer_profile/#{view.model.id}/", trigger: true

  onAdded: =>
    @complex_view.doFilter()


module.exports = LayoutView
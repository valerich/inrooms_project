$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
require 'jquery.inputmask'
bootbox = require 'bootbox'

BuyerProfileCollection = require '../collections/buyer_profile'
BuyerProfileModel = require '../models/buyer_profile'


class FilterModel extends complex.FilterModel

  validation:
      phone: [
        {
          required: true,
          msg: 'Не заполнено'
        }]


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      phone: '[name=search]'

    @$('[name=search]').inputmask("+7 (999) 999-99-99")

    backbone.Validation.bind @

class BuyerProfileComplexItemView extends complex.ComplexItemView
  template: require './templates/item'

  events:
    'click .send-to-sms': 'sendToSms'

  sendToSms: (event) =>
    bootbox.confirm 'Вы действительно хотите отправить номер карты по смс', (result)=>
      if result == true
        $.ajax
          url: "/api/buyers/buyer_profile/#{@model.id}/send_card_number_to_sms/"
          type: 'post'
          success: =>
            helpers.generate_notyfication('success', 'Смс отправлено')
          error: (data) =>
            helpers.modelErrorHandler @model, data



class BuyerProfileComplexListView extends complex.ComplexCompositeView
  childView: BuyerProfileComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class BuyerProfileComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  showPaginator: false
  d: 2

  onRender: =>
    @region_list.show(@list_view)


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @collection = new BuyerProfileCollection
    @filter_model = new FilterModel
    @list_view = new BuyerProfileComplexListView
      collection: @collection

    @complex_view = new BuyerProfileComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'buyer_profiles'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      v = @filter_model.validate()
      if @filter_model.isValid()
        @complex_view.doFilter()
      else
        error_str = _.values(v).join('<br>')
        helpers.generate_notyfication('error', error_str)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)


module.exports = LayoutView
backbone = require 'backbone'


class BuyerProfileModel extends backbone.Model

  url: =>
    if @id
      "/api/buyers/buyer_profile/#{@id}/"
    else
      "/api/buyers/buyer_profile/"

  validation:
      first_name: [
        {
          required: true,
          msg: 'Не заполнено'
        }]
      last_name: [
        {
          required: true,
          msg: 'Не заполнено'
        }]
      birthday: [
        {
          required: true,
          msg: 'Не заполнено'
        }]
      phone: [
        {
          required: true,
          msg: 'Не заполнено'
        }]
      city: [
        {
          required: true,
          msg: 'Не заполнено'
        }]
      card: [
        {
          required: true,
          msg: 'Не заполнено'
        }]

module.exports = BuyerProfileModel
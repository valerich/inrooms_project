backbone = require 'backbone'

ShopSeasonModel = require '../models/shop_season'


class ShopSeasonCollection extends backbone.Collection
  model: ShopSeasonModel
  url: "/api/reports/shop_season/"


module.exports = ShopSeasonCollection
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'

CommentsWidgetLayout = require '../comments/widget/layout'
OffersWidgetLayout = require '../offers/widget/layout'
#OrdersWidgetLayout = require '../orders/widget/layout'
CurrencyWidgetView = require '../payments/widgets/layout'
SupplyWidgetView = require '../supply/widget/layout'
PurchaseWidgetView = require '../purchases/widget/layout'
OrdersWidgetView = require '../orders/widget/layout'
TasksWidgetView = require '../tasks/widgets/layout'

ClientCollectionView = require './clients'
ClientModel = require '../clients/models/client'
ClientCollection = require '../clients/collections/client'
ClientDashboardView = require './client_dashboard'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  className: "dashboard"

  regions:
#    'region_orders': '.region_orders'
    'region_products': '.region_dashboart_products'
    'region_comments': '.region_comments'
    'region_currency': '.region_currency'
    'region_supply': '.region_dashboart_supply'
    'region_purchase': '.region_dashboart_purchase'
    'region_orders': '.region_dashboart_orders'
    'region_tasks': '.region_tasks'
    'region_client_dashboard': '.region_client_dashboard'
    'region_client_list': '.region_client_list'

  initialize: (options) =>
    @user = bus.cache.user
    @can_all_shop_season_report_view = helpers.user_has_permission bus.cache.user, 'can_all_shop_season_report_view'
    @can_self_shop_season_report_view = helpers.user_has_permission bus.cache.user, 'can_self_shop_season_report_view'

    @offers_view = new OffersWidgetLayout
      show_header: true
    @comments_view = new CommentsWidgetLayout()
    @currency_view = new CurrencyWidgetView()
    @supply_view = new SupplyWidgetView
      show_header: true
    @purchase_view = new PurchaseWidgetView
      show_header: true
    @orders_view = new OrdersWidgetView()
    @tasks_view = new TasksWidgetView()

    if @can_all_shop_season_report_view
      @client_collection = new ClientCollection
      @client_view = new ClientCollectionView
        collection: @client_collection

  onRender: =>
    bus.vent.trigger 'menu:active:set', 'dashboard', 'dashboard'
    @region_products.show(@offers_view)
    @region_comments.show(@comments_view)
    @region_currency.show(@currency_view)
    @region_supply.show(@supply_view)
    @region_purchase.show(@purchase_view)
    @region_orders.show(@orders_view)
    @region_tasks.show(@tasks_view)

    if @can_all_shop_season_report_view
      @client_collection.fetch(
        data: {
          is_active: 2,
          kind: 1,
          has_store: 2,
        }
      ).done =>
        blank_client_model = new ClientModel
          id: 0
          name: 'Все партнеры'
        @client_collection.add(blank_client_model)
        @region_client_list.show(@client_view)
        @listenTo @client_view, 'childview:click', (view, options) =>
          client_model_id = view.model.id
          if client_model_id
            is_consolidated = false
          else
            is_consolidated = true
          dashboard_view = new ClientDashboardView
            client_id: client_model_id
            is_consolidated: is_consolidated
          @region_client_dashboard.show(dashboard_view)
    else
      if @can_self_shop_season_report_view
        dashboard_view = new ClientDashboardView
          is_consolidated: false
        @region_client_dashboard.show(dashboard_view)

    @listenTo(bus.vent, 'reload_chart', @reloadChart)

  reloadChart: (client, year, season) =>
    client_model_id = client
    if client_model_id
      is_consolidated = false
    else
      is_consolidated = true
    dashboard_view = new ClientDashboardView
      client_id: client_model_id
      year: year
      season: season
      is_consolidated: is_consolidated
    @region_client_dashboard.show(dashboard_view)


  serializeData: =>
    s_data = super
    s_data['user'] = @user
    s_data['hello_text'] = @getHelloText()
    s_data

  getHelloText: =>
    currentdate = new Date()
    hour = currentdate.getHours()
    text = '🌙&nbsp; Доброй ночи'
    if hour >= 6 and hour < 12
      text = '⏰ &nbsp; Доброе утро'
    if hour >= 12 and hour < 17
      text = '🌞&nbsp; Добрый день'
    if hour >= 17 and hour < 22
      text = '🌖&nbsp; Добрый вечер'
    return text + ', ' + bus.cache.user.first_name




module.exports = LayoutView
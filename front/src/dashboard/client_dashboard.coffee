$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
Highcharts = require('highcharts')

ClientDashboardCollection = require './collections/shop_season'


class ShopSeasonItemView extends marionette.ItemView
  template: require './templates/shop_season_item'
  className: 'shop_season_item'
  events:
    'click .next_report': 'onClickNextReport'
    'click .prev_report': 'onClickPrevReport'

  onRender: =>
#    setTimeout(@showChart, 1000)
    @showChart()

  onClickNextReport: =>
    year = @model.get('year')
    if @model.get('season') == 1
      season = 2
    else
      season = 1
      year = year + 1
    bus.vent.trigger 'reload_chart', @model.get('client'), year, season

  onClickPrevReport: =>
    year = @model.get('year')
    if @model.get('season') == 1
      season = 2
      year = year - 1
    else
      season = 1
    bus.vent.trigger 'reload_chart', @model.get('client'), year, season

  showChart: =>
    $.ajax
      url: "/api/reports/revenue/"
      type: 'get'
      data: {
        'date_from': @model.get('date_from')
        'date_to': @model.get('date_to')
        'client': @model.get('client')
      }
      success: (response) =>
        $chart_1_selector = @$(".chart1")
        revenues = []
        orders_sums = []
        for i in response
          revenues.push([new Date(i['date']).getTime(), i['revenue']])
          orders_sums.push([new Date(i['date']).getTime(), i['orders']])
        chart1 = new Highcharts.chart
            chart:
                renderTo: $chart_1_selector[0]
                height: 400
                zoomType: 'x'
            title:
                text: 'Выручка'
            xAxis:
                type: 'datetime'
            yAxis: [
              {
                title: {
                  text: 'Выручка руб.'
                }
              },
              {
                title: {
                  text: 'Поставка руб.'
                }
                opposite: true,
              }
            ]
            tooltip:
                shared: true
                crosshair: true
                valueSuffix: ' руб'
#            plotOptions:
#                spline:
#                    marker:
#                        enabled: false
            credits:
                enabled: false
            series: [
              {
                  type: 'column',
                  name: 'Поставка',
                  data: orders_sums,
                  yAxis: 1,
              },
              {
                  type: 'spline',
                  name: 'Выручка',
                  data: revenues,
              },
            ]


class ShopSeasonCollectionView extends marionette.CollectionView
  childView: ShopSeasonItemView


class ClientDashboardView extends marionette.LayoutView
  template: require './templates/client_dashboard'
  className: "client_dashboard"

  regions:
    'region_shop_season': '.region_shop_season'

  initialize: (options) =>
    @client_id = options.client_id
    @is_consolidated = options.is_consolidated
    if not @client_id
      if !@is_consolidated
        @client_id = bus.cache.user.client['id']
    @year = options.year
    @season = options.season
    @shop_season_collection = new ClientDashboardCollection
    @shop_season_view = new ShopSeasonCollectionView
       collection: @shop_season_collection

  onRender: =>
    console.log @shop_season_collection
    fetch_data = {
      year: @year
      season: @season
    }
    if @client_id
      fetch_data['client'] = @client_id
    if @is_consolidated
      fetch_data['is_consolidated'] = 1
    @shop_season_collection.fetch(
      data: fetch_data
    ).done =>
      @region_shop_season.show(@shop_season_view)

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    s_data['hello_text'] = @getHelloText()
    s_data

  getHelloText: =>
    currentdate = new Date()
    hour = currentdate.getHours()
    text = '🌙&nbsp; Доброй ночи'
    if hour >= 6 and hour < 12
      text = '⏰ &nbsp; Доброе утро'
    if hour >= 12 and hour < 17
      text = '🌞&nbsp; Добрый день'
    if hour >= 17 and hour < 22
      text = '🌖&nbsp; Добрый вечер'
    return text + ', ' + bus.cache.user.name


module.exports = ClientDashboardView
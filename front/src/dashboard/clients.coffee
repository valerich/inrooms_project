$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'


class ClientItemView extends marionette.ItemView
  template: require './templates/client_item'
  className: 'pull-left'
  triggers:
    'click button': 'click'


class ClientCollectionView extends marionette.CollectionView
  childView: ClientItemView

  onRender: =>
    @listenTo @, 'childview:click', (view, options) =>
      $('.client_item').removeClass('btn-dark')
      $('.client_item').addClass('btn-primary')
      $(".client_item_#{view.model.id}").addClass('btn-dark')
      $(".client_item_#{view.model.id}").removeClass('btn-primary')


module.exports = ClientCollectionView
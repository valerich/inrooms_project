$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
bootbox = require 'bootbox'
require 'select2'
require 'backbone-validation'
require 'blueimp-file-upload'

MusicCollection = require '../collections/music'
MusicModel = require '../models/music'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .add': 'onAddClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  onAddClick: =>
    @$('.fileupload').fileupload
      url: "/api/multimedia/music/upload/"
      dataType: 'json'
      submit: (e, data) =>
        $('#music_add_btn').removeClass('fa-plus-circle')
        $('.add').removeClass('btn-primary')
        $('.add').addClass('btn-default')
        $('#music_add_btn').disabled = true
        $('#music_add_btn').addClass('fa-spinner')
      always: (e, data) =>
        $('#music_add_btn').addClass('fa-plus-circle')
        $('.add').addClass('btn-primary')
        $('.add').removeClass('btn-default')
        $('#music_add_btn').disabled = false
        $('#music_add_btn').removeClass('fa-spinner')
      done: (e, response) =>
        @model.trigger 'change'
      fail: (e, response) =>
        helpers.generate_notyfication('error', 'не поддерживаемый тип файла')


  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class MusicComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click .link': 'click'

  events:
    'click .delete': 'onClickDelete'

  onClickDelete: =>
    bootbox.confirm "Вы действительно хотите удалить трек?", (result)=>
      if result == true
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')
          bus.vent.trigger 'music:delete'


class MusicComplexListView extends complex.ComplexCompositeView
  childView: MusicComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class MusicComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @listenTo(bus.vent, 'music:added', @onAdded)
    @collection = new MusicCollection
    @filter_model = new FilterModel
    @list_view = new MusicComplexListView
      collection: @collection

    @complex_view = new MusicComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'music'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

    @listenTo(bus.vent, 'music:delete', @complex_view.doFilter)

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'music'
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

    @listenTo @complex_view.list_view, 'childview:click', (view)=>
      win = window.open(view.model.get('file'), '_blank')
      win.focus()

  onAdded: =>
    @complex_view.doFilter()


module.exports = LayoutView
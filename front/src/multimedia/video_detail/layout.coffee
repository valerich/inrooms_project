$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'

helpers = require 'helpers'

VideoModel = require '../models/video'


class ItemView extends marionette.ItemView
  template: require './templates/item'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'
  events:
    'click .to_list': 'goToList'
    'click .breadcrumb_list': 'goToList'

  initialize: (options)=>
    @binder = new modelbinder
    @video_model = new VideoModel
      id: options.id

    @item_view = new ItemView
      model: @video_model

  goToList: (event) =>
    event.preventDefault()
    backbone.history.navigate '/video/', trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'video'
    @video_model.fetch().done =>
      @region_item.show(@item_view)
    @binder.bind @video_model, @$el,
      name: '.breadcrumb_name'


module.exports = LayoutView
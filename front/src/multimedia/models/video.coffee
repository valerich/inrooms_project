backbone = require 'backbone'


class VideoModel extends backbone.Model

  url: =>
    if @id
      "/api/multimedia/video/#{@id}/"
    else
      "/api/multimedia/video/"

module.exports = VideoModel
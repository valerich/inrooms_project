backbone = require 'backbone'


class MusicModel extends backbone.Model

  url: =>
    if @id
      "/api/multimedia/music/#{@id}/"
    else
      "/api/multimedia/music/"

module.exports = MusicModel
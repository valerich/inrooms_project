backbone = require 'backbone'

VideoModel = require '../models/video'


class VideoCollection extends backbone.Collection
  model: VideoModel

  url: =>
    "/api/multimedia/video/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = VideoCollection
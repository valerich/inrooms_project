backbone = require 'backbone'

MusicModel = require '../models/music'


class MusicCollection extends backbone.Collection
  model: MusicModel

  url: =>
    "/api/multimedia/music/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = MusicCollection
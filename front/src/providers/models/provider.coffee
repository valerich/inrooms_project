backbone = require 'backbone'


class ProviderModel extends backbone.Model
  defaults:
    kind: 2

  url: =>
    if @id
      "/api/clients/client/#{@id}/"
    else
      "/api/clients/client/"

  validation:
      inn: [
        {
          required: true,
          msg: 'Заполните ИНН'
        }]
      name: [
        {
          required: true,
          msg: 'Заполните название'
        }]

module.exports = ProviderModel
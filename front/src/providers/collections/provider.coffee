backbone = require 'backbone'

ProviderModel = require '../models/provider'


class ProviderCollection extends backbone.Collection
  model: ProviderModel
  url: "/api/clients/client/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ProviderCollection
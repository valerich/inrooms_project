backbone = require 'backbone'

ProviderFileModel = require '../models/provider_file'


class ProviderFileCollection extends backbone.Collection
  model: ProviderFileModel

  initialize: (options) =>
    @provider_id = options.provider_id

  url: =>
    "/api/clients/client/#{@provider_id}/files/"

#  parse: (response) =>
#    @count = response.count
#    return response.results

module.exports = ProviderFileCollection
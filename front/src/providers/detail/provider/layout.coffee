$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'
complex = require 'complex'
helpers = require 'helpers'


class ItemView extends marionette.CompositeView
  template: require './templates/item'
  translated_fields:
    legal_form: 'Организационно-правовая форма'
    name: 'Название'
    phone: 'Номер телефона фабрики'
    manager: 'Менеджер'
    contact_person: 'Контактное лицо'
    email: 'Email'
    inn: 'ИНН'
    kpp: 'КПП'
    legal_index: 'Юридический адрес "Индекс"'
    legal_region: 'Юридический адрес "Регион"'
    legal_district: 'Юридический адрес "Район"'
    legal_city: 'Юридический адрес "Город"'
    legal_street: 'Юридический адрес "Улица"'
    legal_house: 'Юридический адрес "Дом"'
    legal_building: 'Юридический адрес "Корпус"'
    legal_flat: 'Юридический адрес "Квартира"'
    legal_construction: 'Юридический адрес "Строение"'
    actual_index: 'Фактический адрес "Индекс"'
    actual_region: 'Фактический адрес "Регион"'
    actual_district: 'Фактический адрес "Район"'
    actual_city: 'Фактический адрес "Город"'
    actual_street: 'Фактический адрес "Улица"'
    actual_house: 'Фактический адрес "Дом"'
    actual_building: 'Фактический адрес "Корпус"'
    actual_flat: 'Фактический адрес "Квартира"'
    actual_construction: 'Фактический адрес "Строение"'
    reason: 'Основание'
    account_current: 'Расчетный счет'
    account_correspondent: 'Корреспондентский счет'
    bank_name: 'Наименование банка'
    bik: 'БИК'
    contract_number: "Номер договора"
    contract_date: "Дата договора"
    head: "Руководитель"
    accountant_general: "Главный бухгалтер"
    service: 'Услуга'

  events:
    'click .save': 'onClickSave'

  initialize: =>
    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      legal_form: "[name=legal_form]"
      name: "[name=name]"
      inn: "[name=inn]"
      kpp: "[name=kpp]"
      legal_index: "[name=legal_index]"
      legal_region: "[name=legal_region]"
      legal_district: "[name=legal_district]"
      legal_city: "[name=legal_city]"
      legal_street: "[name=legal_street]"
      legal_house: "[name=legal_house]"
      legal_building: "[name=legal_building]"
      legal_flat: "[name=legal_flat]"
      legal_construction: "[name=legal_construction]"
      actual_index: "[name=actual_index]"
      actual_region: "[name=actual_region]"
      actual_district: "[name=actual_district]"
      actual_city: "[name=actual_city]"
      actual_street: "[name=actual_street]"
      actual_house: "[name=actual_house]"
      actual_building: "[name=actual_building]"
      actual_flat: "[name=actual_flat]"
      actual_construction: "[name=actual_construction]"
      manager: "[name=manager]"
      contact_person: "[name=contact_person]"
      phone: "[name=phone]"
      email: "[name=email]"
      account_current: "[name=account_current]"
      account_correspondent: "[name=account_correspondent]"
      bank_name: "[name=bank_name]"
      bik: "[name=bik]"
      contract_number: "[name=contract_number]"
      contract_date: "[name=contract_date]"
      head: "[name=head]"
      accountant_general: "[name=accountant_general]"
      service: '[name=service]'

    @$('[name=contract_date]').datepicker
      autoclose: true

    backbone.Validation.bind @

  onClickSave: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'

  initialize: (options)=>
    @provider = options.model

    @item_view = new ItemView
      model: @provider

  onRender: =>
    @region_item.show(@item_view)


module.exports = LayoutView
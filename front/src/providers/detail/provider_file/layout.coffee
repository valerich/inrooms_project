$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'
complex = require 'complex'
helpers = require 'helpers'
require 'blueimp-file-upload'
bootbox = require 'bootbox'

ProviderFileCollection = require '../../collections/provider_file'


class ProviderFileComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/file_item'

  events:
    'click .delete': 'onClickDelete'

  initialize: (options) =>
    @provider_model = options.provider_model

  onClickDelete: =>
    bootbox.confirm "Вы действительно хотите удалить файл?", (result)=>
      if result == true
        $.ajax
          url: "/api/clients/client/file_delete/"
          data:
            "file_id": @model.id
          type: 'post'
          success: (data) =>
            helpers.generate_notyfication('success', 'Удалено')
            bus.vent.trigger 'file:delete'
          error: (data) =>
            helpers.modelErrorHandler @model, data


class ProviderFileComplexListView extends complex.ComplexCompositeView
  childView: ProviderFileComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/file_items'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.files'

  events:
    'click .add': 'onAddClick'

  initialize: (options) =>
    @provider_model = options.model
    @collection = new ProviderFileCollection
      provider_id: @provider_model.id
    @list_view = new ProviderFileComplexListView
      collection: @collection

  onRender: =>
    @region_complex.show(@list_view)
    @collection.fetch()

    @listenTo(bus.vent, 'file:delete', @onDeleteFile)

  onDeleteFile: =>
    @collection.fetch()

  onAddClick: =>
    @$('.fileupload').fileupload
      url: "/api/clients/client/#{@model.id}/file_upload/"
      dataType: 'json'
      submit: (e, data) =>
        $('#music_add_btn').removeClass('fa-plus-circle')
        $('.add').removeClass('btn-primary')
        $('.add').addClass('btn-default')
        $('#music_add_btn').disabled = true
        $('#music_add_btn').addClass('fa-spinner')
      always: (e, data) =>
        $('#music_add_btn').addClass('fa-plus-circle')
        $('.add').addClass('btn-primary')
        $('.add').removeClass('btn-default')
        $('#music_add_btn').disabled = false
        $('#music_add_btn').removeClass('fa-spinner')
      done: (e, response) =>
        @collection.fetch()
      fail: (e, response) =>
        helpers.generate_notyfication('error', 'не поддерживаемый тип файла')


module.exports = LayoutView
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
helpers = require 'helpers'


class ConsistencyLayout extends marionette.LayoutView
  getTemplate: =>
    if @options.isModal
      return require './templates/modal_layout'
    else
      return require './templates/layout'

  initialize: =>
    @binder = new modelbinder
    
  onRender: =>
    @binder.bind @model, @$el

  serializeData: =>
    data = super
    data.itemsCount = [1..5]
    data

  onBeforeDestroy: =>
    @binder.unbind()

module.exports = ConsistencyLayout

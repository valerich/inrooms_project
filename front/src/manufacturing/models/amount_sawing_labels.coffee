_ = require 'underscore'
backbone = require 'backbone'


class setAmountSawingModel extends backbone.Model
  defaults:
    amount_sawing_labels: '0.00'
  validation:
    amount_sawing_labels: [
      {
        required: true,
        msg: 'Заполните сумму'
      }]


module.exports = setAmountSawingModel

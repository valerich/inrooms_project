backbone = require 'backbone'


class ManufacturingOrderModel extends backbone.Model

  url: =>
    if @id
      "/api/manufacturing/manufacturing_order/#{@id}/"
    else
      "/api/manufacturing/manufacturing_order/"

module.exports = ManufacturingOrderModel
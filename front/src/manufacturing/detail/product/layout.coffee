_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

OrderItemCollection = require '../../collections/manufacturing_order_item'
OrderItemModel = require '../../models/manufacturing_order_item'
product_select2_item_template = require '../../../products/templates/select2_item'
NewProductModel = require '../../../products/models/product'
ImageLayout = require './image/layout'
OfferView = require '../../../products/list/offer_modal_view'
OfferModel = require '../../../offers/models/offer'


class OrderItemItemView extends marionette.ItemView
  template: require './templates/orderitem_item'
  className: 'order_items_item'
  events:
    'click .remove': 'onClickRemove'
    'click .save': 'onClickSave'
    'click .image-link': 'onImageLinkClick'
    'click .image-edit': 'onImageEditClick'
    'click .offer_detail': 'onClickOfferDetail'
  modelEvents:
    'change': 'setSeriesCount'

  onClickSave: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Модель сохранена')
    .fail (data) ->
      helpers.modelErrorHandler @model, data

  onClickOfferDetail: =>
    OfferDataView = new OfferView
      model: new OfferModel
        id: @model.get('product_detail')['offer']

    bus.vent.trigger 'modal:show', OfferDataView,
      title: "Образец №:#{@model.get('product_detail')['offer']}"

  onClickRemove: =>
    bootbox.confirm "Вы действительно хотите удалить модель из корзины?", (result)=>
      if result == true
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')

  onImageLinkClick: =>
    $.magnificPopup.open({
      type:'image',
      gallery: {
        enabled: true
      },
      removalDelay: 300,
      mainClass: 'mfp-fade',
      items: @model.get('product_detail').images,
      gallery: {
        enabled: true
      },
    })

  onImageEditClick: =>
    model = new NewProductModel
      id: @model.get('product_detail').id
    editImageView = new ImageLayout
      model: model
      button_position: 'bottom'

    bus.vent.trigger 'modal:show', editImageView,
      title: 'Изображения модели'
      modal_size: 'lg'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @order_model = options.order_model

  onRender: =>
    @setSeriesCount()
    @binder.bind @model, @$el,
      quantity: '[name=quantity]'
      price: '[name=price]'
      series_type: '[name=series_type]'
      has_xxs: '[name=has_xxs]'
      has_xs: '[name=has_xs]'
      has_s: '[name=has_s]'
      has_m: '[name=has_m]'
      has_l: '[name=has_l]'
      has_xl: '[name=has_xl]'
      has_xxl: '[name=has_xxl]'
      has_xxxl: '[name=has_xxxl]'
      has_34: '[name=has_34]'
      has_35: '[name=has_35]'
      has_36: '[name=has_36]'
      has_37: '[name=has_37]'
      has_38: '[name=has_38]'
      has_39: '[name=has_39]'
      has_40: '[name=has_40]'
      has_41: '[name=has_41]'
      received_xxs_count: '[name=received_xxs_count]'
      received_xs_count: '[name=received_xs_count]'
      received_s_count: '[name=received_s_count]'
      received_m_count: '[name=received_m_count]'
      received_l_count: '[name=received_l_count]'
      received_xl_count: '[name=received_xl_count]'
      received_xxl_count: '[name=received_xxl_count]'
      received_xxxl_count: '[name=received_xxxl_count]'
      received_34_count: '[name=received_34_count]'
      received_35_count: '[name=received_35_count]'
      received_36_count: '[name=received_36_count]'
      received_37_count: '[name=received_37_count]'
      received_38_count: '[name=received_38_count]'
      received_39_count: '[name=received_39_count]'
      received_40_count: '[name=received_40_count]'
      received_41_count: '[name=received_41_count]'
      ordered_xxs_count: '.st_xxs .ordered_st_count'
      ordered_xs_count: '.st_xs .ordered_st_count'
      ordered_s_count: '.st_s .ordered_st_count'
      ordered_m_count: '.st_m .ordered_st_count'
      ordered_l_count: '.st_l .ordered_st_count'
      ordered_xl_count: '.st_xl .ordered_st_count'
      ordered_xxl_count: '.st_xxl .ordered_st_count'
      ordered_xxxl_count: '.st_xxxl .ordered_st_count'
      ordered_34_count: '.st_34 .ordered_st_count'
      ordered_35_count: '.st_35 .ordered_st_count'
      ordered_36_count: '.st_36 .ordered_st_count'
      ordered_37_count: '.st_37 .ordered_st_count'
      ordered_38_count: '.st_38 .ordered_st_count'
      ordered_39_count: '.st_39 .ordered_st_count'
      ordered_40_count: '.st_40 .ordered_st_count'
      ordered_41_count: '.st_41 .ordered_st_count'
      ordered_st_sum: '.ordered_st_sum'
      received_st_sum: '.received_st_sum'
      label_main: '[name=label_main]'
      label_main_xxs_count: '[name=label_main_xxs_count]'
      label_main_xs_count: '[name=label_main_xs_count]'
      label_main_s_count: '[name=label_main_s_count]'
      label_main_m_count: '[name=label_main_m_count]'
      label_main_l_count: '[name=label_main_l_count]'
      label_main_xl_count: '[name=label_main_xl_count]'
      label_main_xxl_count: '[name=label_main_xxl_count]'
      label_main_xxxl_count: '[name=label_main_xxxl_count]'
      label_internal: '[name=label_internal]'
      label_internal_count: '[name=label_internal_count]'
      label_internal2: '[name=label_internal2]'
      label_internal2_count: '[name=label_internal2_count]'
      label_cardboard: '[name=label_cardboard]'
      label_cardboard_count: '[name=label_cardboard_count]'
      has_offer: '[name=has_offer]'

    helpers.initAjaxSelect2 @$('[name=label_main]'),
      url: '/api/products/product_label/'
      placeholder: 'Введите название этикетки'
      minimumInputLength: -1
      get_extra_search_params: () =>
        kind: 1
      text_attr: (obj) =>
        "№:#{obj.id}. #{obj.name}"
      formatResult: (obj) =>
        "№:#{obj.id}. #{obj.name}"
      escapeMarkup: (markup) =>
        markup

    helpers.initAjaxSelect2 @$('[name=label_internal]'),
      url: '/api/products/product_label/'
      placeholder: 'Введите название этикетки'
      minimumInputLength: -1
      get_extra_search_params: () =>
        kind: 2
      text_attr: (obj) =>
        "№:#{obj.id}. #{obj.name}"
      formatResult: (obj) =>
        "№:#{obj.id}. #{obj.name}"
      escapeMarkup: (markup) =>
        markup

    helpers.initAjaxSelect2 @$('[name=label_internal2]'),
      url: '/api/products/product_label/'
      placeholder: 'Введите название этикетки'
      minimumInputLength: -1
      get_extra_search_params: () =>
        kind: 2
      text_attr: (obj) =>
        "№:#{obj.id}. #{obj.name}"
      formatResult: (obj) =>
        "№:#{obj.id}. #{obj.name}"
      escapeMarkup: (markup) =>
        markup

    helpers.initAjaxSelect2 @$('[name=label_cardboard]'),
      url: '/api/products/product_label/'
      placeholder: 'Введите название этикетки'
      minimumInputLength: -1
      get_extra_search_params: () =>
        kind: 3
      text_attr: (obj) =>
        "№:#{obj.id}. #{obj.name}"
      formatResult: (obj) =>
        "№:#{obj.id}. #{obj.name}"
      escapeMarkup: (markup) =>
        markup

  setSeriesCount: =>
    series_type = @model.get('series_type')
    if series_type == 's'
      @$("#order_item_#{@model.id} .c_data").addClass('hidden')
      @$("#order_item_#{@model.id} .s_data").removeClass('hidden')
    else if series_type == 'c'
      @$("#order_item_#{@model.id} .s_data").addClass('hidden')
      @$("#order_item_#{@model.id} .c_data").removeClass('hidden')
    @$("#order_item_#{@model.id} .c_data, #order_item_#{@model.id} .s_data").removeClass('selected_series_type')
    ordered_st_sum = 0
    received_st_sum = 0
    for size in [
              'xxs',
              'xs',
              's',
              'm',
              'l',
              'xl',
              'xxl',
              'xxxl',
              '34',
              '35',
              '36',
              '37',
              '38',
              '39',
              '40',
              '41',
    ]
      has_size = @model.get("has_#{size}")

      if has_size
        if size in ['s', 'm', '37', '38']
          one_count = 2
        else
          one_count = 1
        @$("#order_item_#{@model.id} .st_#{size}").addClass('selected_series_type')
      else
        one_count = 0
      @model.set("ordered_#{size}_count", @model.get('quantity') * one_count)
      ordered_st_sum = ordered_st_sum + @model.get("ordered_#{size}_count")
      count_received_item = parseInt(@model.get("received_#{size}_count"))
      if count_received_item
        received_st_sum = received_st_sum + count_received_item
    @model.set("ordered_st_sum", ordered_st_sum)
    @model.set("received_st_sum", received_st_sum)

  serializeData: =>
    result = super
    if @order_model.get('status_detail')['code'] == 'new'
      result['canEditProducts'] = true
    else
      result['canEditProducts'] = false
    if @order_model.get('status_detail')['code'] == 'sent_to_factory'
      result['canEditReceived'] = true
    else
      result['canEditReceived'] = false
    if @order_model.get('status_detail')['code'] == 'sewing_labels'
      result['canEditLabels'] = true
    else
      result['canEditLabels'] = false
    result['is_sawing_labels'] = @order_model.get('is_sawing_labels')
    result


class NewProductView extends marionette.ItemView
  template: require '../../../products/templates/new_product_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    name: 'Название'
    article: 'Артикул'
    main_collection: 'Категория'
    color: 'Цвет'

  initialize: (options) =>
    @binder = new modelbinder
    @order_item_model = options.order_item_model

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'
      article: '[name=article]'
      main_collection: '[name=main_collection]'
      color: '[name=color]'

    helpers.initAjaxSelect2 @$("[name=main_collection]"),
      url: '/api/products/collection/'
      placeholder: 'Введите название категории'
      minimumInputLength: -1
      text_attr: 'full_name'

    helpers.initAjaxSelect2 @$("[name=color]"),
      url: '/api/products/color/'
      placeholder: 'Введите цвет'
      minimumInputLength: -1
      text_attr: 'name'
      createSearchChoicePosition: 'bottom'
      createSearchChoice: (term) =>
        return {
          id: term + '_4create'
          text: term + ' (создать)'
        }
    @$('[name=color]').on 'change', (event)=>
      url = "/api/products/color/"
      if event.added
        if /_4create/.test(event.added.id.toString())
          $.ajax
            url: url
            type: "post"
            data:
              name: event.added.id.replace('_4create', '')
            success: (data) =>
              @model.set('color', data.id)
              @$('[name=color]').val(data.id).change()
              helpers.generate_notyfication('success', 'Значение создано')

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Модель создана')
          @order_item_model.set('product', @model.id)
          @order_item_model.save {},
            success: (model) =>
              helpers.generate_notyfication('success', 'Модель добавлена')
              bus.vent.trigger 'order:items:changed'
              @destroy()
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()


class NewOrderItemView extends marionette.ItemView
  template: require './templates/new_orderitem'
  events:
    'click [name=add]': 'onAddBtnClick'
    'click [name=create_product]': 'onCreateProductClick'

  initialize: (options)=>
    @binder = new modelbinder
    @order_kind = options.order_kind

  onRender: =>
    if @order_kind == 'o'
      has_offer = 2
    else
      has_offer = 3
    @binder.bind @model, @$el,
      product: '[name=product]'
    helpers.initAjaxSelect2 @$('[name=product]'),
      url: '/api/products/product/'
      placeholder: 'Введите название товара'
      get_extra_search_params: () =>
        has_offer: has_offer
      minimumInputLength: -1
      text_attr: (obj) =>
        "№:#{obj.id}. #{obj.name} / #{obj.color_detail.name}"
      formatResult: (product) =>
        product_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    backbone.Validation.bind @

  onAddBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save()
      .done =>
        helpers.generate_notyfication('success', 'Модель добавлена')
        bus.vent.trigger 'order:items:changed'
      .fail (data) ->
        helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onCreateProductClick: (event) =>
    addNewProductView = new NewProductView
      model: new NewProductModel
      order_item_model: @model

    bus.vent.trigger 'modal:show', addNewProductView,
      title: 'Добавить новую модель'

  @onBeforeDestroy: =>
    @binder.unbind()

  serializeData: =>
    result = super
    result.order_kind = @order_kind
    return result


class OrderItemCollectionView extends marionette.CollectionView
  childView: OrderItemItemView
  className: 'order_items_list'

  initialize: (options) =>
    @order_model = options.order_model

  childViewOptions: (model, index) =>
    data = {
      order_model: @order_model,
      childIndex: index
    }
    return data



class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_order': 'onClickOrderSave'

  regions:
    region_item_list: '.region_item_list'
    region_item_add: '.region_item_add'

  onClickOrderSave: =>
    for item in @orderitem_collection.models
      if item.changedAttributes()
        item.save().done =>
          helpers.generate_notyfication('success', 'Модель сохранена')
    backbone.history.navigate "/manufacturing_order/#{@model.id}/payment/", trigger: true

  serializeData: =>
    result = super

    if @order_model.get('status_detail')['code'] == 'new'
      result['canEditProducts'] = true
    else
      result['canEditProducts'] = false
    if @order_model.get('status_detail')['code'] == 'sent_to_factory'
      result['canEditReceived'] = true
    else
      result['canEditReceived'] = false
    return result

  fetchOrderItemCollection: =>
    @orderitem_collection.fetch
      reset: true
      data: { page_size: 10000}

  initialize: (options) =>
    @order_model = options.model
    @listenTo(bus.vent, 'product_item:update', @fetchOrderItemCollection)

  addOrderItemForm: =>
    if @new_orderitem_view
      @new_orderitem_view.destroy()
    @new_orderitem_view = new NewOrderItemView
      model: new OrderItemModel
        order_id: @order_model.id
        order: @order_model.id
      order_kind: @order_model.get('kind')
    @region_item_add.show(@new_orderitem_view)

  onRender: =>
    @orderitem_collection = new OrderItemCollection
      order_id: @order_model.id

    @orderitem_view = new OrderItemCollectionView
      collection: @orderitem_collection
      order_model: @order_model

    @region_item_list.show(@orderitem_view)

    @listenTo(bus.vent, 'order:items:changed', @fetchOrderItemCollection)
    @listenTo(bus.vent, 'order:items:changed', @addOrderItemForm)
    @fetchOrderItemCollection()
    @addOrderItemForm()


module.exports = LayoutView

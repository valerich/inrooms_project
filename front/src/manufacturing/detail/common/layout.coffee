_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bootbox = require 'bootbox'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'

setAmountSawingModel = require '../../models/amount_sawing_labels'


class SetAmountSawingLabelView extends marionette.ItemView
  template: require './templates/set_amount_sawing_labels_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'

  initialize: (options) =>
    @manufacturing_order = options.manufacturing_order
    @status_code = options.status_code
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      'amount_sawing_labels': '[name=amount_sawing_labels]'

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()

      $.ajax
        url: "/api/manufacturing/manufacturing_order/#{@manufacturing_order.id}/set_amount_sawing_labels/"
        type: 'post'
        data: {
          'amount_sawing_labels': @model.get('amount_sawing_labels')
        }
        success: =>
          $.ajax
            url: "/api/manufacturing/manufacturing_order/#{@manufacturing_order.id}/change_status/"
            type: 'post'
            data: {
              'code': @status_code
            }
            success: =>
              helpers.generate_notyfication('success', 'Статус изменен')
              backbone.history.navigate "/manufacturing_order/", trigger: true
              @destroy()
            error: (data) =>
              helpers.modelErrorHandler @manufacturing_order, data

        error: (data) =>
          helpers.modelErrorHandler @manufacturing_order, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .change_status': 'onClickChangeStatus'
    'click .change_internal_label_status': 'onClickChangeInternalLabelStatus'

  initialize: =>
    @model.fetch()

  changeOrderInternalLabelStatus: (status_code) =>
    $.ajax
      url: "/api/manufacturing/manufacturing_order/#{@model.id}/change_internal_label_status/"
      type: 'post'
      data: {
        'code': status_code
      }
      success: =>
        helpers.generate_notyfication('success', 'Статус изменен')
        backbone.history.loadUrl()
      error: (data) =>
        helpers.modelErrorHandler @model, data

  onClickChangeInternalLabelStatus: (event) =>
    $target = $(event.target)
    new_status_code = $target.data('status_code')
    message = "Вы действительно хотите изменить статус заказа внутренних этикеток?"
    bootbox.confirm message, (result)=>
      if result == true
        $target = $(event.target)
        @changeOrderInternalLabelStatus(new_status_code)

  changeOrderStatus: (status_code) =>
    $.ajax
      url: "/api/manufacturing/manufacturing_order/#{@model.id}/change_status/"
      type: 'post'
      data: {
        'code': status_code
      }
      success: =>
        helpers.generate_notyfication('success', 'Статус изменен')
        backbone.history.loadUrl()
      error: (data) =>
        helpers.modelErrorHandler @model, data

  onClickChangeStatus: (event) =>
    $target = $(event.target)
    new_status_code = $target.data('status_code')
    received_failed = false
    if @model.get('status_detail')['code'] == 'sewing_labels'
      setAmountSawingLabel = new SetAmountSawingLabelView
        model: new setAmountSawingModel
        manufacturing_order: @model
        status_code: new_status_code

      bus.vent.trigger 'modal:show', setAmountSawingLabel,
        title: 'Оплата за пошив этикеток'
    else
      if new_status_code == 'received' and !@model.get('is_good_received')
        if !@model.get('has_products_received')
          message = "Вы не получили ни одного товара"
          bootbox.alert message, =>
            backbone.history.navigate "/manufacturing_order/#{@model.id}/items/", trigger: true
          return
        else
          received_failed = true
          message = "Количество заказанного товара и полученного отличается. Изменить статус заказа?"
      else
        message = "Вы действительно хотите изменить статус заказа?"
      bootbox.confirm message, (result)=>
        if result == true
          $target = $(event.target)
          @changeOrderStatus(new_status_code)
        if result == false
          if received_failed == true
            backbone.history.navigate "/manufacturing_order/#{@model.id}/items/", trigger: true


module.exports = LayoutView

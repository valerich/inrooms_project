_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

OrderItemCollection = require '../../collections/manufacturing_order_item'


class LabelPrintView extends marionette.ItemView
  template: require './templates/label_print'
  events:
    'click .print': 'onClickPrint'
    'click .pdf': 'onClickPdf'

  onClickPrint: (event) =>
    event.preventDefault()
    url = "/manufacturing/manufacturing_order/" + @model.id + "/export_internal_labels/?as=html"
    window.open(url, '_blank')

  onClickPdf: (event) =>
    event.preventDefault()
    url = "/manufacturing/manufacturing_order/" + @model.id + "/export_internal_labels/?"
    window.open(url, '_blank')


class OrderItemItemView extends marionette.ItemView
  template: require './templates/orderitem_item'
  className: 'order_items_item'
  events:
    'click .save': 'onClickSave'

  onClickSave: =>
    @model.save({'internal_label_price': @model.get('internal_label_price')}, {patch: true})
    .done =>
      helpers.generate_notyfication('success', 'Модель сохранена')
    .fail (data) ->
      helpers.modelErrorHandler @model, data

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @order_model = options.order_model

  onRender: =>
    @binder.bind @model, @$el,
      internal_label_price: '[name=internal_label_price]'


class OrderItemCollectionView extends marionette.CollectionView
  childView: OrderItemItemView
  className: 'order_items_list'

  initialize: (options) =>
    @order_model = options.order_model

  childViewOptions: (model, index) =>
    data = {
      order_model: @order_model,
      childIndex: index
    }
    return data



class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_order': 'onClickOrderSave'

  regions:
    region_item_list: '.region_item_list'
    region_label_print: '.region_label_print'

  onClickOrderSave: =>
    for item in @orderitem_collection.models
      if item.changedAttributes()
        item.save().done =>
          helpers.generate_notyfication('success', 'Модель сохранена')
    backbone.history.navigate "/manufacturing_order/#{@model.id}/payment/", trigger: true

  fetchOrderItemCollection: =>
    @orderitem_collection.fetch
      reset: true
      data: { page_size: 10000}

  initialize: (options) =>
    @order_model = options.model
    @listenTo(bus.vent, 'product_item:update', @fetchOrderItemCollection)

  onRender: =>
    @orderitem_collection = new OrderItemCollection
      order_id: @order_model.id

    @orderitem_view = new OrderItemCollectionView
      collection: @orderitem_collection
      order_model: @order_model

    @region_item_list.show(@orderitem_view)

    @print_view = new LabelPrintView
       model: @order_model

    @region_label_print.show(@print_view)

    @listenTo(bus.vent, 'order:items:changed', @fetchOrderItemCollection)
    @fetchOrderItemCollection()


module.exports = LayoutView

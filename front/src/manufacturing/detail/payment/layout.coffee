_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
helpers = require 'helpers'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_order': 'onClickOrderSave'

  onClickOrderSave: =>
    @model.save().done =>
      helpers.generate_notyfication('success', 'Сохранено')
      backbone.history.navigate "/manufacturing_order/#{@model.id}/common/", trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      payment_method: '[name=payment_method]'
      payment_account: '[name=payment_account]'
      internal_label_payment_account: '[name=internal_label_payment_account]'
      ordered_amount: '[name=ordered_amount]'
      amount_received: '[name=amount_received]'
      amount_full: '[name=amount_full]'
      amount_paid: '[name=amount_paid]'
      description: '[name=description]'
      factory: '[name=factory]'
      kind_display: '.kind_display_field'
      transport_costs: '[name=transport_costs]'
      prepayment: '[name=prepayment]'
      left_to_pay: '[name=left_to_pay]'

    helpers.initAjaxSelect2 @$('[name=internal_label_payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        currency: 'CNY'
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}. #{obj.currency_detail.name} / #{obj.user_detail.name}"

    helpers.initAjaxSelect2 @$('[name=payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        currency: 'CNY'
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}. #{obj.currency_detail.name} / #{obj.user_detail.name}"

    helpers.initAjaxSelect2 @$('[name=factory]'),
      url: '/api/factories/factory/'
      placeholder: 'Введите фабрики'
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"

    @$('[name=description]').summernote
      onChange: (description) =>
        @model.set 'description', description

  serializeData: =>
    result = super
    if @model.get('status_detail')['code'] == 'sent_to_factory'
      result['canEditAmountReceived'] = true
    else
      result['canEditAmountReceived'] = false
    if @model.get('status_detail')['code'] == 'new'
      result['canEditPrepayment'] = true
    else
      result['canEditPrepayment'] = false
    result

module.exports = LayoutView

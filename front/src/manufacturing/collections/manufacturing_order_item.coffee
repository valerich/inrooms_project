backbone = require 'backbone'

ManufacturingOrderItemModel = require '../models/manufacturing_order_item'


class ManufacturingOrderItemCollection extends backbone.Collection
  model: ManufacturingOrderItemModel

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    "/api/manufacturing/manufacturing_order/#{@order_id}/items/"

  _prepareModel: (model, options) =>
      model.order_id = @order_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ManufacturingOrderItemCollection
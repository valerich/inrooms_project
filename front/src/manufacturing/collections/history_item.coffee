backbone = require 'backbone'

HistoryItemModel = require '../models/history_item'


class HistoryItemCollection extends backbone.Collection
  model: HistoryItemModel

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    "/api/history/history_item/manufacturing/manufacturingorder/#{@order_id}/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = HistoryItemCollection
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

ManufacturingOrderModel = require '../models/manufacturing_order'
ItemsView = require './items/layout'


class OrderView extends marionette.ItemView
  template: require './templates/order'


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'order_detail_layout'
  regions:
    region_order: '.region_order'
    region_items: '.region_items'

  initialize: (options) =>
    console.log options.id
    @tab_name = options.tab_name
    @binder = new modelbinder
    @model = new ManufacturingOrderModel(id: options.id)

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>

    @model.fetch().done =>

      order_view = new OrderView
        model: @model
      @region_order.show(order_view)

      items_view = new ItemsView
        model: @model
      @region_items.show(items_view)

module.exports = ObjectDetailLayout
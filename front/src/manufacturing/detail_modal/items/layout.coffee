_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

OrderItemCollection = require '../../collections/manufacturing_order_item'
OrderItemModel = require '../../models/manufacturing_order_item'


class OrderItemItemView extends marionette.ItemView
  template: require './templates/orderitem_item'
  className: 'order_items_item'
  modelEvents:
    'change': 'setSeriesCount'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @order_model = options.order_model

  onRender: =>
    @setSeriesCount()
    @binder.bind @model, @$el,
      quantity: '[name=quantity]'
      price: '[name=price]'
      has_xxs: '[name=has_xxs]'
      has_xs: '[name=has_xs]'
      has_s: '[name=has_s]'
      has_m: '[name=has_m]'
      has_l: '[name=has_l]'
      has_xl: '[name=has_xl]'
      has_xxl: '[name=has_xxl]'
      has_xxxl: '[name=has_xxxl]'
      has_34: '[name=has_34]'
      has_35: '[name=has_35]'
      has_36: '[name=has_36]'
      has_37: '[name=has_37]'
      has_38: '[name=has_38]'
      has_39: '[name=has_39]'
      has_40: '[name=has_40]'
      has_41: '[name=has_41]'
      received_xxs_count: '[name=received_xxs_count]'
      received_xs_count: '[name=received_xs_count]'
      received_s_count: '[name=received_s_count]'
      received_m_count: '[name=received_m_count]'
      received_l_count: '[name=received_l_count]'
      received_xl_count: '[name=received_xl_count]'
      received_xxl_count: '[name=received_xxl_count]'
      received_xxxl_count: '[name=received_xxxl_count]'
      received_34_count: '[name=received_34_count]'
      received_35_count: '[name=received_35_count]'
      received_36_count: '[name=received_36_count]'
      received_37_count: '[name=received_37_count]'
      received_38_count: '[name=received_38_count]'
      received_39_count: '[name=received_39_count]'
      received_40_count: '[name=received_40_count]'
      received_41_count: '[name=received_41_count]'
      ordered_xxs_count: '.st_xxs .ordered_st_count'
      ordered_xs_count: '.st_xs .ordered_st_count'
      ordered_s_count: '.st_s .ordered_st_count'
      ordered_m_count: '.st_m .ordered_st_count'
      ordered_l_count: '.st_l .ordered_st_count'
      ordered_xl_count: '.st_xl .ordered_st_count'
      ordered_xxl_count: '.st_xxl .ordered_st_count'
      ordered_xxxl_count: '.st_xxxl .ordered_st_count'
      ordered_34_count: '.st_34 .ordered_st_count'
      ordered_35_count: '.st_35 .ordered_st_count'
      ordered_36_count: '.st_36 .ordered_st_count'
      ordered_37_count: '.st_37 .ordered_st_count'
      ordered_38_count: '.st_38 .ordered_st_count'
      ordered_39_count: '.st_39 .ordered_st_count'
      ordered_40_count: '.st_40 .ordered_st_count'
      ordered_41_count: '.st_41 .ordered_st_count'
      ordered_st_sum: '.ordered_st_sum'
      received_st_sum: '.received_st_sum'

  setSeriesCount: =>
    series_type = @model.get('series_type')
    if series_type == 's'
      @$("#order_item_#{@model.id} .c_data").addClass('hidden')
      @$("#order_item_#{@model.id} .s_data").removeClass('hidden')
    else if series_type == 'c'
      @$("#order_item_#{@model.id} .s_data").addClass('hidden')
      @$("#order_item_#{@model.id} .c_data").removeClass('hidden')
    @$("#order_item_#{@model.id} .c_data, #order_item_#{@model.id} .s_data").removeClass('selected_series_type')
    ordered_st_sum = 0
    received_st_sum = 0
    for size in [
              'xxs',
              'xs',
              's',
              'm',
              'l',
              'xl',
              'xxl',
              'xxxl',
              '34',
              '35',
              '36',
              '37',
              '38',
              '39',
              '40',
              '41',
    ]
      has_size = @model.get("has_#{size}")

      if has_size
        if size in ['s', 'm', '37', '38']
          one_count = 2
        else
          one_count = 1
        @$("#order_item_#{@model.id} .st_#{size}").addClass('selected_series_type')
      else
        one_count = 0
      @model.set("ordered_#{size}_count", @model.get('quantity') * one_count)
      ordered_st_sum = ordered_st_sum + @model.get("ordered_#{size}_count")
      count_received_item = parseInt(@model.get("received_#{size}_count"))
      if count_received_item
        received_st_sum = received_st_sum + count_received_item
    @model.set("ordered_st_sum", ordered_st_sum)
    @model.set("received_st_sum", received_st_sum)


class OrderItemCollectionView extends marionette.CollectionView
  childView: OrderItemItemView
  className: 'order_items_list'

  initialize: (options) =>
    @order_model = options.order_model

  childViewOptions: (model, index) =>
    data = {
      order_model: @order_model,
      childIndex: index
    }
    return data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  regions:
    region_item_list: '.region_item_list'

  fetchOrderItemCollection: =>
    @orderitem_collection.fetch
      reset: true
      data: { page_size: 10000}

  initialize: (options) =>
    @order_model = options.model

  onRender: =>
    @orderitem_collection = new OrderItemCollection
      order_id: @order_model.id

    @orderitem_view = new OrderItemCollectionView
      collection: @orderitem_collection
      order_model: @order_model

    @region_item_list.show(@orderitem_view)

    @orderitem_collection.fetch
      reset: true
      data: { page_size: 10000}


module.exports = LayoutView

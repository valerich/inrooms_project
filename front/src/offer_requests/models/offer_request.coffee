backbone = require 'backbone'


class OfferRequestModel extends backbone.Model

  url: =>
    if @id
      "/api/offer_requests/offer_request/#{@id}/"
    else
      "/api/offer_requests/offer_request/"

module.exports = OfferRequestModel
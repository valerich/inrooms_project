_ = require 'underscore'
backbone = require 'backbone'


class OfferRequestItemModel extends backbone.Model

  initialize: (options) =>
    @offer_request_id = options.offer_request_id

  url: =>
    if @id
      "/api/offer_requests/offer_request/#{@offer_request_id}/items/#{@id}/"
    else
      "/api/offer_requests/offer_request/#{@offer_request_id}/items/"


module.exports = OfferRequestItemModel

backbone = require 'backbone'

OfferRequestItemModel = require '../models/offer_request_item'


class OfferRequestItemCollection extends backbone.Collection
  model: OfferRequestItemModel

  initialize: (options) =>
    @offer_request_id = options.offer_request_id

  url: =>
    "/api/offer_requests/offer_request/#{@offer_request_id}/items/"

  _prepareModel: (model, options) =>
      model.offer_request_id = @offer_request_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = OfferRequestItemCollection
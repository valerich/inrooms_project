backbone = require 'backbone'

OfferRequestModel = require '../models/offer_request'


class OfferRequestCollection extends backbone.Collection
  model: OfferRequestModel
  url: "/api/offer_requests/offer_request/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = OfferRequestCollection
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

OfferRequestCollection = require '../collections/offer_request'
OfferRequestModel = require '../models/offer_request'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .processing': 'onProcessingClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onProcessingClick: =>
    backbone.history.navigate "/offer-requests/processing/", trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      status: '[name=status]'
      client: '[name=client]'

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: "Выберите клиента"
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

  serializeData: =>
    @userCanAllOfferRequestsView = helpers.user_has_permission bus.cache.user, 'can_all_offer_requests_view'
    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanAllOfferRequestsView'] = @userCanAllOfferRequestsView
    s_data


class OfferRequestComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class OfferRequestComplexListView extends complex.ComplexCompositeView
  childView: OfferRequestComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class OfferRequestComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @listenTo(bus.vent, 'client:added', @onAdded)
    @collection = new OfferRequestCollection
    @filter_model = new FilterModel
    @list_view = new OfferRequestComplexListView
      collection: @collection

    @complex_view = new OfferRequestComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'offer_requests'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'offer_requests'
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

    @listenTo @complex_view.list_view, 'childview:click', (view)=>
      backbone.history.navigate "/offer-requests/#{view.model.id}/", trigger: true

  onAdded: =>
    @complex_view.doFilter()


module.exports = LayoutView
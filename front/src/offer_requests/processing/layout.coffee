backbone = require 'backbone'
marionette = require 'backbone.marionette'
bus = require 'bus'
modelbinder = require 'backbone.modelbinder'
helpers = require 'helpers'

require 'jquery.inputmask'


class ProcessingModel extends backbone.Model
  url: =>
    "/api/offer_requests/processing/"


class ProcessingView extends marionette.ItemView
  template: require './templates/transfer_rur_cny'
  events:
    'click .processing': 'onClickProcessing'

  onClickProcessing: =>
    data = {
      'items': [],
      'offer_requests': []
    }
    @$('.processing_item_count').each (index, element) =>
        id = $(element).data("offer_id")
        color_id = $(element).data("color_id")
        count = $(element).val()
        data['items'].push({
          'offer_id': id,
          'color_id': color_id,
          'count': count
        })
    @$('.offer_requests_ids').each (index, element) =>
        id = $(element).data("offer_request_id")
        data['offer_requests'].push(id)
    $.ajax
        url: "/api/offer_requests/processing/"
        type: 'POST'
        data: JSON.stringify(data)
        dataType: 'json'
        contentType: "application/json; charset=utf-8"
#        success: =>
#          helpers.generate_notyfication('success', 'Сформировано')
#          backbone.history.navigate "/offer-requests/", trigger: true
#        error: (data) =>
#          helpers.modelErrorHandler @model, data
    helpers.generate_notyfication('success', 'Сформировано')
    backbone.history.navigate "/offer-requests/", trigger: true

#  serializeData: =>
#    s_data = super
#    s_data['items'] = @model
#    s_data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_processing': '.processing_region'

  events:
    'click .to_list': 'goToList'

  initialize: =>
    @processing_model = new ProcessingModel()
    @processing_view = new ProcessingView
      model: @processing_model

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'offer_requests'
    @processing_model.fetch().done =>
      @region_processing.show(@processing_view)

  goToList: (event) =>
    event.preventDefault()
    backbone.history.navigate '/offer-requests/', trigger: true


module.exports = LayoutView
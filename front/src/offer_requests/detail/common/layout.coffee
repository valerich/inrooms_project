_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bootbox = require 'bootbox'
store = require 'store'
helpers = require 'helpers'
bus = require 'bus'


class ExportView extends marionette.ItemView
  template: require './templates/export_modal'
  events:
    'click [name=export]': 'onExportBtnClick'
  translated_fields:
    with_size_quantity: 'Включая количество размеров'
    delivery_year: 'Год поставки товара'
    delivery_date: 'Дата поставки товара'
    document_number: 'Номер документа'
    with_date: 'Включая дату'
    season_name: 'Сезон'
    season_year: 'Год сезона'

  initialize: (options)=>
    @model = new backbone.Model
      with_size_quantity: false
      with_number: true
      document_number: ''
      with_date: true
      season_year: new Date().getFullYear()
      delivery_year: new Date().getFullYear()
    @order_id = options.order_id
    @format = options.format
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      'delivery_year': '[name=delivery_year]'
      'delivery_date': '[name=delivery_date]'
      'with_size_quantity': '[name=with_size_quantity]'
      'document_number': '[name=document_number]'
      'season_name': '[name=season_name]'
      'season_year': '[name=season_year]'
      'with_date': '[name=with_date]'

    backbone.Validation.bind @

  onExportBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      url = "/offer_requests/offer_request/" + @order_id + "/export/?"
      if @model.get("with_size_quantity")
        url = url + "&with_size_quantity=" + @model.get("with_size_quantity")
      if @model.get("delivery_year")
        url = url + "&delivery_year=" + @model.get("delivery_year")
      if @model.get("delivery_date")
        url = url + "&delivery_date=" + @model.get("delivery_date")
      if @model.get("document_number")
        url = url + "&document_number=" + @model.get("document_number")
      if @model.get("with_date")
        url = url + "&with_date=" + @model.get("with_date")
      if @model.get("season_name")
        url = url + "&season_name=" + @model.get("season_name")
      if @model.get("season_year")
        url = url + "&season_year=" + @model.get("season_year")
      url = url + "&as=" + @format
      window.open(url, '_blank')
      @destroy()
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()

  serializeData: =>
    @userCanExportOrderPdfWithSizeQuantity = helpers.user_has_permission bus.cache.user, 'can_export_order_pdf_with_size_quantity'
    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanExportOrderPdfWithSizeQuantity'] = @userCanExportOrderPdfWithSizeQuantity
    s_data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .print_order': 'onClickPrintOrder'
    'click .pdf_order': 'onClickPDFOrder'
    'click .delete': 'onClickDelete'
    'click .change_status': 'onClickChangeStatus'

  initialize: (options) =>
    @model = options.model

  onClickDelete: (event) =>
    bootbox.confirm "Вы действительно хотите удалить предзаказ?", (result)=>
      if result == true
        @model.destroy()
        .done =>
          helpers.generate_notyfication('success', 'Удалено')
          backbone.history.navigate '/offer-requests/', trigger: true
        .fail (fdata) ->
          helpers.modelErrorHandler @model, fdata

  onClickChangeStatus: (event) =>
    target = $(event.target)
    new_status_id = target.data('newstatus_id')
    @model.set('status', new_status_id)
    @model.save().done =>
      helpers.generate_notyfication('success', 'Статус изменен')
      backbone.history.navigate '/offer-requests/', trigger: true

  onClickPrintOrder: (status_code) =>
    addExportView = new ExportView(
      order_id: @model.id
      format: 'html'
    )
    bus.vent.trigger 'modal:show', addExportView,
      title: 'Распечатать'

  onClickPDFOrder: (status_code) =>
    addExportView = new ExportView(
      order_id: @model.id
      format: 'pdf'
    )
    bus.vent.trigger 'modal:show', addExportView,
      title: 'Скачать в PDF'

  serializeData: =>
    @userCanClose = helpers.user_has_permission bus.cache.user, 'can_close_offer_requests'
    @userCanChangeStatus = helpers.user_has_permission bus.cache.user, 'can_change_status_offer_requests'

    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanClose'] = @userCanClose
    s_data['user']['userCanChangeStatus'] = @userCanChangeStatus
    s_data

module.exports = LayoutView

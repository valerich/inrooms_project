_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
helpers = require 'helpers'
bus = require 'bus'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
      'click .save_order': 'onClickOrderSave'

  onClickOrderSave: =>
    @model.save().done =>
      helpers.generate_notyfication('success', 'Сохранено')
      backbone.history.navigate "/offer-requests/#{@model.id}/items/", trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @order_model = options.model
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      client: '[name=client]'

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: 'Введите клиента'
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

  serializeData: =>
    result = super
    result['canEdit'] = helpers.user_has_permission bus.cache.user, 'can_all_offer_requests_view'
    result


module.exports = LayoutView

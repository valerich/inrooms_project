_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

OfferRequestItemCollection = require '../../collections/offer_request_item'
OfferRequestItemModel = require '../../models/offer_request'


class OfferRequestItemItemView extends marionette.ItemView
  template: require './templates/offer_request_item_item'
  className: 'offer_request_item'
  events:
    'click .save': 'onClickSave'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @offer_request = options.offer_request

  onRender: =>
    @binder.bind @model, @$el,
      series_count: '[name=series_count]'
      amount: '.amount'
    @setSeriesCount()

  onClickSave: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Сохранено')
      bus.vent.trigger 'offer_requests:items:changed'
      @setSeriesCount()
    .fail (fdata) ->
      helpers.modelErrorHandler @model, fdata

  setSeriesCount: =>
    main_collection = @model.get('offer_detail')['main_collection']
    if main_collection
      @$(".series_type_#{main_collection['series_type']}").removeClass('hidden')
    else
      @$(".series_type_c,.series_type_s").removeClass('hidden')


    ordered_sizes_detail = @model.get('ordered_sizes_detail')

    count_sum = 0

    for size in [
              'xxs',
              'xs',
              's',
              'm',
              'l',
              'xl',
              'xxl',
              'xxxl',
              '34',
              '35',
              '36',
              '37',
              '38',
              '39',
              '40',
              '41',
    ]
      d = ordered_sizes_detail[size]
      if d is undefined
        count = 0
      else
        count = d['ordered']

      @$(".ordered_sizes .st_#{size} .ordered_size").html(count)
      count_sum += count

    @$(".count_sum").html(count_sum)

  serializeData: =>
    canEdit = false
    if @offer_request.get('status') == 1 or @offer_request.get('status') == 3
      canEdit = true
    result = super
    result['canEdit'] = canEdit
    result


class OfferRequestItemCollectionView extends marionette.CollectionView
  childView: OfferRequestItemItemView
  className: 'offer_request_item_list'

  initialize: (options) =>
    @offer_request = options.offer_request

  childViewOptions: (model, index) =>
    child_data = {
      offer_request: @offer_request,
      childIndex: index
    }
    return child_data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .tab_alert>button': 'closeAlert'
    'click .save_cart': 'onClickCartSave'

  regions:
    region_item_list: '.region_item_list'
    region_item_add: '.region_item_add'

  initialize: (options) =>
    @offer_request = options.model

  onClickCartSave: =>
    backbone.history.navigate "/offer-requests/#{@offer_request.id}/common/", trigger: true

  fetchOfferRequestItemCollection: =>
    @offer_request_item_collection.fetch(
      reset: true
      data: {'page_size': 1000}
    ).done =>
      @offer_request_item_view = new OfferRequestItemCollectionView
        collection: @offer_request_item_collection
        offer_request: @offer_request

      @region_item_list.show(@offer_request_item_view)

  onRender: =>
    @offer_request_item_collection = new OfferRequestItemCollection
      offer_request_id: @offer_request.id

    @fetchOfferRequestItemCollection()
    @setQuantityAmountFull()
    @listenTo(bus.vent, 'offer_requests:items:changed', @onItemsChanged)

  onItemsChanged: =>
    @offer_request.fetch().done =>
      @setQuantityAmountFull()

  setQuantityAmountFull: =>
    @$('.offer_request_quantity').html(@offer_request.get('quantity'))
    @$('.offer_request_amount_full').html(@offer_request.get('amount_full'))


module.exports = LayoutView

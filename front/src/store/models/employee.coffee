backbone = require 'backbone'


class EmployeeModel extends backbone.Model

  url: =>
    if @id
      "/api/stores/employee/#{@id}/"
    else
      "/api/stores/employee/"

module.exports = EmployeeModel
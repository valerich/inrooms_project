backbone = require 'backbone'


class StoreModel extends backbone.Model

  url: =>
    if @id
      "/api/stores/store/#{@id}/"
    else
      "/api/stores/store/"

  validation:
      name: [
        {
          required: true,
          msg: 'Заполните название'
        }]

module.exports = StoreModel
$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'
complex = require 'complex'
helpers = require 'helpers'
require 'blueimp-file-upload'
bootbox = require 'bootbox'

StoreFileCollection = require '../../collections/store_file'


class StoreFileComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/file_item'

  events:
    'click .delete': 'onClickDelete'
    'click .file_link': 'onClickLink'

  initialize: (options) =>
    @store_model = options.store_model

  onClickLink: (event) =>
    $target = $(event.target)
    link = $target.attr('href')
    window.open(link, '_blank')

  onClickDelete: =>
    bootbox.confirm "Вы действительно хотите удалить файл?", (result)=>
      if result == true
        $.ajax
          url: "/api/stores/store/file_delete/"
          data:
            "file_id": @model.id
          type: 'post'
          success: (data) =>
            helpers.generate_notyfication('success', 'Удалено')
            bus.vent.trigger 'file:delete'
          error: (data) =>
            helpers.modelErrorHandler @model, data


class StoreFileComplexListView extends complex.ComplexCompositeView
  childView: StoreFileComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/file_items'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.files'

  events:
    'click .m_add': 'onAddClick'

  initialize: (options) =>
    @store_model = options.model
    @collection = new StoreFileCollection
      store_id: @store_model.id
    @list_view = new StoreFileComplexListView
      collection: @collection

  onRender: =>
    @region_complex.show(@list_view)
    @collection.fetch()

    @listenTo(bus.vent, 'file:delete', @onDeleteFile)

  onDeleteFile: =>
    @collection.fetch()

  onAddClick: =>
    @$('.fileupload_file').fileupload
      url: "/api/stores/store/#{@model.id}/file_upload/"
      dataType: 'json'
      submit: (e, data) =>
        $('#file_add_btn').removeClass('fa-plus-circle')
        $('.m_add').removeClass('btn-primary')
        $('.m_add').addClass('btn-default')
        $('#file_add_btn').disabled = true
        $('#file_add_btn').addClass('fa-spinner')
      always: (e, data) =>
        $('#file_add_btn').addClass('fa-plus-circle')
        $('.m_add').addClass('btn-primary')
        $('.m_add').removeClass('btn-default')
        $('#file_add_btn').disabled = false
        $('#file_add_btn').removeClass('fa-spinner')
      done: (e, response) =>
        @collection.fetch()
      fail: (e, response) =>
        helpers.generate_notyfication('error', 'не поддерживаемый тип файла')


module.exports = LayoutView
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

EmployeeCollection = require '../../collections/employee'


class FilterModel extends complex.FilterModel
  defaults:
    page: 1
    page_size: '10'
    page_count: 1


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class EmployeeComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class EmployeeComplexListView extends complex.ComplexCompositeView
  childView: EmployeeComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class EmployeeComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2
  scroll_on_update: false


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @can_employees_view = helpers.user_has_permission bus.cache.user, 'can_employees_view'
    @store_model = options.model
    @collection = new EmployeeCollection
    @filter_model = new FilterModel
    @filter_model.set('store', @store_model.id)
    @filter_model.set('is_working', "2")
    @list_view = new EmployeeComplexListView
      collection: @collection

    @complex_view = new EmployeeComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'employees'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()
    if @can_employees_view
      @listenTo @complex_view.list_view, 'childview:click', (view)=>
        window.open("/employee/#{view.model.id}/", '_blank')


module.exports = LayoutView
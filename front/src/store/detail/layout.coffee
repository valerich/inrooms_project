$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'
autosize = require 'autosize'

helpers = require 'helpers'

StoreModel = require '../models/store'
StoreFileLayout = require './store_file/layout'
EmployeeLayout = require './employees/layout'


class ItemView extends marionette.ItemView
  template: require './templates/item'
  translated_fields:
    name: 'Название'
    client: 'Клиент'
    name: 'Название'
    address: 'Адрес'
    address_description: 'Расположение'
    phone: 'Телефон'
    opening: 'Режим работы'
    total_area: 'Общая площадь'
    trade_area: 'Площадь торгового зала'
    employees_num: 'Количество сотрудников'
    it_info: 'Информация для установки и подключения IT оборудования'
    is_active: 'Активен'
    competitors: 'Конкуренты'
    products: 'Товар'
    target_audience: 'Целевая аудитория'
    plan_avg_cheque: 'Плановый средний чек'
    plan_conversion: 'Плановая конверсия'
    emporium_url: 'Урл ТЦ'
    emporium_contacts: 'Контакты ТЦ'

  initialize: =>
    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'
      address: '[name=address]'
      address_description: '[name=address_description]'
      phone: '[name=phone]'
      opening: '[name=opening]'
      total_area: '[name=total_area]'
      trade_area: '[name=trade_area]'
      employees_num: '[name=employees_num]'
      it_info: '[name=it_info]'
      is_active: '[name=is_active]'
      competitors: '[name=competitors]'
      products: '[name=products]'
      target_audience: '[name=target_audience]'
      plan_avg_cheque: '[name=plan_avg_cheque]'
      plan_conversion: '[name=plan_conversion]'
      emporium_url: '[name=emporium_url]'
      emporium_contacts: '[name=emporium_contacts]'

    backbone.Validation.bind @

    ta = @$('textarea')

    setTimeout ->
      autosize(ta)
    , 1000


  onClickSave: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'
    'region_file': '.region_file'
    'region_employees': '.region_employees'
  events:
    'click .to_list': 'goToList'
    'click .breadcrumb_list': 'goToList'
    'click .save': 'onClickSave'

  initialize: (options)=>
    @binder = new modelbinder
    @store_model = new StoreModel
      id: options.id

    @item_view = new ItemView
      model: @store_model

    @is_own = options.is_own

  onClickSave: (event) =>
    @item_view.onClickSave(event)

  goToList: (event) =>
    event.preventDefault()
    if @is_own
      backbone.history.navigate '/own_store/', trigger: true
    else
      backbone.history.navigate '/store/', trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    if @is_own
      bus.vent.trigger 'menu:active:set', null, 'own_store'
    else
      bus.vent.trigger 'menu:active:set', null, 'store'

    @store_model.fetch().done =>

      @region_item.show(@item_view)

      @file_view = new StoreFileLayout
        model: @store_model

      @region_file.show(@file_view)

      @employees_view = new EmployeeLayout
        model: @store_model
      @region_employees.show(@employees_view)

    @binder.bind @store_model, @$el,
      name: '.breadcrumb_name'




module.exports = LayoutView
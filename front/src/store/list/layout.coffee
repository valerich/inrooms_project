$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

StoreCollection = require '../collections/store'
StoreModel = require '../models/store'


class FilterModel extends complex.FilterModel
  defaults:
    page: 1
    page_size: '10'
    page_count: 1
    is_active: "2"


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onAddClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onAddClick: =>
    addNewView = new NewStoreView
      model: new StoreModel
      is_own: @is_own

    bus.vent.trigger 'modal:show', addNewView,
      title: 'Добавить новый магазин'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @is_own = options.is_own

  onRender: =>
    if @is_own
      @binder.bind @model, @$el,
        search: '[name=search]'
        is_active: '[name=is_active]'
    else
      @binder.bind @model, @$el,
        search: '[name=search]'
        client: '[name=client]'
        is_active: '[name=is_active]'

      helpers.initAjaxSelect2 @$('[name=client]'),
        url: '/api/clients/client/'
        placeholder: "Выберите клиента"
        minimumInputLength: -1
        allowClear: true
        get_extra_search_params: () =>
          kind: 1

  serializeData: =>
    s_data = super
    s_data['is_own'] = @is_own
    s_data


class StoreComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class StoreComplexListView extends complex.ComplexCompositeView
  childView: StoreComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class StoreComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class NewStoreView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    name: 'Название'
    client: 'Прартнер'

  initialize: (options) =>
    @binder = new modelbinder
    @is_own = options.is_own

  onRender: =>
    if @is_own
      @model.set('is_own', true)
      @binder.bind @model, @$el,
        name: '[name=name]'
    else
      @binder.bind @model, @$el,
        name: '[name=name]'
        client: "[name=client]"

      helpers.initAjaxSelect2 @$('[name=client]'),
        url: '/api/clients/client/'
        placeholder: "Выберите клиента"
        minimumInputLength: -1
        allowClear: true
        get_extra_search_params: () =>
          kind: 1

    backbone.Validation.bind @

  serializeData: =>
    s_data = super
    s_data['is_own'] = @is_own
    s_data

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Магазин создан')
          @destroy()
          if @is_own
            backbone.history.navigate "/own_store/#{model.id}/", trigger: true
          else
            backbone.history.navigate "/store/#{model.id}/", trigger: true
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @is_own = options.is_own
    @collection = new StoreCollection
    @filter_model = new FilterModel
    if @is_own
      @filter_model.set('is_own', 2)
    else
      @filter_model.set('is_own', 3)
    @list_view = new StoreComplexListView
      collection: @collection

    @complex_view = new StoreComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'stores'

    @filter_view = new FilterView
      model: @filter_model
      is_own: @is_own

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    if @is_own
      bus.vent.trigger 'menu:active:set', null, 'own_store'
    else
      bus.vent.trigger 'menu:active:set', null, 'store'
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

    @listenTo @complex_view.list_view, 'childview:click', (view)=>
      if @is_own
        backbone.history.navigate "/own_store/#{view.model.id}/", trigger: true
      else
        backbone.history.navigate "/store/#{view.model.id}/", trigger: true


module.exports = LayoutView
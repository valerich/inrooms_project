backbone = require 'backbone'

StoreFileModel = require '../models/store_file'


class StoreFileCollection extends backbone.Collection
  model: StoreFileModel

  initialize: (options) =>
    @store_id = options.store_id

  url: =>
    "/api/stores/store/#{@store_id}/files/"

#  parse: (response) =>
#    @count = response.count
#    return response.results

module.exports = StoreFileCollection
backbone = require 'backbone'

EmployeeModel = require '../models/employee'


class EmployeeCollection extends backbone.Collection
  model: EmployeeModel
  url: "/api/stores/employee/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = EmployeeCollection

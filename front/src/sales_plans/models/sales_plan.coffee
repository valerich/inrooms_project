_ = require 'underscore'
backbone = require 'backbone'


class SalesPlanModel extends backbone.Model

  initialize: (options) =>
    @store_id = options.store_id
    console.log @store_id

  url: =>
    if @id
      "/api/orders/sales_plan/#{@store_id}/#{@id}/"
    else
      "/api/orders/sales_plan/#{@store_id}/"

  validation:
    month: [
      {
        required: true,
        msg: 'Заполните месяц'
      }, {
        pattern: 'digits',
        msg: 'Укажите корректный месяц'
      }, {
        range: [1, 12]
        msg: 'Месяц не может быть меньше 1 и больше 12'
      }]
    year: [
      {
        required: true,
        msg: 'Заполните год'
      }, {
        pattern: 'digits',
        msg: 'Укажите корректный год'
      }, {
        range: [2000, 3000]
        msg: 'Год не может быть меньше 2000 и больше 3000'
      }]
    plan: [
      {
        required: true,
        msg: 'Заполните план'
      }, {
        pattern: 'digits',
        msg: 'Укажите корректный план'
      }, {
        range: [0, 1000000000]
        msg: 'План не может быть меньше 0 и больше 1000000000'
      }]


module.exports = SalesPlanModel

backbone = require 'backbone'


class StoreModel extends backbone.Model

  url: =>
    "/api/stores/store/#{@id}/"

module.exports = StoreModel
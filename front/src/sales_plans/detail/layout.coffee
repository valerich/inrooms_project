$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'

helpers = require 'helpers'

StoreModel = require '../models/store'
SalesPlanList = require './sales_plans_list/layout'


class ItemView extends marionette.ItemView
  template: require './templates/item'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'
    'region_sales_plans': '.region_sales_plans'
  events:
    'click .to_list': 'goToList'
    'click .breadcrumb_list': 'goToList'

  initialize: (options)=>
    @userCanViewSalesPlan = helpers.user_has_permission bus.cache.user, 'can_sales_plan_view'
    @binder = new modelbinder
    @store = new StoreModel
      id: options.id

    @item_view = new ItemView
      model: @store

    @sales_plans_view = new SalesPlanList
      store: @store

  goToList: (event) =>
    event.preventDefault()
    backbone.history.navigate '/sales_plan/', trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    if @userCanViewSalesPlan
      bus.vent.trigger 'menu:active:set', null, 'sales_plan'
      @store.fetch().done =>
        @region_item.show(@item_view)
        @region_sales_plans.show(@sales_plans_view)
      @binder.bind @store, @$el,
        name: '.breadcrumb_name'


module.exports = LayoutView
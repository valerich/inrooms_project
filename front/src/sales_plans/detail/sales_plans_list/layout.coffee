$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

SalesPlanCollection = require '../../collections/sales_plan'
SalesPlanModel = require '../../models/sales_plan'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onAddClick'

  onAddClick: =>
    model = new SalesPlanModel
      store_id: @store.id
    addNewView = new NewSalesPlanView
      model: model

    bus.vent.trigger 'modal:show', addNewView,
      title: 'Добавить план на месяц'
      modal_size: 'lg'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @store = options.store

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      month: '[name=month]'
      year: '[name=year]'


class SalesPlanComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class SalesPlanComplexListView extends complex.ComplexCompositeView
  childView: SalesPlanComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class SalesPlanComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class NewSalesPlanView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    month: 'Месяц'
    year: 'Год'
    plan: 'План'

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      month: '[name=month]'
      year: '[name=year]'
      plan: '[name=plan]'

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'План продаж создан')
          bus.vent.trigger 'sales_plan:created'
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class EditSalesPlanView extends marionette.ItemView
  template: require './templates/edit_modal'
  events:
    'click [name=create]': 'onSaveBtnClick'
  translated_fields:
    month: 'Месяц'
    year: 'Год'
    plan: 'План'
    day1: 'Понедельник'
    day2: 'Вторник'
    day3: 'Среда'
    day4: 'Четверг'
    day5: 'Пятница'
    day6: 'Суббота'
    day7: 'Воскресенье'
    week1: 'Неделя 1'
    week2: 'Неделя 2'
    week3: 'Неделя 3'
    week4: 'Неделя 4'
    week5: 'Неделя 5'
    week6: 'Неделя 6'

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      month: '[name=month]'
      year: '[name=year]'
      plan: '[name=plan]'
      day1: '[name=day1]'
      day2: '[name=day2]'
      day3: '[name=day3]'
      day4: '[name=day4]'
      day5: '[name=day5]'
      day6: '[name=day6]'
      day7: '[name=day7]'
      week1: '[name=week1]'
      week2: '[name=week2]'
      week3: '[name=week3]'
      week4: '[name=week4]'
      week5: '[name=week5]'
      week6: '[name=week6]'

    backbone.Validation.bind @

  onSaveBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'План продаж сохранен')
          bus.vent.trigger 'sales_plan:created'
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @store = options.store
    @collection = new SalesPlanCollection
       store_id: @store.id
    @filter_model = new FilterModel
    @list_view = new SalesPlanComplexListView
      collection: @collection

    @complex_view = new SalesPlanComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'stores'

    @filter_view = new FilterView
      model: @filter_model
      store: @store

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

    @listenTo(bus.vent, 'sales_plan:created', @complex_view.doFilter)
    @listenTo @complex_view.list_view, 'childview:click', (view)=>
      @onClickEditPlan(view.model)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

  onClickEditPlan: (model) =>
    addEditView = new EditSalesPlanView
      model: model

    bus.vent.trigger 'modal:show', addEditView,
      title: 'Править план на месяц'
      modal_size: 'lg'

module.exports = LayoutView
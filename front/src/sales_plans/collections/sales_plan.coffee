backbone = require 'backbone'

SalesPlanModel = require '../models/sales_plan'


class SalesPlanCollection extends backbone.Collection
  model: SalesPlanModel

  initialize: (options) =>
    @store_id = options.store_id

  url: =>
    "/api/orders/sales_plan/#{@store_id}/"

  _prepareModel: (model, options) =>
      model.store_id = @store_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = SalesPlanCollection

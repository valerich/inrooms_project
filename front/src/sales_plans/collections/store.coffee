backbone = require 'backbone'

StoreModel = require '../models/store'


class StoreCollection extends backbone.Collection
  model: StoreModel
  url: "/api/stores/store/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = StoreCollection

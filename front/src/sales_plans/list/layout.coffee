$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

StoreCollection = require '../collections/store'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @is_own = options.is_own

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      client: '[name=client]'

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: "Выберите клиента"
      minimumInputLength: -1
      allowClear: true


class StoreComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class StoreComplexListView extends complex.ComplexCompositeView
  childView: StoreComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class StoreComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @userCanViewSalesPlan = helpers.user_has_permission bus.cache.user, 'can_sales_plan_view'
    @collection = new StoreCollection
    @filter_model = new FilterModel
    @list_view = new StoreComplexListView
      collection: @collection

    @complex_view = new StoreComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'stores'

    @filter_view = new FilterView
      model: @filter_model
      is_own: @is_own

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    if @userCanViewSalesPlan
      bus.vent.trigger 'menu:active:set', null, 'sales_plan'
      @region_complex.show(@complex_view)
      @region_filter.show(@filter_view)
      @complex_view.doFilter()

      @listenTo @complex_view.list_view, 'childview:click', (view)=>
        backbone.history.navigate "/sales_plan/#{view.model.id}/", trigger: true


module.exports = LayoutView
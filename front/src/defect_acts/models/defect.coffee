backbone = require 'backbone'


class DefectModel extends backbone.Model

  url: =>
    if @id
      "/api/orders/defect_act/#{@id}/"
    else
      "/api/orders/defect_act/"

  validation:
      client: [
        {
          required: true,
          msg: 'Заполните партнера'
        }]

module.exports = DefectModel
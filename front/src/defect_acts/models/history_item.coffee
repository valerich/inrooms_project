_ = require 'underscore'
backbone = require 'backbone'


class HisotoryItemModel extends backbone.Model

  initialize: (options) =>
    @defect_act_id = options.defect_act_id

  url: =>
    if @id
      "/api/history/history_item/orders/defectact/#{@defect_act_id}/#{@id}/"
    else
      "/api/history/history_item/orders/defectact/#{@defect_act_id}/"


module.exports = HisotoryItemModel

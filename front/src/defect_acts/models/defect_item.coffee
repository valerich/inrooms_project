backbone = require 'backbone'


class DefectItemModel extends backbone.Model

  url: =>
    "/api/orders/defect_act/#{@.get('defect_act_id')}/items/#{@id}/"

module.exports = DefectItemModel
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

DefectItemCollection = require '../../collections/defect_item'
ImageLayout = require '../images/layout'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/orderitem_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    console.log 'togglePanel'
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      sort_by: '.sort_list'


class DefectItemComplexItemView extends complex.ComplexItemView
  template: require './templates/orderitem_item'
  className: 'order_items_item'
  events:
    'click .save': 'onClickSave'
    'click .remove': 'onClickRemove'
    'click .image-link': 'onClickImageLink'
    'click .defect_item_photo': 'onClickPhoto'

  onClickSave: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Модель сохранена')
    .fail (data) ->
      helpers.modelErrorHandler @model, data

  onClickImageLink: (event) =>
    image_data = @model.get('order_item_detail')['product_detail']['image']
    if image_data
      image = image_data['src']
    else
      image = '/static/img/no-image.jpg'
    $.magnificPopup.open
      type:'image'
      mainClass: 'mfp-fade'
      items: [{'src': image}]
      midClick: true

  onClickPhoto: =>
    console.log 'onClickPhoto'
    editImageView = new ImageLayout
      model: @model
      defect_model: @defect_model
      button_position: 'bottom'

    bus.vent.trigger 'modal:show', editImageView,
      title: 'Изображения возврата'
      modal_size: 'lg'

  onClickRemove: =>
    bootbox.confirm "Вы действительно хотите удалить модель?", (result)=>
      if result == true
        @model.set('defect_act_id', @defect_model.id)
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')
          bus.vent.trigger 'product_item:update'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @defect_model = options.defect_model

  onRender: =>
    @binder.bind @model, @$el,
      comment: '[name=comment]'


class DefectItemComplexListView extends complex.ComplexCompositeView
  childView: DefectItemComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/orderitem_items'

  initialize: (options) =>
    @defect_model = options.defect_model

  childViewOptions: (model, index) =>
    data = {
      defect_model: @defect_model,
      childIndex: index
    }
    return data


class DefectItemComplexView extends complex.ComplexView
  template: require './templates/orderitem_complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout_product'

  regions:
#    region_item_list: '.region_item_list'
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @defect_model = options.model
    @collection = new DefectItemCollection
      defect_id: @defect_model.id
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
    @filter_model = new FilterModel
      _client_id: @defect_model.get('client')
    @filter_model.set('sort_by', store.get("defect_#{@defect_model.id}_item_sort_by"))
    @list_view = new DefectItemComplexListView
      collection: @collection
      defect_model: @defect_model

    @complex_view = new DefectItemComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'defect_items'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      store.set("defect_#{@defect_model.id}_item_sort_by", @filter_model.get('sort_by'))
      @complex_view.doFilter()
    @listenTo(bus.vent, 'product_item:update', @complex_view.doFilter)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()


module.exports = LayoutView

_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

HistoryItemCollection = require '../../collections/history_item'
HistoryItemModel = require '../../models/history_item'


class HistoryItemItemView extends marionette.ItemView
  template: require './templates/history_item_item'
  className: 'history_items_item'


class HistoryItemCollectionView extends marionette.CollectionView
  childView: HistoryItemItemView
  className: 'history_items_list'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  regions:
    region_item_list: '.region_item_list'

  initialize: (options) =>
    @defect_act_model = options.model

  onRender: =>
    @history_item_collection = new HistoryItemCollection
      defect_act_id: @defect_act_model.id

    @history_item_view = new HistoryItemCollectionView
      collection: @history_item_collection

    @region_item_list.show(@history_item_view)

    @history_item_collection.fetch
      reset: true
      data:
        page_size: 1000

module.exports = LayoutView

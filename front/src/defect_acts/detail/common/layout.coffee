_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
bootbox = require 'bootbox'
store = require 'store'
helpers = require 'helpers'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  translated_fields:
    comment: 'Комментарий'
    category: 'Категория'

  events:
    'click .exit': 'onClickExit'
    'click .change_status': 'onClickChangeStatus'
    'click .delete': 'onClickDelete'

  initialize: (options) =>
    @invoice_id = options.model.id
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      comment: '[name=comment]'
      category: '[name=category]'

  onBeforeDestroy: =>
    @binder.unbind()

  onClickDelete: =>
    message = "Вы действительно хотите удалить акт о браке?"
    bootbox.confirm message, (result)=>
      if result == true
        @model.destroy
          success: (model) =>
            backbone.history.navigate "/defect_act/", trigger: true
          error: (model, response, options) =>
            helpers.modelErrorHandler model, response, options

  onClickExit: =>
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
          backbone.history.navigate "/defect_act/", trigger: true
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  changeOrderStatus: (status_code) =>
    @model.save().done =>
        helpers.generate_notyfication('success', 'Сохранено')
        $.ajax
          url: "/api/orders/defect_act/#{@model.id}/change_status/"
          type: 'post'
          data: {
            'code': status_code
          }
          success: =>
            helpers.generate_notyfication('success', 'Статус изменен')
            backbone.history.navigate "/defect_act/", trigger: true
          error: (data) =>
            helpers.modelErrorHandler @model, data

  onClickChangeStatus: (event) =>
    $target = $(event.target)
    new_status_code = $target.data('status_code')
    message = "Вы действительно хотите изменить статус акта о браке?"
    bootbox.confirm message, (result)=>
      if result == true
        $target = $(event.target)
        @changeOrderStatus(new_status_code)

  serializeData: =>
    result = super
    result['user'] = bus.cache.user
    result

module.exports = LayoutView

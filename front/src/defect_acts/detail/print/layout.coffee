_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
bootbox = require 'bootbox'
store = require 'store'
helpers = require 'helpers'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .print_invoice': 'onClickPrintInvoice'
    'click .pdf_invoice': 'onClickPDFInvoice'

  initialize: (options) =>
    @defect_act_id = options.model.id

  onClickPrintInvoice: =>
    url = "/orders/defect_act/" + @defect_act_id + "/export/?as=html"
    window.open(url, '_blank')

  onClickPDFInvoice: =>
    url = "/orders/defect_act/" + @defect_act_id + "/export/?as=pdf"
    window.open(url, '_blank')

module.exports = LayoutView

$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'

DefectCollection = require '../collections/defect'
DefectModel = require '../models/defect'


class FilterModel extends complex.FilterModel
  defaults:
    status__is_end_status: '3'
    page: 1
    page_size: '10'
    page_count: 1


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onClickAdd'

  onClickAdd: =>
    can_all_defect_act_view = helpers.user_has_permission bus.cache.user, 'can_all_defect_act_view'
    if can_all_defect_act_view
      addNewView = new NewDefectView
        model: new DefectModel
      bus.vent.trigger 'modal:show', addNewView,
        title: 'Добавить новый акт о браке'
    else
      bootbox.confirm "Вы действительно хотите добавить акт о браке?", (result)=>
        if result == true
          model = new DefectModel
          model.save {},
            success: (model) =>
              backbone.history.navigate "/defect_act/#{model.get('id')}/", {trigger: true}
              helpers.generate_notyfication('success', 'Акр о браке создан')
            error: (model, response, options) =>
              console.log 'model save error'
              helpers.modelErrorHandler model, response, options, @translated_fields

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      client: '[name=client]'
      items__order_item__product: '[name=items__order_item__product]'
      status: '[name=status]'
      status__is_end_status: '[name=status__is_end_status]'
      sort_by: '.sort_list'

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: "/api/clients/client/"
      placeholder: 'Выберите партнера'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true
      get_extra_search_params: () =>
        kind: 1
    helpers.initAjaxSelect2 @$('[name=items__order_item__product]'),
      url: '/api/products/product/'
      placeholder: "Выберите модель"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"

    helpers.initAjaxSelect2 @$('[name=status]'),
      url: "/api/orders/defect_act_status/"
      placeholder: 'Выберите статус'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true

  serializeData: =>
    @userCanViewClientFilter = helpers.user_has_permission bus.cache.user, 'can_all_defect_act_view'

    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanViewClientFilter'] = @userCanViewClientFilter
    s_data


class DefectComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td.defect_link': 'click'
  events:
    'click .order_return_link': 'onClickOrderReturnLink'

  onClickOrderReturnLink: (event) =>
    $target = $(event.target)
    event.preventDefault()
    order_return_id = $target.data('order_return_id')
    window.open("/order_return/#{order_return_id}/", '_blank')

  className: =>
    if @model.get('order_returns_is_good')
      return 'success'
    return ''


class DefectComplexListView extends complex.ComplexCompositeView
  childView: DefectComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class DefectComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2


class NewDefectView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    client: 'Client'

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      'client': '[name=client]'

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: "/api/clients/client/"
      placeholder: 'Выберите партнера'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          backbone.history.navigate "/defect_act/#{model.get('id')}/", {trigger: true}
          helpers.generate_notyfication('success', 'Акр о браке создан')
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    @collection = new DefectCollection

    if !Object.keys(urlParams).length
      params = store.get('defect_list_url_params')
      if params != undefined
          urlParams = params

    @filter_model = new FilterModel(urlParams)
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @filter_view = new FilterView
      model: @filter_model

  onAdded: =>
    @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'defect_act'
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
      @$('.amount_full_sum').text(@collection.amount_full_sum)

    @region_filter.show(@filter_view)

    list_view = new DefectComplexListView
      collection: @collection

    @complex_view = new DefectComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'defect_acts'

    @region_complex.show(@complex_view)

    @listenTo @complex_view, 'childview:click', (view, options) =>
      backbone.history.navigate "/defect_act/#{view.model.id}/", {trigger: true}

    @complex_view.listenTo @filter_model, 'change', =>
      store.set('defect_list_url_params', @filter_model.toJSON())
      @complex_view.doFilter()
    @complex_view.doFilter()

module.exports = LayoutView
backbone = require 'backbone'

HistoryItemModel = require '../models/history_item'


class HistoryItemCollection extends backbone.Collection
  model: HistoryItemModel

  initialize: (options) =>
    @defect_act_id = options.defect_act_id

  url: =>
    "/api/history/history_item/orders/defectact/#{@defect_act_id}/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = HistoryItemCollection
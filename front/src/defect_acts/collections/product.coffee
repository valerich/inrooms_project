backbone = require 'backbone'

ProductModel = require '../models/product'


class ProductCollection extends backbone.Collection
  model: ProductModel

  initialize: (options) =>
    @defect_act_id = options.defect_act_id

  url: =>
    "/api/orders/defect_act/#{@defect_act_id}/product_search/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ProductCollection
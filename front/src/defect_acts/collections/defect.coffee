backbone = require 'backbone'

DefectModel = require '../models/defect'


class DefectCollection extends backbone.Collection
  model: DefectModel
  url: "/api/orders/defect_act/"

  parse: (response) =>
    @count = response.count
    @amount_full_sum = response.amount_full_sum
    return response.results

module.exports = DefectCollection
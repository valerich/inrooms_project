backbone = require 'backbone'

DefectItemModel = require '../models/defect_item'


class DefectItemCollection extends backbone.Collection
  model: DefectItemModel

  initialize: (options) =>
    @defect_id = options.defect_id

  url: =>
    "/api/orders/defect_act/#{@defect_id}/items/"

  _prepareModel: (model, options) =>
      console.log 'ok'
      model.defect_act_id = @defect_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = DefectItemCollection
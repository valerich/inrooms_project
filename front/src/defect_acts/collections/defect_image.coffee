backbone = require 'backbone'

DefectImageModel = require '../models/defect_image'


class DefectImageCollection extends backbone.Collection
  model: DefectImageModel

  initialize: (options) =>
    @defect_act_id = options.defect_act_id
    @defect_item_id = options.defect_item_id

  url: =>
    "/api/orders/defect_act/#{@defect_act_id}/items/#{@defect_item_id}/images/"

module.exports = DefectImageCollection
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
helpers = require 'helpers'


class ChangePasswordView extends marionette.ItemView
  template: require './templates/change_password'
  events:
    'click [name=save]': 'onSaveBtnClick'

  initialize: (options)=>
    @model = new backbone.Model
    @user_id = options.user_id
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      password: '[name=password]'
      password2: '[name=password2]'

    backbone.Validation.bind @

  onSaveBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: "/api/accounts/user/" + @user_id + "/set_password/"
        type: 'post'
        data: {
          'password': @model.get('password')
          'password2': @model.get('password2')
        }
        success: =>
          helpers.generate_notyfication('success', 'Пароль изменен')
          bus.vent.trigger 'user:password_changed', @model
          if @user_id == bus.cache.user.id
            window.open '/login/', '_self', false
          else
            @destroy()
        error: (data) =>
          helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


module.exports = ChangePasswordView

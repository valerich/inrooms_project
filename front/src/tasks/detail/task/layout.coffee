$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'
complex = require 'complex'
helpers = require 'helpers'


class ItemView extends marionette.CompositeView
  template: require './templates/item'
  translated_fields:
    name: 'Название'
    client: 'Партнер'
    date_plan: 'Плановая дата'
    description: 'Описание'
    status: 'Статус'

  events:
    'click .save': 'onClickSave'

  initialize: =>
    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      name: "[name=name]"
      client: "[name=client]"
      date_plan: "[name=date_plan]"
      description: "[name=description]"
      status: "[name=status]"

    @$('[name=date_plan]').datepicker
      autoclose: true

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: 'Введите клиента'
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

    backbone.Validation.bind @

  onClickSave: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  serializeData: =>
    @userCanViewTaskParnter = helpers.user_has_permission bus.cache.user, 'can_view_task_partner'
    @userCanEditTaskDatePlan = helpers.user_has_permission bus.cache.user, 'can_edit_task_date_plan'


    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanViewTaskParnter'] = @userCanViewTaskParnter
    s_data['user']['userCanEditTaskDatePlan'] = @userCanEditTaskDatePlan
    s_data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'

  initialize: (options)=>
    @task = options.model

    @item_view = new ItemView
      model: @task

  onRender: =>
    @region_item.show(@item_view)


module.exports = LayoutView
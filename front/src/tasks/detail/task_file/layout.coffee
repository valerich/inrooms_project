$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'
complex = require 'complex'
helpers = require 'helpers'
require 'blueimp-file-upload'
bootbox = require 'bootbox'

TaskFileCollection = require '../../collections/task_file'


class TaskFileComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/file_item'

  triggers:
    'click .link': 'click'

  events:
    'click .delete': 'onClickDelete'

  initialize: (options) =>
    @task_model = options.task_model

  onClickDelete: =>
    bootbox.confirm "Вы действительно хотите удалить файл?", (result)=>
      if result == true
        $.ajax
          url: "/api/tasks/task/file_delete/"
          data:
            "file_id": @model.id
          type: 'post'
          success: (data) =>
            helpers.generate_notyfication('success', 'Удалено')
            bus.vent.trigger 'file:delete'
          error: (data) =>
            helpers.modelErrorHandler @model, data


class TaskFileComplexListView extends complex.ComplexCompositeView
  childView: TaskFileComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/file_items'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.files'

  events:
    'click .add': 'onAddClick'

  initialize: (options) =>
    @task_model = options.model
    @collection = new TaskFileCollection
      task_id: @task_model.id
    @list_view = new TaskFileComplexListView
      collection: @collection

  onRender: =>
    @region_complex.show(@list_view)
    @collection.fetch()

    @listenTo(bus.vent, 'file:delete', @onDeleteFile)

    @listenTo @list_view, 'childview:click', (view)=>
      win = window.open(view.model.get('file'), '_blank')
      win.focus()

  onDeleteFile: =>
    @collection.fetch()

  onAddClick: =>
    @$('.fileupload').fileupload
      url: "/api/tasks/task/#{@model.id}/file_upload/"
      dataType: 'json'
      submit: (e, data) =>
        $('#music_add_btn').removeClass('fa-plus-circle')
        $('.add').removeClass('btn-primary')
        $('.add').addClass('btn-default')
        $('#music_add_btn').disabled = true
        $('#music_add_btn').addClass('fa-spinner')
      always: (e, data) =>
        $('#music_add_btn').addClass('fa-plus-circle')
        $('.add').addClass('btn-primary')
        $('.add').removeClass('btn-default')
        $('#music_add_btn').disabled = false
        $('#music_add_btn').removeClass('fa-spinner')
      done: (e, response) =>
        @collection.fetch()
      fail: (e, response) =>
        helpers.generate_notyfication('error', 'не поддерживаемый тип файла')


module.exports = LayoutView
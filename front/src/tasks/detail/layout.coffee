marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

TaskLayout = require './task/layout'
TaskFileLayout = require './task_file/layout'

TaskModel = require '../models/task'


class NavbarView extends marionette.ItemView
  template: require './templates/navbar'
  events:
    'click .tab_link': 'onClickTabLink'
  initialize: (options) =>
    @tab_name = options.tab_name
  onRender: =>
    @$(".tab_link[data-tab_name=#{@tab_name}]").addClass('active')
  onClickTabLink: (event) =>
    event.preventDefault()
    tab_name = $(event.target).parent().data('tab_name')
    @$(".tab_link").removeClass('active')
    @$(".tab_link[data-tab_name=#{tab_name}]").addClass('active')
    @trigger 'click', tab_name


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'task_detail_layout'
  regions:
    region_tab: '.region_tab'
    region_navbar: '.region_navbar'
  events:
    'click .list_link': 'onClickListLink'

  onClickListLink: (event) =>
    event.preventDefault()
    backbone.history.navigate '/task/', trigger: true

  initialize: (options) =>
    @tab_name = options.tab_name
    @binder = new modelbinder
    @model = new TaskModel
      id: options.id

  onBeforeDestroy: =>
    @binder.unbind()

  renderTab: (tab_name) =>
    view_class = null
    if tab_name is 'common'
      view_class = TaskLayout
    if tab_name is 'files'
      view_class = TaskFileLayout
    if view_class
      tab_view = new view_class
        model: @model
      @region_tab.show(tab_view)
    else
      @region_tab.empty()

  onRender: =>
    @binder.bind @model, @$el,
      name: '.breadcrumb_name'
    bus.vent.trigger 'menu:active:set', null, 'task'
    @model.fetch().done =>
      @renderTab(@tab_name)
      navbar_view = new NavbarView
        model: @model
        tab_name: @tab_name
      @region_navbar.show(navbar_view)
      @listenTo navbar_view, 'click', (tab_name) =>
        @tab_name = tab_name
        backbone.history.navigate "/task/#{@model.id}/#{tab_name}/", trigger: false
        @renderTab(tab_name)

module.exports = ObjectDetailLayout

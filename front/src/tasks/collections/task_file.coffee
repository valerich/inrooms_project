backbone = require 'backbone'

TaskFileModel = require '../models/task_file'


class TaskFileCollection extends backbone.Collection
  model: TaskFileModel

  initialize: (options) =>
    @task_id = options.task_id

  url: =>
    "/api/tasks/task/#{@task_id}/files/"

module.exports = TaskFileCollection
backbone = require 'backbone'

TaskModel = require '../models/task'


class TaskCollection extends backbone.Collection
  model: TaskModel
  url: "/api/tasks/task/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = TaskCollection
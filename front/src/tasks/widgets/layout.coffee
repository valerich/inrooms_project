$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
complex = require 'complex'
marionette = require 'backbone.marionette'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
Highcharts = require('highcharts/highstock')


class TaskModel extends backbone.Model
  null


class TaskCollection extends backbone.Collection
  model: TaskModel

  initialize: (options) =>
    @url = "/api/tasks/task/"

  parse: (response) =>
    @count = response.count
    return response.results


class TaskItemView extends marionette.ItemView
  tagName: 'tr'
  template: require './templates/task-item'

  triggers:
    'click td': 'click'

  className: =>
    return 'danger' if @model.get('is_alarm')


class TaskCompositeView extends marionette.CompositeView
  template: require './templates/task-items'
  childView: TaskItemView
  childViewContainer: ".region_table_items"


class LayoutView extends marionette.LayoutView
  template: require './templates/task-layout'
  regions:
    'region_items': '.region_items'

  initialize: (options) =>
    @task_collection = new TaskCollection()
    @task_view = new TaskCompositeView
      collection: @task_collection

  onRender: =>
    @userCanViewTaskWidget = helpers.user_has_permission bus.cache.user, 'can_view_task_widget'
    if @userCanViewTaskWidget
      @region_items.show(@task_view)
      @task_collection.fetch(
        data:
          status: 1
          page_size: 100000
        processData: true
      )
      @listenTo @task_view, 'childview:click', (view)=>
        backbone.history.navigate "/task/#{view.model.id}/", trigger: true


module.exports = LayoutView
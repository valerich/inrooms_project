$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

TaskCollection = require '../collections/task'
TaskModel = require '../models/task'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onAddClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onAddClick: =>
    addNewView = new NewTaskView
      model: new TaskModel

    bus.vent.trigger 'modal:show', addNewView,
      title: 'Добавить новую задачу'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'

  serializeData: =>
    @userCanAddTask = helpers.user_has_permission bus.cache.user, 'can_add_task'

    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanAddTask'] = @userCanAddTask
    s_data


class TaskComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'

  className: =>
    return 'danger' if @model.get('is_alarm')


class TaskComplexListView extends complex.ComplexCompositeView
  childView: TaskComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class TaskComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class NewTaskView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    name: 'Название'
    date_plan: 'Дата план'
    client: "Партнер"
    description: "Описание"

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'
      date_plan: "[name=date_plan]"
      client: "[name=client]"
      description: "[name=description]"

    backbone.Validation.bind @

    @$('[name=date_plan]').datepicker
      autoclose: true

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: 'Введите клиента'
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Задача создана')
          @destroy()
          backbone.history.navigate "/task/#{model.id}/", trigger: true
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @listenTo(bus.vent, 'client:added', @onAdded)
    @collection = new TaskCollection
    @filter_model = new FilterModel
    @list_view = new TaskComplexListView
      collection: @collection

    @complex_view = new TaskComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'tasks'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'task'
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

    @listenTo @complex_view.list_view, 'childview:click', (view)=>
      backbone.history.navigate "/task/#{view.model.id}/", trigger: true

  onAdded: =>
    @complex_view.doFilter()


module.exports = LayoutView
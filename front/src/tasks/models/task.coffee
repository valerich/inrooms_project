backbone = require 'backbone'


class TaskModel extends backbone.Model

  url: =>
    if @id
      "/api/tasks/task/#{@id}/"
    else
      "/api/tasks/task/"

module.exports = TaskModel
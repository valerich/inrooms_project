backbone = require 'backbone'


class PayingModel extends backbone.Model
  defaults:
    department: 1
    include_to_office_plan: true
  validation:
    invoice: [
      {
        required: true,
        msg: 'Заполните Счет'
      }]

  url: =>
    if @id
      "/api/invoices/paying/#{@id}/"
    else
      "/api/invoices/paying/"

module.exports = PayingModel
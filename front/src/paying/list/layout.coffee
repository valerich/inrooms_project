$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'

PayingCollection = require '../collections/paying'
PayingModel = require '../models/paying'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onClickAdd'

  onClickAdd: =>
    addNewView = new NewPayingView
      model: new PayingModel

    bus.vent.trigger 'modal:show', addNewView,
      title: 'Добавить новый платеж'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      invoice: '[name=invoice]'
      invoice__customer: '[name=invoice__customer]'
      invoice__kind: '[name=invoice__kind]'
      department: '[name=department]'
      include_to_office_plan: '[name=include_to_office_plan]'
      sort_by: '.sort_list'

    helpers.initAjaxSelect2 @$('[name=invoice]'),
      url: "/api/invoices/invoice/"
      placeholder: 'Выберите счет'
      minimumInputLength: -1
      text_attr: 'number'
      allowClear: true
    helpers.initAjaxSelect2 @$('[name=invoice__customer]'),
      url: '/api/clients/client/'
      placeholder: "Выберите получателя"
      minimumInputLength: -1
      allowClear: true
      text_attr: 'name'


class PayingComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'

  className: =>
    if @model.get('invoice_detail')['category_detail']['id'] == 1
      return 'success'
    return ''


class PayingComplexListView extends complex.ComplexCompositeView
  childView: PayingComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class PayingComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2


class NewPayingView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    invoice: 'Счет'
    date: 'Дата'
    number: 'Номер'
    amount: 'Сумма'

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      'invoice': '[name=invoice]'
      'date': '[name=date]'
      'number': '[name=number]'
      'amount': '[name=amount]'
      'include_to_office_plan': '[name=include_to_office_plan]'
      'department': '[name=department]'

    helpers.initAjaxSelect2 @$('[name=invoice]'),
      url: "/api/invoices/invoice/"
      placeholder: 'Выберите счет'
      minimumInputLength: -1
      text_attr: 'number'
      allowClear: true
      text_attr: (obj) =>
        return "#{obj.number} - #{obj.customer_detail.name} (#{obj.kind_detail.name})"
      get_extra_search_params: () =>
        to_create_paying: 2

    @$('[name=date]').datepicker
      autoclose: true

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          backbone.history.navigate "/paying/#{model.get('id')}/", {trigger: true}
          helpers.generate_notyfication('success', 'Платеж создан')
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    @collection = new PayingCollection

    @filter_model = new FilterModel(urlParams)
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @filter_view = new FilterView
      model: @filter_model

  onAdded: =>
    @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'paying'
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
      @$('.amount_sum').text(@collection.amount_sum)

    @region_filter.show(@filter_view)

    list_view = new PayingComplexListView
      collection: @collection

    @complex_view = new PayingComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'paying'

    @region_complex.show(@complex_view)

    @listenTo @complex_view, 'childview:click', (view, options) =>
      backbone.history.navigate "/paying/#{view.model.id}/", {trigger: true}

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @complex_view.doFilter()

module.exports = LayoutView
backbone = require 'backbone'

PayingModel = require '../models/paying'


class PayingCollection extends backbone.Collection
  model: PayingModel
  url: "/api/invoices/paying/"

  parse: (response) =>
    @count = response.count
    @amount_sum = response.amount_sum
    return response.results

module.exports = PayingCollection

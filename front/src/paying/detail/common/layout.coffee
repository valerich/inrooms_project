_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
helpers = require 'helpers'
bus = require 'bus'
bootbox = require 'bootbox'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  events:
      'click .save': 'onClickSave'

  onClickSave: =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  initialize: (options) =>
    @paying_model = options.model
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      'amount': '[name=amount]'
      'comment': '[name=comment]'

    backbone.Validation.bind @

  serializeData: =>
    s_data = super
    userCanEditAmount = helpers.user_has_permission bus.cache.user, 'can_paying_amount_edit'
    s_data['userCanEditAmount'] = userCanEditAmount
    return s_data

module.exports = LayoutView

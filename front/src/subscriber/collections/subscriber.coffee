backbone = require 'backbone'

SubscriberModel = require '../models/subscriber'


class SubscriberCollection extends backbone.Collection
  model: SubscriberModel
  url: "/api/sms/subscriber/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = SubscriberCollection

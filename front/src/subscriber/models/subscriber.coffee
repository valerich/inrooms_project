backbone = require 'backbone'


class SubscriberModel extends backbone.Model

  url: =>
    if @id
      "/api/sms/subscriber/#{@id}/"
    else
      "/api/sms/subscriber/"

module.exports = SubscriberModel
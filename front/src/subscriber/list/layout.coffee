$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

SubscriberCollection = require '../collections/subscriber'
SubscriberModel = require '../models/subscriber'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
        search: '[name=search]'


class SubscriberComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'


class SubscriberComplexListView extends complex.ComplexCompositeView
  childView: SubscriberComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class SubscriberComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @collection = new SubscriberCollection
    @filter_model = new FilterModel
    @list_view = new SubscriberComplexListView
      collection: @collection

    @complex_view = new SubscriberComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'subscribers'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'subscriber'
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()


module.exports = LayoutView
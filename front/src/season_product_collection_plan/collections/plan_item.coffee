backbone = require 'backbone'

PlanItemModel = require '../models/plan_item'


class PlanItemCollection extends backbone.Collection
  model: PlanItemModel

  initialize: (options) =>
    @plan_id = options.plan_id

  url: =>
      "/api/products/season_product_collection_plan/#{@plan_id}/items/"

  _prepareModel: (model, options) =>
      model.plan_id = @plan_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = PlanItemCollection

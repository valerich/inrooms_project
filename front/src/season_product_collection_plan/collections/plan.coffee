backbone = require 'backbone'

PlanModel = require '../models/plan'


class PlanCollection extends backbone.Collection
  model: PlanModel

  url: =>
    "/api/products/season_product_collection_plan/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = PlanCollection

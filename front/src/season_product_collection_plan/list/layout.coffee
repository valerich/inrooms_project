$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

PlanCollection = require '../collections/plan'
PlanModel = require '../models/plan'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onAddClick'

  onAddClick: =>
    model = new PlanModel
    addNewView = new NewPlanView
      model: model

    bus.vent.trigger 'modal:show', addNewView,
      title: 'Добавить % сезонность'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class NewPlanView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    year: 'Год'
    season: 'Сезон'

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      year: '[name=year]'
      season: '[name=season]'
      units_per_s_m: '[name=units_per_s_m]'
      avg_unit_cost: '[name=avg_unit_cost]'

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', '% сезонность создан')
          bus.vent.trigger 'plan:created'
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class PlanComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class PlanComplexListView extends complex.ComplexCompositeView
  childView: PlanComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class PlanComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @userCanViewSeasonProductCollectionPlan = helpers.user_has_permission bus.cache.user, 'can_season_product_collection_plan_view'

    @collection = new PlanCollection
    @filter_model = new FilterModel
    @list_view = new PlanComplexListView
      collection: @collection

    @complex_view = new PlanComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'plans'

    @filter_view = new FilterView
      model: @filter_model
      is_own: @is_own

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    if @userCanViewSeasonProductCollectionPlan
      bus.vent.trigger 'menu:active:set', null, 'season_product_collection_plan'
      @region_complex.show(@complex_view)
      @region_filter.show(@filter_view)
      @complex_view.doFilter()

      @listenTo @complex_view.list_view, 'childview:click', (view)=>
        backbone.history.navigate "/season_product_collection_plan/#{view.model.id}/", trigger: true
      @listenTo(bus.vent, 'plan:created', @complex_view.doFilter)


module.exports = LayoutView
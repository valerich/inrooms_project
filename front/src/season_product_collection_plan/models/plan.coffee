_ = require 'underscore'
backbone = require 'backbone'


class PlanModel extends backbone.Model

  url: =>
    if @id
      "/api/products/season_product_collection_plan/#{@id}/"
    else
      "/api/products/season_product_collection_plan/"

  validation:
    year: [
      {
        required: true,
        msg: 'Заполните год'
      }, {
        pattern: 'digits',
        msg: 'Укажите корректный год'
      }, {
        range: [2000, 3000]
        msg: 'Год не может быть меньше 2000 и больше 3000'
      }]
    season: [
      {
        required: true,
        msg: 'Заполните сезон'
      }, {
        pattern: 'digits',
        msg: 'Укажите корректный план'
      }, {
        range: [1, 2]
        msg: 'сезон не может быть меньше 1 и больше 2'
      }]


module.exports = PlanModel

_ = require 'underscore'
backbone = require 'backbone'


class PlanItemModel extends backbone.Model

  initialize: (options) =>
    @plan_id = options.plan_id

  url: =>
    if @id
      "/api/products/season_product_collection_plan/#{@plan_id}/items/#{@id}/"
    else
      "/api/products/season_product_collection_plan/#{@plan_id}/items/"

  validation:
    plan: [
      {
        required: true,
        msg: 'Заполните план'
      }]


module.exports = PlanItemModel

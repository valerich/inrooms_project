$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

PlanItemCollection = require '../../collections/plan_item'
PlanItemModel = require '../../models/plan_item'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @store = options.store

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class PlanItemComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class PlanItemComplexListView extends complex.ComplexCompositeView
  childView: PlanItemComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class PlanItemComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class EditPlanItemView extends marionette.ItemView
  template: require './templates/edit_modal'
  events:
    'click [name=create]': 'onSaveBtnClick'
  translated_fields:
    plan: 'План'

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      plan: '[name=plan]'

    backbone.Validation.bind @

  onSaveBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Cохранено')
          bus.vent.trigger 'plan_item:created'
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @plan = options.plan
    @collection = new PlanItemCollection
       plan_id: @plan.id
    @filter_model = new FilterModel
    @list_view = new PlanItemComplexListView
      collection: @collection

    @complex_view = new PlanItemComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'plan_items'

    @filter_view = new FilterView
      model: @filter_model
      plan: @plan

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

    @listenTo(bus.vent, 'plan_item:created', @complex_view.doFilter)
    @listenTo @complex_view.list_view, 'childview:click', (view)=>
      @onClickEditPlan(view.model)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

  onClickEditPlan: (model) =>
    addEditView = new EditPlanItemView
      model: model

    bus.vent.trigger 'modal:show', addEditView,
      title: 'Править план для коллекции'
      modal_size: 'lg'

module.exports = LayoutView
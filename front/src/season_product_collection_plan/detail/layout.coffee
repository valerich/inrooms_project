$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'

helpers = require 'helpers'

PlanModel = require '../models/plan'
PlanItemsList = require './plan_items_list/layout'


class ItemView extends marionette.ItemView
  template: require './templates/item'
  events:
    'click .save': 'onClickSave'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @store = options.store

  onRender: =>
    @binder.bind @model, @$el,
      avg_unit_cost: '[name=avg_unit_cost]'
      units_per_s_m: '[name=units_per_s_m]'

    backbone.Validation.bind @

  onClickSave: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'
    'region_plan_items': '.region_plan_items'
  events:
    'click .to_list': 'goToList'
    'click .breadcrumb_list': 'goToList'

  initialize: (options)=>
    @userCanViewSeasonProductCollectionPlan = helpers.user_has_permission bus.cache.user, 'can_season_product_collection_plan_view'

    @binder = new modelbinder
    @plan = new PlanModel
      id: options.id

    @item_view = new ItemView
      model: @plan

    @plan_items_view = new PlanItemsList
      plan: @plan

  goToList: (event) =>
    event.preventDefault()
    backbone.history.navigate '/season_product_collection_plan/', trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    if @userCanViewSeasonProductCollectionPlan
      bus.vent.trigger 'menu:active:set', null, 'season_product_collection_plan'
      @plan.fetch().done =>
        @region_item.show(@item_view)
        @region_plan_items.show(@plan_items_view)
        @plan.set('name', "#{@plan.get('season_detail')['name']} - #{@plan.get('year')}")
      @binder.bind @plan, @$el,
        name: '.breadcrumb_name'


module.exports = LayoutView
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require '../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
require 'magnific-popup'

ProductCollection = require '../../collections/product'
#ProductModel = require '../../models/product'
#OrderItemModel = require '../../models/order_item'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/product_supply_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    console.log 'togglePanel'
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class ProductComplexItemView extends complex.ComplexItemView
  template: require './templates/product_supply_item'

  events:
    'click .create_defect_item': 'onClickCreateDefectItem'
    'click .create_all_defect_item': 'onClickCreateAllDefectItem'
    'click .image-link': 'onClickImageLink'

  onClickImageLink: (event) =>
    image_data = @model.get('image')
    if image_data
      image = image_data['src']
    else
      image = '/static/img/no-image.jpg'
    $.magnificPopup.open
      type:'image'
      mainClass: 'mfp-fade'
      items: [{'src': image}]
      midClick: true

  initialize: (options)=>
    @binder = new modelbinder
    @defect_model = options.defect_model

  onClickCreateDefectItem: (event)=>
    event.preventDefault()
    target = $(event.target)
    if not target.data('order_id')
      target = target.parent()
    order_id = target.data('order_id')
    size_code = target.data('size_code')
    quantity = @$('#quantity_' + order_id + '_' + size_code).val()
    product_id = @model.id
    $.ajax
        url: "/api/orders/refund_act/#{@defect_model.id}/add_product/"
        data:
          "product_id": product_id
          "size_code": size_code
          "order_id": order_id
          "quantity": quantity
        type: 'post'
        success: =>
          bus.vent.trigger 'order_item:added'
        error: (data) =>
          helpers.modelErrorHandler @model, data

  onClickCreateAllDefectItem: (event)=>
    event.preventDefault()
    target = $(event.target)
    if not target.data('order_id')
      target = target.parent()
    order_id = target.data('order_id')
    product_id = @model.id
    size_codes = {}
    @.$('.size_quantity_' + order_id + '_' + product_id).each (index, el) =>
      size_codes[$(el).data('size_code')] = $(el).val()
    data = {
      "product_id": product_id
      "order_id": order_id
    }
    for i in ['34','35','36','37','38','39','40','41','xs','s','m','l','xl','xxl','xxxl']
        if size_codes.hasOwnProperty(i)
          data['size_quantity_' + i] = size_codes[i]
        else
          data['size_quantity_' + i] = 0
    $.ajax
        url: "/api/orders/refund_act/#{@defect_model.id}/add_products/"
        data: data
        type: 'post'
        success: =>
          bus.vent.trigger 'order_item:added'
        error: (data) =>
          helpers.modelErrorHandler @model, data


class ProductComplexListView extends complex.ComplexCompositeView
  childView: ProductComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/product_supply_items'

  initialize: (options) =>
    @defect_model = options.defect_model

  childViewOptions: (model, index) =>
    data = {
      defect_model: @defect_model,
      childIndex: index
    }
    return data


class ProductComplexView extends complex.ComplexView
  template: require './templates/product_supply_complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout_product_supply_search'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @defect_model = options.model
    @collection = new ProductCollection
      defect_act_id: @defect_model.id

    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
    @filter_model = new FilterModel
      _client_id: @defect_model.get('client')
    @list_view = new ProductComplexListView
      collection: @collection
      defect_model: @defect_model

    @complex_view = new ProductComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'products'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @listenTo(bus.vent, 'order_item:added', @onProductAdded)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

  onProductAdded: (product_id) =>
    @complex_view.doFilter()


module.exports = LayoutView
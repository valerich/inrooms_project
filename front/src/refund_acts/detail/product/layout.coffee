marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'


ProductSearchLayout = require './layout_product_search_supply'
ProductLayout = require './layout_product'


class NavbarView extends marionette.ItemView
  template: require './templates/navbar'
  events:
    'click .tab_link': 'onClickTabLink'
  initialize: (options) =>
    @tab_name = options.tab_name
    @defect_model = options.model

  onRender: =>
    @$(".tab_link[data-tab_name=#{@tab_name}]").addClass('active')
  onClickTabLink: (event) =>
    event.preventDefault()
    tab_name = $(event.target).parent().data('tab_name')
    @$(".tab_link").removeClass('active')
    @$(".tab_link[data-tab_name=#{tab_name}]").addClass('active')
    @trigger 'click', tab_name


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'order_detail_product_layout'
  regions:
    region_tab: '.region_product_tab'
    region_navbar: '.region_product_navbar'

  initialize: (options) =>
    @tab_name = 'product_list'
    @defect_model = options.model

  renderTab: (tab_name) =>
    view_class = null
    if tab_name is 'product_search'
      view_class = ProductSearchLayout
    if tab_name is 'product_list'
      view_class = ProductLayout
    if view_class
      @tab_view = new view_class
        model: @defect_model
      @region_tab.show(@tab_view)
    else
      @region_tab.empty()

  onRender: =>
    @renderTab(@tab_name)
    navbar_view = new NavbarView
      model: @defect_model
      tab_name: @tab_name
    @region_navbar.show(navbar_view)

    @listenTo navbar_view, 'click', (tab_name) =>
      @tab_name = tab_name
      @renderTab(tab_name)

    @renderSidebar()
    @listenTo(bus.vent, 'product_item:update', @onItemsChanged)
    @listenTo(bus.vent, 'order_item:added', @onItemsChanged)

#    @listenTo(bus.vent, 'order_item:added', @onAddedOrderItem)
    @sidebar_obj = @$('#fixed_sidebar')
    @offset = @sidebar_obj.offset()
    @topOffset = @offset.top
    @topPadding = 150

    $(window).bind('scroll', (ev) =>
      @scrollSidebar()
    )

  onItemsChanged: (tab_name) =>
    @defect_model.fetch().done =>
      @renderSidebar()
      if tab_name
        @renderTab(tab_name)

  renderSidebar: =>
    @$('.amount_full').html(@defect_model.get('amount_full'))

  scrollSidebar: =>
    if $(window).scrollTop() > @topPadding
      @sidebar_obj.stop().animate({marginTop: $(window).scrollTop() - @topOffset - @topPadding}, 200)
    else
      @sidebar_obj.stop().animate({marginTop: 0}, 200)


module.exports = ObjectDetailLayout

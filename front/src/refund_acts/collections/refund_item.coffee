backbone = require 'backbone'

RefundItemModel = require '../models/refund_item'


class RefundItemCollection extends backbone.Collection
  model: RefundItemModel

  initialize: (options) =>
    @refund_id = options.refund_id

  url: =>
    "/api/orders/refund_act/#{@refund_id}/items/"

  _prepareModel: (model, options) =>
      console.log 'ok'
      model.refund_act_id = @refund_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = RefundItemCollection
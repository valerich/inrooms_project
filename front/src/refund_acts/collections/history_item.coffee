backbone = require 'backbone'

HistoryItemModel = require '../models/history_item'


class HistoryItemCollection extends backbone.Collection
  model: HistoryItemModel

  initialize: (options) =>
    @refund_act_id = options.refund_act_id

  url: =>
    "/api/history/history_item/orders/refundact/#{@refund_act_id}/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = HistoryItemCollection
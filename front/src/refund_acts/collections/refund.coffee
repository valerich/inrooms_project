backbone = require 'backbone'

DefectModel = require '../models/refund'


class DefectCollection extends backbone.Collection
  model: DefectModel
  url: "/api/orders/refund_act/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = DefectCollection
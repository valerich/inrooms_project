_ = require 'underscore'
backbone = require 'backbone'


class HisotoryItemModel extends backbone.Model

  initialize: (options) =>
    @refund_act_id = options.refund_act_id

  url: =>
    if @id
      "/api/history/history_item/orders/refundact/#{@refund_act_id}/#{@id}/"
    else
      "/api/history/history_item/orders/refundact/#{@refund_act_id}/"


module.exports = HisotoryItemModel

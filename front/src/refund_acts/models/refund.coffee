backbone = require 'backbone'


class RefundModel extends backbone.Model

  url: =>
    if @id
      "/api/orders/refund_act/#{@id}/"
    else
      "/api/orders/refund_act/"

module.exports = RefundModel
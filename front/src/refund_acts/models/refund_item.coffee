backbone = require 'backbone'


class RefundItemModel extends backbone.Model

  url: =>
    "/api/orders/refund_act/#{@.get('refund_act_id')}/items/#{@id}/"

module.exports = RefundItemModel
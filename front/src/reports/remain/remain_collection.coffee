backbone = require 'backbone'

RemainModel = require './remain_model'


class RemainCollection extends backbone.Collection
  model: RemainModel
  url: "/api/products/remain/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = RemainCollection
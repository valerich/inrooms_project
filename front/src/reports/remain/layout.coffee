$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
complex = require 'complex'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
moment = require 'moment'

RemainCollection = require './remain_collection'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/data_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      product: '[name=product]'
    helpers.initAjaxSelect2 @$('[name=product]'),
      url: '/api/products/product/'
      placeholder: "Выберите модель"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"


class DataComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/data_item'


class DataComplexListView extends complex.ComplexCompositeView
  childView: DataComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/data_items'


class DataComplexView extends complex.ComplexView
  template: require './templates/data_complex_layout'
  showSearch: false
  urlFilterParam: false
  d: 2


class DataLayoutView extends marionette.LayoutView
  template: require './templates/data_layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>

    @collection = new RemainCollection

    @form_model = options.form_model
#    console.log options.form_model['warehouse__client']

    @filter_model = new FilterModel
      warehouse__client: options.form_model.get('warehouse__client')
      value: 0

    @filter_view = new FilterView
      model: @filter_model

  onRender: =>
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @region_filter.show(@filter_view)

    list_view = new DataComplexListView
      collection: @collection

    @complex_view = new DataComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'remains'

    @region_complex.show(@complex_view)

#    @listenTo @complex_view, 'childview:click', (view, options) =>
#      backbone.history.navigate "/order/#{view.model.id}/", {trigger: true}

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @complex_view.doFilter()


class FormModel extends backbone.Model
  validation:
    warehouse__client: [
      {
        required: true,
        msg: 'Заполните партнера'
      }]


class FormView extends marionette.ItemView
  template: require './templates/form'
  events:
    'click .submit_form': 'onSubmitForm'

  onSubmitForm: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.trigger 'get_report'
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      warehouse__client: '[name=client]'

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: "Выберите партнера"
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

    backbone.Validation.bind @


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_form': '.region_form'
    'region_data': '.region_data'

  initialize: =>
    @form_model = new FormModel

    @form_view = new FormView
      model: @form_model

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'report_remain'
    @region_form.show(@form_view)

    @listenTo @form_model, 'get_report', =>
      @data_view = new DataLayoutView
        form_model: @form_model
      @region_data.empty()
      @region_data.show(@data_view)

module.exports = LayoutView
backbone = require 'backbone'


class RemainModel extends backbone.Model

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    if @id
      "/api/products/remain/#{@id}/"
    else
      "/api/products/remain/"

module.exports = RemainModel

$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
moment = require 'moment'
Spinner = require 'spin.js'

Highcharts = require('highcharts')


class FormModel extends backbone.Model
  defaults:
    year: 2017

  validation:
    year: [
      {
        required: true,
        msg: 'Заполните год'
      }
    ]


class FormView extends marionette.ItemView
  template: require './templates/form'
  events:
    'click .submit_form': 'onSubmitForm'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change2'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      year: '[name=year]'


class PayingsView extends marionette.ItemView
  template: require './templates/payings_modal'

  initialize: (options) =>
    @report_data = options.report_data

  serializeData: =>
    s_data = super
    s_data['report_data'] = @report_data
    s_data


class TableView extends marionette.ItemView
  template: require './templates/region_table'
  events:
    'click .export_xls': 'onClickExportXls'
    'click .paying_documents': 'onClickPayingDocuments'

  initialize: (options) =>
    @report_data = options.report_data
    @year = options.year
    @filter_params = options.filter_params

  onClickPayingDocuments: (event) =>
    event.preventDefault()
    year = $(event.target).data('year')
    month = $(event.target).data('month')
    department = $(event.target).data('department')
    $.ajax
      url: "/api/invoices/paying/?date_year=" + year + "&date_month=" + month + "&department=" + department + "&page_size=100000&include_to_office_plan=2"
      type: 'get'
      success: (response)=>
        report_data = response.results

        view = new PayingsView
          report_data: report_data

        bus.vent.trigger 'modal:show', view,
          title: 'Платежи за ' + month + "." + year

  serializeData: =>
    data = super
    data['year'] = @year
    data['departments'] = [
      {
        'id': 1,
        'name': 'Развития',
        'data': @report_data[1]
      }, {
        'id': 2,
        'name': 'Сопровождения',
        'data': @report_data[2]
      }
    ]
    data


class ChartView extends marionette.LayoutView
  template: require './templates/chart'
  regions:
    'region_table': '.region_table'

  spinner_opts:
    lines: 11
    length: 10
    width: 4
    radius: 10
    corners: 1
    rotate: 0
    color: '#000'
    trail: 100
    top: '50%'
    left: 0

  initialize: (options) =>
    @form_model = options.form_model
    @spinner = new Spinner(@spinner_opts).spin()

  showChart: =>
    @showSpinner()
    $.ajax
      url: "/api/invoices/reports/office_plan/"
      type: 'get'
      data: {
        'year': @form_model.get('year')
      }
      success: (response) =>
        @table_view = new TableView
          report_data: response
          year: @form_model.get('year')
          filter_params: {
            'year': @form_model.get('year')
          }
        @region_table.show(@table_view)

      error: (data) =>
        helpers.modelErrorHandler @form_model, data

  showSpinner: =>
    @$('.region_table').html('')
    @$('.region_table').append(@spinner.el)

class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_form': '.region_form'
    'region_chart': '.region_chart'

  initialize: =>
    @form_model = new FormModel

    @form_view = new FormView
      model: @form_model

    @chart_view = new ChartView
      form_model: @form_model

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'report_office_plan'
    @region_form.show(@form_view)
    @region_chart.show(@chart_view)

    @listenTo @form_model, 'change2', =>
      @chart_view.showChart()
    @chart_view.showChart()

module.exports = LayoutView
backbone = require 'backbone'

ReportModel = require './model'


class ReportCollection extends backbone.Collection
  model: ReportModel
  url: "/api/clients/reports/movement_of_goods/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ReportCollection
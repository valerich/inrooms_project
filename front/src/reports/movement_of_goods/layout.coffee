$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
complex = require 'complex'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
moment = require 'moment'

ReportCollection = require './collection'
ReportDetailView = require './detail'
ReportDetailModel = require './model_detail'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/data_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .export_btn': 'onClickExport'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  onClickExport: (event) =>
    addExportView = new ExportView(
      filter_model: @model
    )
    bus.vent.trigger 'modal:show', addExportView,
      title: 'Выгрузка в excel'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      id: '[name=product]'
      has_remains: '[name=has_remains]'
      has_tvp: '[name=has_tvp]'
    helpers.initAjaxSelect2 @$('[name=product]'),
      url: '/api/products/product/'
      placeholder: "Выберите модель"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"


class ExportView extends marionette.ItemView
  template: require './templates/export_modal'
  events:
    'click [name=export]': 'onExportBtnClick'
  translated_fields:
    with_images: 'Включая изображения'

  initialize: (options)=>
    @model = new backbone.Model
      with_images: false
    @filter_model = options.filter_model
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      'with_images': '[name=with_images]'

    backbone.Validation.bind @

  onExportBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      url = '/api/clients/reports/movement_of_goods/?as=xls&'
      if @model.get("with_images")
        url = url + "with_images=" + @model.get("with_images") + '&'
      url = url + $.param(@filter_model.toJSON())
      window.open(url, '_blank')
      @destroy()
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class DataComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/data_item'
  triggers:
    'click td': 'click'


class DataComplexListView extends complex.ComplexCompositeView
  childView: DataComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/data_items'


class DataComplexView extends complex.ComplexView
  template: require './templates/data_complex_layout'
  showSearch: false
  urlFilterParam: false
  d: 2


class DataLayoutView extends marionette.LayoutView
  template: require './templates/data_layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>

    @collection = new ReportCollection

    @form_model = options.form_model
#    console.log options.form_model['warehouse__client']

    @filter_model = new FilterModel
      client: options.form_model.get('client')
      interval: options.form_model.get('interval')
      interval_type: options.form_model.get('interval_type')
      value: 0

    @filter_view = new FilterView
      model: @filter_model

  onRender: =>
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @region_filter.show(@filter_view)

    list_view = new DataComplexListView
      collection: @collection

    @complex_view = new DataComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'remains'

    @region_complex.show(@complex_view)

    @listenTo @complex_view, 'childview:click', (view, options) =>
      @onItemClick(view.model.id)

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @complex_view.doFilter()

  onItemClick: (product_id) =>
    detailView = new ReportDetailView
      model: new ReportDetailModel
      client: @form_model.get('client')
      interval_type: @form_model.get('interval_type')
      interval: @form_model.get('interval')
      product_id: product_id

    bus.vent.trigger 'modal:show', detailView,
      title: 'Детальная информация'
      modal_size: 'lg'


class FormModel extends backbone.Model
  validation:
    interval_type: [
      {
        required: true,
        msg: 'Заполните тип интервала'
      }
    ]
    interval: [
      {
        required: true,
        msg: 'Заполните интервал'
      }
    ]

  defaults:
      interval_type: "years"
      interval: 1


class FormView extends marionette.ItemView
  template: require './templates/form'
  events:
    'click .submit_form': 'onSubmitForm'

  onSubmitForm: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.trigger 'get_report'
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder
    @userCanViewAllClient = helpers.user_has_permission bus.cache.user, 'view_all_report_movement_of_goods'

  onRender: =>
    if @userCanViewAllClient
      @binder.bind @model, @$el,
        client: '[name=client]'
        interval_type: '[name=interval_type]'
        interval: '[name=interval]'

      helpers.initAjaxSelect2 @$('[name=client]'),
        url: '/api/clients/client/'
        placeholder: "Выберите партнера"
        minimumInputLength: -1
        allowClear: true
        get_extra_search_params: () =>
          kind: 1
    else
      @binder.bind @model, @$el,
        interval_type: '[name=interval_type]'
        interval: '[name=interval]'

    backbone.Validation.bind @

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanViewAllClient'] = @userCanViewAllClient
    s_data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_form': '.region_form'
    'region_data': '.region_data'

  initialize: =>
    @form_model = new FormModel

    @form_view = new FormView
      model: @form_model

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'report_movement_of_goods'
    @region_form.show(@form_view)

    @listenTo @form_model, 'get_report', =>
      @data_view = new DataLayoutView
        form_model: @form_model
      @region_data.empty()
      @region_data.show(@data_view)

module.exports = LayoutView
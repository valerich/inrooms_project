backbone = require 'backbone'
marionette = require 'backbone.marionette'


class DetailView extends marionette.ItemView
  template: require './templates/detail_modal'

  initialize: (options) =>
    fetching = @.model.fetch({
      data: $.param({
        interval_type: options.interval_type,
        interval: options.interval,
        client: options.client,
        product: options.product_id
      })
    })

    fetching.done =>
      @.render()


module.exports = DetailView
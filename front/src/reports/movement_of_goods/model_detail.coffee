backbone = require 'backbone'


class ReportModel extends backbone.Model

  url: =>
    "/api/clients/reports/movement_of_goods/detail/"

module.exports = ReportModel

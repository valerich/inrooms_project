backbone = require 'backbone'


class ReportModel extends backbone.Model

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    if @id
      "/api/clients/reports/movement_of_goods/#{@id}/"
    else
      "/api/clients/reports/movement_of_goods/"

module.exports = ReportModel

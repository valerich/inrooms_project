$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
moment = require 'moment'


class FormModel extends backbone.Model
  defaults:
      interval_type: "days"
      interval: 10


class FormView extends marionette.ItemView
  template: require './templates/form'
  events:
    'click .submit_form': 'onSubmitForm'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'get_report'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      interval_type: '[name=interval_type]'
      interval: '[name=interval]'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_form': '.region_form'
    'region_chart': '.region_chart'

  initialize: =>
    @form_model = new FormModel

    @form_view = new FormView
      model: @form_model

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'report_products_not_sold'
    @region_form.show(@form_view)

    @listenTo @form_model, 'get_report', =>
      @getReport()

  getReport: =>
    window.open("/api/clients/reports/products_not_sold/?interval_type=#{@form_model.get("interval_type")}&interval=#{@form_model.get("interval")}", '_blank');
module.exports = LayoutView
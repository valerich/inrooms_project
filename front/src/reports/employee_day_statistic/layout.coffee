$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
complex = require 'complex'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
moment = require 'moment'


class FormModel extends backbone.Model
  validation:
    date_from: [
      {
        required: true,
        msg: 'Заполните дату'
      }
    ]
    date_from: [
      {
        required: true,
        msg: 'Заполните дату'
      }
    ]
    client: [
      {
        required: true,
        msg: 'Заполните партнера'
      }
    ]


class FormView extends marionette.ItemView
  template: require './templates/form'
  events:
    'click .submit_form': 'onSubmitForm'

  onSubmitForm: (event) =>
    event.preventDefault()

    v = @model.validate()
    if @model.isValid()
      url = "/api/reports/employee_day_statistic/?date_from=#{@model.get('date_from')}&date_to=#{@model.get('date_to')}&client=#{@model.get('client')}"
      window.open(url, '_blank')
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      client: '[name=client]'
      date_from: '[name=date_from]'
      date_to: '[name=date_to]'

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: "Выберите партнера"
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

    @$('[name=date_from]').datepicker
      autoclose: true

    @$('[name=date_to]').datepicker
      autoclose: true

    backbone.Validation.bind @


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_form': '.region_form'

  initialize: =>
    @form_model = new FormModel

    @form_view = new FormView
      model: @form_model

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'report_employee_day_statistic'
    @region_form.show(@form_view)

module.exports = LayoutView
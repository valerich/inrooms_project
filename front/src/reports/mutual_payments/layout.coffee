$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
moment = require 'moment'
Spinner = require 'spin.js'

Highcharts = require('highcharts')


class FormModel extends backbone.Model
  validation:
    client: [
      {
        required: true,
        msg: 'Заполните партнера'
      }
    ]


class FormView extends marionette.ItemView
  template: require './templates/form'
  events:
    'click .submit_form': 'onSubmitForm'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change2'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder
    @can_view_report_mutual_payments_all = helpers.user_has_permission bus.cache.user, 'view_report_mutual_payments_all'

  onRender: =>
    if @can_view_report_mutual_payments_all
      @binder.bind @model, @$el,
        client: '[name=client]'
        category: '[name=category]'
        kind: '[name=kind]'
        include_returns: '[name=include_returns]'

      helpers.initAjaxSelect2 @$('[name=client]'),
        url: '/api/clients/client/'
        placeholder: "Выберите клиента"
        minimumInputLength: -1
        allowClear: true
        get_extra_search_params: () =>
          kind: 1

  serializeData: =>
    s_data = super
    s_data['can_view_report_mutual_payments_all'] = @can_view_report_mutual_payments_all
    s_data


class TableView extends marionette.ItemView
  template: require './templates/region_table'
  events:
    'click .export_xls': 'onClickExportXls'
    'click .export_pdf': 'onClickExportPdf'
    'click .export_print': 'onClickExportPrint'
    'click .clickabled': 'onClickItem'

  initialize: (options) =>
    @report_data = options.report_data
    @filter_params = options.filter_params

  onClickExportXls: (event) =>
    event.preventDefault()
    url = "/api/invoices/reports/mutual_payments/?as=xls&client=#{@filter_params['client']}&include_returns=#{@filter_params['include_returns']}"
    if @filter_params["category"]
      url = url + "&category=" + @filter_params["category"]
    if @filter_params["kind"]
      url = url + "&kind=" + @filter_params["kind"]
    window.open(url, '_blank')

  onClickExportPdf: (event) =>
    event.preventDefault()
    url = "/api/invoices/reports/mutual_payments/export/?as=pdf&client=#{@filter_params['client']}&include_returns=#{@filter_params['include_returns']}"
    if @filter_params["category"]
      url = url + "&category=" + @filter_params["category"]
    if @filter_params["kind"]
      url = url + "&kind=" + @filter_params["kind"]
    window.open(url, '_blank')

  onClickExportPrint: (event) =>
    event.preventDefault()
    url = "/api/invoices/reports/mutual_payments/export/?as=html&client=#{@filter_params['client']}&include_returns=#{@filter_params['include_returns']}"
    if @filter_params["category"]
      url = url + "&category=" + @filter_params["category"]
    if @filter_params["kind"]
      url = url + "&kind=" + @filter_params["kind"]
    window.open(url, '_blank')


  onClickItem: (event) =>
    target = $(event.target).parent()
    has_class = target.hasClass('active')
    item_id = target.data('item_id')
    @$('.item').removeClass('active')
    @$('.subitems').addClass('hidden')
    if not has_class
      target.addClass('active')
      @$('.subitems_' + item_id).removeClass('hidden')

  serializeData: =>
    data = super
    data['report'] = @report_data
    data


class ChartView extends marionette.LayoutView
  template: require './templates/chart'
  regions:
    'region_table': '.region_table'

  spinner_opts:
    lines: 11
    length: 10
    width: 4
    radius: 10
    corners: 1
    rotate: 0
    color: '#000'
    trail: 100
    top: '50%'
    left: 0

  initialize: (options) =>
    @form_model = options.form_model
    @spinner = new Spinner(@spinner_opts).spin()

  showChart: =>
    @showSpinner()
    $.ajax
      url: "/api/invoices/reports/mutual_payments/"
      type: 'get'
      data: {
        'client': @form_model.get('client')
        'category': @form_model.get('category')
        'kind': @form_model.get('kind')
        'include_returns': @form_model.get('include_returns')
      }
      success: (response) =>
        @table_view = new TableView
          report_data: response
          filter_params: {
            'client': @form_model.get('client')
            'category': @form_model.get('category')
            'kind': @form_model.get('kind')
            'include_returns': @form_model.get('include_returns')
          }
        @region_table.show(@table_view)

      error: (data) =>
        helpers.modelErrorHandler @form_model, data

  showSpinner: =>
    @$('.region_table').html('')
    @$('.region_table').append(@spinner.el)

class LayoutView extends marionette.LayoutView
  className: 'report_mutual_payment'
  template: require './templates/layout'
  regions:
    'region_form': '.region_form'
    'region_chart': '.region_chart'



  initialize: =>
    @form_model = new FormModel
    @form_model.set('include_returns', '1')

    @form_view = new FormView
      model: @form_model

    @chart_view = new ChartView
      form_model: @form_model

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'report_mutual_payments'
    @region_form.show(@form_view)
    @region_chart.show(@chart_view)

    @listenTo @form_model, 'change2', =>
      @chart_view.showChart()

module.exports = LayoutView
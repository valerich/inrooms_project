$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
moment = require 'moment'
Spinner = require 'spin.js'
require 'sticky-table-headers'

Highcharts = require('highcharts')
require('highcharts/highcharts-3d')(Highcharts)


ConsolidatedProductsCollection = require './collections/products'
ConsolidatedProductsModel = require './models/products'
ProductRemainModel = require './models/product_remain'
ClientCollectionView = require '../../dashboard/clients'
ClientCollection = require '../../clients/collections/client'

ReportMovementOfGoodsDetailView = require '../movement_of_goods/detail'
ReportMovementOfGoodsDetailModel = require '../movement_of_goods/model_detail'

ClientModel = require '../../clients/models/client'


class ProductRemainView extends marionette.LayoutView
  template: require './templates/product_remain'
  regions:
    'region_movement_of_goods': '.region_movement_of_goods'

  initialize: (options) =>
    @client = options.client

  onRender: =>
    if @client
      regionMovementOfGoodsDetailView = new ReportMovementOfGoodsDetailView
        model: new ReportMovementOfGoodsDetailModel
        client: @client
        interval_type: 'years'
        interval: 10
        product_id: @model.id

      @region_movement_of_goods.show(regionMovementOfGoodsDetailView)


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/products_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class ProductsComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/products_item'
  events:
    'click td': 'onClickRow'

  initialize: (options) =>
    @client = options.client

  serializeData: =>
    s_data = super
    size_count = @model.get('size_data').length
    if size_count <= 0
      size_count = 1
    s_data['size_count'] = size_count
    s_data

  onClickRow: (event) =>
    product_remain_model = new ProductRemainModel
      id: @model.id
    product_remain_model.fetch().done =>
      remainView = new ProductRemainView
        model: product_remain_model
        client: @client

      bus.vent.trigger 'modal:show', remainView,
        title: 'Остатки'
        modal_size: 'lg'


class ProductsComplexListView extends complex.ComplexCompositeView
  childView: ProductsComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/products_items'

  initialize: (options) =>
    @client = options.client

  childViewOptions: (model, index) =>
    return {client: @client}

  onBeforeDestroy: =>
    @$('#table-sticky').stickyTableHeaders('destroy')

  onRender: =>
    @$('#table-sticky').stickyTableHeaders({marginTop: 50})


class ProductsComplexView extends complex.ComplexView
  template: require './templates/products_complex_layout'
  showSearch: false
  d: 2


class ProductsLayout extends marionette.LayoutView
  template: require './templates/products_layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @product_collection = options.product_collection
    @client = options.client
    @product__kind = options.product__kind
    @has_moskow_remains = options.has_moskow_remains

    @collection = new ConsolidatedProductsCollection
    @filter_model = new FilterModel
    @filter_model.set('collection', @product_collection)
    @filter_model.set('client', @client)
    @filter_model.set('product__kind', @product__kind)
    @filter_model.set('has_moskow_remains', @has_moskow_remains)
    @list_view = new ProductsComplexListView
      collection: @collection
      client: @client

    @complex_view = new ProductsComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'products'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @listenTo @collection, 'sync', =>
      @$('.product_table_header').text(@collection.main_collection_name)
    @complex_view.doFilter()


class TableView extends marionette.ItemView
  template: require './templates/region_table'
  events:
    'click .load_detail': 'onClickLoadDetail'
    'click .load_all_report': 'onClickLoadAllReport'

  initialize: (options) =>
    @report_data = options.report_data
    @report_kind = @report_data['report_kind']

  serializeData: =>
    data = super
    data['report_data'] = @report_data
    data

  onClickLoadDetail: (event) =>
    collection_parrent = $(event.target).data('collection_parrent')
    if collection_parrent == undefined
      collection_parrent = ''
    collection = $(event.target).data('collection')
    if collection == undefined
      collection = ''
    if @report_kind == 1
      bus.vent.trigger 'load_detail', collection, ''
    if @report_kind == 2
      bus.vent.trigger 'load_products', collection

  onClickLoadAllReport: (event) =>
    bus.vent.trigger 'load_detail', '', ''


class ChartView extends marionette.LayoutView
  template: require './templates/chart'
  regions:
    'region_table': '.region_table'
    'region_products_table': '.region_products_table'

  spinner_opts:
    lines: 11
    length: 10
    width: 4
    radius: 10
    corners: 1
    rotate: 0
    color: '#000'
    trail: 100
    top: '50%'
    left: 0

  initialize: (options) =>
    @spinner = new Spinner(@spinner_opts).spin()
    @client = options.client
    @product__kind = options.product__kind
    @has_moskow_remains = options.has_moskow_remains

  showChart: (collection_parrent, collection) =>
    if collection == undefined
      collection = ''

    if collection_parrent == undefined
      collection = ''

    @showSpinner()

    $chart_1_selector = $("#chart1")

    a = new Highcharts.chart
      chart:
          renderTo: $chart_1_selector[0]
    a.destroy()

    $.ajax
      url: "/api/reports/consolidated/?collection_parrent=#{collection_parrent}&collection=#{collection}&client=#{@client}&product__kind=#{@product__kind}&has_moskow_remains=#{@has_moskow_remains}"
      type: 'get'
      success: (response) =>
        chart_data = []

        for i in response['collection_data']
          if i['remain'] != 0 or i['sold'] != 0 or i['supplied'] != 0
            chart_data.push([i['product_collection']['name'], i['all_sold_percent']])

        compare = (a,b) ->
          if (a[1] < b[1])
            return 1
          if (a[1] > b[1])
            return -1
          return 0

        chart_data.sort(compare)

        chart = new Highcharts.chart
            chart:
                renderTo: $chart_1_selector[0]
                height: 400
                type: 'pie'
                options3d:
                    enabled: true
                    alpha: 45
                    beta: 0
            title:
                text: '% продано'
            tooltip:
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            plotOptions:
                pie:
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels:
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            credits:
                enabled: false
            series: [{
                type: 'pie',
                name: '% продано',
                data: chart_data
            }]

        @table_view = new TableView
          report_data: response
        @region_table.show(@table_view)

      error: (data) =>
        helpers.modelErrorHandler @form_model, data

  showProductsTable: (product_collection) =>
    @showProductsSpinner()

    @product_view = new ProductsLayout
       product_collection: product_collection
       client: @client
       product__kind: @product__kind
       has_moskow_remains: @has_moskow_remains

    @region_products_table.show(@product_view)

  showSpinner: =>
    if @region_table != undefined and not @region_table.isEmpty
      @region_table.empty()
    @$('.region_table').append(@spinner.el)

  showProductsSpinner: =>
    if @region_products_table != undefined and not @region_products_table.isEmpty
      @region_products_table.empty()
    @$('.region_products_table').append(@spinner.el)


class FormModel extends backbone.Model
  defaults:
     product__kind: '1'
     has_moskow_remains: false

  validation:
    client: [
      {
        required: true,
        msg: 'Заполните партнера'
      }
    ]


class FormView extends marionette.LayoutView
  template: require './templates/form'
  regions:
    'region_client_list': '.region_client_list'

  onSubmitForm: () =>
    v = @model.validate()
    if @model.isValid()
      @model.trigger 'get_report'
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  initialize: =>
    @binder = new modelbinder
    @client_collection = new ClientCollection
    @client_view = new ClientCollectionView
      collection: @client_collection

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      client: '[name=client]'
      product__kind: '[name=product__kind]'
      has_moskow_remains: '[name=has_moskow_remains]'

    @listenTo @model, 'change', =>
      if @model.get("client")
        @onSubmitForm()

    @client_collection.fetch(
      data: {
        is_active: 2,
        kind: 1,
        has_store: 2,
      }
    ).done =>

      blank_client_model = new ClientModel
        id: 0
        name: 'Все партнеры'
      @client_collection.add(blank_client_model)

      @region_client_list.show(@client_view)
      @listenTo @client_view, 'childview:click', (view, options) =>
        client_id = view.model.id
        if not client_id
          client_id = "all_client"
        @model.set('client', client_id)

    backbone.Validation.bind @


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_form': '.region_form'
    'region_chart': '.region_chart'

  initialize: =>
    @form_model = new FormModel

    @form_view = new FormView
      model: @form_model

    @chart_is_empty = true

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'report_consolidated'
    @region_form.show(@form_view)

    @listenTo @form_model, 'get_report', =>
#        @region_chart.empty()

      client_id = @form_model.get('client')
      if client_id == 'all_client'
        client_id = ''

      @chart_view = new ChartView
        client: client_id
        product__kind: @form_model.get('product__kind', '')
        has_moskow_remains: @form_model.get('has_moskow_remains', false)
      @region_chart.show(@chart_view)
      @chart_view.showChart('', '')

    @listenTo(bus.vent, 'load_detail', @showChart)
    @listenTo(bus.vent, 'load_products', @showProductsTable)

  showChart: (options) =>
    @chart_view.showChart(options)

  showProductsTable: (options) =>
    @chart_view.showProductsTable(options)

module.exports = LayoutView
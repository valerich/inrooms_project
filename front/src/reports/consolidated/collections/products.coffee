backbone = require 'backbone'

ProductsModel = require '../models/products'


class ProductsCollection extends backbone.Collection
  model: ProductsModel
  url: "/api/reports/consolidated/products"

  parse: (response) =>
    @count = response.count
    @main_collection_name = response.main_collection['name']
    return response.results

module.exports = ProductsCollection
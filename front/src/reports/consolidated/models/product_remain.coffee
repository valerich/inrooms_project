backbone = require 'backbone'


class ProductRemainModel extends backbone.Model

  url: =>
    "/api/reports/consolidated/remains/#{@id}/"

module.exports = ProductRemainModel
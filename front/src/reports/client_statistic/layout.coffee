$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
moment = require 'moment'

Highcharts = require('highcharts')


class FormModel extends backbone.Model
  defaults:
      date_from: moment().subtract(10, 'days').format("DD.MM.YYYY")
      date_to: moment().format("DD.MM.YYYY")


class FormView extends marionette.ItemView
  template: require './templates/form'
  events:
    'click .submit_form': 'onSubmitForm'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change2'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      date_from: '[name=date_from]'
      date_to: '[name=date_to]'
      period_kind: '[name=period_kind]'
      include_order_returns: '[name=include_order_returns]'

    @$('[name=date_from]').datepicker
      autoclose: true

    @$('[name=date_to]').datepicker
      autoclose: true


class TableView extends marionette.ItemView
  template: require './templates/region_table'

  initialize: (options) =>
    @report_data = options.report_data

  serializeData: =>
    data = super
    data['report_data'] = @report_data
    data

  onRender: =>
    @$('.weekend').each (index, el) =>
      col_id = $(el).parent().data("col_id")
      @$('.datacol_' + col_id).addClass('col_weekend')

    table = @$('.report_table')
    fixedColumn = table.clone().insertBefore(table).addClass('fixed-column')

    fixedColumn.find('tr').each (i, elem) =>
      @$(elem).find('th').slice(2).remove()
      @$(elem).find('td').slice(2).remove()

    fixedColumn.find('tr').each (i, elem) =>
        @$(elem).height(table.find('tr:eq(' + i + ')').height())

    @$('[data-toggle="popover"]').popover()


class ChartView extends marionette.LayoutView
  template: require './templates/chart'
  regions:
    'region_table': '.region_table'

  initialize: (options) =>
    @form_model = options.form_model

  showChart: =>
    $.ajax
      url: "/api/clients/reports/client_statistic/"
      type: 'get'
      data: {
        'date_from': @form_model.get('date_from')
        'date_to': @form_model.get('date_to')
        'period_kind': @form_model.get('period_kind')
        'include_order_returns': @form_model.get('include_order_returns')
      }
      success: (response) =>
        visitors_count = response.visitors_count
        sales_amount = response.sales_amount
        chart_1_series = []
        chart_2_series_data = []
        chart_3_series = []
        chart_4_series_data = []
        for item in response.report_data
          chart_1_item_data = {
            'name': item['name']
            'data': item['visitors_count']
          }
          chart_2_item_data = {
            'name': item['name']
            'y': item['visitors_sum']
          }
          chart_3_item_data = {
            'name': item['name']
            'data': item['sales_amount']
          }
          chart_4_item_data = {
            'name': item['name']
            'y': item['sales_sum']
          }
          chart_1_series.push chart_1_item_data
          chart_2_series_data.push chart_2_item_data
          chart_3_series.push chart_3_item_data
          chart_4_series_data.push chart_4_item_data

        $chart_1_selector = $("#chart1")
        chart1 = new Highcharts.chart
            chart:
                renderTo: $chart_1_selector[0]
                height: 400
                zoomType: 'x'
            title:
                text: 'Количество посетителей'
            xAxis:
                type: 'category'
                categories: response.dates
            yAxis:
                title:
                    text: 'Посещения (шт)'
            tooltip:
                valueSuffix: ' шт'
                shared: true
                crosshairs: true
            plotOptions:
                spline:
                    marker:
                        enabled: false
            credits:
                enabled: false
            series: chart_1_series

        $chart_2_selector = $("#chart2")
        chart2 = new Highcharts.chart
            chart:
                renderTo: $chart_2_selector[0]
                height: 400
                type: 'pie'
            title:
                text: 'Количество посетителей'
            plotOptions:
                pie:
                    allowPointSelect: true
                    cursor: 'pointer'
                    dataLabels:
                        enabled: true
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            credits:
                enabled: false
            series: [{
              'name': 'Количество посетителей'
              'colorByPoint': true
              'data': chart_2_series_data
            }]

        $chart_3_selector = $("#chart3")
        chart3 = new Highcharts.chart
            chart:
                renderTo: $chart_3_selector[0]
                height: 400
                zoomType: 'x'
            title:
                text: 'Сумма продаж'
            xAxis:
                type: 'category'
                categories: response.dates
            yAxis:
                title:
                    text: 'Продажи (р)'
            tooltip:
                valueSuffix: ' р'
                shared: true
                crosshairs: true
            plotOptions:
                spline:
                    marker:
                        enabled: false
            credits:
                enabled: false
            series: chart_3_series

        $chart_4_selector = $("#chart4")
        chart4 = new Highcharts.chart
            chart:
                renderTo: $chart_4_selector[0]
                height: 400
                type: 'pie'
            title:
                text: 'Cумма продаж'
            plotOptions:
                pie:
                    allowPointSelect: true
                    cursor: 'pointer'
                    dataLabels:
                        enabled: true
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            credits:
                enabled: false
            series: [{
              'name': 'Продажи'
              'colorByPoint': true
              'data': chart_4_series_data
            }]
        @table_view = new TableView
          report_data: response
        @region_table.show(@table_view)

      error: (data) =>
        helpers.modelErrorHandler @form_model, data

class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_form': '.region_form'
    'region_chart': '.region_chart'

  initialize: =>
    @form_model = new FormModel
    @form_model.set('period_kind', 'days')
    @form_model.set('include_order_returns', "0")

    @form_view = new FormView
      model: @form_model

    @chart_view = new ChartView
      form_model: @form_model

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'report_client_statistic'
    @region_form.show(@form_view)
    @region_chart.show(@chart_view)
    @chart_view.showChart()

    @listenTo @form_model, 'change2', =>
      @chart_view.showChart()
#    @listenTo @form_model, 'change', =>
#      @chart_view.showChart()

module.exports = LayoutView
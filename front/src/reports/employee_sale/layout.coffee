$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
moment = require 'moment'
Spinner = require 'spin.js'

Highcharts = require('highcharts')


class FormModel extends backbone.Model
  defaults:
    store_detail: 1

  validation:
    date_from: [
      {
        required: true,
        msg: 'Заполните дату с'
      }
    ]
    date_to: [
      {
        required: true,
        msg: 'Заполните дату по'
      }
    ]


class FormView extends marionette.ItemView
  template: require './templates/form'
  events:
    'click .submit_form': 'onSubmitForm'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change2'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder
    @can_view_report_all_employee_sale = helpers.user_has_permission bus.cache.user, 'view_report_all_employee_sale'

  onRender: =>
    if @can_view_report_all_employee_sale
      @binder.bind @model, @$el,
        date_from: '[name=date_from]'
        date_to: '[name=date_to]'
        store: '[name=store]'
        store_detail: '[name=store_detail]'

      helpers.initAjaxSelect2 @$('[name=store]'),
        url: '/api/stores/store/'
        multiple: true
        placeholder: "Выберите магизин"
        minimumInputLength: -1
        allowClear: true
        text_attr: (obj) =>
          return "#{obj.name} (#{obj.client_detail.name})"

    else
      @binder.bind @model, @$el,
        date_from: '[name=date_from]'
        date_to: '[name=date_to]'

    @$('[name=date_from]').datepicker
      autoclose: true

    @$('[name=date_to]').datepicker
      autoclose: true

  serializeData: =>
    s_data = super
    s_data['can_view_report_all_employee_sale'] = @can_view_report_all_employee_sale
    s_data


class TableView extends marionette.ItemView
  template: require './templates/region_table'

  initialize: (options) =>
    @report_data = options.report_data
    @can_employees_view = helpers.user_has_permission bus.cache.user, 'can_employees_view'

  serializeData: =>
    s_data = super
    s_data['report'] = @report_data
    s_data['can_employees_view'] = @can_employees_view
    console.log 'can_employees_view', s_data['can_employees_view']
    s_data


class ChartView extends marionette.LayoutView
  template: require './templates/chart'
  regions:
    'region_table': '.region_table'

  spinner_opts:
    lines: 11
    length: 10
    width: 4
    radius: 10
    corners: 1
    rotate: 0
    color: '#000'
    trail: 100
    top: '50%'
    left: 0

  initialize: (options) =>
    @form_model = options.form_model
    @spinner = new Spinner(@spinner_opts).spin()

  showChart: =>
    @showSpinner()
    $.ajax
      url: "/api/orders/reports/employee_sale/"
      type: 'get'
      data: {
        'date_from': @form_model.get('date_from')
        'date_to': @form_model.get('date_to')
        'store': @form_model.get('store')
        'store_detail': @form_model.get('store_detail')
      }
      success: (response) =>
        @table_view = new TableView
          report_data: response
        @region_table.show(@table_view)

      error: (data) =>
        helpers.modelErrorHandler @form_model, data

  showSpinner: =>
    @$('.region_table').html('')
    @$('.region_table').append(@spinner.el)

class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_form': '.region_form'
    'region_chart': '.region_chart'

  initialize: =>
    @form_model = new FormModel

    today = new Date()
    date_to = today.toLocaleDateString('ru-RU')
    today.setDate(1)
    date_from = today.toLocaleDateString('ru-RU')
    @form_model.set('date_to', date_to)
    @form_model.set('date_from', date_from)

    @form_view = new FormView
      model: @form_model

    @chart_view = new ChartView
      form_model: @form_model

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'report_employee_sale'
    @region_form.show(@form_view)
    @region_chart.show(@chart_view)

    @listenTo @form_model, 'change2', =>
      @chart_view.showChart()

module.exports = LayoutView
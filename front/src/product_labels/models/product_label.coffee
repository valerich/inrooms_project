backbone = require 'backbone'


class ProductLabelModel extends backbone.Model

  url: =>
    if @id
      "/api/products/product_label/#{@id}/"
    else
      "/api/products/product_label/"

  validation:
      kind: [
        {
          required: true,
          msg: 'Заполните тип'
        }]
      name: [
        {
          required: true,
          msg: 'Заполните название'
        }]

module.exports = ProductLabelModel
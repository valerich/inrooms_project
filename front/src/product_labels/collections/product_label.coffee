backbone = require 'backbone'

ProductLabelModel = require '../models/product_label'


class ProductLabelCollection extends backbone.Collection
  model: ProductLabelModel
  url: "/api/products/product_label/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ProductLabelCollection
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
bootbox = require 'bootbox'
require 'select2'
require 'backbone-validation'
require 'jquery.inputmask'

ProductLabelCollection = require '../collections/product_label'
ProductLabelModel = require '../models/product_label'
ImageLayout = require '../image/layout'
SizesBoard = require '../../components/sizes_board/layout'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onAddClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onAddClick: =>
    addNewProductLabelView = new NewProductLabelView
      model: new ProductLabelModel

    bus.vent.trigger 'modal:show', addNewProductLabelView,
      title: 'Добавить новую этикетку'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      kind: '[name=kind]'
      has_images: '[name=has_images]'

      
class SimpleRemainsItemView extends marionette.ItemView
    template: require './templates/simple_sizes'

    serializeData: =>
      s_data = super

      warehouses = new Object()

      for warehouse, sizes of @model.get('remains')
        if warehouses[warehouse] is undefined
          warehouses[warehouse] = 0

        warehouses[warehouse] += _.reduce (size for label, size of sizes), (memo, num) ->
          memo + num
        , 0

      s_data.remains = not _.isEmpty(warehouses)
      s_data.warehouses = warehouses
      s_data


class ProductLabelComplexItemView extends marionette.LayoutView
  template: require './templates/item'

  events:
    'click .image-link': 'onImageLinkClick'
    'click .image-edit': 'onImageEditClick'
    'click .save': 'saveModel'

  regions:
    'priceBoardRegion': '.price-board-region'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  fetching: =>
    @model.fetch()
    .done =>
      @.render()

  onImageLinkClick: =>
    $.magnificPopup.open({
      type:'image',
      gallery: {
        enabled: true
      },
      removalDelay: 300,
      mainClass: 'mfp-fade',
      items: @model.get('images'),
      gallery: {
        enabled: true
      },
    })

  onImageEditClick: =>
    editImageView = new ImageLayout
      model: @model
      button_position: 'bottom'

    bus.vent.trigger 'modal:show', editImageView,
      title: 'Изображения модели'
      modal_size: 'lg'

  saveModel: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Этикетка сохранена')
    .fail (data) ->
      helpers.modelErrorHandler @model, data

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'
      kind: '[name=kind]'
      country: '[name=country]'
      structure: '[name=structure]'
      more_information: '[name=more_information]'

    # права на просмотр остатков/размеров
    can_product_has_remains_view = helpers.user_has_permission bus.cache.user, 'can_product_has_remains_view'

    if can_product_has_remains_view
      # выводить цены только если "Основная"
      if @model.get('kind') == 1
        # вывод матрицы с ценами
        @priceBoardRegion.show new SizesBoard model: @model, showLetterSizes: true
      else
        # или просто значений по складам
        @priceBoardRegion.show new SimpleRemainsItemView model: @model

    backbone.Validation.bind @

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    s_data


class ProductLabelComplexListView extends complex.ComplexCompositeView
  childView: ProductLabelComplexItemView
  childViewContainer: ".region_product_label_panel_items"
  template: require './templates/items'

  initialize: =>
    @listenTo(bus.vent, 'product_label:images:update', @onProductLabelUpdate)

  onProductLabelUpdate: (model) =>
    console.log 'onProductLabelUpdate'
    item_view = @.children.findByModel(model)
    item_view.fetching()


class ProductLabelComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2


class NewProductLabelView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    name: 'Название'
    kind: 'Тип'

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'
      kind: '[name=kind]'

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Этикетка создана')
          bus.vent.trigger 'product_label:added'
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @listenTo(bus.vent, 'product_label:added', @onProductLabelAdded)

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    @filter_model = new FilterModel(urlParams)
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @collection = new ProductLabelCollection
    @list_view = new ProductLabelComplexListView
      collection: @collection

    @complex_view = new ProductLabelComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'product_labels'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'product_label'
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

  onProductLabelAdded: =>
    @complex_view.doFilter()


module.exports = LayoutView
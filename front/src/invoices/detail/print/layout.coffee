_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
bootbox = require 'bootbox'
store = require 'store'
helpers = require 'helpers'
bootbox = require 'bootbox'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .print_invoice': 'onClickPrintInvoice'
    'click .pdf_invoice': 'onClickPDFInvoice'
    'click .print_invoice_pechat': 'onClickPrintInvoicePechat'
    'click .pdf_invoice_pechat': 'onClickPDFInvoicePechat'
    'click .delete_invoice': 'onClickDeleteInvoice'

  onClickDeleteInvoice: =>
    event.preventDefault()
    bootbox.confirm "Вы действительно хотите удалить счет?", (result)=>
      if result == true
        @model.destroy().done =>
          backbone.history.navigate "/invoice/", trigger: true

  initialize: (options) =>
    @invoice_id = options.model.id

  onClickPrintInvoice: =>
    url = "/invoices/invoice/" + @invoice_id + "/export/?as=html"
    window.open(url, '_blank')

  onClickPrintInvoicePechat: =>
    url = "/invoices/invoice/" + @invoice_id + "/export/?as=html&print_pechat=1"
    window.open(url, '_blank')

  onClickPDFInvoice: =>
    url = "/invoices/invoice/" + @invoice_id + "/export/?as=pdf"
    window.open(url, '_blank')

  onClickPDFInvoicePechat: =>
    url = "/invoices/invoice/" + @invoice_id + "/export/?as=pdf&print_pechat=1"
    window.open(url, '_blank')

module.exports = LayoutView

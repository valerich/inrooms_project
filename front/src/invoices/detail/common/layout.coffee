_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
helpers = require 'helpers'
bus = require 'bus'
bootbox = require 'bootbox'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
      'click .save_invoice': 'onClickInvoiceSave'
      'click .delete_invoice': 'onClickDeleteInvoice'

  modelEvents:
    'change:kind': 'onChangeKind'

  onClickDeleteInvoice: =>
    event.preventDefault()
    bootbox.confirm "Вы действительно хотите удалить счет?", (result)=>
      if result == true
        @model.destroy().done =>
          backbone.history.navigate "/invoice/", trigger: true

  onClickInvoiceSave: =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
          backbone.history.navigate "/invoice/#{@model.id}/items/", trigger: true
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @invoice_model = options.model
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      'number': '[name=number]'
      'provider': '[name=provider]'
      'customer': '[name=customer]'
      'vat_rate': '[name=vat_rate]'
      'status': '[name=status]'
      'amount_paid': '[name=amount_paid]'
      'comment': '[name=comment]'
      'kind': '[name=kind]'
      'payment_perion_month': '[name=payment_perion_month]'
      'payment_perion_year': '[name=payment_perion_year]'
      'total_price':
         selector: '.total_price'
         converter: helpers.decimal_round_converter

    helpers.initAjaxSelect2 @$('[name=provider]'),
      url: "/api/clients/client/"
      placeholder: 'Выберите поставщика'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true

    helpers.initAjaxSelect2 @$('[name=customer]'),
      url: '/api/clients/client/'
      placeholder: "Выберите получателя"
      minimumInputLength: -1
      allowClear: true
      text_attr: 'name'

    backbone.Validation.bind @

  onChangeKind: =>
    console.log 'onChangeKind'
    if @model.get('kind') == '2'
      @$('#payment_period_block').removeClass('hidden')
    else
      @$('#payment_period_block').addClass('hidden')

  serializeData: =>
    @userCanChangeInvoiceStatus = helpers.user_has_permission bus.cache.user, 'can_change_invoice_status'
    if @userCanChangeInvoiceStatus
      if @model.get('status') == 100
        can_change_amount_paid = false
      else
        can_change_amount_paid = true
    else
      can_change_amount_paid = false

    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanChangeInvoiceStatus'] = @userCanChangeInvoiceStatus
    s_data['user']['userCanChangeInvoiceAmountPaid'] = can_change_amount_paid
    s_data


module.exports = LayoutView

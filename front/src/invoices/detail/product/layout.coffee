_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

InvoiceItemCollection = require '../../collections/invoice_item'
InvoiceItemModel = require '../../models/invoice_item'


class InvoiceItemItemView extends marionette.ItemView
  template: require './templates/invoice_item_item'
  className: 'invoice_items_item'
  events:
    'click .remove': 'onClickRemove'
    'click .save': 'onClickSave'

  onClickSave: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Модель сохранена')
#      bus.vent.trigger 'product_item:update_info'
    .fail (data) ->
      helpers.modelErrorHandler @model, data

  onClickRemove: =>
    bootbox.confirm "Вы действительно хотите удалить модель из счета?", (result)=>
      if result == true
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @invoice_model = options.invoice_model

  onRender: =>
    @binder.bind @model, @$el,
      price:
        selector: '[name=price]'
        converter: helpers.float_converter
      product_name: '[name=product_name]'
      count: '[name=count]'
      unit: '[name=unit]'


class NewInvoiceItemView extends marionette.ItemView
  template: require './templates/new_invoice_item'
  events:
    'click [name=add]': 'onAddBtnClick'

  initialize: (options)=>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      price:
        selector: '[name=price]'
        converter: helpers.float_converter
      product_name: '[name=product_name]'
      count: '[name=count]'

    backbone.Validation.bind @

  onAddBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save()
      .done =>
        helpers.generate_notyfication('success', 'Модель добавлена')
        bus.vent.trigger 'invoice:items:changed'
        @destroy()
      .fail (data) ->
        helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class InvoiceItemCollectionView extends marionette.CollectionView
  childView: InvoiceItemItemView
  className: 'invoice_items_list'

  initialize: (options) =>
    @invoice_model = options.invoice_model

  childViewOptions: (model, index) =>
    data = {
      invoice_model: @invoice_model,
      childIndex: index
    }
    return data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_invoice': 'onClickInvoiceSave'
    'click .item_add': 'onClickItemAdd'
    'click .delete_invoice': 'onClickDeleteInvoice'

  regions:
    region_item_list: '.region_item_list'
    region_item_add: '.region_item_add'

  onClickDeleteInvoice: (event) =>
    event.preventDefault()
    bootbox.confirm "Вы действительно хотите удалить счет?", (result)=>
      if result == true
        @model.destroy().done =>
          backbone.history.navigate "/invoice/", trigger: true

  onClickInvoiceSave: =>
    for item in @invoice_item_collection.models
        item.save().done =>
          helpers.generate_notyfication('success', 'Модель сохранена')
    backbone.history.navigate "/invoice/#{@model.id}/print/", trigger: true

  fetchInvoiceItemCollection: =>
    @invoice_item_collection.fetch(
      reset: true,
      data: {
        page_size: 100000
      }
    )

  initialize: (options) =>
    @invoice_model = options.model
    @listenTo bus.vent, 'product_item:update', @fetchInvoiceItemCollection

  onRender: =>
    @invoice_item_collection = new InvoiceItemCollection
      invoice_id: @invoice_model.id

    @invoice_item_view = new InvoiceItemCollectionView
      collection: @invoice_item_collection
      invoice_model: @invoice_model

    @region_item_list.show(@invoice_item_view)

    @listenTo(bus.vent, 'invoice:items:changed', @fetchInvoiceItemCollection)
    @fetchInvoiceItemCollection()

  onClickItemAdd: =>
    addNewView = new NewInvoiceItemView
      model: new InvoiceItemModel
        invoice_id: @model.id

    bus.vent.trigger 'modal:show', addNewView,
      title: 'Добавить новый товар'


module.exports = LayoutView

backbone = require 'backbone'

HistoryItemModel = require '../models/history_item'


class HistoryItemCollection extends backbone.Collection
  model: HistoryItemModel

  initialize: (options) =>
    @invoice_id = options.invoice_id

  url: =>
    "/api/history/history_item/invoices/invoice/#{@invoice_id}/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = HistoryItemCollection
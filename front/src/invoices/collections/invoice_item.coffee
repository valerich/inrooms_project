backbone = require 'backbone'

InvoiceItemModel = require '../models/invoice_item'


class InvoiceItemCollection extends backbone.Collection
  model: InvoiceItemModel

  initialize: (options) =>
    @invoice_id = options.invoice_id

  url: =>
    "/api/invoices/invoice/#{@invoice_id}/items/"

  _prepareModel: (model, options) =>
      model.invoice_id = @invoice_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = InvoiceItemCollection

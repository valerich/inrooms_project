backbone = require 'backbone'

InvoiceModel = require '../models/invoice'


class InvoiceCollection extends backbone.Collection
  model: InvoiceModel
  url: "/api/invoices/invoice/"

  parse: (response) =>
    @count = response.count
    @total_price_sum = response.total_price_sum
    return response.results

module.exports = InvoiceCollection

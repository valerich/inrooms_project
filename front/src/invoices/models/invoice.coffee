backbone = require 'backbone'


class InvoiceModel extends backbone.Model
  validation:
    provider: [
      {
        required: true,
        msg: 'Заполните поставщика'
      }]
    customer: [
      {
        required: true,
        msg: 'Заполните получателя'
      }]

  url: =>
    if @id
      "/api/invoices/invoice/#{@id}/"
    else
      "/api/invoices/invoice/"

module.exports = InvoiceModel
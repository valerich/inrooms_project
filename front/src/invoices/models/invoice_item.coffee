_ = require 'underscore'
backbone = require 'backbone'


class InvoiceItemModel extends backbone.Model
  defaults:
    count: 1
  validation:
    count: [
      {
        required: true,
        msg: 'Заполните количество'
      }, {
        pattern: 'digits',
        msg: 'Укажите корректное количество'
      }, {
        range: [1, 1000000000]
        msg: 'Количество не может быть меньше 1 и больше 1 000 000 000'
      }]
    price: [
      {
        required: true,
        msg: 'Заполните цену'
      }]

  initialize: (options) =>
    @invoice_id = options.invoice_id

  url: =>
    if @id
      "/api/invoices/invoice/#{@invoice_id}/items/#{@id}/"
    else
      "/api/invoices/invoice/#{@invoice_id}/items/"


module.exports = InvoiceItemModel


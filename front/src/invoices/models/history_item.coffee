_ = require 'underscore'
backbone = require 'backbone'


class HisotoryItemModel extends backbone.Model

  initialize: (options) =>
    @invoice_id = options.invoice_id

  url: =>
    if @id
      "/api/history/history_item/invoices/invoice/#{@invoice_id}/#{@id}/"
    else
      "/api/history/history_item/invoices/invoice/#{@invoice_id}/"


module.exports = HisotoryItemModel

_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

OrderItemCollection = require '../../collections/manufacturing_order_item'
OrderItemModel = require '../../models/manufacturing_order_item'


class OrderItemItemView extends marionette.ItemView
  template: require './templates/orderitem_item'
  className: 'order_items_item'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @order_model = options.order_model

  onRender: =>
    @setSeriesCount()
    @binder.bind @model, @$el,
      price: '[name=price]'
      ordered: '[name=ordered]'
      ordered_xxs: '[name=ordered_xxs]'
      ordered_xs: '[name=ordered_xs]'
      ordered_s: '[name=ordered_s]'
      ordered_m: '[name=ordered_m]'
      ordered_l: '[name=ordered_l]'
      ordered_xl: '[name=ordered_xl]'
      ordered_xxl: '[name=ordered_xxl]'
      ordered_xxxl: '[name=ordered_xxxl]'
      received: '[name=received]'
      received_xxs: '[name=received_xxs]'
      received_xs: '[name=received_xs]'
      received_s: '[name=received_s]'
      received_m: '[name=received_m]'
      received_l: '[name=received_l]'
      received_xl: '[name=received_xl]'
      received_xxl: '[name=received_xxl]'
      received_xxxl: '[name=received_xxxl]'
      ordered_st_sum: '.ordered_st_sum'
      received_st_sum: '.received_st_sum'

  setSeriesCount: =>
    ordered_st_sum = parseInt(@model.get("ordered"))
    received_st_sum = parseInt(@model.get("received"))
    for size in [
              'xxs',
              'xs',
              's',
              'm',
              'l',
              'xl',
              'xxl',
              'xxxl',
    ]
      ordered_st_sum = ordered_st_sum + parseInt(@model.get("ordered_#{size}"))
      count_received_item = parseInt(@model.get("received_#{size}"))
      if count_received_item
        received_st_sum = received_st_sum + count_received_item
    @model.set("ordered_st_sum", ordered_st_sum)
    @model.set("received_st_sum", received_st_sum)



class OrderItemCollectionView extends marionette.CollectionView
  childView: OrderItemItemView
  className: 'order_items_list'

  initialize: (options) =>
    @order_model = options.order_model

  childViewOptions: (model, index) =>
    data = {
      order_model: @order_model,
      childIndex: index
    }
    return data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  regions:
    region_item_list: '.region_item_list'

  fetchOrderItemCollection: =>
    @orderitem_collection.fetch
      reset: true
      data: { page_size: 10000}

  initialize: (options) =>
    @order_model = options.model

  onRender: =>
    @orderitem_collection = new OrderItemCollection
      order_id: @order_model.id

    @orderitem_view = new OrderItemCollectionView
      collection: @orderitem_collection
      order_model: @order_model

    @region_item_list.show(@orderitem_view)

    @orderitem_collection.fetch
      reset: true
      data: { page_size: 10000}


module.exports = LayoutView

$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'

ManufacturingOrderCollection = require '../collections/manufacturing_order'
ManufacturingOrderModel = require '../models/manufacturing_order'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onClickAdd'

  onClickAdd: =>
    bootbox.confirm "Вы действительно хотите добавить заказ?", (result)=>
      if result == true
        model = new ManufacturingOrderModel
          kind: @order_kind
        model.save().done =>
          backbone.history.navigate "/manufacturing_label_order/#{model.get('id')}/", {trigger: true}
          helpers.generate_notyfication('success', 'Заказ создан')

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @order_kind = options.order_kind

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      status: '[name=status]'
      items__product_label: '[name=items__product_label]'
      factory: '[name=factory]'
      sort_by: '.sort_list'

    helpers.initAjaxSelect2 @$('[name=status]'),
      url: "/api/manufacturing/manufacturing_status/"
      placeholder: 'Выберите статус'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true
    helpers.initAjaxSelect2 @$('[name=items__product_label]'),
      url: '/api/products/product_label/'
      placeholder: "Выберите этикетку"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"
    helpers.initAjaxSelect2 @$('[name=factory]'),
      url: '/api/factories/factory/'
      placeholder: "Выберите фабрику"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"


class ManufacturingOrderComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class ManufacturingOrder2ComplexItemView extends complex.ComplexItemView
  template: require './templates/item2'
  triggers:
    'click .panel': 'click'


class ManufacturingOrderComplexListView extends complex.ComplexCompositeView
  childView: ManufacturingOrderComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class ManufacturingOrder2ComplexListView extends complex.ComplexCompositeView
  childView: ManufacturingOrder2ComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items2'


class ManufacturingOrderComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'
  events:
    'click .list_type': 'onClickListType'

  initialize: (options) =>
    @listenTo(bus.vent, 'order:added', @onOrderAdded)

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    @collection = new ManufacturingOrderCollection

    @filter_model = new FilterModel(urlParams)
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @filter_view = new FilterView
      model: @filter_model

  onOrderAdded: =>
    @complex_view.doFilter()

  onClickListType: (event) =>
    $target = $(event.target)
    list_type = $target.data('list_type')
    @renderOrderList(list_type)

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'manufacturing_label_order'
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @region_filter.show(@filter_view)
#    @region_widgets.show(@widgets_view)

    list_type = store.get 'order_list_type'
    if !list_type
      list_type = 'list1'
    @renderOrderList(list_type)

  renderOrderList: (list_type) =>
    store.set 'order_list_type', list_type
    if list_type is 'list1'
      ManufacturingOrderView = ManufacturingOrderComplexListView
    if list_type is 'list2'
      ManufacturingOrderView = ManufacturingOrder2ComplexListView
    @$(".list_type").removeClass('active')
    @$(".list_type[data-list_type=#{list_type}]").addClass('active')

    list_view = new ManufacturingOrderView
      collection: @collection

    @complex_view = new ManufacturingOrderComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'orders'

    @region_complex.show(@complex_view)

    @listenTo @complex_view, 'childview:click', (view, options) =>
      backbone.history.navigate "/manufacturing_label_order/#{view.model.id}/", {trigger: true}

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @complex_view.doFilter()

  serializeData: =>
    serialized_data = super
    serialized_data['list_name'] = 'Заказы этикеток'
    serialized_data

module.exports = LayoutView
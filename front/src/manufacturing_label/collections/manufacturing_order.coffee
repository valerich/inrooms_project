backbone = require 'backbone'

ManufacturingOrderModel = require '../models/manufacturing_order'


class ManufacturingOrderCollection extends backbone.Collection
  model: ManufacturingOrderModel
  url: "/api/manufacturing/manufacturing_label_order/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ManufacturingOrderCollection
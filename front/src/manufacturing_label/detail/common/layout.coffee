_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bootbox = require 'bootbox'
store = require 'store'
helpers = require 'helpers'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .change_status': 'onClickChangeStatus'

  initialize: =>
    @model.fetch()

  changeOrderStatus: (status_code) =>
    $.ajax
      url: "/api/manufacturing/manufacturing_label_order/#{@model.id}/change_status/"
      type: 'post'
      data: {
        'code': status_code
      }
      success: =>
        helpers.generate_notyfication('success', 'Статус изменен')
        backbone.history.navigate "/manufacturing_label_order/", trigger: true
      error: (data) =>
        helpers.modelErrorHandler @model, data

  onClickChangeStatus: (event) =>
    $target = $(event.target)
    new_status_code = $target.data('status_code')
    received_failed = false
    if new_status_code == 'received' and !@model.get('is_good_received')
      if !@model.get('has_products_received')
        message = "Вы не получили ни одного товара"
        bootbox.alert message, =>
          backbone.history.navigate "/manufacturing_label_order/#{@model.id}/items/", trigger: true
        return
      else
        received_failed = true
        message = "Количество заказанного товара и полученного отличается. Изменить статус заказа?"
    else
      message = "Вы действительно хотите изменить статус заказа?"
    bootbox.confirm message, (result)=>
      if result == true
        $target = $(event.target)
        @changeOrderStatus(new_status_code)
      if result == false
        if received_failed == true
          backbone.history.navigate "/manufacturing_label_order/#{@model.id}/items/", trigger: true


module.exports = LayoutView

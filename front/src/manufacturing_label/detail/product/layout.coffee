_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

OrderItemCollection = require '../../collections/manufacturing_order_item'
OrderItemModel = require '../../models/manufacturing_order_item'
product_select2_item_template = require '../../../product_labels/templates/select2_item'


class OrderItemItemView extends marionette.ItemView
  template: require './templates/orderitem_item'
  className: 'order_items_item'
  events:
    'click .remove': 'onClickRemove'
    'click .save': 'onClickSave'
  modelEvents:
    'change': 'setSeriesCount'

  onClickSave: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Cохранено')
    .fail (data) ->
      helpers.modelErrorHandler @model, data

  onClickRemove: =>
    bootbox.confirm "Вы действительно хотите удалить этикетку из корзины?", (result)=>
      if result == true
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @order_model = options.order_model

  onRender: =>
    @setSeriesCount()
    @binder.bind @model, @$el,
      price: '[name=price]'
      ordered: '[name=ordered]'
      ordered_xxs: '[name=ordered_xxs]'
      ordered_xs: '[name=ordered_xs]'
      ordered_s: '[name=ordered_s]'
      ordered_m: '[name=ordered_m]'
      ordered_l: '[name=ordered_l]'
      ordered_xl: '[name=ordered_xl]'
      ordered_xxl: '[name=ordered_xxl]'
      ordered_xxxl: '[name=ordered_xxxl]'
      received: '[name=received]'
      received_xxs: '[name=received_xxs]'
      received_xs: '[name=received_xs]'
      received_s: '[name=received_s]'
      received_m: '[name=received_m]'
      received_l: '[name=received_l]'
      received_xl: '[name=received_xl]'
      received_xxl: '[name=received_xxl]'
      received_xxxl: '[name=received_xxxl]'
      ordered_st_sum: '.ordered_st_sum'
      received_st_sum: '.received_st_sum'

  setSeriesCount: =>
    ordered_st_sum = parseInt(@model.get("ordered"))
    received_st_sum = parseInt(@model.get("received"))
    for size in [
              'xxs',
              'xs',
              's',
              'm',
              'l',
              'xl',
              'xxl',
              'xxxl',
    ]
      ordered_st_sum = ordered_st_sum + parseInt(@model.get("ordered_#{size}"))
      count_received_item = parseInt(@model.get("received_#{size}"))
      if count_received_item
        received_st_sum = received_st_sum + count_received_item
    @model.set("ordered_st_sum", ordered_st_sum)
    @model.set("received_st_sum", received_st_sum)

  serializeData: =>
    result = super
    if @order_model.get('status_detail')['code'] == 'new'
      result['canEditProducts'] = true
    else
      result['canEditProducts'] = false
    if @order_model.get('status_detail')['code'] == 'sent_to_factory'
      result['canEditReceived'] = true
    else
      result['canEditReceived'] = false
    result


class NewOrderItemView extends marionette.ItemView
  template: require './templates/new_orderitem'
  events:
    'click [name=add]': 'onAddBtnClick'

  initialize: (options)=>
    @binder = new modelbinder
    @order_kind = options.order_kind

  onRender: =>
    @binder.bind @model, @$el,
      product_label: '[name=product_label]'
    helpers.initAjaxSelect2 @$('[name=product_label]'),
      url: '/api/products/product_label/'
      placeholder: 'Введите название этикетки'
      minimumInputLength: -1
      text_attr: (obj) =>
        "№:#{obj.id}. #{obj.name}"
      formatResult: (product) =>
        product_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    backbone.Validation.bind @

  onAddBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save()
      .done =>
        helpers.generate_notyfication('success', 'Модель добавлена')
        bus.vent.trigger 'order:items:changed'
      .fail (data) ->
        helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class OrderItemCollectionView extends marionette.CollectionView
  childView: OrderItemItemView
  className: 'order_items_list'

  initialize: (options) =>
    @order_model = options.order_model

  childViewOptions: (model, index) =>
    data = {
      order_model: @order_model,
      childIndex: index
    }
    return data



class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_order': 'onClickOrderSave'

  regions:
    region_item_list: '.region_item_list'
    region_item_add: '.region_item_add'

  onClickOrderSave: =>
    for item in @orderitem_collection.models
      if item.changedAttributes()
        item.save().done =>
          helpers.generate_notyfication('success', 'Этикетка сохранена')
    backbone.history.navigate "/manufacturing_label_order/#{@model.id}/payment/", trigger: true

  serializeData: =>
    result = super

    if @order_model.get('status_detail')['code'] == 'new'
      result['canEditProducts'] = true
    else
      result['canEditProducts'] = false
    if @order_model.get('status_detail')['code'] == 'sent_to_factory'
      result['canEditReceived'] = true
    else
      result['canEditReceived'] = false
    return result

  fetchOrderItemCollection: =>
    @orderitem_collection.fetch
      reset: true

  initialize: (options) =>
    @order_model = options.model
    @listenTo(bus.vent, 'product_item:update', @fetchOrderItemCollection)

  addOrderItemForm: =>
    if @new_orderitem_view
      @new_orderitem_view.destroy()
    @new_orderitem_view = new NewOrderItemView
      model: new OrderItemModel
        order_id: @order_model.id
        order: @order_model.id
      order_kind: @order_model.get('kind')
    @region_item_add.show(@new_orderitem_view)

  onRender: =>
    @orderitem_collection = new OrderItemCollection
      order_id: @order_model.id

    @orderitem_view = new OrderItemCollectionView
      collection: @orderitem_collection
      order_model: @order_model

    @region_item_list.show(@orderitem_view)

    @listenTo(bus.vent, 'order:items:changed', @fetchOrderItemCollection)
    @listenTo(bus.vent, 'order:items:changed', @addOrderItemForm)
    @fetchOrderItemCollection()
    @addOrderItemForm()


module.exports = LayoutView

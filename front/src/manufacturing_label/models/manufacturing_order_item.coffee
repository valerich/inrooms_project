_ = require 'underscore'
backbone = require 'backbone'


class ManufacturingOrderItemModel extends backbone.Model
  defaults:
    quantity: 1
  validation:
    quantity: [
      {
        required: true,
        msg: 'Заполните количество'
      }, {
        pattern: 'digits',
        msg: 'Укажите корректное количество'
      }, {
        range: [1, 1000000000]
        msg: 'Количество не может быть меньше 1 и больше 1 000 000 000'
      }]
    product_label: [
      {
        required: true,
        msg: 'Заполните этикетку'
      }
    ]

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    if @id
      "/api/manufacturing/manufacturing_label_order/#{@order_id}/items/#{@id}/"
    else
      "/api/manufacturing/manufacturing_label_order/#{@order_id}/items/"


module.exports = ManufacturingOrderItemModel

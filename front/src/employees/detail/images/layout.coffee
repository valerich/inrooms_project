$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'
complex = require 'complex'
helpers = require 'helpers'
require 'blueimp-file-upload'
bootbox = require 'bootbox'
require 'magnific-popup'

EmployeeImageCollection = require '../../collections/employee_image'


class EmployeeImageComplexItemView extends complex.ComplexItemView
  template: require './templates/file_item'

  triggers:
    'click .image-link': 'click'

  events:
    'click .image-delete': 'onClickDelete'

  initialize: (options) =>
    @employee_model = options.employee_model

  onClickDelete: =>
    bootbox.confirm "Вы действительно хотите удалить изображение?", (result)=>
      if result == true
        $.ajax
          url: "/api/stores/employee/image_delete/"
          data:
            "file_id": @model.id
          type: 'post'
          success: (data) =>
            helpers.generate_notyfication('success', 'Удалено')
            bus.vent.trigger 'file:delete'
          error: (data) =>
            helpers.modelErrorHandler @model, data


class EmployeeImageComplexListView extends complex.ComplexCompositeView
  childView: EmployeeImageComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/file_items'

  initialize: (options) =>
    @employee_model = options.employee_model

  childViewOptions: (model, index) =>
    data = {
      employee_model: @employee_model,
      childIndex: index
    }
    return data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.files'

  events:
    'click .add': 'onAddClick'

  initialize: (options) =>
    @employee_model = options.model
    @collection = new EmployeeImageCollection
      employee_id: @employee_model.id
    @list_view = new EmployeeImageComplexListView
      collection: @collection
      employee_model: @employee_model

  onRender: =>
    @region_complex.show(@list_view)
    @collection.fetch()

    @listenTo(bus.vent, 'file:delete', @onDeleteFile)

    @listenTo @list_view, 'childview:click', (view)=>
      $.magnificPopup.open
        type:'image'
        mainClass: 'mfp-fade'
        items: [{'src': view.model.get('image')}]
        midClick: true
#      win = window.open(view.model.get('image'), '_blank')
#      win.focus()

  onDeleteFile: =>
    @collection.fetch()

  onAddClick: =>
    @$('.fileupload').fileupload
      url: "/api/stores/employee/#{@employee_model.id}/image_upload/"
      dataType: 'json'
      submit: (e, data) =>
        $('#music_add_btn').removeClass('fa-plus-circle')
        $('.add').removeClass('btn-primary')
        $('.add').addClass('btn-default')
        $('#music_add_btn').disabled = true
        $('#music_add_btn').addClass('fa-spinner')
      always: (e, data) =>
        $('#music_add_btn').addClass('fa-plus-circle')
        $('.add').addClass('btn-primary')
        $('.add').removeClass('btn-default')
        $('#music_add_btn').disabled = false
        $('#music_add_btn').removeClass('fa-spinner')
      done: (e, response) =>
        @collection.fetch()
      fail: (e, response) =>
        helpers.generate_notyfication('error', 'не поддерживаемый тип файла')


module.exports = LayoutView
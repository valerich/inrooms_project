$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'
complex = require 'complex'
helpers = require 'helpers'


class ItemView extends marionette.ItemView
  template: require './templates/item'
  translated_fields:
    first_name: 'Имя'
    middle_name: 'Отчество'
    last_name: 'Фамилия'
    email: 'email'
    phone: 'Телефон'
    store: 'Магазин'
    date_birthday: 'Дата рождения'
    date_employment: 'Дата приема на работу'
    position: 'Должность'
    is_working: 'Работает?'

  initialize: =>
    @binder = new modelbinder
    @can_employees_description_view = helpers.user_has_permission bus.cache.user, 'can_employees_description_view'

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    if @can_employees_description_view
      @binder.bind @model, @$el,
        first_name: '[name=first_name]'
        middle_name: '[name=middle_name]'
        last_name: '[name=last_name]'
        email: '[name=email]'
        phone: '[name=phone]'
        store: '[name=store]'
        date_birthday: '[name=date_birthday]'
        date_employment: '[name=date_employment]'
        position: '[name=position]'
        is_working: '[name=is_working]'
        description: '[name=description]'
    else
      @binder.bind @model, @$el,
        first_name: '[name=first_name]'
        middle_name: '[name=middle_name]'
        last_name: '[name=last_name]'
        email: '[name=email]'
        phone: '[name=phone]'
        store: '[name=store]'
        date_birthday: '[name=date_birthday]'
        date_employment: '[name=date_employment]'
        position: '[name=position]'
        is_working: '[name=is_working]'

    helpers.initAjaxSelect2 @$('[name=store]'),
      url: '/api/stores/store/'
      placeholder: "Выберите магизин"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "#{obj.name} (#{obj.client_detail.name})"

    backbone.Validation.bind @

    @$('[name=date_birthday]').datepicker
      autoclose: true

    @$('[name=date_employment]').datepicker
      autoclose: true

  onClickSave: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  serializeData: =>
    s_data = super
    s_data['can_employees_description_view'] = @can_employees_description_view
    s_data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'

  initialize: (options)=>
    @user = options.model

    @item_view = new ItemView
      model: @user

  onRender: =>
    @region_item.show(@item_view)


module.exports = LayoutView
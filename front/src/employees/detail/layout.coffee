marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

CommonLayout = require './common/layout'
EmployeeImageLayout = require './images/layout'
EmployeeFileLayout = require './employee_file/layout'

EmployeeModel = require '../models/employee'


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'client_detail_layout'
  regions:
    region_common: '.region_common'
    region_photo: '.region_photo'
    region_file: '.region_file'
  events:
    'click .list_link': 'onClickListLink'
    'click .save': 'onClickSave'

  onClickListLink: (event) =>
    event.preventDefault()
    backbone.history.navigate '/employee/', trigger: true

  onClickSave: (event) =>
    @common_view.item_view.onClickSave(event)

  initialize: (options) =>
    @binder = new modelbinder
    @model = new EmployeeModel
      id: options.id

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      first_name: '.breadcrumb_name'
    bus.vent.trigger 'menu:active:set', null, 'employee'
    @model.fetch().done =>
      @common_view = new CommonLayout
        model: @model
      @photo_view = new EmployeeImageLayout
        model: @model
      @file_view = new EmployeeFileLayout
        model: @model
      @region_common.show(@common_view)
      @region_photo.show(@photo_view)
      @region_file.show(@file_view)

module.exports = ObjectDetailLayout

$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

EmployeeCollection = require '../collections/employee'
EmployeeModel = require '../models/employee'


class FilterModel extends complex.FilterModel
  defaults:
    page: 1
    page_size: '10'
    page_count: 1
    is_working: "2"


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onAddClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onAddClick: =>
    addNewView = new NewEmployeeView
      model: new EmployeeModel

    bus.vent.trigger 'modal:show', addNewView,
      title: 'Добавить нового сотрудника'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      store: '[name=store]'
      store__client: '[name=store__client]'
      is_working: '[name=is_working]'
      sort_by: '.sort_list'

    helpers.initAjaxSelect2 @$('[name=store]'),
      url: '/api/stores/store/'
      placeholder: "Выберите магизин"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "#{obj.name} (#{obj.client_detail.name})"

    helpers.initAjaxSelect2 @$('[name=store__client]'),
      url: '/api/clients/client/'
      placeholder: "Выберите клиента"
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1


class EmployeeComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class EmployeeComplexListView extends complex.ComplexCompositeView
  childView: EmployeeComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class EmployeeComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class NewEmployeeView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    first_name: 'Имя'
    middle_name: 'Отчество'
    last_name: 'Фамилия'
    store: 'Магазин'

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      first_name: '[name=first_name]'
      middle_name: '[name=middle_name]'
      last_name: '[name=last_name]'
      store: "[name=store]"

    helpers.initAjaxSelect2 @$('[name=store]'),
      url: '/api/stores/store/'
      placeholder: "Выберите магазин"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "#{obj.name} (#{obj.client_detail.name})"

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сотрудник создан')
          @destroy()
          backbone.history.navigate "/employee/#{model.id}/", trigger: true
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @collection = new EmployeeCollection
    @filter_model = new FilterModel
    @list_view = new EmployeeComplexListView
      collection: @collection

    @complex_view = new EmployeeComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'employees'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'employee'
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

    @listenTo @complex_view.list_view, 'childview:click', (view)=>
      backbone.history.navigate "/employee/#{view.model.id}/", trigger: true


module.exports = LayoutView
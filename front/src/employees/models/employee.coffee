backbone = require 'backbone'


class EmployeeModel extends backbone.Model

  url: =>
    if @id
      "/api/stores/employee/#{@id}/"
    else
      "/api/stores/employee/"

  validation:
      first_name: [
        {
          required: true,
          msg: 'Заполните имя'
        }]
      store: [
        {
          required: true,
          msg: 'Заполните магазин'
        }]

module.exports = EmployeeModel
backbone = require 'backbone'

EmployeeImageModel = require '../models/employee_image'


class EmployeeImageCollection extends backbone.Collection
  model: EmployeeImageModel

  initialize: (options) =>
    @employee_id = options.employee_id

  url: =>
    "/api/stores/employee/#{@employee_id}/images/"

module.exports = EmployeeImageCollection
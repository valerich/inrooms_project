backbone = require 'backbone'

EmployeeFileModel = require '../models/employee_file'


class EmployeeFileCollection extends backbone.Collection
  model: EmployeeFileModel

  initialize: (options) =>
    @employee_id = options.employee_id

  url: =>
    "/api/stores/employee/#{@employee_id}/files/"

#  parse: (response) =>
#    @count = response.count
#    return response.results

module.exports = EmployeeFileCollection
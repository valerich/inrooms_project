$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'
require 'jquery.inputmask'


class OfferEditView extends marionette.ItemView
  template: require './templates/offer_edit_modal'
  events:
    'click .save': 'onClickSave'

  translated_fields:
    name: 'Название'
    aggreement_code: 'Код'
    aggreement_price: 'Цена'

  onClickSave: =>
    @model.save {},
      success: (model) =>
        helpers.generate_notyfication('success', 'Модель сохранена')
        bus.vent.trigger 'offer_aggreement:update', @model
        @destroy()
      error: (model, response, options) =>
        console.log 'model save error'
        helpers.modelErrorHandler model, response, options, @translated_fields

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'
      aggreement_code: '[name=aggreement_code]'
      aggreement_price: '[name=aggreement_price]'
      is_manufacturing_aggreement: '[name=is_manufacturing_aggreement]',
      business_name: '[name=business_name]',
      catalog_description: '[name=catalog_description]',

    @$('[name=aggreement_code]').inputmask("9999/999")

    if @model.get('main_collection_detail')
      series_type = @model.get('main_collection_detail')['series_type']
    else
      series_type = ''
    @$('[name=aggreement_sizes]').select2
      multiple: true
      placeholder: "Выберите группу"
      ajax:
        url: '/api/products/size/'
        dataType: 'json'
        quietMillis: 250
        cache: true
        data: (term, page) ->
          return {
            series_type: series_type
            search: term
            page_size: 100
          }
        results: (data)->
          results = data.results
          data = _.map(results, (x) -> x.text = x.name)
          return {
            results: results
          }
    @$('[name=aggreement_sizes]').on 'change', =>
      value = @$('[name=aggreement_sizes]').val()
      result = _.map(value.split(','), (x) -> parseInt(x))
      if result.length and result[0]
        @model.set 'aggreement_sizes', result
      else
        @model.set 'aggreement_sizes', []

    aggreement_sizes_detail = @model.get 'aggreement_sizes_detail'
    for item in aggreement_sizes_detail
      item.text = item.name
    @$('[name=aggreement_sizes]').select2 'data', aggreement_sizes_detail

    @$('[name=aggreement_colors]').select2
      multiple: true
      placeholder: "Выберите цвета"
      ajax:
        url: '/api/products/color/'
        dataType: 'json'
        quietMillis: 250
        cache: true
        data: (term, page) ->
          return {
            search: term
            page: page
          }
        results: (data)->
          results = data.results
          console.log('results', results)
          data = _.map(results, (x) -> x.text = x.name)
          return {
            results: results
          }
      createSearchChoicePosition: 'bottom'
      createSearchChoice: (term) =>
        return {
          id: term + '_4create'
          text: term + ' (создать)'
        }

    @$('[name=aggreement_colors]').on 'change', (event)=>
      url = "/api/products/color/"
      if event.added
        if /_4create/.test(event.added.id.toString())
          $.ajax
            url: url
            type: "post"
            data:
              name: event.added.id.replace('_4create', '')
            success: (data) =>
              values = @$('[name=aggreement_colors]').val().split(',')
              values.pop()
              if data.id not in values
                values.push(data.id)
              @$('[name=aggreement_colors]').val(values.join(",")).change()
              helpers.generate_notyfication('success', 'Значение создано')

    @$('[name=aggreement_colors]').on 'change', =>
      value = @$('[name=aggreement_colors]').val()
      result = _.map(value.split(','), (x) -> parseInt(x))
      if result.length and result[0]
        @model.set 'aggreement_colors', result
      else
        @model.set 'aggreement_colors', []

    aggreement_colors_detail = @model.get 'aggreement_colors_detail'
    for item in aggreement_colors_detail
      item.text = item.name
    @$('[name=aggreement_colors]').select2 'data', aggreement_colors_detail


module.exports = OfferEditView
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'
require 'jquery.inputmask'

OfferCollection = require '../collections/offer'
OfferCartItemCollection = require '../collections/offer_cart_item'
OfferEditView = require './offer_edit'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  regions:
    'region_cart': '.region-cart'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      sort_by: '.sort_list'
      search: '[name=search]'


class CartComplexItemView extends complex.ComplexItemView
  template: require './templates/cart_item'
  events:
    'click .cart-item-delete': 'onClickDelete'

  onClickDelete: =>
    $.ajax
      url: "/api/offers/aggreement-offer-cart/items/delete/"
      type: 'post'
      data: {
        'item_id': @model.id
      }
      success: =>
        bus.vent.trigger 'cart:item:deleted'
        helpers.generate_notyfication('success', 'Удалено из корзины')
      error: (data) =>
        helpers.generate_notyfication('error', data.responseJSON["error"])


class CartComplexListView extends complex.ComplexCompositeView
  childView: CartComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/cart_items'


class CartComplexView extends marionette.LayoutView
  template: require './templates/cart_complex_layout'
  regions:
    'region_list': '.cart_region_list'
  className: 'cart_complex_view'

  events:
    'click .btn-go-to-cart': 'onClickGoToCart'

  onClickGoToCart: =>
    backbone.history.navigate '/aggreement-offer/cart/', trigger: true

  reloadCart: =>
    @collection.fetch(
      reset: true
      data:
        '_fields': 'id,name,article,color_detail',
        'sort_by': '-id'
        'page_size': 3
    ).done =>
      @$('.offer_request_quantity').html(@collection.quantity)
      @$('.offer_request_amount_full').html(@collection.amount_full)

  initialize: (options)=>
    @name = options.name
    @collection = options.collection || throw 'specify collection of complex view'
    @list_view = new CartComplexListView
      collection: @collection
      name: 'cart'

  onRender: =>
    @region_list.show(@list_view)
    @listenTo(bus.vent, 'cart:item:added', @reloadCart)
    @listenTo(bus.vent, 'cart:item:deleted', @reloadCart)
    @reloadCart()
    @listenTo @collection, 'sync', =>
      if @collection.length > 0
        @$('.cart_link').removeClass("hidden")
        @$('.empty_list_text').addClass("hidden")
      else
        @$('.cart_link').addClass("hidden")
        @$('.empty_list_text').removeClass("hidden")


class OfferComplexItemView extends complex.ComplexItemView
  template: require './templates/item'
  events:
    'click .product-to-cart': 'onClickToCart'
    'click .edit-offer': 'onClickOfferEdit'

  onRender: =>
    @listenTo(bus.vent, 'offer_aggreement:update', @onOfferUpdate)

  onOfferUpdate: (model) =>
    if model.id == @model.id
      @model.fetch()
        .done =>
          @render()

  onClickOfferEdit: =>
    editOfferView = new OfferEditView
      model: @model

    bus.vent.trigger 'modal:show', editOfferView,
      title: 'Редактирование предложения'

  onClickToCart: (event) =>
    $target = $(event.target)
    color_id = $target.data('color_id')
    $.ajax
      url: "/api/offers/aggreement-offer-cart/items/add/"
      type: 'post'
      data: {
        'offer_id': @model.id,
        'color_id': color_id
      }
      success: =>
        bus.vent.trigger 'cart:item:added'
        helpers.generate_notyfication('success', 'Добавлено в корзину')
      error: (data) =>
        helpers.generate_notyfication('error', data.responseJSON["error"])

  serializeData: =>
    @userCanEditOffer = helpers.user_has_permission bus.cache.user, 'can_edit_offer_aggreement'
    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanEditOffer'] = @userCanEditOffer
    s_data


class OfferComplexListView extends complex.ComplexCompositeView
  childView: OfferComplexItemView
  childViewContainer: ".region_shop_product_panel_items"
  template: require './templates/items'


class OfferComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2
  regions:
    'region_search': '.region_search'
    'region_list': '.region_list'
    'region_paginator': '.region_paginator'
    'region_sidebar': '.region_sidebar'

  onRender: =>
    @cart_view = new CartComplexView
      collection: new OfferCartItemCollection
    super
    @region_sidebar.show(@cart_view)

    @sidebar_obj = @$('#fixed_sidebar')
    @offset = @sidebar_obj.offset()
    @topOffset = @offset.top
    @topPadding = 200

    $(window).bind('scroll', (ev) =>
      @scrollSidebar()
    )

  scrollSidebar: =>
    if $(window).scrollTop() > @topPadding
      @sidebar_obj.stop().animate({marginTop: $(window).scrollTop() - @topOffset - @topPadding}, 200)
    else
      @sidebar_obj.stop().animate({marginTop: 0}, 200)

  doFilter: =>
    if @blockFilter
      return
    @blockFilter = true

    setTimeout =>
      if @blockFilter
        @showSpinner()
    , 1000

    if @urlFilterParam
      url = window.location.pathname + '?' + $.param(@filter_model.toJSON())
      backbone.history.navigate url, trigger: false

    filter_data = @filter_model.getFilterData()

    @collection.fetch
      data: filter_data
      reset: true
      error: (collection, response) =>
        if response.status is 404 and @filter_model.get('page') > 2
          @blockFilter = false
          @filter_model.set 'page', 1
      complete: =>
        @blockFilter = false
        @hideSpinner()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    @collection = new OfferCollection

    @filter_model = new FilterModel(urlParams)
    data.reqres.setHandler 'filter_model', =>
      @filter_model
    @filter_model.set('to_aggreement', "2")

    @filter_view = new FilterView
      model: @filter_model

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'aggreement-offer'
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @region_filter.show(@filter_view)
    @renderSupplyList()

  renderSupplyList: () =>
    list_view = new OfferComplexListView
      collection: @collection

    @complex_view = new OfferComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'supply'

    @region_complex.show(@complex_view)

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @complex_view.doFilter()

module.exports = LayoutView
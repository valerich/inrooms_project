backbone = require 'backbone'


class OfferModel extends backbone.Model

  initialize: (options) =>
    @is_edit = options.is_edit

  url: =>
    if @is_edit
      url_part = "/api/offers/aggreement-offer-edit/"
    else
      url_part = "/api/offers/aggreement-offer/"

    if @id
      "#{url_part}#{@id}/"
    else
      "#{url_part}"

module.exports = OfferModel
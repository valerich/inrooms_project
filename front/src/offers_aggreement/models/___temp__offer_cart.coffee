backbone = require 'backbone'


class OfferCartModel extends backbone.Model

  url: =>
    if @id
      "/api/offers/aggreement-offer-cart/#{@id}/"
    else
      "/api/offers/aggreement-offer-cart/"

module.exports = OfferCartModel
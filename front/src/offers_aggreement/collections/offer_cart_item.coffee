backbone = require 'backbone'

CartItemModel = require '../models/offer_cart_item'


class CartItemCollection extends backbone.Collection
  model: CartItemModel

  url: =>
    "/api/offers/aggreement-offer-cart/items/"

  parse: (response) =>
    @quantity = response.quantity
    @amount_full = response.amount_full
    return response.results

module.exports = CartItemCollection
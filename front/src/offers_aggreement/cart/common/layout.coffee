_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bootbox = require 'bootbox'
store = require 'store'
helpers = require 'helpers'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .create': 'onClickCreate'

  initialize: (options) =>
    @model.fetch()
    @cart_id = options.cart_id

  onClickCreate: =>
    $.ajax
      url: "/api/offers/aggreement-offer-cart/create_order/"
      type: 'post'
      success: =>
        helpers.generate_notyfication('success', 'Заявка сформирована')
        backbone.history.navigate "/aggreement-offer/", trigger: true
      error: (data) =>
        helpers.modelErrorHandler @model, data

module.exports = LayoutView

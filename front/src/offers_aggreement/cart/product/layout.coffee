_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

CartItemCollection = require '../../collections/offer_cart_item'
CartItemModel = require '../../models/offer'


class CartItemItemView extends marionette.ItemView
  template: require './templates/cartitem_item'
  className: 'cart_items_item'
  events:
    'click .remove': 'onClickRemove'

  onClickRemove: =>
    bootbox.confirm "Вы действительно хотите удалить предложение из корзины?", (result)=>
      if result == true
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')
          bus.vent.trigger 'cart:items:changed'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder

  onRender: =>
    null


class CartItemCollectionView extends marionette.CollectionView
  childView: CartItemItemView
  className: 'cart_items_list'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .tab_alert>button': 'closeAlert'
    'click .save_cart': 'onClickCartSave'

  regions:
    region_item_list: '.region_item_list'
    region_item_add: '.region_item_add'

  onClickCartSave: =>
    backbone.history.navigate "/aggreement-offer/cart/common/", trigger: true

  fetchCartItemCollection: =>
    @cartitem_collection.fetch(
      data: {
        'page_count': 10000
      }
      reset: true
    ).done =>
      @$('.offer_request_quantity').html(@cartitem_collection.quantity)
      @$('.offer_request_amount_full').html(@cartitem_collection.amount_full)

  initialize: (options) =>
    @listenTo(bus.vent, 'product_item:update', @fetchCartItemCollection)

  onRender: =>
    @cartitem_collection = new CartItemCollection

    @cartitem_view = new CartItemCollectionView
      collection: @cartitem_collection

    @region_item_list.show(@cartitem_view)

    @listenTo(bus.vent, 'cart:items:changed', @fetchCartItemCollection)
    @fetchCartItemCollection()


module.exports = LayoutView

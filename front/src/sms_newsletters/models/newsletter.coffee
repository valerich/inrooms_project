backbone = require 'backbone'


class NewsletterModel extends backbone.Model

  url: =>
    if @id
      "/api/sms/newsletter/#{@id}/"
    else
      "/api/sms/newsletter/"

  validation:
      name: [
        {
          required: true,
          msg: 'Заполните название'
        }]

module.exports = NewsletterModel
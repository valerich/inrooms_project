$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

NewsletterCollection = require '../collections/newsletter'
NewsletterModel = require '../models/newsletter'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onAddClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onAddClick: =>
    addNewView = new NewNewsletterView
      model: new NewsletterModel
        kind: 2

    bus.vent.trigger 'modal:show', addNewView,
      title: 'Добавить новую рассылку'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class NewsletterComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class NewsletterComplexListView extends complex.ComplexCompositeView
  childView: NewsletterComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class NewsletterComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class NewNewsletterView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    name: 'Название'

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Рассылка создана')
          @destroy()
          backbone.history.navigate "/newsletter/#{model.id}/", trigger: true
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @collection = new NewsletterCollection
    @filter_model = new FilterModel
    @list_view = new NewsletterComplexListView
      collection: @collection

    @complex_view = new NewsletterComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'newsletters'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'newsletter'
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

    @listenTo @complex_view.list_view, 'childview:click', (view)=>
      backbone.history.navigate "/newsletter/#{view.model.id}/", trigger: true

  onAdded: =>
    @complex_view.doFilter()


module.exports = LayoutView
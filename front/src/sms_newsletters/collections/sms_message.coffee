backbone = require 'backbone'

SmsMessageModel = require '../models/sms_message'


class SmsMessageCollection extends backbone.Collection
  model: SmsMessageModel

  url: =>
    "/api/sms/message_sms/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = SmsMessageCollection

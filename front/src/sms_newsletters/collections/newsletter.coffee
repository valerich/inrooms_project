backbone = require 'backbone'

NewsletterModel = require '../models/newsletter'


class NewsletterCollection extends backbone.Collection
  model: NewsletterModel
  url: "/api/sms/newsletter/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = NewsletterCollection
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'

SmsMessageCollection = require '../../collections/sms_message'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @buyer_model = options.buyer_model

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class MessageSMSComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'

  onRender: =>
    @$('[data-toggle="tooltip"]').tooltip();


class MessageSMSComplexListView extends complex.ComplexCompositeView
  childView: MessageSMSComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class MessageSMSComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class NewsLetterSend extends marionette.ItemView
  template: require './templates/newsletter_send'

  events:
    'click .send_newsletter': 'onSendClick'

  onSendClick: (event) =>
    event.preventDefault()
    $.ajax
      url: "/api/sms/newsletter/#{@model.id}/send/"
      type: 'post'
      success: =>
        helpers.generate_notyfication('success', 'Отправлено')
        backbone.history.loadUrl()
      error: (data) =>
        helpers.modelErrorHandler @model, data, null, {}


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @newsletter_model = options.model

    if @newsletter_model.get('status') == 1
      null
    else
      @filter_model = new FilterModel()
      @filter_model.set('newsletter', @newsletter_model.id)
      data.reqres.setHandler 'filter_model', =>
        @filter_model

      @filter_view = new FilterView
        model: @filter_model
        buyer_model: @buyer_model

        @collection = new SmsMessageCollection
          newsletter_id: @newsletter_model.id
        @list_view = new MessageSMSComplexListView
          collection: @collection

  onRender: =>
    if @newsletter_model.get('status') == 1
      @send_view = new NewsLetterSend
        model: @newsletter_model
      @region_complex.show(@send_view)
    else
      @listenTo @collection, 'sync', =>
        @$('.objects_count').text(@collection.count)

      @region_filter.show(@filter_view)

      view = new MessageSMSComplexListView
        collection: @collection

      @complex_view = new MessageSMSComplexView
        collection: @collection
        filter_model: @filter_model
        list_view: view
        name: 'orders'

      @region_complex.show(@complex_view)

      @complex_view.listenTo @filter_model, 'change', =>
        @complex_view.doFilter()
      @complex_view.doFilter()

module.exports = LayoutView
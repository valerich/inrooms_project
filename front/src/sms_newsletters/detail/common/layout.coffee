$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'
complex = require 'complex'
helpers = require 'helpers'


class ItemView extends marionette.CompositeView
  template: require './templates/item'
  translated_fields:
    name: 'Название'
    text: 'Текст сообщения'
    clients: 'Партнеры'
    buyer_accounts: 'Покупатели'

  events:
    'click .save': 'onClickSave'

  initialize: =>
    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      name: "[name=name]"
      text: "[name=text]"

    @$('[name=clients]').select2
      multiple: true
      placeholder: "Выберите партнеров"
      ajax:
        url: '/api/clients/client/'
        dataType: 'json'
        quietMillis: 250
        cache: true
        data: (term, page) ->
          return {
            search: term
            page: page
            kind: 1
          }
        results: (data)->
          results = data.results
          console.log('results', results)
          data = _.map(results, (x) -> x.text = x.name)
          return {
            results: results
          }

    clients_detail = @model.get 'clients_detail'
    for item in clients_detail
      item.text = item.name
    @$('[name=clients]').select2 'data', clients_detail

    @$('[name=clients]').on 'change', =>
      value = @$('[name=clients]').val()
      result = _.map(value.split(','), (x) -> parseInt(x))
      @model.set 'clients', result


    @$('[name=buyer_profiles]').select2
      multiple: true
      placeholder: "Выберите покупателей"
      ajax:
        url: '/api/buyers/buyer_profile/'
        dataType: 'json'
        quietMillis: 250
        cache: true
        data: (term, page) ->
          return {
            search: term
            page: page
          }
        results: (data)->
          results = data.results
          console.log('results', results)
          data = _.map(results, (x) -> x.text = x.first_name + ' ' + x.middle_name + ' ' + x.last_name)
          return {
            results: results
          }

    buyer_profiles_detail = @model.get 'buyer_profiles_detail'
    for item in buyer_profiles_detail
      item.text = item.name
    @$('[name=buyer_profiles]').select2 'data', buyer_profiles_detail

    @$('[name=buyer_profiles]').on 'change', =>
      value = @$('[name=buyer_profiles]').val()
      result = _.map(value.split(','), (x) -> parseInt(x))
      @model.set 'buyer_profiles', result

    backbone.Validation.bind @

  onClickSave: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'

  initialize: (options)=>
    @newsletter = options.model

    @item_view = new ItemView
      model: @newsletter

  onRender: =>
    @region_item.show(@item_view)


module.exports = LayoutView
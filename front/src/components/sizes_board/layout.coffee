marionette = require 'backbone.marionette'
bus = require 'bus'
helpers = require 'helpers'


class SizesBoard extends marionette.ItemView
  template: require './templates/layout'
  className: 'col-xs-12'
  modelEvents:
    'change:main_collection': 'setSeriesCount'

  serializeData: =>
    s_data = super
    s_data['showLetterSizes'] = @options?.showLetterSizes
    s_data['showNumberSizes'] = @options?.showNumberSizes
    s_data

  setSeriesCount: =>
    for warehouse in ['msk', 'chn', 'showroom']
      remains = @model.get("remains")[warehouse]
      if remains is undefined
        remains = {}
      for size in [
                'xxs',
                'xs',
                's',
                'm',
                'l',
                'xl',
                'xxl',
                'xxxl',
                '34',
                '35',
                '36',
                '37',
                '38',
                '39',
                '40',
                '41',
      ]
        has_size = remains[size]
        if has_size is undefined
          @$(".warehouse_#{warehouse} .has_#{size}").parent().removeClass('bold')
          @$(".warehouse_#{warehouse} .has_#{size}").parent().addClass('c-gray')
        else
          if parseInt(has_size) > 0
            @$(".warehouse_#{warehouse} .has_#{size}").parent().addClass('bold')
            @$(".warehouse_#{warehouse} .has_#{size}").parent().removeClass('c-gray')
          else
            @$(".warehouse_#{warehouse} .has_#{size}").parent().removeClass('bold')
            @$(".warehouse_#{warehouse} .has_#{size}").parent().addClass('c-gray')
        @$(".warehouse_#{warehouse} .has_#{size}").text(has_size)

  onRender: =>
    @setSeriesCount()

module.exports = SizesBoard

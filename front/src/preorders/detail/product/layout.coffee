_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

PreorderItemCollection = require '../../collections/preorder_item'


class PreorderItemItemView extends marionette.ItemView
  template: require './templates/item'
  className: 'preorder_items_item'


class PreorderItemCollectionView extends marionette.CollectionView
  childView: PreorderItemItemView
  className: 'preorder_items_list'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  regions:
    region_item_list: '.region_item_list'

  fetchPreorderItemCollection: =>
    @preorder_item_collection.fetch(
      reset: true,
      data: {
        page_size: 100000
      }
    )

  initialize: (options) =>
    @preorder_model = options.model
    @listenTo bus.vent, 'product_item:update', @fetchPreorderItemCollection

  onRender: =>
    @preorder_item_collection = new PreorderItemCollection
      preorder_id: @preorder_model.id

    @preorder_item_view = new PreorderItemCollectionView
      collection: @preorder_item_collection

    @region_item_list.show(@preorder_item_view)

    @listenTo(bus.vent, 'invoice:items:changed', @fetchPreorderItemCollection)
    @fetchPreorderItemCollection()


module.exports = LayoutView

_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
helpers = require 'helpers'
bus = require 'bus'
bootbox = require 'bootbox'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
      'click .create_order': 'onClickCreateOrder'
      'click .save': 'onClickSave'

  onClickSave: =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


  onClickCreateOrder: =>
    bootbox.confirm "Вы действительно хотите создать бланк-заказ?", (result)=>
      if result == true
        $.ajax
          url: "/api/cart/preorder/#{@model.id}/create_order/"
          type: 'post'
          success: (response) =>
            if response.order_id
              backbone.history.navigate "/order/#{response.order_id}/", {trigger: true}
            else
              helpers.generate_notyfication('error', 'Не заполнено поле "партнер"')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @preorder_model = options.model
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      client: '[name=client]'

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: "/api/clients/client/"
      placeholder: 'Выберите партнера'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true

    backbone.Validation.bind @


module.exports = LayoutView

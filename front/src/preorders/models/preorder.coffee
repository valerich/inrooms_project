backbone = require 'backbone'


class PreorderModel extends backbone.Model

  url: =>
    if @id
      "/api/cart/preorder/#{@id}/"
    else
      "/api/cart/preorder/"

module.exports = PreorderModel
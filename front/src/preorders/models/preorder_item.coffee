_ = require 'underscore'
backbone = require 'backbone'


class PreorderItemModel extends backbone.Model

  initialize: (options) =>
    @preorder_id = options.preorder_id

  url: =>
    if @id
      "/api/cart/preorder/#{@preorder_id}/items/#{@id}/"
    else
      "/api/cart/preorder/#{@preorder_id}/items/"


module.exports = PreorderItemModel


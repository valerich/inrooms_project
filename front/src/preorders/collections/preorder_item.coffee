backbone = require 'backbone'

PreorderItemModel = require '../models/preorder_item'


class PreorderItemCollection extends backbone.Collection
  model: PreorderItemModel

  initialize: (options) =>
    @preorder_id = options.preorder_id

  url: =>
    "/api/cart/preorder/#{@preorder_id}/items/"

  _prepareModel: (model, options) =>
      model.preorder_id = @preorder_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = PreorderItemCollection

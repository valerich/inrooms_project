backbone = require 'backbone'

PreorderModel = require '../models/preorder'


class PreorderCollection extends backbone.Collection
  model: PreorderModel
  url: "/api/cart/preorder/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = PreorderCollection

backbone = require 'backbone'

CommentModel = require '../models/comment'


class CommentCollection extends backbone.Collection
  model: CommentModel
  url: "/api/comments/comment/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = CommentCollection
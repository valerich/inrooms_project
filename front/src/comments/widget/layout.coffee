$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
complex = require 'complex'
marionette = require 'backbone.marionette'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'

CommentCollection = require '../collections/comment'
CommentModel = require '../models/comment'


class CommentComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  events:
    'click .object_link': 'onClickObjectLink'


  onClickObjectLink: (event)=>
     event.preventDefault()
     ct = @model.get('content_object_detail').content_type
     obj = @model.get('content_object_detail').object
     if ct.app_label == 'products' and ct.model == 'product'
       link = '/product/' + obj.id + '/'
     if ct.app_label == 'orders' and ct.model == 'order'
       link = '/product/' + obj.id + '/'
     if ct.app_label == 'offers' and ct.model == 'offer'
       link = '/offer/?id=' + obj.id
     backbone.history.navigate link, trigger: true


class CommentComplexListView extends complex.ComplexCompositeView
  childView: CommentComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class CommentComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  scroll_on_update: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @userCanViewWidget = helpers.user_has_permission bus.cache.user, 'can_view_comments_widget'

    @collection = new CommentCollection
    @list_view = new CommentComplexListView
      collection: @collection

    @complex_view = new CommentComplexView
      collection: @collection
      list_view: @list_view
      name: 'comments_widget'

  onRender: =>
    if @userCanViewWidget
      @region_complex.show(@complex_view)
      @complex_view.doFilter()

  serializeData: =>
    data = super
    data['show_header'] = @show_header
    data['userCanViewWidget'] = @userCanViewWidget
    data

module.exports = LayoutView
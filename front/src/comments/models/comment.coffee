backbone = require 'backbone'


class CommentModel extends backbone.Model

  url: =>
    if @id
      "/api/comments/comment/#{@id}/"
    else
      "/api/comments/comment/"

module.exports = CommentModel
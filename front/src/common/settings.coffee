$ = require 'jquery'
moment = require 'moment'
_ = require 'underscore'
backbone = require 'backbone'

#require 'moment-ru'
require 'jquery.cookie'
require 'select2'
require 'select2_locale_ru'
require 'summernote'
require 'summernote_ru'

require 'bootstrap'
require 'bootstrap-datepicker'
require 'bootstrap-datepicker-ru'

require 'eonasdan-bootstrap-datetimepicker'
moment.locale 'ru'

$.fn.datepicker.defaults.language = "ru"
$.fn.datepicker.defaults.format = "dd.mm.yyyy"
$.fn.datetimepicker.defaults.format ='YYYY-MM-DDTHH:mm:ssZ'

$.summernote.options.lang = 'ru-RU'
$.summernote.options.height = 150
$.summernote.options.toolbar = [
  ["style", ["style"]],
  ["style", ["bold", "italic", "underline", "clear"]],
  ["fontsize", ["fontsize"]],
  ["color", ["color"]],
  ["para", ["ul", "ol", "paragraph"]],
  ["height", ["height"]],
  ["table", ["table"]],
  ['view', ['codeview']],
]

$.summernote.options.onPaste = (e) ->
  e.preventDefault()
  bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text')
  bufferText = bufferText.replace(/(\r\n|\n|\r)/gm, " ")
  bufferText = bufferText.replace(/( {2,})/gm, " ")
  document.execCommand('insertText', false, bufferText)

$.fn.tooltip.Constructor.DEFAULTS.container = 'body'

modelbinder = require 'backbone.modelbinder'
modelbinder.SetOptions
  changeTriggers:
    '': 'change'
    '.btn': 'click'
    '[contenteditable]': 'blur'
    '.datetimepicker': 'dp.change'

_sync = backbone.sync
backbone.sync = (method, model, options)->
    _url = if _.isFunction(model.url) then model.url() else model.url
    _url += if _url.charAt(_url.length - 1) == '/' then '' else '/'

    options = _.extend(options,
        url: _url
    )

    return _sync(method, model, options)

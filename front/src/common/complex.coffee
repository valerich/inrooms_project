marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
store = require 'store'

PaginatorView = require 'paginator'
Spinner = require 'spin.js'

class FilterModel extends backbone.Model
  defaults:
    page: 1
    page_size: '10'
    page_count: 1

  set: (attributes) =>
    if !_.isEmpty(attributes.page)
      attributes.page = parseInt(attributes.page)
    super

  initialize: (options) =>
    if options
      @name = options.name

    page_size = store.get "#{@name}_page_size"
    if !page_size
      page_size = '10'
    @set 'page_size', page_size

    @on 'change:page_size', (model, value) =>
      store.set "#{@name}_page_size", value

  getFilterData: =>
    data = {}
    _.each @.toJSON(), (value, key) =>
      if !_.isUndefined(value)
        data[key] = value
    return @.toJSON()

class SearchView extends marionette.ItemView
  template: require './templates/complex/search'
  events:
    'submit form': 'submit'
  submit: (event)=>
    event.preventDefault()
    @trigger 'search'
  onBeforeDestroy: =>
    @binder.unbind()
  initialize: (options)=>
    @binder = new modelbinder
    @filter_model = options.filter_model
  onRender: =>
    @binder.bind @filter_model, @$el,
      search: '.search'


class ComplexItemView extends marionette.ItemView
  template: require './templates/complex/item'
  triggers:
    'click': 'click'
  initialize: (options) =>
    @name = options.name
  onRender: =>
    @$el.addClass "#{@name}_item"


class ComplexListView extends marionette.CollectionView
  childViewOptions: (model, index) =>
    return name: @name
  onRender: =>
    @$el.addClass "#{@name}_list"
  initialize: (options) =>
    @name = options.name

  getChildView: (item) =>
    if (@options.childView)
      return @options.childView
    if @childView
      return @childView
    return ComplexItemView



class ComplexCompositeView extends marionette.CompositeView
  getChildView: (item) =>
    if (@options.childView)
      return @options.childView
    if @childView
      return @childView
    return ComplexItemView
  childViewOptions: (model, index) =>
    return name: @name
  onRender: =>
    @$el.addClass "#{@name}_list"
  initialize: (options) =>
    @name = options.name

class ComplexView extends marionette.LayoutView
  template: require './templates/complex/layout'
  regions:
    'region_search': '.region_search'
    'region_list': '.region_list'
    'region_paginator': '.region_paginator'
  showSearch: false # Отображать строку поиска
  showPaginator: true # Отображать пагинатор
  manyPaginators: false # Пагинаторы вверху и внизу страницы
  blockFilter: false
  show_page_list: true
  urlFilterParam: false
  className: 'complex_view'
  scroll_on_update: true
  page_size: '10'
  page_size_list: ['10', '20', '50']
  d: 3
  spinner_opts:
    lines: 11
    length: 10
    width: 4
    radius: 10
    corners: 1
    rotate: 0
    color: '#000'
    trail: 100
    top: '50%'
    left: 0

  doFilter: (scroll_on_this_step)=>
    if scroll_on_this_step != false
      @scroll_on_this_step = true
    else
      @scroll_on_this_step = false
    if @blockFilter
      return
    @blockFilter = true

    setTimeout =>
      if @blockFilter
        @showSpinner()
    , 1000

    if @urlFilterParam
      url = window.location.pathname + '?' + $.param(@filter_model.toJSON())
      backbone.history.navigate url, trigger: false

    @collection.fetch
      data: @filter_model.getFilterData()
      reset: true
      error: (collection, response) =>
        if response.status is 404 and @filter_model.get('page') > 2
          @blockFilter = false
          @filter_model.set 'page', 1
      complete: =>
        @blockFilter = false
        @hideSpinner()
        if @scroll_on_update
          if @scroll_on_this_step
            $('body').animate {
              scrollTop: @$('.region_list').offset().top - 50
            }, 500

  showSpinner: =>
    @$('.container_spinner').append(@spinner.el)
  hideSpinner: =>
    @$('.container_spinner').html('')

  renderPaginator: =>
    paginator_view = new PaginatorView
      show_page_list: @show_page_list
      collection: @collection
      filter_model: @filter_model
      page_size_list: @page_size_list
      d: @d
    @region_paginator.show(paginator_view)

    if @manyPaginators
      paginator_view2 = new PaginatorView
        show_page_list: @show_page_list
        collection: @collection
        filter_model: @filter_model
        page_size_list: @page_size_list
        d: @d
      @region_paginator2.show(paginator_view2)

  initialize: (options)=>
    @collection = options.collection || throw 'specify collection of complex view'
    if options.name
      @name = options.name
    if !@name
      throw 'specify name of complex view'
    @spinner = new Spinner(@spinner_opts).spin()
    if options.d isnt undefined
      @d = options.d

    if options.filter_model
      @filter_model = options.filter_model
      @filter_model.set 'name', @name
      @filter_model.name = @name
      @filter_model.set 'page_size', @page_size, silent: true
    else
      @filter_model = new FilterModel
        name: @name
        page_size: @page_size

    page_size = store.get "#{@name}_page_size"
    if @page_size_list and @page_size_list.length
      if page_size in @page_size_list
        @filter_model.set 'page_size', page_size
      else
        page_size = @page_size_list[0]
        store.set "#{@name}_page_size", page_size
        @filter_model.set 'page_size', page_size
    if options.list_view
      @list_view = options.list_view
      @list_view.name = @name
    else if @list_class
      @list_view = new @list_class
        name: @name
        collection: @collection
        filter_model: @filter_model
    else if options.item_view
      @list_view = new ComplexListView
        name: @name
        collection: @collection
        filter_model: @filter_model
        childView: options.item_view
    else
      @list_view = new ComplexListView
        name: @name
        collection: @collection
        filter_model: @filter_model

    if @showSearch
      if options.search_view
        @search_view = options.search_view
      else
        @search_view = new SearchView
          filter_model: @filter_model
      @listenTo @search_view, 'search', @doFilter

    if @showPaginator
      @listenTo @filter_model, 'change:sort change:sort_asc change:page change:page_size', @doFilter
      @listenTo @collection, 'reset', @renderPaginator


  onRender: =>
    @region_list.show(@list_view)

    if @showSearch
      @region_search.show(@search_view)

    console.log 'manyPaginators', @manyPaginators
    if @manyPaginators
      @addRegion("region_paginator2", ".region_paginator2")

    if @showPaginator
      @renderPaginator()
    @doFilter()


module.exports =
  ComplexView: ComplexView
  ComplexItemView: ComplexItemView
  ComplexCompositeView: ComplexCompositeView
  ComplexListView: ComplexListView
  FilterModel: FilterModel
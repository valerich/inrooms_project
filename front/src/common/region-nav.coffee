Marionette = require 'backbone.marionette'
bootstrap = require 'bootstrap'


class NavRegion extends Marionette.Region
  attachHtml: (view) =>
    @$el.html("""
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
            <h4 class="modal-title"><strong>Colored</strong> Header</h4>
          </div>
          <div class="modal-body"></div>
          <!--<div class="modal-footer"></div>-->
        </div>
      </div> """)

    @$el.find('.modal-body').empty().append(view.el)


  onShow: (view, region, options) =>
    @listenTo view, "destroy", @hideModal

    @$el.find('.modal-title').empty().text(options.title)
    @$el.modal options

  hideModal: (view) =>
    @$el.modal 'hide'


module.exports = ModalRegion
moment = require 'moment'
_ = require 'underscore'
Handlebars = require 'hbsfy/runtime'
Humanize = require 'humanize-plus'

Handlebars.registerHelper "debug", ->
  console.log("Current Context")
  console.log("====================")
  console.log(@)

pad = (a, b) ->
  (1e15 + a + "").slice(-b)

Handlebars.registerHelper "datetime", (datetime, format) ->
  m = moment(datetime)
  if _.isString(format)
    return m.format(format)
  else
    return m.toLocaleString()

price_currency_filter = (value, currency) ->
  if !_.isNumber(value)
    value = '-'
    return value
  cents = parseInt((value - Math.floor(value)) * 100)
  dollars = Math.floor(value)
  if value < 10000
    dollars_str = Math.floor(value).toString()
  else
    dollars_str = ''
    while dollars > 0
      left = dollars % 1000
      if dollars > 1000
        left = pad(left, 3)
      dollars_str = ' ' + left + dollars_str
      dollars = parseInt(Math.floor(dollars / 1000))
    dollars_str = dollars_str[1..]

  return "#{dollars_str} #{currency}."

Handlebars = require 'hbsfy/runtime'

Handlebars.registerHelper 'price_currency', (value, currency) ->
  return price_currency_filter(value, currency)

Handlebars.registerHelper 'concat', (json) ->
  concat = ''
  flipArray = []
  for key, val of json.hash
    flipArray.push val

  for i in [flipArray.length - 1..0]
    concat += flipArray[i]

  return concat

Handlebars.registerHelper 'contains', (collection, item, options) ->
  if typeof collection is 'string'
    r = new RegExp(item.toString(), 'i')
    if r.test(collection) then options.fn @ else options.inverse @
  else
    for prop in collection
      if collection.hasOwnProperty prop
        if collection[prop] == item
          return options.fn @
      else
        if typeof prop is 'object'
          if prop.hasOwnProperty 'codename'
            if prop['codename'] == item
              return options.fn @
    return options.inverse @

Handlebars.registerHelper 'ifCond', (v1, operator, v2, options)->
  switch operator
    when '==', '==='
      return if v1 is v2 then options.fn @ else options.inverse @
    when '!='
      return if v1 isnt v2 then options.fn @ else options.inverse @
    when '<'
      return if v1 < v2 then options.fn @ else options.inverse @
    when '<='
      return if v1 <= v2 then options.fn @ else options.inverse @
    when '>'
      return if v1 > v2 then options.fn @ else options.inverse @
    when '>='
      return if v1 >= v2 then options.fn @ else options.inverse @
    when '&&'
      return if v1 && v2 then options.fn @ else options.inverse @
    when '||'
      return if v1 || v2 then options.fn @ else options.inverse @
    else
      return options.inverse @

Handlebars.registerHelper 'decimal_round', (value)->
  if value == null
    return '-'
  return parseFloat(value).toFixed(2)

Handlebars.registerHelper 'decimal_round_plus', (value)->
  if value == null
    return '-'
  return Humanize.formatNumber(value, 2, '&nbsp;')

Handlebars.registerHelper "declOfNum", (number, titles..., options) ->
  cases = [2, 0, 1, 1, 1, 2]
  result = titles[if number % 100 > 4 and number % 100 < 20 then 2 else cases[if number % 10 < 5 then number % 10 else 5]]
  return result

Handlebars.registerHelper "formControlStatic", (name, leftColumn, options) ->
  template = require './templates/formControlStatic'
  if leftColumn is undefined
    leftColumn = 8
  rightColumn = 12 - leftColumn
  html = template
    name: name
    leftColumn: leftColumn
    rightColumn: rightColumn
    value: options.fn(@)
  return new Handlebars.SafeString(html)

Handlebars.registerHelper "formControl", (name, leftColumn, options) ->
  template = require './templates/formControl'
  if leftColumn is undefined
    leftColumn = 8
  rightColumn = 12 - leftColumn
  html = template
    name: name
    leftColumn: leftColumn
    rightColumn: rightColumn
    value: options.fn(@)
  return new Handlebars.SafeString(html)

Handlebars.registerHelper "formControlCheckbox", (leftColumn, options) ->
  template = require './templates/formControlCheckbox'
  if leftColumn is undefined
    leftColumn = 8
  rightColumn = 12 - leftColumn
  html = template
    leftColumn: leftColumn
    rightColumn: rightColumn
    value: options.fn(@)
  return new Handlebars.SafeString(html)

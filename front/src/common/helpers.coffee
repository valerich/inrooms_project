_ = require 'underscore'
bus = require 'bus'
require 'icheck'
require 'noty'
require 'select2'


generate_notyfication = (type, content) ->
    if type == 'success'
      content = '<div class="alert alert-success media fade in"><p>' + content + '</p></div>'
    if type == 'error'
      content = '<div class="alert alert-danger media fade in"><p>' + content + '</p></div>'
    noty({
      dismissQueue: true,
      layout: 'topRight',
      theme : 'made',
      text: content,
      animation: {
        open  : 'animated fadeIn',
        close : 'animated fadeOut'},
      type: type,
      timeout:3000
    })


module.exports = {
  handleiCheck: ($el) ->
    checkboxClass = if $el.attr('data-checkbox') then $el.attr('data-checkbox') else 'icheckbox_minimal-grey'
    radioClass = if $el.attr('data-radio') then $el.attr('data-radio') else 'iradio_minimal-grey';
    console.log(checkboxClass)
    console.log(radioClass)
    if (checkboxClass.indexOf('_line') > -1 || radioClass.indexOf('_line') > -1)
        $el.iCheck({
            checkboxClass: checkboxClass,
            radioClass: radioClass,
            insert: '<div class="icheck_line-icon"></div>' + $el.attr("data-label")
        })
    else
        $el.iCheck({
            checkboxClass: checkboxClass,
            radioClass: radioClass
        })

  user_has_permission: (user, permission) ->
    for prop in user.user_permissions
        if typeof prop is 'object'
          if prop.hasOwnProperty 'codename'
            if prop['codename'] == permission
              return true
    return false

  registerNotCachingModelHandler: (ModelClass, eventName, cacheName) ->
    bus.reqres.setHandler eventName, (modelId) ->
      promise = $.Deferred()
      modelInstance = new ModelClass id: modelId
      modelInstance.fetch
        success: (model, data) ->
          model.changed = {}
          promise.resolve model
        error: (data) ->
          promise.reject data
      promise

  initAjaxSelect2: ($el, config) ->
    url = config.url
    minimumInputLength = config.minimumInputLength || 1
    minimumResultsForSearch = config.minimumResultsForSearch || 1
    multiple = config.multiple || false
    placeholder = config.placeholder || ''
    page_size = config.page_size || 10
    text_attr = config.text_attr || 'name'
    allowClear = config.allowClear || false
    options = {
      multiple: multiple
      minimumInputLength: minimumInputLength
      minimumResultsForSearch: minimumResultsForSearch
      placeholder: placeholder
      allowClear: allowClear
      ajax:
        url: url
        dataType: 'json'
        quietMillis: 250
        cache: true
        data: (term, page) ->
          base =
            search: term
            page: page
            page_size: page_size
          if config.get_extra_search_params
            _.extend base, config.get_extra_search_params()
          return base
        results: (data, page) =>
          if data.results isnt undefined
            results = data.results
          else
            results = data
          for obj in results
            if typeof text_attr is "function"
              obj.text = text_attr(obj)
            else
              obj.text = obj[text_attr]
          count = data.count
          more = ( page + 1 ) * page_size < count
          return {
          results: results
          more: more
          }
    }
    if config.width
      options.width = config.width
    if config.templateResult
      options.templateResult = config.templateResult
    if config.formatResult
      options.formatResult = config.formatResult
    if config.createSearchChoicePosition
      options.createSearchChoicePosition = config.createSearchChoicePosition
    if config.createSearchChoice
      options.createSearchChoice = config.createSearchChoice
    if config.escapeMarkup
      options.escapeMarkup = config.escapeMarkup
    if config.initSelection
      options.initSelection = config.initSelection
    else
      options.initSelection = (el, callback)=>
        value = $el.val()
        $.ajax
          url: url + value + '/'
          success: (response) =>
            obj = response
            if typeof text_attr is "function"
              obj.text = text_attr(obj)
            else
              obj.text = obj[text_attr]
            callback(obj)
    $el.select2(options)

  generate_notyfication: generate_notyfication

  modelErrorHandler: (model, response, options, translated_fields) ->
    errors = response.responseJSON

    if errors
      for field, text of errors
        if translated_fields
          generate_notyfication('error', translated_fields[field] + ': ' + text)
        else
          generate_notyfication('error', field + ': ' + text)
    else
      generate_notyfication('error', response.responseText)

  null_converter: (direction, value, name, models, els) =>
    if direction is 'ModelToView'
      return value
    if direction is 'ViewToModel'
      if value is ''
        return null
      return value

  int_converter: (direction, value, name, models, els) =>
    if direction is 'ModelToView'
      return if not _.isUndefined(value) and not _.isNull(value) then value.toString() else value
    if direction is 'ViewToModel'
      return parseInt(value)

  float_converter: (direction, value, name, models, els) =>
    if direction is 'ModelToView'
      return if not _.isUndefined(value) and not _.isNull(value) then value.toString() else value
    if direction is 'ViewToModel'
      return parseFloat(value)

  decimal_round_converter: (direction, value, name, models, els) =>
    if direction is 'ModelToView'
      return if not _.isUndefined(value) and not _.isNull(value) then parseFloat(value).toFixed(2).toString() else value
    if direction is 'ViewToModel'
      return parseFloat(value)

}
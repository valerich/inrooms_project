Backbone = require 'backbone'
Marionette = require 'backbone.marionette'
store = require 'store'

cache = new Object()

get_list_defer = (options)->
  name = options.name
  url = options.url
  remote = options.remote
  d = $.Deferred()
  timestamp = $("meta[name='timestamp-#{name}']").attr('content')
  old_timestamp = store.get(name + '_timestamp')
  old_data = store.get("#{name}_list")
  if (old_timestamp is timestamp) and old_data and !remote
    cache["#{name}_list"] = old_data
    d.resolve()
  else
    if timestamp
      store.set("#{name}_timestamp", timestamp)
    $.ajax
      url: url
      success: (data) ->
        store.set("#{name}_list", data)
        cache["#{name}_list"] = data
        d.resolve()
      error: ->
        cache["#{name}_list"] = []
        d.resolve()
  return d

get_data = (var_list)->
#  var_list - Это список объектов для доставания из localstorage. Пример var_list:
#  [
#    {name: 'intagcategory', url: '/rest/content_manager/mass/intag_category/'}
#    {name: 'productcollection', url: '/rest/content_manager/mass/collection_tab/'}
#  ]
#  Работает это так: создаете дефер, он смотрит внутрь html на наличие timestamp изменение модели и сравнивает
#  с тем, что есть в localstorage. Если текущий localstorage просрочен - нужно сходить на сервер за данными и
#  записать их в localstorage. Таким образом, эта штука - кэш с инвалидацией. Удобно для редко-меняющихся данных,
#  например список регионов, статусов заказов, коллекций и т.д. Объем localstorage - 5-10 Mb. Пожалуйста, пихайте данные
#  в localstorage, если вы уверены, что они не превыщают 100Кб. Все улицы (модель Street), например, пихать не нужно.
  global_remote = false
  app_hash = null
  for s in document.getElementsByTagName('script')
    match = s.src.match(/(\/oi\/app)\.(\w+)\.js/)
    if match
      app_hash = match[2]
  old_app_hash = store.get 'oi_app_hash'
  if old_app_hash isnt app_hash
    store.set 'oi_app_hash', app_hash
    global_remote = true
  def_list = []
  for v in var_list
    name = v.name
    remote = v.remote and global_remote
    url = v.url || "/rest/localcache/#{name}/"
    d = get_list_defer
      name: name
      url: url
      remote: remote
    def_list.push(d)

  d = $.Deferred()
  $.when.apply($, def_list).then ->
    for v in var_list
      list_data = cache["#{v.name}_list"]
      dict_data = {}
      for obj in list_data
        dict_data[obj.id] = obj
      cache["#{v.name}_dict"] = dict_data

    d.resolve()
  return d

get_user = () ->
  d = $.Deferred()
  $.ajax
    url: '/api/accounts/user/current/'
    success: (data) =>
      cache.user = data
      d.resolve()
  return d

module.exports =
  reqres: new Backbone.Wreqr.RequestResponse()
  vent: new Backbone.Wreqr.EventAggregator()
  get_data: get_data
  get_user: get_user
  cache: cache
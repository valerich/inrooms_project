marionette = require 'backbone.marionette'
backbone = require 'backbone'
bus = require 'bus'
_ = require 'underscore'
ModelBinder = require 'backbone.modelbinder'


class PaginatorView extends marionette.ItemView
  className: 'paginator_view'
  events:
    'click .page': 'onClickPage'
    'click .page_next': 'onClickPageNext'
    'click .page_prev': 'onClickPagePrev'
    'change .page_size': 'onChangePageSize'
  d: 3
  show_page_list: true
  page_size_list: ['10', '20', '50']

  getTemplate: =>
    if @show_page_list
      return require './templates/paginator'
    else
      return require './templates/pager'

  initialize: (options) =>
    if options.d isnt undefined
      @d = options.d
    if options.show_page_list isnt undefined
      @show_page_list = options.show_page_list
    @filter_model = options.filter_model
    @collection = options.collection
    if options.page_size_list
      @page_size_list = options.page_size_list

  onClickPage: (event) =>
    event.preventDefault()
    $target = $(event.target)
    page = $target.data('page')

    @filter_model.set 'page', page

  onChangePageSize: (event) =>
    page_size = @$('.page_size').val()
    @filter_model.set 'page_size', page_size

  onClickPageNext: (event) =>
    event.preventDefault()
    page = @filter_model.get 'page'

    page_size = @filter_model.get 'page_size'
    page_count = Math.ceil(@collection.count / page_size)
    if page < page_count
      @filter_model.set 'page', page + 1


  onClickPagePrev: (event) =>
    event.preventDefault()
    page = @filter_model.get 'page'
    if page > 1
      @filter_model.set 'page', page - 1

  serializeData: =>
    d = @d
    count = @collection.count
    page = @filter_model.get 'page'
    page_size = @filter_model.get 'page_size'
    page_count = Math.ceil(count / page_size)

    result =
      page: page
      page_size: page_size
      page_count: page_count
      page_size_list: @page_size_list
      has_prev: page > 1
      has_next: page < page_count

    result.has_pages = result.has_next or result.has_prev


    if page_count > 2 * d + 5
      middle_first_page = page - d
      middle_last_page = page + d

      if middle_first_page < 3
        diff = 3 - middle_first_page
        middle_first_page += diff
        middle_last_page += diff

      if middle_first_page is 3
        middle_first_page = 2
        first_page_space = false
      else
        first_page_space = true


      if middle_last_page > page_count - 2
        diff = middle_last_page - page_count + 2
        middle_last_page -= diff
        middle_first_page -= diff

      if middle_last_page is page_count - 2
        middle_last_page = page_count - 1
        last_page_space = false
      else
        last_page_space = true

      result.first_page_space = first_page_space # Отображать ли пустое место после первой страницы
      result.last_page_space = last_page_space # Отображать ли пустое место перед последней страницей
      result.page_list = [middle_first_page..middle_last_page] # Здесь не отображать первую и последнюю страницу.
      result.show_first_page = true
      result.show_last_page = true

    else
      result.show_first_page = false
      result.show_last_page = false
      result.first_page_space = false
      result.last_page_space = false

      if page_count > 0
        result.page_list = [1..page_count]
      else
        result.page_list = [1]

    return result

module.exports = PaginatorView
backbone = require 'backbone'


class OrderModel extends backbone.Model

  url: =>
    if @id
      "/api/orders/order_from_site/#{@id}/"
    else
      "/api/orders/order_from_site/"

module.exports = OrderModel
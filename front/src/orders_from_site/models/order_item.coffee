_ = require 'underscore'
backbone = require 'backbone'


class OrderItemModel extends backbone.Model

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    if @id
      "/api/orders/order_from_site/#{@order_id}/items/#{@id}/"
    else
      "/api/orders/order_from_site/#{@order_id}/items/"


module.exports = OrderItemModel

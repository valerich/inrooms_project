_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

OrderItemCollection = require '../../collections/order_item'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/orderitem_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    console.log 'togglePanel'
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class OrderItemComplexItemView extends complex.ComplexItemView
  template: require './templates/orderitem_item'
  className: 'order_items_item'

  modelEvents:
    'change': 'setSeriesCount'

  events:
    'click .remove': 'onClickRemove'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @order_model = options.order_model

  onClickRemove: =>
    bootbox.confirm "Вы действительно хотите удалить модель из заказа с сайта?", (result)=>
      if result == true
        @model.destroy()
        .done =>
          helpers.generate_notyfication('success', 'Удалено')
          bus.vent.trigger 'order:items:changed'
          bus.vent.trigger 'product_item:update'
        .fail (fdata) ->
          helpers.modelErrorHandler @model, fdata
          bus.vent.trigger 'order:items:changed'
          bus.vent.trigger 'product_item:update'

  onRender: =>
    @setSeriesCount()
    @binder.bind @model, @$el,
      count_xxs: '[name=count_xxs]'
      count_xs: '[name=count_xs]'
      count_s: '[name=count_s]'
      count_m: '[name=count_m]'
      count_l: '[name=count_l]'
      count_xl: '[name=count_xl]'
      count_xxl: '[name=count_xxl]'
      count_xxxl: '[name=count_xxxl]'
      count_34: '[name=count_34]'
      count_35: '[name=count_35]'
      count_36: '[name=count_36]'
      count_37: '[name=count_37]'
      count_38: '[name=count_38]'
      count_39: '[name=count_39]'
      count_40: '[name=count_40]'
      count_41: '[name=count_41]'
      count_sum: '.count_sum'

    @listenTo @model, 'change', @onModelChange

  onModelChange: =>
    keys = []
    _.each @model.changed, ( val, key ) =>
      if key != 'count_sum'
        keys.push(key)

  setSeriesCount: =>
    product_detail = @model.get('product_detail')
    if product_detail
      series_type = product_detail['main_collection_detail']['series_type']
    if series_type == 's'
      @$("#order_item_#{@model.id} .c_data").addClass('hidden')
      @$("#order_item_#{@model.id} .s_data").removeClass('hidden')
    else if series_type == 'c'
      @$("#order_item_#{@model.id} .s_data").addClass('hidden')
      @$("#order_item_#{@model.id} .c_data").removeClass('hidden')
    product_detail = @model.get('product_detail')
    if product_detail is undefined
      product_detail = {}

    count_sum = 0
    for size in [
              'xxs',
              'xs',
              's',
              'm',
              'l',
              'xl',
              'xxl',
              'xxxl',
              '34',
              '35',
              '36',
              '37',
              '38',
              '39',
              '40',
              '41',
    ]
      count_sum = count_sum + parseInt(@model.get("count_#{size}"))
    @model.set("count_sum", count_sum)

  serializeData: =>
    @can_orders_return_delete = helpers.user_has_permission bus.cache.user, 'can_orders_return_delete'
    result = super

    can_delete = false
    if @can_orders_return_delete
      if @order_model.id == 145
        can_delete = true
    result['canDeleteProducts'] = can_delete
    result


class OrderItemComplexListView extends complex.ComplexCompositeView
  childView: OrderItemComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/orderitem_items'

  initialize: (options) =>
    @order_model = options.order_model

  childViewOptions: (model, index) =>
    data = {
      order_model: @order_model,
      childIndex: index
    }
    return data


class OrderItemComplexView extends complex.ComplexView
  template: require './templates/orderitem_complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout_product'

  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @order_model = options.model
    @collection = new OrderItemCollection
      order_id: @order_model.id
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
    @filter_model = new FilterModel
    @list_view = new OrderItemComplexListView
      collection: @collection
      order_model: @order_model

    @complex_view = new OrderItemComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'order_items'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
#    @listenTo(bus.vent, 'product_item:update', @complex_view.doFilter)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()


module.exports = LayoutView

wreqr = require 'backbone.wreqr'

module.exports =
  reqres: new wreqr.RequestResponse()
  vent: new wreqr.EventAggregator()
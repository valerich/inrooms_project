backbone = require 'backbone'

OrderItemModel = require '../models/order_item'


class OrderItemCollection extends backbone.Collection
  model: OrderItemModel

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    "/api/orders/order_from_site/#{@order_id}/items/"

  _prepareModel: (model, options) =>
      console.log 'ok'
      model.order_id = @order_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = OrderItemCollection
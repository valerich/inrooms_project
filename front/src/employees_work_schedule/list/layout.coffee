$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

StoreModel = require '../../store/models/store'
StoreCollection = require '../../store/collections/store'


class FilterModel extends complex.FilterModel
  defaults:
    page: 1
    page_size: '10'
    page_count: 1
    is_active: "2"


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onAddClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class StoreComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class StoreComplexListView extends complex.ComplexCompositeView
  childView: StoreComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class StoreComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @userCanViewEmployeesWorkSchedule = helpers.user_has_permission bus.cache.user, 'can_employees_work_schedule_view'

    @is_own = options.is_own
    @collection = new StoreCollection
    @filter_model = new FilterModel
    @list_view = new StoreComplexListView
      collection: @collection

    @complex_view = new StoreComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'stores'

    @filter_view = new FilterView
      model: @filter_model
      is_own: @is_own

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    if @userCanViewEmployeesWorkSchedule
      bus.vent.trigger 'menu:active:set', null, 'employees_work_schedule'
      @region_complex.show(@complex_view)
      @region_filter.show(@filter_view)
      @complex_view.doFilter()

      @listenTo @complex_view.list_view, 'childview:click', (view)=>
        backbone.history.navigate "/employees_work_schedule/#{view.model.id}/", trigger: true

module.exports = LayoutView
_ = require 'underscore'
backbone = require 'backbone'


class EmployeeWorkScheduleModel extends backbone.Model

  initialize: (options) =>
    @store_id = options.store_id
    @year = options.year
    @month = options.month

  url: =>
    "/api/stores/store/#{@store_id}/work_schedule/#{@year}/#{@month}/"


module.exports = EmployeeWorkScheduleModel

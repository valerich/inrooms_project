marionette = require 'backbone.marionette'
backbone = require 'backbone'
_ = require 'underscore'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'

TableLayout = require './table'
EmployeeWorkScheduleModel = require '../models/employee_work_schedule'
FormModel = require '../models/form'


class FormView extends marionette.ItemView
  template: require './templates/form'
  events:
    'click .submit': 'onSubmitForm'

  onSubmitForm: () =>
    v = @model.validate()
    if @model.isValid()
      @model.trigger 'get_report'
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  initialize: =>
    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      month: '[name=month]'
      year: '[name=year]'

    backbone.Validation.bind @


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'employee_work_schedule'
  regions:
    region_table: '.region_table'
    region_form: '.region_form'
  events:
    'click .list_link': 'goToList'


  initialize: (options) =>
    @userCanViewEmployeesWorkSchedule = helpers.user_has_permission bus.cache.user, 'can_employees_work_schedule_view'

    @binder = new modelbinder
    @store_id = options.store_id

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    if @userCanViewEmployeesWorkSchedule
      bus.vent.trigger 'menu:active:set', null, 'employees_work_schedule'
      today = new Date

      @form_model = new FormModel
      @form_model.set('year', today.getFullYear())
      @form_model.set('month', today.getMonth() + 1)

      form_view = new FormView
        model: @form_model

      @region_form.show(form_view)

      @listenTo @form_model, 'get_report', =>
        @showMonth()
      @showMonth()

  showMonth: =>
    model = new EmployeeWorkScheduleModel
      store_id: @store_id
      year: @form_model.get('year')
      month: @form_model.get('month')
    model.fetch().done =>

      table_view = new TableLayout
        model: model
      @region_table.show(table_view)

    @binder.bind model, @$el,
      store_name: '.breadcrumb_name'

  goToList: (event) =>
    event.preventDefault()
    backbone.history.navigate '/employees_work_schedule/', trigger: true

module.exports = ObjectDetailLayout

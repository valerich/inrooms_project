_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
helpers = require 'helpers'
bus = require 'bus'
modelbinder = require 'backbone.modelbinder'


class EditColView extends marionette.ItemView
  template: require './templates/edit_col'
  events:
    'click [name=save]': 'onSaveBtnClick'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @employee_id = options.employee_id
    @day = options.day

  onRender: =>
    bindings = {}

    bindings["empl_#{@employee_id}_day_#{@day}"] = "[name=day_hours]"

    @binder.bind @model, @$el, bindings

  onSaveBtnClick: (event) =>
    event.preventDefault()
    @destroy()


class LayoutView extends marionette.LayoutView
  template: require './templates/table'

  events:
    'click td.empl_day_data': 'onClickCol'
    'click .save_model': 'saveModel'

  modelEvents:
    'change': 'setTotals'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    bindings = {}

    for e in @model.get('employees')
      for d in @model.get('days')
        bindings["empl_#{e['id']}_day_#{d['num']}"] = ".empl_#{e['id']}_day_#{d['num']}"
      bindings["total_#{e['id']}_hours_sum"] = ".total_#{e['id']}_hours_sum"
      bindings["total_#{e['id']}_days_count"] = ".total_#{e['id']}_days_count"

    @binder.bind @model, @$el, bindings
    @setTotals()

  setTotals: =>
    for e in @model.get('employees')
      hours_sum = 0
      days_count = 0
      for d in @model.get('days')
        hours = @model.get("empl_#{e['id']}_day_#{d['num']}")
        hours = parseInt(hours)
        if hours
          hours_sum += hours
          days_count += 1
      @model.set("total_#{e['id']}_hours_sum", hours_sum)
      @model.set("total_#{e['id']}_days_count", days_count)

  onClickCol: (event) =>
    $target = $(event.target)
    day = $target.data('day')
    employee_id = $target.data('employee')

    edit_view = new EditColView
      model: @model
      employee_id: employee_id
      day: day

    bus.vent.trigger 'modal:show', edit_view,
      title: 'Редактирование количества часов'

  saveModel: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Сохранено')
    .fail (data) =>
      helpers.modelErrorHandler @model, data


module.exports = LayoutView

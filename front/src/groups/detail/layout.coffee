$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'

helpers = require 'helpers'

GroupModel = require '../models/group'


class ItemView extends marionette.ItemView
  template: require './templates/item'
  translated_fields:
    name: 'Название'

  initialize: =>
    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'
  events:
    'click .group_list': 'goGroupList'
    'click .breadcrumb_groups': 'goGroupList'

  initialize: (options)=>
    @userCanViewGroups = helpers.user_has_permission bus.cache.user, 'can_groups_view'

    @binder = new modelbinder
    @group = new GroupModel
      id: options.id

    @item_view = new ItemView
      model: @group

  goGroupList: (event) =>
    event.preventDefault()
    backbone.history.navigate '/group/', trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    if @userCanViewGroups
      bus.vent.trigger 'menu:active:set', null, 'group'
      @region_item.show(@item_view)
      @group.fetch()
      @binder.bind @group, @$el,
        name: '.breadcrumb_group_name'


module.exports = LayoutView
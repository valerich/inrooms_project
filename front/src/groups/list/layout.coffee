$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

GroupCollection = require '../collections/group'
GroupModel = require '../models/group'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
#      is_active: '[name=is_active]'
      search: '[name=search]'

#    @$('[name=is_active]').select2()
#    @$('[name=groups]').select2
#      placeholder: "Выберите группу"
#      allowClear: true
#      ajax:
#        url: '/api/accounts/group/'
#        dataType: 'json'
#        quietMillis: 250
#        cache: true
#        data: (term, page) ->
#          return {
#            search: term
#            page: page
#          }
#        results: (data)->
#          results = data.results
#          data = _.map(results, (x) -> x.text = x.name)
#          return {
#            results: results
#          }
#    @$('[name=groups]').on 'change', =>
#      value = @$('[name=groups]').val()
#      if !_.isEmpty(value)
#        result = parseInt(value)
#      @model.set 'groups', result



class GroupComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class GroupComplexListView extends complex.ComplexCompositeView
  childView: GroupComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class GroupComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @userCanViewGroups = helpers.user_has_permission bus.cache.user, 'can_groups_view'

    @collection = new GroupCollection
    @filter_model = new FilterModel
    @list_view = new GroupComplexListView
      collection: @collection

    @complex_view = new GroupComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'groups'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    if @userCanViewGroups
      bus.vent.trigger 'menu:active:set', null, 'group'
      @region_complex.show(@complex_view)
      @region_filter.show(@filter_view)
      @complex_view.doFilter()

      @listenTo @complex_view.list_view, 'childview:click', (view)=>
        backbone.history.navigate "/group/#{view.model.id}/", trigger: true


module.exports = LayoutView
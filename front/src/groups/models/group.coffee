backbone = require 'backbone'


class GroupModel extends backbone.Model

  url: =>
    if @id
      "/api/accounts/group/#{@id}/"
    else
      "/api/accounts/group/"

module.exports = GroupModel
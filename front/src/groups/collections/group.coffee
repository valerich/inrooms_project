backbone = require 'backbone'

GroupModel = require '../models/group'


class GroupCollection extends backbone.Collection
  model: GroupModel
  url: "/api/accounts/group/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = GroupCollection
$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
backbone = require 'backbone'
bus = require 'bus'
store = require 'store'

ChangePasswordView = require '../accounts/utils'


class TopbarView extends marionette.LayoutView
  template: require './templates/topbar'

  events:
    'click .menutoggle': 'onClickMenuToggle'
    'click .change_self_password_button': 'onClickChangeSelfPassword'

  serializeData: =>
    data = super
    data['user'] = bus.cache.user
    data

  onClickMenuToggle: =>
    menu_collapsed = store.get "menu_collapsed"
    if menu_collapsed == true
      menu_collapsed = false
    else
      menu_collapsed = true
    bus.vent.trigger 'menu:collapsed:set', menu_collapsed

  onClickChangeSelfPassword: =>
    PasswordView = new ChangePasswordView(
      user_id: bus.cache.user.id
    )
    bus.vent.trigger 'modal:show', PasswordView,
      title: 'Смена пароля'


module.exports = TopbarView
$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
backbone = require 'backbone'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'


class MenuView extends marionette.LayoutView
  template: require './templates/sidebar-menu'

  events:
    "mouseenter .nav-sidebar>li"   : "menuHoverOn"
    "mouseleave .nav-sidebar>li"   : "menuHoverOff"
    "click .nav-sidebar>li a"      : "onClickMenu"
    "click li.nav-parent>a"        : "onClickParentMenu"

  initialize: =>
    @listenTo(bus.vent, 'menu:collapsed:set', @setMenuColapsed)
    @listenTo(bus.vent, 'menu:active:set', @setActiveMenu)

  onClickMenu: (event) =>
    elem = event.currentTarget
    link = $(elem).data("link")
    console.log link
    if link and link != "#"
      backbone.history.navigate link, {trigger: true}
    else
      null

  onClickParentMenu: (e) =>
    elem = e.currentTarget
    e.preventDefault()
    if ($('body').hasClass('sidebar-collapsed') && !$('body').hasClass('sidebar-hover'))
      return
    if ($('body').hasClass('submenu-hover'))
      return
    parent = $(elem).parent().parent()
    parent.children('li.active').children('.children').slideUp(200)
    $('.nav-sidebar .arrow').removeClass('active')
    parent.children('li.active').removeClass('active')
    sub = $(elem).next()
    if (sub.is(":visible"))
        sub.children().addClass('hidden-item')
        $(elem).parent().removeClass("active");
        sub.slideUp 200, ->
            sub.children().removeClass('hidden-item')
    else
        $(elem).find('.arrow').addClass('active')
        sub.children().addClass('is-hidden')
        setTimeout ( =>
            sub.children().addClass('is-shown')
        ), 0
        sub.slideDown 200, ->
            $(elem).parent().addClass("active")
            setTimeout ( =>
                sub.children().removeClass('is-hidden').removeClass('is-shown')
            ), 500

  menuHoverOn: (event) =>
    elem = event.currentTarget
    $(elem).siblings().removeClass('nav-hover')
    $(elem).addClass('nav-hover')

  menuHoverOff: (event) =>
    elem = event.currentTarget
    $(elem).removeClass('nav-hover')

  setActiveMenu: (menu, submenu) =>
    $('.nav-sidebar>li, .nav-sidebar .children>li').removeClass('nav-active active')
    if menu
      $('#menu-'+menu).addClass('nav-active active')
    else
      null
    if submenu
      $('#submenu-'+submenu).addClass('active')
      if !menu
        $('#submenu-'+submenu).parent().parent().addClass('nav-active active')

  setMenuColapsed: (collapsed) =>
    store.set "menu_collapsed", collapsed
    @.setMenu()

  setMenu: =>
    menu_collapsed = store.get "menu_collapsed"
    if menu_collapsed == true
      null
    else
      menu_collapsed = false
    if menu_collapsed == true
      $('body').addClass('sidebar-collapsed')
      $('body').removeClass('sidebar-show')
    else
      $('body').removeClass('sidebar-collapsed')
      $('body').addClass('sidebar-show')

    if ($('body').hasClass('sidebar-collapsed') || $('body').hasClass('sidebar-top') || $('body').hasClass('submenu-hover'))
        $('.nav-sidebar .children').css({
            display: ''
        })
    else $('.nav-active.active .children').css('display', 'block')

  onRender: =>
    @.setMenu()

  serializeData: =>
    @userCanViewSalesPlan = helpers.user_has_permission bus.cache.user, 'can_sales_plan_view'
    @userCanViewGroups = helpers.user_has_permission bus.cache.user, 'can_groups_view'
    @userCanViewUsers = helpers.user_has_permission bus.cache.user, 'can_users_view'
    @userCanViewOrders = helpers.user_has_permission bus.cache.user, 'can_orders_view'
    @userCanViewOffers = helpers.user_has_permission bus.cache.user, 'can_offers_view'
    @userCanViewProducts = helpers.user_has_permission bus.cache.user, 'can_products_view'
    @userCanViewProductLabels = helpers.user_has_permission bus.cache.user, 'can_product_labels_view'
    @userCanViewFactories = helpers.user_has_permission bus.cache.user, 'can_factories_view'
    @userCanViewWarehouses = helpers.user_has_permission bus.cache.user, 'can_warehouses_view'
    @userCanViewClients = helpers.user_has_permission bus.cache.user, 'can_clients_view'
    @userCanViewProviders = helpers.user_has_permission bus.cache.user, 'can_providers_view'
    @userCanViewStores = helpers.user_has_permission bus.cache.user, 'can_store_view'
    @userCanViewManufacturingOrders = helpers.user_has_permission bus.cache.user, 'can_manufacturing_orders_view'
    @userCanViewSupply = helpers.user_has_permission bus.cache.user, 'can_supply_view'
    @userCanViewPurchase = helpers.user_has_permission bus.cache.user, 'can_purchase_view'
    @userCanViewShop = helpers.user_has_permission bus.cache.user, 'cat_shop_view'
    @userCanViewAllClientStatistic = helpers.user_has_permission bus.cache.user, 'view_all_client_statistic'
    @userCanViewSelfClientStatistic = helpers.user_has_permission bus.cache.user, 'view_self_client_statistic'
    @userCanViewReports = helpers.user_has_permission bus.cache.user, 'view_reports_menu'
    @userCanViewReportClientStatistic = helpers.user_has_permission bus.cache.user, 'view_report_client_statistic'
    @userCanViewEmployees = helpers.user_has_permission bus.cache.user, 'can_employees_view'
    @userCanViewPaymentAccounts = helpers.user_has_permission bus.cache.user, 'can_payment_accounts_view'
    @userCanAllPaymentAccountsView = helpers.user_has_permission bus.cache.user, 'can_all_payment_accounts_view'
    @userCanViewOrdersB2С = helpers.user_has_permission bus.cache.user, 'can_orders_b2с_view'
    @userCanViewReportProductsNotSold = helpers.user_has_permission bus.cache.user, 'view_report_products_not_sold'
    @userCanViewReportRemain = helpers.user_has_permission bus.cache.user, 'view_report_remain'
    @userCanViewReportSalesPlan = helpers.user_has_permission bus.cache.user, 'view_report_sales_plan'
    @userCanViewReportSalesPlanProject = helpers.user_has_permission bus.cache.user, 'view_report_sales_plan_project'
    @userCanViewReportEmployeeSale = helpers.user_has_permission bus.cache.user, 'view_report_employee_sale'
    @userCanViewReportMutualPayments = helpers.user_has_permission bus.cache.user, 'view_report_mutual_payments'
    @userCanViewReportPartnerDayStatistic = helpers.user_has_permission bus.cache.user, 'view_partner_day_statistic'
    @userCanViewReportEmployeeDayStatistic = helpers.user_has_permission bus.cache.user, 'view_employee_day_statistic'
    @userCanViewPaymentAccounts = helpers.user_has_permission bus.cache.user, 'can_payment_accounts_view'
    @userCanAllPaymentAccountsView = helpers.user_has_permission bus.cache.user, 'can_all_payment_accounts_view'
    @userCanViewOrdersB2С = helpers.user_has_permission bus.cache.user, 'can_orders_b2с_view'
    @userCanViewOrdersReturnB2С = helpers.user_has_permission bus.cache.user, 'can_orders_return_b2с_view'
    @userCanViewOrdersReturn = helpers.user_has_permission bus.cache.user, 'can_orders_return_view'
    @userCanViewReportMovementOfGoods = helpers.user_has_permission bus.cache.user, 'view_report_movement_of_goods'
    @userCanViewExpenseItem = helpers.user_has_permission bus.cache.user, 'can_expense_item_view'
    @userCanViewOfferRequest = helpers.user_has_permission bus.cache.user, 'can_offer_requests_view'
    @userCanViewAggreementOffer = helpers.user_has_permission bus.cache.user, 'can_aggreement_offer_view'
    @userCanMainMenuView = helpers.user_has_permission bus.cache.user, 'can_main_menu_view'
    @userCanClientMenuView = helpers.user_has_permission bus.cache.user, 'can_client_menu_view'
    @userCanViewMusic = helpers.user_has_permission bus.cache.user, 'can_music_view'
    @userCanViewVideo = helpers.user_has_permission bus.cache.user, 'can_video_view'
    @userCanViewTask = helpers.user_has_permission bus.cache.user, 'can_task_view'
    @userCanViewInvoice = helpers.user_has_permission bus.cache.user, 'can_invoice_view'
    @userCanViewDefectAct = helpers.user_has_permission bus.cache.user, 'can_defect_act_view'
    @userCanViewBuyerProfiles = helpers.user_has_permission bus.cache.user, 'can_view_buyer_profiles'
    @userCanViewRefundAct = helpers.user_has_permission bus.cache.user, 'can_refund_act_view'
    @userCanViewDocumentScans = helpers.user_has_permission bus.cache.user, 'can_document_scan_view'
    @userCanViewDocumentPriceSet = helpers.user_has_permission bus.cache.user, 'can_price_set_view'
    @userCanViewOwnStore = helpers.user_has_permission bus.cache.user, 'can_own_store_view'
    @userCanViewNewsletter = helpers.user_has_permission bus.cache.user, 'can_newsletter_view'
    @userCanViewPaying = helpers.user_has_permission bus.cache.user, 'can_paying_view'
    @userCanViewPreorder = helpers.user_has_permission bus.cache.user, 'can_preorder_view'
    @userCanViewReportOfficePlan = helpers.user_has_permission bus.cache.user, 'view_report_office_plan'
    @userCanViewReportConsolidated = helpers.user_has_permission bus.cache.user, 'view_report_consolidated'
    @userCanViewSeasonProductCollectionPlan = helpers.user_has_permission bus.cache.user, 'can_season_product_collection_plan_view'
    @userCanViewEmployeesWorkSchedule = helpers.user_has_permission bus.cache.user, 'can_employees_work_schedule_view'
    @userCanViewSubscriber = helpers.user_has_permission bus.cache.user, 'can_subscriber_view'
    @userCanViewHolderPrint = helpers.user_has_permission bus.cache.user, 'can_holder_print_view'
    @userCanViewOrderFromSite = helpers.user_has_permission bus.cache.user, 'can_order_from_site_view'

    data = super
    data['user'] = bus.cache.user
    data['user']['userCanViewSalesPlan'] = @userCanViewSalesPlan
    data['user']['userCanViewGroups'] = @userCanViewGroups
    data['user']['userCanViewUsers'] = @userCanViewUsers
    data['user']['userCanViewOrders'] = @userCanViewOrders
    data['user']['userCanViewOffers'] = @userCanViewOffers
    data['user']['userCanViewProducts'] = @userCanViewProducts
    data['user']['userCanViewProductLabels'] = @userCanViewProductLabels
    data['user']['userCanViewFactories'] = @userCanViewFactories
    data['user']['userCanViewWarehouses'] = @userCanViewWarehouses
    data['user']['userCanViewClients'] = @userCanViewClients
    data['user']['userCanViewManufacturingOrders'] = @userCanViewManufacturingOrders
    data['user']['userCanViewSupply'] = @userCanViewSupply
    data['user']['userCanViewPurchase'] = @userCanViewPurchase
    data['user']['userCanViewShop'] = @userCanViewShop
    data['user']['userCanViewAllClientStatistic'] = @userCanViewAllClientStatistic
    data['user']['userCanViewSelfClientStatistic'] = @userCanViewSelfClientStatistic
    data['user']['userCanViewReports'] = @userCanViewReports
    data['user']['userCanViewReportClientStatistic'] = @userCanViewReportClientStatistic
    data['user']['userCanViewStores'] = @userCanViewStores
    data['user']['userCanViewEmployees'] = @userCanViewEmployees
    data['user']['userCanViewPaymentAccounts'] = @userCanViewPaymentAccounts
    data['user']['userCanAllPaymentAccountsView'] = @userCanAllPaymentAccountsView
    data['user']['userCanViewOrdersB2С'] = @userCanViewOrdersB2С
    data['user']['userCanViewReportProductsNotSold'] = @userCanViewReportProductsNotSold
    data['user']['userCanViewReportRemain'] = @userCanViewReportRemain
    data['user']['userCanViewReportSalesPlan'] = @userCanViewReportSalesPlan
    data['user']['userCanViewReportSalesPlanProject'] = @userCanViewReportSalesPlanProject
    data['user']['userCanViewReportEmployeeSale'] = @userCanViewReportEmployeeSale
    data['user']['userCanViewReportPartnerDayStatistic'] = @userCanViewReportPartnerDayStatistic
    data['user']['userCanViewReportEmployeeDayStatistic'] = @userCanViewReportEmployeeDayStatistic
    data['user']['userCanViewPaymentAccounts'] = @userCanViewPaymentAccounts
    data['user']['userCanAllPaymentAccountsView'] = @userCanAllPaymentAccountsView
    data['user']['userCanViewOrdersB2С'] = @userCanViewOrdersB2С
    data['user']['userCanViewOrdersReturnB2С'] = @userCanViewOrdersReturnB2С
    data['user']['userCanViewOrdersReturn'] = @userCanViewOrdersReturn
    data['user']['userCanViewReportMovementOfGoods'] = @userCanViewReportMovementOfGoods
    data['user']['userCanViewExpenseItem'] = @userCanViewExpenseItem
    data['user']['userCanViewOfferRequest'] = @userCanViewOfferRequest
    data['user']['userCanViewAggreementOffer'] = @userCanViewAggreementOffer
    data['user']['userCanMainMenuView'] = @userCanMainMenuView
    data['user']['userCanClientMenuView'] = @userCanClientMenuView
    data['user']['userCanViewVideo'] = @userCanViewVideo
    data['user']['userCanViewMusic'] = @userCanViewMusic
    data['user']['userCanViewTask'] = @userCanViewTask
    data['user']['userCanViewProviders'] = @userCanViewProviders
    data['user']['userCanViewInvoice'] = @userCanViewInvoice
    data['user']['userCanViewDefectAct'] = @userCanViewDefectAct
    data['user']['userCanViewBuyerProfiles'] = @userCanViewBuyerProfiles
    data['user']['userCanViewRefundAct'] = @userCanViewRefundAct
    data['user']['userCanViewDocumentScans'] = @userCanViewDocumentScans
    data['user']['userCanViewDocumentPriceSet'] = @userCanViewDocumentPriceSet
    data['user']['userCanViewOwnStore'] = @userCanViewOwnStore
    data['user']['userCanViewNewsletter'] = @userCanViewNewsletter
    data['user']['userCanViewPaying'] = @userCanViewPaying
    data['user']['userCanViewPreorder'] = @userCanViewPreorder
    data['user']['userCanViewReportMutualPayments'] = @userCanViewReportMutualPayments
    data['user']['userCanViewReportOfficePlan'] = @userCanViewReportOfficePlan
    data['user']['userCanViewReportConsolidated'] = @userCanViewReportConsolidated
    data['user']['userCanViewSeasonProductCollectionPlan'] = @userCanViewSeasonProductCollectionPlan
    data['user']['userCanViewEmployeesWorkSchedule'] = @userCanViewEmployeesWorkSchedule
    data['user']['userCanViewSubscriber'] = @userCanViewSubscriber
    data['user']['userCanViewHolderPrint'] = @userCanViewHolderPrint
    data['user']['userCanViewOrderFromSite'] = @userCanViewOrderFromSite

    data['user']['userCanViewOrdersMenu'] = [
      @userCanViewManufacturingOrders,
      @userCanViewSupply,
      @userCanViewPurchase,
    ].some(@isTrue)

    data['user']['canViewReportsMenu'] = [
      @userCanViewReportSalesPlan,
      @userCanViewReportClientStatistic,
      @userCanViewReportMovementOfGoods,
      @userCanViewReportRemain,
      @userCanViewReportProductsNotSold,
      @userCanViewReportSalesPlanProject,
      @userCanViewReportEmployeeSale,
      @userCanViewReportMutualPayments,
      @userCanViewReportOfficePlan,
      @userCanViewReportConsolidated,
      @userCanViewReportPartnerDayStatistic,
      @userCanViewReportEmployeeDayStatistic,
    ].some(@isTrue)

    data['user']['canViewShopMenu'] = [
      @userCanViewShop,
      @userCanViewPreorder,
      @userCanViewAggreementOffer,
      @userCanViewOrders,
      @userCanViewOrdersReturn,
      @userCanViewOfferRequest,
      @userCanViewDefectAct,
      @userCanViewRefundAct,
      @userCanViewInvoice,
      @userCanViewPaying,
      @userCanViewOfferRequest,
    ].some(@isTrue)
    data['user']['CanViewCataloguesMenu'] = [
      @userCanViewProducts,
      @userCanViewProductLabels,
      @userCanViewFactories,
      @userCanViewWarehouses,
      @userCanViewClients,
      @userCanViewProviders,
      @userCanViewStores,
      @userCanViewEmployees,
    ].some(@isTrue)
    data['user']['canViewPartnerMenu'] = [
      @userCanViewOrdersB2С,
      @userCanViewOrdersReturnB2С,
      @userCanViewTask,
      @userCanViewBuyerProfiles,
      @userCanViewDocumentPriceSet,
      @userCanViewNewsletter,
    ].some(@isTrue)
    data['user']['userCanViewPlanning'] = [
      @userCanViewSalesPlan,
      @userCanViewSeasonProductCollectionPlan,
      @userCanViewEmployeesWorkSchedule,
    ].some(@isTrue)
    data['user']['userCanViewMerchandising'] = [
      @userCanViewHolderPrint
    ].some(@isTrue)
    data['user']['CanViewOwnRetailMenu'] = [
      @userCanViewOwnStore,
      @userCanViewSubscriber,
    ].some(@isTrue)
    data

  isTrue: (item) =>
    item == true


class FooterView extends marionette.LayoutView
  template: require './templates/sidebar-footer'

  events:
    'click .toggle_fullscreen': 'onClickToggleFullScreen'

  onRender: =>
    @$('a').tooltip()

  onClickToggleFullScreen: (e) =>
    doc = document
    docEl = document.documentElement
    if !doc.fullscreenElement && !doc.msFullscreenElement && !doc.webkitIsFullScreen && !doc.mozFullScreenElement
        if docEl.requestFullscreen
            docEl.requestFullscreen()
        else if docEl.webkitRequestFullScreen
            docEl.webkitRequestFullscreen()
        else if docEl.webkitRequestFullScreen
            docEl.webkitRequestFullScreen()
        else if docEl.msRequestFullscreen
            docEl.msRequestFullscreen()
        else if docEl.mozRequestFullScreen
            docEl.mozRequestFullScreen()
    else
        if doc.exitFullscreen
            doc.exitFullscreen()
        else if doc.webkitExitFullscreen
            doc.webkitExitFullscreen()
        else if doc.webkitCancelFullScreen
            doc.webkitCancelFullScreen()
        else if doc.msExitFullscreen
            doc.msExitFullscreen()
        else if doc.mozCancelFullScreen
            doc.mozCancelFullScreen()


class SidebarView extends marionette.LayoutView
  template: require './templates/sidebar'
  regions:
    'region_menu': '#nav-region'
    'region_footer': '.sidebar-footer'

  initialize: (options) =>
    @menu_view = new MenuView
    @footer_view = new FooterView

  onRender: =>
    @region_menu.show(@menu_view)
    @region_footer.show(@footer_view)

  serializeData: =>
    data = super
    data['user'] = bus.cache.user
    data


module.exports = SidebarView
$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'

helpers = require 'helpers'

FactoryModel = require '../models/factory'


class ItemView extends marionette.ItemView
  template: require './templates/item'
  translated_fields:
    name: 'Название'
    phone: 'Номер телефона фабрики'
    address: 'Адрес фабрики'
    contact_person: 'Контактное лицо'
    contact_email: 'Email'

  events:
    'click .save': 'onClickSave'

  initialize: =>
    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'
      phone: "[name=phone]"
      address: "[name=address]"
      contact_person: "[name=contact_person]"
      contact_email: "[name=contact_email]"

    backbone.Validation.bind @

  onClickSave: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'
  events:
    'click .to_list': 'goToList'
    'click .breadcrumb_list': 'goToList'

  initialize: (options)=>
    @binder = new modelbinder
    @user = new FactoryModel
      id: options.id

    @item_view = new ItemView
      model: @user

  goToList: (event) =>
    event.preventDefault()
    backbone.history.navigate '/factory/', trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'factory'
    @region_item.show(@item_view)
    @user.fetch()
    @binder.bind @user, @$el,
      name: '.breadcrumb_name'


module.exports = LayoutView
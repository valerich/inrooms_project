backbone = require 'backbone'

FactoryModel = require '../models/factory'


class FactoryCollection extends backbone.Collection
  model: FactoryModel
  url: "/api/factories/factory/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = FactoryCollection
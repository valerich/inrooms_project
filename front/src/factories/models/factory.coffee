backbone = require 'backbone'


class FactoryModel extends backbone.Model

  url: =>
    if @id
      "/api/factories/factory/#{@id}/"
    else
      "/api/factories/factory/"

  validation:
      name: [
        {
          required: true,
          msg: 'Заполните название'
        }]

module.exports = FactoryModel
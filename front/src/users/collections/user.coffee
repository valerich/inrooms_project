backbone = require 'backbone'

UserModel = require '../models/user'


class UserCollection extends backbone.Collection
  model: UserModel
  url: "/api/accounts/user/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = UserCollection
backbone = require 'backbone'

HistoryItemModel = require '../models/history_item'


class HistoryItemCollection extends backbone.Collection
  model: HistoryItemModel

  initialize: (options) =>
    @user_id = options.user_id

  url: =>
    "/api/history/history_item/accounts/user/#{@user_id}/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = HistoryItemCollection
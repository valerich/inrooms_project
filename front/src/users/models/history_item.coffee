_ = require 'underscore'
backbone = require 'backbone'


class HisotoryItemModel extends backbone.Model

  initialize: (options) =>
    @user_id = options.user_id

  url: =>
    if @id
      "/api/history/history_item/accounts/user/#{@user_id}/#{@id}/"
    else
      "/api/history/history_item/accounts/user/#{@user_id}/"


module.exports = HisotoryItemModel

backbone = require 'backbone'


class UserModel extends backbone.Model
  defaults:
    is_active: false
  validation:
    email: [
      {
        required: true,
        msg: 'Заполните email'
      },{
        pattern: 'email',
        msg: 'Укажите корректный email'
      }]
    first_name:
      required: true
      msg: 'Заполните ФИО'

  url: =>
    if @id
      "/api/accounts/user/#{@id}/"
    else
      "/api/accounts/user/"

module.exports = UserModel
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

UserCollection = require '../collections/user'
UserModel = require '../models/user'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      is_active: '[name=is_active]'
      search: '[name=search]'

    @$('[name=is_active]').select2()
    @$('[name=groups]').select2
      placeholder: "Выберите группу"
      allowClear: true
      ajax:
        url: '/api/accounts/group/'
        dataType: 'json'
        quietMillis: 250
        cache: true
        data: (term, page) ->
          return {
            search: term
            page: page
          }
        results: (data)->
          results = data.results
          data = _.map(results, (x) -> x.text = x.name)
          return {
            results: results
          }
    @$('[name=groups]').on 'change', =>
      value = @$('[name=groups]').val()
      if !_.isEmpty(value)
        result = parseInt(value)
      @model.set 'groups', result



class UserComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class UserComplexListView extends complex.ComplexCompositeView
  childView: UserComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class UserComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2

  events:
    'click .add': 'onClickAdd'

  onClickAdd: (event) =>
    addNewUserView = new NewUserView
      model: new UserModel()

    bus.vent.trigger 'modal:show', addNewUserView,
      title: 'Добавить нового пользователя'


class NewUserView extends marionette.ItemView
  template: require './templates/new_user_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    is_active: 'Активен'
    first_name: 'ФИО'
    email: 'email'

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      'is_active': '[name=is_active]'
      'first_name': '[name=name]'
      'email': '[name=email]'
    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          backbone.history.navigate "/user/#{model.get('id')}/", {trigger: true}
          helpers.generate_notyfication('success', 'Пользователь создан')
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @userCanViewUsers = helpers.user_has_permission bus.cache.user, 'can_users_view'

    @collection = new UserCollection
    @filter_model = new FilterModel
    @list_view = new UserComplexListView
      collection: @collection

    @complex_view = new UserComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'users'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    if @userCanViewUsers
      bus.vent.trigger 'menu:active:set', null, 'user'
      @region_complex.show(@complex_view)
      @region_filter.show(@filter_view)
      @complex_view.doFilter()

      @listenTo @complex_view.list_view, 'childview:click', (view)=>
        backbone.history.navigate "/user/#{view.model.id}/", trigger: true


module.exports = LayoutView
$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'

helpers = require 'helpers'

UserModel = require '../models/user'

ChangePasswordView = require '../../accounts/utils'
HistoryLayout = require './history/layout'


class NavbarView extends marionette.ItemView
  template: require './templates/navbar'
  events:
    'click .tab_link': 'onClickTabLink'
  initialize: (options) =>
    @tab_name = options.tab_name
  onRender: =>
    @$(".tab_link[data-tab_name=#{@tab_name}]").addClass('active')
  onClickTabLink: (event) =>
    event.preventDefault()
    tab_name = $(event.target).parent().data('tab_name')
    @$(".tab_link").removeClass('active')
    @$(".tab_link[data-tab_name=#{tab_name}]").addClass('active')
    @trigger 'click', tab_name


class ItemView extends marionette.ItemView
  template: require './templates/item'
  translated_fields:
    is_active: 'Активен'
    first_name: 'Имя'
    middle_name: 'Отчество'
    last_name: 'Фамилия'
    email: 'email'
    groups: 'группы'

  events:
    'click .save': 'onClickSave'
    'click .change_self_password_button': 'onClickChangePassword'

  onClickChangePassword: =>
    PasswordView = new ChangePasswordView(
      user_id: @model.id
    )
    bus.vent.trigger 'modal:show', PasswordView,
      title: 'Смена пароля'

  initialize: =>
    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      is_active: '[name=is_active]'
      first_name: '[name=first_name]'
      middle_name: '[name=middle_name]'
      last_name: '[name=last_name]'
      email: '[name=email]'
      description: '[name=description]'

    @$('[name=groups]').select2
      multiple: true
      placeholder: "Выберите группу"
      ajax:
        url: '/api/accounts/group/'
        dataType: 'json'
        quietMillis: 250
        cache: true
        data: (term, page) ->
          return {
            search: term
            page: page
          }
        results: (data)->
          results = data.results
          console.log('results', results)
          data = _.map(results, (x) -> x.text = x.name)
          return {
            results: results
          }
    groups_detail = @model.get 'groups_detail'
    for item in groups_detail
      item.text = item.name
    @$('[name=groups]').select2 'data', groups_detail

    @$('[name=groups]').on 'change', =>
      value = @$('[name=groups]').val()
      result = _.map(value.split(','), (x) -> parseInt(x))
      @model.set 'groups', result

    backbone.Validation.bind @

  onClickSave: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Пользователь сохранен')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  serializeData: =>
    @userCanChangePassword = helpers.user_has_permission bus.cache.user, 'can_set_all_passwords'
    data = super
    data['user'] = bus.cache.user
    data['user']['userCanChangePassword'] = @userCanChangePassword
    data


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'order_detail_layout'
  regions:
    region_tab: '.region_tab'
    region_navbar: '.region_navbar'
  events:
    'click .breadcrumb_users': 'onClickListLink'

  onClickListLink: (event) =>
    event.preventDefault()
    backbone.history.navigate '/user/', trigger: true

  initialize: (options) =>
    @userCanViewUsers = helpers.user_has_permission bus.cache.user, 'can_users_view'

    @tab_name = options.tab_name
    @binder = new modelbinder
    @model = new UserModel
      id: options.id

  onBeforeDestroy: =>
    @binder.unbind()

  renderTab: (tab_name) =>
    view_class = null
    if tab_name is 'common'
      view_class = ItemView
    if tab_name is 'history'
      view_class = HistoryLayout
    if view_class
      tab_view = new view_class
        model: @model
      @region_tab.show(tab_view)
    else
      @region_tab.empty()

  onRender: =>
    if @userCanViewUsers
      @binder.bind @model, @$el,
        name: '.breadcrumb_user_name'
      @model.fetch().done =>
        bus.vent.trigger 'menu:active:set', null, 'user'
        @renderTab(@tab_name)
        navbar_view = new NavbarView
          model: @model
          tab_name: @tab_name
        @region_navbar.show(navbar_view)
        @listenTo navbar_view, 'click', (tab_name) =>
          @tab_name = tab_name
          backbone.history.navigate "/user/#{@model.id}/#{tab_name}/", trigger: false
          @renderTab(tab_name)


module.exports = ObjectDetailLayout
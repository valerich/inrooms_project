$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require '../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
require 'magnific-popup'
require 'blueimp-file-upload'


class LayoutView extends marionette.ItemView
  template: require './templates/layout_product_from_file'

  events:
    'click .add_file': 'onAddClick'

  initialize: (options) =>
    @price_set_model = options.model

  onAddClick: =>
    @$('.fileupload').fileupload
      url: "/api/prices/price_set/#{@price_set_model.id}/file_upload/"
      dataType: 'json'
      submit: (e, data) =>
        $('#music_add_btn').removeClass('fa-plus-circle')
        $('.add').removeClass('btn-primary')
        $('.add').addClass('btn-default')
        $('#music_add_btn').disabled = true
        $('#music_add_btn').addClass('fa-spinner')
      always: (e, data) =>
        $('#music_add_btn').addClass('fa-plus-circle')
        $('.add').addClass('btn-primary')
        $('.add').removeClass('btn-default')
        $('#music_add_btn').disabled = false
        $('#music_add_btn').removeClass('fa-spinner')
      done: (e, response) =>
        @$('.result').html('')
        _.each response.result['errors'], (index, el) =>
          @$('.result').append('<p>' + index + '</p>')
      fail: (e, response) =>
        helpers.generate_notyfication('error', 'не поддерживаемый тип файла')

module.exports = LayoutView
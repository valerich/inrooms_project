$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require '../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
require 'magnific-popup'

MainCollectionCollection = require '../../collections/main_collection'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/main_collection_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    console.log 'togglePanel'
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class MainCollectionComplexItemView extends complex.ComplexItemView
  template: require './templates/main_collection_item'

  events:
    'click .create_price_set_item': 'onClickCreatePriceSetItem'

  initialize: (options)=>
    @binder = new modelbinder
    @price_set_model = options.price_set_model

  onClickCreatePriceSetItem: (event)=>
    event.preventDefault()
    product_collection_id = @model.id
    $.ajax
        url: "/api/prices/price_set/#{@price_set_model.id}/add_product_collection/"
        data:
          "product_collection_id": product_collection_id
        type: 'post'
        success: =>
          bus.vent.trigger 'order_item:added'
        error: (data) =>
          helpers.modelErrorHandler @model, data


class MainCollectionComplexListView extends complex.ComplexCompositeView
  childView: MainCollectionComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/main_collection_items'

  initialize: (options) =>
    @price_set_model = options.price_set_model

  childViewOptions: (model, index) =>
    data = {
      price_set_model: @price_set_model,
      childIndex: index
    }
    return data


class MainCollectionComplexView extends complex.ComplexView
  template: require './templates/main_collection_complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout_main_collection_search'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @price_set_model = options.model
    @collection = new MainCollectionCollection
      price_set_id: @price_set_model.id

    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
    @filter_model = new FilterModel
      _client_id: @price_set_model.get('client')
    @list_view = new MainCollectionComplexListView
      collection: @collection
      price_set_model: @price_set_model

    @complex_view = new MainCollectionComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'products'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @listenTo(bus.vent, 'order_item:added', @onProductAdded)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

  onProductAdded: (product_id) =>
    @complex_view.doFilter()


module.exports = LayoutView
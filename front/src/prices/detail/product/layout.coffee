marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'


ProductSearchLayout = require './layout_product_search'
MainCollectionSearchLayout = require './layout_main_collection_search'
ProductLayout = require './layout_product'
ProductFromFileLayout = require './layout_product_from_file'


class NavbarView extends marionette.ItemView
  template: require './templates/navbar'
  events:
    'click .tab_link': 'onClickTabLink'
  initialize: (options) =>
    @tab_name = options.tab_name
    @price_set_model = options.model

  onRender: =>
    @$(".tab_link[data-tab_name=#{@tab_name}]").addClass('active')
  onClickTabLink: (event) =>
    event.preventDefault()
    tab_name = $(event.target).parent().data('tab_name')
    @$(".tab_link").removeClass('active')
    @$(".tab_link[data-tab_name=#{tab_name}]").addClass('active')
    @trigger 'click', tab_name


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'order_detail_product_layout'
  regions:
    region_tab: '.region_product_tab'
    region_navbar: '.region_product_navbar'

  initialize: (options) =>
    @tab_name = 'product_list'
    @price_set_model = options.model

  renderTab: (tab_name) =>
    view_class = null
    if tab_name is 'product_search'
      view_class = ProductSearchLayout
    if tab_name is 'product_list'
      view_class = ProductLayout
    if tab_name is 'main_collection_search'
      view_class = MainCollectionSearchLayout
    if tab_name is 'from_file'
      view_class = ProductFromFileLayout
    if view_class
      @tab_view = new view_class
        model: @price_set_model
      @region_tab.show(@tab_view)
    else
      @region_tab.empty()
    @listenTo(bus.vent, 'order:items:changed', @onItemsChanged)

  onRender: =>
    @renderTab(@tab_name)
    if @price_set_model.get('source') == 2
      navbar_view = new NavbarView
        model: @order_model
        tab_name: @tab_name
      @region_navbar.show(navbar_view)

      @listenTo navbar_view, 'click', (tab_name) =>
        @tab_name = tab_name
        @renderTab(tab_name)

    @listenTo(bus.vent, 'order_item:delete', @onDeleteOrderItem)
    @listenTo(bus.vent, 'order_item:added', @onAddedOrderItem)

  onDeleteOrderItem: =>
    @renderTab('product_search')

#  onAddedOrderItem: =>
#    @price_set_model.fetch().done =>
#      @renderTab('product_list')

module.exports = ObjectDetailLayout

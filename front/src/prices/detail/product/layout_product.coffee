_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'
require 'jquery.inputmask'

PriceSetItemCollection = require '../../collections/price_set_item'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/orderitem_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    console.log 'togglePanel'
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      main_collection: '[name=main_collection]'
      sort_by: '.sort_list'

    helpers.initAjaxSelect2 @$('[name=main_collection]'),
      url: '/api/products/collection/'
      placeholder: "Выберите главную категорию"
      minimumInputLength: -1
      text_attr: 'full_name'
      allowClear: true


class DefectItemComplexItemView extends complex.ComplexItemView
  template: require './templates/orderitem_item'
  className: 'order_items_item'
  events:
    'click .save-price_set_item': 'onClickSavePriceSetItem'
    'click .remove': 'onClickRemove'
    'click .image-link': 'onClickImageLink'

  initialize: (options)=>
    @binder = new modelbinder
    @price_set_model = options.price_set_model

  onClickSavePriceSetItem: (event)=>
    console.log 'ok'
    event.preventDefault()
    @model.save()
      .done =>
        helpers.generate_notyfication('success', 'Модель сохранена')
      .fail (data) ->
        helpers.modelErrorHandler @model, data

  onClickImageLink: (event) =>
    image_data = @model.get('order_item_detail')['product_detail']['image']
    if image_data
      image = image_data['src']
    else
      image = '/static/img/no-image.jpg'
    $.magnificPopup.open
      type:'image'
      mainClass: 'mfp-fade'
      items: [{'src': image}]
      midClick: true

  onClickRemove: =>
    bootbox.confirm "Вы действительно хотите удалить модель?", (result)=>
      if result == true
        @model.set('price_set_id', @price_set_model.id)
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')
          bus.vent.trigger 'product_item:update'

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      new_price: '[name=new_price]'

  serializeData: =>
    s_data = super
    s_data['price_set_model'] = @price_set_model
    if @price_set_model.get('source') == 2
      s_data['can_edit'] = true
    else
      s_data['can_edit'] = false
    s_data


class PriceSetItemComplexListView extends complex.ComplexCompositeView
  childView: DefectItemComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/orderitem_items'

  initialize: (options) =>
    @price_set_model = options.price_set_model

  childViewOptions: (model, index) =>
    data = {
      price_set_model: @price_set_model,
      childIndex: index
    }
    return data


class PriceSetItemComplexView extends complex.ComplexView
  template: require './templates/orderitem_complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout_product'

  regions:
#    region_item_list: '.region_item_list'
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @price_set_model = options.model
    @collection = new PriceSetItemCollection
      price_set_id: @price_set_model.id
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
    @filter_model = new FilterModel
      _client_id: @price_set_model.get('client')
    @list_view = new PriceSetItemComplexListView
      collection: @collection
      price_set_model: @price_set_model

    @complex_view = new PriceSetItemComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'price_set_items'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @listenTo(bus.vent, 'product_item:update', @complex_view.doFilter)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()


module.exports = LayoutView

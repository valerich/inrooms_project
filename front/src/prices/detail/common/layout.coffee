_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
helpers = require 'helpers'
bus = require 'bus'
bootbox = require 'bootbox'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
      'click .save_invoice': 'onClickSave'
      'click .re_price_set': 'onClickRePriceSet'

  onClickSave: =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
          backbone.history.navigate "/price_set/#{@model.id}/items/", trigger: true
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onClickRePriceSet: =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      bootbox.confirm 'Вы действительно хотите переназначить цены', (result)=>
        if result == true
          @model.save {},
            success: (model) =>
              $.ajax
                url: "/api/prices/price_set/#{@model.id}/re_price_set/"
                type: 'post'
                success: =>
                  backbone.history.navigate "/price_set/#{@model.id}/items/", trigger: true
                error: (data) =>
                  helpers.modelErrorHandler @model, data
            error: (model, response, options) =>
              console.log 'model save error'
              helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @model.fetch()
    @binder = new modelbinder

  onRender: =>
    if @model.get('source') == 2
      @binder.bind @model, @$el,
        markup_percentage: '[name=markup_percentage]'
        price_set_kind: '[name=price_set_kind]'

    backbone.Validation.bind @

  serializeData: =>
    s_data = super
    if @model.get('source') == 2
      s_data['can_edit'] = true
    else
      s_data['can_edit'] = false
    s_data


module.exports = LayoutView

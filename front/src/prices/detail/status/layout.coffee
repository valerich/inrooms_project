_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
bootbox = require 'bootbox'
store = require 'store'
helpers = require 'helpers'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .set_complete_status': 'onClickSetCompleteStatus'
    'click .exit': 'onClickExit'

  initialize: (options) =>
    @price_set_id = options.model.id

  onClickSetCompleteStatus: =>
    event.preventDefault()
    $.ajax
      url: "/api/prices/price_set/#{@price_set_id}/change_status/"
      type: 'post'
      data: {
        'code': 'complete'
      }
      success: =>
        helpers.generate_notyfication('success', 'Статус изменен')
        backbone.history.navigate "/price_set/", trigger: true
        @destroy()
      error: (data) =>
        helpers.modelErrorHandler @model, data

  onClickExit: =>
    backbone.history.navigate "/price_set/", trigger: true

module.exports = LayoutView

backbone = require 'backbone'

PriceSetItemModel = require '../models/price_set_item'


class PriceSetItemCollection extends backbone.Collection
  model: PriceSetItemModel

  initialize: (options) =>
    @price_set_id = options.price_set_id

  url: =>
    "/api/prices/price_set/#{@price_set_id}/items/"

  _prepareModel: (model, options) =>
      model.price_set_id = @price_set_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = PriceSetItemCollection

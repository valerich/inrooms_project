backbone = require 'backbone'

PriceSetModel = require '../models/price_set'


class PriceSetCollection extends backbone.Collection
  model: PriceSetModel
  url: "/api/prices/price_set/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = PriceSetCollection

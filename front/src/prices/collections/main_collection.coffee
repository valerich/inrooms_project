backbone = require 'backbone'

MainCollectionModel = require '../models/main_collection'


class MainCollectionCollection extends backbone.Collection
  model: MainCollectionModel

  initialize: (options) =>
    @price_set_id = options.price_set_id

  url: =>
    "/api/prices/price_set/#{@price_set_id}/product_collection_search/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = MainCollectionCollection
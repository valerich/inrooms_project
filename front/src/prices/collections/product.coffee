backbone = require 'backbone'

ProductModel = require '../models/product'


class ProductCollection extends backbone.Collection
  model: ProductModel

  initialize: (options) =>
    @price_set_id = options.price_set_id

  url: =>
    "/api/prices/price_set/#{@price_set_id}/product_search/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ProductCollection
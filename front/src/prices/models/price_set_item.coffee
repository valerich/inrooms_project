_ = require 'underscore'
backbone = require 'backbone'


class PriceSetItemModel extends backbone.Model
  defaults:
    count: 1
  validation:
    price: [
      {
        required: true,
        msg: 'Заполните цену'
      }]

  initialize: (options) =>
    @price_set_id = options.price_set_id

  url: =>
    if @id
      "/api/prices/price_set/#{@price_set_id}/items/#{@id}/"
    else
      "/api/prices/price_set/#{@price_set_id}/items/"


module.exports = PriceSetItemModel


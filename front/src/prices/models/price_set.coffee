backbone = require 'backbone'


class PriceSetModel extends backbone.Model
  defaults:
    markup_percentage: 0
    price_set_kind: 1

  validation:
    client: [
      {
        required: true,
        msg: 'Заполните партнера'
      }]
    price_set_kind: [
      {
        required: true,
        msg: 'Заполните тип установки цен'
      }
    ]
    markup_percentage: [
      {
        required: true,
        msg: 'Заполните процент наценки'
      }
    ]

  url: =>
    if @id
      "/api/prices/price_set/#{@id}/"
    else
      "/api/prices/price_set/"

module.exports = PriceSetModel
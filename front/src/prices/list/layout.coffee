$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'

PriceSetCollection = require '../collections/price_set'
PriceSetModel = require '../models/price_set'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onClickAdd'

  onClickAdd: =>
    addNewView = new NewPriceSetView
      model: new PriceSetModel

    bus.vent.trigger 'modal:show', addNewView,
      title: 'Добавить новый документ установки цен'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      client: '[name=client]'
      items__order_item__product: '[name=items__order_item__product]'
      sort_by: '.sort_list'

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: "/api/clients/client/"
      placeholder: 'Выберите партнера'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

    helpers.initAjaxSelect2 @$('[name=items__order_item__product]'),
      url: '/api/products/product/'
      placeholder: "Выберите модель"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"


class PriceSetComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class PriceSetComplexListView extends complex.ComplexCompositeView
  childView: PriceSetComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class PriceSetComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2


class NewPriceSetView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    client: 'Партнер'

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      client: '[name=client]'
      markup_percentage: '[name=markup_percentage]'
      price_set_kind: '[name=price_set_kind]'

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: "/api/clients/client/"
      placeholder: 'Выберите партнера'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          backbone.history.navigate "/price_set/#{model.get('id')}/", {trigger: true}
          helpers.generate_notyfication('success', 'Документ создан')
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    @collection = new PriceSetCollection

    @filter_model = new FilterModel(urlParams)
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @filter_view = new FilterView
      model: @filter_model

  onAdded: =>
    @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'price_set'
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @region_filter.show(@filter_view)

    list_view = new PriceSetComplexListView
      collection: @collection

    @complex_view = new PriceSetComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'price_set'

    @region_complex.show(@complex_view)

    @listenTo @complex_view, 'childview:click', (view, options) =>
      backbone.history.navigate "/price_set/#{view.model.id}/", {trigger: true}

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @complex_view.doFilter()

module.exports = LayoutView
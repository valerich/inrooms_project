$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

DocumentScanCollection = require '../collections/document_scan'
DocumentScanModel = require '../models/document_scan'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onAddClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onAddClick: =>
    addNewView = new NewDocumentScanView
      model: new DocumentScanModel

    bus.vent.trigger 'modal:show', addNewView,
      title: 'Добавить новый документ'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      client: '[name=client]'
      kind: '[name=kind]'
      status: '[name=status]'
      sort_by: '.sort_list'

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: 'Введите клиента'
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

    helpers.initAjaxSelect2 @$('[name=kind]'),
      url: '/api/documents/document_scan_kind/'
      placeholder: 'Введите тип документа'
      minimumInputLength: -1
      allowClear: true

  serializeData: =>
    @userCanAddDocumentScan = helpers.user_has_permission bus.cache.user, 'can_add_document_scan'
    @userCanAddDocumentScanWithClient = helpers.user_has_permission bus.cache.user, 'can_document_scan_add_with_client'
    @userCanViewAllDocumentScan = helpers.user_has_permission bus.cache.user, 'can_all_document_scan_view'
    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanAddDocumentScan'] = [
      @userCanAddDocumentScan,
      @userCanAddDocumentScanWithClient,
    ].some(@isTrue)
    s_data['user']['userCanViewAllDocumentScan'] = @userCanViewAllDocumentScan
    s_data

  isTrue: (item) =>
    item == true


class DocumentScanComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    @userCanViewAllDocumentScan = helpers.user_has_permission bus.cache.user, 'can_all_document_scan_view'
    s_data['user']['userCanViewAllDocumentScan'] = @userCanViewAllDocumentScan
    s_data


class DocumentScanComplexListView extends complex.ComplexCompositeView
  childView: DocumentScanComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    @userCanViewAllDocumentScan = helpers.user_has_permission bus.cache.user, 'can_all_document_scan_view'
    s_data['user']['userCanViewAllDocumentScan'] = @userCanViewAllDocumentScan
    s_data


class DocumentScanComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class NewDocumentScanView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    kind: 'Тип документа'
    client: 'Партнер'

  initialize: =>
    @binder = new modelbinder
    @userCanAddDocumentScanWithClient = helpers.user_has_permission bus.cache.user, 'can_document_scan_add_with_client'

  onRender: =>
    if @userCanAddDocumentScanWithClient
      @binder.bind @model, @$el,
        kind: '[name=kind]'
        client: "[name=client]"

      helpers.initAjaxSelect2 @$('[name=client]'),
        url: '/api/clients/client/'
        placeholder: 'Введите клиента'
        minimumInputLength: -1
        allowClear: true
        get_extra_search_params: () =>
          kind: 1
    else
      @binder.bind @model, @$el,
        kind: '[name=kind]'

    backbone.Validation.bind @

    helpers.initAjaxSelect2 @$('[name=kind]'),
      url: '/api/documents/document_scan_kind/'
      placeholder: 'Введите тип документа'
      minimumInputLength: -1
      allowClear: true

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Документ создан')
          @destroy()
          backbone.history.navigate "/document_scan/#{model.id}/", trigger: true
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanAddDocumentScanWithClient'] = @userCanAddDocumentScanWithClient
    s_data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @collection = new DocumentScanCollection
    @filter_model = new FilterModel
    @list_view = new DocumentScanComplexListView
      collection: @collection

    @complex_view = new DocumentScanComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'document_scans'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'document_scan'
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

    @listenTo @complex_view.list_view, 'childview:click', (view)=>
      backbone.history.navigate "/document_scan/#{view.model.id}/", trigger: true

  onAdded: =>
    @complex_view.doFilter()


module.exports = LayoutView
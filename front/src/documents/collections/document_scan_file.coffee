backbone = require 'backbone'

DocumentScanFileModel = require '../models/document_scan_file'


class DocumentScanFileCollection extends backbone.Collection
  model: DocumentScanFileModel

  initialize: (options) =>
    @document_scan_id = options.document_scan_id

  url: =>
    "/api/documents/document_scan/#{@document_scan_id}/files/"

module.exports = DocumentScanFileCollection
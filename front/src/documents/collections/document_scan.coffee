backbone = require 'backbone'

DocumentScanModel = require '../models/document_scan'


class DocumentScanCollection extends backbone.Collection
  model: DocumentScanModel
  url: "/api/documents/document_scan/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = DocumentScanCollection

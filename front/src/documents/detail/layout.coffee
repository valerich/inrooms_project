marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

CommonLayout = require './common/layout'
FileLayout = require './file/layout'

DocumentScanModel = require '../models/document_scan'


class NavbarView extends marionette.ItemView
  template: require './templates/navbar'
  events:
    'click .tab_link': 'onClickTabLink'
  initialize: (options) =>
    @tab_name = options.tab_name
  onRender: =>
    @$(".tab_link[data-tab_name=#{@tab_name}]").addClass('active')
  onClickTabLink: (event) =>
    event.preventDefault()
    tab_name = $(event.target).parent().data('tab_name')
    @$(".tab_link").removeClass('active')
    @$(".tab_link[data-tab_name=#{tab_name}]").addClass('active')
    @trigger 'click', tab_name


class StatusLayout extends marionette.CompositeView
  template: require './templates/status'
  events:
    'click .to_cancel_status': 'onCancelClick'
    'click .to_approved_status': 'onApprovedClick'

  onCancelClick: =>
    @changeStatus(10)

  onApprovedClick: =>
    @changeStatus(2)

  changeStatus: (status_id) =>
    $.ajax
      url: "/api/documents/document_scan/#{@model.id}/change_status/"
      type: 'post'
      data: {
        'code': status_id
      }
      success: =>
        helpers.generate_notyfication('success', 'Статус изменен')
        backbone.history.navigate "/document_scan/", trigger: true
      error: (data) =>
        helpers.modelErrorHandler @model, data

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    @userCanStatusChange = helpers.user_has_permission bus.cache.user, 'can_document_scan_status_change'
    s_data['user']['userCanStatusChange'] = [
      @userCanStatusChange,
      @model.get('status') == 1].every(@isTrue)
    s_data

  isTrue: (item) =>
    item == true


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'task_detail_layout'
  regions:
    region_tab: '.region_tab'
    region_navbar: '.region_navbar'
    region_status: '.region_status'
  events:
    'click .list_link': 'onClickListLink'

  onClickListLink: (event) =>
    event.preventDefault()
    backbone.history.navigate '/document_scan/', trigger: true

  initialize: (options) =>
    @tab_name = options.tab_name
    @binder = new modelbinder
    @model = new DocumentScanModel
      id: options.id

  onBeforeDestroy: =>
    @binder.unbind()

  renderTab: (tab_name) =>
    view_class = null
    if tab_name is 'common'
      view_class = CommonLayout
    if tab_name is 'files'
      view_class = FileLayout
    if view_class
      tab_view = new view_class
        model: @model
      @region_tab.show(tab_view)
    else
      @region_tab.empty()

  onRender: =>
    @binder.bind @model, @$el,
      breadcrumb_name: '.breadcrumb_name'
    bus.vent.trigger 'menu:active:set', null, 'document_scan'
    @model.fetch().done =>
      @model.set('breadcrumb_name', @model.get('id')+ ' ' + @model.get('kind_detail')['name'])
      @renderTab(@tab_name)
      navbar_view = new NavbarView
        model: @model
        tab_name: @tab_name
      status_view = new StatusLayout
        model: @model
      @region_navbar.show(navbar_view)
      @region_status.show(status_view)
      @listenTo navbar_view, 'click', (tab_name) =>
        @tab_name = tab_name
        backbone.history.navigate "/document_scan/#{@model.id}/#{tab_name}/", trigger: false
        @renderTab(tab_name)

module.exports = ObjectDetailLayout

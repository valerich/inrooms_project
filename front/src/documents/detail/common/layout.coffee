$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'
complex = require 'complex'
helpers = require 'helpers'


class ItemView extends marionette.CompositeView
  template: require './templates/item'

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    @userCanViewAllDocumentScan = helpers.user_has_permission bus.cache.user, 'can_all_document_scan_view'
    s_data['user']['userCanViewAllDocumentScan'] = @userCanViewAllDocumentScan
    s_data

class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'

  initialize: (options)=>
    @document_scan = options.model

    @item_view = new ItemView
      model: @document_scan

  onRender: =>
    @region_item.show(@item_view)


module.exports = LayoutView
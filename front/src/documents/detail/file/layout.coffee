$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'
complex = require 'complex'
helpers = require 'helpers'
require 'blueimp-file-upload'
bootbox = require 'bootbox'

DocumentScanFileCollection = require '../../collections/document_scan_file'


class DocumentScanFileComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/file_item'

  triggers:
    'click .link': 'click'

  events:
    'click .delete': 'onClickDelete'

  initialize: (options) =>
    @document_scan_model = options.document_scan_model

  onClickDelete: =>
    bootbox.confirm "Вы действительно хотите удалить файл?", (result)=>
      if result == true
        $.ajax
          url: "/api/documents/document_scan/file_delete/"
          data:
            "file_id": @model.id
          type: 'post'
          success: (data) =>
            helpers.generate_notyfication('success', 'Удалено')
            bus.vent.trigger 'file:delete'
          error: (data) =>
            helpers.modelErrorHandler @model, data

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['canChangeFiles'] = @document_scan_model.get('status') != 2
    s_data


class DocumentScanFileComplexListView extends complex.ComplexCompositeView
  childView: DocumentScanFileComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/file_items'

  initialize: (options) =>
    @document_scan_model = options.document_scan_model

  childViewOptions: (model, index) =>
    data = {
      document_scan_model: @document_scan_model,
      childIndex: index
    }
    return data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.files'

  events:
    'click .add': 'onAddClick'

  initialize: (options) =>
    @document_scan_model = options.model
    @collection = new DocumentScanFileCollection
      document_scan_id: @document_scan_model.id
    @list_view = new DocumentScanFileComplexListView
      collection: @collection
      document_scan_model: @document_scan_model

  onRender: =>
    @region_complex.show(@list_view)
    @collection.fetch()

    @listenTo(bus.vent, 'file:delete', @onDeleteFile)

    @listenTo @list_view, 'childview:click', (view)=>
      win = window.open(view.model.get('file'), '_blank')
      win.focus()

  onDeleteFile: =>
    @collection.fetch()

  onAddClick: =>
    @$('.fileupload').fileupload
      url: "/api/documents/document_scan/#{@model.id}/file_upload/"
      dataType: 'json'
      submit: (e, data) =>
        $('#music_add_btn').removeClass('fa-plus-circle')
        $('.add').removeClass('btn-primary')
        $('.add').addClass('btn-default')
        $('#music_add_btn').disabled = true
        $('#music_add_btn').addClass('fa-spinner')
      always: (e, data) =>
        $('#music_add_btn').addClass('fa-plus-circle')
        $('.add').addClass('btn-primary')
        $('.add').removeClass('btn-default')
        $('#music_add_btn').disabled = false
        $('#music_add_btn').removeClass('fa-spinner')
      done: (e, response) =>
        @collection.fetch()
      fail: (e, response) =>
        helpers.generate_notyfication('error', 'не поддерживаемый тип файла')

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['canChangeFiles'] = @model.get('status') != 2
    s_data


module.exports = LayoutView
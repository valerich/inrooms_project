backbone = require 'backbone'


class DocumentScanModel extends backbone.Model

  url: =>
    if @id
      "/api/documents/document_scan/#{@id}/"
    else
      "/api/documents/document_scan/"

module.exports = DocumentScanModel
$ = require 'jquery'
global.jQuery = $
backbone = require 'backbone'
backbone.$ = $
marionette = require 'backbone.marionette'
#require 'backbone.localstorage'

require './common/csrf'
require './common/behaviors'
require './common/validation'
require './common/settings'
require './common/handlebars_helpers'

ModalRegion = require './common/region-modal'

#_ = require 'underscore'
bus = require 'bus'
helpers = require 'helpers'

#store = require 'store'
#require 'bootstrap'



#NavRegion = require './common/region-nav'
SidebarView = require './core/sidebar'
TopbarView = require './core/topbar'


class RootView extends marionette.LayoutView
  el: 'body'
  regions:
    modalRegion:
      selector: '#modal-region'
      regionClass: ModalRegion
    modalRegion2:
      selector: '#modal-region2'
      regionClass: ModalRegion
#    sidebarRegion: '.sidebar'
    mainRegion: '#region_main'

  initialize: (options) =>
    @sidebar_view = new SidebarView()
    @topbar_view = new TopbarView()


class MyApp extends marionette.Application
  hide_loader: =>
    setTimeout ( ->
      $('.loader-overlay').addClass('loaded')
      $('body > section').animate({
        opacity: 1,
      }, 400)
    ), 500


app = new MyApp()

app.rootView = new RootView

bus.vent.on "modal:show", (view, options) ->
  app.rootView.modalRegion.show view, options


bus.vent.on "modal2:show", (view, options) ->
  app.rootView.modalRegion2.show view, options


class Router extends marionette.AppRouter
  routes:
    '': 'dashboardLayout'
    'buyer_info/': 'buyerInfoLayout'
    'employees_work_schedule/': 'employeesWorkScheduleList'
    'employees_work_schedule/:id/': 'employeesWorkScheduleDetail'
    'subscriber/': 'subscriberList'
    'preorder/': 'preorderList'
    'preorder/:id/': 'preorderDetailRedirect'
    'preorder/:id/:tab_name/': 'preorderDetail'
    'sales_plan/': 'salePlanList'
    'sales_plan/:id/': 'salePlanDetail'
    'season_product_collection_plan/': 'SeasonProductCollectionPlanList'
    'season_product_collection_plan/:id/': 'SeasonProductCollectionPlanDetail'
    'holder_print/': 'holderPrint'
    'video/': 'videoList'
    'video/:id/': 'videoDetail'
    'paying/': 'payingList'
    'paying/:id/': 'payingDetailRedirect'
    'paying/:id/:tab_name/': 'payingDetail'
    'buyer_profile/': 'buyerProfileList'
    'buyer_profile/:id/': 'buyerProfileDetailRedirect'
    'buyer_profile/:id/:tab_name/': 'buyerProfileDetail'
    'newsletter/': 'newsletterList'
    'newsletter/:id/': 'newsletterDetailRedirect'
    'newsletter/:id/:tab_name/': 'newsletterDetail'
    'refund_act/': 'refundActList'
    'refund_act/:id/': 'refundActDetailRedirect'
    'refund_act/:id/:tab_name/': 'refundActDetail'
    'defect_act/': 'defectActList'
    'defect_act/:id/': 'defectActDetailRedirect'
    'defect_act/:id/:tab_name/': 'defectActDetail'
    'client/': 'clientList'
    'client_statistic/': 'clientStatisticList'
    'client/:id/': 'clientDetailRedirect'
    'client/:id/:tab_name/': 'clientDetail'
    'price_set/': 'priceSetList'
    'price_set/:id/': 'priceSetDetailRedirect'
    'price_set/:id/:tab_name/': 'priceSetDetail'
    'document_scan/': 'documentScanList'
    'document_scan/:id/': 'documentScanDetailRedirect'
    'document_scan/:id/:tab_name/': 'documentScanDetail'
    'provider/': 'providerList'
    'provider/:id/': 'providerDetailRedirect'
    'provider/:id/:tab_name/': 'providerDetail'
    'task/': 'taskList'
    'task/:id/': 'taskDetailRedirect'
    'task/:id/:tab_name/': 'taskDetail'
    'user/': 'userList'
    'user/:id/': 'userRedirect'
    'user/:id/:tab_name/': 'userDetail'
    'group/': 'groupList'
    'group/:id/': 'groupDetail'
    'aggreement-offer/': 'aggreementOffer'
    'aggreement-offer/cart/': 'aggreementOfferCartRedirect'
    'aggreement-offer/cart/:tab_name/': 'aggreementOfferCartDetail'
    'offer-requests/': 'offerRequestsList'
    'offer-requests/processing/': 'offerRequestsProcessing'
    'offer-requests/:id/': 'offerRequestsDetail'
    'offer-requests/:id/': 'offerRequestsRedirect'
    'offer-requests/:id/:tab_name/': 'offerRequestsDetail'
    'offer/': 'offerList'
    'offer-draft/': 'offerDraftList'
    'offer-deleted/': 'offerDeletedList'
    'offer-archive/': 'offerArchiveList'
    'offer-approved/': 'offerApprovedList'
    'offer-approving/': 'offerApprovingList'
    'offer-unread-comments/': 'offerUnreadCommentsList'
    'invoice/': 'invoiceList'
    'invoice/:id/': 'invoiceDetailRedirect'
    'invoice/:id/:tab_name/': 'invoiceDetail'
    'order/': 'orderList'
    'order/:id/': 'orderDetailRedirect'
    'order/:id/:tab_name/': 'orderDetail'
    'order_b2c/': 'orderB2CList'
    'order_b2c/:id/': 'orderB2CDetail'
    'order_return/': 'orderReturnList'
    'order_return/:id/': 'orderReturnDetailRedirect'
    'order_return/:id/:tab_name/': 'orderReturnDetail'
    'order_from_site/': 'orderFromSiteList'
    'order_from_site/:id/': 'orderFromSiteDetailRedirect'
    'order_from_site/:id/:tab_name/': 'orderFromSiteDetail'
    'order_return_b2c/': 'orderReturnB2CList'
    'order_return_b2c/:id/': 'orderReturnB2CDetailRedirect'
    'order_return_b2c/:id/:tab_name/': 'orderReturnB2CDetail'
    'product/': 'productList'
    'product/:id/': 'productDetailRedirect'
    'product/:id/:tab_name/': 'productDetail'
    'shop_equipment/': 'shop_equipmentList'
    'shop_equipment/:id/': 'productDetailRedirect'
    'shop_equipment/:id/:tab_name/': 'productDetail'
    'promo/': 'promoList'
    'promo/:id/': 'productDetailRedirect'
    'promo/:id/:tab_name/': 'productDetail'
    'lighting/': 'lightingList'
    'lighting/:id/': 'productDetailRedirect'
    'lighting/:id/:tab_name/': 'productDetail'
    'product_label/': 'productLabelList'
    'factory/': 'factoryList'
    'factory/:id/': 'factoryDetail'
    'manufacturing_order/': 'manufacturingOrderList'
    'manufacturing_order-factory/': 'manufacturingOrderFactoryList'
    'manufacturing_order-offer/': 'manufacturingOrderOfferList'
    'manufacturing_order/:id/': 'manufacturingOrderDetailRedirect'
    'manufacturing_order/:id/:tab_name/': 'manufacturingOrderDetail'
    'manufacturing_label_order/': 'manufacturingLabelOrderList'
    'manufacturing_label_order/:id/': 'manufacturingLabelOrderDetailRedirect'
    'manufacturing_label_order/:id/:tab_name/': 'manufacturingLabelOrderDetail'
    'shop/catalog/': 'shopCatalog'
    'shop/cart/': 'shopCartRedirect'
    'shop/cart/:tab_name/': 'cartDetail'
    'supply/': 'supplyList'
    'supply/:id/': 'supplyDetailRedirect'
    'supply/:id/:tab_name/': 'supplyDetail'
    'own_store/': 'ownStoreList'
    'own_store/:id/': 'ownStoreDetail'
    'store/': 'storeList'
    'store/:id/': 'storeDetail'
    'employee/': 'employeeList'
    'employee/:id/': 'employeeDetail'
    'purchase/': 'purchaseList'
    'purchase/:id/': 'purchaseDetailRedirect'
    'purchase/:id/:tab_name/': 'purchaseDetail'
    'warehouse/': 'warehouseList'
    'warehouse/:id/': 'warehouseDetail'
    'report/client_statistic/': 'reportClientStatistic'
    'report/products_not_sold/': 'reportProductsNotSold'
    'report/remain/': 'reportRemain'
    'report/movement_of_goods/': 'reportMovementOfGoods'
    'report/sales_plan/': 'reportSalesPlan'
    'report/sales_plan_project/': 'reportSalesPlanProject'
    'report/employee_sale/': 'reportEmployeeSale'
    'report/mutual_payments/': 'reportMutualPayments'
    'report/office_plan/': 'reportOfficePlan'
    'report/consolidated/': 'reportConsolidated'
    'report/partner_day_statistic/': 'reportPartnerDayStatistic'
    'report/employee_day_statistic/': 'reportEmployeeDayStatistic'
    'payment_account/': 'paymentAccountList'
    'payment_account/transfers/': 'paymentAccountTransfersList'
    'payment_account/:id/': 'paymentAccountDetail'
    'payment_account_for_user/:user_id/': 'paymentAccountListForUser'
    'payment_user/': 'paymentUserList'
    'expense_item/': 'expenseItemList'
    'expense_item/:id/': 'expenseItemDetail'
    'multimedia/music/': 'musicList'

  initialize: (options) =>
    @content_region = options.content_region

  subscriberList: =>
    View = require './subscriber/list/layout'
    view = new View
    @content_region.show(view)

  buyerInfoLayout: =>
    View = require './buyer_profiles/info/layout'
    view = new View
    @content_region.show(view)

  employeesWorkScheduleList: (id) =>
    View = require './employees_work_schedule/list/layout'
    view = new View
    @content_region.show(view)

  employeesWorkScheduleDetail: (id) =>
    View = require './employees_work_schedule/detail/layout'
    view = new View
      store_id: id
    @content_region.show(view)

  preorderList: =>
    View = require './preorders/list/layout'
    view = new View
    @content_region.show(view)

  preorderDetail: (id, tab_name) =>
    View = require './preorders/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  preorderDetailRedirect: (id) =>
    backbone.history.navigate "/preorder/#{id}/items/",
      trigger: true
      replace: true

  videoList: =>
    View = require './multimedia/video_list/layout'
    view = new View
    @content_region.show(view)

  videoDetail: (id) =>
    View = require './multimedia/video_detail/layout'
    view = new View
      id: id
    @content_region.show(view)

  payingList: =>
    View = require './paying/list/layout'
    view = new View
    @content_region.show(view)

  payingDetail: (id, tab_name) =>
    View = require './paying/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  payingDetailRedirect: (id) =>
    backbone.history.navigate "/paying/#{id}/common/",
      trigger: true
      replace: true

  buyerProfileList: =>
    View = require './buyer_profiles/list/layout'
    view = new View
    @content_region.show(view)

  buyerProfileDetail: (id, tab_name) =>
    View = require './buyer_profiles/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  buyerProfileDetailRedirect: (id) =>
    backbone.history.navigate "/buyer_profile/#{id}/common/",
      trigger: true
      replace: true

  holderPrint: =>
    View = require './holder_print/layout'
    view = new View
    @content_region.show(view)

  dashboardLayout: =>
    View = require './dashboard/layout'
    view = new View
    @content_region.show(view)

  musicList: =>
    View = require './multimedia/list/layout'
    view = new View
    @content_region.show(view)

  priceSetList: =>
    View = require './prices/list/layout'
    view = new View
    @content_region.show(view)

  priceSetDetailRedirect: (id) ->
    backbone.history.navigate "/price_set/#{id}/common/",
      trigger: true
      replace: true

  priceSetDetail: (id, tab_name)=>
    View = require './prices/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  newsletterList: =>
    View = require './sms_newsletters/list/layout'
    view = new View
    @content_region.show(view)

  newsletterDetailRedirect: (id) ->
    backbone.history.navigate "/newsletter/#{id}/common/",
      trigger: true
      replace: true

  newsletterDetail: (id, tab_name)=>
    View = require './sms_newsletters/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  refundActList: =>
    View = require './refund_acts/list/layout'
    view = new View
    @content_region.show(view)

  refundActDetailRedirect: (id) ->
    backbone.history.navigate "/refund_act/#{id}/item/",
      trigger: true
      replace: true

  refundActDetail: (id, tab_name)=>
    View = require './refund_acts/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  salePlanList: =>
    View = require './sales_plans/list/layout'
    view = new View
    @content_region.show(view)

  salePlanDetail: (id)=>
    View = require './sales_plans/detail/layout'
    view = new View
      id: id
    @content_region.show(view)

  SeasonProductCollectionPlanList: =>
    View = require './season_product_collection_plan/list/layout'
    view = new View
    @content_region.show(view)

  SeasonProductCollectionPlanDetail: (id)=>
    View = require './season_product_collection_plan/detail/layout'
    view = new View
      id: id
    @content_region.show(view)

  defectActList: =>
    View = require './defect_acts/list/layout'
    view = new View
    @content_region.show(view)

  defectActDetailRedirect: (id) ->
    backbone.history.navigate "/defect_act/#{id}/item/",
      trigger: true
      replace: true

  defectActDetail: (id, tab_name)=>
    View = require './defect_acts/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  documentScanList: =>
    View = require './documents/list/layout'
    view = new View
    @content_region.show(view)

  documentScanDetailRedirect: (id) ->
    backbone.history.navigate "/document_scan/#{id}/files/",
      trigger: true
      replace: true

  documentScanDetail: (id, tab_name)=>
    View = require './documents/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  taskList: =>
    View = require './tasks/list/layout'
    view = new View
    @content_region.show(view)

  taskDetailRedirect: (id) ->
    backbone.history.navigate "/task/#{id}/common/",
      trigger: true
      replace: true

  taskDetail: (id, tab_name)=>
    View = require './tasks/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  clientList: =>
    View = require './clients/list/layout'
    view = new View
    @content_region.show(view)

  clientDetailRedirect: (id) ->
    backbone.history.navigate "/client/#{id}/common/",
      trigger: true
      replace: true

  clientDetail: (id, tab_name)=>
    View = require './clients/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  providerList: =>
    View = require './providers/list/layout'
    view = new View
    @content_region.show(view)

  providerDetailRedirect: (id) ->
    backbone.history.navigate "/provider/#{id}/common/",
      trigger: true
      replace: true

  providerDetail: (id, tab_name)=>
    View = require './providers/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  clientStatisticList: =>
    View = require './client_statistics/list/layout'
    view = new View
    @content_region.show(view)

  userList: =>
    View = require './users/list/layout'
    view = new View
    @content_region.show(view)

  userRedirect: (id) ->
    backbone.history.navigate "/user/#{id}/common/",
      trigger: true
      replace: true

  userDetail: (id, tab_name)=>
    View = require './users/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  groupList: =>
    View = require './groups/list/layout'
    view = new View
    @content_region.show(view)

  groupDetail: (id)=>
    View = require './groups/detail/layout'
    view = new View({id: id})
    @content_region.show(view)

  aggreementOffer: =>
    View = require './offers_aggreement/catalog/layout'
    view = new View
    @content_region.show(view)

  aggreementOfferCartRedirect: (id) ->
    backbone.history.navigate "/aggreement-offer/cart/items/",
      trigger: true
      replace: true

  aggreementOfferCartDetail: (tab_name)=>
    View = require './offers_aggreement/cart/layout'
    view = new View
      tab_name: tab_name
    @content_region.show(view)

  offerRequestsList: =>
    View = require './offer_requests/list/layout'
    view = new View
    @content_region.show(view)

  offerRequestsProcessing: =>
    View = require './offer_requests/processing/layout'
    view = new View
    @content_region.show(view)

  offerRequestsRedirect: (id) ->
    backbone.history.navigate "/offer-requests/#{id}/items/",
      trigger: true
      replace: true

  offerRequestsDetail: (id, tab_name)=>
    View = require './offer_requests/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  offerList: =>
    View = require './offers/list/layout'
    view = new View
    @content_region.show(view)

  offerDraftList: =>
    View = require './offers/list/layout'
    view = new View
      offer_list_type: 'draft'
    @content_region.show(view)

  offerDeletedList: =>
    View = require './offers/list/layout'
    view = new View
      offer_list_type: 'deleted'
    @content_region.show(view)

  offerArchiveList: =>
    View = require './offers/list/layout'
    view = new View
      offer_list_type: 'archive'
    @content_region.show(view)

  offerApprovedList: =>
    View = require './offers/list/layout'
    view = new View
      offer_list_type: 'approved'
    @content_region.show(view)

  offerUnreadCommentsList: =>
    View = require './offers/list/layout'
    view = new View
      offer_list_type: 'unread_comments'
    @content_region.show(view)

  offerApprovingList: =>
    View = require './offers/list/layout'
    view = new View
      offer_list_type: 'approving'
    @content_region.show(view)

  invoiceList: =>
    View = require './invoices/list/layout'
    view = new View
    @content_region.show(view)

  invoiceDetailRedirect: (id) ->
    backbone.history.navigate "/invoice/#{id}/common/",
      trigger: true
      replace: true

  invoiceDetail: (id, tab_name)=>
    View = require './invoices/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  orderList: =>
    View = require './orders/list/layout'
    view = new View
    @content_region.show(view)

  orderDetailRedirect: (id) ->
    backbone.history.navigate "/order/#{id}/items/",
      trigger: true
      replace: true

  orderDetail: (id, tab_name)=>
    View = require './orders/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  orderB2CList: =>
    View = require './orders_b2c/list/layout'
    view = new View
    @content_region.show(view)

  orderB2CDetail: (id)=>
    console.log 'ok ok'
    View = require './orders_b2c/detail/layout'
    view = new View
      id: id
    @content_region.show(view)

  orderReturnB2CList: =>
    View = require './orders_return_b2c/list/layout'
    view = new View
    @content_region.show(view)

  orderReturnB2CDetailRedirect: (id) ->
    backbone.history.navigate "/order_return_b2c/#{id}/common/",
      trigger: true
      replace: true

  orderReturnB2CDetail: (id, tab_name)=>
    console.log 'ok ok'
    View = require './orders_return_b2c/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  orderReturnList: =>
    View = require './orders_return/list/layout'
    view = new View
    @content_region.show(view)

  orderReturnDetailRedirect: (id) ->
    backbone.history.navigate "/order_return/#{id}/common/",
      trigger: true
      replace: true

  orderReturnDetail: (id, tab_name)=>

    View = require './orders_return/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  orderFromSiteList: =>
    console.log 'orderFromSiteList ok'
    View = require './orders_from_site/list/layout'
    view = new View
    @content_region.show(view)

  orderFromSiteDetailRedirect: (id) ->
    backbone.history.navigate "/order_from_site/#{id}/common/",
      trigger: true
      replace: true

  orderFromSiteDetail: (id, tab_name)=>
    console.log 'orderFromSiteDetail ok'
    View = require './orders_from_site/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  productList: =>
    View = require './products/list/layout'
    view = new View
      product_kind: 1
    @content_region.show(view)

  shop_equipmentList: =>
    View = require './products/list/layout'
    view = new View
      product_kind: 2
    @content_region.show(view)

  promoList: =>
    View = require './products/list/layout'
    view = new View
      product_kind: 3
    @content_region.show(view)

  lightingList: =>
    View = require './products/list/layout'
    view = new View
      product_kind: 4
    @content_region.show(view)

  productDetailRedirect: (id) ->
    backbone.history.navigate "/product/#{id}/label/",
      trigger: true
      replace: true

  productDetail: (id, tab_name)=>
    View = require './products/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  productLabelList: =>
    View = require './product_labels/list/layout'
    view = new View
    @content_region.show(view)

  factoryList: =>
    View = require './factories/list/layout'
    view = new View
    @content_region.show(view)

  factoryDetail: (id)=>
    View = require './factories/detail/layout'
    view = new View({id: id})
    @content_region.show(view)

  manufacturingOrderList: =>
    View = require './manufacturing/list/layout'
    view = new View
    @content_region.show(view)

  manufacturingOrderFactoryList: =>
    View = require './manufacturing/list/layout'
    view = new View
      kind: 'f'
    @content_region.show(view)

  manufacturingOrderOfferList: =>
    View = require './manufacturing/list/layout'
    view = new View
      kind: 'o'
    @content_region.show(view)

  manufacturingOrderDetailRedirect: (id) ->
    backbone.history.navigate "/manufacturing_order/#{id}/items/",
      trigger: true
      replace: true

  manufacturingOrderDetail: (id, tab_name)=>
    View = require './manufacturing/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  manufacturingLabelOrderList: =>
    View = require './manufacturing_label/list/layout'
    view = new View
    @content_region.show(view)

  manufacturingLabelOrderDetailRedirect: (id) ->
    backbone.history.navigate "/manufacturing_label_order/#{id}/items/",
      trigger: true
      replace: true

  manufacturingLabelOrderDetail: (id, tab_name)=>
    View = require './manufacturing_label/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  shopCatalog: =>
    View = require './shop/catalog/layout'
    view = new View
    @content_region.show(view)

  shopCartRedirect: (id) ->
    backbone.history.navigate "/shop/cart/items/",
      trigger: true
      replace: true

  cartDetail: (tab_name)=>
    console.log 'cartDetail'
    console.log tab_name
    View = require './shop/cart/layout'
    view = new View
      tab_name: tab_name
    @content_region.show(view)

  supplyList: =>
    View = require './supply/list/layout'
    view = new View
    @content_region.show(view)

  supplyDetailRedirect: (id) ->
    backbone.history.navigate "/supply/#{id}/items/",
      trigger: true
      replace: true

  supplyDetail: (id, tab_name)=>
    View = require './supply/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  ownStoreList: =>
    View = require './store/list/layout'
    view = new View
      is_own: true
    @content_region.show(view)

  ownStoreDetail: (id) ->
    View = require './store/detail/layout'
    view = new View
      id: id
      is_own: true
    @content_region.show(view)

  storeList: =>
    View = require './store/list/layout'
    view = new View
    @content_region.show(view)

  storeDetail: (id) ->
    View = require './store/detail/layout'
    view = new View
      id: id
    @content_region.show(view)

  employeeList: =>
    View = require './employees/list/layout'
    view = new View
    @content_region.show(view)

  employeeDetail: (id) ->
    View = require './employees/detail/layout'
    view = new View
      id: id
    @content_region.show(view)

  purchaseList: =>
    View = require './purchases/list/layout'
    view = new View
    @content_region.show(view)

  purchaseDetailRedirect: (id) ->
    backbone.history.navigate "/purchase/#{id}/items/",
      trigger: true
      replace: true

  purchaseDetail: (id, tab_name)=>
    View = require './purchases/detail/layout'
    view = new View
      id: id
      tab_name: tab_name
    @content_region.show(view)

  warehouseList: =>
    View = require './warehouses/list/layout'
    view = new View
    @content_region.show(view)

  warehouseDetail: (id)=>
    View = require './warehouses/detail/layout'
    view = new View({id: id})
    @content_region.show(view)

  reportClientStatistic: =>
    View = require './reports/client_statistic/layout'
    view = new View
    @content_region.show(view)

  reportMutualPayments: =>
    View = require './reports/mutual_payments/layout'
    view = new View
    @content_region.show(view)

  reportSalesPlan: =>
    View = require './reports/sales_plan/layout'
    view = new View
    @content_region.show(view)

  reportSalesPlanProject: =>
    View = require './reports/sales_plan_project/layout'
    view = new View
    @content_region.show(view)

  reportEmployeeSale: =>
    View = require './reports/employee_sale/layout'
    view = new View
    @content_region.show(view)

  reportOfficePlan: =>
    View = require './reports/office_plan/layout'
    view = new View
    @content_region.show(view)

  reportConsolidated: =>
    View = require './reports/consolidated/layout'
    view = new View
    @content_region.show(view)

  reportPartnerDayStatistic: =>
    View = require './reports/partner_day_statistic/layout'
    view = new View
    @content_region.show(view)

  reportEmployeeDayStatistic: =>
    View = require './reports/employee_day_statistic/layout'
    view = new View
    @content_region.show(view)

  paymentAccountTransfersList: =>
    View = require './payments/transfers/layout'
    view = new View
    @content_region.show(view)

  paymentAccountList: =>
    View = require './payments/payment_account/list/layout'
    view = new View
    @content_region.show(view)

  paymentAccountListForUser: (user_id) =>
    View = require './payments/payment_account/list/layout'
    view = new View
      user_id: user_id
    @content_region.show(view)

  paymentAccountDetail: (id)=>
    View = require './payments/payment_account/detail/layout'
    view = new View({id: id})
    @content_region.show(view)

  paymentUserList: =>
    View = require './payments/payment_user/list/layout'
    view = new View
    @content_region.show(view)

  reportProductsNotSold: =>
    View = require './reports/products_not_sold/layout'
    view = new View
    @content_region.show(view)

  reportRemain: =>
    View = require './reports/remain/layout'
    view = new View
    @content_region.show(view)

  reportMovementOfGoods: =>
    View = require './reports/movement_of_goods/layout'
    view = new View
    @content_region.show(view)

  expenseItemList: =>
    View = require './payments/expense_item/list/layout'
    view = new View
    @content_region.show(view)

  expenseItemDetail: (id)=>
    View = require './payments/expense_item/detail/layout'
    view = new View({id: id})
    @content_region.show(view)


app.on 'before:start', ->
  content_region = new marionette.Region({el: '.page-content'})
  router = new Router({
    content_region: content_region,
  })

  # Прокручиваем страницу на верх при переходе между страницами
  router.on 'route', ->
    scrollTop = history.state && history.state.scrollTop || 0
    $("html,body").scrollTop(scrollTop)

  return router

app.on 'start', ->
  @sidebar_region = new marionette.Region({el: '.sidebar'})
  @sidebarInstance = new SidebarView
  @sidebar_region.show(@sidebarInstance)
  @topbar_region = new marionette.Region({el: '.topbar'})
  @topbarInstance = new TopbarView
  @topbar_region.show(@topbarInstance)

  backbone.history.start pushState: true

window.addEventListener 'load', ->
  bus.get_data([
    {name: 'intagcategory'}
  ]).then =>
    bus.get_user().then =>
      app.start()
      app.hide_loader()

if window.__backboneAgent
  window.__backboneAgent.handleBackbone(backbone)

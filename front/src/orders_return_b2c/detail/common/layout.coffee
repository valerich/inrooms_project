_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  initialize: (options) =>
    @order_model = options.model


module.exports = LayoutView

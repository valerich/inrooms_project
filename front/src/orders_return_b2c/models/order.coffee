backbone = require 'backbone'


class OrderModel extends backbone.Model

  url: =>
    if @id
      "/api/orders/order_return_b2c/#{@id}/"
    else
      "/api/orders/order_return_b2c/"

module.exports = OrderModel
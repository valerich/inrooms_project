backbone = require 'backbone'

OrderModel = require '../models/order'


class OrderCollection extends backbone.Collection
  model: OrderModel
  url: "/api/orders/order_return_b2c/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = OrderCollection
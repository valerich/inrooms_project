$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'

OrderCollection = require '../collections/order'
OrderModel = require '../models/order'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      items__product: '[name=items__product]'
      sort_by: '.sort_list'

    helpers.initAjaxSelect2 @$('[name=items__product]'),
      url: '/api/products/product/'
      placeholder: "Выберите модель"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"

  serializeData: =>
    @can_view_order_return_b2c_filter_products = helpers.user_has_permission bus.cache.user, 'can_view_order_return_b2c_filter_products'
    result = super
    result['user'] = bus.cache.user
    result['user']['can_view_order_return_b2c_filter_products'] = @can_view_order_return_b2c_filter_products
    result


class OrderComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class Order2ComplexItemView extends complex.ComplexItemView
  template: require './templates/item2'
  triggers:
    'click .panel': 'click'


class OrderComplexListView extends complex.ComplexCompositeView
  childView: OrderComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class Order2ComplexListView extends complex.ComplexCompositeView
  childView: Order2ComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items2'


class OrderComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'
  events:
    'click .list_type': 'onClickListType'

  initialize: (options) =>

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    @collection = new OrderCollection

    @filter_model = new FilterModel(urlParams)
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @filter_view = new FilterView
      model: @filter_model

  onClickListType: (event) =>
    $target = $(event.target)
    list_type = $target.data('list_type')
    @renderOrderList(list_type)

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'order_return_b2c'
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @region_filter.show(@filter_view)

    list_type = store.get 'order_list_type'
    if !list_type
      list_type = 'list1'
    @renderOrderList(list_type)

  renderOrderList: (list_type) =>
    store.set 'order_list_type', list_type
    if list_type is 'list1'
      OrderView = OrderComplexListView
    if list_type is 'list2'
      OrderView = Order2ComplexListView
    @$(".list_type").removeClass('active')
    @$(".list_type[data-list_type=#{list_type}]").addClass('active')

    list_view = new OrderView
      collection: @collection

    @complex_view = new OrderComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'orders'

    @region_complex.show(@complex_view)

    @listenTo @complex_view, 'childview:click', (view, options) =>
      backbone.history.navigate "/order_return_b2c/#{view.model.id}/", {trigger: true}

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @complex_view.doFilter()

module.exports = LayoutView
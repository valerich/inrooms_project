backbone = require 'backbone'

OfferArchiveModel = require '../models/offer-archive'


class OfferArchiveCollection extends backbone.Collection
  model: OfferArchiveModel
  url: "/api/offers/offer-archive/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = OfferArchiveCollection
backbone = require 'backbone'

HistoryItemModel = require '../models/offer-history-item'


class HistoryItemCollection extends backbone.Collection
  model: HistoryItemModel

  initialize: (options) =>
    @offer_id = options.offer_id

  url: =>
    "/api/history/history_item/offers/offer/#{@offer_id}/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = HistoryItemCollection
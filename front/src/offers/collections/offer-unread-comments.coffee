backbone = require 'backbone'

OfferUnreadCommentsModel = require '../models/offer-unread-comments'


class OfferUnreadCommentsCollection extends backbone.Collection
  model: OfferUnreadCommentsModel
  url: "/api/offers/offer-unread-comments/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = OfferUnreadCommentsCollection
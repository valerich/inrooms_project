backbone = require 'backbone'

OfferApprovedModel = require '../models/offer-approved'


class OfferApprovedCollection extends backbone.Collection
  model: OfferApprovedModel
  url: "/api/offers/offer-approved/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = OfferApprovedCollection
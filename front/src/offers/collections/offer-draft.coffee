backbone = require 'backbone'

OfferDraftModel = require '../models/offer-draft'


class OfferDraftCollection extends backbone.Collection
  model: OfferDraftModel
  url: "/api/offers/offer-draft/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = OfferDraftCollection
backbone = require 'backbone'

OfferModel = require '../models/offer'


class OfferCollection extends backbone.Collection
  model: OfferModel
  url: "/api/offers/offer/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = OfferCollection
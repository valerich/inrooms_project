backbone = require 'backbone'

OfferApprovingModel = require '../models/offer-approving'


class OfferApprovingCollection extends backbone.Collection
  model: OfferApprovingModel
  url: "/api/offers/offer-approving/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = OfferApprovingCollection
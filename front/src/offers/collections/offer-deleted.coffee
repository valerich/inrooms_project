backbone = require 'backbone'

OfferDeletedModel = require '../models/offer-deleted'


class OfferDeletedCollection extends backbone.Collection
  model: OfferDeletedModel
  url: "/api/offers/offer-deleted/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = OfferDeletedCollection
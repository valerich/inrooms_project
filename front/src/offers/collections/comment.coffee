backbone = require 'backbone'

CommentModel = require '../models/comment'


class CommentCollection extends backbone.Collection
  model: CommentModel

  initialize: (options) =>
    @url = "/api/comments/comment/offers/offer/#{options.object_id}/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = CommentCollection
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
complex = require 'complex'
marionette = require 'backbone.marionette'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
modelbinder = require 'backbone.modelbinder'


class OfferSummaryModel extends backbone.Model
  url: '/api/offers/offer/summary/'


class LayoutView extends marionette.LayoutView
  show_header: false
  template: require './templates/layout'

  events:
    'click .region_draft': 'onClickRegionDraft'
    'click .region_approving': 'onClickRegionApproving'
    'click .region_approved': 'onClickRegionApproved'
    'click .region_unread_comments': 'onClickRegionUnreadComments'

  initialize: (options) =>
    @userCanViewOffers = helpers.user_has_permission bus.cache.user, 'can_view_offers_widget'
    @listenTo(bus.vent, 'offers:widget:update', @OfferWidgetUpdate)
    @model = new OfferSummaryModel
    if options.show_header
      @show_header = options.show_header

    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    if @userCanViewOffers
      @OfferWidgetUpdate()
      @binder.bind @model, @$el,
        draft_all: '.draft_all'
        draft_to_work: '.draft_to_work'
        approving_all: '.approving_all'
        approving_to_work: '.approving_to_work'
        approved_all: '.approved_all'
        approved_to_work: '.approved_to_work'
        unread_comments: '.unread_comments'
        unread_comments_to_work: '.unread_comments_to_work'
      @$('[data-toggle="tooltip"]').tooltip();

  onClickRegionDraft: =>
    backbone.history.navigate 'offer-draft/?to_work=2', trigger: true

  onClickRegionApproving: =>
    backbone.history.navigate 'offer-approving/?to_work=2', trigger: true

  onClickRegionApproved: =>
    backbone.history.navigate 'offer-approved/?to_work=2', trigger: true

  onClickRegionUnreadComments: =>
    backbone.history.navigate 'offer-unread-comments/?', trigger: true

  serializeData: =>
    data = super
    data['show_header'] = @show_header
    data['userCanViewOffers'] = @userCanViewOffers
    data

  OfferWidgetUpdate: =>
    @model.fetch()

module.exports = LayoutView
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

OfferModel = require '../models/offer'


class OfferView extends marionette.ItemView
  template: require './templates/offer'


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'order_detail_layout'
  regions:
    region_order: '.region_offer'

  initialize: (options) =>
    console.log options.id
    @tab_name = options.tab_name
    @binder = new modelbinder
    @model = new OfferModel(id: options.id)

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>

    @model.fetch().done =>

      offer_view = new OfferView
        model: @model
      @region_order.show(offer_view)

module.exports = ObjectDetailLayout
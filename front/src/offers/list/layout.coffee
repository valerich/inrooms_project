$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
require 'magnific-popup'
bootbox = require 'bootbox'
require 'bootbox'
require 'jquery.inputmask'

OfferCollection = require '../collections/offer/'
OfferArchiveCollection = require '../collections/offer-archive'
OfferApprovedCollection = require '../collections/offer-approved'
OfferApprovingCollection = require '../collections/offer-approving'
OfferDraftCollection = require '../collections/offer-draft'
OfferDeletedCollection = require '../collections/offer-deleted'
OfferUnreadCommentsCollection = require '../collections/offer-unread-comments'
OfferModel = require '../models/offer'
OfferWidgetLayout = require '../widget/layout'
CommentModel = require '../models/comment'
CommentCollection = require '../collections/comment'
ImageLayout = require './image/layout'
NewProductModel = require '../../products/models/product'
ConsistencyLayout = require '../../consistency/layout'
PaymentInformationLayout = require './history/layout'
OfferEditView = require '../../offers_aggreement/catalog/offer_edit'
AggreementOfferModel = require '../../offers_aggreement/models/offer'
care_item_select2_item_template = require '../../care/templates/select2_item'


class FilterModel extends complex.FilterModel
  defaults:
    page: 1
    page_size: '10'
    page_count: 1


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'toggleFilterPanel'
    'click .add': 'onAddClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  toggleFilterPanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onAddClick: =>
    bootbox.confirm "Вы действительно хотите добавить предложение?", (result)=>
      if result == true
        model = new OfferModel
        model.save().done =>
          helpers.generate_notyfication('success', 'Предложение создано')
          bus.vent.trigger 'offer:added'
          bus.vent.trigger 'offers:widget:update'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @offer_list_type = options.offer_list_type

  serializeData: =>
    result = super
    if @offer_list_type == 'draft'
      result.show_add_btn = true
    else
      result.show_add_btn = false
    return result

  onRender: =>
    bindings = {
      search: '[name=search]'
      status: '[name=status]'
      user: '[name=user]'
      to_work: '[name=to_work]'
      to_aggreement: '[name=to_aggreement]'
      main_collection: '[name=main_collection]'
      has_main_collection: '[name=has_main_collection]'
    }

    @binder.bind @model, @$el,
      bindings

    helpers.initAjaxSelect2 @$('[name=status]'),
      url: "/api/offers/status/"
      placeholder: 'Выберите статус'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true
    helpers.initAjaxSelect2 @$('[name=user]'),
      url: '/api/accounts/user/'
      placeholder: "Выберите пользователя"
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true

    helpers.initAjaxSelect2 @$('[name=main_collection]'),
      url: '/api/products/collection/'
      placeholder: "Выберите главную категорию"
      minimumInputLength: -1
      text_attr: 'full_name'
      allowClear: true
      get_extra_search_params: () =>
        kind: 1


class NewProductView extends marionette.ItemView
  template: require './templates/new_product_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    name: 'Название'
    article: 'Артикул'
    main_collection: 'Категория'
    color: 'Цвет'
    price: 'Цена'
    brand: 'Бренд'
    seasonality: 'Сезонность'
    business_name: 'Коммерческое название'

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      name: '[name=name]'
      article: '[name=article]'
      main_collection: '[name=main_collection]'
      color: '[name=color]'
      price: '[name=price]'
      copy_images: '[name=copy_images]'
      brand: '[name=brand]'
      seasonality: '[name=seasonality]'
      business_name: '[name=business_name]'

    helpers.initAjaxSelect2 @$("[name=main_collection]"),
      url: '/api/products/collection/'
      placeholder: 'Введите название категории'
      minimumInputLength: -1
      text_attr: 'full_name'

    helpers.initAjaxSelect2 @$("[name=brand]"),
      url: '/api/brands/brand/'
      placeholder: 'Введите название бренда'
      minimumInputLength: -1

    helpers.initAjaxSelect2 @$("[name=color]"),
      url: '/api/products/color/'
      placeholder: 'Введите цвет'
      minimumInputLength: -1
      text_attr: 'name'
      createSearchChoicePosition: 'bottom'
      createSearchChoice: (term) =>
        return {
          id: term + '_4create'
          text: term + ' (создать)'
        }
    @$('[name=color]').on 'change', (event)=>
      url = "/api/products/color/"
      if event.added
        if /_4create/.test(event.added.id.toString())
          $.ajax
            url: url
            type: "post"
            data:
              name: event.added.id.replace('_4create', '')
            success: (data) =>
              @model.set('color', data.id)
              @$('[name=color]').val(data.id).change()
              helpers.generate_notyfication('success', 'Значение создано')

    @$('[name=article]').inputmask("9999/999")

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      if @model.get('price') == ''
        @model.set('price', null)
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Модель создана')
          if @redirect_to_product_list
            backbone.history.navigate "/product/?search=#{model.get('id')}", {trigger: true}
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    userCanSetPrice = helpers.user_has_permission bus.cache.user, 'can_set_offer_new_product_price'
    s_data['user']['userCanSetPrice'] = userCanSetPrice
    s_data


class OfferUpdateFromZaraView extends marionette.ItemView
  template: require './templates/offer_update_from_zara_modal'
  events:
    'click [name=update]': 'onUpdateBtnClick'
  translated_fields:
    url: 'Ссылка на страницу zara'

  initialize: (options)=>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      url: '[name=url]'

    backbone.Validation.bind @

  onUpdateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @$('[name=update]').removeClass('btn-success')
      @$('[name=update]').disabled = true
      @$('[name=update]').html('Идет обновление')
      @$('[name=update]').addClass('btn-default')
      $.ajax
        url: "/api/offers/offer/#{@model.id}/update_from_zara/"
        data:
          "url": @model.get('url')
        type: 'post'
        success: =>
          bus.vent.trigger 'offers:list:change'
          bus.vent.trigger 'offers:widget:update'
          helpers.generate_notyfication('success', 'Предложение обновлено')
          @destroy()
        error: (data) =>
          helpers.modelErrorHandler @model, data
          @$('[name=update]').removeClass('btn-default')
          @$('[name=update]').disabled = false
          @$('[name=update]').html('Обновить')
          @$('[name=update]').addClass('btn-success')
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()


class OfferSampleView extends marionette.ItemView
  template: require './templates/offer_sample_modal'
  events:
    'click [name=save]': 'onSaveBtnClick'

  initialize: (options)=>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      sample_date_order: '[name=sample_date_order]'
      sample_order_number: '[name=sample_order_number]'
      sample_payment_method: '[name=sample_payment_method]'

    @$('[name=sample_date_order]').datepicker
      autoclose: true

    backbone.Validation.bind @

  onSaveBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save()
      .done =>
        bus.vent.trigger 'offers:list:change'
        bus.vent.trigger 'offers:widget:update'
        helpers.generate_notyfication('success', 'Предложение обновлено')
        @destroy()
      .fail (data) ->
        helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()


class OfferLabelView extends marionette.ItemView
  template: require './templates/offer_label_modal'
  events:
    'click [name=save]': 'onSaveBtnClick'

  initialize: (options)=>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      consistency_value1: '[name=consistency_value1]'
      consistency_value2: '[name=consistency_value2]'
      consistency_value3: '[name=consistency_value3]'
      consistency_value4: '[name=consistency_value4]'
      consistency_type1: '[name=consistency_type1]'
      consistency_type2: '[name=consistency_type2]'
      consistency_type3: '[name=consistency_type3]'
      consistency_type4: '[name=consistency_type4]'
      wash: '[name=wash]'
      bleach: '[name=bleach]'
      drying: '[name=drying]'
      ironing: '[name=ironing]'
      professional_care: '[name=professional_care]'

    helpers.initAjaxSelect2 @$('[name=consistency_type1]'),
      url: '/api/consistency/consistency_item/'
      placeholder: 'Введите название состава'
      allowClear: true
      minimumInputLength: -1

    helpers.initAjaxSelect2 @$('[name=consistency_type2]'),
      url: '/api/consistency/consistency_item/'
      placeholder: 'Введите название состава'
      allowClear: true
      minimumInputLength: -1

    helpers.initAjaxSelect2 @$('[name=consistency_type3]'),
      url: '/api/consistency/consistency_item/'
      placeholder: 'Введите название состава'
      allowClear: true
      minimumInputLength: -1

    helpers.initAjaxSelect2 @$('[name=consistency_type4]'),
      url: '/api/consistency/consistency_item/'
      placeholder: 'Введите название состава'
      allowClear: true
      minimumInputLength: -1

    helpers.initAjaxSelect2 @$('[name=wash]'),
      url: '/api/care/care_item/'
      placeholder: 'Введите выберите параметры стирки'
      allowClear: true
      minimumInputLength: -1
      text_attr: (product) =>
       care_item_select2_item_template(product)
      get_extra_search_params: () =>
        kind: 1
        page_size: 100
      formatResult: (product) =>
        care_item_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    helpers.initAjaxSelect2 @$('[name=bleach]'),
      url: '/api/care/care_item/'
      placeholder: 'Введите выберите параметры отбеливания'
      allowClear: true
      minimumInputLength: -1
      text_attr: (product) =>
       care_item_select2_item_template(product)
      get_extra_search_params: () =>
        kind: 2
        page_size: 100
      formatResult: (product) =>
        care_item_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    helpers.initAjaxSelect2 @$('[name=drying]'),
      url: '/api/care/care_item/'
      placeholder: 'Введите выберите параметры сушки'
      allowClear: true
      minimumInputLength: -1
      text_attr: (product) =>
       care_item_select2_item_template(product)
      get_extra_search_params: () =>
        kind: 3
        page_size: 100
      formatResult: (product) =>
        care_item_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    helpers.initAjaxSelect2 @$('[name=ironing]'),
      url: '/api/care/care_item/'
      placeholder: 'Введите выберите параметры глажения'
      allowClear: true
      minimumInputLength: -1
      text_attr: (product) =>
       care_item_select2_item_template(product)
      get_extra_search_params: () =>
        kind: 4
        page_size: 100
      formatResult: (product) =>
        care_item_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    helpers.initAjaxSelect2 @$('[name=professional_care]'),
      url: '/api/care/care_item/'
      placeholder: 'Введите выберите параметры профессионального ухода'
      allowClear: true
      minimumInputLength: -1
      text_attr: (product) =>
       care_item_select2_item_template(product)
      get_extra_search_params: () =>
        kind: 5
      formatResult: (product) =>
        care_item_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    backbone.Validation.bind @

  onSaveBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save()
      .done =>
        bus.vent.trigger 'offers:list:change'
        bus.vent.trigger 'offers:widget:update'
        helpers.generate_notyfication('success', 'Предложение обновлено')
        @destroy()
      .fail (data) ->
        helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()


class OfferReturnedView extends marionette.ItemView
  template: require './templates/offer_returned_modal'
  events:
    'click [name=save]': 'onSaveBtnClick'
  translated_fields:
    cost: 'Стоимость'
    delivery_cost: 'Стоимость доставки'

  initialize: (options)=>
    @binder = new modelbinder
    @new_status_code = options.new_status_code
    @new_status_name = options.new_status_name

  onRender: =>
    @binder.bind @model, @$el,
      payment_account: '[name=payment_account]'

    helpers.initAjaxSelect2 @$('[name=payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        currency: 'CNY'
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}. #{obj.currency_detail.name} / #{obj.user_detail.name}"

    backbone.Validation.bind @

  onSaveBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: "/api/offers/offer/#{@model.id}/return_cost/"
        data:
          "payment_account": @model.get('payment_account')
        type: 'post'
        success: =>
          $.ajax
            url: "/api/offers/offer/#{@model.id}/change_status/"
            data: {"status_code": @new_status_code}
            type: 'post'
            success: =>
              bus.vent.trigger 'offers:list:change'
              bus.vent.trigger 'offers:widget:update'
              helpers.generate_notyfication('success', 'Статус изменен')
              @destroy()
            error: (data) =>
              helpers.modelErrorHandler @model, data
        error: (data) =>
          helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()

  serializeData: =>
    result = super
    result.new_status_name = @new_status_name
    return result


class OfferPrepaymentCostView extends marionette.ItemView
  template: require './templates/offer_prepayment_cost_modal'
  events:
    'click [name=save]': 'onSaveBtnClick'
  translated_fields:
    prepayment_cost: 'Заказ образца'
    prepayment_delivery_cost: 'Доставка образца с суммами'

  initialize: (options)=>
    @binder = new modelbinder
    @new_status_code = options.new_status_code
    @new_status_name = options.new_status_name

  onRender: =>
    @binder.bind @model, @$el,
      prepayment_cost: '[name=prepayment_cost]'
      prepayment_delivery_cost: '[name=prepayment_delivery_cost]'
      payment_account: '[name=payment_account]'

    helpers.initAjaxSelect2 @$('[name=payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        currency: 'CNY'
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}. #{obj.currency_detail.name} / #{obj.user_detail.name}"

    backbone.Validation.bind @

  onSaveBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: "/api/offers/offer/#{@model.id}/change_prepayment/"
        data:
          "prepayment_delivery_cost": @model.get('prepayment_delivery_cost')
          "prepayment_cost": @model.get('prepayment_cost')
          "payment_account": @model.get('payment_account')
        type: 'post'
        success: =>
          $.ajax
            url: "/api/offers/offer/#{@model.id}/change_status/"
            data: {"status_code": @new_status_code}
            type: 'post'
            success: =>
              bus.vent.trigger 'offers:list:change'
              bus.vent.trigger 'offers:widget:update'
              helpers.generate_notyfication('success', 'Статус изменен')
              @destroy()
            error: (data) =>
              helpers.modelErrorHandler @model, data
        error: (data) =>
          helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()

  serializeData: =>
    result = super
    result.new_status_name = @new_status_name
    return result


class OfferCostView extends marionette.ItemView
  template: require './templates/offer_cost_modal'
  events:
    'click [name=save]': 'onSaveBtnClick'
  translated_fields:
    cost: 'Стоимость'
    delivery_cost: 'Стоимость доставки'

  initialize: (options)=>
    @binder = new modelbinder
    @new_status_code = options.new_status_code
    @new_status_name = options.new_status_name
    @prepayment_cost = options.prepayment_cost
    @prepayment_delivery_cost = options.prepayment_delivery_cost

  onRender: =>
    @binder.bind @model, @$el,
      cost: '[name=cost]'
      delivery_cost: '[name=delivery_cost]'
      payment_account: '[name=payment_account]'

    helpers.initAjaxSelect2 @$('[name=payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        currency: 'CNY'
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}. #{obj.currency_detail.name} / #{obj.user_detail.name}"

    backbone.Validation.bind @

  onSaveBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: "/api/offers/offer/#{@model.id}/change_cost/"
        data:
          "delivery_cost": @model.get('delivery_cost')
          "cost": @model.get('cost')
          "payment_account": @model.get('payment_account')
        type: 'post'
        success: =>
          $.ajax
            url: "/api/offers/offer/#{@model.id}/change_status/"
            data: {"status_code": @new_status_code}
            type: 'post'
            success: =>
              bus.vent.trigger 'offers:list:change'
              bus.vent.trigger 'offers:widget:update'
              helpers.generate_notyfication('success', 'Статус изменен')
              @destroy()
            error: (data) =>
              helpers.modelErrorHandler @model, data
        error: (data) =>
          helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onBeforeDestroy: =>
    @binder.unbind()

  serializeData: =>
    result = super
    result.new_status_name = @new_status_name
    result.prepayment_cost = @prepayment_cost
    result.prepayment_delivery_cost = @prepayment_delivery_cost
    return result


class OfferComplexItemView extends marionette.ItemView
  template: require './templates/item'

  events:
    'click .image-link': 'onImageLinkClick'
    'click .image-edit': 'onImageEditClick'
    'click .offer-delete': 'onOfferDeleteClick'
    'click .offer-changestatus': 'onOfferChangeStatusClick'
    'click .offer-restore': 'onOfferRestoreClick'
    'click .create-product-btn': 'onClickCreateProduct'
    'click .save': 'saveModel'
    'click .get_offer': 'onClickGetOffer'
    'click .comment_create_btn': 'onCreateCommentBtnClick'
    'click .comment_read_button i': 'onClickCommentReadButton'
    'click .care_consistency': 'onCareConsistencyBtnClick'
    'click .care-information': 'onCareInformationBtnClick'
    'click .payment-information': 'onPaymentInformationBtnClick'
    "keyup [name=body]" : "keyPressEventHandler"
    'click .aggreement_edit': 'onClickAggreementEdit'
    'click .update_from_zara': 'onUpdateFromZaraClick'
    'click .sample-information': 'onClickSampleInformation'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onClickAggreementEdit: (event) =>
    event.preventDefault()
    aggreement_model = new AggreementOfferModel
       id: @model.id
       is_edit: true
    aggreement_model.fetch().done =>
      editOfferView = new OfferEditView
        model: aggreement_model

      bus.vent.trigger 'modal:show', editOfferView,
        title: 'Редактирование предложения'

  onClickSampleInformation: (event) =>
    event.preventDefault()
    view = new OfferSampleView
      model: @model

    bus.vent.trigger 'modal:show', view,
        title: 'Данные по образцу'

  onCareConsistencyBtnClick: =>
    careOptionsView = new OfferLabelView
      model: @model

    bus.vent.trigger 'modal:show', careOptionsView,
      title: 'Состав и уход'
      modal_size: 'lg'

  onPaymentInformationBtnClick: (event) =>
    event.preventDefault()

    paymentInformationView = new PaymentInformationLayout
      button_position: 'bottom'
      model: @model

    bus.vent.trigger 'modal:show', paymentInformationView,
      title: 'Информация об оплате'
      modal_size: 'lg'

  onClickCommentReadButton: (event) =>
    comment_id = $(event.target).parent().parent().data('comment_id')
    action = $(event.target).data('action')

    $.ajax
      url: "/api/comments/comment/" + comment_id + "/set_is_new/"
      type: 'post'
      data: {
        'action': action
      }
      success: (data) =>
        is_new = data['is_new']
        if is_new == true
          $('#comment_item_' + comment_id).addClass('comment_new')
        else
          $('#comment_item_' + comment_id).removeClass('comment_new')
      error: (data) =>
        helpers.generate_notyfication('error', 'Произошла ошибка при обновлении комментария')
    console.log comment_id
    console.log action

  fetching: =>
    @model.fetch()
    .done =>
      @.render()
      @scrollComments()
      @changeToAggreementSignal()

  scrollComments: =>
    $('.comments-list').animate({scrollTop:$(document).height()}, 0);

  changeToAggreementSignal: =>
    @.model.on( "change:to_aggreement", @saveModel, @ )
    @.model.on( "change:links", @saveModel, @ )

  onClickCreateProduct: =>
    addNewProductView = new NewProductView
      model: new NewProductModel
        name: @model.get 'name'
        business_name: @model.get 'business_name'
        currency: @model.get 'currency'
        offer: @model.get 'id'
        article: @model.get 'aggreement_code'
        price: @model.get 'aggreement_price'
        main_collection: @model.get 'main_collection'
        # Уход
        wash: @model.get 'wash'
        bleach: @model.get 'bleach'
        drying: @model.get 'drying'
        ironing: @model.get 'ironing'
        professional_care: @model.get 'professional_care'
        # Состав
        consistency_type1: @model.get 'consistency_type1'
        consistency_value1: @model.get 'consistency_value1'
        consistency_type2: @model.get 'consistency_type2'
        consistency_value2: @model.get 'consistency_value2'
        consistency_type3: @model.get 'consistency_type3'
        consistency_value3: @model.get 'consistency_value3'
        consistency_type4: @model.get 'consistency_type4'
        consistency_value4: @model.get 'consistency_value4'
        consistency_type5: @model.get 'consistency_type5'
        consistency_value5: @model.get 'consistency_value5'
        copy_images: true

    bus.vent.trigger 'modal:show', addNewProductView,
      title: 'Добавить новую модель'

  keyPressEventHandler: (event) =>
    if event.keyCode == 13
        @.$(".comment_create_btn").click()

  onCreateCommentBtnClick: (event)=>
    event.preventDefault()
    @comment_model = new CommentModel
      object_id: @model.id
    backbone.Validation.bind @, {'model': @comment_model}
    @comment_model.set({"body": $('.new_comment_body_' + @model.id).val()})

    v = @comment_model.validate()
    if @comment_model.isValid()
      @comment_model.save {},
        success: (model) =>
          @fetching()
          helpers.generate_notyfication('success', 'Комментарий создан')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  onImageLinkClick: =>
    $.magnificPopup.open({
      type:'image',
      gallery: {
        enabled: true
      },
      removalDelay: 300,
      mainClass: 'mfp-fade',
      items: @model.get('images'),
      gallery: {
        enabled: true
      },
    })

  onImageEditClick: =>
    editImageView = new ImageLayout
      model: @model
      button_position: 'bottom'

    bus.vent.trigger 'modal:show', editImageView,
      title: 'Изображения предложения'
      modal_size: 'lg'

  onClickGetOffer: =>
    bootbox.confirm "Вы действительно хотите начать обрабатывать предложение?", (result)=>
      if result == true
        $.ajax
          url: "/api/offers/offer/#{@model.id}/set_worker/"
          type: 'post'
          success: =>
            @fetching()
            bus.vent.trigger 'offers:widget:update'
            helpers.generate_notyfication('success', 'Восстановлено')
          error: (data) =>
            helpers.modelErrorHandler @model, data

  onOfferDeleteClick: =>
    bootbox.confirm "Вы действительно хотите удалить предложение?", (result)=>
      if result == true
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')
          bus.vent.trigger 'offers:widget:update'

  onOfferRestoreClick: =>
    bootbox.confirm "Вы действительно хотите восстановить предложение?", (result)=>
      if result == true
        $.ajax
          url: "/api/offers/offer-deleted/#{@model.id}/restore/"
          type: 'post'
          success: =>
            bus.vent.trigger 'offers:list:change'
            bus.vent.trigger 'offers:widget:update'
            helpers.generate_notyfication('success', 'Восстановлено')
          error: (data) =>
            helpers.modelErrorHandler @model, data

  onUpdateFromZaraClick: (event) =>
    view = new OfferUpdateFromZaraView
      model: @model

    bus.vent.trigger 'modal:show', view,
      title: 'Обновление данных с zara'

  onOfferChangeStatusClick: (event) =>
    button = $(event.currentTarget)
    new_status_name = button.data('original-title')
    new_status_code = button.data('statuscode')
    if new_status_code == 'sample_ready'
      changeOfferCostView = new OfferCostView
        model: @model
        new_status_code: new_status_code
        new_status_name: new_status_name
        prepayment_cost: @model.get('prepayment_cost')
        prepayment_delivery_cost: @model.get('prepayment_delivery_cost')

      bus.vent.trigger 'modal:show', changeOfferCostView,
        title: 'Изменение статуса предложения'
    else if new_status_code == 'sample_ordered'
      changeOfferPrepaymentCostView = new OfferPrepaymentCostView
        model: @model
        new_status_code: new_status_code
        new_status_name: new_status_name

      bus.vent.trigger 'modal:show', changeOfferPrepaymentCostView,
        title: 'Изменение статуса предложения'
    else if new_status_code == 'sample_returned'
      changeOfferReturnedView = new OfferReturnedView
        model: @model
        new_status_code: new_status_code
        new_status_name: new_status_name

      bus.vent.trigger 'modal:show', changeOfferReturnedView,
        title: 'Изменение статуса предложения'
    else
      bootbox.confirm "Вы действительно хотите изменить статус предложения с \"#{@model.get('status_detail')['name']}\" на \"#{new_status_name}\"?", (result)=>
        if result
          $.ajax
            url: "/api/offers/offer/#{@model.id}/change_status/"
            data: {"status_code": new_status_code}
            type: 'post'
            success: =>
              bus.vent.trigger 'offers:list:change', false
              bus.vent.trigger 'offers:widget:update'
              helpers.generate_notyfication('success', 'Статус изменен')
            error: (data) =>
              helpers.modelErrorHandler @model, data

  saveModel: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Предложение сохранено')
    .fail (data) ->
      helpers.modelErrorHandler @model, data

  onRender: =>
    @$('[data-toggle="tooltip"]').tooltip();
    bindings = {
      "name": "[name=name]"
      "links": "[name=links]"
      "to_aggreement": "[name=to_aggreement]"
      "aggreement_code": "[name=aggreement_code]"
      "main_collection": "[name=main_collection]"
    }

    if @model.get("can_edit_price", false)
      bindings["price"] = "[name=price]"

    if @model.get('status_detail')['to_edit_offer_data']
      bindings['series_type'] = "[name=series_type#{@model.id}]"

    @binder.bind @model, @$el, bindings


    @$('[name=links]').select2({
      tags: [],
      formatSelection: (item)->
        return '<a href="' + item.text + '" class="tag-linked" target="_blank" onclick=window.open("' + item.id + '","_blank");>' + item.text + '</a>'
    })
    @$('[name=aggreement_code]').inputmask("9999/999")
    helpers.initAjaxSelect2 @$('[name=main_collection]'),
      url: '/api/products/collection/'
      placeholder: "Выберите главную категорию"
      minimumInputLength: -1
      text_attr: 'full_name'
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

  onShow: =>
    @scrollComments()
    @changeToAggreementSignal()

  serializeData: =>
    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user_id'] = bus.cache.user['id']
    if @model.get('worker_detail') == null
      can_save = helpers.user_has_permission bus.cache.user, 'can_save_offers_blank_worker'
      can_get_blank_offer = helpers.user_has_permission bus.cache.user, 'can_get_offers_blank_worker'
    else
      can_save = true
      can_get_blank_offer = false
    status_code = @model.get('status_detail')['code']
    if status_code in ["approved", "models_created", "sample_ready"]
      can_create_model = true
    else
      can_create_model = false
    s_data['can_create_model'] = can_create_model
    s_data['can_save'] = can_save
    s_data['can_get_blank_offer'] = can_get_blank_offer
    s_data['cost'] = @model.get('cost')
    s_data['delivery_cost'] = @model.get('delivery_cost')
    s_data['can_edit_to_aggreement'] = @model.get('status_detail')['can_edit_to_aggreement']
    s_data['userCanLinksView'] = helpers.user_has_permission bus.cache.user, 'can_offers_links_view'
    s_data


class OfferComplexListView extends complex.ComplexCompositeView
  childView: OfferComplexItemView
  childViewContainer: ".region_offer_panel_items"
  template: require './templates/items'

  initialize: =>
    @listenTo(bus.vent, 'offer:images:update', @onOfferUpdate)

  onOfferUpdate: (offer_model) =>
    item_view = @.children.findByModel(offer_model)
    item_view.fetching()


class OfferComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  manyPaginators: true
  d: 2

  initialize: (options)=>
    OfferComplexView.__super__.initialize.apply(@, arguments);
    @offer_list_type = options.offer_list_type


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'
    'region_widgets': '.region_widgets'

  initialize: (options) =>
    @listenTo(bus.vent, 'offer:added', @onOfferAdded)
    @listenTo(bus.vent, 'offers:list:change', @onOffersListChange)

    @offer_list_type = options.offer_list_type

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    @filter_model = new FilterModel(urlParams)
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    if @offer_list_type == 'archive'
      @collection = new OfferArchiveCollection
    else if @offer_list_type == 'approved'
      @collection = new OfferApprovedCollection
    else if @offer_list_type == 'approving'
      @collection = new OfferApprovingCollection
    else if @offer_list_type == 'deleted'
      @collection = new OfferDeletedCollection
    else if @offer_list_type == 'unread_comments'
      @collection = new OfferUnreadCommentsCollection
    else if @offer_list_type == 'draft'
      @collection = new OfferDraftCollection
    else
      @collection = new OfferCollection
    @list_view = new OfferComplexListView
      collection: @collection

    @complex_view = new OfferComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'offers'
      offer_list_type: @offer_list_type

    @filter_view = new FilterView
      model: @filter_model
      offer_list_type: @offer_list_type

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

    @widgets_view = new OfferWidgetLayout

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'offer-' + @offer_list_type
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()
    @region_widgets.show(@widgets_view)

    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @listenTo(bus.vent, 'offer_aggreement:update', @complex_view.doFilter)

  onOfferAdded: =>
    @complex_view.doFilter()

  onOffersListChange: (scroll_top) =>
    if scroll_top != false
      scroll_top = true

    console.log 'scroll_top', scroll_top

    @complex_view.doFilter(scroll_top)

  serializeData: =>
    result = super
    if @offer_list_type == 'archive'
      breadcrumb_name = 'Архив'
    else if @offer_list_type == 'approved'
      breadcrumb_name = 'Заказ'
    else if @offer_list_type == 'approving'
      breadcrumb_name = 'Образец'
    else if @offer_list_type == 'deleted'
      breadcrumb_name = 'Корзина'
    else if @offer_list_type == 'unread_comments'
      breadcrumb_name = 'Не прочитанные сообщения'
    else if @offer_list_type == 'draft'
      breadcrumb_name = 'Новое'
    else
      breadcrumb_name = 'Предложения'
    result.breadcrumb_name = breadcrumb_name
    return result


module.exports = LayoutView
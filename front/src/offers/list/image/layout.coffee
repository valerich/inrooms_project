marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
Sortable = require 'sortablejs'
require 'blueimp-file-upload'
require 'magnific-popup'

class ImageModel extends backbone.Model
  initialize: =>
    @on 'change', (model, options) =>
      if @changed.visual_order isnt undefined or options.xhr
        return
      @save({patch: true}).done =>
        if @changed.visual_order is undefined
          helpers.generate_notyfication('success', 'Сохранено')
  url: =>
    return "/api/offers/image/#{@id}/"

class ImageCollection extends backbone.Collection
  model: ImageModel

  parse: (response) =>
    @count = response.count
    return response.results

class ImageItemView extends marionette.ItemView
  template: require './templates/image_item'
  className: 'image_item'
  events:
    'click .image-delete': 'onClickRemove'
    'click .image-link': 'onClickImageLink'

  onClickRemove: =>
    @model.destroy().done =>
      helpers.generate_notyfication('success', 'Удалено')
      bus.vent.trigger 'offer:images:update', @offer_model

  onClickImageLink: (event) =>
    $.magnificPopup.open
      type:'image'
      mainClass: 'mfp-fade'
      items: [{'src': @model.get('image')}]
      midClick: true

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @offer_model = options.offer_model

  onRender: (options) =>
    @$el.data('model_id', @model.id)


class ImageCollectionView extends marionette.CollectionView
  childView: ImageItemView
  className: 'image_list'

  initialize: (options) =>
    @offer_model = options.offer_model

  childViewOptions: (model, index) =>
    data = {
      offer_model: @offer_model,
      childIndex: index
    }
    return data

  onRender: =>
    @sortable = new Sortable @el,
      onSort: @onSort
      handle: '.image'

  onSort: (event) =>
    data = []
    @$el.children().each (index, el) =>
      model_id = $(el).data('model_id')
      data.push({
        id: model_id
        visual_order: index
      })
      model = @collection.find id: model_id
      model.set 'visual_order', index
    if data
      $.ajax
        type: 'post'
        url: '/api/offers/image-order/'
        contentType: 'application/json'
        data: JSON.stringify(data)
        success: =>
          helpers.generate_notyfication('success', 'Порядок сохранен')
          bus.vent.trigger 'offer:images:update', @offer_model


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  events:
    'click .tab_alert>button': 'closeAlert'
  show_alert_key: 'show_offer_image_alert'
  regions:
    region_image_list: '.region_image_list'
  className: 'image_layout'

  closeAlert: =>
    store.set(@show_alert_key, false)
    @$('.tab_alert').addClass('hide')

  serializeData: =>
    result = super
    if store.get(@show_alert_key) is undefined
      store.set(@show_alert_key, true)
    result.show_alert = store.get(@show_alert_key) is true
    result.button_position = @button_position
    return result

  initialize: (options) =>
    @image_collection = new ImageCollection
    @image_collection.url = "/api/offers/offer/#{@model.id}/image/"

    @image_view = new ImageCollectionView
      collection: @image_collection
      offer_model: @model

    if options.button_position isnt undefined
      @button_position = options.button_position
    else
      @button_position = 'top'

  onRender: =>
    @region_image_list.show(@image_view)
    @image_collection.fetch()

    @$('.fileupload').fileupload
      url: "/api/offers/offer/#{@model.id}/image-upload/"
      dataType: 'json'
      done: (e, response) =>
        @image_collection.fetch()
        bus.vent.trigger 'offer:images:update', @model


module.exports = LayoutView

_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

HistoryItemCollection = require '../../collections/offer-history-item'
HistoryItemModel = require '../../models/offer-history-item'


class HistoryItemItemView extends marionette.ItemView
  template: require './templates/history_item_item'
  className: 'history_items_item'

#  onBeforeDestroy: =>
#    @binder.unbind()
#
#  initialize: (options)=>
#    @binder = new modelbinder
#    @order_model = options.order_model


class HistoryItemCollectionView extends marionette.CollectionView
  childView: HistoryItemItemView
  className: 'history_items_list'

  initialize: (options) =>
    @offer_model = options.offer_model

#  childViewOptions: (model, index) =>
#    data = {
#      order_model: @order_model,
#      childIndex: index
#    }
#    return data



class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  regions:
    region_item_list: '.region_item_list'

  initialize: (options) =>
    @offer_model = options.model

  onRender: =>
    @history_item_collection = new HistoryItemCollection
      offer_id: @offer_model.id

    @history_item_view = new HistoryItemCollectionView
      collection: @history_item_collection
      offer_model: @offer_model

    @region_item_list.show(@history_item_view)

    @history_item_collection.fetch
      data: {
        'page_size': 100000
      }
      reset: true

module.exports = LayoutView

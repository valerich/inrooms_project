backbone = require 'backbone'


class OfferArchiveModel extends backbone.Model

  url: =>
    if @id
      "/api/offers/offer/#{@id}/"
    else
      "/api/offers/offer-archive/"

module.exports = OfferArchiveModel
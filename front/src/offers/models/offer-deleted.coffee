backbone = require 'backbone'


class OfferDeletedModel extends backbone.Model

  url: =>
    if @id
      "/api/offers/offer-deleted/#{@id}/"
    else
      "/api/offers/offer-deleted/"

module.exports = OfferDeletedModel
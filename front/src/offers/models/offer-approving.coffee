backbone = require 'backbone'


class OfferApprovingModel extends backbone.Model

  url: =>
    if @id
      "/api/offers/offer/#{@id}/"
    else
      "/api/offers/offer-approving/"

module.exports = OfferApprovingModel
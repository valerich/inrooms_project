backbone = require 'backbone'


class OfferModel extends backbone.Model

  url: =>
    if @id
      "/api/offers/offer/#{@id}/"
    else
      "/api/offers/offer/"

module.exports = OfferModel
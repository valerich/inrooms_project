backbone = require 'backbone'


class OfferDraftModel extends backbone.Model

  url: =>
    if @id
      "/api/offers/offer/#{@id}/"
    else
      "/api/offers/offer-draft/"

module.exports = OfferDraftModel
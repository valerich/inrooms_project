backbone = require 'backbone'


class OfferUnreadCommentsModel extends backbone.Model

  url: =>
    if @id
      "/api/offers/offer/#{@id}/"
    else
      "/api/offers/offer-unread-comments/"

module.exports = OfferUnreadCommentsModel
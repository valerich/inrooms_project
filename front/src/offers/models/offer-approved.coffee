backbone = require 'backbone'


class OfferApprovedModel extends backbone.Model

  url: =>
    if @id
      "/api/offers/offer/#{@id}/"
    else
      "/api/offers/offer-approved/"

module.exports = OfferApprovedModel
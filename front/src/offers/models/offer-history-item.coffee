_ = require 'underscore'
backbone = require 'backbone'


class HisotoryItemModel extends backbone.Model

  initialize: (options) =>
    @offer_id = options.offer_id

  url: =>
    if @id
      "/api/history/history_item/offers/offer/#{@offer_id}/#{@id}/"
    else
      "/api/history/history_item/offers/offer/#{@offer_id}/"


module.exports = HisotoryItemModel

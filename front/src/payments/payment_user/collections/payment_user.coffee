backbone = require 'backbone'

PaymentUserModel = require '../models/payment_user'


class PaymentUserCollection extends backbone.Collection
  model: PaymentUserModel
  url: "/api/payments/payment_user/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = PaymentUserCollection

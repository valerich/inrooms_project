backbone = require 'backbone'


class PaymentUserModel extends backbone.Model

  url: =>
    if @id
      "/api/payments/payment_user/#{@id}/"
    else
      "/api/payments/payment_user/"

module.exports = PaymentUserModel
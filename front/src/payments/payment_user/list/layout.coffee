$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

PaymentUserCollection = require '../collections/payment_user'
PaymentUserModel = require '../models/payment_user'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'

  serializeData: =>
    @userCanTransferPaymentAccounts = helpers.user_has_permission bus.cache.user, 'can_transfer_payment_accounts_view'
    serialize_data = super
    serialize_data['user'] = bus.cache.user
    serialize_data


class PaymentUserComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class PaymentUserComplexListView extends complex.ComplexCompositeView
  childView: PaymentUserComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class PaymentUserComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @collection = new PaymentUserCollection
    @filter_model = new FilterModel
    @list_view = new PaymentUserComplexListView
      collection: @collection

    @complex_view = new PaymentUserComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'payment_users'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'payment_account'
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

    @listenTo @complex_view.list_view, 'childview:click', (view)=>
      backbone.history.navigate "/payment_account_for_user/#{view.model.id}/", trigger: true

  onAdded: =>
    @complex_view.doFilter()


module.exports = LayoutView
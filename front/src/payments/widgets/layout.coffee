$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
complex = require 'complex'
marionette = require 'backbone.marionette'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
Highcharts = require('highcharts/highstock')


class CurrencyModel extends backbone.Model
  null


class CurrencyCollection extends backbone.Collection
  model: CurrencyModel

  initialize: (options) =>
    @url = "/api/payments/currency-month/"


class CurrencyItemView extends marionette.ItemView
  template: require './templates/currency-item'

  onShow: =>
    @smallStockCharts()

  smallStockCharts: =>
    $selector = $("#stock-data-sm_#{@model.id}")
    data1 = [@model.get('data')]
    chart1 = new Highcharts.StockChart({
        chart: {
            renderTo: $selector[0],
            height: 110,
            plotBorderColor: '#C21414',
            plotBorderColor: '#C21414',
            backgroundColor: 'transparent',
            spacingRight: 0,
            spacingLeft: 0,
            spacingBottom: 0,
            spacingTop: 0,
            marginBottom: 0
        },
        credits: {
            enabled: false
        },
        colors: ['rgba(0,0,0,0.3)', 'rgba(0,0,0,0.3)'],
        exporting: {
            enabled: false
        },
        rangeSelector: {
            selected: 0,
            enabled: false
        },
        scrollbar: {
            enabled: false
        },
        navigator: {
            enabled: false
        },
        navigation: {
            buttonOptions: {
                enabled: false
            }
        },
        xAxis: {
            gridLineColor: 'transparent',
            gridLineColor: 'transparent',
            lineColor: 'transparent',
            tickColor: 'transparent',
            minorGridLineWidth: 0,
            labels: {
                enabled: false
            }
        },
        yAxis: {
            gridLineColor: 'transparent',
            gridLineColor: 'transparent',
            lineColor: 'transparent',
            labels: {
                enabled: false
            }
        },
        series: [{
            name: @model.get('name'),
            data: @model.get('data'),
            type: 'spline',
            tooltip: {
                valueDecimals: 2
            }
        }]
    })


class CurrencyListView extends marionette.CollectionView
  template: require './templates/currency-items'
  childView: CurrencyItemView
  childViewContainer: ".region_items"


class LayoutView extends marionette.LayoutView
  template: require './templates/currency-layout'
  regions:
    'region_items': '.region_list'

  initialize: (options) =>
    @currency_collection = new CurrencyCollection()
    @currency_view = new CurrencyListView
      collection: @currency_collection

  onRender: =>
    @userCanViewCurrencyWidget = helpers.user_has_permission bus.cache.user, 'can_view_currency_widget'
    if @userCanViewCurrencyWidget
      @region_items.show(@currency_view)
      @currency_collection.fetch()

module.exports = LayoutView
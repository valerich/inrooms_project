backbone = require 'backbone'

ExpenseItemModel = require '../models/expense_item'


class ExpenseItemCollection extends backbone.Collection
  model: ExpenseItemModel
  url: "/api/payments/expense_item/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ExpenseItemCollection
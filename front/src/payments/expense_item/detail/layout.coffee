$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'

helpers = require 'helpers'

ExpenseItemModel = require '../models/expense_item'


class ItemView extends marionette.ItemView
  template: require './templates/item'

  translated_fields:
    name: 'Название'

  events:
    'click .save': 'onClickSave'

  initialize: =>
    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      name: "[name=name]"

    backbone.Validation.bind @

  onClickSave: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'
    'region_payment_documents': '.region_payment_documents'
  events:
    'click .to_list': 'goToList'
    'click .breadcrumb_list': 'goToList'

  initialize: (options)=>
    @binder = new modelbinder
    @model = new ExpenseItemModel
      id: options.id

    @item_view = new ItemView
      model: @model

  goToList: (event) =>
    event.preventDefault()
    backbone.history.navigate '/expense_item/', trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'expense_item'
    @model.fetch().done =>
      @region_item.show(@item_view)
    @binder.bind @model, @$el,
      name: '.breadcrumb_name'


module.exports = LayoutView
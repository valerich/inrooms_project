backbone = require 'backbone'


class ExpenseItemModel extends backbone.Model

  url: =>
    if @id
      "/api/payments/expense_item/#{@id}/"
    else
      "/api/payments/expense_item/"

module.exports = ExpenseItemModel
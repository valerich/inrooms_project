backbone = require 'backbone'
marionette = require 'backbone.marionette'
bus = require 'bus'
modelbinder = require 'backbone.modelbinder'
helpers = require 'helpers'
require 'jquery.inputmask'


class transferRurUsdModel extends backbone.Model
  defaults:
    rur: '0'
    rur_usd_rate: '-'
    usd: '0'
    additional_costs: '0'
    comment: ""


class transferRurUsdCnyModel extends backbone.Model
  defaults:
    rur: '0'
    usd: '0'
    cny: '0'
    additional_costs: '0'
    comment: ""
    rur_usd_rate: '-'
    usd_cny_rate: '-'


class transferRurCnyModel extends backbone.Model
  defaults:
    rur: '0'
    rur_cny_rate: '-'
    cny: '0'
    additional_costs: '0'
    comment: ""



class transferRurUsdView extends marionette.ItemView
  template: require './templates/transfer_rur_usd'
  events:
    'click .transfer': 'onClickTransfer'

  translated_fields:
    rur: 'Рубли'
    usd: 'Доллары'
    rur_usd_rate: 'Курс'
    usd_payment_account: 'Счет в долларах'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      rur: '[name=rur]'
      rur_usd_rate: '.rur_usd_rate'
      usd: '[name=usd]'
      usd_payment_account: '[name=usd_payment_account]'
      additional_costs: '[name=additional_costs]'
      comment: '[name=comment]'

    helpers.initAjaxSelect2 @$('[name=usd_payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        currency: 'USD'
#        user: bus.cache.user.id
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}"

    @$('[name=rur]').inputmask("9{1,100}")
    @$('[name=usd]').inputmask("9{1,100}")
    @$('[name=additional_costs]').inputmask("9{1,100}")

    @listenTo @model, 'change', =>
      @onModelChange()

    backbone.Validation.bind @

  onModelChange: =>
    rur = @model.get('rur')
    usd = @model.get('usd')
    if usd > 0
      rur_usd_rate = rur / usd
    else
      rur_usd_rate = '-'
    @model.set('rur_usd_rate', rur_usd_rate)

  onClickTransfer: =>
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: '/api/payments/transfer/rur_usd/'
        type: "post"
        data:
          rur: @model.get('rur')
          usd: @model.get('usd')
          usd_payment_account: @model.get('usd_payment_account')
          additional_costs: @model.get('additional_costs')
          comment: @model.get('comment')
        success: (data) =>
          helpers.generate_notyfication('success', 'Пополнено')
          backbone.history.navigate '/payment_account/', trigger: true
          @destroy()
        error: (data) =>
          helpers.modelErrorHandler @model, data, null, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class transferRurUsdCnyView extends marionette.ItemView
  template: require './templates/transfer_rur_usd_cny'
  events:
    'click .transfer': 'onClickTransfer'

  translated_fields:
    rur: 'Рубли'
    usd: 'Доллары'
    cny: 'Юани'
    cny_payment_account: 'Счет в юанях'
    usd_payment_account: 'Счет в долларах'
    additional_costs: 'Дополнительные расходы'
    comment: 'Комментарий'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      rur: '[name=rur]'
      rur_usd_rate: '.rur_usd_rate'
      usd: '[name=usd]'
      usd_payment_account: '[name=usd_payment_account]'
      usd_cny_rate: '.usd_cny_rate'
      cny: '[name=cny]'
      cny_payment_account: '[name=cny_payment_account]'
      additional_costs: '[name=additional_costs]'
      comment: '[name=comment]'

    helpers.initAjaxSelect2 @$('[name=cny_payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        currency: 'CNY'
#        user: bus.cache.user.id
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}"

    helpers.initAjaxSelect2 @$('[name=usd_payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        currency: 'USD'
#        user: bus.cache.user.id
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}"

    @$('[name=rur]').inputmask("9{1,100}")
    @$('[name=usd]').inputmask("9{1,100}")
    @$('[name=cny]').inputmask("9{1,100}")
    @$('[name=additional_costs]').inputmask("9{1,100}")

    @listenTo @model, 'change', =>
      @onModelChange()

    backbone.Validation.bind @

  onModelChange: =>
    rur = @model.get('rur')
    usd = @model.get('usd')
    cny = @model.get('cny')
    if usd > 0
      rur_usd_rate = rur / usd
    else
      rur_usd_rate = '-'
    if cny > 0
      usd_cny_rate = cny / usd
    else
      usd_cny_rate = '-'
    @model.set('rur_usd_rate', rur_usd_rate)
    @model.set('usd_cny_rate', usd_cny_rate)

  onClickTransfer: =>
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: '/api/payments/transfer/rur_usd_cny/'
        type: "post"
        data:
          rur: @model.get('rur')
          usd: @model.get('usd')
          cny: @model.get('cny')
          usd_payment_account: @model.get('usd_payment_account')
          cny_payment_account: @model.get('cny_payment_account')
          additional_costs: @model.get('additional_costs')
          comment: @model.get('comment')
        success: (data) =>
          helpers.generate_notyfication('success', 'Пополнено')
          backbone.history.navigate '/payment_account/', trigger: true
          @destroy()
        error: (data) =>
          helpers.modelErrorHandler @model, data, null, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class transferRurCnyView extends marionette.ItemView
  template: require './templates/transfer_rur_cny'
  events:
    'click .transfer': 'onClickTransfer'

  translated_fields:
    rur: 'Рубли'
    cny: 'Юани'
    rur_cny_rate: 'Курс йены к доллару'
    cny_payment_account: 'Счет в юанях'
    additional_costs: 'Дополнительные расходы'
    comment: 'Комментарий'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      rur: '[name=rur]'
      rur_cny_rate: '.rur_cny_rate'
      cny: '[name=cny]'
      cny_payment_account: '[name=cny_payment_account]'
      additional_costs: '[name=additional_costs]'
      comment: '[name=comment]'

    helpers.initAjaxSelect2 @$('[name=cny_payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        currency: 'CNY'
#        user: bus.cache.user.id
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}"

    @$('[name=rur]').inputmask("9{1,100}")
    @$('[name=cny]').inputmask("9{1,100}")
    @$('[name=additional_costs]').inputmask("9{1,100}")

    @listenTo @model, 'change', =>
      @onModelChange()

    backbone.Validation.bind @

  onModelChange: =>
    rur = @model.get('rur')
    cny = @model.get('cny')
    if cny > 0
      rur_cny_rate = rur / cny
    else
      rur_cny_rate = '-'
    @model.set('rur_cny_rate', rur_cny_rate)

  onClickTransfer: =>
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: '/api/payments/transfer/rur_cny/'
        type: "post"
        data:
          rur: @model.get('rur')
          cny: @model.get('cny')
          cny_payment_account: @model.get('cny_payment_account')
          additional_costs: @model.get('additional_costs')
          comment: @model.get('comment')
        success: (data) =>
          helpers.generate_notyfication('success', 'Пополнено')
          backbone.history.navigate '/payment_account/', trigger: true
          @destroy()
        error: (data) =>
          helpers.modelErrorHandler @model, data, null, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_rur_usd': '.region_rur_usd'
    'region_rur_usd_cny': '.region_rur_usd_cny'
    'region_rur_cny': '.region_rur_cny'

  events:
    'click .to_list': 'goToList'

  initialize: =>
    @rur_usd_model = new transferRurUsdModel
    @rur_usd_view = new transferRurUsdView
      model: @rur_usd_model

    @rur_usd_cny_model = new transferRurUsdCnyModel
    @rur_usd_cny_view = new transferRurUsdCnyView
      model: @rur_usd_cny_model

    @rur_cny_model = new transferRurCnyModel
    @rur_cny_view = new transferRurCnyView
      model: @rur_cny_model

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'payment_account'
    @region_rur_usd.show(@rur_usd_view)
    @region_rur_usd_cny.show(@rur_usd_cny_view)
    @region_rur_cny.show(@rur_cny_view)

  goToList: (event) =>
    event.preventDefault()
    backbone.history.navigate '/payment_account/', trigger: true


module.exports = LayoutView
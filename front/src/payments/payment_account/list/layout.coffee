$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

PaymentAccountCollection = require '../collections/payment_account'
PaymentAccountModel = require '../models/payment_account'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .transfer': 'onTransferClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'

  onTransferClick: =>
    backbone.history.navigate "/payment_account/transfers/", trigger: true

  serializeData: =>
    @userCanTransferPaymentAccounts = helpers.user_has_permission bus.cache.user, 'can_transfer_payment_accounts_view'
    serialize_data = super
    serialize_data['user'] = bus.cache.user
    serialize_data['user']['userCanTransferPaymentAccounts'] = @userCanTransferPaymentAccounts
    serialize_data


class PaymentAccountComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class PaymentAccountComplexListView extends complex.ComplexCompositeView
  childView: PaymentAccountComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class PaymentAccountComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @user_id = options.user_id
    @collection = new PaymentAccountCollection
    @filter_model = new FilterModel
       user: @user_id
    @list_view = new PaymentAccountComplexListView
      collection: @collection

    @complex_view = new PaymentAccountComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'payment_accounts'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'payment_account'
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

    @listenTo @complex_view.list_view, 'childview:click', (view)=>
      backbone.history.navigate "/payment_account/#{view.model.id}/", trigger: true

  onAdded: =>
    @complex_view.doFilter()


module.exports = LayoutView
_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

PaymentDocumentCollection = require '../collections/payment_document'

ManufacturingOrderModalView = require '../../../manufacturing/detail_modal/layout'
ManufacturingLabelOrderModalView = require '../../../manufacturing_label/detail_modal/layout'
OfferModalView = require '../../../offers/detail_modal/layout'
SupplyModalView = require '../../../supply/detail_modal/layout'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/payment_document_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    console.log 'togglePanel'
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      created_gte: '[name=created_gte]'
      created_lte: '[name=created_lte]'
      expense_item: '[name=expense_item]'

    @$('[name=created_gte]').datepicker
      autoclose: true

    @$('[name=created_lte]').datepicker
      autoclose: true

    helpers.initAjaxSelect2 @$('[name=expense_item]'),
      url: '/api/payments/expense_item/'
      placeholder: "Выберите статью расходов"
      minimumInputLength: -1
      allowClear: true
      text_attr: 'name'


class PaymentDocumentComplexItemView extends complex.ComplexItemView
  template: require './templates/payment_document_item'
  className: 'payment_document_item'
  tagName: 'tr'

  events:
      'click .object_detail': 'onClickObjectDetail'

  onClickObjectDetail: (e)=>
    e.preventDefault()
    content_object_detail = @model.get('content_object_detail')
    if content_object_detail
      view = null
      if content_object_detail['content_type']['model'] == 'manufacturingorder'
        view = ManufacturingOrderModalView
        title = "Заказ на фабрику №#{content_object_detail['object_id']}"
      if content_object_detail['content_type']['model'] == 'manufacturinglabelorder'
        view = ManufacturingLabelOrderModalView
        title = "Заказ этикеток на фабрику №#{content_object_detail['object_id']}"
      if content_object_detail['content_type']['model'] == 'offer'
        view = OfferModalView
        title = "Предложение №#{content_object_detail['object_id']}"
      if content_object_detail['content_type']['model'] == 'supply'
        view = SupplyModalView
        title = "Поставка №#{content_object_detail['object_id']}"

      if view
        modalView = new view
          id: content_object_detail['object_id']

        bus.vent.trigger 'modal:show', modalView,
          title: title
          modal_size: 'lg'


class PaymentDocumentComplexListView extends complex.ComplexCompositeView
  childView: PaymentDocumentComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/payment_document_items'


class PaymentDocumentComplexView extends complex.ComplexView
  template: require './templates/payment_document_complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout_payment_document'

  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @payment_document_model = options.model
    @collection = new PaymentDocumentCollection
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
      @$('.amount_rur_sum').text(@collection.amount_rur_sum)
      @$('.amount_sum').text(@collection.amount_sum)
    @filter_model = new FilterModel
      current_payment_account: @payment_document_model.id
    @list_view = new PaymentDocumentComplexListView
      collection: @collection

    @complex_view = new PaymentDocumentComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'payment_document_items'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()


module.exports = LayoutView

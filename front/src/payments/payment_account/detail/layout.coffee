$ = require 'jquery'
_ = require 'underscore'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
bus = require 'bus'
require 'backbone-validation'

helpers = require 'helpers'

PaymentAccountModel = require '../models/payment_account'
RefillModel = require '../models/refill'
TransferModel = require '../models/transfer'
WritingOffModel = require '../models/writing_off'
PaymentDocumentLayoutView = require './payment_document'


class refillView extends marionette.ItemView
  template: require './templates/refill_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    amount: 'Cумма'
    error: 'Ошибка'

  initialize: (options) =>
    @binder = new modelbinder
    @payment_account_model = options.payment_account_model

  onRender: =>
    @binder.bind @model, @$el,
      amount: '[name=amount]'

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: '/api/payments/payment_account/' + @payment_account_model.id + '/refill/'
        type: "post"
        data:
          amount: @model.get('amount')
        success: (data) =>
          console.log 'save'
          helpers.generate_notyfication('success', 'Пополнено')
          bus.vent.trigger 'payment_account:refill', @payment_account_model
          @destroy()
        error: (data) =>
          helpers.modelErrorHandler @model, data, null, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class writingOffView extends marionette.ItemView
  template: require './templates/writing_off_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    amount: 'Cумма'
    error: 'Ошибка'

  initialize: (options) =>
    @binder = new modelbinder
    @payment_account_model = options.payment_account_model

  onRender: =>
    @binder.bind @model, @$el,
      amount: '[name=amount]'
      expense_item: '[name=expense_item]'
      comment: '[name=comment]'

    helpers.initAjaxSelect2 @$('[name=expense_item]'),
      url: '/api/payments/expense_item/'
      placeholder: "Выберите статью расходов"
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: '/api/payments/payment_account/' + @payment_account_model.id + '/writing_off/'
        type: "post"
        data:
          amount: @model.get('amount')
          comment: @model.get('comment')
          expense_item: @model.get('expense_item')
        success: (data) =>
          console.log 'save'
          helpers.generate_notyfication('success', 'Пополнено')
          bus.vent.trigger 'payment_account:refill', @payment_account_model
          @destroy()
        error: (data) =>
          helpers.modelErrorHandler @model, data, null, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class selfTransferView extends marionette.ItemView
  template: require './templates/self_transfer_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    to_payment_account: 'Счет назначения'
    amount: 'Cумма'
    error: 'Ошибка'

  initialize: (options) =>
    @binder = new modelbinder
    @payment_account_model = options.payment_account_model

  onRender: =>
    @binder.bind @model, @$el,
      amount: '[name=amount]'
      to_payment_account: '[name=to_payment_account]'

    helpers.initAjaxSelect2 @$('[name=to_payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        transfer_from: @payment_account_model.id
        transfer_type: 'self_transfer'
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}. #{obj.currency_detail.name} / #{obj.user_detail.name}"

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: '/api/payments/payment_account/' + @payment_account_model.id + '/transfer/'
        type: "post"
        data:
          to_payment_account: @model.get('to_payment_account')
          transfer_type: 'self_transfer'
          amount: @model.get('amount')
        success: (data) =>
          console.log 'save'
          helpers.generate_notyfication('success', 'Переведено')
          bus.vent.trigger 'payment_account:refill', @payment_account_model
          @destroy()
        error: (data) =>
          helpers.modelErrorHandler @model, data, null, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class selfCurrencyTransferView extends marionette.ItemView
  template: require './templates/self_currency_transfer_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    to_payment_account: 'Счет назначения'
    currency_rate: 'Курс'
    amount: 'Cумма'
    error: 'Ошибка'

  initialize: (options) =>
    @binder = new modelbinder
    @payment_account_model = options.payment_account_model

  onRender: =>
    @binder.bind @model, @$el,
      currency_rate: '[name=currency_rate]'
      amount: '[name=amount]'
      to_payment_account: '[name=to_payment_account]'

    helpers.initAjaxSelect2 @$('[name=to_payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        transfer_from: @payment_account_model.id
        transfer_type: 'self_currency_transfer'
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}. #{obj.currency_detail.name} / #{obj.user_detail.name}"

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: '/api/payments/payment_account/' + @payment_account_model.id + '/transfer/'
        type: "post"
        data:
          amount: @model.get('amount')
          currency_rate: @model.get('currency_rate')
          transfer_type: 'self_currency_transfer'
          to_payment_account: @model.get('to_payment_account')
        success: (data) =>
          console.log 'save'
          helpers.generate_notyfication('success', 'Пополнено')
          bus.vent.trigger 'payment_account:refill', @payment_account_model
          @destroy()
        error: (data) =>
          helpers.modelErrorHandler @model, data, null, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class userTransferView extends marionette.ItemView
  template: require './templates/user_transfer_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    to_payment_account: 'Счет назначения'
    currency_rate: 'Курс'
    amount: 'Cумма'
    error: 'Ошибка'

  initialize: (options) =>
    @binder = new modelbinder
    @payment_account_model = options.payment_account_model

  onRender: =>
    @binder.bind @model, @$el,
      amount: '[name=amount]'
      to_payment_account: '[name=to_payment_account]'

    helpers.initAjaxSelect2 @$('[name=to_payment_account]'),
      url: '/api/payments/payment_account/'
      placeholder: 'Введите название счета'
      get_extra_search_params: () =>
        transfer_from: @payment_account_model.id
        transfer_type: 'user_transfer'
      minimumInputLength: -1
      text_attr: (obj) =>
        "#{obj.name}. #{obj.currency_detail.name} / #{obj.user_detail.name}"

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      $.ajax
        url: '/api/payments/payment_account/' + @payment_account_model.id + '/transfer/'
        type: "post"
        data:
          amount: @model.get('amount')
          transfer_type: 'user_transfer'
          to_payment_account: @model.get('to_payment_account')

        success: (data) =>
          console.log 'save'
          helpers.generate_notyfication('success', 'Пополнено')
          bus.vent.trigger 'payment_account:refill', @payment_account_model
          @destroy()
        error: (data) =>
          helpers.modelErrorHandler @model, data, null, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class ItemView extends marionette.ItemView
  template: require './templates/item'

  events:
    'click .refill': 'refill'
    'click .self_transfer': 'selfTransfer'
    'click .self_currency_transfer': 'selfCurrencyTransfer'
    'click .user_transfer': 'userTransfer'
    'click .writing_off': 'writingOff'

  initialize: =>
    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      name: "[name=name]"
      balance: "[name=balance]"
      balance_rur: "[name=balance_rur]"
      currency_name: "[name=currency_name]"

    backbone.Validation.bind @

  writingOff: =>
    newWritingOffView = new writingOffView
      payment_account_model: @model
      model: new WritingOffModel
        payment_account_id: @model.id

    bus.vent.trigger 'modal:show', newWritingOffView,
      title: 'Списание со счета'

  refill: =>
    newRefillView = new refillView
      payment_account_model: @model
      model: new RefillModel
        payment_account_id: @model.id

    bus.vent.trigger 'modal:show', newRefillView,
      title: 'Пополнение счета'

  selfTransfer: =>
    newTransferView = new selfTransferView
      payment_account_model: @model
      model: new TransferModel
        payment_account_id: @model.id

    bus.vent.trigger 'modal:show', newTransferView,
      title: 'Перевод между своими счетами в одной валюте'

  selfCurrencyTransfer: =>
    newTransferView = new selfCurrencyTransferView
      payment_account_model: @model
      model: new TransferModel
        payment_account_id: @model.id

    bus.vent.trigger 'modal:show', newTransferView,
      title: 'Перевод между своими счетами с разной валютой'

  userTransfer: =>
    newTransferView = new userTransferView
      payment_account_model: @model
      model: new TransferModel
        payment_account_id: @model.id

    bus.vent.trigger 'modal:show', newTransferView,
      title: 'Перевод другому пользователю'

  serializeData: =>
    @userCanWritingOff = helpers.user_has_permission bus.cache.user, 'payment_has_writing_off'
    data = super
    data['user'] = bus.cache.user
    data['user']['userCanWritingOff'] = @userCanWritingOff
    data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_item': '.region_item'
    'region_payment_documents': '.region_payment_documents'
  events:
    'click .to_list': 'goToList'
    'click .breadcrumb_list': 'goToList'

  initialize: (options)=>
    @binder = new modelbinder
    @model = new PaymentAccountModel
      id: options.id

    @item_view = new ItemView
      model: @model

    @payment_document_view = new PaymentDocumentLayoutView
      model: @model

  goToList: (event) =>
    event.preventDefault()
    backbone.history.navigate '/payment_account/', trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'payment_account'
    @model.fetch().done =>
      @model.set('currency_name', @model.get('currency_detail')['name'])
      @region_item.show(@item_view)
    @binder.bind @model, @$el,
      name: '.breadcrumb_name'
    @listenTo(bus.vent, 'payment_account:refill', @onRefill)
    @region_payment_documents.show(@payment_document_view)

  onRefill: (payment_account_id) =>
    console.log 'payment_account onRefill'
    @model.fetch()


module.exports = LayoutView
backbone = require 'backbone'

PaymentDocumentModel = require '../models/payment_document'


class PaymentDocumentCollection extends backbone.Collection
  model: PaymentDocumentModel

  url: =>
    "/api/payments/payment_document/"

  parse: (response) =>
    @count = response.count
    @amount_rur_sum = response.amount_rur_sum_display
    @amount_sum = response.amount_sum_display
    return response.results

module.exports = PaymentDocumentCollection
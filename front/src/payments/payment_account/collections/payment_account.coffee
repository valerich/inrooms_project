backbone = require 'backbone'

PaymentAccountModel = require '../models/payment_account'


class PaymentAccountCollection extends backbone.Collection
  model: PaymentAccountModel
  url: "/api/payments/payment_account/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = PaymentAccountCollection
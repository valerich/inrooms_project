backbone = require 'backbone'


class WritingOffModel extends backbone.Model
  defaults:
    amount: '0.00'

  initialize: (options) =>
    @payment_account_id = options.payment_account_id

  url: =>
    "/api/payments/payment_account/#{@payment_account_id}/writing_off/"

  validation:
      amount: [
        {
          required: true,
          msg: 'Заполните сумму'
        }]
      comment: [
        {
          required: true,
          msg: 'Заполните комментарий'
        }]
      expense_item: [
        {
          required: true,
          msg: 'Заполните статью расходов'
        }]

module.exports = WritingOffModel
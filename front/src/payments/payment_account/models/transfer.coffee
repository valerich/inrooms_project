backbone = require 'backbone'


class TransferModel extends backbone.Model
  defaults:
    amount: '0.00'
    currency_rate: '1.00'

  initialize: (options) =>
    @payment_account_id = options.payment_account_id

  url: =>
    "/api/payments/payment_account/#{@payment_account_id}/transfer/"

  validation:
      amount: [
        {
          required: true,
          msg: 'Заполните сумму'
        }]

module.exports = TransferModel
backbone = require 'backbone'


class PaymentDocumentModel extends backbone.Model

  url: =>
    if @id
      "/api/payments/payment_document/#{@id}/"
    else
      "/api/payments/payment_document/"

module.exports = PaymentDocumentModel

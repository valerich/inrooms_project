backbone = require 'backbone'


class PaymentAccountModel extends backbone.Model

  url: =>
    if @id
      "/api/payments/payment_account/#{@id}/"
    else
      "/api/payments/payment_account/"

module.exports = PaymentAccountModel
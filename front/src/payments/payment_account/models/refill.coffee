backbone = require 'backbone'


class RefillModel extends backbone.Model
  defaults:
    amount: '0.00'

  initialize: (options) =>
    @payment_account_id = options.payment_account_id

  url: =>
    "/api/payments/payment_account/#{@payment_account_id}/refill/"

  validation:
      amount: [
        {
          required: true,
          msg: 'Заполните сумму'
        }]

module.exports = RefillModel
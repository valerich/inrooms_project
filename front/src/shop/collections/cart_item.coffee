backbone = require 'backbone'

CartItemModel = require '../models/cart_item'


class CartItemCollection extends backbone.Collection
  model: CartItemModel

  url: =>
    "/api/cart/cart/items/"

  parse: (response) =>
    @count = response.count
    return response.results


module.exports = CartItemCollection
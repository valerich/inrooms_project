backbone = require 'backbone'

ProductCollectionModel = require '../models/product_collection'


class ProductCollectionCollection extends backbone.Collection
  model: ProductCollectionModel

  url: =>
    "/api/products/collection/"

  parse: (response) =>
    @count = response.count
    return response.results


module.exports = ProductCollectionCollection
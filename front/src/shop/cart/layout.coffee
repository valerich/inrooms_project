marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

CartModel = require '../models/cart'
CommonLayout = require './common/layout'
ClientLayout = require './client/layout'
ItemsLayout = require './product/layout'


class NavbarView extends marionette.ItemView
  template: require './templates/navbar'
  events:
    'click .tab_link': 'onClickTabLink'
  initialize: (options) =>
    @tab_name = options.tab_name
    console.log @tab_name
  onRender: =>
    @$(".tab_link[data-tab_name=#{@tab_name}]").addClass('active')
  onClickTabLink: (event) =>
    event.preventDefault()
    tab_name = $(event.target).parent().data('tab_name')
    @$(".tab_link").removeClass('active')
    @$(".tab_link[data-tab_name=#{tab_name}]").addClass('active')
    @trigger 'click', tab_name


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'cart_detail_layout'
  regions:
    region_tab: '.region_tab'
    region_navbar: '.region_navbar'
  events:
    'click .list_link': 'onClickListLink'
    'click .save_cart': 'onClickCartSave'

  onClickListLink: (event) =>
    event.preventDefault()
    backbone.history.navigate '/shop/catalog/', trigger: true

  onClickCartSave: =>
    for item in @cartitem_collection.models
      if item.changedAttributes()
        item.save().done =>
          helpers.generate_notyfication('success', 'Модель сохранена')
    backbone.history.navigate "/shop/cart/client/", trigger: true

  onClickTabLink: (event) =>
    event.preventDefault()
    if @modelFetchDone
      tab_name = $(event.target).parent().data('tab_name')
      backbone.history.navigate "/shop/cart/#{tab_name}/", trigger: false
      @renderTab(tab_name)

  initialize: (options) =>
    @tab_name = options.tab_name
    @binder = new modelbinder
    @model = new CartModel

  onBeforeDestroy: =>
    @binder.unbind()

  renderTab: (tab_name) =>
    view_class = null
    if tab_name is 'common'
      view_class = CommonLayout
    if tab_name is 'items'
      view_class = ItemsLayout
#    if tab_name is 'client'
#      view_class = ClientLayout
    if view_class
      tab_view = new view_class
        model: @model
      @region_tab.show(tab_view)
    else
      @region_tab.empty()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'shop_catalog'
    @renderTab(@tab_name)
    navbar_view = new NavbarView
      tab_name: @tab_name
    @region_navbar.show(navbar_view)
    @listenTo navbar_view, 'click', (tab_name) =>
      @tab_name = tab_name
      backbone.history.navigate "/shop/cart/#{tab_name}/", trigger: false
      @renderTab(tab_name)

module.exports = ObjectDetailLayout

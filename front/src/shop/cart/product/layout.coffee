_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

CartItemCollection = require '../../collections/cart_item'
CartItemModel = require '../../models/cart_item'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class CartItemComplexItemView extends complex.ComplexItemView
  template: require './templates/item'
  className: 'cart_items_item'
  events:
    'click .remove': 'onClickRemove'

  onClickRemove: =>
    bootbox.confirm "Вы действительно хотите удалить модель из корзины?", (result)=>
      if result == true
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')
          bus.vent.trigger 'cart_item:update'


class CartItemComplexListView extends complex.ComplexCompositeView
  childView: CartItemComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class CartItemComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @collection = new CartItemCollection
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
    @filter_model = new FilterModel
    @list_view = new CartItemComplexListView
      collection: @collection

    @complex_view = new CartItemComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'cart_items'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

    @listenTo(bus.vent, 'cart_item:update', @complex_view.doFilter)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()


module.exports = LayoutView

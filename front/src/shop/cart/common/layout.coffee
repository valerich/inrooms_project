_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bootbox = require 'bootbox'
store = require 'store'
helpers = require 'helpers'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .create_order': 'onClickCreateOrder'

  initialize: (options) =>
    @model.fetch()

  onClickCreateOrder: =>
    $.ajax
      url: "/api/cart/cart/create_order/"
      type: 'post'
      success: =>
        helpers.generate_notyfication('success', 'Заказ успешно создан')
        backbone.history.navigate "/shop/catalog/", trigger: true
      error: (data) =>
        helpers.modelErrorHandler @model, data

module.exports = LayoutView

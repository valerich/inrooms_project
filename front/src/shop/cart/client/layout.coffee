_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
helpers = require 'helpers'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_cart2': 'onClickCartSave'

  onClickCartSave: =>
    @model.save().done =>
      helpers.generate_notyfication('success', 'Сохранено')
      backbone.history.navigate "/shop/cart/common/", trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder
    @model = options.model

  onRender: =>
    @model.fetch().done =>
      @binder.bind @model, @$el,
        client: '[name=client]'

      helpers.initAjaxSelect2 @$('[name=client]'),
        url: '/api/clients/client/'
        placeholder: 'Введите название клиента'
        minimumInputLength: -1
        text_attr: (obj) =>
          "№:#{obj.id}. #{obj.name}"
        get_extra_search_params: () =>
          kind: 1


module.exports = LayoutView

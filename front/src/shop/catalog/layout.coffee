$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'
require 'magnific-popup'

ProductCollection = require '../../products/collections/product'
CartItemCollection = require '../collections/cart_item'
ProductCollectionCollection = require '../collections/product_collection'
CartItemModel = require '../models/cart_item'
CartModel = require '../models/cart'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  regions:
    'region_cart': '.region-cart'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      main_collection: '[name=main_collection]'
      sort_by: '.sort_list'
      search: '[name=search]'

    helpers.initAjaxSelect2 @$('[name=main_collection]'),
      url: '/api/products/collection/'
      placeholder: "Выберите главную категорию"
      minimumInputLength: -1
      get_extra_search_params: () =>
        warehouse_remains: 'msk'
      text_attr: 'full_name'
      allowClear: true


class CartComplexItemView extends complex.ComplexItemView
  template: require './templates/cart_item'
  events:
    'click .cart-item-delete': 'onClickDelete'

  onClickDelete: =>
    $.ajax
      url: "/api/cart/cart/items/delete/"
      type: 'post'
      data: {
        'product_id': @model.get('product_detail')['id']
      }
      success: =>
        bus.vent.trigger 'cart:item:deleted'
        helpers.generate_notyfication('success', 'Удалено из корзины')
      error: (data) =>
        helpers.generate_notyfication('error', data.responseJSON["error"])

#  initialize: =>
#    @setLink()

#  setLink: =>
#    ct = @model.get('content_object_detail').content_type
#    obj = @model.get('content_object_detail').object
#    if ct.app_label == 'products' and ct.model == 'product'
#      link = '/product/' + obj.id + '/'
#    if ct.app_label == 'orders' and ct.model == 'order'
#      link = '/product/' + obj.id + '/'
#    @model.set('link', link)
#
#  onClickObjectLink: (event)=>
#     event.preventDefault()
#     backbone.history.navigate $(event.target).parent().data('link'), trigger: true


class CartComplexListView extends complex.ComplexCompositeView
  childView: CartComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/cart_items'


class CartComplexView extends marionette.LayoutView
  template: require './templates/cart_complex_layout'
  regions:
    'region_list': '.cart_region_list'
  className: 'cart_complex_view'

  events:
    'click .btn-go-to-cart': 'onClickGoToCart'
    'click .go_to_cart': 'onClickGoToCart'

  onClickGoToCart: =>
    backbone.history.navigate '/shop/cart/', trigger: true

  reloadCart: =>
    @collection.fetch
      reset: true
      data:
        '_fields': 'id,name,article,color_detail'

  initialize: (options)=>
    @name = options.name
    @collection = options.collection || throw 'specify collection of complex view'
    @list_view = new CartComplexListView
      collection: @collection
      name: 'cart'

  onRender: =>
    @region_list.show(@list_view)
    @listenTo(bus.vent, 'cart:item:added', @reloadCart)
    @listenTo(bus.vent, 'cart:item:deleted', @reloadCart)
    @reloadCart()
    @listenTo @collection, 'sync', =>
      if @collection.length > 0
        @$('.cart_link').removeClass("hidden")
        @$('.empty_list_text').addClass("hidden")
        @$('.go_to_cart').removeClass("hidden")
      else
        @$('.cart_link').addClass("hidden")
        @$('.empty_list_text').removeClass("hidden")
        @$('.go_to_cart').addClass("hidden")


class ProductCollectionComplexItemView extends complex.ComplexItemView
  template: require './templates/product_collection_item'
  events:
    'click .product_collection': 'onClickProductCollection'

  onClickProductCollection: (event) =>
    event.preventDefault()
    collection_id = $(event.target).data('id')
    full_name = $(event.target).data('full_name')
    bus.vent.trigger 'collection:selected', collection_id, full_name


class ProductCollectionComplexListView extends complex.ComplexCompositeView
  childView: ProductCollectionComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/product_collection_items'


class ProductCollectionComplexView extends marionette.LayoutView
  template: require './templates/product_collection_complex_layout'
  regions:
    'region_list': '.product_collection_region_list'
  className: 'product_collection_complex_view'

#  events:
#    'click .btn-go-to-cart': 'onClickGoToCart'
#    'click .go_to_cart': 'onClickGoToCart'

#  onClickGoToCart: =>
#    backbone.history.navigate '/shop/cart/', trigger: true
#
#  reloadCart: =>
#    @collection.fetch
#      reset: true
#      data:
#        '_fields': 'id,name,article,color_detail'

  initialize: (options)=>
    @name = options.name
    @collection = options.collection || throw 'specify collection of complex view'
    @list_view = new ProductCollectionComplexListView
      collection: @collection
      name: 'product_collection'

  onRender: =>
    @collection.fetch(
      data:
        '_fields': 'id,name,childs,full_name'
        'for_catalog': 2
    ).done =>
      @region_list.show(@list_view)
#    @listenTo(bus.vent, 'cart:item:added', @reloadCart)
#    @listenTo(bus.vent, 'cart:item:deleted', @reloadCart)
#    @reloadCart()
#    @listenTo @collection, 'sync', =>
#      if @collection.length > 0
#        @$('.cart_link').removeClass("hidden")
#        @$('.empty_list_text').addClass("hidden")
#        @$('.go_to_cart').removeClass("hidden")
#      else
#        @$('.cart_link').addClass("hidden")
#        @$('.empty_list_text').removeClass("hidden")
#        @$('.go_to_cart').addClass("hidden")


class ProductComplexItemView extends complex.ComplexItemView
  template: require './templates/item'
  events:
    'click .product-to-cart': 'onClickToCart'
    'click .image-link': 'onImageLinkClick'

  onImageLinkClick: =>
    title = @model.get('article') + ', ' + @model.get('name') + ', ' + @model.get('color_detail')['name'] + ' (' + @model.get('full_consistency') + ')'
    data = []

    _.each @model.get('images'), (el, index) =>
      i = {
        'src': el['src'],
        'title': title
      }
      data.push(i)

    $.magnificPopup.open({
      type:'image',
      gallery: {
        enabled: true
      },
      removalDelay: 300,
      mainClass: 'mfp-fade',
      items: data,
    })

  onClickToCart: =>
    $.ajax
      url: "/api/cart/cart/items/add/"
      type: 'post'
      data: {
        'product_id': @model.id
      }
      success: =>
        bus.vent.trigger 'cart:item:added'
        helpers.generate_notyfication('success', 'Добавлено в корзину')
      error: (data) =>
        helpers.generate_notyfication('error', data.responseJSON["error"])


class ProductComplexListView extends complex.ComplexCompositeView
  childView: ProductComplexItemView
  childViewContainer: ".region_shop_product_panel_items"
  template: require './templates/items'


class ProductComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2
  regions:
    'region_search': '.region_search'
    'region_list': '.region_list'
    'region_paginator': '.region_paginator'
    'region_sidebar': '.region_sidebar'

  onRender: =>
    @product_collection_view = new ProductCollectionComplexView
      collection: new ProductCollectionCollection
    super
    @region_sidebar.show(@product_collection_view)

  doFilter: =>
    if @blockFilter
      return
    @blockFilter = true

    setTimeout =>
      if @blockFilter
        @showSpinner()
    , 1000

    filter_data = @filter_model.getFilterData()
    filter_data['warehouse_remains'] = 'msk,showroom'
    filter_data['brand__code'] = 'zaracity'
    filter_data['is_active'] = 2
    filter_data['_fields'] = 'id,name,article,price,image,color_detail,images,full_consistency,is_new_collection'
    filter_data['sort_by'] = '-is_new_collection'

    @collection.fetch
      data: filter_data
      reset: true
      error: (collection, response) =>
        if response.status is 404 and @filter_model.get('page') > 2
          @blockFilter = false
          @filter_model.set 'page', 1
      complete: =>
        @blockFilter = false
        @hideSpinner()


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>

    @collection = new ProductCollection
#
#    @cart_collection = new CartItemCollection

    @filter_model = new FilterModel()

    @filter_view = new FilterView
      model: @filter_model

#    @cart_complex_view = new CartComplexView
#      collection: @cart_collection
#      name: 'cart_complex'

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'shop_catalog'
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @region_filter.show(@filter_view)
    @renderSupplyList()
#    @renderCartView()

  renderSupplyList: () =>
    list_view = new ProductComplexListView
      collection: @collection

    @complex_view = new ProductComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'supply'

    @region_complex.show(@complex_view)

    @listenTo(bus.vent, 'collection:selected', @onProductCollectionSelected)

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @complex_view.doFilter()

  onProductCollectionSelected: (product_collection_id, full_name) =>
    $('.product_collection').removeClass('active')
    $(".product_collection_#{product_collection_id}").addClass('active')
    $(".catalog_part_name").html(full_name)
    @filter_model.set('page', 1)
    @filter_model.set('main_collection_r', product_collection_id)

module.exports = LayoutView
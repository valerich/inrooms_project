backbone = require 'backbone'


class ProductCollectionModel extends backbone.Model

  url: =>
    if @id
      "/api/products/collection/#{@id}/"
    else
      "/api/products/collection/"

module.exports = ProductCollectionModel
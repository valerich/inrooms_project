backbone = require 'backbone'


class CartItemModel extends backbone.Model

  url: =>
    if @id
      "/api/cart/cart/items/#{@id}/"
    else
      "/api/cart/cart/items/"

module.exports = CartItemModel
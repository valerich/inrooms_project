backbone = require 'backbone'
moment = require 'moment'


class ClientStatisticModel extends backbone.Model
  defaults:
      date: moment().format("DD.MM.YYYY")
      visitors_count: 0
      sales_amount: 0

  url: =>
    if @id
      "/api/clients/client_statistics/#{@id}/"
    else
      "/api/clients/client_statistics/"

module.exports = ClientStatisticModel
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'

ClientStatisticCollection = require '../collections/client_statistic'
ClientStatisticModel = require '../models/client_statistic'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onAddClick'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onAddClick: =>
    addNewView = new NewClientStatisticView
      model: new ClientStatisticModel

    bus.vent.trigger 'modal:show', addNewView,
      title: 'Добавить нового запись'

  serializeData: =>
    @userCanCreateAllClientStatistic = helpers.user_has_permission bus.cache.user, 'create_all_client_statistic'
    @userCanCreateSelfClientStatistic = helpers.user_has_permission bus.cache.user, 'create_self_client_statistic'
    result = super
    result['user'] = bus.cache.user
    result['user']['userCanCreateAllClientStatistic'] = @userCanCreateAllClientStatistic
    result['user']['userCanCreateSelfClientStatistic'] = @userCanCreateSelfClientStatistic
    result


class ClientStatisticComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'


class ClientStatisticComplexListView extends complex.ComplexCompositeView
  childView: ClientStatisticComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class ClientStatisticComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  d: 2


class NewClientStatisticView extends marionette.ItemView
  template: require './templates/new_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    client: 'Партнер'
    date: 'Дата'
    visitors_count: 'Количество посетителей'
    sales_amount: 'Сумма продаж'

  initialize: =>
    @binder = new modelbinder

    @userCanCreateAllClientStatistic = helpers.user_has_permission bus.cache.user, 'create_all_client_statistic'
    @userCanCreateSelfClientStatistic = helpers.user_has_permission bus.cache.user, 'create_self_client_statistic'

  onRender: =>
    if @userCanCreateAllClientStatistic
      @binder.bind @model, @$el,
        client: '[name=client]'
        date: '[name=date]'
        visitors_count: '[name=visitors_count]'
        sales_amount: "[name=sales_amount]"
    else
      @binder.bind @model, @$el,
        date: '[name=date]'
        visitors_count: '[name=visitors_count]'
        sales_amount: "[name=sales_amount]"

    @$('[name=date]').datepicker
      autoclose: true

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: "Выберите клиента"
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Сохранено')
          @destroy()
          bus.vent.trigger 'client:added'
        error: (model, response, options) =>
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()

  serializeData: =>
    result = super
    result['user'] = bus.cache.user
    result['user']['userCanCreateAllClientStatistic'] = @userCanCreateAllClientStatistic
    result['user']['userCanCreateSelfClientStatistic'] = @userCanCreateSelfClientStatistic
    result


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @listenTo(bus.vent, 'client:added', @onAdded)
    @collection = new ClientStatisticCollection
    @filter_model = new FilterModel
    @list_view = new ClientStatisticComplexListView
      collection: @collection

    @complex_view = new ClientStatisticComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'client_statistic'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'client_statistic'
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()

  onAdded: =>
    @complex_view.doFilter()


module.exports = LayoutView
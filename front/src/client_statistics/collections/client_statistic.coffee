backbone = require 'backbone'

ClientStatisticModel = require '../models/client_statistic'


class ClientStatisticCollection extends backbone.Collection
  model: ClientStatisticModel
  url: "/api/clients/client_statistics/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ClientStatisticCollection
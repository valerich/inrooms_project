backbone = require 'backbone'

OrderModel = require '../models/order'


class OrderCollection extends backbone.Collection
  model: OrderModel
  url: "/api/orders/order_return/"

  parse: (response) =>
    @count = response.count
    @amount_full_sum = response.amount_full_sum
    return response.results

module.exports = OrderCollection
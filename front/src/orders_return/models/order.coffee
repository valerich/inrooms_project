backbone = require 'backbone'


class OrderModel extends backbone.Model

  url: =>
    if @id
      "/api/orders/order_return/#{@id}/"
    else
      "/api/orders/order_return/"

module.exports = OrderModel
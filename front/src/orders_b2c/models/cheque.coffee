backbone = require 'backbone'


class ChequeModel extends backbone.Model

  url: =>
    if @id
      "/api/orders/cheque/#{@id}/"
    else
      "/api/orders/cheque/"

module.exports = ChequeModel
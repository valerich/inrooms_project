_ = require 'underscore'
backbone = require 'backbone'


class OrderItemModel extends backbone.Model

  initialize: (options) =>
    @order_id = options.order_id

  url: =>
    if @id
      "/api/orders/order_b2c/#{@order_id}/cheque_1c/#{@id}/"
    else
      "/api/orders/order_b2c/#{@order_id}/cheque_1c/"


module.exports = OrderItemModel

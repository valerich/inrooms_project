backbone = require 'backbone'

ChequeModel = require '../models/cheque'


class ChequeCollection extends backbone.Collection
  model: ChequeModel
  url: "/api/orders/cheque/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = ChequeCollection
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'
Highcharts = require('highcharts')
require('highcharts/highcharts-3d')(Highcharts)

ChequeCollection = require '../../collections/cheque'
ChequeModel = require '../../models/cheque'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      items__product: '[name=items__product]'
      client: '[name=client]'
      sort_by: '.sort_list'

    helpers.initAjaxSelect2 @$('[name=items__product]'),
      url: '/api/products/product/'
      placeholder: "Выберите модель"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"

    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: "Выберите клиента"
      minimumInputLength: -1
      allowClear: true
      get_extra_search_params: () =>
        kind: 1

  serializeData: =>
    @can_view_order_b2c_filter_client = helpers.user_has_permission bus.cache.user, 'can_view_order_b2c_filter_client'
    @can_view_order_b2c_filter_products = helpers.user_has_permission bus.cache.user, 'can_view_order_b2c_filter_products'
    result = super
    result['user'] = bus.cache.user
    result['user']['can_view_order_b2c_filter_client'] = @can_view_order_b2c_filter_client
    result['user']['can_view_order_b2c_filter_products'] = @can_view_order_b2c_filter_products
    result


class ReportView extends marionette.LayoutView
  template: require './templates/report'




class ChequeComplexItemView extends complex.ComplexItemView
  template: require './templates/item'

  onRender: =>
    @$('[data-toggle="tooltip"]').tooltip()


class ChequeComplexListView extends complex.ComplexCompositeView
  childView: ChequeComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class ChequeComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'
    'region_report': '.region_report'

  initialize: (options) =>

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    @collection = new ChequeCollection

    @filter_model = new FilterModel(urlParams)
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @filter_view = new FilterView
      model: @filter_model

  onRender: =>
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @region_filter.show(@filter_view)

    list_view = new ChequeComplexListView
      collection: @collection

    @complex_view = new ChequeComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'cheque'

    @region_complex.show(@complex_view)

#    @report_view = new ReportView
#    @region_report.show(@report_view)

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
      @showChart()
    @complex_view.doFilter()
    @showChart()

  showChart: =>

    $chart_1_selector = @$("#chart1")

    a = new Highcharts.chart
      chart:
          renderTo: $chart_1_selector[0]
    a.destroy()

    $.ajax
      url: "/api/orders/reports/cheque_day_report/"
      type: 'get'
      success: (response) =>
        chart_data = []

        for i in response['client_order_b2c']
          chart_data.push([i['client_name'], i['price_sum']])

#        compare = (a,b) ->
#          if (a[1] < b[1])
#            return 1
#          if (a[1] > b[1])
#            return -1
#          return 0
#
#        chart_data.sort(compare)

        chart = new Highcharts.chart
            chart:
                renderTo: $chart_1_selector[0]
                height: 400
                type: 'pie'
                options3d:
                    enabled: true
                    alpha: 45
                    beta: 0
            title:
                text: 'Продажи'
            tooltip:
                pointFormat: '{series.name}: <b>{point.y:.1f}р. ({point.percentage:.1f}%)</b>'
            plotOptions:
                pie:
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels:
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y:.1f}р. ({point.percentage:.1f}%)'
            credits:
                enabled: false
            series: [{
                type: 'pie',
                name: 'Продажи',
                data: chart_data
            }]

      error: (data) =>
        helpers.modelErrorHandler @form_model, data


module.exports = LayoutView
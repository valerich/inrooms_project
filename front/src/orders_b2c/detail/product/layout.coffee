marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

ProductListLayout = require './layout_product'


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'order_detail_product_layout'
  regions:
    region_data: '.region_product_data'

  initialize: (options) =>
    @tab_name = 'product_list'
    @order_model = options.model

  renderTab: (tab_name) =>
    @tab_view = new ProductListLayout
      model: @order_model
    @region_tab.show(@tab_view)

  onRender: =>
    @tab_view = new ProductListLayout
      model: @order_model
    @region_data.show(@tab_view)

    @sidebar_obj = @$('#fixed_sidebar')
    @offset = @sidebar_obj.offset()
    @topOffset = @offset.top
    @topPadding = 220

    $(window).bind('scroll', (ev) =>
      @scrollSidebar()
    )

  scrollSidebar: =>
    if $(window).scrollTop() > @topPadding
      @sidebar_obj.stop().animate({marginTop: $(window).scrollTop() - @topOffset - @topPadding}, 200)
    else
      @sidebar_obj.stop().animate({marginTop: 0}, 200)

module.exports = ObjectDetailLayout

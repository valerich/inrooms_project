_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

OrderItemCollection = require '../../collections/order_item'
BuyerProfileModel = require '../../../buyer_profiles/models/buyer_profile'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/orderitem_filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'

  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    console.log 'togglePanel'
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'


class BuyerProfileItemView extends marionette.ItemView
  template: require './templates/buyer_profile_item'


class OrderItemComplexItemView extends complex.ComplexItemView
  template: require './templates/orderitem_item'
  className: 'order_items_item'

  events:
    'click .buyer_profile_link': 'buyerProfileLinkClick'

  buyerProfileLinkClick: (event) =>
    event.preventDefault()
    buyer_profile_id = $(event.target).data('buyer_profile_id')
    buyer_model = new BuyerProfileModel
      id: buyer_profile_id
    view = new BuyerProfileItemView
      model: buyer_model

    buyer_model.fetch().done =>
      bus.vent.trigger 'modal:show', view,
        title: 'Клиент'
        modal_size: 'lg'

  initialize: (options)=>
    @order_model = options.order_model

  onRender: =>
    @$('[data-toggle="tooltip"]').tooltip();


class OrderItemComplexListView extends complex.ComplexCompositeView
  childView: OrderItemComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/orderitem_items'

  initialize: (options) =>
    @order_model = options.order_model

  childViewOptions: (model, index) =>
    data = {
      order_model: @order_model,
      childIndex: index
    }
    return data


class OrderItemComplexView extends complex.ComplexView
  template: require './templates/orderitem_complex_layout'
  showSearch: false
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout_product'

  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'

  initialize: (options) =>
    @order_model = options.model
    @collection = new OrderItemCollection
      order_id: @order_model.id
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)
    @filter_model = new FilterModel
    @list_view = new OrderItemComplexListView
      collection: @collection
      order_model: @order_model

    @complex_view = new OrderItemComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: @list_view
      name: 'order_items'

    @filter_view = new FilterView
      model: @filter_model

    @listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @listenTo(bus.vent, 'product_item:update', @complex_view.doFilter)

  onRender: =>
    @region_complex.show(@complex_view)
    @region_filter.show(@filter_view)
    @complex_view.doFilter()


module.exports = LayoutView

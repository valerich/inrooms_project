_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
helpers = require 'helpers'
bus = require 'bus'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  initialize: (options) =>
    @order_model = options.model

  serializeData: =>
    @userCanAllOrdersB2CView = helpers.user_has_permission bus.cache.user, 'can_all_orders_b2с_view'
    can_currency_summ_view = false
    if @userCanAllOrdersB2CView == true
      can_currency_summ_view = true
    else
      if bus.cache.user['client'] and bus.cache.user['client']['order_b2c_currency'] != 'RUR'
        can_currency_summ_view = true
    s_data = super
    s_data['user'] = bus.cache.user
    s_data['user']['userCanCurrencySummView'] = can_currency_summ_view
    s_data

module.exports = LayoutView

marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'

CommonLayout = require './common/layout'
ItemsLayout = require './product/layout'

OrderModel = require '../models/order'


class ObjectDetailLayout extends marionette.LayoutView
  template: require './templates/layout'
  className: 'order_detail_layout'
  regions:
    region_products: '.region_products'
    region_common: '.region_common'
  events:
    'click .list_link': 'onClickListLink'

  onClickListLink: (event) =>
    event.preventDefault()
    backbone.history.navigate '/order_b2c/', trigger: true

  initialize: (options) =>
    @binder = new modelbinder
    @model = new OrderModel
      id: options.id

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    @binder.bind @model, @$el,
      number: '.order-id'
    @model.fetch().done =>
      bus.vent.trigger 'menu:active:set', null, 'order_b2c'

      common_view = new CommonLayout
        model: @model
      @region_common.show(common_view)

      products_view = new ItemsLayout
        model: @model
      @region_products.show(products_view)

module.exports = ObjectDetailLayout

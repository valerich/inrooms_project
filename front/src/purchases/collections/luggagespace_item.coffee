backbone = require 'backbone'

LuggageSpaceItemModel = require '../models/luggagespace_item'


class LuggageSpaceItemCollection extends backbone.Collection
  model: LuggageSpaceItemModel

  initialize: (options) =>
    @purchase_id = options.purchase_id

  url: =>
    "/api/purchases/purchase/#{@purchase_id}/luggage_spaces/"

  _prepareModel: (model, options) =>
      model.purchase_id = @purchase_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = LuggageSpaceItemCollection

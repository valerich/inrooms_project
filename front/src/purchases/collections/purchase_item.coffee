backbone = require 'backbone'

PurchaseItemModel = require '../models/purchase_item'


class PurchaseItemCollection extends backbone.Collection
  model: PurchaseItemModel

  initialize: (options) =>
    @purchase_id = options.purchase_id

  url: =>
    "/api/purchases/purchase/#{@purchase_id}/items/"

  _prepareModel: (model, options) =>
      model.purchase_id = @purchase_id
      return backbone.Collection.prototype._prepareModel.call(@, model, options)

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = PurchaseItemCollection

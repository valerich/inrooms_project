backbone = require 'backbone'

PurchaseModel = require '../models/purchase'


class PurchaseCollection extends backbone.Collection
  model: PurchaseModel
  url: "/api/purchases/purchase/"

  parse: (response) =>
    @count = response.count
    return response.results

module.exports = PurchaseCollection
backbone = require 'backbone'


class PurchaseModel extends backbone.Model

  url: =>
    if @id
      "/api/purchases/purchase/#{@id}/"
    else
      "/api/purchases/purchase/"

module.exports = PurchaseModel
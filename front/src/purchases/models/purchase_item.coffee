_ = require 'underscore'
backbone = require 'backbone'


class PurchaseItemModel extends backbone.Model
  defaults:
    quantity: 1
  validation:
    quantity: [
      {
        required: true,
        msg: 'Заполните количество'
      }, {
        pattern: 'digits',
        msg: 'Укажите корректное количество'
      }, {
        range: [1, 1000000000]
        msg: 'Количество не может быть меньше 1 и больше 1 000 000 000'
      }]
    product: [
      {
        required: true,
        msg: 'Заполните модель'
      }
    ]

  initialize: (options) =>
    @purchase_id = options.purchase_id

  url: =>
    if @id
      "/api/purchases/purchase/#{@purchase_id}/items/#{@id}/"
    else
      "/api/purchases/purchase/#{@purchase_id}/items/"


module.exports = PurchaseItemModel

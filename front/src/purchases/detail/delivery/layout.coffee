marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
backbone = require 'backbone'
_ = require 'underscore'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
require "backbone-relational"

purchase_models = require '../../models/purchase2'
LuggageSpaceView = require './luggagespace/layout'
LuggageSpaceItemCollection = require '../../collections/luggagespace_item'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_purchase': 'onClickPurchaseSave'

  regions:
    luggagespace_region: '.luggagespace_region'
    region_luggagespace_add: '.region_luggagespace_add'

  onClickPurchaseSave: =>
    @model.save().done =>
      helpers.generate_notyfication('success', 'Закупка сохранена')
      backbone.history.navigate "/purchase/#{@model.id}/payment/", trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  addLuggageSpaceForm: =>
    if @new_luggagespave_view
      @new_luggagespave_view.destroy()
    @new_luggagespave_view = new NewLuggageSpaceView
      model: new purchase_models.LuggageSpaceModel()
    @region_luggagespace_add.show(@new_luggagespave_view)

  onRender: =>
    @binder.bind @model, @$el,
        delivery_type: '[name=delivery_type]'
        delivery_amount_full: '[name=delivery_amount_full]'
        delivery_number: '[name=delivery_number]'
        delivery_client: '[name=delivery_client]'
        delivery_date:
          selector: '[name=delivery_date]'
          converter: helpers.null_converter
        _all_weight: '.all_weight'
    helpers.initAjaxSelect2 @$('[name=delivery_type]'),
        url: '/api/purchases/delivery_type/'
        placeholder: 'Введите тип доставки'
        allowClear: true
        minimumInputLength: -1
        text_attr: 'name'
    helpers.initAjaxSelect2 @$('[name=delivery_client]'),
        url: '/api/clients/client/'
        placeholder: 'Введите клиента'
        allowClear: true
        minimumInputLength: -1
        text_attr: 'name'
    @$('[name=delivery_date]').datepicker
      autoclose: true

    @luggagespace_collection = new LuggageSpaceItemCollection
      purchase_id: @model.id

    @luggagespace_view = new LuggageSpaceView
      collection: @luggagespace_collection
      model: @model

    @luggagespace_region.show(@luggagespace_view)

  serializeData: =>
    result = super
    result['delivery_warehouse_to_name'] = @model.get('delivery_warehouse_to_detail')['name']
    result


module.exports = LayoutView

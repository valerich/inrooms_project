_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

LuggageSpaceItemCollection = require '../../../collections/luggagespace_item'
LuggageSpaceItemModel = require '../../../models/luggagespace_item'


class LuggageSpaceItemItemView extends marionette.ItemView
  template: require './templates/luggagespace_item_item'
  className: 'luggagespace_items_item'
  events:
    'click .remove': 'onClickRemove'
    'click .save_item': 'onClickSave'

  onClickSave: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Сохранено')
#      bus.vent.trigger 'product_item:update_info'
    .fail (data) ->
      helpers.modelErrorHandler @model, data

  onClickRemove: =>
    bootbox.confirm "Вы действительно хотите элемент из багажа?", (result)=>
      if result == true
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @invoice_model = options.invoice_model

  onRender: =>
    @binder.bind @model, @$el,
      'weight': '[name=weight]'
      'count': '[name=count]'


class NewLuggageSpaceItemView extends marionette.ItemView
  template: require './templates/new_luggagespace_item'
  events:
    'click [name=add]': 'onAddBtnClick'

  initialize: (options)=>
    @binder = new modelbinder
    @purchase_model = options.purchase_model

  onRender: =>
    @binder.bind @model, @$el,
      'weight': '[name=weight]'
      'count': '[name=count]'

    backbone.Validation.bind @

  onAddBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save()
      .done =>
        helpers.generate_notyfication('success', 'Багажное место добавлено')
        bus.vent.trigger 'invoice:items:changed'
        @destroy()
      .fail (data) ->
        helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class LuggageSpaceItemCollectionView extends marionette.CollectionView
  childView: LuggageSpaceItemItemView
  className: 'luggagespace_items_list'

  initialize: (options) =>
    @purchase_model = options.purchase_model

  childViewOptions: (model, index) =>
    data = {
      purchase_model: @purchase_model,
      childIndex: index
    }
    return data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  regions:
    region_item_list: '.region_luggagespace_list'
    region_item_add: '.region_luggagespace_add'

  fetchLuggageSpaceItemCollection: =>
    @luggagespace_item_collection.fetch(
      reset: true,
      data: {
        page_size: 100000
      }
    )

  addNewLuggageSpaceView: =>
    new_luggagespace = new LuggageSpaceItemModel
      purchase_id: @model.id
    @luggagespace_add_view = new NewLuggageSpaceItemView
      model: new_luggagespace
      purchase_model: @purchase_model

    @region_item_add.show(@luggagespace_add_view)

  initialize: (options) =>
    @purchase_model = options.model
    @listenTo bus.vent, 'product_item:update', @fetchLuggageSpaceItemCollection

  onRender: =>
    @luggagespace_item_collection = new LuggageSpaceItemCollection
      purchase_id: @purchase_model.id

    @luggagespace_item_view = new LuggageSpaceItemCollectionView
      collection: @luggagespace_item_collection
      purchase_model: @purchase_model

    @region_item_list.show(@luggagespace_item_view)

    @listenTo(bus.vent, 'invoice:items:changed', @fetchLuggageSpaceItemCollection)
    @listenTo(bus.vent, 'invoice:items:changed', @addNewLuggageSpaceView)
    @fetchLuggageSpaceItemCollection()
    @addNewLuggageSpaceView()


module.exports = LayoutView

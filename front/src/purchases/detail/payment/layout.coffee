_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
helpers = require 'helpers'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_purchase': 'onClickPurchaseSave'

  onClickPurchaseSave: =>
    @model.save().done =>
      helpers.generate_notyfication('success', 'Сохранено')
      backbone.history.navigate "/purchase/#{@model.id}/common/", trigger: true

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      payment_method: '[name=payment_method]'
      amount_full: '[name=amount_full]'
      amount_paid: '[name=amount_paid]'
      description: '[name=description]'

    helpers.initAjaxSelect2 @$('[name=payment_method]'),
      url: '/api/payments/payment_method/'
      placeholder: 'Введите название типа оплаты'
      allowClear: true
      minimumInputLength: -1
      text_attr: 'name'

    @$('[name=description]').summernote
      onChange: (description) =>
        @model.set 'description', description


module.exports = LayoutView

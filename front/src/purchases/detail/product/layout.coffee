_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bus = require 'bus'
store = require 'store'
helpers = require 'helpers'
complex = require 'complex'
bootbox = require 'bootbox'
require 'magnific-popup'

PurchaseItemCollection = require '../../collections/purchase_item'
PurchaseItemModel = require '../../models/purchase_item'
product_select2_item_template = require '../../../products/templates/select2_item'


class PurchaseItemItemView extends marionette.ItemView
  template: require './templates/purchaseitem_item'
  className: 'purchase_items_item'
  events:
    'click .remove': 'onClickRemove'
    'click .save': 'onClickSave'
  modelEvents:
    'change': 'setSeriesCount'

  onClickSave: =>
    @model.save()
    .done =>
      helpers.generate_notyfication('success', 'Модель сохранена')
    .fail (data) ->
      helpers.modelErrorHandler @model, data

  onClickRemove: =>
    bootbox.confirm "Вы действительно хотите удалить модель из поставки?", (result)=>
      if result == true
        @model.destroy().done =>
          helpers.generate_notyfication('success', 'Удалено')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options)=>
    @binder = new modelbinder
    @purchase_model = options.purchase_model

  onRender: =>
    @setSeriesCount()
    @binder.bind @model, @$el,
      price: '[name=price]'
      count_xxs: '[name=count_xxs]'
      count_xs: '[name=count_xs]'
      count_s: '[name=count_s]'
      count_m: '[name=count_m]'
      count_l: '[name=count_l]'
      count_xl: '[name=count_xl]'
      count_xxl: '[name=count_xxl]'
      count_xxxl: '[name=count_xxxl]'
      count_34: '[name=count_34]'
      count_35: '[name=count_35]'
      count_36: '[name=count_36]'
      count_37: '[name=count_37]'
      count_38: '[name=count_38]'
      count_39: '[name=count_39]'
      count_40: '[name=count_40]'
      count_41: '[name=count_41]'
      count_sum: '.count_sum'

  setSeriesCount: =>
    series_type = @model.get('series_type')
    if series_type == 's'
      @$("#order_item_#{@model.id} .c_data").addClass('hidden')
      @$("#order_item_#{@model.id} .s_data").removeClass('hidden')
    else if series_type == 'c'
      @$("#order_item_#{@model.id} .s_data").addClass('hidden')
      @$("#order_item_#{@model.id} .c_data").removeClass('hidden')
    count_sum = 0
    for size in [
              'xxs',
              'xs',
              's',
              'm',
              'l',
              'xl',
              'xxl',
              'xxxl',
              '34',
              '35',
              '36',
              '37',
              '38',
              '39',
              '40',
              '41',
    ]
      count_sum = count_sum + parseInt(@model.get("count_#{size}"))
    @model.set("count_sum", count_sum)

  serializeData: =>
    result = super
    if @purchase_model.get('status_detail')['code'] == 'new'
      result['canEditProducts'] = true
    else
      result['canEditProducts'] = false
    if @purchase_model.get('status_detail')['code'] == 'sent_to_warehouse'
      result['canEditPrice'] = true
    else
      result['canEditPrice'] = false
    result


class NewPurchaseItemView extends marionette.ItemView
  template: require './templates/new_purchaseitem'
  events:
    'click [name=add]': 'onAddBtnClick'

  initialize: (options)=>
    @binder = new modelbinder
    @items_product_ids = options.items_product_ids
    @purchase_model = options.purchase_model

  onRender: =>
    @binder.bind @model, @$el,
      product: '[name=product]'
    helpers.initAjaxSelect2 @$('[name=product]'),
      url: '/api/products/product/'
      placeholder: 'Введите название товара'
      get_extra_search_params: () =>
        exclude_ids: @items_product_ids.join()
        brand: @purchase_model.get('delivery_warehouse_to_detail')['brand_detail']['id']
      minimumInputLength: -1
      text_attr: (obj) =>
        "№:#{obj.id}. #{obj.name} / #{obj.color_detail.name}"
      formatResult: (product) =>
        product_select2_item_template(product)
      escapeMarkup: (markup) =>
        markup

    backbone.Validation.bind @

  onAddBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save()
      .done =>
        helpers.generate_notyfication('success', 'Модель добавлена')
        bus.vent.trigger 'purchase:items:changed'
      .fail (data) ->
        helpers.modelErrorHandler @model, data
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class PurchaseItemCollectionView extends marionette.CollectionView
  childView: PurchaseItemItemView
  className: 'purchase_items_list'

  initialize: (options) =>
    @purchase_model = options.purchase_model

  childViewOptions: (model, index) =>
    data = {
      purchase_model: @purchase_model,
      childIndex: index
    }
    return data


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .save_purchase': 'onClickPurchaseSave'

  regions:
    region_item_list: '.region_item_list'
    region_item_add: '.region_item_add'

  onClickPurchaseSave: =>
    for item in @purchaseitem_collection.models
      if item.changedAttributes()
        item.save().done =>
          helpers.generate_notyfication('success', 'Модель сохранена')
    status_code = @purchase_model.get('status_detail')['code']
    if status_code == 'sent_to_warehouse'
      backbone.history.navigate "/purchase/#{@model.id}/common/", trigger: true
    else
      backbone.history.navigate "/purchase/#{@model.id}/delivery/", trigger: true

  serializeData: =>
    result = super

    if @purchase_model.get('status_detail')['code'] == 'new'
      result['canEditProducts'] = true
    else
      result['canEditProducts'] = false
    return result

  fetchPurchaseItemCollection: =>
    @purchaseitem_collection.fetch(
      reset: true,
      data: {
        page_size: 100000
      }
    ).done =>
      @addPurchaseItemForm()

  initialize: (options) =>
    @purchase_model = options.model
    @listenTo(bus.vent, 'product_item:update', @fetchPurchaseItemCollection)

  addPurchaseItemForm: =>
    items_product_ids = []
    for item in @purchaseitem_collection.models
      items_product_ids.push item.get('product')

    if @new_purchaseitem_view
      @new_purchaseitem_view.destroy()

    @new_purchaseitem_view = new NewPurchaseItemView
      model: new PurchaseItemModel
        purchase_id: @purchase_model.id
      items_product_ids: items_product_ids
      purchase_model: @purchase_model

    @region_item_add.show(@new_purchaseitem_view)

  onRender: =>
    @purchaseitem_collection = new PurchaseItemCollection
      purchase_id: @purchase_model.id

    @purchaseitem_view = new PurchaseItemCollectionView
      collection: @purchaseitem_collection
      purchase_model: @purchase_model

    @region_item_list.show(@purchaseitem_view)

    @listenTo(bus.vent, 'purchase:items:changed', @fetchPurchaseItemCollection)
    @fetchPurchaseItemCollection()


module.exports = LayoutView

_ = require 'underscore'
backbone = require 'backbone'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
bootbox = require 'bootbox'
store = require 'store'
helpers = require 'helpers'


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'

  events:
    'click .change_status': 'onClickChangeStatus'

  initialize: =>
    @model.fetch()

  changeOrderStatus: (status_code) =>
    $.ajax
      url: "/api/purchases/purchase/#{@model.id}/change_status/"
      type: 'post'
      data: {
        'code': status_code
      }
      success: =>
        helpers.generate_notyfication('success', 'Статус изменен')
        backbone.history.navigate "/purchase/", trigger: true
      error: (data) =>
        helpers.modelErrorHandler @model, data

  onClickChangeStatus: (event) =>
    $target = $(event.target)
    new_status_code = $target.data('status_code')
    if new_status_code == 'delivery_waiting' and !@model.get('prices_assigned')
      message = "Вы должны установить цены товаров"
      bootbox.alert message, =>
          backbone.history.navigate "/purchase/#{@model.id}/items/", trigger: true
    else
      message = "Вы действительно хотите изменить статус закупки?"
      bootbox.confirm message, (result)=>
        if result == true
          $target = $(event.target)
          @changeOrderStatus(new_status_code)

module.exports = LayoutView

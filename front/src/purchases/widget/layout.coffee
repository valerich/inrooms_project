$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
complex = require 'complex'
marionette = require 'backbone.marionette'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
modelbinder = require 'backbone.modelbinder'


class PurchaseSummaryModel extends backbone.Model
  url: '/api/purchases/purchase/summary/'


class LayoutView extends marionette.LayoutView
  show_header: false
  template: require './templates/layout'

  events:
    'click .region_set_price': 'onClickRegionToSetPrice'

  initialize: (options) =>
    @userCanViewPurchase = helpers.user_has_permission bus.cache.user, 'can_view_purchase_widget'
    @listenTo(bus.vent, 'purchase:widget:update', @PurchaseWidgetUpdate)
    @model = new PurchaseSummaryModel
    if options.show_header
      @show_header = options.show_header

    @binder = new modelbinder

  onBeforeDestroy: =>
    @binder.unbind()

  onRender: =>
    if @userCanViewPurchase
      @PurchaseWidgetUpdate()
      @binder.bind @model, @$el,
        to_set_prices: '.to_set_prices'
      @$('[data-toggle="tooltip"]').tooltip();

  onClickRegionToSetPrice: =>
    backbone.history.navigate 'purchase/?to_set_prices=2', trigger: true

  serializeData: =>
    data = super
    data['show_header'] = @show_header
    data['userCanViewPurchase'] = @userCanViewPurchase
    data

  PurchaseWidgetUpdate: =>
    @model.fetch()

module.exports = LayoutView
$ = require 'jquery'
_ = require 'underscore'
backbone = require 'backbone'
data = require './../data'
complex = require 'complex'
marionette = require 'backbone.marionette'
modelbinder = require 'backbone.modelbinder'
store = require 'store'
bus = require 'bus'
helpers = require 'helpers'
require 'select2'
require 'backbone-validation'
bootbox = require 'bootbox'

PurchaseCollection = require '../collections/purchase'
PurchaseModel = require '../models/purchase'
PurchaseWidgetLayout = require '../widget/layout'


class FilterModel extends complex.FilterModel


class FilterView extends marionette.ItemView
  template: require './templates/filter'
  events:
    'submit .search_form': 'onSubmitForm'
    'click .toggle_filter_panel': 'togglePanel'
    'click .add': 'onClickAdd'

  onClickAdd: =>
    addNewPurchaseView = new NewPurchaseView
      model: new PurchaseModel

    bus.vent.trigger 'modal:show', addNewPurchaseView,
      title: 'Добавить новую закупку'


  onSubmitForm: (event) =>
    event.preventDefault()
    @model.trigger 'change'

  togglePanel: =>
    @$('#region_filter_panel').toggleClass('hidden')

  onBeforeDestroy: =>
    @binder.unbind()

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      search: '[name=search]'
      status: '[name=status]'
      items__product: '[name=product]'
      delivery_client: '[name=client]'
      to_set_prices: '[name=to_set_prices]'
      sort_by: '.sort_list'
      delivery_warehouse_to: '[name=delivery_warehouse_to]'

    helpers.initAjaxSelect2 @$('[name=delivery_warehouse_to]'),
      url: '/api/warehouses/warehouse/'
      placeholder: 'Выберите склад поставки'
      minimumInputLength: -1
      text_attr: 'name'
      get_extra_search_params: () =>
        to_purchase: 2

    helpers.initAjaxSelect2 @$('[name=status]'),
      url: "/api/purchases/purchase_status/"
      placeholder: 'Выберите статус'
      minimumInputLength: -1
      text_attr: 'name'
      allowClear: true
    helpers.initAjaxSelect2 @$('[name=product]'),
      url: '/api/products/product/'
      placeholder: "Выберите модель"
      minimumInputLength: -1
      allowClear: true
      text_attr: (obj) =>
        return "№:#{obj.id}. #{obj.name}"
    helpers.initAjaxSelect2 @$('[name=client]'),
      url: '/api/clients/client/'
      placeholder: "Выберите клиента"
      minimumInputLength: -1
      allowClear: true


class NewPurchaseView extends marionette.ItemView
  template: require './templates/new_purchase_modal'
  events:
    'click [name=create]': 'onCreateBtnClick'
  translated_fields:
    delivery_warehouse_to: 'На склад'

  initialize: (options) =>
    @binder = new modelbinder

  onRender: =>
    @binder.bind @model, @$el,
      delivery_warehouse_to: '[name=delivery_warehouse_to]'

    helpers.initAjaxSelect2 @$('[name=delivery_warehouse_to]'),
      url: '/api/warehouses/warehouse/'
      placeholder: 'Выберите склад поставки'
      minimumInputLength: -1
      text_attr: 'name'
      get_extra_search_params: () =>
        to_purchase: 2

    backbone.Validation.bind @

  onCreateBtnClick: (event) =>
    event.preventDefault()
    v = @model.validate()
    if @model.isValid()
      @model.save {},
        success: (model) =>
          helpers.generate_notyfication('success', 'Закупка создана')
          backbone.history.navigate "/purchase/#{model.id}/", {trigger: true}
          @destroy()
        error: (model, response, options) =>
          console.log 'model save error'
          helpers.modelErrorHandler model, response, options, @translated_fields
    else
      error_str = _.values(v).join('<br>')
      helpers.generate_notyfication('error', error_str)

  @onBeforeDestroy: =>
    @binder.unbind()


class PurchaseComplexItemView extends complex.ComplexItemView
  tagName: 'tr'
  template: require './templates/item'
  triggers:
    'click td': 'click'


class Purchase2ComplexItemView extends complex.ComplexItemView
  template: require './templates/item2'
  triggers:
    'click .panel': 'click'


class PurchaseComplexListView extends complex.ComplexCompositeView
  childView: PurchaseComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items'


class Purchase2ComplexListView extends complex.ComplexCompositeView
  childView: Purchase2ComplexItemView
  childViewContainer: ".region_table_items"
  template: require './templates/items2'


class PurchaseComplexView extends complex.ComplexView
  template: require './templates/complex_layout'
  showSearch: false
  urlFilterParam: true
  d: 2


class LayoutView extends marionette.LayoutView
  template: require './templates/layout'
  regions:
    'region_complex': '.region_complex'
    'region_filter': '.region_filter'
    'region_widgets': '.region_widgets'
  events:
    'click .list_type': 'onClickListType'

  initialize: (options) =>
    @listenTo(bus.vent, 'purchase:added', @onPurchaseAdded)

    #http://stackoverflow.com/a/2880929/752397
    pl = /\+/g
    search = /([^&=]+)=?([^&]*)/g
    decode = (s)->
      return decodeURIComponent(s.replace(pl, " "))
    query = window.location.search.substring(1)

    urlParams = {}
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2])

    @collection = new PurchaseCollection

    @filter_model = new FilterModel(urlParams)
    data.reqres.setHandler 'filter_model', =>
      @filter_model

    @filter_view = new FilterView
      model: @filter_model

    @widgets_view = new PurchaseWidgetLayout

  onPurchaseAdded: =>
    @complex_view.doFilter()

  onClickListType: (event) =>
    $target = $(event.target)
    list_type = $target.data('list_type')
    @renderPurchaseList(list_type)

  onRender: =>
    bus.vent.trigger 'menu:active:set', null, 'purchase'
    @listenTo @collection, 'sync', =>
      @$('.objects_count').text(@collection.count)

    @region_filter.show(@filter_view)
#    @region_widgets.show(@widgets_view)

    list_type = store.get 'purchase_list_type'
    if !list_type
      list_type = 'list1'
    @renderPurchaseList(list_type)

    @region_widgets.show(@widgets_view)

  renderPurchaseList: (list_type) =>
    store.set 'purchase_list_type', list_type
    if list_type is 'list1'
      PurchaseView = PurchaseComplexListView
    if list_type is 'list2'
      PurchaseView = Purchase2ComplexListView
    @$(".list_type").removeClass('active')
    @$(".list_type[data-list_type=#{list_type}]").addClass('active')

    list_view = new PurchaseView
      collection: @collection

    @complex_view = new PurchaseComplexView
      collection: @collection
      filter_model: @filter_model
      list_view: list_view
      name: 'purchase'

    @region_complex.show(@complex_view)

    @listenTo @complex_view, 'childview:click', (view, options) =>
      backbone.history.navigate "/purchase/#{view.model.id}/", {trigger: true}

    @complex_view.listenTo @filter_model, 'change', =>
      @complex_view.doFilter()
    @complex_view.doFilter()

  serializeData: =>
    serialized_data = super
    serialized_data['list_name'] = 'Закупки'
    serialized_data

module.exports = LayoutView
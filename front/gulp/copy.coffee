gulp = require 'gulp'

gulp.task 'copy', ->
  gulp.src './copy/**'
  .pipe gulp.dest "./dist/"

  gulp.src [
    'fonts/**/*'
    'node_modules/bootstrap/fonts/*'
    'node_modules/font-awesome/fonts/*'
    'node_modules/simple-line-icons/fonts/*'
  ]
  .pipe gulp.dest('./dist/fonts')

  gulp.src [
    'node_modules/open-sans-fontface/fonts/**/*'
  ]
  .pipe gulp.dest('./dist/fonts/open-sans')

  gulp.src [
    'node_modules/raven-js/dist/raven.js'
  ]
  .pipe gulp.dest('./dist/')

#  gulp.src [
#    'node_modules/select2/*.{png,gif}'
#  ]
#  .pipe gulp.dest('./dist/css/')
from django.conf.urls import url

from .views import PlayerView

urlpatterns = [
    url(r'$', PlayerView.as_view(), name='player'),
]

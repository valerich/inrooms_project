import os
import uuid

from django.db import models
from model_utils.models import TimeStampedModel


def generate_music_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'music/{}{}'.format(uuid.uuid4(), ext)


class Music(TimeStampedModel):
    filename = models.CharField('Название файла', max_length=1000, blank=True)
    file = models.FileField('Файл', upload_to=generate_music_filename, max_length=755, blank=True)

    class Meta:
        verbose_name = 'Музыкальная запись'
        verbose_name_plural = 'Музыкальные записи'
        ordering = ('-id', )

    def get_file_name(self):
        file = self.file.name.split('/')[-1]
        return file[:file.rfind('.')]

    def __str__(self):
        return self.filename


class Video(TimeStampedModel):
    name = models.CharField('Название', max_length=100)
    video_code = models.TextField('Код видео', blank=True)

    description = models.TextField('Описание', blank=True)

    class Meta:
        verbose_name = 'Видео'
        verbose_name_plural = 'Видео'
        ordering = ('-id', )

    def __str__(self):
        return self.name

from pydub import AudioSegment

from config.celery import app
from .models import Music


@app.task(name='multimedia.convert_to_mp3')
def convert_to_mp3(music_ids):
    """Конвертирование треков из mp3 в ogg
    """
    for id in music_ids:
        music = Music.objects.get(id=id)
        song = AudioSegment.from_mp3(music.file.path)
        song.export(music.file.path[:-3] + 'ogg', format="ogg")

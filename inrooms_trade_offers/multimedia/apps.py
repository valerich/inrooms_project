from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'multimedia'
    verbose_name = 'Мультимедиа'

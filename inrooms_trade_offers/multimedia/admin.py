from django.contrib import admin

from .models import Music, Video


class VideoAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', ]


class MusicAdmin(admin.ModelAdmin):
    list_display = ['id', 'filename', 'created', ]


for model_or_iterable, admin_class in (
    (Music, MusicAdmin),
    (Video, VideoAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

from django.conf import settings
from rest_framework import filters
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.viewsets import GenericViewSet

from ..serializers import VideoSerializer
from ...models import Video


class VideoViewSet(RetrieveModelMixin,
                   ListModelMixin,
                   GenericViewSet):
    serializer_class = VideoSerializer
    model = Video
    queryset = Video.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('=id', )


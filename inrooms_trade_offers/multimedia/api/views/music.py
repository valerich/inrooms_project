from django.conf import settings
from rest_framework import filters, status
from rest_framework.decorators import list_route
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from config.celery import app
from ..serializers import MusicSerializer
from ...models import Music


class MusicUploadView(APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request, **kwargs):
        music_file = request.FILES['music_file']

        Music.objects.create(
            file=music_file,
        )
        return Response({
            'status': 'ok'
        })


class MusicViewSet(ModelViewSet):
    serializer_class = MusicSerializer
    model = Music
    queryset = Music.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('=id', )

    @list_route(methods=['post'])
    def upload(self, request):
        music_file = request.FILES['music_file']
        if not music_file.name.split('.')[-1] in ["mp3", ]:
           return Response(status=status.HTTP_400_BAD_REQUEST, data={
            'error': 'Не поддерживаемый формат файла'
        })
        music_obj = Music.objects.create(
            filename=music_file.name,
            file=music_file,
        )
        if not settings.IS_TESTING:
            app.send_task('multimedia.convert_to_mp3', args=((music_obj.id,),))
        return Response({
            'status': 'ok'
        })

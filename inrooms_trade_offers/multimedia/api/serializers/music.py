from rest_framework import serializers

from ...models import Music


class MusicSerializer(serializers.ModelSerializer):
    filename = serializers.SerializerMethodField()

    class Meta:

        model = Music
        fields = [
            'id',
            'file',
            'filename',
        ]

    def get_filename(self, obj):
        return obj.filename

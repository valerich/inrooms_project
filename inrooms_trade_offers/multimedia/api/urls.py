from django.conf import urls
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'music', views.MusicViewSet, base_name='music')
router.register(r'video', views.VideoViewSet, base_name='video')


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
]

import random

from django.views.generic import TemplateView

from multimedia.models import Music


class PlayerView(TemplateView):
    template_name = 'multimedia/player.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tracks = list(Music.objects.all())
        random.shuffle(tracks)
        context['tracks'] = tracks
        return context

import datetime

from django.shortcuts import get_object_or_404
from wkhtmltopdf.views import PDFTemplateView
from pytils import numeral

from core.utils.lists import group
from .models import Offer


class AggreementOfferExport(PDFTemplateView,):
    template_name = 'offers/aggreement_offer_pdf.html'
    filename = "Новые модели.pdf"

    def get_context_data(self, *args, **kwargs):
        context = super(AggreementOfferExport, self).get_context_data(*args, **kwargs)

        qs = Offer.objects.order_by('-modified')
        qs = qs.filter(status__can_edit_to_aggreement=True)
        qs = qs.filter(to_aggreement=True)

        # items = group(qs, 4)
        context['items'] = qs
        return context
import factory
import factory.django
import factory.fuzzy

from accounts.factories import UserFactory
from .models import Offer


class OfferFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Offer

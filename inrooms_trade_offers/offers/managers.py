import operator
from functools import reduce

from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.query_utils import Q

from comments.models import Comment


class OfferManager(models.Manager):

    def get_queryset(self):
        return OfferQuerySet(self.model).filter(is_deleted=False)


class OfferDeletedManager(models.Manager):

    def get_queryset(self):
        return OfferQuerySet(self.model).filter(is_deleted=True)


class OfferQuerySet(models.query.QuerySet):

    def for_user(self, user):
        if user.has_perm('auth.can_offers_view'):
            if user.has_perm('auth.can_offers_all_view'):
                return self
            else:
                q_list = [Q(user=user), Q(worker=user)]
                if user.has_perm('auth.can_get_offers_blank_worker'):
                    q_list.append(Q(worker__isnull=True))
                return self.filter(reduce(operator.or_, q_list))
        else:
            return self.none()

    def approving_queue(self):
        """Товары раздела "Согласование" """
        return self.filter(status__code__in=['order_sample',
                                             'order_sample_cor',
                                             'sample_ordered',
                                             'sample_ordered_cor',
                                             'sample_ready',
                                             'sample_fail',
                                             'return_sample',
                                             'sample_returned',
                                             'sample_returned_fail',
                                             'sample_correction', ])

    def draft_queue(self):
        """Образцы раздела "Предложения" """
        return self.filter(status__code__in=['draft', 'approving', 'correction', ])

    def approved(self):
        """Согласованные образцы"""

        return self.filter(status__code__in=['approved', 'models_created'])

    def archive(self):
        """Образцы в архиве"""

        return self.filter(status__code='archive')

    def to_work(self, user):
        """Возвращает список образцов для работы

        Сейчас это все образцы, для статусов которых есть право 'auth.can_samples_edit_{code статуса}'
        """

        from offers.models import OfferStatus
        psw = 'auth.can_offers_edit_'
        work_status_codes = [perm[len(psw):] for perm in user.get_all_permissions() if perm.startswith(psw)]
        statuses = OfferStatus.objects.filter(code__in=work_status_codes)
        return self.filter(status__in=statuses)

    def unread_comments(self, user=None):
        groups_mapping = {
            'Администратор процесса': 'Бренд менеджер',
            'Бренд менеджер': 'Администратор процесса',
        }
        offer_content_type = ContentType.objects.get(app_label="offers", model="offer")
        comments = Comment.objects.all()

        if user:
            names = []
            for group in user.groups.all():
                unread_group_name = groups_mapping.get(group.name, None)
                if unread_group_name is not None:
                    names.append(unread_group_name)
            if names:
                comments = comments.filter(user__groups__name__in=names)
            else:
                comments = comments.none()
        comments = comments.filter(is_new=True)
        comments = comments.filter(content_type=offer_content_type)
        offer_ids = comments.values_list('object_id', flat=True)
        return self.filter(id__in=offer_ids)


class AggreementCartManager(models.Manager):

    def get_for_user(self, user):
        from .models import AggreementCart
        return AggreementCart.objects.get_or_create(user=user)[0]

from autocomplete_light import shortcuts as autocomplete_light
from django import forms


class OfferAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'user': autocomplete_light.ChoiceWidget('UserAutocomplete'),
            'status': autocomplete_light.ChoiceWidget('OfferStatusAutocomplete'),
        }


class OfferImageAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'offer': autocomplete_light.ChoiceWidget('OfferAutocomplete'),
        }


class OfferStatusForm(forms.ModelForm):

    class Meta:
        widgets = {
            'next_status': autocomplete_light.MultipleChoiceWidget('OfferStatusAutocomplete')
        }

import os
import uuid
from decimal import Decimal

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.db.models.deletion import PROTECT, SET_NULL
from django_resized import ResizedImageField
from model_utils.choices import Choices
from model_utils.models import TimeStampedModel
from model_utils.tracker import FieldTracker

from accounts.models import User
from comments.models import Comment
from consistency.models import ConsistencyMixin
from care.models import CareMixin
from core.models import CURRENCY_CHOICES, DeleteMixin
from .managers import AggreementCartManager, OfferDeletedManager, OfferManager


class OfferStatus(models.Model):
    name = models.CharField('Название', max_length=100, unique=True)
    code = models.CharField('Код', max_length=20, unique=True, db_index=True)

    next_status = models.ManyToManyField('self', verbose_name=u'Следующие статусы', blank=True, symmetrical=False)
    weight = models.PositiveSmallIntegerField('Вес', default=0)
    icon = models.CharField('Иконка', max_length=50, blank=True)
    color = models.CharField('Цвет', max_length=50, blank=True)
    to_edit_offer_data = models.BooleanField('Можно редактировать данные предложения в этом статусе', default=False)
    can_edit_to_aggreement = models.BooleanField('Можно отправлять на согласование клиентами', default=False)

    class Meta:
        verbose_name = 'Статус'
        verbose_name_plural = 'Статусы'
        ordering = ('-weight', )

    def __str__(self):
        return self.name


def get_default_offer_status():
    return OfferStatus.objects.get_or_create(code='draft', defaults={'name': 'Черновик'})[0].id


SAMPLE_PAYMENT_TYPE_CHOICES = Choices(
    (1, 'card', 'Картой'),
    (2, 'weechat', 'WeeChat'),
)


class Offer(ConsistencyMixin, CareMixin, DeleteMixin, TimeStampedModel):
    CURRENCY = CURRENCY_CHOICES
    SAMPLE_PAYMENT_TYPE = SAMPLE_PAYMENT_TYPE_CHOICES
    status = models.ForeignKey(OfferStatus, verbose_name='Статус', default=get_default_offer_status)
    user = models.ForeignKey(User, verbose_name='Создатель', related_name='offers_user')
    worker = models.ForeignKey(User, verbose_name='Ответственный', related_name='offers_worker', blank=True, null=True)
    name = models.CharField('Название', blank=True, max_length=255)
    currency = models.CharField('Валюта', choices=CURRENCY, default=CURRENCY.CNY, max_length=3)
    links = models.TextField('Ссылки', blank=True)

    main_collection = models.ForeignKey('products.ProductCollection', verbose_name='Коллекция', blank=True, null=True, on_delete=SET_NULL)

    price = models.PositiveIntegerField('Цена', blank=True, null=True)

    payment_account = models.ForeignKey('payments.PaymentAccount', verbose_name='Счет', blank=True, null=True)

    prepayment_cost = models.PositiveSmallIntegerField('Заказ образца', default=0)
    prepayment_delivery_cost = models.PositiveIntegerField('Доставка образца с суммами', default=0)
    cost = models.PositiveSmallIntegerField('Остаток стоимости', default=0)
    delivery_cost = models.PositiveIntegerField('Доп транспортные расходы', default=0)
    to_aggreement = models.BooleanField('На согласование клиентами', default=False)
    is_manufacturing_aggreement = models.BooleanField('Согласование до получения заказа с фабрики', default=False)
    aggreement_name = models.CharField('Наименование согласования', max_length=255, default='-')
    business_name = models.CharField('Коммерческое наимерование', max_length=255, blank=True)
    catalog_description = models.TextField('Описание для каталога', blank=True)
    aggreement_code = models.CharField('Код согласования', max_length=100, default='0000/000')
    aggreement_price = models.PositiveSmallIntegerField('Цена согласования', default=0)
    aggreement_colors = models.ManyToManyField('products.ProductColor', verbose_name='Цвета на согласование', blank=True)
    aggreement_sizes = models.ManyToManyField('products.Size', verbose_name='Размеры для согласования', blank=True)

    sample_date_order = models.DateField('Дата заказа образца', blank=True, null=True)
    sample_order_number = models.CharField('Номер заказа образца на таобао', null=True, max_length=100)
    sample_payment_method = models.PositiveSmallIntegerField('Способ оплаты образца', choices=SAMPLE_PAYMENT_TYPE,
                                                             blank=True, null=True)

    cost_rur = models.DecimalField('Стоимость в рублях', max_digits=20, decimal_places=10, default=Decimal('0.00'))
    is_suplied = models.BooleanField('Отправлен в Москву', default=False)
    can_edit_price = models.BooleanField('Можно редактировать цену руками?', default=True)

    comments = GenericRelation(Comment)

    tracker = FieldTracker(fields=['status'])

    # Managers
    objects = OfferManager()
    deleted_objects = OfferDeletedManager()

    class Meta:
        verbose_name = 'Предложение'
        verbose_name_plural = 'Предложения'
        ordering = ('-id',)

    def __str__(self):
        return '№:{}. {}'.format(self.id, self.name)

    @property
    def main_im(self):
        try:
            return self.images.all()[0].image
        except IndexError:
            return None


def generate_offer_image_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'offer/fullsize/{}{}'.format(uuid.uuid4(), ext)


class OfferImage(TimeStampedModel):
    image = ResizedImageField('Изображение', upload_to=generate_offer_image_filename, max_length=755, blank=True)
    visual_order = models.PositiveSmallIntegerField('Порядок отображения', default=0)
    offer = models.ForeignKey(Offer, related_name='images')

    class Meta:
        verbose_name = 'Изображение предложения'
        verbose_name_plural = 'Изображения предложений'
        ordering = ['visual_order', '-id']

    def __str__(self):
        return 'id: {} - sample_id: {}'.format(self.pk, self.product_id)


class AggreementCart(models.Model):
    user = models.OneToOneField('accounts.User', verbose_name='Создатель')
    client = models.ForeignKey('clients.Client', verbose_name='Партнер', blank=True, null=True)

    objects = AggreementCartManager()

    class Meta:
        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзины'
        ordering = ('id', )


class AggreementCartItem(models.Model):
    cart = models.ForeignKey(AggreementCart, verbose_name='Корзина', related_name='items')
    offer = models.ForeignKey(Offer, verbose_name='Предложение')
    color = models.ForeignKey('products.ProductColor', verbose_name='Цвет')

    class Meta:
        verbose_name = 'Элемент корзины'
        verbose_name_plural = 'Элементы корзины'
        ordering = ('id', )
        unique_together = ('cart', 'offer', 'color')

    def get_quantity(self):
        quantity = 0
        sizes = {size.code: size for size in self.offer.aggreement_sizes.all()}
        if len(sizes) == 1 and 'm' in sizes:
            quantity = 6
        elif len(sizes) == 2 and all([item in sizes for item in ['s', 'm']]):
            quantity = 6
        else:
            for size in sizes.values():
                quantity += size.multiplier
        return quantity

    def get_amount(self):
        if self.offer.aggreement_price:
            return self.offer.aggreement_price * self.get_quantity()
        else:
            return 0


def offer_set_price(sender, instance, **kwargs):
    if instance.cost or instance.prepayment_cost or instance.delivery_cost or instance.prepayment_delivery_cost:
        instance.price = instance.prepayment_cost + instance.cost
        instance.can_edit_price = False


models.signals.pre_save.connect(offer_set_price, sender=Offer)

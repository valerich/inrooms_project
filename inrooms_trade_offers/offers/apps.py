from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'offers'
    verbose_name = 'Предложения'

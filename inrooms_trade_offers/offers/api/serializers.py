import logging

from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from comments.api.serializers import CommentSerializer
from ..models import AggreementCart, AggreementCartItem, Offer, OfferImage, OfferStatus

logger = logging.getLogger(__name__)


class UpdateFromZaraFormSerializer(serializers.Serializer):
    url = serializers.URLField()

    class Meta:
        fields = [
            'url',
        ]


class OfferStatusSerializer(serializers.ModelSerializer):
    next_status_detail = serializers.SerializerMethodField()

    class Meta:
        model = OfferStatus
        fields = [
            'id',
            'name',
            'code',
            'next_status_detail'
        ]

    def get_next_status_detail(self, obj):
        return [{'id': s.id,
                 'name': s.name,
                 'code': s.code} for s in obj.next_status.all()]


class AggreementOfferSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()
    aggreement_colors_detail = serializers.SerializerMethodField()
    main_collection_detail = serializers.SerializerMethodField()
    aggreement_sizes_detail = serializers.SerializerMethodField()

    class Meta:
        model = Offer
        fields = [
            'id',
            'name',
            'aggreement_code',
            'aggreement_price',
            'aggreement_colors',
            'aggreement_colors_detail',
            'aggreement_sizes',
            'aggreement_sizes_detail',
            'images',
            'is_manufacturing_aggreement',
            'business_name',
            'catalog_description',
            'main_collection_detail',
        ]

    def get_main_collection_detail(self, obj):
        if obj.main_collection:
            return {
                'id': obj.main_collection.id,
                'name': obj.main_collection.name,
                'series_type': obj.main_collection.series_type,
            }
        return None

    def get_aggreement_colors_detail(self, obj):
        return [
            {
                'id': color.id,
                'name': color.name,
            } for color in obj.aggreement_colors.all()]

    def get_aggreement_sizes_detail(self, obj):
        return [
            {
                'id': size.id,
                'name': size.name,
            } for size in obj.aggreement_sizes.all()]

    def get_images(self, obj):
        data = []
        for image in list(obj.images.all())[:3]:
            item = self.resize_image(image, obj)
            if item:
                data.append(item)
        return data

    def resize_image(self, pi, obj):
        if pi:
            try:
                t = get_thumbnail(pi.image, '400x400', crop="center")
            except IOError as e:
                logger.warning('Проблемы с изображением {} продукта {}: {}'.format(pi.image, obj, e))
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            _200x200 = t.url
            _950x1200 = t.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,
                    '950x1200': _950x1200}
        else:
            return None


class OfferSerializer(serializers.ModelSerializer):
    status_detail = serializers.SerializerMethodField()
    user_detail = serializers.SerializerMethodField()
    worker_detail = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    currency_display = serializers.SerializerMethodField()
    comments = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()
    products = serializers.SerializerMethodField()
    prepayment_cost = serializers.SerializerMethodField()
    prepayment_delivery_cost = serializers.SerializerMethodField()
    cost = serializers.SerializerMethodField()
    delivery_cost = serializers.SerializerMethodField()
    aggreement_price = serializers.SerializerMethodField()

    class Meta:
        model = Offer

        fields = [
            'id',
            'status',
            'status_detail',
            'name',
            'business_name',
            'image',
            'user_detail',
            'worker_detail',
            'price',
            'prepayment_cost',
            'prepayment_delivery_cost',
            'cost',
            'delivery_cost',
            'currency',
            'currency_display',
            'created',
            'modified',
            'comments',
            'images',
            'links',
            'is_deleted',
            'products',
            'to_aggreement',
            'aggreement_code',
            'main_collection',
            # 'payment_account',
            # Уход
            'wash',
            'bleach',
            'drying',
            'ironing',
            'professional_care',
            # Состав
            'consistency_type1',
            'consistency_value1',
            'consistency_type2',
            'consistency_value2',
            'consistency_type3',
            'consistency_value3',
            'consistency_type4',
            'consistency_value4',
            'consistency_type5',
            'consistency_value5',
            'aggreement_price',
            'can_edit_price',

            'sample_date_order',
            'sample_order_number',
            'sample_payment_method',
        ]
        read_only_fields = (
            'can_edit_price',
        )

    def main_collection_detail(self, obj):
        if obj.main_collection:
            return {
                'id': obj.main_collection.id,
                'name': obj.main_collection.name,
                'series_type': obj.main_collection.series_type,
            }
        return None

    def get_prepayment_cost(self, obj):
        return obj.prepayment_cost

    def get_prepayment_delivery_cost(self, obj):
        return obj.prepayment_delivery_cost

    def get_cost(self, obj):
        return obj.cost

    def get_delivery_cost(self, obj):
        return obj.delivery_cost

    def get_status_detail(self, obj):
        return {'id': obj.status_id,
                'name': obj.status.name,
                'code': obj.status.code,
                'icon': obj.status.icon,
                'color': obj.status.color,
                'to_edit_offer_data': obj.status.to_edit_offer_data,
                'can_edit_to_aggreement': obj.status.can_edit_to_aggreement,
                'next_statuses': [{
                    'id': s.id,
                    'name': s.name,
                    'code': s.code,
                    'icon': s.icon,
                    'color': s.color,
                } for s in obj.status.next_status.all()]}


    def get_user_detail(self, obj):
        return {'id': obj.user_id,
                'name': obj.user.name}

    def get_worker_detail(self, obj):
        if obj.worker_id:
            return {'id': obj.worker_id,
                    'name': obj.worker.name}
        return None

    def get_image(self, obj):
        pi = obj.images.all().first()
        return self.resize_image(pi, obj)

    def get_images(self, obj):
        data = []
        for image in obj.images.all():
            item = self.resize_image(image, obj)
            if item:
                data.append(item)
        return data

    def resize_image(self, pi, obj):
        if pi:
            try:
                t = get_thumbnail(pi.image, '400x400', crop="center")
            except IOError as e:
                logger.warning('Проблемы с изображением {} продукта {}: {}'.format(pi.image, obj, e))
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            _200x200 = t.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,}
        else:
            return None

    def get_currency_display(self, obj):
        if obj.currency:
            return obj.get_currency_display()
        else:
            return ''

    def get_comments(self, obj):
        kwargs = {}
        if 'request' in self.context:
            kwargs['request_user'] = self.context['request'].user
        data = []
        for comment in obj.comments.all().order_by('created'):
            data.append(CommentSerializer(comment, **kwargs).data)
        return data

    def get_products(self, obj):
        data = []
        for product in obj.product_set.all():
            data.append({
                'id': product.id,
                'name': product.name,
                'article': product.article,
                'color': product.color.name,
            })
        return data

    def get_aggreement_price(self, obj):
        return obj.aggreement_price


class OfferImageSerializer(serializers.ModelSerializer):
    thumb = serializers.SerializerMethodField()
    image = serializers.ImageField(read_only=True)

    def get_thumb(self, obj):
        if obj.image is None:
            return None
        try:
            t = get_thumbnail(obj.image, '200x200', crop='center')
        except IOError:
            return None
        if t:
            return t.url
        else:
            return None

    class Meta:
        model = OfferImage
        fields = [
            'id',
            'thumb',
            'image',
            'visual_order',
        ]


class AggreementCartSerializer(serializers.ModelSerializer):
    offer_item_count = serializers.SerializerMethodField()

    class Meta:
        model = AggreementCart
        fields = (
            'id',
            'offer_item_count',
            'client',
        )

    def get_offer_item_count(self, obj):
        return obj.items.count()


class AggreementCartItemSerializer(serializers.ModelSerializer):
    offer_detail = serializers.SerializerMethodField()
    color_detail = serializers.SerializerMethodField()
    sizes_detail = serializers.SerializerMethodField()
    quantity = serializers.SerializerMethodField()
    amount = serializers.SerializerMethodField()

    class Meta:
        model = AggreementCartItem
        fields = (
            'id',
            'offer_detail',
            'color_detail',
            'sizes_detail',
            'quantity',
            'amount',
        )

    def get_quantity(self, obj):
        quantity = 0
        sizes = {size.code: size for size in obj.offer.aggreement_sizes.all()}
        if len(sizes) == 1 and 'm' in sizes:
            quantity = 6
        elif len(sizes) == 2 and all([item in sizes for item in ['s', 'm']]):
            quantity = 6
        else:
            for size in sizes.values():
                quantity += size.multiplier
        return quantity

    def get_amount(self, obj):
        if obj.offer and obj.offer.aggreement_price:
            return obj.offer.aggreement_price * self.get_quantity(obj)
        else:
            return 0

    def get_sizes_detail(self, obj):
        return [
            {
                'id': size.id,
                'name': size.name
            } for size in obj.offer.aggreement_sizes.all()
        ]

    def get_offer_detail(self, obj):
        return {
            'id': obj.offer.id,
            'business_name': obj.offer.business_name,
            'name': obj.offer.name,
            'aggreement_code': obj.offer.aggreement_code,
            'aggreement_price': obj.offer.aggreement_price,
            'image': self.resize_image(obj.offer.images.all()[0], obj.offer) if obj.offer.images.all().exists() else None,
        }

    def get_color_detail(self, obj):
        return {
            'id': obj.color.id,
            'name': obj.color.name,
        }

    def resize_image(self, pi, obj):
        if pi:
            try:
                t = get_thumbnail(pi.image, '400x400', crop="center")
            except IOError as e:
                logger.warning('Проблемы с изображением {} продукта {}: {}'.format(pi.image, obj, e))
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            _200x200 = t.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,}
        else:
            return None

from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from offers.api.serializers import AggreementOfferSerializer
from offers.models import Offer


class AggreementOfferMixin(mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.ListModelMixin,
                           GenericViewSet):
    serializer_class = AggreementOfferSerializer
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend)
    filter_fields = [
        'to_aggreement',
    ]
    search_fields = ('name', '=id')
    queryset = Offer.objects.order_by('-modified')
    ordering_fields = (
        'id',
        'name',
        'modified',
    )

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(status__can_edit_to_aggreement=True)
        return qs


class AggreementOfferEditViewSet(AggreementOfferMixin):
    pass


class AggreementOfferViewSet(AggreementOfferMixin):

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(to_aggreement=True)
        if not self.request.user.has_perm('auth.can_aggreement_offer_view'):
            qs = qs.none()
        return qs

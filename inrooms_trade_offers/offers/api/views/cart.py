from django.contrib.auth.models import Group
from django.db import transaction
from django.db.utils import IntegrityError
from rest_framework import filters, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from clients.models import Client
from core.utils.views.additional_data_pagination import AdditionalDataPaginatedViewMixin
from offers.models import Offer
from products.models import Product, ProductColor
from ..serializers import AggreementCartItemSerializer, AggreementCartSerializer
from ...models import AggreementCart, AggreementCartItem


class CartMixin(APIView):
    def dispatch(self, request, *args, **kwargs):
        self.cart = AggreementCart.objects.get_for_user(request.user)
        return super(CartMixin, self).dispatch(request, *args, **kwargs)


class UserCartView(CartMixin):
    def get(self, request, *args, **kwargs):
        return Response(data=AggreementCartSerializer(self.cart).data)

    def post(self, request, *args, **kwargs):
        self.cart.client_id = request.data.get('client', None)
        self.cart.save()
        return Response(data=AggreementCartSerializer(self.cart).data)

    def put(self, request, *args, **kwargs):
        client = request.data.get('client', None)
        if client:
            client = Client.objects.get(id=client)
        self.cart.client = client
        self.cart.save()
        return Response(data=AggreementCartSerializer(self.cart).data)


class UserCartCreateOrderView(CartMixin):

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        from offer_requests.models import OfferRequest, OfferRequestItem

        o_status = OfferRequest.STATUS.agreement
        group_process_administrator = Group.objects.get_or_create(name='Администратор процесса')[0]
        if group_process_administrator in request.user.groups.all():
            o_status = OfferRequest.STATUS.new

        offer_request = OfferRequest.objects.create(
            user=self.cart.user,
            client=self.cart.user.client,
            status=o_status,
        )
        for item in self.cart.items.all():
            OfferRequestItem.objects.create(
                offer_request=offer_request,
                offer=item.offer,
                color=item.color,
            )
        self.cart.delete()
        return Response(status=status.HTTP_200_OK)


class UserCartItemsView(CartMixin):
    def get(self, request, *args, **kwargs):
        return Response(data=AggreementCartItemSerializer(self.cart.items.all(), many=True).data)


class UserCartItemsAddView(CartMixin):
    def post(self, request, *args, **kwargs):
        offer_id = request.data.get('offer_id', None)
        color_id = request.data.get('color_id', None)
        errors = []
        if offer_id and color_id:
            try:
                offer = Offer.objects.get(id=offer_id)
            except Product.DoesNotExist:
                errors.append('Предложение не найдено')
            else:
                try:
                    color = ProductColor.objects.get(id=color_id)
                except ProductColor.DoesNotExist:
                    errors.append('Цвет не найден')
                else:
                    try:
                        cart_item = AggreementCartItem(cart=self.cart, offer=offer, color=color)
                        cart_item.save()
                    except IntegrityError:
                        errors.append('Предложение уже добавлено')
                    else:
                        return Response(status=status.HTTP_201_CREATED)
        else:
            errors.append('Не передан offer_id или color_id')

        return Response(data={"error": ', '.join(errors)}, status=status.HTTP_400_BAD_REQUEST)


class UserCartItemsDeleteView(CartMixin):
    def post(self, request, *args, **kwargs):
        item_id = request.data.get('item_id', None)
        if item_id:
            AggreementCartItem.objects.filter(cart=self.cart, id=item_id).delete()
        return Response(status=status.HTTP_200_OK)


class AggreementOfferCartItemsViewSet(AdditionalDataPaginatedViewMixin, CartMixin, ModelViewSet):
    model = AggreementCartItem
    serializer_class = AggreementCartItemSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    ordering_fields = (
        'id',
    )

    def get_queryset(self):
        return AggreementCartItem.objects.filter(cart=self.cart)

    def get_additional_data(self, queryset):
        quantity = 0
        amount_full = 0
        for i in queryset:
            quantity += i.get_quantity()
            amount_full += i.get_amount()
        return {
            'quantity': quantity,
            'amount_full': amount_full
        }

from decimal import Decimal

import django_filters
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.db.models.aggregates import Max
from django.utils import timezone
from rest_framework import filters, mixins, status
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from config.celery import app
from history.models import HistoryItem
from offers.api.serializers import UpdateFromZaraFormSerializer
from offers.parsers.zara import ZaraParser, ParserException
from payments.models import PaymentAccount, PaymentDocument, ExpenseItem
from service_1c.tasks import notify_1c_offers_change
from ..serializers import OfferSerializer
from ...models import Offer, OfferStatus

def filter_has_main_collection(queryset, value):
    if value:
        queryset = queryset.filter(main_collection_id__isnull=False)
    else:
        queryset = queryset.filter(main_collection_id__isnull=True)
    return queryset


class OfferFilterSet(django_filters.FilterSet):
    has_main_collection = django_filters.BooleanFilter(action=filter_has_main_collection)

    class Meta:
        model = Offer
        fields = [
            'id',
            'user',
            'status',
            'status__code',
            'to_aggreement',
            'main_collection',
            'has_main_collection',
        ]


class OfferListMixin(object):
    serializer_class = OfferSerializer
    filter_class = OfferFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)
    search_fields = ('name', '=id', '=product__id', '=product__article', 'main_collection__name', 'main_collection__parent__name')
    queryset = Offer.objects.order_by('-modified')
    ordering_fields = (
        'id',
        'name',
        'modified',
    )

    def dispatch(self, request, *args, **kwargs):
        self.is_search = True if request.GET.get('search', None) else False
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = self.queryset
        qs = qs.select_related(
            'user'
        )
        qs = qs.prefetch_related(
            'images'
        )
        return qs.for_user(self.request.user)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        if request.GET.get('to_work', None) == '2':
            queryset = queryset.to_work(request.user)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class OfferViewSet(OfferListMixin, ModelViewSet):

    def perform_create(self, serializer):
        data = {
            'user': self.request.user,
        }
        if not self.request.user.has_perm('auth.can_offers_blank_worker_create'):
            data['worker'] = self.request.user
        else:
            data['status'] = OfferStatus.objects.get(code='approving')
        serializer.save(**data)
        history_item = HistoryItem.objects.create(
            kind=HistoryItem.KIND.status_change,
            content_object=serializer.instance,
            body='Пользователь {} {} создал предложение №{} в статусе "{}"'.format(
                self.request.user.name,
                timezone.now().strftime('%d.%m.%Y %H:%M'),
                serializer.instance.id,
                serializer.instance.status.name
            ),
        )
        notify_1c_offers_change([serializer.instance.id, ])

    def perform_update(self, serializer):
        old_obj = self.get_object()
        new_data_dict = serializer.validated_data
        name_changed = old_obj.name != new_data_dict.get('name', None)
        super(OfferViewSet, self).perform_update(serializer)
        if name_changed:
            notify_1c_offers_change([serializer.instance.id, ])

    @detail_route(methods=['post', ])
    def update_from_zara(self, request, **kwargs):
        serializer = UpdateFromZaraFormSerializer(data=request.data)
        serializer.is_valid(True)
        offer = self.get_object()
        try:
            zp = ZaraParser(offer.id, serializer.validated_data['url'], request.user)
            zp.update_offer_data()
        except ParserException as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        return Response()

    @list_route()
    def summary(self, request):
        data = {}
        qs = Offer.objects.all().for_user(request.user)
        draft_qs = qs.draft_queue()
        data['draft_all'] = draft_qs.count()
        data['draft_to_work'] = draft_qs.to_work(request.user).count()
        approving_qs = qs.approving_queue()
        data['approving_all'] = approving_qs.count()
        data['approving_to_work'] = approving_qs.to_work(request.user).count()
        approved_qs = qs.approved()
        data['approved_all'] = approved_qs.count()
        data['approved_to_work'] = approved_qs.to_work(request.user).count()
        unread_comments_qs = qs.unread_comments()
        data['unread_comments'] = qs.unread_comments().count()
        data['unread_comments_to_work'] = qs.unread_comments(request.user).count()
        return Response(data)

    @detail_route(methods=['post', ])
    def change_status(self, request, **kwargs):
        offer = self.get_object()
        new_status_code = request.data.get('status_code', None)
        if new_status_code:
            try:
                offer_status = OfferStatus.objects.get(code=new_status_code)
            except ObjectDoesNotExist:
                return Response({'error': 'Не существующий статус'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                if offer_status.code in set(offer.status.next_status.all().values_list('code', flat=True)):
                    if offer_status.code == 'models_created' and not offer.product_set.all():
                        return Response({'error': 'Для предложения еще не создали модели'}, status=status.HTTP_400_BAD_REQUEST)
                    old_status_name = offer.status.name
                    offer.status = offer_status
                    offer.save()

                    history_item = HistoryItem.objects.create(
                        kind=HistoryItem.KIND.status_change,
                        content_object=offer,
                        body='Пользователь {} {} изменил статус предложения №{} с "{}" на "{}"'.format(
                            request.user.name,
                            timezone.now().strftime('%d.%m.%Y %H:%M'),
                            offer.id,
                            old_status_name,
                            offer_status.name
                        ),
                    )
                    return Response(status=status.HTTP_200_OK)
                else:
                    return Response({'error': 'Этот статус выставить нельзя'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'error': 'Не передан статус'}, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post', ])
    def change_prepayment(self, request, **kwargs):
        offer = self.get_object()
        payment_history_body = ""
        payment_delivery_history_body = ""
        try:
            cost = Decimal(request.data.get('prepayment_cost', 0))
        except Exception:
            cost = Decimal('0.0000000000')
        try:
            delivery_cost = Decimal(request.data.get('prepayment_delivery_cost', 0))
        except Exception:
            delivery_cost = Decimal('0.0000000000')

        payment_account_id = request.data.get('payment_account', None)
        try:
            payment_account = PaymentAccount.objects.get(id=payment_account_id)
        except PaymentAccount.DoesNotExist:
            return Response({'error': 'Не верный счет'}, status=status.HTTP_400_BAD_REQUEST)
        amount_chn = cost + delivery_cost
        if payment_account.balance < amount_chn:
            return Response({'error': 'Не достаточно средств на счете'}, status=status.HTTP_400_BAD_REQUEST)
        amount_rur = payment_account.get_amount_rur(amount_chn)

        if cost:
            payment_history_body += 'Предоплата за заказ образца: {} {}ю., {}р.'.format(
                timezone.now().strftime('%d.%m.%Y'),
                cost.quantize(Decimal('0.00')),
                payment_account.get_amount_rur(cost).quantize(Decimal('0.00'))
            )
        if delivery_cost:
            payment_delivery_history_body += 'Предоплата за доставку образца: {} {}ю., {}р.'.format(
                timezone.now().strftime('%d.%m.%Y'),
                delivery_cost.quantize(Decimal('0.00')),
                payment_account.get_amount_rur(delivery_cost).quantize(Decimal('0.00'))
            )

        offer.prepayment_delivery_cost = delivery_cost
        offer.prepayment_cost = cost
        offer.payment_account = payment_account
        offer.cost_rur += amount_rur
        offer.save()

        expense_item = ExpenseItem.get_offer_prepayment()

        payment_document = PaymentDocument(
            kind=PaymentDocument.KIND.debit,
            payment_account=payment_account,
            destination_payment_account=payment_account,
            amount=amount_chn,
            amount_rur=amount_rur,
            user=self.request.user,
            description="Аванс за предложение №{}".format(offer.id),
            content_object=offer,
            expense_item=expense_item,
        )
        payment_document.save()
        payment_document.process()

        if payment_history_body:
            history_item = HistoryItem.objects.create(
                kind=HistoryItem.KIND.payments,
                content_object=offer,
                body=payment_history_body,
            )
        if payment_delivery_history_body:
            history_item = HistoryItem.objects.create(
                kind=HistoryItem.KIND.payments,
                content_object=offer,
                body=payment_delivery_history_body,
            )

        return Response(status=status.HTTP_200_OK)

    @detail_route(methods=['post', ])
    def change_cost(self, request, **kwargs):
        payment_history_body = ""
        payment_delivery_history_body = ""
        offer = self.get_object()
        try:
            cost = Decimal(request.data.get('cost', 0))
        except Exception:
            cost = Decimal('0.0000000000')
        try:
            delivery_cost = Decimal(request.data.get('delivery_cost', 0))
        except Exception:
            delivery_cost = Decimal('0.0000000000')
        payment_account_id = request.data.get('payment_account', None)
        try:
            payment_account = PaymentAccount.objects.get(id=payment_account_id)
        except PaymentAccount.DoesNotExist:
            return Response({'error': 'Не верный счет'}, status=status.HTTP_400_BAD_REQUEST)
        amount_chn = cost + delivery_cost
        if payment_account.balance < amount_chn:
            return Response({'error': 'Не достаточно средств на счете'}, status=status.HTTP_400_BAD_REQUEST)
        amount_rur = payment_account.get_amount_rur(amount_chn)

        if cost:
            payment_history_body += 'Оплата за заказ образца: {} {}ю., {}р.'.format(
                timezone.now().strftime('%d.%m.%Y'),
                cost.quantize(Decimal('0.00')),
                payment_account.get_amount_rur(cost).quantize(Decimal('0.00'))
            )
        if delivery_cost:
            payment_delivery_history_body += 'Оплата за доставку образца: {} {}ю., {}р.'.format(
                timezone.now().strftime('%d.%m.%Y'),
                delivery_cost.quantize(Decimal('0.00')),
                payment_account.get_amount_rur(delivery_cost).quantize(Decimal('0.00'))
            )

        offer.delivery_cost = delivery_cost
        offer.cost = cost
        offer.payment_account = payment_account
        offer.cost_rur += amount_rur
        offer.save()

        expense_item = ExpenseItem.get_offer_payment()

        payment_document = PaymentDocument(
            kind=PaymentDocument.KIND.debit,
            payment_account=payment_account,
            destination_payment_account=payment_account,
            amount=amount_chn,
            amount_rur=amount_rur,
            user=self.request.user,
            description="Оплата за предложение №{}".format(offer.id),
            content_object=offer,
            expense_item=expense_item,
        )
        payment_document.save()
        payment_document.process()

        if payment_history_body:
            history_item = HistoryItem.objects.create(
                kind=HistoryItem.KIND.payments,
                content_object=offer,
                body=payment_history_body,
            )
        if payment_delivery_history_body:
            history_item = HistoryItem.objects.create(
                kind=HistoryItem.KIND.payments,
                content_object=offer,
                body=payment_delivery_history_body,
            )

        return Response(status=status.HTTP_200_OK)

    @detail_route(methods=['post', ])
    def return_cost(self, request, **kwargs):
        offer = self.get_object()
        payment_account_id = request.data.get('payment_account', None)
        if offer.prepayment_cost or offer.cost:
            try:
                payment_account = PaymentAccount.objects.get(id=payment_account_id)
            except PaymentAccount.DoesNotExist:
                return Response({'error': 'Не верный счет'}, status=status.HTTP_400_BAD_REQUEST)
            amount_chn = Decimal('0.0000000000')
            amount_rur = Decimal('0.0000000000')
            amount_chn += Decimal(offer.cost)
            amount_chn += Decimal(offer.prepayment_cost)
            full_amount = offer.cost + offer.prepayment_cost

            if full_amount:
                one_chn_cost = offer.cost_rur / full_amount
                amount_rur += one_chn_cost * (offer.cost + offer.prepayment_cost)
            offer.cost = 0
            offer.prepayment_cost = 0
            offer.cost_rur -= amount_rur
            offer.save()

            expense_item = ExpenseItem.get_offer_return()

            payment_document = PaymentDocument(
                kind=PaymentDocument.KIND.credit,
                payment_account=payment_account,
                destination_payment_account=payment_account,
                amount=amount_chn,
                amount_rur=amount_rur,
                user=self.request.user,
                description="Возврат предложения №{}".format(offer.id),
                content_object=offer,
                expense_item=expense_item,
            )
            payment_document.save()
            payment_document.process()

            history_item = HistoryItem.objects.create(
                kind=HistoryItem.KIND.payments,
                content_object=offer,
                body="Возврат образца {} {} ю. {} руб.".format(
                    timezone.now().strftime('%d.%m.%Y'),
                    amount_chn.quantize(Decimal('0.00')),
                    amount_rur.quantize(Decimal('0.00')),
                ),
            )

        return Response(status=status.HTTP_200_OK)

    @detail_route(methods=['post', ])
    def set_worker(self, request, **kwargs):
        offer = self.get_object()
        if offer.worker:
            return Response({'error': 'Предложение уже назначено на другого пользователя'}, status=status.HTTP_400_BAD_REQUEST)
        current_user = request.user
        # TODO: Когда-нибудь пригодится, сейчас не актуально
        # worker_id = request.data.get('worker_id', None)
        # if worker_id:
        #     if current_user.has_perm('auth.can_set_offers_worker'):
        #         try:
        #             worker = User.objects.get(id=worker_id)
        #         except User.DoesNotExist:
        #             return Response({'error': 'Пользователь не найден'}, status=status.HTTP_400_BAD_REQUEST)
        #         else:
        #             offer.worker = worker
        #             offer.save()
        #             return Response(status=status.HTTP_200_OK)
        #     else:
        #         return Response({'error': 'Не достаточно прав'}, status=status.HTTP_400_BAD_REQUEST)
        # else:
        if current_user.has_perm('auth.can_get_offers_blank_worker'):
            offer.worker = current_user
            offer.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Не достаточно прав'}, status=status.HTTP_400_BAD_REQUEST)


class OfferArchiveViewSet(OfferListMixin,
                          mixins.RetrieveModelMixin,
                          mixins.ListModelMixin,
                          GenericViewSet):

    def get_queryset(self):
        qs = super(OfferArchiveViewSet, self).get_queryset()
        if not self.is_search:
            qs = qs.archive().order_by('-id')
        return qs


class OfferDraftViewSet(OfferListMixin,
                        mixins.RetrieveModelMixin,
                        mixins.ListModelMixin,
                        GenericViewSet):

    def get_queryset(self):
        qs = super(OfferDraftViewSet, self).get_queryset()
        if not self.is_search:
            qs = qs.draft_queue().order_by('-id')
        return qs


class OfferApprovingViewSet(OfferListMixin,
                            mixins.RetrieveModelMixin,
                            mixins.ListModelMixin,
                            GenericViewSet):

    def get_queryset(self):
        qs = super(OfferApprovingViewSet, self).get_queryset()
        if not self.is_search:
            qs = qs.approving_queue().order_by('-id')
        return qs


class OfferApprovedViewSet(OfferListMixin,
                           mixins.RetrieveModelMixin,
                           mixins.ListModelMixin,
                           GenericViewSet):

    def get_queryset(self):
        qs = super(OfferApprovedViewSet, self).get_queryset()
        if not self.is_search:
            qs = qs.approved().order_by('-id')
        return qs


class OfferUnreadCommentsViewSet(OfferListMixin,
                                 mixins.RetrieveModelMixin,
                                 mixins.ListModelMixin,
                                 GenericViewSet):

    def get_queryset(self):
        qs = super(OfferUnreadCommentsViewSet, self).get_queryset()
        if not self.is_search:
            qs = qs.unread_comments(self.request.user)
            qs = qs.annotate(max_comments_created=Max('comments__created'))
            qs = qs.order_by('-max_comments_created')
        return qs


class OfferDeletedViewSet(OfferListMixin,
                          mixins.ListModelMixin,
                          mixins.RetrieveModelMixin,
                          GenericViewSet):
    queryset = Offer.deleted_objects.order_by('-modified')

    @detail_route(methods=['post', ])
    def restore(self, request, **kwargs):
        product = self.get_object()
        product.is_deleted = False
        product.save()
        return Response(status=status.HTTP_200_OK)

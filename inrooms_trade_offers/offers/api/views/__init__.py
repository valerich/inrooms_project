from django.db.models.query_utils import Q
from rest_framework import filters
from rest_framework.exceptions import ParseError
from rest_framework.generics import ListAPIView, RetrieveAPIView

from .aggreement_offer import AggreementOfferViewSet, AggreementOfferEditViewSet
from .cart import (
    UserCartItemsAddView,
    UserCartItemsDeleteView,
    UserCartItemsView,
    UserCartView,
    UserCartCreateOrderView,
    AggreementOfferCartItemsViewSet,
)
from .image import (
    OfferImageListView,
    OfferImageDetailView,
    OfferImageUploadView,
    OfferImageOrderView,
)
from .offers import (
    OfferArchiveViewSet,
    OfferApprovedViewSet,
    OfferApprovingViewSet,
    OfferDeletedViewSet,
    OfferDraftViewSet,
    OfferUnreadCommentsViewSet,
    OfferViewSet,
)
from ..serializers import (
    OfferStatusSerializer,
)
from ...models import (
    Offer,
    OfferStatus,
)


class OfferMixin(object):
    def dispatch(self, request, *args, **kwargs):
        try:
            self.offer = Offer.objects.get(pk=self.kwargs.get('pk'))
        except Offer.DoesNotExist:
            raise ParseError('wrong offer id')
        return super(OfferMixin, self).dispatch(request, *args, **kwargs)


class OfferStatusDetail(OfferMixin, RetrieveAPIView):
    serializer_class = OfferStatusSerializer
    queryset = OfferStatus.objects.all()


class ProductStatusNextListView(OfferMixin, ListAPIView):
    serializer_class = OfferStatusSerializer
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )

    def get_queryset(self):
        status_ids = self.offer.status.next_status.all().values_list('id', flat=True)
        return OfferStatus.objects.filter(Q(id=self.offer.status_id) | Q(id__in=status_ids))


class StatusListView(ListAPIView):
    serializer_class = OfferStatusSerializer
    queryset = OfferStatus.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )

from rest_framework import filters
from rest_framework.exceptions import ParseError
from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from .. import serializers
from ...models import Offer, OfferImage


class OfferMixin(object):
    def dispatch(self, request, *args, **kwargs):
        try:
            self.offer = Offer.objects.get(pk=self.kwargs.get('pk'))
        except Offer.DoesNotExist:
            raise ParseError('wrong offer id')
        return super(OfferMixin, self).dispatch(request, *args, **kwargs)


class OfferImageListView(OfferMixin, ListAPIView):
    serializer_class = serializers.OfferImageSerializer
    filter_backends = (filters.SearchFilter,)

    def get_queryset(self):
        return self.offer.images.all()


class OfferImageDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.OfferImageSerializer
    queryset = OfferImage.objects.all()


class OfferImageUploadView(OfferMixin, APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request, **kwargs):
        image = request.FILES['image']

        OfferImage.objects.create(
            offer=self.offer,
            image=image,
        )
        return Response({
            'status': 'ok'
        })


class OfferImageOrderView(APIView):
    def post(self, request, **kwargs):
        for item in request.data:
            OfferImage.objects.filter(
                id=item.get('id')
            ).update(
                visual_order=item.get('visual_order')
            )
        return Response({})

from django.conf.urls import include, url
from rest_framework import routers

from . import views

router = routers.SimpleRouter()

router.register(r'offer', views.OfferViewSet, base_name='offer')
router.register(r'aggreement-offer', views.AggreementOfferViewSet, base_name='aggreement-offer')
router.register(r'aggreement-offer-edit', views.AggreementOfferEditViewSet, base_name='aggreement-offer-edit')
router.register(r'offer-archive', views.OfferArchiveViewSet, base_name='offer_archive')
router.register(r'offer-approving', views.OfferApprovingViewSet, base_name='offer_approving')
router.register(r'offer-approved', views.OfferApprovedViewSet, base_name='offer_approved')
router.register(r'offer-draft', views.OfferDraftViewSet, base_name='offer_draft')
router.register(r'offer-deleted', views.OfferDeletedViewSet, base_name='offer_deleted')
router.register(r'offer-unread-comments', views.OfferUnreadCommentsViewSet, base_name='offer_unread_comments')

router2 = routers.SimpleRouter()
router2.register(r'items', views.AggreementOfferCartItemsViewSet, base_name='user-cart')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^offer/(?P<pk>\d+)/image/$', views.OfferImageListView.as_view(), name='image-list'),
    url(r'^offer/(?P<pk>\d+)/image-upload/$', views.OfferImageUploadView.as_view(), name='image-upload'),
    url(r'^status/$', views.StatusListView.as_view(), name='status-list'),
    url(r'^image/(?P<pk>\d+)/$', views.OfferImageDetailView.as_view(), name='image-detail'),
    url(r'^image-order/$', views.OfferImageOrderView.as_view(), name='image-order'),
    url(r'^aggreement-offer-cart/items/add/$', views.UserCartItemsAddView.as_view(), name='user-cart-items-add'),
    url(r'^aggreement-offer-cart/items/delete/$', views.UserCartItemsDeleteView.as_view(), name='user-cart-items-delete'),
    url(r'^aggreement-offer-cart/$', views.UserCartView.as_view(), name='user-cart'),
    url(r'^aggreement-offer-cart/create_order/$', views.UserCartCreateOrderView.as_view(), name='user-cart-create-order'),
    url(r'^aggreement-offer-cart/', include(router2.urls)),
    url(r'^aggreement-offer-cart/items/$', views.UserCartItemsView.as_view(), name='user-cart-items'),
]

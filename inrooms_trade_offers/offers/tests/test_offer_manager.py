from django.test import TestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from offers.factories import OfferFactory
from offers.models import Offer, OfferStatus


class OfferManagerTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(OfferManagerTestCase, cls).setUpClass()

        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_bm2 = User.objects.get(email='brandmanager2@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')
        for status in OfferStatus.objects.all():
            OfferFactory(user=cls.user_bm, status=status)
            OfferFactory(user=cls.user_pa, status=status)
            OfferFactory(user=cls.user_bm, status=status, worker=cls.user_bm)
            OfferFactory(user=cls.user_pa, status=status, worker=cls.user_bm2)

    def test_for_user(self):
        # Если у пользователя нет права видеть все образцы, то он видит только свои
        self.assertEqual(Offer.objects.all().for_user(self.user_bm).count(), 60)
        # Иначе видит все товары
        self.assertEqual(Offer.objects.all().for_user(self.user_pa).count(), 60)

    def test_archive(self):
        # Без указания пользователя результат - все архивные товары
        self.assertEqual(Offer.objects.all().archive().count(), 4)
        # Если у пользователя нет права видеть все товары, то он видит только свои
        self.assertEqual(Offer.objects.all().for_user(self.user_bm).archive().count(), 4)
        # Иначе видит все товары
        self.assertEqual(Offer.objects.all().for_user(self.user_pa).archive().count(), 4)

    def test_approving_queue(self):
        """Очередь товаров - утверждение"""
        self.assertEqual(Offer.objects.all().approving_queue().count(), 36)
        self.assertEqual(Offer.objects.all().for_user(self.user_bm).approving_queue().count(), 36)
        # Иначе видит все товары
        self.assertEqual(Offer.objects.all().for_user(self.user_pa).approving_queue().count(), 36)

    def test_draft_queue(self):
        """Очередь товаров - предложения"""
        self.assertEqual(Offer.objects.all().draft_queue().count(), 12)
        self.assertEqual(Offer.objects.all().for_user(self.user_bm).draft_queue().count(), 12)
        self.assertEqual(Offer.objects.all().for_user(self.user_pa).draft_queue().count(), 12)

    def test_approved(self):
        """Утвержденные предложения"""
        self.assertEqual(Offer.objects.all().approved().count(), 8)
        self.assertEqual(Offer.objects.all().for_user(self.user_bm).approved().count(), 8)
        self.assertEqual(Offer.objects.all().for_user(self.user_pa).approved().count(), 8)

    def test_delete(self):
        """ Удаление товара
        """

        self.assertEqual(Offer.objects.all().count(), 60)
        self.assertEqual(Offer.deleted_objects.all().count(), 0)
        product = Offer.objects.all()[0]
        product.delete()
        self.assertEqual(Offer.objects.all().count(), 59)
        self.assertEqual(Offer.deleted_objects.all().count(), 1)
        product2 = Offer.deleted_objects.all()[0]
        self.assertEqual(product.id, product2.id)

    def test_to_work(self):
        """Возвращает список товаров для работы

        Сейчас это все товары, для статусов которых есть право 'auth.can_products_edit_{code статуса}'
        """
        # Все заказы
        self.assertEqual(Offer.objects.all().to_work(self.user_bm).count(), 32)
        self.assertEqual(Offer.objects.all().to_work(self.user_pa).count(), 32)
        # Заказы, которые видит пользователь
        self.assertEqual(Offer.objects.all().for_user(self.user_bm).to_work(self.user_bm).count(), 32)
        self.assertEqual(Offer.objects.all().for_user(self.user_pa).to_work(self.user_pa).count(), 32)

from django.test import TestCase

from accounts.tests.setup import UsersSetup
from ..factories import OfferFactory
from ..models import OfferStatus


class OfferTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(OfferTestCase, cls).setUpClass()

        UsersSetup()

    def test_status_changed(self):
        """Offer.tracker отслеживает изменение поля status"""

        status = OfferStatus.objects.get(code='draft')
        next_status = OfferStatus.objects.get(code='approved')

        p = OfferFactory(status=status)
        self.assertEqual(p.tracker.has_changed('status'), False)
        p.status = next_status
        self.assertEqual(p.tracker.has_changed('status'), True)
        p.save()

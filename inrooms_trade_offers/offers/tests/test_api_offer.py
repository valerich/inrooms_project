from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from offers.factories import OfferFactory
from offers.models import Offer, OfferStatus


class OfferViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(OfferViewTestCase, cls).setUpClass()
        cls.offer_url = reverse('api:offers:offer-list')
        cls.summary_url = reverse('api:offers:offer-summary')
        cls.offer_draft_url = reverse('api:offers:offer_draft-list')
        cls.offer_approving_url = reverse('api:offers:offer_approving-list')
        cls.offer_approved_url = reverse('api:offers:offer_approved-list')
        cls.offer_archive_url = reverse('api:offers:offer_archive-list')
        cls.offer_deleted_url = reverse('api:offers:offer_deleted-list')
        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_bm2 = User.objects.get(email='brandmanager2@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')
        for status in OfferStatus.objects.all():
            OfferFactory(user=cls.user_bm, status=status)
            OfferFactory(user=cls.user_pa, status=status)
            OfferFactory(user=cls.user_bm, status=status, worker=cls.user_bm)
            OfferFactory(user=cls.user_pa, status=status, worker=cls.user_bm2)

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')
        pass

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_authentication_fail(self):
        self.client.force_authenticate(user=None)
        response = self.client.post(self.offer_url)
        self.assertIn(response.status_code, [status.HTTP_403_FORBIDDEN, status.HTTP_302_FOUND])

    def test_list_offer(self):
        response = self.client_bm.get(self.offer_url)
        self.assertEqual(response.data['count'], 60)
        response = self.client_pa.get(self.offer_url)
        self.assertEqual(response.data['count'], 60)

    def test_list_draft(self):
        response = self.client_bm.get(self.offer_draft_url)
        self.assertEqual(response.data['count'], 12)
        response = self.client_pa.get(self.offer_draft_url)
        self.assertEqual(response.data['count'], 12)

    def test_list_approving(self):
        response = self.client_bm.get(self.offer_approving_url)
        self.assertEqual(response.data['count'], 36)
        response = self.client_pa.get(self.offer_approving_url)
        self.assertEqual(response.data['count'], 36)

    def test_list_approved(self):
        response = self.client_bm.get(self.offer_approved_url)
        self.assertEqual(response.data['count'], 8)
        response = self.client_pa.get(self.offer_approved_url)
        self.assertEqual(response.data['count'], 8)

    def test_list_archive(self):
        response = self.client_bm.get(self.offer_archive_url)
        self.assertEqual(response.data['count'], 4)
        response = self.client_pa.get(self.offer_archive_url)
        self.assertEqual(response.data['count'], 4)

    def test_summary(self):
        """Данные для виджетов - сводная информация о товарах"""
        response = self.client_bm.get(self.summary_url)
        self.assertDictEqual(response.data, {"draft_to_work": 8,
                                             "draft_all": 12,
                                             "approving_to_work": 20,
                                             "approving_all": 36,
                                             "approved_to_work": 4,
                                             "approved_all": 8,
                                             "unread_comments": 0,
                                             "unread_comments_to_work": 0})
        response = self.client_pa.get(self.summary_url)
        self.assertEqual(response.data, {"draft_to_work": 4,
                                         "draft_all": 12,
                                         "approving_to_work": 20,
                                         "approving_all": 36,
                                         "approved_to_work": 4,
                                         "approved_all": 8,
                                         "unread_comments": 0,
                                         "unread_comments_to_work": 0})

    def test_create_offer(self):
        response = self.client_bm.post(self.offer_url, {})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client_pa.post(self.offer_url, {})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_blank_request_data(self):
        response = self.client_bm.post(self.offer_url)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_list_deleted(self):
        response = self.client_bm.get(self.offer_deleted_url)
        self.assertEqual(response.data['count'], 0)
        response = self.client_pa.get(self.offer_deleted_url)
        self.assertEqual(response.data['count'], 0)
        Offer.objects.filter(user=self.user_bm)[0].delete()
        response = self.client_bm.get(self.offer_deleted_url)
        self.assertEqual(response.data['count'], 1)
        response = self.client_pa.get(self.offer_deleted_url)
        self.assertEqual(response.data['count'], 1)

    def test_worker_bm(self):
        """Если предложение создает БМ, то оно закрепляется за ним и статус - "Черновик"

        issue #31
        """
        response = self.client_bm.post(self.offer_url, {})
        offer = Offer.objects.get(id=response.data['id'])
        self.assertEqual(offer.worker, self.user_bm)
        self.assertEqual(offer.status.code, 'draft')

    def test_worker_pa(self):
        """Если предложение создает ПА, то поле worker не заполняется и статус у заказа "Утверждение"

        В дальнейшем такие предложения смогут закрепить за собой БМ
        issue #31
        """
        response = self.client_pa.post(self.offer_url, {})
        offer = Offer.objects.get(id=response.data['id'])
        self.assertIsNone(offer.worker)
        self.assertEqual(offer.status.code, 'approving')

    def test_set_worker_exist(self):
        """Назначение предложения на исполнителя

        Предложение уже назначено на другого пользователя
        issue #31
        """

        offer = OfferFactory(user=self.user_bm, worker=self.user_bm)
        set_worker_url = reverse('api:offers:offer-set-worker', kwargs={'pk': offer.id})
        response = self.client_pa.post(set_worker_url, {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['error'], 'Предложение уже назначено на другого пользователя')

        offer = OfferFactory(user=self.user_pa, worker=self.user_pa)
        set_worker_url = reverse('api:offers:offer-set-worker', kwargs={'pk': offer.id})
        response = self.client_bm.post(set_worker_url, {})
        # У БМ нет права обращаться к заказам других пользователей
        # TODO: Раньше тут возвращалось 404, а когда начал фиксить тесты, то уже 400.
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_set_worker_to_user(self):
        """Назначение предложения на себя

        Предложение не назначено ни на кого. Попытка назначить на себя
        issue #31
        """

        offer = OfferFactory(user=self.user_bm, worker=None)
        set_worker_url = reverse('api:offers:offer-set-worker', kwargs={'pk': offer.id})
        response = self.client_pa.post(set_worker_url, {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['error'], 'Не достаточно прав')

        offer = OfferFactory(user=self.user_pa, worker=None)
        set_worker_url = reverse('api:offers:offer-set-worker', kwargs={'pk': offer.id})
        response = self.client_bm.post(set_worker_url, {})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        offer = Offer.objects.get(id=offer.id)
        self.assertEqual(offer.worker, self.user_bm)

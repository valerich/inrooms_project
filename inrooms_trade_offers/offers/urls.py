from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^aggreement_offer/export/$', views.AggreementOfferExport.as_view(), name='aggreement-offer-export'),
]

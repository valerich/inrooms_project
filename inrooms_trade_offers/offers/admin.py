from django.contrib import admin
from sorl.thumbnail.shortcuts import get_thumbnail

from .forms import OfferAdminForm, OfferImageAdminForm, OfferStatusForm
from .models import Offer, OfferImage, OfferStatus


class OfferAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'status', 'user', 'worker', 'is_deleted')
    list_filter = ('is_deleted', )
    search_fields = ('name', '=id', )
    form = OfferAdminForm

    def get_thumbnail_html(self, obj):
        img = obj.main_im
        img_resize_url = None

        if img is not None and img.name:
            try:
                img_resize_url = str(get_thumbnail(img, '100x100').url)
            except IOError:
                pass

        if not img:
            return '<a class="image-picker" href=""><img src="" alt=""/></a>'
        html = '''<a class="image-picker" href="%s"><img src="%s"
                alt="%s"/></a>''' % (img.url, img_resize_url, obj.name)
        return html

    get_thumbnail_html.short_description = 'Миниатюра'
    get_thumbnail_html.allow_tags = True

    def get_queryset(self, request):
        qs = Offer.all_objects.get_queryset()
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs.prefetch_related('images')


class OfferImageAdmin(admin.ModelAdmin):
    form = OfferImageAdminForm


class OfferStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name', 'weight')
    search_fields = ('name', 'slug',)
    form = OfferStatusForm


for model_or_iterable, admin_class in (
    (Offer, OfferAdmin),
    (OfferImage, OfferImageAdmin),
    (OfferStatus, OfferStatusAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

import requests
import tempfile

from django.core import files

from bot.utils import send_telegram_message
from care.models import CareItem
from consistency.models import ConsistencyItem
from offers.models import Offer, OfferImage


class ParserException(Exception):
    pass


class ZaraParser():

    def __init__(self, offer_id, url, user=None):
        self.offer_id = offer_id
        self.source_url = url
        self.user = user

    def get_offer(self):
        try:
            self.offer = Offer.objects.get(id=self.offer_id)
        except Offer.DoesNotExist as e:
            raise ParserException(str(e))
        else:
            return self.offer

    def get_source_data(self):
        try:
            r = requests.get(self.source_url, params={'ajax': 'true'})
            return r.json()
        except Exception as e:
            raise ParserException(str(e))

    def get_offer_data(self):
        source_data = self.get_source_data()

        offer_name = source_data['product']['name']
        offer_article = source_data['product']['detail']['displayReference']

        offer_images = []
        for image_data in source_data['product']['xmedia']:
            if image_data['type'] == 'image':
                offer_images.append(
                    'https://static.zara.net/photos{}/{}.jpg'.format(
                        image_data['path'],
                        image_data['name']
                    )
                )

        offer_composition = []
        for part in source_data['product']['detail'].get('detailedComposition', {}).get('parts', []):
            for area in part.get('areas', []):
                for component in area.get('components', []):
                    offer_composition.append({
                        'name': component['material'],
                        'percentage': component['percentage'].replace('%', ''),
                    })
            for component in part.get('components', []):
                offer_composition.append({
                    'name': component['material'],
                    'percentage': component['percentage'].replace('%', ''),
                })

        offer_care = []
        for care in source_data['product']['detail'].get('care', []):
            offer_care.append({
                'code': care['name'],
                'description': care['description']
            })

        return {
            'name': offer_name,
            'article': offer_article,
            'images': offer_images,
            'composition': offer_composition,
            'care': offer_care,
        }

    def update_offer_data(self):
        errors = []
        try:
            offer_data = self.get_offer_data()
            offer = self.get_offer()

            offer.aggreement_code = offer_data['article']
            offer.business_name = offer_data['name']

            for i in range(1, 6):
                setattr(offer, 'consistency_type{}'.format(i), None)
                setattr(offer, 'consistency_value{}'.format(i), 0)

            if len(offer_data['composition']) > 5:
                errors.append('Больше 5-ти значений в составе')

            i = 1
            for composition_data in offer_data['composition'][:5]:
                consistency_type = ConsistencyItem.objects.filter(zara_code=composition_data['name']).first()
                if consistency_type is None:
                    errors.append('Не найден состав с кодом "{}"'.format(composition_data['name']))
                else:
                    if composition_data['percentage'].isdigit():
                        setattr(offer, 'consistency_type{}'.format(i), consistency_type)
                        setattr(offer, 'consistency_value{}'.format(i), int(composition_data['percentage']))
                    else:
                        errors.append('Cостав "{}" не является числом ({})'.format(composition_data['name'], composition_data['percentage']))
                i += 1


            offer.wash = None
            offer.bleach = None
            offer.drying = None
            offer.ironing = None
            offer.professional_care = None

            care_mapper = {v: k for k,v in CareItem.KIND._identifier_map.items()}

            for care_data in offer_data['care']:
                care_item = CareItem.objects.filter(zara_code=care_data['code']).first()
                if care_item is None:
                    errors.append('Не найден уход с кодом "{}" и описанием "{}"'.format(care_data['code'], care_data["description"]))
                else:
                    setattr(offer, care_mapper[care_item.kind], care_item)

            offer.save()

            if errors:
                message = """{} для предложения {} с сайта zara (url = {}). Ошибки: \n{}""".format(
                    "{} пытался загрузать данные".format(self.user) if self.user else 'Была попытка загрузки данных',
                    self.offer_id,
                    self.source_url,
                    '\n'.join(errors)
                )
                send_telegram_message(message=message)

            offer.images.all().delete()

            i = 0
            for image_url in offer_data['images']:
                request = requests.get(image_url, stream=True)

                if request.status_code != requests.codes.ok:
                    continue

                file_name = image_url.split('/')[-1]

                lf = tempfile.NamedTemporaryFile()

                for block in request.iter_content(1024 * 8):
                    if not block:
                        break
                    lf.write(block)

                offer_image = OfferImage(
                    offer=offer,
                    visual_order=i
                )
                offer_image.image.save(file_name, files.File(lf))
                offer_image.save()
                i += 1
                if i >= 4:
                    break
        except Exception as e:
            raise ParserException(str(e))

        return offer
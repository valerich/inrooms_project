from autocomplete_light import shortcuts as autocomplete_light

from .models import Offer, OfferStatus


class OfferAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['=id', 'name', ]
    attrs = {
        'placeholder': u'=id, Название',
        'data-autocomplete-minimum-characters': 0,
    }


class OfferStatusAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', 'code']
    attrs = {
        'placeholder': u'Название',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (Offer, OfferAutocomplete),
    (OfferStatus, OfferStatusAutocomplete),
)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

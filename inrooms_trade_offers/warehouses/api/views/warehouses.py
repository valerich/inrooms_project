import django_filters
from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import WarehouseSerializer
from ...models import Warehouse


class WarehouseFilterSet(django_filters.FilterSet):

    class Meta:
        model = Warehouse
        fields = [
            'to_orders',
            'to_supply',
            'to_purchase',
            'is_active',
        ]


class WarehouseViewSet(mixins.RetrieveModelMixin,
                       mixins.UpdateModelMixin,
                       mixins.ListModelMixin,
                       GenericViewSet):
    serializer_class = WarehouseSerializer
    filter_class = WarehouseFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, )
    search_fields = ('name', '=id', '=code')
    queryset = Warehouse.objects.all()
    ordering_fields = (
        'id',
        'name',
        'code',
        'modified',
    )

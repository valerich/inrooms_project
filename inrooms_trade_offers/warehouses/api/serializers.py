from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ..models import Warehouse


class WarehouseSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = Warehouse
        fields = [
            'id',
            'name',
            'code',
            'created',
            'modified',
            'is_active',
        ]

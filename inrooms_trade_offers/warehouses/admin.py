from django.contrib import admin

from .forms import WarehouseAdminForm
from .models import Warehouse


class WarehouseAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'code',
        'accept_1c_remains',
        'to_orders',
        'to_supply',
        'to_purchase',
    )
    list_filter = (
        'accept_1c_remains',
        'to_orders',
        'to_supply',
        'to_purchase',
    )
    search_fields = ('name', '=id', '=code')
    form = WarehouseAdminForm


for model_or_iterable, admin_class in (
    (Warehouse, WarehouseAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

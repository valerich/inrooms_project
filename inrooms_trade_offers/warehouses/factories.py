import factory
import factory.django
import factory.fuzzy

from .models import Warehouse


class WarehouseFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    code = factory.fuzzy.FuzzyText()

    class Meta:
        model = Warehouse

from django.test import TestCase

from ..factories import WarehouseFactory
from ..models import Warehouse


class WarehouseFactoryTestCase(TestCase):
    def setUp(self):
        Warehouse.objects.all().delete()

    def test_factory_factory(self):
        warehouse = WarehouseFactory(name='Склад 1')
        warehouse_from_db = Warehouse.objects.get(name='Склад 1')
        self.assertEqual(warehouse, warehouse_from_db)

    def test_multiple_factories_factory(self):
        factories = []
        for i in range(10):
            factories.append(WarehouseFactory())

        self.assertEqual(len(factories), 10)
        self.assertEqual(len(factories), len(Warehouse.objects.all()))

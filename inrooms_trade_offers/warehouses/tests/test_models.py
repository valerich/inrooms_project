from django.db.utils import IntegrityError
from django.test import TestCase

from ..models import Warehouse


class WarehouseModelsTestCase(TestCase):
    def setUp(self):
        Warehouse.objects.all().delete()

    def test_factory_name_unique(self):
        """Поле "Название" у складов уникально"""

        Warehouse.objects.create(name='Склад 1')
        with self.assertRaises(IntegrityError):
            Warehouse.objects.create(name='Склад 1')

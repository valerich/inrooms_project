from django.core.urlresolvers import reverse
from freezegun.api import freeze_time
from rest_framework.test import APIClient, APITestCase

from accounts.tests.setup import UsersSetup
from ..factories import WarehouseFactory
from ..models import Warehouse


class WarehouseViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.list_url = reverse('api:warehouses:warehouse-list')
        UsersSetup()
        cls.client_admin = APIClient()
        cls.client_admin.login(email='admin@example.com', password='password')

    def test_list(self):
        response = self.client_admin.get(self.list_url)
        self.assertEqual(response.data['count'], 3)

    @freeze_time("2016-1-31")
    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        Warehouse.objects.all().delete()
        warehouse = WarehouseFactory(
            name="test_name",
            code="tst",

        )
        response = self.client_admin.get(self.list_url)
        object_dict = response.data['results'][0]
        valid_object_dict = {
            "id": warehouse.id,
            "name": "test_name",
            "code": "tst",
            "created": "2016-01-31T00:00:00Z",
            "modified": "2016-01-31T00:00:00Z"
        }
        self.assertDictEqual(object_dict, valid_object_dict)

    def test_list_filter_to_orders(self):
        response = self.client_admin.get(self.list_url, data={'to_orders': 2})
        self.assertEqual(response.data['count'], 2)

        response = self.client_admin.get(self.list_url, data={'to_orders': 3})
        self.assertEqual(response.data['count'], 1)

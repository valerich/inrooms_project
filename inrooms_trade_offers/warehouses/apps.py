from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'warehouses'
    verbose_name = 'Склады'

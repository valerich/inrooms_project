from django.db import models

from model_utils.models import TimeStampedModel


class Warehouse(TimeStampedModel):
    is_active = models.BooleanField('Активен?', default=True)
    name = models.CharField('Название', max_length=100, unique=True)
    code = models.CharField('Код', max_length=100, unique=True)
    client = models.ForeignKey('clients.Client', verbose_name='Партнер', blank=True, null=True)
    brand = models.ForeignKey('brands.Brand', verbose_name='Бренд', blank=True, null=True)
    accept_1c_remains = models.BooleanField('Принимать остатки от 1с?', default=True)
    to_orders = models.BooleanField('Используется для создания отгрузок клиентам', default=False)
    to_supply = models.BooleanField('Используется для поставок из Китая', default=False)
    to_purchase = models.BooleanField('Используется для закупок', default=False)
    take_cost_from_1c = models.BooleanField('Получать стоимость товаров от 1с в выгрузке остатков', default=False)

    class Meta:
        verbose_name = 'Склад'
        verbose_name_plural = 'Склады'
        ordering = ('name', )

    def __str__(self):
        return 'id:{}, {}'.format(self.id, self.name)

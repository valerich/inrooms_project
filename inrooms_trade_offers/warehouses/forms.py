from autocomplete_light import shortcuts as autocomplete_light
from django import forms


class WarehouseAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'brand': autocomplete_light.ChoiceWidget('BrandAutocomplete'),
            'client': autocomplete_light.ChoiceWidget('ClientAutocomplete'),
        }
from django.conf.urls import include, url
from rest_framework import routers

from .views import ProviderViewSet

router = routers.SimpleRouter()

router.register(r'provider', ProviderViewSet, base_name='provider')


urlpatterns = [
    url(r'^', include(router.urls)),
]

from rest_framework import filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from providers.models import ProviderFile
from ..serializers import ProviderFileSerializer, ProviderSerializer
from ...models import Provider


class ProviderViewSet(ModelViewSet):
    serializer_class = ProviderSerializer
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', '=id')
    queryset = Provider.objects.all()
    ordering_fields = (
        'id',
        'name',
        'modified',
    )

    @detail_route(methods=['post'])
    def file_upload(self, request, pk=None):
        obj = self.get_object()
        file_obj = request.FILES['file']

        _file = ProviderFile.objects.create(
            filename=file_obj.name,
            file=file_obj,
        )
        obj.files.add(_file)
        return Response({
            'status': 'ok'
        })

    @list_route(methods=['post'])
    def file_delete(self, request):
        # obj = self.get_object()
        file_id = request.data.get('file_id', None)

        try:
            _file = ProviderFile.objects.get(
                id=file_id
            )
        except ProviderFile.DoesNotExist:
            pass
        else:
            _file.delete()
        return Response({
            'status': 'ok'
        })

    @detail_route(methods=['get'])
    def files(self, request, pk=None):
        obj = self.get_object()
        return Response(ProviderFileSerializer(obj.files.all(), many=True).data)

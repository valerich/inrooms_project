from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import ProviderFile


class ProviderFileSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = ProviderFile
        fields = [
            'id',
            'filename',
            'file',
        ]

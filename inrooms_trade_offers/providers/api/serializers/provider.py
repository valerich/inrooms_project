from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import Provider


class ProviderSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = Provider
        fields = [
            'id',
            'name',
            'inn',
            'kpp',
            'account_current',
            'account_correspondent',
            'bank_name',
            'bik',
            'legal_form',
            'legal_index',
            'legal_region',
            'legal_district',
            'legal_city',
            'legal_street',
            'legal_house',
            'legal_building',
            'legal_flat',
            'legal_construction',
            'actual_index',
            'actual_region',
            'actual_district',
            'actual_city',
            'actual_street',
            'actual_house',
            'actual_building',
            'actual_flat',
            'actual_construction',
            'manager',
            'contact_person',
            'phone',
            'email',
            'reason',
            'contract_number',
            'contract_date',
            'head',
            'accountant_general',
            'service',
        ]

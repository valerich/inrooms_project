from django.contrib import admin

from .models import Provider


class ProviderAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', )
    search_fields = ('name', '=id', )

    def get_queryset(self, request):
        qs = Provider.all_objects.get_queryset()
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs

import factory
import factory.django
import factory.fuzzy

from .models import Provider


class ClientFactory(factory.django.DjangoModelFactory):
    inn = factory.fuzzy.FuzzyText(length=12)

    class Meta:
        model = Provider

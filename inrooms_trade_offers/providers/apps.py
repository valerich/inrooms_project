from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'providers'
    verbose_name = 'Поставщики'

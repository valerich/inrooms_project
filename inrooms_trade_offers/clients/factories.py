import datetime

import factory
import factory.django
import factory.fuzzy

from .models import Client, ClientStatistics


class ClientFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText(length=12)
    inn = factory.fuzzy.FuzzyText(length=12)

    class Meta:
        model = Client


class ClientStatisticsFactory(factory.django.DjangoModelFactory):
    client = factory.SubFactory(ClientFactory)
    date = factory.fuzzy.FuzzyDate(datetime.date(2016, 1, 1))

    class Meta:
        model = ClientStatistics

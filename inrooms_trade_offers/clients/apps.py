from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'clients'
    verbose_name = 'Партнеры'

from django.conf.urls import include, url
from rest_framework import routers

from .views import (
    ClientStatisticsViewSet,
    ClientViewSet,
    ReportClientStatistics,
    ReportMovementOfGoods,
    ReportMovementOfGoodsDetail,
    ReportProductsNotSold,
)

router = routers.SimpleRouter()

router.register(r'client', ClientViewSet, base_name='client')
router.register(r'client_statistics', ClientStatisticsViewSet, base_name='client_statistics')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^reports/client_statistic/$', ReportClientStatistics.as_view(), name='report_client_statistic'),
    url(r'^reports/products_not_sold/$', ReportProductsNotSold.as_view(), name='report_products_not_sold'),
    url(r'^reports/movement_of_goods/$', ReportMovementOfGoods.as_view(), name='report_movement_of_goods'),
    url(r'^reports/movement_of_goods/detail/$', ReportMovementOfGoodsDetail.as_view(), name='report_movement_of_goods_detail')
]

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .client import ClientSerializer
from ...models import ClientStatistics


class ClientStatisticsSerializer(serializers.ModelSerializer):
    client_detail = serializers.SerializerMethodField()

    class Meta:
        model = ClientStatistics
        fields = [
            'id',
            'date',
            'client',
            'client_detail',
            'visitors_count',
            'sales_amount',
        ]

    def get_client_detail(self, obj):
        if obj.client_id:
            return ClientSerializer(
                obj.client,
                fields=[
                    'id',
                    'name',
                ]).data
        return None

    def validate(self, data):
        client = data.get('client', None)
        user_client = self.context['user'].client
        if client and not self.context['user'].has_perm('auth.create_all_client_statistic'):
            if user_client and client.id != user_client.id:
                raise ValidationError('Вы можете создавать отчеты только на себя')
        return data

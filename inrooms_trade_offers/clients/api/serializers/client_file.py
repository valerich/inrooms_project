from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import ClientFile


class ClientFileSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = ClientFile
        fields = [
            'id',
            'filename',
            'file',
        ]

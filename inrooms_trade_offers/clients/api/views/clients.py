import django_filters
from django.conf import settings
from rest_framework import filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from clients.models import ClientFile
from config.celery import app
from ..serializers import ClientFileSerializer, ClientSerializer
from ...models import Client


def filter_has_store(queryset, value):
    if value:
        queryset = queryset.filter(store__isnull=False)
    else:
        queryset = queryset.filter(store__isnull=True)
    return queryset


class ClientFilterSet(django_filters.FilterSet):
    has_store = django_filters.BooleanFilter(action=filter_has_store)

    class Meta:
        model = Client
        fields = [
            'kind',
            'legal_form',
            'is_active',
            'has_store',
        ]


class ClientViewSet(ModelViewSet):
    serializer_class = ClientSerializer
    filter_class = ClientFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter)
    search_fields = ('name', '=id')
    queryset = Client.objects.all()
    ordering_fields = (
        'id',
        'name',
        'modified',
    )

    def perform_update(self, serializer):
        super(ClientViewSet, self).perform_update(serializer)
        if not settings.IS_TESTING:
            app.send_task('service_1c.notify_1c_client_change', args=((serializer.instance.id,),))

    def perform_create(self, serializer):
        obj = serializer.save()
        if not settings.IS_TESTING:
            app.send_task('service_1c.notify_1c_client_change', args=((obj.id,),))

    @detail_route(methods=['post'])
    def file_upload(self, request, pk=None):
        obj = self.get_object()
        file_obj = request.FILES['file']

        _file = ClientFile.objects.create(
            filename=file_obj.name,
            file=file_obj,
        )
        obj.files.add(_file)
        return Response({
            'status': 'ok'
        })

    @list_route(methods=['post'])
    def file_delete(self, request):
        # obj = self.get_object()
        file_id = request.data.get('file_id', None)

        try:
            _file = ClientFile.objects.get(
                id=file_id
            )
        except ClientFile.DoesNotExist:
            pass
        else:
            _file.delete()
        return Response({
            'status': 'ok'
        })

    @detail_route(methods=['get'])
    def files(self, request, pk=None):
        obj = self.get_object()
        return Response(ClientFileSerializer(obj.files.all(), many=True).data)

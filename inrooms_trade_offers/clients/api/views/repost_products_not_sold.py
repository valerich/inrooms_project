import os
import tempfile
from collections import defaultdict

from dateutil.relativedelta import relativedelta
from django.http.response import HttpResponse
from django.utils import timezone
from model_utils.choices import Choices
from rest_framework import serializers
from rest_framework.views import APIView

from clients.models import Client
from core.utils.excel import NewExcelHelper
from orders.models import OrderItem
from orders.models.order_b2c import OrderB2CItem
from products.models import Product, Remain
from warehouses.models import Warehouse


class ReportProductsNotSold(APIView):

    def get(self, request, *args, **kwargs):
        s = ReportSerializer(data=request.GET)
        s.is_valid(raise_exception=True)
        xls_helper = Report(date_from=s.get_date_from()).get_data()

        fd, fn = tempfile.mkstemp()
        os.close(fd)
        xls_helper.book.save(fn)
        fh = open(fn, 'rb')
        resp = fh.read()
        fh.close()
        response = HttpResponse(resp, content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=report.xlsx'
        return response


INTERVAL_TYPE_CHOICE = Choices(
    ('days', 'days', 'Дни'),
    ('months', 'months', 'Месяцы'),
    ('years', 'years', 'Годы'),
)


class ReportSerializer(serializers.Serializer):
    interval = serializers.IntegerField()
    interval_type = serializers.ChoiceField(choices=INTERVAL_TYPE_CHOICE)

    def get_date_from(self):
        today = timezone.now().date()
        return today - relativedelta(**{self.validated_data['interval_type']: self.validated_data['interval']})


class Report(object):
    def __init__(self, date_from):
        self.date_from = date_from
        self.data = []

    def get_orders_queryset(self):
        qs = OrderItem.objects.filter(order__created__gte=self.date_from)
        qs = qs.select_related('order', 'order__client')
        # qs = qs.order_by('-date')
        return qs

    def get_orders_b2c_queryset(self):
        qs = OrderB2CItem.objects.filter(order__date__gte=self.date_from)
        qs = qs.select_related('order', 'order__client')
        # qs = qs.order_by('-date')
        return qs

    def get_clients(self):
        return Client.objects.all()

    def get_available_products(self):
        warehouses = Warehouse.objects.filter(client__isnull=False).select_related('client')
        available_products = defaultdict(lambda: set())
        for r in Remain.objects.filter(warehouse__in=warehouses, value__gt=1):
            available_products[r.warehouse.client_id].add(r.product_id)
        return available_products

    def get_order_b2c_products(self):
        data = defaultdict(lambda: set())
        for o in self.get_orders_b2c_queryset():
            data[o.order.client_id].add(o.product_id)
        return data

    def get_order_products(self):
        data = defaultdict(lambda: set())
        for o in self.get_orders_queryset():
            data[o.order.client_id].add(o.product_id)
        return data

    def get_data(self):
        self.in_report_product_ids = set()

        available_products = self.get_available_products()
        order_b2c_products = self.get_order_b2c_products()

        data = defaultdict(lambda: set())
        for client_id, product_ids in available_products.items():
            data[client_id] = product_ids - order_b2c_products[client_id]
            self.in_report_product_ids.update(data[client_id])

        return self.get_xls_helper(data)

    def get_xls_helper(self, data):
        order_products = self.get_order_products()

        products = {p.id: p for p in Product.objects.filter(id__in=self.in_report_product_ids)}

        xls_helper = NewExcelHelper()
        xls_helper.add_page('Товары')

        xls_helper.write_cell("Клиент", 0, 0)
        xls_helper.write_cell("id товара", 0, 1)
        xls_helper.write_cell("Название товара", 0, 2)
        xls_helper.write_cell("Артикул товара", 0, 3)
        xls_helper.write_cell("Была ли закупка?", 0, 4)

        row_num = 1
        for client in self.get_clients():
            for product_id in data[client.id]:
                product = products[product_id]
                xls_helper.write_cell(client.name, row_num, 0)
                xls_helper.write_cell(product.id, row_num, 1)
                xls_helper.write_cell(product.name, row_num, 2)
                xls_helper.write_cell(product.article, row_num, 3)
                xls_helper.write_cell(product_id in order_products[client.id], row_num, 4)
                row_num += 1

        return xls_helper

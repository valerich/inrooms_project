import locale
from collections import defaultdict
from decimal import Decimal

from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from core.utils.date import date_range
from orders.models.order_b2c import OrderB2C, OrderReturnB2C
from stores.models import EmployeeWorkFactItem
from ...models import Client, ClientStatistics


class ReportClientStatistics(APIView):

    def get(self, request, *args, **kwargs):
        s = ReportSerializer(data=request.GET)
        s.is_valid(raise_exception=True)
        report_data = Report(date_from=s.validated_data['date_from'],
                             date_to=s.validated_data['date_to'],
                             user=request.user,
                             period_kind=s.validated_data.get('period_kind', 'days'),
                             include_order_returns=s.validated_data.get('include_order_returns', False)).get_data()
        return Response(data=report_data)


class ReportSerializer(serializers.Serializer):
    date_from = serializers.DateField()
    date_to = serializers.DateField()
    period_kind = serializers.ChoiceField(choices=(('days', 'Дни'), ('months', 'Месяцы')), required=False)
    include_order_returns = serializers.BooleanField(required=False)


class Report(object):
    def __init__(self, date_from, date_to, user, period_kind, include_order_returns):
        self.date_from = date_from
        self.date_to = date_to
        self.user = user
        self.period_kind = period_kind
        self.include_order_returns = include_order_returns
        self.data = []

        if user.has_perm('auth.view_all_client_statistic'):
            self.qs_kind = 'full'
        elif user.has_perm('auth.view_self_client_statistic'):
            if user.client_id:
                self.qs_kind = 'self'
            else:
                self.qs_kind = 'none'
        else:
            self.qs_kind = 'none'

    def get_client_statistic_queryset(self):
        qs = ClientStatistics.objects.filter(date__gte=self.date_from, date__lte=self.date_to)
        if self.qs_kind == 'none':
            qs = qs.none()
        elif self.qs_kind == 'self':
            qs = qs.filter(client=self.user.client)
        qs = qs.select_related('client')
        qs = qs.order_by('-date')
        return qs

    def get_order_b2c_queryset(self):
        qs = OrderB2C.objects.filter(date__gte=self.date_from, date__lte=self.date_to)
        if self.qs_kind == 'none':
            qs = qs.none()
        elif self.qs_kind == 'self':
            qs = qs.filter(client=self.user.client)
        qs = qs.select_related('client')
        qs = qs.order_by('-date')
        return qs

    def get_order_return_b2c_queryset(self):
        qs = OrderReturnB2C.objects.filter(date__gte=self.date_from, date__lte=self.date_to)
        if self.qs_kind == 'none':
            qs = qs.none()
        elif self.qs_kind == 'self':
            qs = qs.filter(client=self.user.client)
        qs = qs.select_related('client')
        qs = qs.order_by('-date')
        return qs

    def get_clients(self):
        qs = Client.objects.all()
        if self.qs_kind == 'none':
            qs = qs.none()
        elif self.qs_kind == 'self' and self.user.client:
            qs = qs.filter(id=self.user.client.id)
        return qs

    def get_employees(self):
        qs = EmployeeWorkFactItem.objects.filter(
            date__gte=self.date_from,
            date__lte=self.date_to,
        )
        if self.qs_kind == 'none':
            qs = qs.none()
        elif self.qs_kind == 'self':
            qs = qs.filter(store__client=self.user.client)
        qs = qs.select_related('employee')
        qs = qs.order_by('-date', 'employee__last_name')
        return qs

    def get_data(self):
        all_sales_amount = 0
        if self.period_kind == 'days':
            dates = date_range(self.date_from, self.date_to)
        else:
            dates = []
            for d in date_range(self.date_from, self.date_to):
                key = '{}.{}'.format(d.month, d.year)
                if key not in dates:
                    dates.append(key)
        date_sum_sales_amount = [0 for _ in dates]
        data = defaultdict(lambda: dict())
        # for cs in self.get_client_statistic_queryset():
        #     data[cs.client][cs.date] = {'visitors_count': cs.visitors_count,
        #                                 'sales_amount': Decimal('0.0000000000'),}
        for o in self.get_order_b2c_queryset():
            if self.period_kind == 'days':
                key = o.date
            else:
                key = '{}.{}'.format(o.date.month, o.date.year)
            if key not in data[o.client]:
                data[o.client][key] = {'visitors_count': o.visits,
                                          'sales_amount': o.amount_full_rur,}
            else:
                data[o.client][key]['sales_amount'] += o.amount_full_rur
                data[o.client][key]['visitors_count'] += o.visits

        if self.include_order_returns:
            for orr in self.get_order_return_b2c_queryset():
                if self.period_kind == 'days':
                    key = orr.date
                else:
                    key = '{}.{}'.format(orr.date.month, orr.date.year)
                if key not in data[orr.client]:
                    data[orr.client][key] = {'visitors_count': 0,
                                           'sales_amount': -1 * orr.amount_full_rur,}
                else:
                    data[orr.client][key]['sales_amount'] -= orr.amount_full_rur

        employees_work_report_data = defaultdict(lambda: defaultdict(list))
        for i in self.get_employees():
            if self.period_kind == 'days':
                key = i.date
            else:
                break
            employees_work_report_data[i.store.client][key].append(i.employee.get_short_name())

        report_data = []
        for client in self.get_clients():
            client_data = data[client]
            client_employee_data = employees_work_report_data[client]
            visitors_count_data = []
            sales_amount_data = []
            employee_work_data = []
            for i, date in enumerate(dates):
                item_data = client_data.get(
                    date,
                    {
                        'visitors_count': 0,
                        'sales_amount': Decimal('0.0000000000'),
                    }
                )
                visitors_count_data.append(item_data['visitors_count'])
                sales_amount_data.append(item_data['sales_amount'])
                employee_work_data.append(client_employee_data[date])
                date_sum_sales_amount[i] += item_data['sales_amount']
            sum_visitors_count_data = sum(visitors_count_data)
            sum_sales_amount_data = sum(sales_amount_data)
            all_sales_amount += sum_sales_amount_data
            if sum_visitors_count_data or sum_sales_amount_data:
                report_data.append({
                    'name': client.name,
                    'visitors_count': visitors_count_data,
                    'sales_amount': sales_amount_data,
                    'visitors_sum': sum_visitors_count_data,
                    'sales_sum': sum_sales_amount_data,
                    'employee_work_data': employee_work_data,
                })
        locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
        if self.period_kind == 'days':
            return_dates = ['<span style="color: red;fill: red;" class="weekend">{}</span>'.format(d.strftime('%d.%m %a')) if d.weekday() >= 5 else d.strftime('%d.%m %a') for d in date_range(self.date_from, self.date_to)]
        else:
            return_dates = dates
        return {'date_from': self.date_from,
                'date_to': self.date_to,
                'dates': return_dates,
                'report_data': report_data,
                'date_sum_sales_amount': date_sum_sales_amount,
                'all_sales_amount': all_sales_amount}

from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import ClientStatisticsSerializer
from ...models import ClientStatistics


class ClientStatisticsViewSet(mixins.CreateModelMixin,
                              mixins.ListModelMixin,
                              GenericViewSet):
    serializer_class = ClientStatisticsSerializer
    filter_backends = (filters.SearchFilter, )
    queryset = ClientStatistics.objects.all()

    def get_queryset(self):
        qs = super().get_queryset()

        qs = qs.select_related('client')

        if self.request.user.has_perm('auth.view_all_client_statistic'):
            return qs
        elif self.request.user.has_perm('auth.view_self_client_statistic'):
            if self.request.user.client_id:
                return qs.filter(client_id=self.request.user.client_id)
        return qs.none()

    def create(self, request, *args, **kwargs):
        if not request.data.get('client', None):
            request.data['client'] = request.user.client_id
        return super().create(request, *args, **kwargs)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['user'] = self.request.user
        return context

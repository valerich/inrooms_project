import os
import tempfile
import datetime
from django.conf import settings
from collections import OrderedDict, defaultdict

import openpyxl
from dateutil.relativedelta import relativedelta
from django.db.models.aggregates import Sum, Count
from django.db.models.query_utils import Q
from django.utils import timezone
from model_utils.choices import Choices
from rest_framework import filters, serializers
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from django.http.response import HttpResponse
from rest_framework.views import APIView
from sorl.thumbnail.shortcuts import get_thumbnail

from clients.models import Client
from orders.models import OrderItem, OrderItemSize, OrderStatus
from orders.models.order_b2c import (
    OrderB2CItem,
    OrderB2CItemSize,
    OrderReturnItem,
    OrderReturnItemSize,
    OrderReturnB2CItem, OrderReturnB2CItemSize)
from products.models import Product, Remain, RemainHistory
from warehouses.models import Warehouse



from core.utils.excel import NewExcelHelper

INTERVAL_TYPE_CHOICE = Choices(
    ('months', 'months', 'Месяцы'),
    ('years', 'years', 'Годы'),
)


class ReportSerializer(serializers.Serializer):
    interval = serializers.IntegerField()
    interval_type = serializers.ChoiceField(choices=INTERVAL_TYPE_CHOICE)
    client = serializers.PrimaryKeyRelatedField(queryset=Client.objects.all(), required=False)

    def get_date_from(self):
        today = timezone.now().date()
        return today - relativedelta(**{self.validated_data['interval_type']: self.validated_data['interval']})


class ReportDetailSerializer(ReportSerializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())


class ReportDataSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    color_detail = serializers.SerializerMethodField()
    orders = serializers.SerializerMethodField()
    orders_b2c = serializers.SerializerMethodField()
    orders_return = serializers.SerializerMethodField()
    orders_return_b2c = serializers.SerializerMethodField()
    remains = serializers.SerializerMethodField()
    order_dates = serializers.SerializerMethodField()
    order_price = serializers.SerializerMethodField()
    turnaround = serializers.SerializerMethodField()
    main_collection_detail = serializers.SerializerMethodField()

    class Meta:
        model = Product

        fields = [
            'id',
            'article',
            'name',
            'image',
            'color_detail',
            'orders',
            'orders_b2c',
            'orders_return',
            'orders_return_b2c',
            'remains',
            'order_dates',
            'order_price',
            'turnaround',
            'main_collection_detail',
        ]

    def get_image(self, obj):
        if self.context['with_images']:
            pi = obj.images.all().first()
            return self.resize_image(pi, obj)
        else:
            return None

    def resize_image(self, pi, obj):
        if pi:
            try:
                _200x200 = get_thumbnail(pi.image, '400x400', crop="center").url
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
            except IOError as e:
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1500 = request.build_absolute_uri(_950x1200)
                src = request.build_absolute_uri(src)
            return {
                'src': src,
                '200x200': _200x200,
                '950x1200': _950x1200
            }
        else:
            return None

    def get_color_detail(self, obj):
        return {
            'id': obj.color_id,
            'name': obj.color.name
        }

    def get_main_collection_detail(self, obj):
        data = {
            'id': obj.main_collection.id,
            'name': obj.main_collection.name,
            'parent': None
        }
        if obj.main_collection.parent:
            data['parent'] = {
                'id': obj.main_collection.parent.id,
                'name': obj.main_collection.parent.name,
            }
        return data

    def get_orders(self, obj):
        return self.context['data'][obj.id]['orders']

    def get_orders_b2c(self, obj):
        return self.context['data'][obj.id]['orders_b2c']

    def get_orders_return(self, obj):
        return self.context['data'][obj.id]['orders_return']

    def get_orders_return_b2c(self, obj):
        return self.context['data'][obj.id]['orders_return_b2c']

    def get_remains(self, obj):
        return self.context['data'][obj.id]['remains']

    def get_order_dates(self, obj):
        return self.context['data'][obj.id]['order_dates']

    def get_order_price(self, obj):
        return self.context['data'][obj.id]['order_price']

    def get_turnaround(self, obj):
        order_b2c_quantity = self.context['order_b2c_quantity_data'][obj.id]
        remain_history = self.context['remain_history_data'][obj.id]

        data = {
            'remain_avg': None,
            'value': None,
        }

        if remain_history['count_value_avg']:
            remain_avg = remain_history['sum_value_avg'] / remain_history['count_value_avg']
            data['remain_avg'] = remain_avg
            if order_b2c_quantity:
                data['value'] = int((remain_avg * remain_history['count_value_avg'] * 30) / order_b2c_quantity)

        return data


class ReportMovementOfGoods(ListAPIView):
    serializer_class = ReportDataSerializer
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)
    search_fields = ('^article', 'name', '=id')
    filter_fields = ('id', )

    def dispatch(self, request, *args, **kwargs):
        serializer = ReportSerializer(data=request.GET)
        serializer.is_valid(True)
        self.client = serializer.validated_data.get('client', None)
        if not self.request.user.has_perm('auth.view_all_report_movement_of_goods'):
            if self.request.user.client:
                self.client = self.request.user.client
            else:
                self.client = None
        self.today = datetime.date.today()
        self.date_from = serializer.get_date_from()

        self.filter_has_remains = None
        if 'has_remains' in request.GET:
            if request.GET['has_remains'] == '3':
                self.filter_has_remains = False
            elif request.GET['has_remains'] == '2':
                self.filter_has_remains = True

        self.filter_has_tvp = None
        if 'has_tvp' in request.GET:
            if request.GET['has_tvp'] == '3':
                self.filter_has_tvp = False
            elif request.GET['has_tvp'] == '2':
                self.filter_has_tvp = True
        return super(ReportMovementOfGoods, self).dispatch(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        self.with_images = True

        if request.GET.get('as', None) == 'xls':
            queryset = self.filter_queryset(self.get_queryset())

            self.with_images = bool(request.GET.get('with_images', False))

            serializer = self.get_serializer(queryset, many=True)
            xls_helper = Report(data=serializer.data, with_images=self.with_images).get_data()

            fd, fn = tempfile.mkstemp()
            os.close(fd)
            xls_helper.book.save(fn)
            fh = open(fn, 'rb')
            resp = fh.read()
            fh.close()
            response = HttpResponse(resp, content_type='application/ms-excel')
            response['Content-Disposition'] = 'attachment; filename=report.xlsx'
            return response
        return super().list(request, *args, **kwargs)

    def get_queryset(self):
        qs = Product.objects.all()
        if not self.client:
            return qs.none()
        product_on_remains = Remain.objects.filter(
            value__gt=0,
            warehouse__client=self.client
        ).values_list('product_id', flat=True).distinct()

        product_on_orders = OrderItem.objects.filter(
            order__created__gte=self.date_from,
            order__client=self.client,
            order__status__code__in=[
                'received',
                'shipped',
            ]
        ).values_list('product_id', flat=True)

        if self.filter_has_tvp is not None:
            status = OrderStatus.objects.get(code='shipped')
            if self.filter_has_tvp:
                product_on_orders = product_on_orders.filter(order__status=status)
            else:
                product_on_orders = product_on_orders.exclude(order__status=status)

        product_on_orders_b2c = OrderB2CItem.objects.filter(
            order__date__gte=self.date_from,
            order__client=self.client
        ).values_list('product_id', flat=True)
        product_on_orders_return = OrderReturnItem.objects.filter(
            order__date__gte=self.date_from,
            order__client=self.client
        ).values_list('product_id', flat=True)
        product_on_orders_return_b2c = OrderReturnB2CItem.objects.filter(
            order__date__gte=self.date_from,
            order__client=self.client
        ).values_list('product_id', flat=True)

        q = Q(id__in=product_on_orders)
        q |= Q(id__in=product_on_orders_b2c)
        q |= Q(id__in=product_on_orders_return_b2c)
        q |= Q(id__in=product_on_orders_return)


        if self.filter_has_remains is not None:
            if self.filter_has_remains:
                qs = qs.filter(id__in=product_on_remains)
            else:
                qs = qs.exclude(id__in=product_on_remains)
        else:
            q |= Q(id__in=product_on_remains)

        qs = qs.filter(q).select_related('color', 'main_collection', 'main_collection__parent')
        return qs

    def get_serializer_context(self):
        data = defaultdict(lambda: defaultdict(list))

        # orders
        sizes = dict()
        for i in OrderItemSize.objects.filter(
            order_item__order__created__gte=self.date_from,
            order_item__order__client=self.client
        ).values('order_item__product_id', 'order_item__price', 'quantity', 'size__name').order_by('size__weight'):
            if i['order_item__product_id'] not in sizes:
                sizes[i['order_item__product_id']] = OrderedDict()
            if i['size__name'] not in sizes[i['order_item__product_id']]:
                sizes[i['order_item__product_id']][i['size__name']] = {
                    'quantity': i['quantity'],
                    'summ': i['quantity'] * i['order_item__price']
                }
            else:
                sizes[i['order_item__product_id']][i['size__name']]['quantity'] += i['quantity']
                sizes[i['order_item__product_id']][i['size__name']]['summ'] += i['quantity'] * i['order_item__price']
        for product_id, size_data in sizes.items():
            for size_name in size_data:
                data[product_id]['orders'].append({
                    'size': size_name,
                    'quantity': size_data[size_name]['quantity'],
                    'summ': size_data[size_name]['summ']
                })
        for oi in OrderItem.objects.filter(
            order__created__gte=self.date_from,
            order__client=self.client
        ).order_by('-order__created').select_related('order'):
            data[oi.product_id]['order_dates'].append(
                oi.order.created.date()
            )
            data[oi.product_id]['order_price'] = oi.price

        # order_b2c
        sizes = dict()
        for i in OrderB2CItemSize.objects.filter(
            order_item__order__date__gte=self.date_from,
            order_item__order__client=self.client
        ).values('order_item__product_id', 'order_item__price', 'quantity', 'size__name').order_by('size__weight'):
            if i['order_item__product_id'] not in sizes:
                sizes[i['order_item__product_id']] = OrderedDict()
            if i['size__name'] not in sizes[i['order_item__product_id']]:
                sizes[i['order_item__product_id']][i['size__name']] = {
                    'quantity': i['quantity'],
                    'summ': i['quantity'] * i['order_item__price']
                }
            else:
                sizes[i['order_item__product_id']][i['size__name']]['quantity'] += i['quantity']
                sizes[i['order_item__product_id']][i['size__name']]['summ'] += i['quantity'] * i['order_item__price']
        for product_id, size_data in sizes.items():
            for size_name in size_data:
                data[product_id]['orders_b2c'].append({
                    'size': size_name,
                    'quantity': size_data[size_name]['quantity'],
                    'summ': size_data[size_name]['summ']
                })

        # order_return
        sizes = dict()
        for i in OrderReturnItemSize.objects.filter(
            order_item__order__date__gte=self.date_from,
            order_item__order__client=self.client
        ).values('order_item__product_id', 'order_item__price', 'quantity', 'size__name').order_by('size__weight'):
            if i['order_item__product_id'] not in sizes:
                sizes[i['order_item__product_id']] = OrderedDict()
            if i['size__name'] not in sizes[i['order_item__product_id']]:
                sizes[i['order_item__product_id']][i['size__name']] = {
                    'quantity': i['quantity'],
                    'summ': i['quantity'] * i['order_item__price']
                }
            else:
                sizes[i['order_item__product_id']][i['size__name']]['quantity'] += i['quantity']
                sizes[i['order_item__product_id']][i['size__name']]['summ'] += i['quantity'] * i['order_item__price']
        for product_id, size_data in sizes.items():
            for size_name in size_data:
                data[product_id]['orders_return'].append({
                    'size': size_name,
                    'quantity': size_data[size_name]['quantity'],
                    'summ': size_data[size_name]['summ']
                })

        # order_return_b2c
        sizes = dict()
        for i in OrderReturnB2CItemSize.objects.filter(
            order_item__order__date__gte=self.date_from,
            order_item__order__client=self.client
        ).values('order_item__product_id', 'order_item__price', 'quantity', 'size__name').order_by('size__weight'):
            if i['order_item__product_id'] not in sizes:
                sizes[i['order_item__product_id']] = OrderedDict()
            if i['size__name'] not in sizes[i['order_item__product_id']]:
                sizes[i['order_item__product_id']][i['size__name']] = {
                    'quantity': i['quantity'],
                    'summ': i['quantity'] * i['order_item__price']
                }
            else:
                sizes[i['order_item__product_id']][i['size__name']]['quantity'] += i['quantity']
                sizes[i['order_item__product_id']][i['size__name']]['summ'] += i['quantity'] * i['order_item__price']
        for product_id, size_data in sizes.items():
            for size_name in size_data:
                data[product_id]['orders_return_b2c'].append({
                    'size': size_name,
                    'quantity': size_data[size_name]['quantity'],
                    'summ': size_data[size_name]['summ']
                })

        # remains
        sizes = dict()
        for i in Remain.objects.filter(
            warehouse__client=self.client,
            value__gt=0,
        ).values('size__name', 'value', 'product_id').order_by('size__weight'):
            if i['product_id'] not in sizes:
                sizes[i['product_id']] = OrderedDict()
            if i['size__name'] not in sizes[i['product_id']]:
                sizes[i['product_id']][i['size__name']] = {
                    'quantity': i['value'],
                }
            else:
                sizes[i['product_id']][i['size__name']]['quantity'] += i['value']
        for product_id, size_data in sizes.items():
            for size_name in size_data:
                data[product_id]['remains'].append({
                    'size': size_name,
                    'quantity': size_data[size_name]['quantity'],
                })

        # turnaround
        order_b2c_quantity_qs = OrderB2CItemSize.objects.filter(
            order_item__order__date__gte=self.date_from,
            order_item__order__client=self.client
        ).values('order_item__product').order_by('order_item__product').annotate(sum_quantity=Sum('quantity'))

        order_b2c_quantity_data = defaultdict(int)

        for s in order_b2c_quantity_qs:
            order_b2c_quantity_data[s['order_item__product']] = s['sum_quantity']

        remain_history_qs = RemainHistory.objects.filter(
            warehouse__in=Warehouse.objects.filter(client=self.client),
            value_avg__gt=0,
        ).exclude(
            Q(year__gt=self.today.year) | Q(year=self.today.year, month__gt=self.today.month)
        )
        if self.today.year > self.date_from.year:
            remain_history_qs = remain_history_qs.filter(
                Q(year=self.date_from.year, month__gte=self.date_from.month) | Q(year__gt=self.date_from.year)
            )
        else:
            remain_history_qs = remain_history_qs.filter(year=self.date_from.year, month__gte=self.date_from.month)
        remain_history_qs = remain_history_qs.values('product').order_by('product').annotate(
            sum_value_avg=Sum('value_avg'),
            count_value_avg=Count('value_avg'),
        )

        remain_history_data = defaultdict(lambda: defaultdict(int))
        for s in remain_history_qs:
            remain_history_data[s['product']] = {
                'sum_value_avg': s['sum_value_avg'],
                'count_value_avg': s['count_value_avg'],
            }

        return {'data': data,
                'with_images': self.with_images,
                'order_b2c_quantity_data': order_b2c_quantity_data,
                'remain_history_data': remain_history_data}


class ReportMovementOfGoodsDetail(APIView):

    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)
    search_fields = ('^article', 'name', '=id')

    def get(self, request, *args, **kwargs):
        serializer = ReportDetailSerializer(data=request.GET)
        serializer.is_valid(True)
        self.client = serializer.validated_data.get('client', None)
        if not self.request.user.has_perm('auth.view_all_report_movement_of_goods'):
            if self.request.user.client:
                self.client = self.request.user.client
            else:
                self.client = None
        self.product = serializer.validated_data['product']
        self.date_from = serializer.get_date_from()
        data = self.get_report_data()
        return Response(data=data)

    def get_report_data(self):
        data = {
            'order': self.get_order_data(),
            'order_b2c': self.get_order_b2c_data(),
            'order_return': self.get_order_return_data(),
            'order_return_b2c': self.get_order_return_b2c_data(),
            'remains': self.get_remains_data()
        }
        return data

    def get_order_data(self):
        data = []
        for oi in OrderItem.objects.filter(
            order__created__gte=self.date_from,
            order__client=self.client,
            product=self.product,
        ).order_by('-order__created'):
            sizes = OrderedDict()
            item_data = {
                'id': oi.order_id,
                'date': oi.order.created.date(),
                'sizes': []
            }
            for ois in oi.orderitemsize_set.all().order_by('size__weight'):
                if ois.size.name not in sizes:
                    sizes[ois.size.name] = ois.quantity
                else:
                    sizes[ois.size.name] += ois.quantity
            for size, quantity in sizes.items():
                item_data['sizes'].append(
                    {'size': size,
                     'quantity': quantity
                     }
                )
            data.append(item_data)
        return data

    def get_order_b2c_data(self):
        data = []
        for oi in OrderB2CItem.objects.filter(
            order__date__gte=self.date_from,
            order__client=self.client,
            product=self.product,
        ).order_by('-order__date'):
            item_data = {
                'id': oi.order_id,
                'date': oi.order.date,
                'sizes': []
            }
            for ois in oi.orderb2citemsize_set.all().order_by('size__weight'):
                item_data['sizes'].append({
                    'size': ois.size.name,
                    'quantity': ois.quantity,
                })
            data.append(item_data)
        return data

    def get_order_return_data(self):
        data = []
        for oi in OrderReturnItem.objects.filter(
            order__date__gte=self.date_from,
            order__client=self.client,
            product=self.product,
        ).order_by('-order__date'):
            item_data = {
                'id': oi.order_id,
                'date': oi.order.date,
                'sizes': []
            }
            for ois in oi.orderreturnitemsize_set.all().order_by('size__weight'):
                item_data['sizes'].append({
                    'size': ois.size.name,
                    'quantity': ois.quantity,
                })
            data.append(item_data)
        return data

    def get_order_return_b2c_data(self):
        data = []
        for oi in OrderReturnB2CItem.objects.filter(
            order__date__gte=self.date_from,
            order__client=self.client,
            product=self.product,
        ).order_by('-order__date'):
            item_data = {
                'id': oi.order_id,
                'date': oi.order.date,
                'sizes': []
            }
            for ois in oi.orderreturnb2citemsize_set.all().order_by('size__weight'):
                item_data['sizes'].append({
                    'size': ois.size.name,
                    'quantity': ois.quantity,
                })
            data.append(item_data)
        return data

    def get_remains_data(self):
        sizes = OrderedDict()
        data = []
        for i in Remain.objects.filter(
            value__gt=0,
            warehouse__client=self.client,
            product=self.product,
        ).select_related('size'):
            if i.size.name not in sizes:
                sizes[i.size.name] = i.value
            else:
                sizes[i.size.name] += i.value
        for size_name, quantity in sizes.items():
            data.append({
                'size': size_name,
                'quantity': quantity
            })
        return data


class Report(object):

    def __init__(self, data, with_images=False):
        self.data = data
        self.with_images = with_images

    def get_data(self):
        return self.get_xls_helper()

    def get_xls_helper(self):
        xls_helper = NewExcelHelper()
        xls_helper.add_page('Товары')

        if self.with_images:
            xls_helper.write_cell('фото', 0, 0)
            xls_helper.sheet.column_dimensions['A'].width = 15

        xls_helper.write_cell('Наименование', 0, 1)
        xls_helper.write_cell('Артикул', 0, 2)
        xls_helper.write_cell('Поставлено', 0, 3)
        xls_helper.write_cell('Дата', 0, 5)
        xls_helper.write_cell('Продано', 0, 6)
        xls_helper.write_cell('Возврат от партнера', 0, 8)
        xls_helper.write_cell('Возврат от покупателя', 0, 10)
        xls_helper.write_cell('Остаток', 0, 12)
        xls_helper.write_cell('Оборачиваемость', 0, 14)
        xls_helper.write_cell('Коллекция', 0, 15)
        xls_helper.write_cell('Родительская коллекция', 0, 16)
        xls_helper.sheet.merge_cells(
            start_row=1,
            start_column=4,
            end_row=1,
            end_column=5
        )
        xls_helper.sheet.merge_cells(
            start_row=1,
            start_column=7,
            end_row=1,
            end_column=8
        )
        xls_helper.sheet.merge_cells(
            start_row=1,
            start_column=9,
            end_row=1,
            end_column=10
        )
        xls_helper.sheet.merge_cells(
            start_row=1,
            start_column=11,
            end_row=1,
            end_column=12
        )
        xls_helper.sheet.merge_cells(
            start_row=1,
            start_column=13,
            end_row=1,
            end_column=14
        )

        row = 1

        for item in self.data:
            prev_row = row
            if self.with_images:
                image = item.get('image', None)
                if image:
                    image = image.get('200x200', None)
                    if image:
                        try:
                            img = openpyxl.drawing.image.Image(settings.MEDIA_ROOT + image[6:], size=(None, 100))
                            img.anchor(xls_helper.sheet.cell('A{}'.format(row + 1)), anchortype='oneCell')
                            xls_helper.sheet.add_image(img)
                        except FileNotFoundError:
                            pass

            if item['turnaround']['value']:
                xls_helper.write_cell(str(item['turnaround']['value']), row, 14)

            for order in item['orders']:
                xls_helper.write_cell(order['size'], row, 3)
                xls_helper.write_cell(order['quantity'], row, 4)
                row += 1
            row = prev_row

            for date in item['order_dates']:
                xls_helper.write_cell(date.strftime('%d.%m.%Y'), row, 5)
                row += 1
            row = prev_row

            for order_b2c in item['orders_b2c']:
                xls_helper.write_cell(order_b2c['size'], row, 6)
                xls_helper.write_cell(order_b2c['quantity'], row, 7)
                row += 1
            row = prev_row

            for order_return in item['orders_return']:
                xls_helper.write_cell(order_return['size'], row, 8)
                xls_helper.write_cell(order_return['quantity'], row, 9)
                row += 1
            row = prev_row

            for order_return_b2c in item['orders_return_b2c']:
                xls_helper.write_cell(order_return_b2c['size'], row, 10)
                xls_helper.write_cell(order_return_b2c['quantity'], row, 11)
                row += 1
            row = prev_row

            for remain in item['remains']:
                xls_helper.write_cell(remain['size'], row, 12)
                xls_helper.write_cell(remain['quantity'], row, 13)
                row += 1
            row = prev_row

            max_row_count = max(
                len(item['orders']),
                len(item['order_dates']),
                len(item['orders_b2c']),
                len(item['orders_return']),
                len(item['orders_return_b2c']),
                len(item['remains']),
            )

            for i in range(max_row_count):
                xls_helper.write_cell('№{} {} ({})'.format(item['id'], item['name'], item['color_detail']['name']), row + i, 1, wrap_text=True)
                xls_helper.write_cell(item['article'], row + i, 2, wrap_text=True)
                xls_helper.write_cell(item['main_collection_detail']['name'], row + i, 15)
                if item['main_collection_detail']['parent']:
                    xls_helper.write_cell(item['main_collection_detail']['parent']['name'], row + i, 16)
                else:
                    xls_helper.write_cell(item['main_collection_detail']['name'], row + i, 16)

            if self.with_images:
                if max_row_count <= 1:
                    xls_helper.sheet.row_dimensions[row + 1].height = 80
                elif max_row_count == 2:
                    xls_helper.sheet.row_dimensions[row + 1].height = 40
                    xls_helper.sheet.row_dimensions[row + 2].height = 40
                elif max_row_count == 3:
                    xls_helper.sheet.row_dimensions[row + 1].height = 27
                    xls_helper.sheet.row_dimensions[row + 2].height = 27
                    xls_helper.sheet.row_dimensions[row + 3].height = 27
                elif max_row_count == 4:
                    xls_helper.sheet.row_dimensions[row + 1].height = 20
                    xls_helper.sheet.row_dimensions[row + 2].height = 20
                    xls_helper.sheet.row_dimensions[row + 3].height = 20
                    xls_helper.sheet.row_dimensions[row + 4].height = 20

            if max_row_count > 1:
                xls_helper.sheet.merge_cells(
                    start_row=prev_row + 1,
                    start_column=1,
                    end_row=prev_row + max_row_count,
                    end_column=1
                )
                xls_helper.sheet.merge_cells(
                    start_row=prev_row + 1,
                    start_column=15,
                    end_row=prev_row + max_row_count,
                    end_column=15
                )
                row += max_row_count
            else:
                row += 1

        return xls_helper

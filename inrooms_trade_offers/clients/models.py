import os
import uuid
from collections import OrderedDict

from autoslug.fields import AutoSlugField
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

from core.models import CURRENCY_CHOICES, DeleteMixin


def generate_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'client/{}{}'.format(uuid.uuid4(), ext)


class ClientFile(TimeStampedModel):
    filename = models.CharField('Название файла', max_length=1000, blank=True)
    file = models.FileField('Файл', upload_to=generate_filename, max_length=755, blank=True)

    class Meta:
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'
        ordering = ('-id', )

    def get_file_name(self):
        file = self.file.name.split('/')[-1]
        return file[:file.rfind('.')]


class ClientManager(models.Manager):

    def get_own(self):
        return Client.objects.get_or_create(
            code='_own',
            defaults={
                'name': 'Своя розница',
                'inn': '-'
            }
        )[0]


CLIENT_KIND_CHOICES = Choices(
    (1, 'partner', 'Партнер'),
    (2, 'provider', 'Поставщик'),
)


LEGAL_FORM_CHOICES = Choices(
    (1, 'ip', 'Индивидуальный предприниматель'),
    (2, 'ooo', 'Общество с ограниченной ответственностью'),
    (3, 'pp', 'Частное лицо'),
)


class Client(DeleteMixin, TimeStampedModel):
    LEGAL_FORM = LEGAL_FORM_CHOICES
    KIND = CLIENT_KIND_CHOICES

    is_active = models.BooleanField('Активен', default=True)
    kind = models.PositiveSmallIntegerField('Тип', choices=KIND, default=KIND.partner)
    name = models.CharField('Название', max_length=100, blank=True)
    code = AutoSlugField('Код', unique=True, populate_from='name',
                         max_length=300, editable=False, always_update=False)

    inn = models.CharField('ИНН', max_length=12, unique=True)
    kpp = models.CharField('КПП', max_length=9, blank=True)

    account_current = models.CharField('Расчетный счет', max_length=255, blank=True)
    account_correspondent = models.CharField('Корреспондентский счет', max_length=255, blank=True)
    bank_name = models.CharField('Наименование банка', max_length=255, blank=True)
    bik = models.CharField('БИК', max_length=255, blank=True)

    legal_form = models.PositiveSmallIntegerField('Организационно-правовая форма', choices=LEGAL_FORM, default=LEGAL_FORM.ip)

    legal_index = models.CharField('Юридический адрес "Индекс"', max_length=255, blank=True)
    legal_region = models.CharField('Юридический адрес "Регион"', max_length=255, blank=True)
    legal_district = models.CharField('Юридический адрес "Район"', max_length=255, blank=True)
    legal_city = models.CharField('Юридический адрес "Город"', max_length=255, blank=True)
    legal_street = models.CharField('Юридический адрес "Улица"', max_length=255, blank=True)
    legal_house = models.CharField('Юридический адрес "Дом"', max_length=255, blank=True)
    legal_building = models.CharField('Юридический адрес "Корпус"', max_length=255, blank=True)
    legal_flat = models.CharField('Юридический адрес "Квартира"', max_length=255, blank=True)
    legal_construction = models.CharField('Юридический адрес "Строение"', max_length=255, blank=True)

    actual_index = models.CharField('Фактический адрес "Индекс"', max_length=255, blank=True)
    actual_region = models.CharField('Фактический адрес "Регион"', max_length=255, blank=True)
    actual_district = models.CharField('Фактический адрес "Район"', max_length=255, blank=True)
    actual_city = models.CharField('Фактический адрес "Город"', max_length=255, blank=True)
    actual_street = models.CharField('Фактический адрес "Улица"', max_length=255, blank=True)
    actual_house = models.CharField('Фактический адрес "Дом"', max_length=255, blank=True)
    actual_building = models.CharField('Фактический адрес "Корпус"', max_length=255, blank=True)
    actual_flat = models.CharField('Фактический адрес "Квартира"', max_length=255, blank=True)
    actual_construction = models.CharField('Фактический адрес "Строение"', max_length=255, blank=True)

    manager = models.CharField('Менеджер', max_length=100, blank=True)
    contact_person = models.CharField('Контактное лицо', max_length=100, blank=True)

    phone = models.CharField('Номер телефона', max_length=100, blank=True)
    email = models.EmailField('Email контактного лица', max_length=100, blank=True)

    reason = models.TextField('Основание', blank=True, null=True)
    contract_number = models.CharField('Номер договора', max_length=255, blank=True)
    contract_date = models.DateField('Дата договора', blank=True, null=True)
    files = models.ManyToManyField(ClientFile, verbose_name='Файлы', blank=True)

    # Поля партнера
    can_set_white_category = models.BooleanField('Может устанавливать первую категорию', default=True)
    order_b2c_currency = models.CharField('Валюта', choices=CURRENCY_CHOICES, default=CURRENCY_CHOICES.RUR, max_length=3)
    banner_size = models.CharField('Размер баннера', max_length=255, blank=True)
    banner_type = models.CharField('Тип баннера', max_length=255, blank=True)
    contacts_administrations = models.TextField('Контакты администрации ТЦ', blank=True)
    contacts_advertising = models.TextField('Контакты реклама', blank=True)
    contacts_construction = models.TextField('Контакты стройка', blank=True)
    contacts_video = models.TextField('Контакты видео', blank=True)
    contacts_music = models.TextField('Контакты музыка', blank=True)
    contacts_anti_theft = models.TextField('Контакты антикражная система', blank=True)
    series_count = models.PositiveSmallIntegerField('Количество серий', default=1,
                                                    validators=[
                                                        MaxValueValidator(99),
                                                        MinValueValidator(1)
                                                    ])

    # Поля поставщика

    service = models.CharField('Услуга', max_length=500, blank=True)
    head = models.CharField('Руководитель', max_length=255, blank=True)
    accountant_general = models.CharField('Главный бухгалтер', max_length=255, blank=True)

    old_provider_id = models.PositiveSmallIntegerField('Старый id провайдера', null=True, blank=True)

    weight = models.PositiveSmallIntegerField('Вес', default=0, help_text='Используется для сортировки')

    defect_act_expired_days = models.PositiveSmallIntegerField(
        'Количество дней, в течении которых возможно оформить акт на возврат', default=20)

    objects = ClientManager()

    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'
        ordering = ('-weight', 'name', )

    def __str__(self):
        return 'id:{}, {} ({})'.format(self.id, self.name, self.inn)

    def get_person_name(self):
        """Используется при печати документов для генерации блока с подписями сторон"""
        if self.legal_form == self.LEGAL_FORM.ip:
            if self.name.startswith('ИП'):
                return self.name[2:].strip()
            else:
                return self.name
        return '____________________'

    def get_full_legal_address(self):
        data = OrderedDict([
            ('legal_index', ''),
            ('legal_region', ''),
            ('legal_district', ''),
            ('legal_city', 'г.'),
            ('legal_street', 'ул.'),
            ('legal_house', 'д.'),
            ('legal_building', 'корп.'),
            ('legal_construction', 'стр.'),
            ('legal_flat', 'кв.'),
        ])
        raw_address = ['{}{}'.format(data[item], getattr(self, item)) for item in data.keys() if getattr(self, item)]
        return ', '.join(raw_address)


class ClientStatistics(models.Model):
    client = models.ForeignKey(Client, verbose_name='Партнер', on_delete=models.PROTECT)
    date = models.DateField('Дата')
    visitors_count = models.PositiveIntegerField('Количество посетителей', default=0)
    sales_amount = models.PositiveIntegerField('Сумма продаж', default=0)

    class Meta:
        verbose_name = 'Статистика по клиентам'
        verbose_name_plural = 'Статистика по клиентам'
        ordering = ('-date', 'client')
        unique_together = ('client', 'date')

    def __str__(self):
        return '{}, {}'.format(self.date, self.client)

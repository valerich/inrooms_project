from autocomplete_light import shortcuts as autocomplete_light
from django import forms


class ClientStatisticsAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'client': autocomplete_light.ChoiceWidget('ClientPartnerAutocomplete'),
        }

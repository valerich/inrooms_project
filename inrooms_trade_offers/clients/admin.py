from django.contrib import admin

from config.celery import app
from .forms import ClientStatisticsAdminForm
from .models import Client, ClientStatistics


class ClientAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'kind')
    list_filter = ('kind', )
    search_fields = ('name', '=id', )

    actions = ('send_to_1c_action',)

    def send_to_1c_action(self, request, queryset):
        product_ids = list(queryset.values_list('id', flat=True))
        app.send_task('service_1c.notify_1c_client_change', args=(product_ids,))
        self.message_user(request, 'Отправлено на выгрузку в 1с')

    send_to_1c_action.short_description = 'Выгрузить в 1с'

    def get_queryset(self, request):
        qs = Client.all_objects.get_queryset()
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs


class ClientStatisticsAdmin(admin.ModelAdmin):
    list_display = ('client', 'date', 'visitors_count', 'sales_amount')
    list_filter = ('client', )
    form = ClientStatisticsAdminForm


for model_or_iterable, admin_class in (
    (Client, ClientAdmin),
    (ClientStatistics, ClientStatisticsAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

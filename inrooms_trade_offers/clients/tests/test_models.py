from django.db.utils import IntegrityError
from django.test import TestCase

from ..models import Client


class ClientsModelsTestCase(TestCase):
    def setUp(self):
        Client.objects.all().delete()

    def test_client_inn_unique(self):
        """Поле "ИНН" у клиентов уникально"""

        Client.objects.create(name='qwe', inn='Партнер 1')
        with self.assertRaises(IntegrityError):
            Client.objects.create(name='qwee', inn='Партнер 1')

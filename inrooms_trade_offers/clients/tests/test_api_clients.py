from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from ..factories import ClientFactory
from ..models import Client


class ClientViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(ClientViewTestCase, cls).setUpClass()
        cls.objects_list_url = reverse('api:clients:client-list')
        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        Client.objects.all().delete()
        for i in range(10):
            ClientFactory()

    def setUp(self):
        self.client = APIClient()
        self.client.login(email='admin@example.com', password='password')
        pass

    def tearDown(self):
        self.client.session.clear()

    def test_list_factory(self):
        response = self.client.get(self.objects_list_url)
        self.assertEqual(response.data['count'], 10)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client.get(self.objects_list_url)
        factory_dict = response.data['results'][0]
        fields = [
            'id',
            'name',
            'inn',
            'kpp',
            'legal_index',
            'legal_region',
            'legal_district',
            'legal_city',
            'legal_street',
            'legal_house',
            'legal_building',
            'legal_flat',
            'legal_construction',
            'actual_index',
            'actual_region',
            'actual_district',
            'actual_city',
            'actual_street',
            'actual_house',
            'actual_building',
            'actual_flat',
            'actual_construction',
            'manager',
            'contact_person',
            'phone',
            'email',
            'series_count',
        ]
        for field in fields:
            self.assertIn(field, factory_dict, 'Не передается поле {} в {}'.format(field, self.objects_list_url))

    def test_create(self):
        response = self.client.post(self.objects_list_url, data={'name': '1234', 'inn': '1234'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

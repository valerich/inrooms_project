from django.test import TestCase

from ..factories import ClientFactory, ClientStatisticsFactory
from ..models import Client, ClientStatistics


class ClientsFactoryTestCase(TestCase):
    def setUp(self):
        Client.objects.all().delete()

    def test_factory_factory(self):
        client = ClientFactory(inn='Партнер 1')
        client_from_db = Client.objects.get(inn='Партнер 1')
        self.assertEqual(client, client_from_db)

    def test_multiple_factories_factory(self):
        clients = []
        for i in range(10):
            clients.append(ClientFactory())

        self.assertEqual(len(clients), 10)
        self.assertEqual(len(clients), len(Client.objects.all()))


class ClientStatisticsFactoryTestCase(TestCase):
    def setUp(self):
        ClientStatistics.objects.all().delete()

    def test_factory(self):
        client = ClientStatisticsFactory()
        client_from_db = ClientStatistics.objects.get(id=client.id)
        self.assertEqual(client, client_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(ClientStatisticsFactory())

        self.assertEqual(len(objects), 10)
        self.assertEqual(len(objects), len(ClientStatistics.objects.all()))

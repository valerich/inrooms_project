import datetime

from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from ..factories import ClientFactory, ClientStatisticsFactory
from ..models import ClientStatistics


class ClientStatisticsViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.objects_list_url = reverse('api:clients:client_statistics-list')
        UsersSetup()
        cls._client = ClientFactory(name='Название клиента')
        cls._client2 = ClientFactory(name='Название клиента2')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_cl = User.objects.get(email='client@example.com')
        cls.user_cl.client = cls._client
        cls.user_cl.save()
        ClientStatistics.objects.all().delete()

        cls.client_statistic = ClientStatisticsFactory(
            client=cls._client,
            date=datetime.date(2016, 3, 24),
            visitors_count=5,
            sales_amount=10,
        )

    def setUp(self):
        self.client_pa = APIClient()
        self.client_pa.login(email=self.user_pa.email, password='password')
        self.client_bm = APIClient()
        self.client_bm.login(email=self.user_bm.email, password='password')
        self.client_cl = APIClient()
        self.client_cl.login(email=self.user_cl.email, password='password')
        pass

    def tearDown(self):
        self.client_pa.session.clear()
        self.client_bm.session.clear()
        self.client_cl.session.clear()

    def test_list(self):
        for i in range(10):
            ClientStatisticsFactory()

        # Есть право просматривать все отчеты клиентов
        response = self.client_pa.get(self.objects_list_url)
        self.assertEqual(response.data['count'], 11)

        # Нет права просматривать все отчеты клиентов
        response = self.client_bm.get(self.objects_list_url)
        self.assertEqual(response.data['count'], 0)

        # Есть право просматривать свои отчеты клиентов
        response = self.client_cl.get(self.objects_list_url)
        self.assertEqual(response.data['count'], 1)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client_cl.get(self.objects_list_url)
        object_as_dict = response.data['results'][0]

        valid_list_item_dict = {
            "id": self.client_statistic.id,
            "date": "24.03.2016",
            "client": self.client_statistic.client_id,
            "client_detail": {
                "id": self.client_statistic.client_id,
                "name": "Название клиента"
            },
            "visitors_count": 5,
            "sales_amount": 10,
        }

        self.assertDictEqual(object_as_dict, valid_list_item_dict)

    def test_create_for_pa(self):
        """Создание отчета пользователем, имеющим права на создание отчетов по любым клиентам
        """

        response = self.client_pa.post(self.objects_list_url, data={
            'date': '01.05.2016',
            'visitors_count': 3,
            'sales_amount': 6,
        })
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_for_pa_with_client_data(self):
        """Создание отчета пользователем, имеющим права на создание отчетов по любым клиентам
        """

        response = self.client_pa.post(self.objects_list_url, data={
            'client': self._client.id,
            'date': '01.05.2016',
            'visitors_count': 3,
            'sales_amount': 6,
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_for_client(self):
        """Создание отчета пользователем без права создавать отчеты на любых клиентов. Не передан client

        В этом случае с отчет добавляется user.client
        """

        response = self.client_cl.post(self.objects_list_url, data={
            'date': '02.03.2016',
            'visitors_count': 3,
            'sales_amount': 6,
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_for_client_with_client_data(self):
        """Создание отчета пользователем без права создавать отчеты на любых клиентов. client передан

        Если клиент пользователя отличается от переданного, то вернуть 403
        """

        response = self.client_cl.post(self.objects_list_url, data={
            'client': self._client.id,
            'date': '01.04.2016',
            'visitors_count': 3,
            'sales_amount': 6,
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client_cl.post(self.objects_list_url, data={
            'client': self._client2.id,
            'date': '02.04.2016',
            'visitors_count': 3,
            'sales_amount': 6,
        })
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

from autocomplete_light import shortcuts as autocomplete_light

from .models import Client


class ClientAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['=id', 'name', ]
    attrs = {
        'placeholder': u'=id, Название',
        'data-autocomplete-minimum-characters': 0,
    }


class PartnerAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['=id', 'name', ]
    choices = Client.objects.filter(kind=Client.KIND.partner)
    attrs = {
        'placeholder': u'=id, Название',
        'data-autocomplete-minimum-characters': 0,
    }


class ProviderAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['=id', 'name', ]
    choices = Client.objects.filter(kind=Client.KIND.provider)
    attrs = {
        'placeholder': u'=id, Название',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (Client, ClientAutocomplete),
    (Client, PartnerAutocomplete),
    (Client, ProviderAutocomplete),
)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

import telebot
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site


def send_telegram_message(message, url=None):
    if settings.TELEBOT_TOKEN:
        tb = telebot.TeleBot(settings.TELEBOT_TOKEN)
        if url:
            if not url.startswith('http'):
                request = None
                url = ''.join(['http://', get_current_site(request).domain, url])
            message = '{}\n<a href="{}">link</a>'.format(message, url)
        try:
            tb.send_message(
                settings.TELEGRAM_CHAT_ID,
                message,
                parse_mode='HTML'
            )
        except Exception:
            pass

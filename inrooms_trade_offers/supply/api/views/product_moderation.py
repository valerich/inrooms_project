import django_filters
from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin
from rest_framework.viewsets import GenericViewSet

from products.models import Product
from ..serializers import ProductModerationSerializer
from supply.models import Supply


class ProductFilterSet(django_filters.FilterSet):

    class Meta:
        model = Product
        fields = [
            'is_active',
        ]


class ProductModerationViewSet(RetrieveModelMixin,
                               UpdateModelMixin,
                               ListModelMixin,
                               GenericViewSet):

    serializer_class = ProductModerationSerializer
    filter_class = ProductFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter, )
    search_fields = ('=article', 'name', '=id')
    queryset = Product.objects.order_by('-modified')
    ordering_fields = (
        'id',
        'name',
        'article',
        'created',
        'main_collection__name',
        'modified',
        'price',
    )

    def dispatch(self, request, supply_pk: int, *args, **kwargs):
        self.supply = get_object_or_404(Supply, pk=supply_pk)
        return super(ProductModerationViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        product_ids = self.supply.items.all().values_list(
            'product_id', flat=True
        ).distinct()

        qs = Product.objects.filter(id__in=product_ids)
        qs = qs.select_related(
            'color',
        )
        qs = qs.prefetch_related(
            'images'
        )
        qs = qs.order_by('-modified')
        return qs

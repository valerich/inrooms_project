from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import DeliveryTypeSerializer
from ...models import DeliveryType


class DeliveryTypeViewSet(mixins.RetrieveModelMixin,
                          mixins.ListModelMixin,
                          GenericViewSet):
    serializer_class = DeliveryTypeSerializer
    model = DeliveryType
    queryset = DeliveryType.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )

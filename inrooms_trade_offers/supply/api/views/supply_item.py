from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import SupplyItemSerializer
from ...models import Supply, SupplyItem


class SupplyItemViewSet(ModelViewSet):
    serializer_class = SupplyItemSerializer
    model = SupplyItem
    queryset = SupplyItem.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('^product__article', 'product__name')
    ordering_fields = (
        'product__article',
        'product__name',
    )

    def dispatch(self, request, supply_pk: int, *args, **kwargs):
        self.supply = get_object_or_404(Supply, pk=supply_pk)
        return super(SupplyItemViewSet, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(supply=self.supply)

    def get_queryset(self):
        qs = super(SupplyItemViewSet, self).get_queryset()
        qs = qs.filter(supply=self.supply)
        return qs

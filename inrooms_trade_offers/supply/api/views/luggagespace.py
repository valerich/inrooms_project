from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import LuggageSpaceSerializer
from ...models import Supply, LuggageSpace


class LuggageSpaceViewSet(ModelViewSet):
    serializer_class = LuggageSpaceSerializer
    queryset = LuggageSpace.objects.all()
    ordering_fields = (
        'id',
        'count',
        'weight',
    )

    def dispatch(self, request, supply_pk: int, *args, **kwargs):
        self.supply = get_object_or_404(Supply, pk=supply_pk)
        return super(LuggageSpaceViewSet, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        instance = serializer.save(supply=self.supply)
        return instance

    def get_queryset(self):
        qs = super(LuggageSpaceViewSet, self).get_queryset()
        qs = qs.filter(supply=self.supply)
        return qs

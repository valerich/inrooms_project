from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import SupplyStatusSerializer
from ...models import SupplyStatus


class SupplyStatusViewSet(mixins.RetrieveModelMixin,
                          mixins.ListModelMixin,
                          GenericViewSet):
    serializer_class = SupplyStatusSerializer
    queryset = SupplyStatus.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )

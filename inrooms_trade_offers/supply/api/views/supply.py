import django_filters
from django.db import transaction
from django.db.models import F
from django.db.models.aggregates import Sum
from rest_framework import filters, status
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from extensions.drf.views import MultiSerializerViewSetMixin
from payments.models import PaymentDocument, ExpenseItem
from supply.api.serializers.supply import SupplyListSerializer
from ..serializers import SupplySerializer
from ...models import Supply, SupplyItem, SupplyStatus


def filter_is_overdue(queryset, value):
    if value:
        queryset = queryset.overdue()
    else:
        queryset = queryset.not_overdue()
    return queryset


def filter_to_set_prices(queryset, value):
    if value:
        queryset = queryset.to_set_prices()
    return queryset


class SupplyFilterSet(django_filters.FilterSet):
    is_overdue = django_filters.BooleanFilter(action=filter_is_overdue)
    to_set_prices = django_filters.BooleanFilter(action=filter_to_set_prices)

    class Meta:
        model = Supply
        fields = [
            'is_overdue',
            'to_set_prices',
            'creator',
            'performer',
            'items__product',
            'status',
            'created',
            'modified',
            'delivery_warehouse_to',
        ]


class SupplyViewSet(MultiSerializerViewSetMixin, ModelViewSet):
    serializers = {
        'default': SupplySerializer,
        'list': SupplyListSerializer,
        'create': SupplySerializer,
        'retrieve': SupplySerializer,
    }
    model = Supply
    filter_class = SupplyFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', '^items__product__article', 'items__product__name')
    ordering_fields = (
        'id',
        'created',
        'modified',
    )

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)

    def get_queryset(self):
        if self.action == 'list':
            qs = Supply.objects.all().select_related(
                'delivery_warehouse_to',
                'status',
            )
            qs = qs.prefetch_related(
                'luggagespace_set',
                'items',
                'items__product',
                'items__product__images',
            )
        else:
            qs = Supply.objects.all().select_related(
                'payment_method',
                'delivery_type',
                'delivery_warehouse_to',
                'delivery_warehouse_from',
                'status',
            )
            qs = qs.prefetch_related(
                'luggagespace_set',
            )
        return qs

    @detail_route(methods=['post'])
    @transaction.atomic
    def change_status(self, request, pk=None):
        supply = self.get_object()
        status_code = request.data.get('code', None)
        if status_code:
            try:
                new_status = SupplyStatus.objects.get(code=status_code)
            except SupplyStatus.DoesNotExist:
                pass
            else:
                psw = 'auth.can_supply_set_'
                approved_status_codes = [
                    perm[len(psw):] for perm in request.user.get_all_permissions() if perm.startswith(psw)]
                if status_code in approved_status_codes:
                    # if new_status.code == 'sent_to_warehouse':
                    #     if supply.items.filter(product__is_active=False).exists():
                    #         return Response({'error': 'Не все товары прошли модерацию'},
                    #                         status=status.HTTP_400_BAD_REQUEST)
                    if new_status.code == 'delivery_waiting':
                        if not supply.payment_account:
                            return Response({'error': 'Не заполнено поле "Счет"'},
                                            status=status.HTTP_400_BAD_REQUEST)
                        supply.all_amount_paid_rur = supply.amount_paid_rur
                        if supply.amount_paid:
                            if supply.payment_account.balance < supply.amount_paid:
                                return Response({'error': 'Не достаточно средств на счете'},
                                            status=status.HTTP_400_BAD_REQUEST)
                            amount_paid_to_rur = supply.payment_account.get_amount_rur(supply.amount_paid)
                            supply.all_amount_paid_rur += amount_paid_to_rur

                            expense_item = ExpenseItem.get_supply_delivery_payment()

                            payment_document = PaymentDocument(
                                kind=PaymentDocument.KIND.debit,
                                payment_account=supply.payment_account,
                                destination_payment_account=supply.payment_account,
                                amount=supply.amount_paid,
                                amount_rur=amount_paid_to_rur,
                                user=self.request.user,
                                description="Оплата доставки №{}".format(supply.id),
                                content_object=supply,
                                expense_item=expense_item,
                            )
                            payment_document.save()
                            payment_document.process()
                    supply.status = new_status
                    supply.save()
                    supply.process_supply()
                    return Response(status=status.HTTP_200_OK)
        return Response({'error': 'Не верный статус'},
                        status=status.HTTP_400_BAD_REQUEST)

    @list_route
    def summary(self, request):
        data = {}
        qs = Supply.objects.all()
        data['to_set_prices'] = qs.to_set_prices().count()
        return Response(data)

    @list_route(methods=['get'])
    def info(self, request):
        """
        Информация о всех перемещениях
        """
        supply_items_avgs = list()

        supply_qs = SupplyItem.objects.filter(supply__kind=Supply.KIND.movement)

        # расчитать среднюю наценку
        for old_price, price in supply_qs.values_list('product__price', 'price'):
            value = ((price - old_price) * 100) / price if price and old_price else 0.0
            supply_items_avgs.append(value)

        average_margin = sum(supply_items_avgs) / len(supply_items_avgs) if supply_items_avgs else 'Неизвестно'

        # общая себестоимость и количество товаров
        aggregate_data = supply_qs.aggregate(
            amount_price=Sum('price'),
            count=Sum(
                F('count_xxs') +
                F('count_xs') +
                F('count_s') +
                F('count_m') +
                F('count_l') +
                F('count_xl') +
                F('count_xxl') +
                F('count_xxxl') +
                F('count_34') +
                F('count_35') +
                F('count_36') +
                F('count_37') +
                F('count_38') +
                F('count_39') +
                F('count_40') +
                F('count_41')
            )
        )

        data = {
            'count': aggregate_data['count'],  # количество
            'cost': aggregate_data['amount_price'],  # себестоимость
            'average_margin': average_margin,  # средняя наценка
        }

        return Response(data)

    @detail_route(methods=['get'])
    def detail_info(self, request, pk=None):
        """
        Информация о всех перемещениях
        """
        obj = self.get_object()
        supply_items_avgs = list()

        average_margins = []

        for i in obj.items.all():
            new_price = i.price
            quantity = i.get_quantity()
            if new_price is not None:
                average_margins.append(new_price * quantity)

        supply_qs = SupplyItem.objects.filter(supply_id=obj.id)

        aggregate_data = supply_qs.aggregate(
            count=Sum(
                F('count_xxs') +
                F('count_xs') +
                F('count_s') +
                F('count_m') +
                F('count_l') +
                F('count_xl') +
                F('count_xxl') +
                F('count_xxxl') +
                F('count_34') +
                F('count_35') +
                F('count_36') +
                F('count_37') +
                F('count_38') +
                F('count_39') +
                F('count_40') +
                F('count_41')
            )
        )

        cost = obj.get_cost_price()
        new_price = sum(average_margins)

        data = {
            'count': aggregate_data['count'],  # количество
            'cost': obj.get_cost_price(), # aggregate_data['amount_price'],  # себестоимость
            'average_margin': ((new_price - cost) / cost) * 100 if cost else 0,  # средняя наценка
        }

        return Response(data)

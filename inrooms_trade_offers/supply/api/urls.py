from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import (
    DeliveryTypeViewSet,
    LuggageSpaceViewSet,
    ProductModerationViewSet,
    SupplyItemViewSet,
    SupplyStatusViewSet,
    SupplyViewSet,
)

router = DefaultRouter()
router.register(r'delivery_type', DeliveryTypeViewSet)
router.register(r'supply', SupplyViewSet, base_name='supply')
router.register(r'supply/(?P<supply_pk>\d+)/items', SupplyItemViewSet, base_name='supply_items'),
router.register(r'supply/(?P<supply_pk>\d+)/luggage_spaces', LuggageSpaceViewSet, base_name='supply_luggagespaces'),
router.register(r'supply/(?P<supply_pk>\d+)/product_moderation', ProductModerationViewSet, base_name='product_moderation'),
router.register(r'status', SupplyStatusViewSet)


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
]

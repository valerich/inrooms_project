from rest_framework import serializers

from ...models import DeliveryType


class DeliveryTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = DeliveryType

        fields = [
            'id',
            'name',
        ]

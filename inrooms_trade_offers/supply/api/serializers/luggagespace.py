from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import LuggageSpace


class LuggageSpaceSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = LuggageSpace
        fields = [
            'id',
            'count',
            'weight',
        ]

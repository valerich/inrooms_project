from rest_framework import serializers

from payments.api.serializers.payment_method import PaymentMethodSerializer
from supply.api.serializers.delivery_type import DeliveryTypeSerializer
from supply.api.serializers.supply_item import SupplyItemListSerializer
from .supply_item import SupplyItemSerializer
from ...models import Supply, SupplyStatus


class SupplySerializer(serializers.ModelSerializer):
    status_detail = serializers.SerializerMethodField()
    prices_assigned = serializers.SerializerMethodField()
    delivery_warehouse_from_detail = serializers.SerializerMethodField()
    delivery_warehouse_to_detail = serializers.SerializerMethodField()
    delivery_type_detail = serializers.SerializerMethodField()
    payment_method_detail = serializers.SerializerMethodField()
    has_non_moderated_products = serializers.SerializerMethodField()

    class Meta:
        model = Supply

        fields = [
            'id',
            'status_detail',
            'created',
            'modified',
            'description',
            'payment_account',
            'amount_full',
            'payment_method',
            'payment_method_detail',
            'amount_paid',
            'amount_paid_rur',
            'prices_assigned',
            'delivery_number',
            'delivery_type',
            'delivery_type_detail',
            'delivery_date',
            'delivery_amount_full',
            'delivery_warehouse_from_detail',
            'delivery_warehouse_to_detail',
            'delivery_warehouse_to',
            'number_1c',
            'has_non_moderated_products',
        ]

    def get_status_detail(self, obj):
        psw = 'auth.can_supply_set_'
        work_status_codes = [
            perm[len(psw):] for perm in self.context['request'].user.get_all_permissions() if perm.startswith(psw)]
        user_status_ids = SupplyStatus.objects.filter(code__in=work_status_codes).values_list('id', flat=True)
        return {'id': obj.status_id,
                'name': obj.status.name,
                'code': obj.status.code,
                'next_statuses': [{
                    'id': s.id,
                    'name': s.name,
                    'button_name': s.button_name,
                    'code': s.code,
                } for s in obj.status.next_status.filter(id__in=user_status_ids)]}

    def get_prices_assigned(self, obj):
        for item in obj.items.all():
            if not item.price:
                return False
        return True

    def get_delivery_warehouse_from_detail(self, obj):
        if obj.delivery_warehouse_from:
            return {
                'id': obj.delivery_warehouse_from.id,
                'name': obj.delivery_warehouse_from.name,
                'code': obj.delivery_warehouse_from.code,
            }
        else:
            return None

    def get_delivery_warehouse_to_detail(self, obj):
        data = {
            'id': obj.delivery_warehouse_to.id,
            'name': obj.delivery_warehouse_to.name,
            'code': obj.delivery_warehouse_to.code,
            'brand_detail': None,
        }
        if obj.delivery_warehouse_to.brand:
            data['brand_detail'] = {
                'id': obj.delivery_warehouse_to.brand_id,
            }
        return data

    def get_delivery_type_detail(self, obj):
        if obj.delivery_type:
            return DeliveryTypeSerializer(obj.delivery_type).data
        else:
            return None

    def get_payment_method_detail(self, obj):
        if obj.payment_method:
            return PaymentMethodSerializer(obj.payment_method).data
        else:
            return None

    def get_has_non_moderated_products(self, obj):
        return obj.items.filter(product__is_active=False).exists()


class SupplyListSerializer(serializers.ModelSerializer):
    status_detail = serializers.SerializerMethodField()
    items = SupplyItemListSerializer(many=True, read_only=True)
    delivery_warehouse_to_detail = serializers.SerializerMethodField()

    class Meta:
        model = Supply

        fields = [
            'id',
            'status_detail',
            'created',
            'modified',
            'amount_full',
            'items',
            'delivery_date',
            'delivery_warehouse_to_detail',
            'number_1c',
        ]

    def get_status_detail(self, obj):
        return {'id': obj.status_id,
                'name': obj.status.name,
                'code': obj.status.code,
                }

    def get_delivery_warehouse_to_detail(self, obj):
        data = {
            'id': obj.delivery_warehouse_to.id,
            'name': obj.delivery_warehouse_to.name,
            'code': obj.delivery_warehouse_to.code,
            'brand_detail': None,
        }
        if obj.delivery_warehouse_to.brand:
            data['brand_detail'] = {
                'id': obj.delivery_warehouse_to.brand_id,
            }
        return data

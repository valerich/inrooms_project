from django.db.utils import IntegrityError
from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from products.models import Product

class ProductModerationSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()

    class Meta:
        model = Product

        fields = [
            'id',
            'article',
            'name',
            'image',
            'images',
            'price',
            'color',
            'main_collection',
            'seasonality',
            'is_active',
        ]

    def get_image(self, obj):
        pi = obj.images.all().first()
        return self.resize_image(pi, obj)

    def get_images(self, obj):
        data = []
        for image in obj.images.all():
            item = self.resize_image(image, obj)
            if item:
                data.append(item)
        return data

    def resize_image(self, pi, obj):
        if pi:
            try:
                _200x200 = get_thumbnail(pi.image, '400x400', crop="center").url
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
            except IOError as e:
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1500 = request.build_absolute_uri(_950x1200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,
                    '950x1200': _950x1200}
        else:
            return None


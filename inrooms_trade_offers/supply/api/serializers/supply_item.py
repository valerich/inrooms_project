from decimal import Decimal

from django.db.models.aggregates import Sum
from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from products.api.serializers import ProductSerializer
from products.models import Remain
from ...models import SupplyItem


class SupplyItemSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    series_type = serializers.CharField(required=False)
    product_detail = serializers.SerializerMethodField()
    cost_price = serializers.SerializerMethodField()

    class Meta:
        model = SupplyItem
        fields = [
            'id',
            'price',
            'product',
            'product_detail',
            'series_type',
            'count_xxs',
            'count_xs',
            'count_s',
            'count_m',
            'count_l',
            'count_xl',
            'count_xxl',
            'count_xxxl',
            'count_34',
            'count_35',
            'count_36',
            'count_37',
            'count_38',
            'count_39',
            'count_40',
            'count_41',
            'cost_price',
        ]

    def get_product_detail(self, obj):
        if obj.product_id:
            product = ProductSerializer(obj.product).data
            remain_data = Remain.objects.filter(
                product=obj.product,
                warehouse=obj.supply.delivery_warehouse_from
            ).aggregate(Sum('value'), Sum('amount_rur'))
            remain = remain_data['value__sum']
            amount_rur = remain_data['amount_rur__sum']
            if remain:
                product['avg_cost'] = amount_rur / remain
            else:
                product['avg_cost'] = Decimal('0.00')
            return product
        else:
            return None

    def get_cost_price(self, obj):
        return obj.get_cost_price()


class SupplyItemListSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    product_detail = serializers.SerializerMethodField()

    class Meta:
        model = SupplyItem
        fields = [
            'id',
            'product_detail',
        ]

    def get_product_detail(self, obj):
        if obj.product_id:
            product = ProductSerializer(obj.product, fields=['id', 'article', 'name', 'image']).data
            return product
        else:
            return None

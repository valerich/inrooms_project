import factory
import factory.django
import factory.fuzzy

from accounts.factories import UserFactory
from payments.factories import PaymentMethodFactory
from products.factories import ProductFactory
from .models import Supply, SupplyItem, SupplyStatus


class SupplyStatusFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    code = factory.fuzzy.FuzzyText()

    class Meta:
        model = SupplyStatus


class SupplyFactory(factory.django.DjangoModelFactory):
    status = factory.SubFactory(SupplyStatusFactory)
    payment_method = factory.SubFactory(PaymentMethodFactory)
    creator = factory.SubFactory(UserFactory)
    performer = factory.SubFactory(UserFactory)

    class Meta:
        model = Supply


class SupplyItemFactory(factory.django.DjangoModelFactory):
    supply = factory.SubFactory(SupplyFactory)
    product = factory.SubFactory(ProductFactory)

    class Meta:
        model = SupplyItem

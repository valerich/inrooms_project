from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'supply'
    verbose_name = 'Поставки'

import datetime

from django.test import TestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from ..factories import SupplyFactory
from ..models import Supply, SupplyStatus


class SupplyManagerTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(SupplyManagerTestCase, cls).setUpClass()

        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')

    def test_overdue(self):
        """ Просроченные поставки

        Это поставки, контрольная дата доставки которых меньше чем текущая и статус не конечный
        """

        overdue_date = datetime.date.today() - datetime.timedelta(days=1)
        status = SupplyStatus.objects.filter(next_status__isnull=False)[0]
        SupplyFactory(creator=self.user_bm, delivery_date=overdue_date, status=status)
        SupplyFactory(creator=self.user_pa, delivery_date=overdue_date, status=status)

        # Без указания пользователя результат - все не выполненные поставки
        self.assertEqual(Supply.objects.all().overdue().count(), 2)

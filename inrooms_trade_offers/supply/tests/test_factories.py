from django.test import TestCase

from ..factories import SupplyFactory, SupplyItemFactory, SupplyStatusFactory
from ..models import Supply, SupplyItem, SupplyStatus


class SupplyFactoryTestCase(TestCase):
    def setUp(self):
        Supply.objects.all().delete()

    def test_factory(self):
        object = SupplyFactory()
        object_from_db = Supply.objects.all()[0]
        self.assertEqual(object, object_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(SupplyFactory())

        self.assertEqual(len(objects), len(Supply.objects.all()))


class SupplyItemFactoryTestCase(TestCase):
    def setUp(self):
        SupplyItem.objects.all().delete()

    def test_factory(self):
        object = SupplyItemFactory()
        object_from_db = SupplyItem.objects.all()[0]
        self.assertEqual(object, object_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(SupplyItemFactory())

        self.assertEqual(len(objects), len(SupplyItem.objects.all()))


class SupplyStatusFactoryTestCase(TestCase):
    def setUp(self):
        SupplyStatus.objects.all().delete()

    def test_factory(self):
        object = SupplyStatusFactory()
        object_from_db = SupplyStatus.objects.all()[0]
        self.assertEqual(object, object_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(SupplyStatusFactory())

        self.assertEqual(len(objects), len(SupplyStatus.objects.all()))

from collections import defaultdict


class SupplyHelper(object):

    def __init__(self, supply_ids=None, supply_statuses=None, product_ids=None, order_ids=None):
        self.supply_ids = supply_ids
        self.order_ids = order_ids
        self.supply_statuses = supply_statuses
        self.product_ids = product_ids
        self.SIZE_CODES = ['xxs',
                           'xs',
                           's',
                           'l',
                           'm',
                           'xl',
                           'xxl',
                           'xxxl',
                           '34',
                           '35',
                           '36',
                           '37',
                           '38',
                           '39',
                           '40',
                           '41', ]

    def _get_supply_qs(self):
        from supply.models import Supply
        qs = Supply.objects.all()
        if self.supply_ids:
            qs = qs.filter(id__in=self.supply_ids)
        if self.supply_statuses:
            qs = qs.filter(status__code__in=self.supply_statuses)
        return qs

    def get_products_by_size(self, without_order_reserve=True):
        """
        Возвращает количество товаров в поставках по размерам
        :return {product_id: {size_code: value}}
        """
        from supply.models import SupplyItem
        from orders.models.order import OrderItemSize

        data = defaultdict(lambda: defaultdict(int))

        qs = SupplyItem.objects.filter(supply__in=self._get_supply_qs())
        if self.product_ids:
            qs = qs.filter(product_id__in=self.product_ids)
        for supply_item in qs:
            for size_code in self.SIZE_CODES:
                data[supply_item.product_id][size_code] += getattr(supply_item, 'count_{}'.format(size_code), 0)
        if without_order_reserve:
            order_qs = OrderItemSize.objects.filter(
                is_delivery_reserve=True,
                warehouse__code='msk',
            ).exclude(order_item__order__status__code='new')
            if self.product_ids:
                order_qs = order_qs.filter(order_item__product_id__in=self.product_ids)
            if self.order_ids:
                order_qs = order_qs.exclude(order_item__order_id__in=self.order_ids)
            for ois in order_qs:
                data[ois.order_item.product_id][ois.size.code] -= ois.quantity
        return data

    def get_flat_products(self):
        """
        Возвращает количество товара в поставках
        :return {product_id: value}
        """

        products_by_size = self.get_products_by_size()

        return {product_id: sum(size_data.values())
                for product_id, size_data in products_by_size.items()}

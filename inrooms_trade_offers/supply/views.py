import os
import tempfile
import openpyxl
from openpyxl.styles import Alignment
from django.http.response import HttpResponse
from rest_framework.views import APIView

from core.utils.excel import NewExcelHelper
from supply.models import Supply


class SupplyExport(APIView):

    def get(self, request, object_id, *args, **kwargs):
        supply = Supply.objects.get(id=object_id)
        xls_helper = Report(supply=supply).get_data()

        fd, fn = tempfile.mkstemp()
        os.close(fd)
        xls_helper.book.save(fn)
        fh = open(fn, 'rb')
        resp = fh.read()
        fh.close()
        response = HttpResponse(resp, content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=report.xlsx'
        return response


class Report(object):

    def __init__(self, supply):
        self.supply = supply

    def get_data(self):
        return self.get_xls_helper()

    def get_xls_helper(self):
        xls_helper = NewExcelHelper()
        xls_helper.add_page('Товары')

        xls_helper.write_cell('НАКЛАДНАЯ №{}'.format(self.supply.id), 0, 0)
        xls_helper.write_cell('от {}г.'.format(self.supply.created.strftime('%s.%m.%Y')), 1, 0)
        xls_helper.write_cell('Пункт отправления', 2, 0)
        xls_helper.write_cell('Отправитель / Код клиента', 3, 0)
        xls_helper.write_cell('Получатель', 4, 0)

        xls_helper.write_cell('Мешок', 2, 23)
        xls_helper.write_cell('неупакованный или нерасфасованный', 3, 23)
        xls_helper.write_cell('Мешок из полимерной пленки', 4, 23)
        xls_helper.write_cell('Коробка, картонная', 5, 23)
        xls_helper.write_cell('Пакет', 6, 23)
        xls_helper.write_cell('Ящик деревянный', 7, 23)

        xls_helper.write_cell('мешок из полимерной ткани', 6, 25)
        xls_helper.write_cell('Коробка, фанерная', 7, 25)

        xls_helper.write_cell('№ п\п', 9, 0)
        xls_helper.write_cell('Наименование', 9, 1)
        xls_helper.write_cell('детская', 9, 2)
        xls_helper.write_cell('Материал изготовления', 9, 3)
        xls_helper.write_cell('Код ТН ВЭД', 9, 4)
        xls_helper.write_cell('Номер места', 9, 5)
        xls_helper.write_cell('Арт.', 9, 6)
        xls_helper.write_cell('Модель', 9, 7)
        xls_helper.write_cell('Страна происхождения', 9, 8)
        xls_helper.write_cell('Завод изготовитель', 9, 9)
        xls_helper.write_cell('Торговая марка', 9, 10)
        xls_helper.write_cell('Доп. характеристики товара(размер)', 9, 11)
        xls_helper.write_cell('Количество мест', 9, 12)
        xls_helper.write_cell('Количество в месте (доп.ед.изм)', 9, 13)
        xls_helper.write_cell('Доп. единица измерения (пара, кг, шт., рулон…)', 9, 14)
        xls_helper.write_cell('Итого (доп.ед.изм)', 9, 15)
        xls_helper.write_cell('Вес места, кг', 9, 16)
        xls_helper.write_cell('', 9, 17)
        xls_helper.write_cell('Итого вес, кг', 9, 18)
        xls_helper.write_cell('', 9, 19)
        xls_helper.write_cell('Размеры места, см.', 9, 20)
        xls_helper.write_cell('', 9, 21)
        xls_helper.write_cell('', 9, 22)
        xls_helper.write_cell('Объем места, м3', 9, 23)
        xls_helper.write_cell('Упаковка', 9, 24)
        xls_helper.write_cell('', 9, 25)
        xls_helper.write_cell('Фото', 9, 26)

        xls_helper.write_cell('брутто', 10, 16)
        xls_helper.write_cell('нетто', 10, 17)
        xls_helper.write_cell('брутто', 10, 18)
        xls_helper.write_cell('нетто', 10, 19)
        xls_helper.write_cell('Д', 10, 20)
        xls_helper.write_cell('В', 10, 21)
        xls_helper.write_cell('Ш', 10, 22)

        xls_helper.write_cell('наружняя', 10, 24)
        xls_helper.write_cell('индив.', 10, 25)

        xls_helper.write_cell('1', 11, 0)
        xls_helper.write_cell('2', 11, 1)
        xls_helper.write_cell('3', 11, 2)
        xls_helper.write_cell('4', 11, 3)
        xls_helper.write_cell('5', 11, 4)
        xls_helper.write_cell('6', 11, 5)
        xls_helper.write_cell('7', 11, 6)
        xls_helper.write_cell('8', 11, 7)
        xls_helper.write_cell('9', 11, 8)
        xls_helper.write_cell('10', 11, 9)
        xls_helper.write_cell('11', 11, 10)
        xls_helper.write_cell('12', 11, 11)
        xls_helper.write_cell('13', 11, 12)
        xls_helper.write_cell('14', 11, 13)
        xls_helper.write_cell('15', 11, 14)
        xls_helper.write_cell('16', 11, 15)
        xls_helper.write_cell('17', 11, 16)
        xls_helper.write_cell('18', 11, 17)
        xls_helper.write_cell('19', 11, 18)
        xls_helper.write_cell('20', 11, 19)
        xls_helper.write_cell('21', 11, 20)
        xls_helper.write_cell('22', 11, 21)
        xls_helper.write_cell('23', 11, 22)
        xls_helper.write_cell('24', 11, 23)
        xls_helper.write_cell('25', 11, 24)
        xls_helper.write_cell('26', 11, 25)
        xls_helper.write_cell('27', 11, 26)

        for i, column_width in (
            ('A', 10),
            ('B', 30),
            ('C', 15),
            ('D', 25),
            ('E', 10),
            ('F', 7),
            ('G', 25),
            ('I', 10),
            ('J', 20),
            ('K', 10),
            ('L', 15),
            ('M', 10),
            ('N', 10),
            ('O', 10),
            ('P', 10),
            ('Q', 10),
            ('R', 10),
            ('S', 10),
            ('T', 10),
            ('U', 10),
            ('V', 10),
            ('W', 10),
            ('X', 15),
            ('Y', 15),
            ('Z', 15),
            ('AA', 300),

        ):
            xls_helper.sheet.column_dimensions[i].width = column_width

        row = 11

        for item in self.supply.items.all():
            row += 1
            xls_helper.sheet.row_dimensions[row + 1].height = 200
            if item.product.intag_weight:
                all_weight = (item.get_quantity() * item.product.intag_weight) / 1000
            else:
                all_weight = 0
            name = ''
            if item.product.tnved_code:
                name += item.product.tnved_code.name
                if item.product.tnved_code.material:
                    name += '. {}'.format(item.product.tnved_code.material.name)
            else:
                name = item.product.name
            xls_helper.write_cell(row - 11, row, 0)
            xls_helper.write_cell(name, row, 1)
            xls_helper.write_cell('', row, 2)
            xls_helper.write_cell(item.product.get_full_consistency(), row, 3)
            xls_helper.write_cell('', row, 4)
            xls_helper.write_cell('{}'.format(self.supply.id), row, 5)
            xls_helper.write_cell(item.product.article, row, 6)
            xls_helper.write_cell('', row, 7)
            xls_helper.write_cell(item.product.country.name if item.product.country else '', row, 8)
            xls_helper.write_cell('', row, 9)
            xls_helper.write_cell(item.product.brand.name, row, 10)
            xls_helper.write_cell('', row, 11)
            xls_helper.write_cell('1', row, 12)
            xls_helper.write_cell(item.get_quantity(), row, 13)
            xls_helper.write_cell('шт', row, 14)
            xls_helper.write_cell(item.get_quantity(), row, 15)
            xls_helper.write_cell('', row, 16)
            xls_helper.write_cell(all_weight, row, 17)
            xls_helper.write_cell('', row, 18)
            xls_helper.write_cell(all_weight, row, 19)
            xls_helper.write_cell('', row, 20)
            xls_helper.write_cell('', row, 21)
            xls_helper.write_cell('', row, 22)
            xls_helper.write_cell('', row, 23)
            xls_helper.write_cell('', row, 24)
            xls_helper.write_cell('', row, 25)
            xls_helper.write_cell('', row, 26)

            image = item.product.main_im
            if image:
                try:
                    img = openpyxl.drawing.image.Image(image.path, size=(None, 200))
                    img.anchor(xls_helper.sheet.cell('AA{}'.format(row + 1)), anchortype='oneCell')
                    xls_helper.sheet.add_image(img)
                except FileNotFoundError:
                    pass

            xls_helper.sheet.merge_cells(
                start_row=1,
                start_column=1,
                end_row=1,
                end_column=27
            )
            xls_helper.sheet.merge_cells(
                start_row=5,
                start_column=1,
                end_row=5,
                end_column=2
            )
            xls_helper.sheet.merge_cells(
                start_row=7,
                start_column=1,
                end_row=7,
                end_column=2
            )

            for col in [
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                24,
                27,
            ]:
                xls_helper.sheet.merge_cells(
                    start_row=10,
                    start_column=col,
                    end_row=11,
                    end_column=col
                )

            xls_helper.sheet.merge_cells(
                start_row=10,
                start_column=17,
                end_row=10,
                end_column=18
            )
            xls_helper.sheet.merge_cells(
                start_row=10,
                start_column=19,
                end_row=10,
                end_column=20
            )
            xls_helper.sheet.merge_cells(
                start_row=10,
                start_column=21,
                end_row=10,
                end_column=23
            )
            xls_helper.sheet.merge_cells(
                start_row=10,
                start_column=25,
                end_row=10,
                end_column=26
            )

        return xls_helper

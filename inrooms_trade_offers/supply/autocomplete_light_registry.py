from autocomplete_light import shortcuts as autocomplete_light

from .models import DeliveryType, Supply, SupplyStatus


class DeliveryTypeAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


class SupplyAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['id', ]
    attrs = {
        'placeholder': 'Номер',
        'data-autocomplete-minimum-characters': 0,
    }


class SupplyStatusAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (DeliveryType, DeliveryTypeAutocomplete),
    (Supply, SupplyAutocomplete),
    (SupplyStatus, SupplyStatusAutocomplete),
)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^supply/(?P<object_id>\d+)/export/$', views.SupplyExport.as_view(), name='supply-export'),
]

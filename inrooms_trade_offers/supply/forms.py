from autocomplete_light import shortcuts as autocomplete_light
from django import forms
from django_summernote.widgets import SummernoteWidget


class SupplyItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'supply': autocomplete_light.ChoiceWidget('SupplyAutocomplete'),
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete')
        }


class SupplyStatusAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'next_status': autocomplete_light.MultipleChoiceWidget('SupplyStatusAutocomplete'),
        }


class SupplyAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'creator': autocomplete_light.ChoiceWidget('UserAutocomplete'),
            'description': SummernoteWidget(),
            'payment_method': autocomplete_light.ChoiceWidget('PaymentMethodAutocomplete'),
            'performer': autocomplete_light.ChoiceWidget('UserAutocomplete'),
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
            'status': autocomplete_light.ChoiceWidget('SupplyStatusAutocomplete'),
            'delivery_type': autocomplete_light.ChoiceWidget('DeliveryTypeAutocomplete'),
            'delivery_warehouse_from': autocomplete_light.ChoiceWidget('WarehouseAutocomplete'),
            'delivery_warehouse_to': autocomplete_light.ChoiceWidget('WarehouseAutocomplete'),
        }

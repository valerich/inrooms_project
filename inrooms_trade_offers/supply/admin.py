from django.contrib import admin

from core.admin import NotDeleteModelAdmin
from .forms import SupplyAdminForm, SupplyItemAdminForm, SupplyStatusAdminForm
from .models import DeliveryType, LuggageSpace, Supply, SupplyItem, SupplyStatus


class LuggageSpaceInline(admin.TabularInline):
    model = LuggageSpace
    extra = 0


class SupplyItemInline(admin.TabularInline):
    model = SupplyItem
    form = SupplyItemAdminForm
    extra = 0


class DeliveryTypeAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )


class SupplyAdmin(NotDeleteModelAdmin):
    list_display = ('id', 'status', 'created', 'modified', )
    search_fields = ('id', )
    list_filter = ('status', )
    form = SupplyAdminForm
    inlines = [LuggageSpaceInline, SupplyItemInline]
    actions = ('send_to_1c_action',)

    def send_to_1c_action(self, request, queryset):
        for supply in queryset:
            supply.create_1c_document()
        self.message_user(request, 'Отправлено на выгрузку в 1с')

    send_to_1c_action.short_description = 'Выгрузить в 1с'


class SupplyStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name', 'weight')
    search_fields = ('name', 'slug',)
    form = SupplyStatusAdminForm


for model_or_iterable, admin_class in (
    (DeliveryType, DeliveryTypeAdmin),
    (Supply, SupplyAdmin),
    (SupplyStatus, SupplyStatusAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

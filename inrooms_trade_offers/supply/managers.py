from django.db import models
from django.utils import timezone


class SupplyManager(models.Manager):

    def get_queryset(self):
        return SupplyQuerySet(self.model)


class SupplyQuerySet(models.query.QuerySet):

    def overdue(self):
        """Просроченные поставки

        Это поставки, контрольная дата доставки которых меньше чем текущая и статус не конечный
        """

        return self.filter(
            delivery_date__lt=timezone.now()
        ).filter(
            status__next_status__isnull=False
        )

    def not_overdue(self):
        """Не просроченные поставки
        """

        return self.exclude(
            delivery_date__lt=timezone.now()
        ).exclude(
            status__next_status__isnull=False
        )

    def to_set_prices(self):
        from supply.models import SupplyStatus
        status, c = SupplyStatus.objects.get_or_create(
            code='set_prices',
            defaults={
                'name': 'Установка цен'
            }
        )
        return self.filter(status=status)

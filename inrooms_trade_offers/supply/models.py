from decimal import Decimal

from django.db import models, transaction
from django_extensions.db.models import TimeStampedModel
from model_utils.choices import Choices

from clients.models import Client
from products.models import SERIES_TYPE_CHOICES, Remain, Size
from warehouses.models import Warehouse
from .managers import SupplyManager


class SupplyStatus(models.Model):
    name = models.CharField('Название', max_length=100, unique=True)
    code = models.CharField('Код', max_length=20, unique=True, db_index=True)
    button_name = models.CharField('Название на кнопке', max_length=100, blank=True)

    create_1c_document = models.BooleanField('Отправлять документ в 1с?', default=False)

    debit_warehouse_from_remains = models.BooleanField('Списать остатки со склада источника?', default=True)
    set_product_price = models.BooleanField('Изменять цены товаров?', default=False)

    next_status = models.ManyToManyField('self', verbose_name=u'Следующие статусы', blank=True, symmetrical=False)
    weight = models.PositiveSmallIntegerField('Вес', default=0)

    class Meta:
        verbose_name = 'Статус'
        verbose_name_plural = 'Статусы'
        ordering = ('-weight', )

    def __str__(self):
        return self.name


class DeliveryType(models.Model):
    name = models.CharField('Тип доставки', max_length=255, unique=True)

    class Meta:
        verbose_name = 'Тип доставки'
        verbose_name_plural = 'Типы доставки'
        ordering = ('name', )

    def __str__(self):
        return self.name


def get_default_warehouse_from():
    return Warehouse.objects.get_or_create(code='chn', defaults={'name': 'Склад Китай'})[0].id


def get_default_warehouse_to():
    return Warehouse.objects.get_or_create(code='msk', defaults={'name': 'Склад Москвы', 'to_orders': True})[0].id


def get_default_status():
    return SupplyStatus.objects.get_or_create(code='new', defaults={'name': 'Новый'})[0].id


SUPPLY_KIND_CHOICE = Choices(
    (10, 'movement', 'Перемещение'),
    (20, 'supply', 'Поставка'),
)


class Supply(TimeStampedModel):
    KIND = SUPPLY_KIND_CHOICE
    number_1c = models.CharField('Номер 1с', max_length=100, blank=True, null=True)
    date_1c = models.DateField('Дата 1с', blank=True, null=True)
    kind = models.PositiveSmallIntegerField('Тип', choices=KIND, default=KIND.movement)
    status = models.ForeignKey(SupplyStatus, verbose_name='Статус', default=get_default_status)
    description = models.TextField('Описание', blank=True)
    payment_account = models.ForeignKey('payments.PaymentAccount', verbose_name='Счет для оплаты', blank=True, null=True)
    payment_method = models.ForeignKey('payments.PaymentMethod', verbose_name='Тип оплаты', blank=True, null=True)
    amount_full = models.DecimalField('Полная стоимость', default=0, max_digits=20, decimal_places=2)
    amount_paid = models.DecimalField('Оплачено (в валюте)', default=Decimal('0.00'), max_digits=20, decimal_places=10)
    amount_paid_rur = models.DecimalField('Оплачено (в рублях)', default=Decimal('0.00'), max_digits=20, decimal_places=10)
    all_amount_paid_rur = models.DecimalField('Сумма оплачено (в рублях)', default=Decimal('0.00'), max_digits=20, decimal_places=10)

    creator = models.ForeignKey('accounts.User', verbose_name='Создатель', related_name='supply_creator')
    performer = models.ForeignKey('accounts.User', verbose_name='Исполнитель', related_name='supply_performer',
                                  blank=True, null=True, )

    # Поля доставки

    delivery_number = models.CharField('Номер поставки', max_length=7, blank=True)
    delivery_type = models.ForeignKey(DeliveryType, verbose_name='Тип доставки', blank=True, null=True)
    delivery_amount_full = models.DecimalField('Стоимость отправки ($)', default=0, max_digits=20, decimal_places=10)
    delivery_date = models.DateField('Дата доставки', blank=True, null=True)
    delivery_warehouse_from = models.ForeignKey(Warehouse, verbose_name='Со склада', related_name='supply_from',
                                                default=get_default_warehouse_from, on_delete=models.PROTECT)
    delivery_warehouse_to = models.ForeignKey(Warehouse, verbose_name='На склад', related_name='supply_to',
                                              default=get_default_warehouse_to, on_delete=models.PROTECT)

    objects = SupplyManager()

    class Meta:
        verbose_name = 'Поставка'
        verbose_name_plural = 'Поставки'
        ordering = ('-id', )
        unique_together = ('number_1c', 'date_1c')

    def __str__(self):
        return '№:{}'.format(self.id)

    def get_delivery_weight(self):
        weight = 0
        for ls in self.luggagespace_set.all():
            weight += ls.weight
        return weight

    def get_quantity(self):
        quantity = 0
        for i in self.items.all():
            quantity += i.get_quantity()
        return quantity

    def get_item_all_amount_paid_rur(self):
        quantity = self.get_quantity()
        if quantity:
            return self.all_amount_paid_rur / quantity
        else:
            return Decimal('0.00')

    def get_cost_price(self):
        item_costs = Decimal('0.00')
        for i in self.items.all():
            item_costs += i.summ_rur
        return self.all_amount_paid_rur + item_costs

    def create_1c_document(self):
        """Создание документа для синхронизации с 1с"""

        from service_1c.utils.supply_exporter import SupplyExporter
        document = SupplyExporter(self.id).create_document()
        return document

    def process_supply(self, create_1c_document=True):
        if self.status.set_product_price:
            for item in self.items.all():
                if not item.set_product_price:
                    with transaction.atomic():
                        item.product.price = item.price
                        item.product.save()
                        item.set_product_price = True
                        item.save()
        if self.status.debit_warehouse_from_remains:
            for item in self.items.all():
                if not item.debit_warehouse_from_remains:
                    with transaction.atomic():
                        amount_rur_summ = Decimal('0.0')
                        for size in Size.objects.all():
                            size_remain = getattr(item, 'count_{}'.format(size.code), 0)
                            if size_remain:
                                value, amount_rur = Remain.remove_remain(
                                    product=item.product,
                                    warehouse=self.delivery_warehouse_from,
                                    value=size_remain,
                                    size=size,
                                )
                                amount_rur_summ += amount_rur
                        item.debit_warehouse_from_remains = True
                        item.summ_rur = amount_rur_summ
                        item.save()
        if self.status.create_1c_document and create_1c_document:
            self.create_1c_document()


class LuggageSpace(models.Model):
    supply = models.ForeignKey(Supply, verbose_name='Поставка')
    count = models.IntegerField('Количество', default=1)
    weight = models.DecimalField('Вес', default=0, max_digits=20, decimal_places=10)

    class Meta:
        verbose_name = 'Багажное место'
        verbose_name_plural = 'Багажные места'
        ordering = ('-id', )

    def __str__(self):
        return '{} - {} ({})'.format(
            self.supply,
            self.id,
            self.weight, )


class SupplyItem(models.Model):
    SERIES_TYPE = SERIES_TYPE_CHOICES
    supply = models.ForeignKey(Supply, verbose_name='Поставка', related_name="items")
    product = models.ForeignKey('products.Product', verbose_name='Товар')

    price = models.DecimalField('Цена', blank=True, null=True, max_digits=12, decimal_places=2)

    summ_rur = models.DecimalField('Себестоимость', default=Decimal('0.00'), max_digits=20, decimal_places=10)

    debit_warehouse_from_remains = models.BooleanField('Остатки со склада источника списаны?', default=False)
    set_product_price = models.BooleanField('Цена товаров изменена?', default=False)

    series_type = models.CharField('Тип серии', max_length=1, choices=SERIES_TYPE)

    count_xxs = models.PositiveSmallIntegerField('XXS', default=0)
    count_xs = models.PositiveSmallIntegerField('XS', default=0)
    count_s = models.PositiveSmallIntegerField('S', default=0)
    count_m = models.PositiveSmallIntegerField('M', default=0)
    count_l = models.PositiveSmallIntegerField('L', default=0)
    count_xl = models.PositiveSmallIntegerField('XL', default=0)
    count_xxl = models.PositiveSmallIntegerField('XXL', default=0)
    count_xxxl = models.PositiveSmallIntegerField('XXXL', default=0)
    count_34 = models.PositiveSmallIntegerField('34', default=0)
    count_35 = models.PositiveSmallIntegerField('35', default=0)
    count_36 = models.PositiveSmallIntegerField('36', default=0)
    count_37 = models.PositiveSmallIntegerField('37', default=0)
    count_38 = models.PositiveSmallIntegerField('38', default=0)
    count_39 = models.PositiveSmallIntegerField('39', default=0)
    count_40 = models.PositiveSmallIntegerField('40', default=0)
    count_41 = models.PositiveSmallIntegerField('41', default=0)

    class Meta:
        verbose_name = 'Товар в поставке'
        verbose_name_plural = 'Товары в поставке'
        ordering = ('-id', )
        unique_together = ('supply', 'product')

    def get_quantity(self):
        quantity = 0
        for field_name in [_ for _ in dir(self) if _.startswith('count_')]:
            quantity += getattr(self, field_name)
        return quantity

    def get_cost_price(self):
        """Себестоимость за штуку"""
        quantity = self.get_quantity()

        if quantity:
            cost = (self.summ_rur / quantity).quantize(Decimal('0.0000000000'))
        else:
            cost = Decimal('0.0000000000')

        return cost + self.supply.get_item_all_amount_paid_rur()


def supply_item_set_fields_by_product(sender, instance, **kwargs):
    if instance.product_id:
        if not instance.series_type:
            instance.series_type = instance.product.main_collection.series_type


models.signals.pre_save.connect(supply_item_set_fields_by_product, sender=SupplyItem)

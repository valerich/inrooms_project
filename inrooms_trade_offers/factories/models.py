from django.db import models

from model_utils.models import TimeStampedModel


class Factory(TimeStampedModel):
    name = models.CharField('Название', max_length=100, unique=True)
    phone = models.CharField('Номер телефона', max_length=100, blank=True)
    address = models.CharField('Адрес', max_length=500, blank=True)
    contact_person = models.CharField('Контактное лицо', max_length=100, blank=True)
    contact_email = models.EmailField('Email контактного лица', max_length=100, blank=True)

    class Meta:
        verbose_name = 'Фабрика'
        verbose_name_plural = 'Фабрики'
        ordering = ('name', )

    def __str__(self):
        return 'id:{}, {}'.format(self.id, self.name)

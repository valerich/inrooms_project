from django.contrib import admin

from .models import Factory


class FactoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', )
    search_fields = ('name', '=id', )


for model_or_iterable, admin_class in (
    (Factory, FactoryAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

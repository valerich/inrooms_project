from django.db.utils import IntegrityError
from django.test import TestCase

from ..models import Factory


class FactoriesModelsTestCase(TestCase):
    def setUp(self):
        Factory.objects.all().delete()

    def test_factory_name_unique(self):
        """Поле "Название" у фабрик уникально"""

        Factory.objects.create(name='Фабрика 1')
        with self.assertRaises(IntegrityError):
            Factory.objects.create(name='Фабрика 1')

from django.test import TestCase

from ..factories import FactoryFactory
from ..models import Factory


class FactoriesFactoryTestCase(TestCase):
    def setUp(self):
        Factory.objects.all().delete()

    def test_factory_factory(self):
        factory = FactoryFactory(name='Фабрика 1')
        factory_from_db = Factory.objects.get(name='Фабрика 1')
        self.assertEqual(factory, factory_from_db)

    def test_multiple_factories_factory(self):
        factories = []
        for i in range(10):
            factories.append(FactoryFactory())

        self.assertEqual(len(factories), 10)
        self.assertEqual(len(factories), len(Factory.objects.all()))

from django.core.urlresolvers import reverse
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from ..factories import FactoryFactory
from ..models import Factory


class FactoryViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(FactoryViewTestCase, cls).setUpClass()
        cls.factory_list_url = reverse('api:factories:factory-list')
        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        Factory.objects.all().delete()
        for i in range(10):
            FactoryFactory()

    def setUp(self):
        self.client = APIClient()
        self.client.login(email='admin@example.com', password='password')
        pass

    def tearDown(self):
        self.client.session.clear()

    def test_list_factory(self):
        response = self.client.get(self.factory_list_url)
        self.assertEqual(response.data['count'], 10)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client.get(self.factory_list_url)
        factory_dict = response.data['results'][0]
        fields = [
            'id',
            'name',
            'created',
            'modified',
            'phone',
            'address',
            'contact_person',
            'contact_email',
        ]
        for field in fields:
            self.assertIn(field, factory_dict, 'Не передается поле {} в {}'.format(field, self.factory_list_url))

from rest_framework import serializers

from ..models import Factory


class FactorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Factory
        fields = [
            'id',
            'name',
            'created',
            'modified',
            'phone',
            'address',
            'contact_person',
            'contact_email',
        ]

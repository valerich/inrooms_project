from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from ..serializers import FactorySerializer
from ...models import Factory


class FactoryViewSet(ModelViewSet):
    serializer_class = FactorySerializer
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', '=id')
    queryset = Factory.objects.all()
    ordering_fields = (
        'id',
        'name',
        'modified',
    )

from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'factories'
    verbose_name = 'Производство'

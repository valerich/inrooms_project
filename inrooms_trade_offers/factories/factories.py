import factory
import factory.django
import factory.fuzzy

from .models import Factory


class FactoryFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()

    class Meta:
        model = Factory

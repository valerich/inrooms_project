from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'service_1c'
    verbose_name = 'Взаимодействие с 1с'

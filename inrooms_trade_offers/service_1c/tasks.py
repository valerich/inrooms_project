from config.celery import app
from service_1c.utils.manufacturing_order_acceptance_importer.importer import ManufacturingOrderAcceptanceImporter
from service_1c.utils.manufacturing_order_status_parser.parser import ManufacturingOrderStatusParser
from service_1c.utils.supply_importer.importer import SupplyImporter

from .utils.client_exporter import ClientExporter
from .utils.defect_act_status_parser import DefectActStatusParser
from .utils.employee_exporter import EmployeeExporter
from .utils.order_b2c_importer import OrderB2CImporter
from .utils.order_importer import OrderImporter
from .utils.order_return_b2c_importer import OrderReturnB2CImporter
from .utils.order_return_importer import OrderReturnImporter
from .utils.order_status_parser import OrderStatusParser
from .utils.price_set_exporter import PriceSetExporter
from .utils.offer_exporter import OfferExporter
from .utils.product_exporter import ProductExporter
from .utils.refund_act_status_parser import RefundActStatusParser
from .utils.remain_parser import RemainParser
from .utils.scanner_data_parser import ScannerDataParser
from .utils.store_exporter import StoreExporter
from .utils.supply_status_parser import SupplyStatusParser


@app.task(name='service_1c.sync_remains')
def sync_remains():
    """Обработка документов "изменение остатков" от 1с
    """
    rp = RemainParser()
    rp.run()


@app.task(name='service_1c.sync_scanner_data')
def sync_scanner_data():
    """Обработка документов "данные сканера штрих кодов" от 1с
    """
    parser = ScannerDataParser()
    parser.run()


@app.task(name='service_1c.sync_supply_status')
def sync_supply_status():
    """Обработка документов "изменение статуса поставки" от 1с
    """
    rp = SupplyStatusParser()
    rp.run()


@app.task(name='service_1c.sync_defect_act_status')
def sync_defect_act_status():
    """Обработка документов "изменение статуса акта о браке" от 1с
    """
    rp = DefectActStatusParser()
    rp.run()


@app.task(name='service_1c.sync_refund_act_status')
def sync_refund_act_status():
    """Обработка документов "изменение статуса акта на возврат" от 1с
    """
    rp = RefundActStatusParser()
    rp.run()


@app.task(name='service_1c.sync_order_status')
def sync_order_status():
    """Обработка документов "изменение статуса заказа" от 1с
    """
    rp = OrderStatusParser()
    rp.run()


@app.task(name='service_1c.order_import')
def order_import():
    """Изменение заказа от 1с
    """
    oi = OrderImporter()
    oi.run()


@app.task(name='service_1c.supply_import')
def supply_import():
    """Получение поставки от 1с
    """
    oi = SupplyImporter()
    oi.run()


@app.task(name='service_1c.manufacturing_order_acceptance_import')
def manufacturing_order_acceptance_import():
    """Изменение заказа от 1с
    """
    oi = ManufacturingOrderAcceptanceImporter()
    oi.run()


@app.task(name='service_1c.sync_manufacturing_order_status')
def sync_manufacturing_order_status():
    """Обработка документов "изменение статуса заказа на фабрику" от 1с
    """
    rp = ManufacturingOrderStatusParser()
    rp.run()


@app.task(name='service_1c.order_b2c_import')
def order_b2c_import():
    """Изменение заказа от 1с
    """
    oi = OrderB2CImporter()
    oi.run()


@app.task(name='service_1c.order_return_import')
def order_return_import():
    """Изменение возврата от 1с
    """
    oi = OrderReturnImporter()
    oi.run()


@app.task(name='service_1c.order_return_b2c_import')
def order_return_b2c_import():
    """Изменение возврата от 1с
    """
    oi = OrderReturnB2CImporter()
    oi.run()


@app.task(name='service_1c.notify_1c_offers_change', ignore_result=True)
def notify_1c_offers_change(offer_ids: list):
    """
    Оповестить 1С об изменении свойств предложений
    :param offer_ids: список ID предложений
    :return:
    """
    assert isinstance(offer_ids, (list, tuple))
    pe = OfferExporter(offer_ids)
    pe.run()


@app.task(name='service_1c.notify_1c_product_change', ignore_result=True)
def notify_1c_product_change(product_ids: list):
    """
    Оповестить 1С об изменении свойств продуктов
    :param product_ids: список ID продуктов
    :return:
    """
    assert isinstance(product_ids, (list, tuple))
    pe = ProductExporter(product_ids)
    pe.run()


@app.task(name='service_1c.notify_1c_client_change', ignore_result=True)
def notify_1c_client_change(client_ids: list):
    """
    Оповестить 1С об изменении клиента
    :param client_ids: список ID клиентов
    :return:
    """
    assert isinstance(client_ids, (list, tuple))
    ce = ClientExporter(client_ids)
    ce.run()


@app.task(name='service_1c.notify_1c_store_change', ignore_result=True)
def notify_1c_store_change(store_ids: list):
    """
    Оповестить 1С об изменении магазина
    :param store_ids: список ID магазинов
    :return:
    """
    assert isinstance(store_ids, (list, tuple))
    se = StoreExporter(store_ids)
    se.run()


@app.task(name='service_1c.notify_1c_employee_change', ignore_result=True)
def notify_1c_employee_change(employee_ids: list):
    """
    Оповестить 1С об изменении работника
    :param store_ids: список ID работников
    :return:
    """
    assert isinstance(employee_ids, (list, tuple))
    se = EmployeeExporter(employee_ids)
    se.run()

from django.contrib.postgres.fields import JSONField
from django.core.urlresolvers import reverse
from django.db import models

from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

STATUS_CHOICE = Choices(
    (0, 'new', 'Новый'),
    (100, 'process', 'Обрабатывается'),
    (200, 'complete', 'Обработан'),
    (-100, 'error', 'Обработан с ошибкой'),
)

DIRECTION_CHOICE = Choices(
        (10,  'incoming', 'Входящие'),
        (20,  'outgoing', 'Исходящие'),
    )

KIND_CHOICE = Choices(
    (100, 'supply', 'Поставка'),
    (101, 'supply_status_change', 'Смена статуса поставки'),
    (102, 'supply_import', 'Прием поставки'),
    (200, 'remains', 'Остатки'),
    (300, 'order', 'Заказ'),
    (301, 'order_status_change', 'Смена статуса заказа'),
    (302, 'order_import', 'Прием заказа'),
    (400, 'purchase', 'Закупка'),
    (401, 'purchase_status_change', 'Смена статуса закупки'),
    (500, 'scanner_data', 'Данные со сканера штрих кодов'),
    (600, 'order_b2c_import', 'Прием заказа b2c'),
    (700, 'order_return_b2c_import', 'Прием возврата b2c'),
    (800, 'order_return_import', 'Прием возврата'),
    (900, 'defect_act', 'Акт о браке'),
    (901, 'defect_act_status_change', 'Смена статуса акта о браке'),
    (1000, 'refund_act', 'Акт на возврат'),
    (1001, 'refund_act_status_change', 'Смена статуса акта на возврат'),
    (1100, 'price_set', 'Установка цен'),
    (1200, 'offer', 'Образцы'),
    (1300, 'manufacturing_order', 'Заказ на фабрику'),
    (1301, 'manufacturing_order_acceptance_import', 'Прием заказа с фабрики'),
    (1302, 'manufacturing_order_status_change', 'Смена статуса заказа на фабрику'),
)


class Document(TimeStampedModel):
    """Документы для обмена с 1с"""

    STATUS = STATUS_CHOICE
    DIRECTION = DIRECTION_CHOICE
    KIND = KIND_CHOICE
    status = models.SmallIntegerField('Статус', choices=STATUS, default=STATUS.new, db_index=True)
    kind = models.PositiveSmallIntegerField('Тип', choices=KIND, db_index=True)
    direction = models.PositiveSmallIntegerField('Получатель', choices=DIRECTION, db_index=True)
    brand = models.ForeignKey('brands.Brand', verbose_name='Бренд', blank=True, null=True, on_delete=models.PROTECT)
    data = JSONField('Данные')
    result = models.TextField('Результат', blank=True)

    class Meta:
        verbose_name = 'Документ'
        verbose_name_plural = 'Документы'
        ordering = ('-created', )

    def __str__(self):
        return 'Документ №{} - {}: {}'.format(self.id, self.get_kind_display(), self.get_status_display())

    def get_absolute_url(self):
        return reverse('admin:service_1c_document_change', args=(self.id,))

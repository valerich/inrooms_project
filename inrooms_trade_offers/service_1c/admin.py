from django.contrib import admin

from core.admin import JsonAdmin
from .models import Document


class DocumentAdmin(JsonAdmin):
    list_display = ['id', 'brand', 'kind', 'direction', 'status', 'created']
    list_filter = ['kind', 'direction', 'status', 'brand', ]


for model_or_iterable, admin_class in (
    (Document, DocumentAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

import json

from django.contrib.sites.models import Site
from django.test import TestCase
from freezegun import freeze_time

from brands.factories import BrandFactory
from products.factories import ProductCollectionFactory, ProductColorFactory, ProductFactory, ProductImageFactory
from ..utils.product_exporter import ProductExporter


@freeze_time("2016-1-31 00:00:00")
class ProductExporterTestCase(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super(ProductExporterTestCase, cls).setUpClass()

        Site.objects.all().update(domain='test.inrooms.club')

        cls.color1 = ProductColorFactory(name='color1')
        cls.color2 = ProductColorFactory(name='color2')
        cls.collection = ProductCollectionFactory(name='Коллекция', series_type="c")

        cls.brand1 = BrandFactory(
            product_export_url='url',
            product_export_login='login',
            product_export_pass='pass',
        )

        cls.brand2 = BrandFactory(
            product_export_url='url2',
            product_export_login='login2',
            product_export_pass='pass2',
        )

        cls.product_1 = ProductFactory(
            is_deleted=False,
            article='article_1',
            name='name_1',
            description='description_1',
            is_active=True,
            color=cls.color1,
            main_collection=cls.collection,
            price=None,
            brand=cls.brand1,
        )
        cls.product_2 = ProductFactory(
            is_deleted=True,
            article='article_2',
            name='name_2',
            description='description_2',
            is_active=True,
            color=cls.color2,
            main_collection=cls.collection,
            price=100,
            brand=cls.brand2,
        )

        cls.image1 = ProductImageFactory(product=cls.product_1)
        cls.image2 = ProductImageFactory(product=cls.product_2)

        cls.serializer_valid_data = {
            str(cls.brand2.id): [
                {
                    "id": cls.product_2.id,
                    "is_deleted": True,
                    "article": 'article_2',
                    "name": 'name_2',
                    "description": 'description_2',
                    "is_active": True,
                    "image": {
                        'src': "http://test.inrooms.club{}".format(cls.image2.image.url)
                    },
                    "color": {
                        "id": cls.color2.id,
                        "name": "color2"
                    },
                    'created': '2016-01-31T00:00:00Z',
                    'modified': '2016-01-31T00:00:00Z',
                    "main_collection": {
                        "id": cls.collection.id,
                        "name": "Коллекция",
                        "series_type": "c"
                    },
                    "price": '100.00',
                },
            ],
            str(cls.brand1.id): [
                {
                    "id": cls.product_1.id,
                    "is_deleted": False,
                    "article": 'article_1',
                    "name": 'name_1',
                    "description": 'description_1',
                    "is_active": True,
                    "image": {
                        'src': "http://test.inrooms.club{}".format(cls.image1.image.url)
                    },
                    "color": {
                        "id": cls.color1.id,
                        "name": "color1"
                    },
                    'created': '2016-01-31T00:00:00Z',
                    'modified': '2016-01-31T00:00:00Z',
                    "main_collection": {
                        "id": cls.collection.id,
                        "name": "Коллекция",
                        "series_type": "c"
                    },
                    "price": None,
                },
            ],
        }

    def test_get_clients(self):
        # Активный товар
        product_exporter = ProductExporter([self.product_1.id, ])
        products = product_exporter.get_objects()
        self.assertEqual(products.count(), 1)
        self.assertEqual(products[0].id, self.product_1.id)

        # Удаленный това
        product_exporter = ProductExporter([self.product_2.id, ])
        products = product_exporter.get_objects()
        self.assertEqual(products.count(), 1)
        self.assertEqual(products[0].id, self.product_2.id)

        # Оба товара
        product_exporter = ProductExporter([self.product_1.id, self.product_2.id, ])
        products = product_exporter.get_objects()
        self.assertEqual(products.count(), 2)

    def test_client_serializer(self):
        product_exporter = ProductExporter([self.product_1.id, self.product_2.id])
        products = product_exporter.get_objects()
        brand_tech_data = product_exporter.get_brands_soap_client_parameters(products)
        data = product_exporter.get_objects_data(products, brand_tech_data)
        self.assertJSONEqual(json.dumps(data), self.serializer_valid_data)

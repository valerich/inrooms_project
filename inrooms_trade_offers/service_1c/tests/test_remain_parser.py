import datetime

from django.test import TestCase
from django.utils import timezone

from core.server_init import ServerInit
from products.factories import ProductFactory
from products.models import Remain
from warehouses.factories import WarehouseFactory
from ..factories import DocumentFactory
from ..models import DIRECTION_CHOICE, KIND_CHOICE, STATUS_CHOICE, Document
from ..utils.remain_parser import RemainParser


class RemainParserTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(RemainParserTestCase, cls).setUpClass()

        server_init = ServerInit()
        server_init.create_product_sizes()
        server_init.create_warehouses()
        cls.product = ProductFactory()
        cls.valid_data = [{'product_id': cls.product.id,
                           'warehouse_code': 'msk',
                           'remains': [{'size': '2xs', 'value': 3},
                                       {'size': 's', 'value': 15}, ]}, ]

        cls.remain_parser = RemainParser()

        # Генерируем все возможные документы
        for kind in KIND_CHOICE._db_values:
            for status in STATUS_CHOICE._db_values:
                for direction in DIRECTION_CHOICE._db_values:
                    DocumentFactory(kind=kind, status=status, direction=direction)

        cls.to_work_document_id = Document.objects.get(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.remains,
            status=Document.STATUS.new,
        ).id

    def test_get_documents(self):
        self.remain_parser._get_documents()
        self.assertEqual(len(self.remain_parser._documents), 1)
        document = self.remain_parser._documents[0]
        self.assertEqual(document.id, self.to_work_document_id)

    def test_set_documents_process(self):
        self.remain_parser._set_documents_process()
        self.assertEqual(self.remain_parser.document_qs.count(), 0)
        to_work_document = Document.objects.get(id=self.to_work_document_id)
        self.assertEqual(to_work_document.status,
                         STATUS_CHOICE.process)
        to_work_document.status = STATUS_CHOICE.new
        to_work_document.save()

    def test_set_document_error(self):
        document = Document.objects.get(id=self.to_work_document_id)
        error_message = 'Тестовая ошибка'
        self.remain_parser.set_document_error(document, error_message)
        document.refresh_from_db()
        self.assertEqual(document.status, STATUS_CHOICE.error)
        self.assertEqual(document.result, error_message)
        document.result = ''
        document.status = STATUS_CHOICE.new
        document.save()

    def test_set_document_complete(self):
        document = Document.objects.get(id=self.to_work_document_id)
        self.remain_parser.set_document_complete(document)
        document.refresh_from_db()
        self.assertEqual(document.status, STATUS_CHOICE.complete)
        document.status = STATUS_CHOICE.new
        document.save()

    def test_parse_document(self):
        document = Document.objects.get(id=self.to_work_document_id)

        # Не массив
        document.data = {}
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не верный формат документа')

        # Не переданы данные. Валидный документ
        document.data = []
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], True)
        self.assertEqual(len(data['errors']), 0)

        # Ожидается массив словарей
        document.data = [[], ]
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не верный формат документа')

        # Не передан product_id
        document.data = [{}, ]
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не верный product_id (None)')

        # Нет товара с id=1
        document.data = [{'product_id': 100500}, ]
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не найдена модель с id: 100500')

        # Нет передан remains
        document.data = [{'product_id': self.product.id}, ]
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не верный формат документа')

        # Нет передан размер
        document.data = [{'product_id': self.product.id,
                          'remains': [{}]}, ]
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Передан не существующий размер')

        # Передан не существующий размер
        document.data = [{'product_id': self.product.id,
                          'remains': [{'size': 'fail_zise'}]}, ]
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Передан не существующий размер')

        # Не передан остаток размера
        document.data = [{'product_id': self.product.id,
                          'remains': [{'size': '2xs'}]}, ]
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не передан остаток')

        # Остаток должен быть целым числом
        document.data = [{'product_id': self.product.id,
                          'remains': [{'size': '2xs', 'value': 'fail_remain'}]}, ]
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не передан остаток')

        # Передан не верный warehouse_code
        document.data = [{'product_id': self.product.id,
                          'warehouse_code': 'fail_code',
                          'remains': [{'size': '2xs', 'value': 1}]}, ]
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не найден склад с code: fail_code')

        # Валидный документ
        document.data = self.valid_data
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], True)
        self.assertEqual(len(data['errors']), 0)

    def test_warehouse(self):
        document = Document.objects.get(id=self.to_work_document_id)

        # Не передан warehouse_code - значит остатки для склада москвы
        document.data = [{'product_id': self.product.id,
                          'remains': [{'size': '2xs', 'value': 1}]}, ]
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], True)
        self.assertEqual(data['data'][0]['warehouse'].code, 'msk')

        # Передан warehouse_code - значит остатки для склада с этим кодом
        document.data = [{'product_id': self.product.id,
                          'warehouse_code': 'msk',
                          'remains': [{'size': '2xs', 'value': 1}]}, ]
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], True)
        self.assertEqual(data['data'][0]['warehouse'].code, 'msk')

        document.data = [{'product_id': self.product.id,
                          'warehouse_code': 'chn',
                          'remains': [{'size': '2xs', 'value': 1}]}, ]
        data = self.remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], True)
        self.assertEqual(data['data'][0]['warehouse'].code, 'chn')

    def test_accept_1c_remains(self):
        """Записываем в базу остатки складов у которых выставлен разрешающий флаг

        В 1с есть склад "Китай" по которому первоисточник данных - inrooms
        """

        document = Document.objects.get(id=self.to_work_document_id)
        warehouse = WarehouseFactory(code='no_1c_warehouse', accept_1c_remains=False)
        document.data = [{'product_id': self.product.id,
                          'warehouse_code': warehouse.code,
                          'remains': [{'size': '2xs', 'value': 1}]}, ]
        remain_parser = RemainParser()
        data = remain_parser.parse_document(document)
        self.assertEqual(data['is_valid'], True)
        self.assertEqual(len(data['data']), 0)

    def test_run(self):
        Document.objects.update(status=STATUS_CHOICE.new, )
        # Проблемный документ
        self.remain_parser.run()
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.error).count(), 1)
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.complete).count(), 0)

        Document.objects.update(status=STATUS_CHOICE.new, data=self.valid_data)
        # Валидный документ
        self.remain_parser.run()
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.error).count(), 0)
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.complete).count(), 4)
        self.assertEqual(Remain.objects.count(), 2)
        self.assertEqual(self.product.remain_sum, 18)

    def test_document_queue(self):
        """Очередность обработки документов

        first in, first out
        """

        Document.objects.all().delete()
        doc1 = Document.objects.create(
            created=timezone.make_aware(datetime.datetime(2015, 1, 5), timezone.get_current_timezone()),
            kind=KIND_CHOICE.remains,
            status=STATUS_CHOICE.new,
            data={},
            direction=DIRECTION_CHOICE.incoming)
        doc2 = Document.objects.create(
            created=timezone.make_aware(datetime.datetime(2015, 1, 3), timezone.get_current_timezone()),
            kind=KIND_CHOICE.remains,
            status=STATUS_CHOICE.new,
            data={},
            direction=DIRECTION_CHOICE.incoming)
        doc3 = Document.objects.create(
            created=timezone.make_aware(datetime.datetime(2015, 1, 7), timezone.get_current_timezone()),
            kind=KIND_CHOICE.remains,
            status=STATUS_CHOICE.new,
            data={},
            direction=DIRECTION_CHOICE.incoming)

        remain_parser = RemainParser()
        qs = remain_parser.get_document_queryset()

        self.assertEqual(qs[0].id, doc2.id)
        self.assertEqual(qs[1].id, doc1.id)
        self.assertEqual(qs[2].id, doc3.id)

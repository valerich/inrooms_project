import datetime
import json

from django.test import TestCase

from clients.factories import ClientFactory
from stores.factories import StoreFactory, EmployeeFactory
from ..utils.employee_exporter import EmployeeExporter


class EmployeeExporterTestCase(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super(EmployeeExporterTestCase, cls).setUpClass()

        cls.client_1 = ClientFactory()
        cls.client_2 = ClientFactory()
        cls.store_1 = StoreFactory(client=cls.client_1)
        cls.store_2 = StoreFactory(client=cls.client_2)

        cls.employee_1 = EmployeeFactory(
            store=cls.store_1,
            first_name='first_name1',
            middle_name='middle_name1',
            last_name='last_name1',
            phone='1111111111',
            email='user1@example.com',
            date_birthday=datetime.date(2015, 10, 24),
            date_employment=datetime.date(2015, 10, 25),
            is_working=True,
            position='position1'
        )
        cls.employee_2 = EmployeeFactory(
            store=cls.store_2,
            first_name='first_name2',
            middle_name='middle_name2',
            last_name='last_name2',
            phone='2222222222',
            email='user2@example.com',
            date_birthday=datetime.date(2015, 10, 26),
            date_employment=datetime.date(2015, 10, 27),
            is_working=False,
            position='position2'
        )

        cls.serializer_valid_data = [
            {
                "id": cls.employee_1.id,
                # "store": cls.store_1.id,
                "client": cls.client_1.id,
                "first_name": 'first_name1',
                "middle_name": 'middle_name1',
                "last_name": 'last_name1',
                "phone": '1111111111',
                "email": 'user1@example.com',
                "date_birthday": "2015-10-24",
                "date_employment": "2015-10-25",
                "is_working": True,
                "position": 'position1'
            },
            {
                "id": cls.employee_2.id,
                # "store": cls.store_2.id,
                "client": cls.client_2.id,
                "first_name": 'first_name2',
                "middle_name": 'middle_name2',
                "last_name": 'last_name2',
                "phone": '2222222222',
                "email": 'user2@example.com',
                "date_birthday": "2015-10-26",
                "date_employment": "2015-10-27",
                "is_working": False,
                "position": 'position2'
            },
        ]

    def test_get_employees(self):
        employee_exporter = EmployeeExporter([self.employee_1.id, ])
        employees = employee_exporter.get_objects()
        self.assertEqual(employees.count(), 1)
        self.assertEqual(employees[0].id, self.employee_1.id)

        # Оба клиента
        employee_exporter = EmployeeExporter([self.employee_1.id, self.employee_2.id, ])
        employees = employee_exporter.get_objects()
        self.assertEqual(employees.count(), 2)

    def test_employee_serializer(self):
        employee_exporter = EmployeeExporter([self.employee_1.id, self.employee_2.id])
        employees = employee_exporter.get_objects()
        data = employee_exporter.get_objects_data(employees)
        self.assertJSONEqual(json.dumps(data), self.serializer_valid_data)

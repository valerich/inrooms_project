import json

from decimal import Decimal
from django.test import TestCase
from freezegun import freeze_time

from clients.factories import ClientFactory
from core.server_init import ServerInit
from prices.factories import PriceSetFactory, PriceSetItemFactory
from products.factories import ProductFactory
from service_1c.utils.price_set_exporter.exporter import PriceSetExporter
from ..models import DIRECTION_CHOICE, KIND_CHOICE, Document


@freeze_time("2016-1-31 00:00:00")
class PriceSetExporterTestCase(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super(PriceSetExporterTestCase, cls).setUpClass()

        server_init = ServerInit()
        server_init.create_product_sizes()
        server_init.create_warehouses()
        server_init.create_groups()
        server_init.create_order_statuses()

        cls.product_1 = ProductFactory()
        cls.product_2 = ProductFactory()

        cls.price_set_client = ClientFactory()

        cls.price_set = PriceSetFactory(client=cls.price_set_client)
        cls.price_set_item1 = PriceSetItemFactory(price_set=cls.price_set,
                                                  product=cls.product_1,
                                                  old_price=Decimal('100.00'),
                                                  new_price=Decimal('200.00'))
        cls.price_set_item2 = PriceSetItemFactory(price_set=cls.price_set,
                                                  product=cls.product_2,
                                                  old_price=Decimal('300.00'),
                                                  new_price=Decimal('400.00'))

        cls.price_set_exporter = PriceSetExporter(cls.price_set.id)

        cls.valid_data = {
            "id": cls.price_set.id,
            "client_id": cls.price_set_client.id,
            "items": [{
                "product_id": cls.product_1.id,
                "new_price": "200.00",
            }, {
                "product_id": cls.product_2.id,
                "new_price": "400.00",
            }],
            "created": "2016-01-31T00:00:00Z",
        }

    def test_create_document(self):
        created_document = self.price_set_exporter.create_document()
        document = Document.objects.get(id=created_document.id)
        self.assertEqual(document.kind, KIND_CHOICE.price_set)
        self.assertEqual(document.direction, DIRECTION_CHOICE.outgoing)
        self.assertJSONEqual(json.dumps(document.data), self.valid_data)

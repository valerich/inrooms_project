import datetime

from django.test import TestCase
from django.utils import timezone

from orders.models import ScannerData
from products.factories import ProductFactory
from ..factories import DocumentFactory
from ..models import DIRECTION_CHOICE, KIND_CHOICE, STATUS_CHOICE, Document
from ..utils.scanner_data_parser import ScannerDataParser


class ScannerDataParserTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(ScannerDataParserTestCase, cls).setUpClass()

        cls.product = ProductFactory()
        cls.valid_data = [cls.product.id, ]

        # Генерируем все возможные документы
        for kind in KIND_CHOICE._db_values:
            for status in STATUS_CHOICE._db_values:
                for direction in DIRECTION_CHOICE._db_values:
                    DocumentFactory(kind=kind, status=status, direction=direction)

        cls.parser = ScannerDataParser()

        cls.to_work_document_id = Document.objects.get(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.scanner_data,
            status=Document.STATUS.new,
        ).id

    def test_get_documents(self):
        self.parser._get_documents()
        self.assertEqual(len(self.parser._documents), 1)
        document = self.parser._documents[0]
        self.assertEqual(document.id, self.to_work_document_id)

    def test_set_documents_process(self):
        self.parser._set_documents_process()
        self.assertEqual(self.parser.document_qs.count(), 0)
        to_work_document = Document.objects.get(id=self.to_work_document_id)
        self.assertEqual(to_work_document.status,
                         STATUS_CHOICE.process)
        to_work_document.status = STATUS_CHOICE.new
        to_work_document.save()

    def test_set_document_error(self):
        document = Document.objects.get(id=self.to_work_document_id)
        self.parser.set_document_error(document)
        document.refresh_from_db()
        self.assertEqual(document.status, STATUS_CHOICE.error)
        document.result = ''
        document.status = STATUS_CHOICE.new
        document.save()

    def test_set_document_complete(self):
        document = Document.objects.get(id=self.to_work_document_id)
        self.parser.set_document_complete(document)
        document.refresh_from_db()
        self.assertEqual(document.status, STATUS_CHOICE.complete)
        document.status = STATUS_CHOICE.new
        document.save()

    def test_parse_document(self):
        document = Document.objects.get(id=self.to_work_document_id)

        # Не массив
        document.data = {}
        data = self.parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не верный формат документа')

        # Не переданы данные. Валидный документ
        document.data = []
        data = self.parser.parse_document(document)
        self.assertEqual(data['is_valid'], True)
        self.assertEqual(len(data['errors']), 0)

        # Ожидается массив id товаров. Записываем всё, что является настоящими id
        document.data = [100500, 'fail_id', self.product.id]
        data = self.parser.parse_document(document)
        self.assertEqual(data['is_valid'], True)
        self.assertEqual(data['errors'][0], 'Не найден товар с id: 100500')
        self.assertEqual(data['errors'][1], 'Не найден товар с id: fail_id')

    def test_run(self):
        Document.objects.update(status=STATUS_CHOICE.new, data={})
        # Проблемный документ
        self.parser.run()
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.error).count(), 1)
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.complete).count(), 0)

        Document.objects.update(status=STATUS_CHOICE.new, data=self.valid_data)
        # Валидный документ
        self.parser.run()
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.error).count(), 0)
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.complete).count(), 4)
        self.assertEqual(ScannerData.objects.count(), 4)
        sd = ScannerData.objects.all()[0]
        self.assertEqual(sd.products.count(), 1)
        self.assertEqual(sd.products.all()[0].id, self.product.id)

    def test_document_queue(self):
        """Очередность обработки документов

        first in, first out
        """

        Document.objects.all().delete()
        doc1 = Document.objects.create(
            created=timezone.make_aware(datetime.datetime(2015, 1, 5), timezone.get_current_timezone()),
            kind=KIND_CHOICE.scanner_data,
            status=STATUS_CHOICE.new,
            data={},
            direction=DIRECTION_CHOICE.incoming)
        doc2 = Document.objects.create(
            created=timezone.make_aware(datetime.datetime(2015, 1, 3), timezone.get_current_timezone()),
            kind=KIND_CHOICE.scanner_data,
            status=STATUS_CHOICE.new,
            data={},
            direction=DIRECTION_CHOICE.incoming)
        doc3 = Document.objects.create(
            created=timezone.make_aware(datetime.datetime(2015, 1, 7), timezone.get_current_timezone()),
            kind=KIND_CHOICE.scanner_data,
            status=STATUS_CHOICE.new,
            data={},
            direction=DIRECTION_CHOICE.incoming)

        parser = ScannerDataParser()
        qs = parser.get_document_queryset()

        self.assertEqual(qs[0].id, doc2.id)
        self.assertEqual(qs[1].id, doc1.id)
        self.assertEqual(qs[2].id, doc3.id)

import json

from django.test import TestCase
from freezegun import freeze_time

from clients.factories import ClientFactory
from core.server_init import ServerInit
from core.utils.constants import ORDER_CATEGORY_CHOICE
from orders.factories import OrderFactory, OrderItemFactory, OrderItemSizeFactory
from orders.models import OrderStatus
from products.factories import ProductCollectionFactory, ProductFactory
from products.models import SERIES_TYPE_CHOICES, Size
from warehouses.models import Warehouse
from ..models import DIRECTION_CHOICE, KIND_CHOICE, Document
from ..utils.order_exporter import OrderExporter


@freeze_time("2016-1-31 00:00:00")
class OrderExporterTestCase(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super(OrderExporterTestCase, cls).setUpClass()

        server_init = ServerInit()
        server_init.create_product_sizes()
        server_init.create_warehouses()
        server_init.create_groups()
        server_init.create_order_statuses()

        cls.warehouse_data = {warehouse.code: warehouse for warehouse in Warehouse.objects.all()}
        cls.size_data = {size.code: size for size in Size.objects.all()}

        main_collection_c = ProductCollectionFactory(series_type=SERIES_TYPE_CHOICES.clothes)
        main_collection_s = ProductCollectionFactory(series_type=SERIES_TYPE_CHOICES.shoes)
        cls.product_clothes = ProductFactory(main_collection=main_collection_c, price=100)
        cls.product_shoes = ProductFactory(main_collection=main_collection_s, price=200)

        client = ClientFactory()

        status = OrderStatus.objects.get(code='new')

        cls.order = OrderFactory(client=client, status=status)
        cls.order_item1 = OrderItemFactory(order=cls.order, product=cls.product_clothes, price=10)
        cls.order_item2 = OrderItemFactory(order=cls.order, product=cls.product_shoes, price=20)

        for category in ORDER_CATEGORY_CHOICE._db_values:
            for i in range(1, 3):
                OrderItemSizeFactory(
                    order_item=getattr(cls, 'order_item{}'.format(i)),
                    size=cls.size_data['m'],
                    warehouse=cls.warehouse_data['msk'],
                    category=category,
                    quantity=10
                )
                OrderItemSizeFactory(
                    order_item=getattr(cls, 'order_item{}'.format(i)),
                    size=cls.size_data['m'],
                    warehouse=cls.warehouse_data['showroom'],
                    category=category,
                    quantity=20
                )
                OrderItemSizeFactory(
                    order_item=getattr(cls, 'order_item{}'.format(i)),
                    size=cls.size_data['s'],
                    warehouse=cls.warehouse_data['msk'],
                    category=category,
                    quantity=30
                )

        cls.order_exporter = OrderExporter(cls.order.id)

        cls.valid_data = {
            "categories": [{
                "category_id": 1,
                "items": [{
                    "data": [{
                        "size": "s",
                        "quantity": 30,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 10,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 20,
                        "warehouse": {
                            "code": "showroom"
                        }
                    }],
                    "price": '20.00',
                    "product_id": cls.product_shoes.id
                }, {
                    "data": [{
                        "size": "s",
                        "quantity": 30,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 10,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 20,
                        "warehouse": {
                            "code": "showroom"
                        }
                    }],
                    "price": '10.00',
                    "product_id": cls.product_clothes.id
                }]
            }, {
                "category_id": 2,
                "items": [{
                    "data": [{
                        "size": "s",
                        "quantity": 30,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 10,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 20,
                        "warehouse": {
                            "code": "showroom"
                        }
                    }],
                    "price": '20.00',
                    "product_id": cls.product_shoes.id
                }, {
                    "data": [{
                        "size": "s",
                        "quantity": 30,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 10,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 20,
                        "warehouse": {
                            "code": "showroom"
                        }
                    }],
                    "price": '10.00',
                    "product_id": cls.product_clothes.id
                }]
            }],
            "client": {
                "id": client.id
            },
            "status": {
                "code": "new"
            },
            "created": "2016-01-31T00:00:00Z",
            "id": cls.order.id
        }

    def test_create_document(self):
        created_document = self.order_exporter.create_document()
        document = Document.objects.get(id=created_document.id)
        self.assertEqual(document.kind, KIND_CHOICE.order)
        self.assertEqual(document.direction, DIRECTION_CHOICE.outgoing)
        self.assertJSONEqual(json.dumps(document.data), self.valid_data)

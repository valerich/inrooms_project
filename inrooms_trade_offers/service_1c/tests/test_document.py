from django.test import TestCase

from ..models import Document


class DocumentTestCase(TestCase):

    def test_new_document_status(self):
        """У нового документа статус = Новый"""

        document = Document.objects.create(
            kind=Document.KIND.supply,
            direction=Document.DIRECTION.incoming,
            data={},
        )

        self.assertEqual(document.status, Document.STATUS.new)

import datetime
import json

from django.test import TestCase

from clients.factories import ClientFactory
from ..utils.client_exporter import ClientExporter


class ClientExporterTestCase(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super(ClientExporterTestCase, cls).setUpClass()

        cls.client_1 = ClientFactory(
            is_deleted=False,
            kind=1,
            name='name_1',
            inn='inn_1',
            kpp='kpp_1',
            legal_index='legal_index_1',
            legal_region='legal_region_1',
            legal_district='legal_district_1',
            legal_city='legal_city_1',
            legal_street='legal_street_1',
            legal_house='legal_house_1',
            legal_building='legal_building_1',
            legal_flat='legal_flat_1',
            legal_construction='legal_construction_1',
            actual_index='actual_index_1',
            actual_region='actual_region_1',
            actual_district='actual_district_1',
            actual_city='actual_city_1',
            actual_street='actual_street_1',
            actual_house='actual_house_1',
            actual_building='actual_building_1',
            actual_flat='actual_flat_1',
            actual_construction='actual_construction_1',
            manager='manager_1',
            contact_person='contact_person_1',
            phone='phone_1',
            email='email_1@email_1.ru',
            reason='reason_1',
            account_current='account_current_1',
            account_correspondent='account_correspondent_1',
            bank_name='bank_name_1',
            bik='bik_1',
            contract_date=datetime.date(2016, 1, 1),
            contract_number='number_1',
        )
        cls.client_2 = ClientFactory(
            is_deleted=True,
            kind=2,
            name='name_2',
            inn='inn_2',
            kpp='kpp_2',
            legal_index='legal_index_2',
            legal_region='legal_region_2',
            legal_district='legal_district_2',
            legal_city='legal_city_2',
            legal_street='legal_street_2',
            legal_house='legal_house_2',
            legal_building='legal_building_2',
            legal_flat='legal_flat_2',
            legal_construction='legal_construction_2',
            actual_index='actual_index_2',
            actual_region='actual_region_2',
            actual_district='actual_district_2',
            actual_city='actual_city_2',
            actual_street='actual_street_2',
            actual_house='actual_house_2',
            actual_building='actual_building_2',
            actual_flat='actual_flat_2',
            actual_construction='actual_construction_2',
            manager='manager_2',
            contact_person='contact_person_2',
            phone='phone_2',
            email='email_2@email_2.ru',
            reason='reason_2',
            account_current='account_current_2',
            account_correspondent='account_correspondent_2',
            bank_name='bank_name_2',
            bik='bik_2',
            contract_number='number_2'
        )

        cls.serializer_valid_data = [
            {
                "id": cls.client_1.id,
                "kind": 1,
                "is_deleted": False,
                "name": 'name_1',
                "inn": 'inn_1',
                "kpp": 'kpp_1',
                "legal_index": 'legal_index_1',
                "legal_region": 'legal_region_1',
                "legal_district": 'legal_district_1',
                "legal_city": 'legal_city_1',
                "legal_street": 'legal_street_1',
                "legal_house": 'legal_house_1',
                "legal_building": 'legal_building_1',
                "legal_flat": 'legal_flat_1',
                "legal_construction": 'legal_construction_1',
                "actual_index": 'actual_index_1',
                "actual_region": 'actual_region_1',
                "actual_district": 'actual_district_1',
                "actual_city": 'actual_city_1',
                "actual_street": 'actual_street_1',
                "actual_house": 'actual_house_1',
                "actual_building": 'actual_building_1',
                "actual_flat": 'actual_flat_1',
                "actual_construction": 'actual_construction_1',
                "manager": 'manager_1',
                "contact_person": 'contact_person_1',
                "phone": 'phone_1',
                "email": 'email_1@email_1.ru',
                "reason": 'reason_1',
                "account_current": 'account_current_1',
                "account_correspondent": 'account_correspondent_1',
                "bank_name": 'bank_name_1',
                "bik": 'bik_1',
                "contract_date": "2016-01-01",
                "contract_number": 'number_1',
            },
            {
                "id": cls.client_2.id,
                "kind": 2,
                "is_deleted": True,
                "name": 'name_2',
                "inn": 'inn_2',
                "kpp": 'kpp_2',
                "legal_index": 'legal_index_2',
                "legal_region": 'legal_region_2',
                "legal_district": 'legal_district_2',
                "legal_city": 'legal_city_2',
                "legal_street": 'legal_street_2',
                "legal_house": 'legal_house_2',
                "legal_building": 'legal_building_2',
                "legal_flat": 'legal_flat_2',
                "legal_construction": 'legal_construction_2',
                "actual_index": 'actual_index_2',
                "actual_region": 'actual_region_2',
                "actual_district": 'actual_district_2',
                "actual_city": 'actual_city_2',
                "actual_street": 'actual_street_2',
                "actual_house": 'actual_house_2',
                "actual_building": 'actual_building_2',
                "actual_flat": 'actual_flat_2',
                "actual_construction": 'actual_construction_2',
                "manager": 'manager_2',
                "contact_person": 'contact_person_2',
                "phone": 'phone_2',
                "email": 'email_2@email_2.ru',
                "reason": 'reason_2',
                "account_current": 'account_current_2',
                "account_correspondent": 'account_correspondent_2',
                "bank_name": 'bank_name_2',
                "bik": 'bik_2',
                "contract_date": "",
                "contract_number": 'number_2',
            }
        ]

    def test_get_clients(self):
        # Активный клиент
        client_exporter = ClientExporter([self.client_1.id, ])
        clients = client_exporter.get_objects()
        self.assertEqual(clients.count(), 1)
        self.assertEqual(clients[0].id, self.client_1.id)

        # Удаленный клиент
        client_exporter = ClientExporter([self.client_2.id, ])
        clients = client_exporter.get_objects()
        self.assertEqual(clients.count(), 1)
        self.assertEqual(clients[0].id, self.client_2.id)

        # Оба клиента
        client_exporter = ClientExporter([self.client_1.id, self.client_2.id, ])
        clients = client_exporter.get_objects()
        self.assertEqual(clients.count(), 2)

    def test_client_serializer(self):
        client_exporter = ClientExporter([self.client_1.id, self.client_2.id])
        clients = client_exporter.get_objects()
        data = client_exporter.get_objects_data(clients)
        self.assertJSONEqual(json.dumps(data), self.serializer_valid_data)

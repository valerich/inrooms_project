import datetime
import json

from django.test import TestCase
from freezegun import freeze_time

from clients.factories import ClientFactory
from core.server_init import ServerInit
from products.factories import ProductCollectionFactory, ProductFactory
from products.models import SERIES_TYPE_CHOICES
from purchases.factories import PurchaseFactory, PurchaseItemFactory, PurchaseStatusFactory
from warehouses.models import Warehouse
from ..models import DIRECTION_CHOICE, KIND_CHOICE, Document
from ..utils.purchase_exporter import PurchaseExporter


@freeze_time("2016-1-31 00:00:00")
class PurchaseExporterTestCase(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super(PurchaseExporterTestCase, cls).setUpClass()

        server_init = ServerInit()
        server_init.create_product_sizes()
        server_init.create_warehouses()

        main_collection_c = ProductCollectionFactory(series_type=SERIES_TYPE_CHOICES.clothes)
        main_collection_s = ProductCollectionFactory(series_type=SERIES_TYPE_CHOICES.shoes)
        cls.product_clothes = ProductFactory(main_collection=main_collection_c, price=100)
        cls.product_shoes = ProductFactory(main_collection=main_collection_s, price=200)

        cls.purchase_client = ClientFactory()
        warehouse_to = Warehouse.objects.get(code='msk')

        status = PurchaseStatusFactory(code='delivery_waiting')

        cls.purchase = PurchaseFactory(
            status=status,
            delivery_date=datetime.date(2016, 2, 19),
            delivery_client=cls.purchase_client,
            delivery_warehouse_to=warehouse_to,
        )
        PurchaseItemFactory(
            purchase=cls.purchase,
            product=cls.product_clothes,
            price=500,
            count_s=5,
            count_xxl=10,
        )
        PurchaseItemFactory(
            purchase=cls.purchase,
            product=cls.product_shoes,
            price=1000,
            count_34=5,
            count_39=10,
        )

        cls.purchase_exporter = PurchaseExporter(cls.purchase.id)

        cls.valid_data = {
            "id": cls.purchase.id,
            "status": {
                "code": "delivery_waiting"
            },
            "created": "2016-01-31T00:00:00Z",
            "delivery": {
                "date": "19.02.2016",
                "client": cls.purchase_client.id,
                "warehouse_to": {
                    "code": "msk",
                }
            },
            "items": [
                {
                    "product_id": cls.product_shoes.id,
                    "price": '1000.00',
                    "sizes": [
                        {"size": "34", "quantity": 5},
                        {"size": "35", "quantity": 0},
                        {"size": "36", "quantity": 0},
                        {"size": "37", "quantity": 0},
                        {"size": "38", "quantity": 0},
                        {"size": "39", "quantity": 10},
                        {"size": "40", "quantity": 0},
                        {"size": "41", "quantity": 0}
                    ]
                },
                {
                    "product_id": cls.product_clothes.id,
                    "price": '500.00',
                    "sizes": [
                        {"size": "2xs", "quantity": 0},
                        {"size": "xs", "quantity": 0},
                        {"size": "s", "quantity": 5},
                        {"size": "m", "quantity": 0},
                        {"size": "l", "quantity": 0},
                        {"size": "xl", "quantity": 0},
                        {"size": "2xl", "quantity": 10}
                    ]
                }
            ]
        }

    def test_create_document(self):
        created_document = self.purchase_exporter.create_document()
        document = Document.objects.get(id=created_document.id)
        self.assertEqual(document.kind, KIND_CHOICE.purchase)
        self.assertEqual(document.direction, DIRECTION_CHOICE.outgoing)
        self.assertJSONEqual(json.dumps(document.data), self.valid_data)

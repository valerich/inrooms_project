import datetime
import json
from decimal import Decimal

from django.test import TestCase
from freezegun import freeze_time

from core.server_init import ServerInit
from products.factories import ProductCollectionFactory, ProductFactory
from products.models import SERIES_TYPE_CHOICES
from supply.factories import SupplyFactory, SupplyItemFactory, SupplyStatusFactory
from warehouses.models import Warehouse
from ..models import DIRECTION_CHOICE, KIND_CHOICE, Document
from ..utils.supply_exporter import SupplyExporter


@freeze_time("2016-1-31 00:00:00")
class SupplyExporterTestCase(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super(SupplyExporterTestCase, cls).setUpClass()

        server_init = ServerInit()
        server_init.create_product_sizes()
        server_init.create_warehouses()

        main_collection_c = ProductCollectionFactory(series_type=SERIES_TYPE_CHOICES.clothes)
        main_collection_s = ProductCollectionFactory(series_type=SERIES_TYPE_CHOICES.shoes)
        cls.product_clothes = ProductFactory(main_collection=main_collection_c, price=100)
        cls.product_shoes = ProductFactory(main_collection=main_collection_s, price=200)

        warehouse_from = Warehouse.objects.get(code='chn')
        warehouse_to = Warehouse.objects.get(code='msk')

        status = SupplyStatusFactory(code='delivery_waiting')

        cls.supply = SupplyFactory(
            status=status,
            all_amount_paid_rur=Decimal('30.00'),
            delivery_date=datetime.date(2016, 2, 19),
            delivery_warehouse_from=warehouse_from,
            delivery_warehouse_to=warehouse_to,
        )
        SupplyItemFactory(
            supply=cls.supply,
            product=cls.product_clothes,
            price=500,
            count_s=5,
            count_xxl=10,
            summ_rur=Decimal("16.8450000000"),
        )
        SupplyItemFactory(
            supply=cls.supply,
            product=cls.product_shoes,
            price=1000,
            count_34=5,
            count_39=10,
            summ_rur=Decimal("31.8450000000"),
        )

        cls.supply_exporter = SupplyExporter(cls.supply.id)

        cls.valid_data = {
            "id": cls.supply.id,
            "status": {
                "code": "delivery_waiting"
            },
            "created": "2016-01-31T00:00:00Z",
            "delivery": {
                "date": "19.02.2016",
                "warehouse_from": {
                    "code": "chn",
                },
                "warehouse_to": {
                    "code": "msk",
                }
            },
            "items": [
                {
                    "product_id": cls.product_shoes.id,
                    "price": '1000.00',
                    "cost_price": "3.1230000000",
                    "sizes": [
                        {"size": "34", "quantity": 5},
                        {"size": "35", "quantity": 0},
                        {"size": "36", "quantity": 0},
                        {"size": "37", "quantity": 0},
                        {"size": "38", "quantity": 0},
                        {"size": "39", "quantity": 10},
                        {"size": "40", "quantity": 0},
                        {"size": "41", "quantity": 0}
                    ]
                },
                {
                    "product_id": cls.product_clothes.id,
                    "price": '500.00',
                    "cost_price": "2.1230000000",
                    "sizes": [
                        {"size": "2xs", "quantity": 0},
                        {"size": "xs", "quantity": 0},
                        {"size": "s", "quantity": 5},
                        {"size": "m", "quantity": 0},
                        {"size": "l", "quantity": 0},
                        {"size": "xl", "quantity": 0},
                        {"size": "2xl", "quantity": 10}
                    ]
                }
            ]
        }

    def test_create_document(self):
        created_document = self.supply_exporter.create_document()
        document = Document.objects.get(id=created_document.id)
        self.assertEqual(document.kind, KIND_CHOICE.supply)
        self.assertEqual(document.direction, DIRECTION_CHOICE.outgoing)
        self.assertJSONEqual(json.dumps(document.data), self.valid_data)

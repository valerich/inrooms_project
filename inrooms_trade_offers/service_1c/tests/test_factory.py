from django.test import TestCase

from ..factories import DocumentFactory


class FactoryTestCase(TestCase):

    def test_document_factory(self):
        document = DocumentFactory()
        self.assertIsNotNone(document.id)

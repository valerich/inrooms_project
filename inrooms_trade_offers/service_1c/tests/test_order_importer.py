import datetime

from django.db.models.aggregates import Sum
from django.test import TestCase
from django.utils import timezone

from clients.factories import ClientFactory
from core.server_init import ServerInit
from orders.factories import OrderFactory, OrderItemFactory, OrderItemSizeFactory
from orders.models import Order, OrderItem, OrderItemSize, OrderStatus
from products.factories import ProductCollectionFactory, ProductFactory
from products.models import SERIES_TYPE_CHOICES, Size
from service_1c.utils.order_importer.importer import OrderImporter
from warehouses.models import Warehouse
from ..factories import DocumentFactory
from ..models import DIRECTION_CHOICE, KIND_CHOICE, STATUS_CHOICE, Document


class OrderImporterTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(OrderImporterTestCase, cls).setUpClass()

        server_init = ServerInit()
        server_init.create_groups()
        server_init.create_product_sizes()
        server_init.create_order_statuses()
        server_init.create_warehouses()

        cls.old_status = OrderStatus.objects.get(code='reserved')
        cls.new_status = OrderStatus.objects.get(code='shipment_preparing')

        main_collection_c = ProductCollectionFactory(series_type=SERIES_TYPE_CHOICES.clothes)
        main_collection_s = ProductCollectionFactory(series_type=SERIES_TYPE_CHOICES.shoes)
        cls.product_clothes = ProductFactory(main_collection=main_collection_c, price=100)
        cls.product_shoes = ProductFactory(main_collection=main_collection_s, price=200)

        cls.old_order_client = ClientFactory()
        cls.order_client = ClientFactory()

        cls.order = OrderFactory(status=cls.old_status, client=cls.old_order_client)

        cls.valid_data = {
            "categories": [{
                "category_id": 1,
                "items": [{
                    "data": [{
                        "size": "s",
                        "quantity": 30,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 10,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 20,
                        "warehouse": {
                            "code": "showroom"
                        }
                    }],
                    "price": 20.00,
                    "product_id": cls.product_shoes.id
                }, {
                    "data": [{
                        "size": "s",
                        "quantity": 30,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 10,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 20,
                        "warehouse": {
                            "code": "showroom"
                        }
                    }],
                    "price": 10.00,
                    "product_id": cls.product_clothes.id
                }]
            }, {
                "category_id": 2,
                "items": [{
                    "data": [{
                        "size": "s",
                        "quantity": 30,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 10,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 20,
                        "warehouse": {
                            "code": "showroom"
                        }
                    }],
                    "price": 20.00,
                    "product_id": cls.product_shoes.id
                }, {
                    "data": [{
                        "size": "s",
                        "quantity": 30,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 10,
                        "warehouse": {
                            "code": "msk"
                        }
                    }, {
                        "size": "m",
                        "quantity": 20,
                        "warehouse": {
                            "code": "showroom"
                        }
                    }],
                    "price": 10.00,
                    "product_id": cls.product_clothes.id
                }]
            }],
            "client": {
                "id": cls.order_client.id
            },
            "status": {
                "code": cls.new_status.code
            },
            "created": "2016-01-31T00:00:00Z",
            "id": cls.order.id
        }

        for i in range(5):
            order_item = OrderItemFactory(order=cls.order)
            for i in range(5):
                OrderItemSizeFactory(order_item=order_item)

        # Генерируем все возможные документы
        for kind in KIND_CHOICE._db_values:
            for status in STATUS_CHOICE._db_values:
                for direction in DIRECTION_CHOICE._db_values:
                    DocumentFactory(kind=kind, status=status, direction=direction)

        cls.to_work_document_id = Document.objects.get(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.order_import,
            status=Document.STATUS.new,
        ).id

    def test_get_documents(self):
        self.order_importer = OrderImporter()
        self.order_importer._get_documents()
        self.assertEqual(len(self.order_importer._documents), 1)
        document = self.order_importer._documents[0]
        self.assertEqual(document.id, self.to_work_document_id)

    def test_set_documents_process(self):
        self.order_importer = OrderImporter()
        self.order_importer._set_documents_process()
        self.assertEqual(self.order_importer.document_qs.count(), 0)
        to_work_document = Document.objects.get(id=self.to_work_document_id)
        self.assertEqual(to_work_document.status,
                         STATUS_CHOICE.process)
        to_work_document.status = STATUS_CHOICE.new
        to_work_document.save()
    #
    def test_set_document_error(self):
        self.order_importer = OrderImporter()
        document = Document.objects.get(id=self.to_work_document_id)
        error_message = 'Тестовая ошибка'
        self.order_importer.set_document_error(document, error_message)
        document.refresh_from_db()
        self.assertEqual(document.status, STATUS_CHOICE.error)
        self.assertEqual(document.result, error_message)
        document.result = ''
        document.status = STATUS_CHOICE.new
        document.save()
    #
    def test_set_document_complete(self):
        self.order_importer = OrderImporter()
        document = Document.objects.get(id=self.to_work_document_id)
        self.order_importer.set_document_complete(document)
        document.refresh_from_db()
        self.assertEqual(document.status, STATUS_CHOICE.complete)
        document.status = STATUS_CHOICE.new
        document.save()
    #
    def test_parse_document(self):
        self.order_importer = OrderImporter()
        document = Document.objects.get(id=self.to_work_document_id)

        # Не словарь
        document.data = []
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не верный формат документа')

        # Не переданы данные.
        document.data = {}
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(len(data['errors']), 3)

        # Не передан order_id
        document.data = {'status': {'code': 'delivery_accepted'},
                         'client': {'id': self.order_client.id}}
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не передан id')

        # Нет заказа с таким id
        document.data = {'status': {'code': 'delivery_accepted'},
                         'client': {'id': self.order_client.id},
                         'id': 100500}
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не найден заказ с id: 100500')

        # Нет передан status_code
        document.data = {'client': {'id': self.order_client.id},
                         'id': self.order.id}
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не передан status_code')

        # Нет статуса с таким status_code
        document.data = {'status': {'code': 'fail_status_code'},
                         'client': {'id': self.order_client.id},
                         'id': self.order.id}
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не найден статус с code: fail_status_code')

        # Нет передан client_id
        document.data = {'status': {'code': 'new'},
                         'id': self.order.id}
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не передан client.id')

        # Нет статуса с таким client.id
        document.data = {'status': {'code': 'new'},
                         'client': {'id': 100500},
                         'id': self.order.id}
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не найден клиент с id: 100500')

        # Не передан category_id
        document.data = {
            'status': {'code': 'new'},
            'client': {'id': self.order_client.id},
            'id': self.order.id,
            'categories': [{}]
        }
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не передан category_id')

        # Не передан product_id
        document.data = {
            'status': {'code': 'new'},
            'client': {'id': self.order_client.id},
            'id': self.order.id,
            'categories': [{
                "category_id": 1,
                "items": [{}]
            }]
        }
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не передан product_id')

        # Не верный product_id
        document.data = {
            'status': {'code': 'new'},
            'client': {'id': self.order_client.id},
            'id': self.order.id,
            'categories': [{
                "category_id": 1,
                "items": [{
                    "product_id": 100500
                }]
            }]
        }
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не найден товар с id: 100500')

        # Не передан price
        document.data = {
            'status': {'code': 'new'},
            'client': {'id': self.order_client.id},
            'id': self.order.id,
            'categories': [{
                "category_id": 1,
                "items": [{
                    "product_id": self.product_clothes.id,
                }]
            }]
        }
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не передан price')

        # Не передан size
        document.data = {
            'status': {'code': 'new'},
            'client': {'id': self.order_client.id},
            'id': self.order.id,
            'categories': [{
                "category_id": 1,
                "items": [{
                    "product_id": self.product_clothes.id,
                    "price": 100.00,
                    "data": [{
                        "quantity": 30,
                        "warehouse": {
                            "code": "msk"
                        }
                    }]
                }]
            }]
        }
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не передан size')

        # Не верный size
        document.data = {
            'status': {'code': 'new'},
            'client': {'id': self.order_client.id},
            'id': self.order.id,
            'categories': [{
                "category_id": 1,
                "items": [{
                    "product_id": self.product_clothes.id,
                    "price": 100.00,
                    "data": [{
                        "size": "fail_size_code",
                        "quantity": 30,
                        "warehouse": {
                            "code": "msk"
                        }
                    }]
                }]
            }]
        }
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не найден размер с 1с_code: fail_size_code')

        # Не передан warehouse.code
        document.data = {
            'status': {'code': 'new'},
            'client': {'id': self.order_client.id},
            'id': self.order.id,
            'categories': [{
                "category_id": 1,
                "items": [{
                    "product_id": self.product_clothes.id,
                    "price": 100.00,
                    "data": [{
                        "quantity": 30,
                        "size": "s",
                    }]
                }]
            }]
        }
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не передан warehouse.code')

        # Не верный warehouse.code
        document.data = {
            'status': {'code': 'new'},
            'client': {'id': self.order_client.id},
            'id': self.order.id,
            'categories': [{
                "category_id": 1,
                "items": [{
                    "product_id": self.product_clothes.id,
                    "price": 100.00,
                    "data": [{
                        "size": "s",
                        "quantity": 30,
                        "warehouse": {
                            "code": "fail_warehouse_code"
                        }
                    }]
                }]
            }]
        }
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не найдена точка с code: fail_warehouse_code')

        # Не верный формат quantity
        document.data = {
            'status': {'code': 'new'},
            'client': {'id': self.order_client.id},
            'id': self.order.id,
            'categories': [{
                "category_id": 1,
                "items": [{
                    "product_id": self.product_clothes.id,
                    "price": 100.00,
                    "data": [{
                        "size": "s",
                        "quantity": "fail_quantity",
                        "warehouse": {
                            "code": "msk"
                        }
                    }]
                }]
            }]
        }
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не верный формат quantity: fail_quantity')

        # Валидный документ
        document.data = self.valid_data
        data = self.order_importer.parse_document(document)
        self.assertEqual(data['is_valid'], True)
        self.assertEqual(len(data['errors']), 0)

    def test_quantity(self):
        self.order_importer = OrderImporter()
        order_item = OrderItemFactory(order=self.order, product=self.product_shoes)
        order_item_size = OrderItemSizeFactory(order_item=order_item,
                                               size=Size.objects.get(code='l'),
                                               warehouse=Warehouse.objects.get(code='msk'),
                                               quantity=200)
        Document.objects.all().update(status=200)
        Document.objects.filter(id=self.to_work_document_id).update(status=STATUS_CHOICE.new, data=self.valid_data)
        self.order_importer.run()

        shoes_quantity = OrderItemSize.objects.filter(
            order_item__order=self.order,
            order_item__product=self.product_shoes).aggregate(Sum('quantity'))['quantity__sum']
        self.assertEqual(shoes_quantity, 120)

        shoes_quantity = OrderItemSize.objects.filter(
            order_item__order=self.order,
            order_item__product=self.product_clothes).aggregate(Sum('quantity'))['quantity__sum']
        self.assertEqual(shoes_quantity, 120)

        shoes_quantity_m = OrderItemSize.objects.filter(
            order_item__order=self.order,
            order_item__product=self.product_clothes,
            size=Size.objects.get(code='m')
        ).aggregate(Sum('quantity'))['quantity__sum']
        self.assertEqual(shoes_quantity_m, 60)

    def test_run(self):
        self.order_importer = OrderImporter()
        Document.objects.update(status=STATUS_CHOICE.new, )
        # Проблемный документ
        self.order_importer.run()
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.error).count(), 4)
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.complete).count(), 0)

        Document.objects.update(status=STATUS_CHOICE.new, data=self.valid_data)
        # Валидный документ
        self.order_importer.run()
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.error).count(), 0)
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.complete).count(), 4)

        order = Order.objects.get(id=self.order.id)

        self.assertEqual(order.status.code, self.new_status.code)
        self.assertEqual(order.client.id, self.order_client.id)
        self.assertEqual(OrderItem.objects.filter(order=order).count(), 2)
        self.assertEqual(OrderItemSize.objects.filter(order_item__order=order).count(), 12)

    def test_document_queue(self):
        """Очередность обработки документов

        first in, first out
        """

        Document.objects.all().delete()
        doc1 = DocumentFactory(
            created=timezone.make_aware(datetime.datetime(2015, 1, 5), timezone.get_current_timezone()),
            kind=KIND_CHOICE.order_import,
            status=STATUS_CHOICE.new,
            direction=DIRECTION_CHOICE.incoming)
        doc2 = DocumentFactory(
            created=timezone.make_aware(datetime.datetime(2015, 1, 3), timezone.get_current_timezone()),
            kind=KIND_CHOICE.order_import,
            status=STATUS_CHOICE.new,
            direction=DIRECTION_CHOICE.incoming)
        doc3 = DocumentFactory(
            created=timezone.make_aware(datetime.datetime(2015, 1, 7), timezone.get_current_timezone()),
            kind=KIND_CHOICE.order_import,
            status=STATUS_CHOICE.new,
            direction=DIRECTION_CHOICE.incoming)

        order_importer = OrderImporter()
        qs = order_importer.get_document_queryset()

        self.assertEqual(qs[0].id, doc2.id)
        self.assertEqual(qs[1].id, doc1.id)
        self.assertEqual(qs[2].id, doc3.id)

import datetime

from django.test import TestCase
from django.utils import timezone

from core.server_init import ServerInit
from purchases.factories import PurchaseFactory
from purchases.models import Purchase, PurchaseStatus
from ..factories import DocumentFactory
from ..models import DIRECTION_CHOICE, KIND_CHOICE, STATUS_CHOICE, Document
from ..utils.purchase_status_parser import PurchaseStatusParser


class PurchaseStatusParserTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(PurchaseStatusParserTestCase, cls).setUpClass()

        server_init = ServerInit()
        server_init.create_groups()
        server_init.create_purchase_statuses()

        cls.old_status = PurchaseStatus.objects.get(code='delivery_waiting')
        cls.new_status = PurchaseStatus.objects.get(code='delivery_accepted')

        cls.purchase = PurchaseFactory(status=cls.old_status)

        cls.valid_data = {
            'status_code': cls.new_status.code,
            'purchase_id': cls.purchase.id
        }

        cls.purchase_status_parser = PurchaseStatusParser()

        # Генерируем все возможные документы
        for kind in KIND_CHOICE._db_values:
            for status in STATUS_CHOICE._db_values:
                for direction in DIRECTION_CHOICE._db_values:
                    DocumentFactory(kind=kind, status=status, direction=direction)

        cls.to_work_document_id = Document.objects.get(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.purchase_status_change,
            status=Document.STATUS.new,
        ).id

    def test_get_documents(self):
        self.purchase_status_parser._get_documents()
        self.assertEqual(len(self.purchase_status_parser._documents), 1)
        document = self.purchase_status_parser._documents[0]
        self.assertEqual(document.id, self.to_work_document_id)

    def test_set_documents_process(self):
        self.purchase_status_parser._set_documents_process()
        self.assertEqual(self.purchase_status_parser.document_qs.count(), 0)
        to_work_document = Document.objects.get(id=self.to_work_document_id)
        self.assertEqual(to_work_document.status,
                         STATUS_CHOICE.process)
        to_work_document.status = STATUS_CHOICE.new
        to_work_document.save()

    def test_set_document_error(self):
        document = Document.objects.get(id=self.to_work_document_id)
        error_message = 'Тестовая ошибка'
        self.purchase_status_parser.set_document_error(document, error_message)
        document.refresh_from_db()
        self.assertEqual(document.status, STATUS_CHOICE.error)
        self.assertEqual(document.result, error_message)
        document.result = ''
        document.status = STATUS_CHOICE.new
        document.save()

    def test_set_document_complete(self):
        document = Document.objects.get(id=self.to_work_document_id)
        self.purchase_status_parser.set_document_complete(document)
        document.refresh_from_db()
        self.assertEqual(document.status, STATUS_CHOICE.complete)
        document.status = STATUS_CHOICE.new
        document.save()

    def test_parse_document(self):
        document = Document.objects.get(id=self.to_work_document_id)

        # Не словарь
        document.data = []
        data = self.purchase_status_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не верный формат документа')

        # Не переданы данные.
        document.data = {}
        data = self.purchase_status_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(len(data['errors']), 2)

        # Не передан purchase_id
        document.data = {'status_code': 'delivery_accepted'}
        data = self.purchase_status_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не передан purchase_id')

        # Нет поставки с таким id
        document.data = {'status_code': 'delivery_accepted',
                         'purchase_id': 100500}
        data = self.purchase_status_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не найдена закупка с id: 100500')

        # Нет передан status_code
        document.data = {'purchase_id': self.purchase.id}
        data = self.purchase_status_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не передан status_code')

        # Нет статуса с таким status_code
        document.data = {'status_code': 'fail_status_code',
                         'purchase_id': self.purchase.id}
        data = self.purchase_status_parser.parse_document(document)
        self.assertEqual(data['is_valid'], False)
        self.assertEqual(data['errors'][0], 'Не найден статус с code: fail_status_code')

        # Валидный документ
        document.data = self.valid_data
        data = self.purchase_status_parser.parse_document(document)
        self.assertEqual(data['is_valid'], True)
        self.assertEqual(len(data['errors']), 0)

    def test_run(self):
        Document.objects.update(status=STATUS_CHOICE.new, )
        # Проблемный документ
        self.purchase_status_parser.run()
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.error).count(), 1)
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.complete).count(), 0)

        Document.objects.update(status=STATUS_CHOICE.new, data=self.valid_data)
        # Валидный документ
        self.purchase_status_parser.run()
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.error).count(), 0)
        self.assertEqual(Document.objects.filter(status=STATUS_CHOICE.complete).count(), 4)
        self.assertEqual(Purchase.objects.get(id=self.purchase.id).status.code, self.new_status.code)

    def test_document_queue(self):
        """Очередность обработки документов

        first in, first out
        """

        Document.objects.all().delete()
        doc1 = DocumentFactory(
            created=timezone.make_aware(datetime.datetime(2015, 1, 5), timezone.get_current_timezone()),
            kind=KIND_CHOICE.purchase_status_change,
            status=STATUS_CHOICE.new,
            direction=DIRECTION_CHOICE.incoming)
        doc2 = DocumentFactory(
            created=timezone.make_aware(datetime.datetime(2015, 1, 3), timezone.get_current_timezone()),
            kind=KIND_CHOICE.purchase_status_change,
            status=STATUS_CHOICE.new,
            direction=DIRECTION_CHOICE.incoming)
        doc3 = DocumentFactory(
            created=timezone.make_aware(datetime.datetime(2015, 1, 7), timezone.get_current_timezone()),
            kind=KIND_CHOICE.purchase_status_change,
            status=STATUS_CHOICE.new,
            direction=DIRECTION_CHOICE.incoming)

        purchase_status_parser = PurchaseStatusParser()
        qs = purchase_status_parser.get_document_queryset()

        self.assertEqual(qs[0].id, doc2.id)
        self.assertEqual(qs[1].id, doc1.id)
        self.assertEqual(qs[2].id, doc3.id)

import datetime
import json

from django.test import TestCase

from clients.factories import ClientFactory
from stores.factories import StoreFactory
from ..utils.store_exporter import StoreExporter


class StoreExporterTestCase(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super(StoreExporterTestCase, cls).setUpClass()

        cls.client_1 = ClientFactory()
        cls.client_2 = ClientFactory()

        cls.store_1 = StoreFactory(
            client=cls.client_1,
            name='name1',
            address='address1',
            phone='1111111111',
            opening='opening1',
            total_area=1,
            trade_area=11,
            employees_num=111,
            it_info='it_info1'
        )
        cls.store_2 = StoreFactory(
            client=cls.client_2,
            name='name2',
            address='address2',
            phone='2222222222',
            opening='opening2',
            total_area=2,
            trade_area=22,
            employees_num=222,
            it_info='it_info2'
        )

        cls.serializer_valid_data = [
            {
                "id": cls.store_1.id,
                "client": cls.client_1.id,
                "name": 'name1',
                "address": 'address1',
                "phone": '1111111111',
                "opening": 'opening1',
                "total_area": 1,
                "trade_area": 11,
                "employees_num": 111,
                "it_info": 'it_info1'
            },
            {
                "id": cls.store_2.id,
                "client": cls.client_2.id,
                "name": 'name2',
                "address": 'address2',
                "phone": '2222222222',
                "opening": 'opening2',
                "total_area": 2,
                "trade_area": 22,
                "employees_num": 222,
                "it_info": 'it_info2'
            },
        ]

    def test_get_stores(self):
        store_exporter = StoreExporter([self.store_1.id, ])
        stores = store_exporter.get_objects()
        self.assertEqual(stores.count(), 1)
        self.assertEqual(stores[0].id, self.store_1.id)

        # Оба клиента
        store_exporter = StoreExporter([self.store_1.id, self.store_2.id, ])
        stores = store_exporter.get_objects()
        self.assertEqual(stores.count(), 2)

    def test_store_serializer(self):
        store_exporter = StoreExporter([self.store_1.id, self.store_2.id])
        stores = store_exporter.get_objects()
        data = store_exporter.get_objects_data(stores)
        self.assertJSONEqual(json.dumps(data), self.serializer_valid_data)

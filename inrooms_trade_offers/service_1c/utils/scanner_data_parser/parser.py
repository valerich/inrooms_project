from django.db import transaction

from bot.utils import send_telegram_message
from orders.models import ScannerData
from products.models import Product
from ...models import Document


class ScannerDataParser(object):

    def __init__(self):
        self.document_qs = self.get_document_queryset()
        self.products = {p.id: p for p in Product.objects.all()}

    def run(self):
        with transaction.atomic():
            self._get_documents()
            self._set_documents_process()
        self.process_documents()

    def get_document_queryset(self):
        return Document.objects.filter(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.scanner_data,
            status=Document.STATUS.new,
        ).order_by('created')

    def _get_documents(self):
        """Получаем не обработанные документы"""

        self._documents = list(self.document_qs)

    def _set_documents_process(self):
        """Выставляем документам статус "В обработке" """

        self.document_qs.update(status=Document.STATUS.process)

    def process_documents(self):
        for document in self._documents:
            self.process_document(document)

    def process_document(self, document: Document):
        parse_document_data = self.parse_document(document)
        document.result = ',\n'.join(parse_document_data['errors'])
        if parse_document_data['is_valid']:
            self._save_data(parse_document_data['data'])
            self.set_document_complete(document)
        else:
            self.set_document_error(document)

    def _save_data(self, parse_document_data: list):
        scanner_data = ScannerData.objects.create()
        for item in parse_document_data:
            scanner_data.products.add(item)

    def parse_document(self, document: Document) -> dict:
        """Разбираем документ

        :param document: Обрабатываемый документ
        :rtype: dict
        :return: Признак валидности документа, подготовленные данные для сохранения в базу, список ошибок

        """
        raw_data = document.data
        data = set()
        errors = []
        is_valid = True

        if isinstance(raw_data, list):
            for raw_product_id in raw_data:
                try:
                    product = self.products[raw_product_id]
                except KeyError:
                    errors.append('Не найден товар с id: {}'.format(raw_product_id))
                else:
                    data.add(product)
        else:
            errors.append('Не верный формат документа')
            is_valid = False
        return {'is_valid': is_valid,
                'data': data,
                'errors': errors}

    def set_document_error(self, document: Document):
        """Пометить документ как обработанный с ошибкой

        :type result: str
        :param result: Описание ошибки
        :param document: Обрабатываемый документ

        """

        document.status = Document.STATUS.error
        document.save()

        send_telegram_message(message=str(document), url=document.get_absolute_url())

    def set_document_complete(self, document: Document):
        """Пометить документ как обработанный

        :param document: Обрабатываемый документ

        """

        document.status = Document.STATUS.complete
        document.save()

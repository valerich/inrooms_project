from django.db import transaction

from bot.utils import send_telegram_message
from purchases.models import Purchase, PurchaseStatus
from ...models import Document


class PurchaseStatusParser(object):

    def __init__(self):
        self.document_qs = self.get_document_queryset()
        self.status_codes = {s.code: s for s in PurchaseStatus.objects.all()}

    def run(self):
        with transaction.atomic():
            self._get_documents()
            self._set_documents_process()
        self.process_documents()

    def get_document_queryset(self):
        return Document.objects.filter(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.purchase_status_change,
            status=Document.STATUS.new,
        ).order_by('created')

    def _get_documents(self):
        """Получаем не обработанные документы на изменение статуса поставки от 1с"""

        self._documents = list(self.document_qs)

    def _set_documents_process(self):
        """Выставляем документам статус "В обработке" """

        self.document_qs.update(status=Document.STATUS.process)

    def process_documents(self):
        for document in self._documents:
            self.process_document(document)

    def process_document(self, document: Document):
        parse_document_data = self.parse_document(document)
        if parse_document_data['is_valid']:
            self._update_purchase_status(parse_document_data['data'])
            self.set_document_complete(document)
        else:
            self.set_document_error(document, ',\n'.join(parse_document_data['errors']))

    def _update_purchase_status(self, parse_document_data: dict):
        purchase = parse_document_data['purchase']
        new_status = parse_document_data['status']
        purchase.status = new_status
        purchase.save()
        purchase.process_purchase()

    def parse_document(self, document: Document) -> dict:
        """Разбираем документ

        :param document: Обрабатываемый документ
        :rtype: dict
        :return: Признак валидности документа, подготовленные данные для сохранения в базу, список ошибок

        """
        raw_data = document.data
        errors = []
        purchase = None
        status = None

        if isinstance(raw_data, dict):
            purchase_id = raw_data.get('purchase_id', None)
            if purchase_id and isinstance(purchase_id, int):
                try:
                    purchase = Purchase.objects.get(id=purchase_id)
                except Purchase.DoesNotExist:
                    errors.append('Не найдена закупка с id: {}'.format(purchase_id))
            else:
                errors.append('Не передан purchase_id')

            status_code = raw_data.get('status_code', None)
            if status_code and isinstance(status_code, str):
                try:
                    status = PurchaseStatus.objects.get(code=status_code)
                except PurchaseStatus.DoesNotExist:
                    errors.append('Не найден статус с code: {}'.format(status_code))
            else:
                errors.append('Не передан status_code')

        else:
            errors.append('Не верный формат документа')
        return {'is_valid': False if errors else True,
                'data': {
                    'purchase': purchase,
                    'status': status,
                },
                'errors': errors}

    def set_document_error(self, document: Document, result: str):
        """Пометить документ как обработанный с ошибкой

        :type result: str
        :param result: Описание ошибки
        :param document: Обрабатываемый документ

        """

        document.result = result
        document.status = Document.STATUS.error
        document.save()

        send_telegram_message(message=str(document), url=document.get_absolute_url())

    def set_document_complete(self, document: Document):
        """Пометить документ как обработанный

        :param document: Обрабатываемый документ

        """

        document.status = Document.STATUS.complete
        document.save()

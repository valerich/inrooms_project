from collections import defaultdict

from decimal import Decimal, InvalidOperation
from django.db import transaction

from bot.utils import send_telegram_message
from products.models import Product, Remain, Size
from warehouses.models import Warehouse
from ...models import Document


class RemainParser(object):

    def __init__(self):
        self.document_qs = self.get_document_queryset()
        self.sizes_1c = {s.code_1c: s for s in Size.objects.all()}
        self.products = {p.id: p for p in Product.objects.all()}
        self.warehouses = {w.code: w for w in Warehouse.objects.all()}

    def run(self):
        with transaction.atomic():
            self._get_documents()
            self._set_documents_process()
        self.process_documents()

    def get_document_queryset(self):
        return Document.objects.filter(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.remains,
            status=Document.STATUS.new,
        ).order_by('created')

    def _get_documents(self):
        """Получаем не обработанные документы на изменение остатков от 1с"""

        self._documents = list(self.document_qs)

    def _set_documents_process(self):
        """Выставляем документам статус "В обработке" """

        self.document_qs.update(status=Document.STATUS.process)

    def process_documents(self):
        for document in self._documents:
            self.process_document(document)

    def process_document(self, document: Document):
        parse_document_data = self.parse_document(document)
        if parse_document_data['is_valid']:
            self._save_remains(parse_document_data['data'])
            self.set_document_complete(document)
        else:
            self.set_document_error(document, ',\n'.join(parse_document_data['errors']))

    def _save_remains(self, parse_document_data: list):
        for item in parse_document_data:
            for size, data in item['remains_data'].items():
                defaults = {
                    'value': data['value']
                }
                if item['warehouse'].take_cost_from_1c:
                    defaults['amount_rur'] = data['amount_rur']
                remain, c = Remain.objects.update_or_create(product=item['product'],
                                                            warehouse=item['warehouse'],
                                                            size=size,
                                                            defaults=defaults)

    def parse_document(self, document: Document) -> dict:
        """Разбираем документ

        :param document: Обрабатываемый документ
        :rtype: dict
        :return: Признак валидности документа, подготовленные данные для сохранения в базу, список ошибок

        """
        raw_data = document.data
        data = []
        errors = []

        if isinstance(raw_data, list):
            for raw_product_data in raw_data:
                if isinstance(raw_product_data, dict):
                    warehouse_code = raw_product_data.get('warehouse_code', 'msk')
                    try:
                        warehouse = self.warehouses[warehouse_code]
                    except KeyError:
                        errors.append('Не найден склад с code: {}'.format(warehouse_code))
                    else:
                        # Парсим остатки только для тех точек, для которых установлен разрешающий флаг
                        if warehouse.accept_1c_remains:
                            product_id = raw_product_data.get('product_id', None)
                            if product_id and isinstance(product_id, int):
                                try:
                                    product = self.products[product_id]
                                except KeyError:
                                    errors.append('Не найдена модель с id: {}'.format(product_id))
                                else:
                                    remains_data = defaultdict(lambda: defaultdict(int))
                                    raw_remains_data = raw_product_data.get('remains', None)
                                    if isinstance(raw_remains_data, list):
                                        for raw_remain_data in raw_remains_data:
                                            size_code = raw_remain_data.get('size', None)
                                            size = self.sizes_1c.get(size_code, None)
                                            if size:
                                                value = raw_remain_data.get('value', None)
                                                if isinstance(value, int):
                                                    remains_data[size]['value'] += value
                                                else:
                                                    errors.append('Не передан остаток')

                                                if warehouse.take_cost_from_1c:
                                                    amount_rur = raw_remain_data.get('amount_rur', None)
                                                    try:
                                                        amount_rur = Decimal(amount_rur)
                                                    except (InvalidOperation, TypeError):
                                                        errors.append('Значение amount_rur должно быть числом с плавающей точкой')
                                                    else:
                                                        remains_data[size]['amount_rur'] += amount_rur
                                            else:
                                                errors.append('Передан не существующий размер')
                                        data.append({'product': product,
                                                     'warehouse': warehouse,
                                                     'remains_data': remains_data})
                                    else:
                                        errors.append('Не верный формат документа')
                            else:
                                errors.append('Не верный product_id ({})'.format(product_id))

                else:
                    errors.append('Не верный формат документа')
        else:
            errors.append('Не верный формат документа')
        return {'is_valid': False if errors else True,
                'data': data,
                'errors': errors}

    def set_document_error(self, document: Document, result: str):
        """Пометить документ как обработанный с ошибкой

        :type result: str
        :param result: Описание ошибки
        :param document: Обрабатываемый документ

        """

        document.result = result
        document.status = Document.STATUS.error
        document.save()

        send_telegram_message(message=str(document), url=document.get_absolute_url())

    def set_document_complete(self, document: Document):
        """Пометить документ как обработанный

        :param document: Обрабатываемый документ

        """

        document.status = Document.STATUS.complete
        document.save()

from django.conf import settings

from rest_framework.renderers import JSONRenderer
from suds.client import Client

from bot.utils import send_telegram_message
from brands.models import Brand
from products.models import Product
from .serializers import ProductSerializer


class ProductExporter():
    model = Product
    serializer_class = ProductSerializer
    soap_wsdl_name = 'inrooms_1c_wsdl'
    soap_service_method = 'LoadProduct'

    def __init__(self, object_ids: list):
        self.object_ids = object_ids

    def run(self):
        try:
            objects = self.get_objects()
            brand_tech_data = self.get_brands_soap_client_parameters(objects)
            data = self.get_objects_data(objects, brand_tech_data)
            self.export_to_1c(data, brand_tech_data)
        except Exception as e:
            message = self.get_bot_error_message()
            if str(e):
                message += '\nДополнительные данные: ' + str(e)
            send_telegram_message(message=message)
            raise e

    def get_bot_error_message(self):
        return 'Не удалось выгрузить товар(ы) #{} в 1с'.format(self.object_ids)

    def get_queryset(self):
        return Product.all_objects.filter(id__in=self.object_ids).order_by('id')

    def get_brands_soap_client_parameters(self, queryset):
        data = {}
        for b in Brand.objects.filter(id__in=queryset.values_list('brand_id', flat=True)):
            if any([
                not b.product_export_url,
                not b.product_export_login,
                not b.product_export_pass
            ]):
                raise Exception('Не заданы параметры выгрузки в 1с для бренда - {}'.format(b.name))
            data[b.id] = {
                'url': b.product_export_url,
                'username': b.product_export_login,
                'password': b.product_export_pass,
            }
        return data

    def get_objects(self):
        return self.get_queryset().filter(id__in=self.object_ids)

    def get_objects_data(self, objects, brand_tech_data):
        data = {}
        for brand_id in brand_tech_data:
            data[brand_id] = self.serializer_class(objects.filter(brand_id=brand_id), many=True).data
        return data

    def export_to_1c(self, data, brand_tech_data):
        for brand_id, brand_data in data.items():
            data_json = JSONRenderer().render(data[brand_id]).decode('utf-8')
            client = self.get_soap_client(brand_tech_data[brand_id])
            if not self.soap_service_method:
                raise Exception('Не задан soap_service_method')
            result = getattr(client.service, self.soap_service_method)(data_json)
            if not any([result[0] != 200, result == '200']):
                send_telegram_message(message=self.get_bot_error_message())

    def get_soap_client(self, soap_client_parameters):
        return Client(**soap_client_parameters)

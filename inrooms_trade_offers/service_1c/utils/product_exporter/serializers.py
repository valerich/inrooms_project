from django.contrib.sites.models import Site
from rest_framework import serializers

from products.models import Product, ProductCollection, ProductColor


class ColorSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductColor
        fields = [
            'id',
            'name',
        ]


class ProductCollectionSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductCollection
        fields = [
            'id',
            'name',
            'series_type',
        ]


class ProductSerializer(serializers.ModelSerializer):
    color = ColorSerializer()
    main_collection = ProductCollectionSerializer()
    image = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = [
            'id',
            'article',
            'name',
            'description',
            'image',
            'created',
            'modified',
            'is_active',
            'color',
            'main_collection',
            'is_deleted',
            'price',
        ]

    def get_image(self, obj):
        pi = obj.images.all().first()
        return self.resize_image(pi)

    def resize_image(self, pi):
        domain = Site.objects.all()[0].domain
        if pi:
            src = pi.image.url
            return {'src': 'http://{}{}'.format(domain, src), }
        else:
            return None

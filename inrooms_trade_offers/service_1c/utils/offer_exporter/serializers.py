from django.contrib.sites.models import Site
from rest_framework import serializers

from offers.models import Offer


class OfferSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = Offer
        fields = [
            'id',
            'name',
            'image',
            'created',
            'modified',
        ]

    def get_image(self, obj):
        pi = obj.images.all().first()
        return self.resize_image(pi)

    def resize_image(self, pi):
        domain = Site.objects.all()[0].domain
        if pi:
            src = pi.image.url
            return {'src': 'http://{}{}'.format(domain, src), }
        else:
            return None

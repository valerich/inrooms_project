from bot.utils import send_telegram_message
from offers.models import Offer
from .serializers import OfferSerializer
from ...models import DIRECTION_CHOICE, KIND_CHOICE, Document


class OfferExporter(object):
    """Создание документов с данными о предложениях"""

    def __init__(self, object_ids: list):
        self.object_ids = object_ids

    def run(self):
        try:
            objects = self.get_objects()
            data = self.get_data(objects)
            self.export_to_1c(data)
        except Exception as e:
            message = self.get_bot_error_message()
            if str(e):
                message += '\nДополнительные данные: ' + str(e)
            send_telegram_message(message=message)
            raise e

    def get_bot_error_message(self):
        return 'Не удалось выгрузить предложение(я) #{} в 1с'.format(self.object_ids)

    def get_objects(self):
        return Offer.all_objects.filter(id__in=self.object_ids)

    def get_data(self, objects):
        return OfferSerializer(objects, many=True).data

    def export_to_1c(self, data):
        return Document.objects.create(
            kind=KIND_CHOICE.offer,
            direction=DIRECTION_CHOICE.outgoing,
            data=data
        )

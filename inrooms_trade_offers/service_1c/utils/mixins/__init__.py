from django.conf import settings

from rest_framework.renderers import JSONRenderer
from suds.client import Client

from bot.utils import send_telegram_message


class SoapExporterMixin():
    bot_error_message = 'Не удалось выгрузить объекты в 1с'
    model = None
    serializer_class = None
    soap_wsdl_name = None
    soap_service_method = None

    def __init__(self, object_ids: list):
        self.object_ids = object_ids

    def run(self):
        try:
            objects = self.get_objects()
            data = self.get_objects_data(objects)
            self.export_to_1c(data)
        except Exception as e:
            message = self.get_bot_error_message()
            if str(e):
                message += '\nДополнительные данные: ' + str(e)
            send_telegram_message(message=message)
            raise e

    def get_objects(self):
        return self.get_queryset().filter(id__in=self.object_ids)

    def get_objects_data(self, objects):
        return self.get_serializer()(objects, many=True).data

    def export_to_1c(self, data):
        data_json = JSONRenderer().render(data).decode('utf-8')
        client = self.get_soap_client()
        if not self.soap_service_method:
            raise Exception('Не задан soap_service_method')
        result = getattr(client.service, self.soap_service_method)(data_json)
        if not any([result[0] != 200, result == '200']):
            send_telegram_message(message=self.get_bot_error_message())

    def get_bot_error_message(self):
        return self.bot_error_message

    def get_queryset(self):
        if self.model is None:
            raise Exception('Не задана модель')
        return self.model.objects.all().order_by('id')

    def get_serializer(self):
        if self.serializer_class is None:
            raise Exception('Не задан сериалайзер')
        return self.serializer_class

    def get_soap_client(self):
        return Client(**self.get_soap_client_parameters())

    def get_soap_client_parameters(self):
        if self.soap_wsdl_name is None:
            raise Exception('Не задан soap wsdl name')
        return settings.SERVICE_1C[self.soap_wsdl_name]





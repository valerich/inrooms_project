from orders.models import RefundAct
from .serializers import RefundActSerializer
from ...models import DIRECTION_CHOICE, KIND_CHOICE, Document


class RefundActExporter(object):
    """Создание документов с данными о акте на возврат для выгрузки в 1с"""

    def __init__(self, defect_act_id: int):
        self.object = RefundAct.objects.get(id=defect_act_id)

    def get_object_data(self):
        data = RefundActSerializer(self.object).data
        return data

    def create_document(self):
        object_data = self.get_object_data()
        return Document.objects.create(
            kind=KIND_CHOICE.refund_act,
            direction=DIRECTION_CHOICE.outgoing,
            data=object_data
        )

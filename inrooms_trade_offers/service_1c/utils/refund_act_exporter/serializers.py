from rest_framework import serializers

from clients.models import Client
from orders.models import RefundAct, RefundActStatus


class RefundActStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = RefundActStatus
        fields = [
            'code',
        ]


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = [
            'id',
        ]


class RefundActSerializer(serializers.ModelSerializer):
    client = ClientSerializer()
    status = RefundActStatusSerializer()
    items = serializers.SerializerMethodField()

    class Meta:
        model = RefundAct

        fields = [
            'id',
            'client',
            'status',
            'created',
            'items',
        ]

    def get_items(self, obj):
        data = []
        for item in obj.items.all():
            data.append({
                'product_id': item.order_item.product_id,
                'order_id': item.order_item.order_id,
                'price': str(item.order_item.price),
                'quantity': item.quantity,
                'size': item.size.code_1c,
            })
        return data

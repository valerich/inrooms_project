from rest_framework import serializers

from products.models import Size
from supply.models import Supply, SupplyItem, SupplyStatus
from warehouses.models import Warehouse


class SupplyItemSerializer(serializers.ModelSerializer):
    product_id = serializers.SerializerMethodField()
    sizes = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()
    cost_price = serializers.SerializerMethodField()

    class Meta:
        model = SupplyItem
        fields = [
            'product_id',
            'price',
            'sizes',
            'cost_price',
        ]

    def get_sizes(self, obj):
        data = []
        for size in Size.objects.filter(series_type=obj.series_type):
            data.append({
                'size': size.code_1c,
                'quantity': getattr(obj, 'count_{}'.format(size.code), 0),
            })
        return data

    def get_product_id(self, obj):
        return obj.product_id

    def get_price(self, obj):
        if obj.price is not None:
            return str(obj.price)
        return None

    def get_cost_price(self, obj):
        return str(obj.get_cost_price())


class WarehouseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Warehouse
        fields = [
            'code',
        ]


class SupplyStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = SupplyStatus
        fields = [
            'code',
        ]


class SupplySerializer(serializers.ModelSerializer):
    status = SupplyStatusSerializer()
    delivery = serializers.SerializerMethodField()
    number_retail = serializers.SerializerMethodField()
    items = SupplyItemSerializer(many=True, read_only=True)

    class Meta:
        model = Supply

        fields = [
            'id',
            'status',
            'created',
            'delivery',
            'number_retail',
            'items',
        ]

    def get_status_code(self, obj):
        return obj.status.code

    def get_delivery(self, obj):
        return {
            'date': obj.delivery_date.strftime('%d.%m.%Y'),
            'warehouse_from': WarehouseSerializer(obj.delivery_warehouse_from).data,
            'warehouse_to': WarehouseSerializer(obj.delivery_warehouse_to).data,
        }

    def get_number_retail(self, obj):
        return obj.number_1c

from prices.models import PriceSet
from .serializers import PriceSetSerializer
from ...models import DIRECTION_CHOICE, KIND_CHOICE, Document


class PriceSetExporter(object):
    """Создание документов переоценки для выгрузки в 1с"""

    def __init__(self, object_id: int):
        self.object = PriceSet.objects.get(id=object_id)

    def get_object_data(self):
        data = PriceSetSerializer(self.object).data
        return data

    def create_document(self):
        object_data = self.get_object_data()
        return Document.objects.create(
            kind=KIND_CHOICE.price_set,
            direction=DIRECTION_CHOICE.outgoing,
            data=object_data
        )

from rest_framework import serializers

from prices.models import PriceSet, PriceSetItem


class PriceSetItemSerializer(serializers.ModelSerializer):
    product_id = serializers.SerializerMethodField()
    new_price = serializers.SerializerMethodField()

    class Meta:
        model = PriceSetItem
        fields = [
            'product_id',
            'new_price',
        ]

    def get_product_id(self, obj):
        return obj.product_id

    def get_new_price(self, obj):
        if obj.new_price:
            return str(obj.new_price)
        return None


class PriceSetSerializer(serializers.ModelSerializer):
    items = serializers.SerializerMethodField()
    client_id = serializers.SerializerMethodField()

    class Meta:
        model = PriceSet

        fields = [
            'id',
            'created',
            'client_id',
            'items'
        ]

    def get_items(self, obj):
        data = []
        for item in obj.items.all():
            data.append(PriceSetItemSerializer(item).data)
        return data

    def get_client_id(self, obj):
        return obj.client_id

from rest_framework import serializers

from clients.models import Client
from core.utils.constants import ORDER_CATEGORY_CHOICE
from orders.models import Order, OrderItem, OrderStatus
from warehouses.models import Warehouse


class OrderItemSerializer(serializers.ModelSerializer):
    product_id = serializers.SerializerMethodField()
    data = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()

    class Meta:
        model = OrderItem
        fields = [
            'product_id',
            'price',
            'data',
        ]

    def get_data(self, obj):
        data = []
        for order_item_size in obj.orderitemsize_set.filter(category=self.context['category_id']):
            data.append({
                'size': order_item_size.size.code_1c,
                'warehouse': WarehouseSerializer(order_item_size.warehouse).data,
                'quantity': order_item_size.quantity,
                'is_delivery_reserve': order_item_size.is_delivery_reserve,
            })
        return data

    def get_product_id(self, obj):
        return obj.product_id

    def get_price(self, obj):
        if obj.price is not None:
            return str(obj.price)
        return None


class WarehouseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Warehouse
        fields = [
            'code',
        ]


class OrderStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderStatus
        fields = [
            'code',
        ]


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = [
            'id',
        ]


class OrderSerializer(serializers.ModelSerializer):
    client = ClientSerializer()
    status = OrderStatusSerializer()
    categories = serializers.SerializerMethodField()

    class Meta:
        model = Order

        fields = [
            'id',
            'client',
            'status',
            'created',
            'categories',
        ]

    def get_categories(self, obj):
        data = []
        for category_id in ORDER_CATEGORY_CHOICE._db_values:
            items = []
            category_data = {'category_id': category_id,}
            category_item_ids = obj.items.filter(orderitemsize__category=category_id)
            for item in OrderItem.objects.filter(id__in=category_item_ids):
                items.append(OrderItemSerializer(item, context={'category_id': category_id}).data)
            category_data['items'] = items
            data.append(category_data)
        return data

    OrderItemSerializer(many=True, read_only=True)

from orders.models import Order
from .serializers import OrderSerializer
from ...models import DIRECTION_CHOICE, KIND_CHOICE, Document


class OrderExporter(object):
    """Создание документов с данными о заказе для выгрузки в 1с"""

    def __init__(self, order_id: int):
        self.order = Order.objects.get(id=order_id)

    def get_order_data(self):
        data = OrderSerializer(self.order).data
        return data

    def create_document(self):
        order_data = self.get_order_data()
        return Document.objects.create(
            kind=KIND_CHOICE.order,
            direction=DIRECTION_CHOICE.outgoing,
            data=order_data
        )

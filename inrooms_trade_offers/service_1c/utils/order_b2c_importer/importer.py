import datetime
from decimal import Decimal, InvalidOperation

from django.db import transaction

from bot.utils import send_telegram_message
from client_cards.models import BuyerCard
from clients.models import Client
from orders.models import OrderB2C, OrderB2CItem, OrderB2CItemSize
from orders.models.order_b2c import Cheque1c
from products.models import Product, Size
from stores.models import Employee
from ...models import Document


class OrderB2CImporter(object):

    def __init__(self):
        self.document_qs = self.get_document_queryset()

    def run(self):
        with transaction.atomic():
            self._get_documents()
            self._set_documents_process()
        self.process_documents()

    def get_document_queryset(self):
        return Document.objects.filter(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.order_b2c_import,
            status=Document.STATUS.new,
        ).order_by('created')

    def _get_documents(self):
        """Получаем не обработанные документы от 1с"""

        self._documents = list(self.document_qs)

    def _set_documents_process(self):
        """Выставляем документам статус "В обработке" """

        self.document_qs.update(status=Document.STATUS.process)

    def process_documents(self):
        for document in self._documents:
            self.process_document(document)

    def process_document(self, document: Document):
        parse_document_data = self.parse_document(document)
        if parse_document_data['is_valid']:
            self._update_order(parse_document_data['data'])
            self.set_document_complete(document)
        else:
            self.set_document_error(document, ',\n'.join(parse_document_data['errors']))

    @transaction.atomic()
    def _update_order(self, parse_document_data: list):
        data = parse_document_data
        for order_data in data:
            order, created = OrderB2C.objects.get_or_create(
                number=order_data['number'],
                date=order_data['datadoc'],
                defaults={
                    'client': order_data['client'],
                    'visits': order_data['visits'],
                }
            )
            if not created:
                order.client = order_data['client']
                order.visits = order_data['visits']
                order.save()
                OrderB2CItem.objects.filter(order=order).delete()
                Cheque1c.objects.filter(order=order).delete()

            for product_data in order_data.get('items', []):
                cheque_1c, created = Cheque1c.objects.get_or_create(
                    order=order,
                    number_1c=product_data['number_1c'],
                    seller=product_data['seller'],
                    buyer_card=product_data['buyer_card'],
                )
                order_item = OrderB2CItem.objects.create(
                    order=order,
                    cheque_1c=cheque_1c,
                    product=product_data['product'],
                    price=product_data['cost'],
                    price_before_discount=product_data['cost_not_d'],
                    series_type=product_data['product'].main_collection.series_type
                )
                OrderB2CItemSize.objects.create(
                    order_item=order_item,
                    size=product_data['size'],
                    quantity=product_data['quantity']
                )

            order.set_amount_full()
            order.save()

    def parse_document(self, document: Document) -> dict:
        """Разбираем документ

        :param document: Обрабатываемый документ
        :rtype: dict
        :return: Признак валидности документа, подготовленные данные для сохранения в базу, список ошибок

        """
        document_data = document.data
        errors = []
        data = []

        if isinstance(document_data, list):
            for raw_data in document_data:
                if isinstance(raw_data, dict):
                    order_data = {}

                    order_number = raw_data.get('doc_id', None)
                    if order_number and isinstance(order_number, str):
                        order_data['number'] = order_number
                    else:
                        errors.append('Не передан doc_id!')

                    partner_id = raw_data.get('partner_id', None)
                    if partner_id and isinstance(partner_id, str):
                        try:
                            client = Client.objects.get(id=partner_id)
                        except Client.DoesNotExist:
                            errors.append('Не найден партнер с id: {}'.format(partner_id))
                        else:
                            order_data['client'] = client
                    else:
                        errors.append('Не передан partner_id')

                    visits = raw_data.get('visits', 0)
                    try:
                        order_data['visits'] = int(visits)
                    except Exception:
                        errors.append('Не передан visits')

                    datadoc = raw_data.get('datadoc', None)
                    if datadoc and isinstance(datadoc, str):
                        try:
                            order_data['datadoc'] = datetime.datetime.strptime(datadoc, '%Y-%m-%d').date()
                        except ValueError:
                            errors.append('Не верный формат даты: {}'.format(datadoc))
                    else:
                        errors.append('Не передан datadoc')

                    order_data['items'] = []

                    for item in raw_data.get("items", []):
                        product_data = {}
                        product_id = item.get("product_id", None)
                        if product_id:
                            try:
                                product = Product.objects.get(id=product_id)
                            except Product.DoesNotExist:
                                errors.append('Не найден товар с id: {}'.format(product_id))
                            else:
                                product_data['product'] = product

                                cost = item.get("cost", None)
                                try:
                                    if isinstance(cost, str):
                                        cost = cost.replace(' ', '')
                                    cost = Decimal(cost)
                                except (ValueError, TypeError, InvalidOperation):
                                    errors.append('Не передан cost')
                                else:

                                    cost_not_d = item.get("cost_not_d", 0)
                                    try:
                                        if isinstance(cost_not_d, str):
                                            cost_not_d = cost_not_d.replace(' ', '')
                                            cost_not_d = Decimal(cost_not_d)
                                    except (ValueError, TypeError, InvalidOperation):
                                        cost_not_d = 0

                                    product_data['cost'] = cost
                                    product_data['cost_not_d'] = cost_not_d

                                    size_code = item.get("size", None)
                                    if size_code:
                                        try:
                                            size = Size.objects.get(code_1c=size_code)
                                            product_data['size'] = size
                                        except Size.DoesNotExist:
                                            errors.append('Не найден размер с 1с_code: {}'.format(size_code))
                                    else:
                                        errors.append("Не передан size")

                                    quantity = item.get("quantity", None)
                                    if not isinstance(quantity, int):
                                        errors.append("Не верный формат quantity: {}".format(quantity))
                                    else:
                                        product_data['quantity'] = quantity

                                    buyer_card_code = item.get("client_card", None)
                                    if buyer_card_code:
                                        try:
                                            buyer_card = BuyerCard.objects.get(number=buyer_card_code)
                                            product_data['buyer_card'] = buyer_card
                                        except BuyerCard.DoesNotExist:
                                            errors.append('Не найдена карта клиента с кодом: {}'.format(buyer_card_code))
                                    else:
                                        product_data['buyer_card'] = None

                                    product_data['number_1c'] = item.get("doc_num", '')
                                    if product_data['number_1c'] is None:
                                        product_data['number_1c'] = ''

                                    seller_id = item.get("seller_id", None)
                                    if seller_id:
                                        try:
                                            seller = Employee.objects.get(id=seller_id)
                                            product_data['seller'] = seller
                                        except Employee.DoesNotExist:
                                            errors.append('Не найден сотрудник с id: {}'.format(seller_id))
                                    else:
                                        product_data['seller'] = None

                            order_data['items'].append(product_data)
                        else:
                            errors.append('Не передан product_id')
                    data.append(order_data)
                else:
                    errors.append('Не верный формат документа')
        else:
            errors.append('Не верный формат документа')

        return {'is_valid': False if errors else True,
                'data': data,
                'errors': errors}

    def set_document_error(self, document: Document, result: str):
        """Пометить документ как обработанный с ошибкой

        :type result: str
        :param result: Описание ошибки
        :param document: Обрабатываемый документ

        """

        document.result = result
        document.status = Document.STATUS.error
        document.save()

        send_telegram_message(message=str(document), url=document.get_absolute_url())

    def set_document_complete(self, document: Document):
        """Пометить документ как обработанный

        :param document: Обрабатываемый документ

        """

        document.status = Document.STATUS.complete
        document.save()

from rest_framework import serializers

from stores.models import Store


class StoreSerializer(serializers.ModelSerializer):

    class Meta:
        model = Store
        fields = [
            'id',
            'client',
            'name',
            'address',
            'phone',
            'opening',
            'total_area',
            'trade_area',
            'employees_num',
            'it_info',
        ]

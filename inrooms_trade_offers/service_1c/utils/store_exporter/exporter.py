from stores.models import Store
from ..mixins import SoapExporterMixin
from .serializers import StoreSerializer


class StoreExporter(SoapExporterMixin):
    model = Store
    serializer_class = StoreSerializer
    soap_wsdl_name = 'op_inrooms_1c_wsdl'
    soap_service_method = 'LoadStore'

    def get_bot_error_message(self):
        return 'Не удалось выгрузить магазин(ы) #{} в 1с'.format(self.object_ids)

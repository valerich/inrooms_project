from decimal import Decimal, InvalidOperation
from django.db import transaction

from bot.utils import send_telegram_message
from clients.models import Client
from core.utils.constants import ORDER_CATEGORY_CHOICE
from orders.models import Order, OrderItem, OrderItemSize, OrderStatus
from products.models import Product, Size
from warehouses.models import Warehouse
from ...models import Document


class OrderImporter(object):

    def __init__(self):
        self.document_qs = self.get_document_queryset()

    def run(self):
        with transaction.atomic():
            self._get_documents()
            self._set_documents_process()
        self.process_documents()

    def get_document_queryset(self):
        return Document.objects.filter(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.order_import,
            status=Document.STATUS.new,
        ).order_by('created')

    def _get_documents(self):
        """Получаем не обработанные документы от 1с"""

        self._documents = list(self.document_qs)

    def _set_documents_process(self):
        """Выставляем документам статус "В обработке" """

        self.document_qs.update(status=Document.STATUS.process)

    def process_documents(self):
        for document in self._documents:
            self.process_document(document)

    def process_document(self, document: Document):
        parse_document_data = self.parse_document(document)
        if parse_document_data['is_valid']:
            self._update_order(parse_document_data['data'])
            self.set_document_complete(document)
        else:
            self.set_document_error(document, ',\n'.join(parse_document_data['errors']))

    @transaction.atomic()
    def _update_order(self, parse_document_data: dict):
        order = parse_document_data['order']
        order.status = parse_document_data['status']
        order.client = parse_document_data['client']
        order.save()

        product_ids = set()
        for c, c_data in parse_document_data['categories'].items():
            for product_id in c_data.keys():
                product_ids.add(product_id)

        OrderItem.objects.filter(order=order).exclude(product_id__in=product_ids).delete()

        for category_id, category_data in parse_document_data['categories'].items():
            for product_id, product_data in category_data.items():
                order_item, created = OrderItem.objects.update_or_create(
                    order=order,
                    product_id=product_id,
                    defaults={
                        "price": product_data["price"],
                        "series_type": product_data["product"].main_collection.series_type,
                    }
                )

                if not created:
                    existing_data = dict()
                    for ois in order_item.orderitemsize_set.filter(category=category_id):
                        existing_data[(ois.size_id, ois.warehouse_id, ois.is_delivery_reserve)] = ois.id
                    for (size_id, warehouse_id, is_delivery_reserve) in product_data['size_data'].keys():
                        existing_data.pop((size_id, warehouse_id, is_delivery_reserve), None)
                    OrderItemSize.objects.filter(id__in=existing_data.values()).delete()
                else:
                    order_item.set_recommended_retail_price()
                order_item.save()

                for (size_id, warehouse_id, is_delivery_reserve), quantity in product_data['size_data'].items():
                    OrderItemSize.objects.update_or_create(
                        order_item=order_item,
                        size_id=size_id,
                        warehouse_id=warehouse_id,
                        category=category_id,
                        is_delivery_reserve=is_delivery_reserve,
                        defaults={
                            "quantity": quantity,
                        }
                    )

        order.set_amount_full()
        order.save()

    def parse_document(self, document: Document) -> dict:
        """Разбираем документ

        :param document: Обрабатываемый документ
        :rtype: dict
        :return: Признак валидности документа, подготовленные данные для сохранения в базу, список ошибок

        """
        raw_data = document.data
        errors = []
        order = None
        status = None
        client = None
        categories = None

        if isinstance(raw_data, dict):
            order_id = raw_data.get('id', None)
            if order_id and isinstance(order_id, int):
                try:
                    order = Order.objects.get(id=order_id)
                except Order.DoesNotExist:
                    errors.append('Не найден заказ с id: {}'.format(order_id))
            else:
                errors.append('Не передан id')

            status_code = raw_data.get('status', {}).get('code', None)
            if status_code and isinstance(status_code, str):
                try:
                    status = OrderStatus.objects.get(code=status_code)
                except OrderStatus.DoesNotExist:
                    errors.append('Не найден статус с code: {}'.format(status_code))
            else:
                errors.append('Не передан status_code')

            client_id = raw_data.get('client', {}).get('id', None)
            if client_id and isinstance(client_id, int):
                try:
                    client = Client.objects.get(id=client_id)
                except Client.DoesNotExist:
                    errors.append('Не найден клиент с id: {}'.format(client_id))
            else:
                errors.append('Не передан client.id')

            categories = {}
            for category_data in raw_data.get('categories', []):
                category_id = category_data.get("category_id", None)
                if category_id in ORDER_CATEGORY_CHOICE._db_values:
                    categories[category_id] = {}
                    for item in category_data.get("items", []):
                        product_id = item.get("product_id", None)
                        if product_id:
                            try:
                                product = Product.objects.get(id=product_id)
                            except Product.DoesNotExist:
                                errors.append('Не найден товар с id: {}'.format(product_id))
                            else:
                                price = item.get("price", None)
                                try:
                                    price = Decimal(price)
                                except (ValueError, TypeError, InvalidOperation):
                                    errors.append('Не передан price')
                                else:
                                    categories[category_id][product.id] = {"price": price,
                                                                           "product": product,
                                                                           "size_data": {}}
                                    for size_data in item.get("data", []):
                                        size = None
                                        warehouse = None

                                        size_code = size_data.get("size", None)
                                        if size_code:
                                            try:
                                                size = Size.objects.get(code_1c=size_code)
                                            except Size.DoesNotExist:
                                                errors.append('Не найден размер с 1с_code: {}'.format(size_code))
                                        else:
                                            errors.append("Не передан size")

                                        warehouse_code = size_data.get("warehouse", {}).get("code", None)
                                        if warehouse_code:
                                            try:
                                                warehouse = Warehouse.objects.get(code=warehouse_code)
                                            except Warehouse.DoesNotExist:
                                                errors.append('Не найдена точка с code: {}'.format(warehouse_code))
                                        else:
                                            errors.append("Не передан warehouse.code")

                                        is_delivery_reserve = size_data.get("is_delivery_reserve", False)
                                        if not isinstance(is_delivery_reserve, bool):
                                            errors.append("is_delivery_reserve должен быть bool")

                                        quantity = size_data.get("quantity", None)
                                        if not isinstance(quantity, int):
                                            errors.append("Не верный формат quantity: {}".format(quantity))

                                        if not errors:
                                            if (size.id, warehouse.id, is_delivery_reserve) in categories[category_id][product.id]["size_data"]:
                                                categories[category_id][product.id]["size_data"][(size.id, warehouse.id, is_delivery_reserve)] += quantity
                                            else:
                                                categories[category_id][product.id]["size_data"][(size.id, warehouse.id, is_delivery_reserve)] = quantity
                        else:
                            errors.append('Не передан product_id')
                else:
                    errors.append('Не передан category_id')

        else:
            errors.append('Не верный формат документа')
        return {'is_valid': False if errors else True,
                'data': {
                    'order': order,
                    'client': client,
                    'status': status,
                    'categories': categories
                },
                'errors': errors}

    def set_document_error(self, document: Document, result: str):
        """Пометить документ как обработанный с ошибкой

        :type result: str
        :param result: Описание ошибки
        :param document: Обрабатываемый документ

        """

        document.result = result
        document.status = Document.STATUS.error
        document.save()

        send_telegram_message(message=str(document), url=document.get_absolute_url())

    def set_document_complete(self, document: Document):
        """Пометить документ как обработанный

        :param document: Обрабатываемый документ

        """

        document.status = Document.STATUS.complete
        document.save()

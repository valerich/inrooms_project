from rest_framework import serializers

from clients.models import Client


class ClientSerializer(serializers.ModelSerializer):

    contract_date = serializers.SerializerMethodField()

    class Meta:
        model = Client
        fields = [
            'id',
            'kind',
            'is_deleted',
            'name',
            'inn',
            'kpp',
            'legal_index',
            'legal_region',
            'legal_district',
            'legal_city',
            'legal_street',
            'legal_house',
            'legal_building',
            'legal_flat',
            'legal_construction',
            'actual_index',
            'actual_region',
            'actual_district',
            'actual_city',
            'actual_street',
            'actual_house',
            'actual_building',
            'actual_flat',
            'actual_construction',
            'manager',
            'contact_person',
            'phone',
            'email',
            'reason',
            'account_current',
            'account_correspondent',
            'bank_name',
            'bik',
            'contract_number',
            'contract_date',
        ]

    def get_contract_date(self, obj):
        if not obj.contract_date:
            return ''
        else:
            return obj.contract_date.strftime('%Y-%m-%d')

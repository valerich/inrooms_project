from clients.models import Client as ClientModel
from ..mixins import SoapExporterMixin
from .serializers import ClientSerializer


class ClientExporter(SoapExporterMixin):
    serializer_class = ClientSerializer
    soap_wsdl_name = 'inrooms_1c_wsdl'
    soap_service_method = 'LoadClient'

    def get_bot_error_message(self):
        return 'Не удалось выгрузить клиента(ов) #{} в 1с'.format(self.object_ids)

    def get_queryset(self):
        return ClientModel.all_objects.filter(id__in=self.object_ids)

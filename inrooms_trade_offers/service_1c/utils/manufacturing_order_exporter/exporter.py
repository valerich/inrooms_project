from manufacturing.models import ManufacturingOrder
from .serializers import ManufacturingOrderSerializer
from ...models import DIRECTION_CHOICE, KIND_CHOICE, Document


class ManufacturingOrderExporter(object):
    """Создание документов с данными о заказе на фабрику для выгрузки в 1с"""

    def __init__(self, order_id: int):
        self.order = ManufacturingOrder.objects.get(id=order_id)

    def get_order_data(self):
        data = ManufacturingOrderSerializer(self.order).data
        return data

    def create_document(self):
        order_data = self.get_order_data()
        return Document.objects.create(
            kind=KIND_CHOICE.manufacturing_order,
            direction=DIRECTION_CHOICE.outgoing,
            data=order_data
        )

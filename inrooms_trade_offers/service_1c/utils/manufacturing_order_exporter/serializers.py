from rest_framework import serializers

from factories.models import Factory
from manufacturing.models import ManufacturingOrder, ManufacturingOrderItem, ManufacturingStatus
from products.models import Size
from warehouses.models import Warehouse


class ManufacturingOrderItemSerializer(serializers.ModelSerializer):
    product_id = serializers.SerializerMethodField()
    data = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()

    class Meta:
        model = ManufacturingOrderItem
        fields = [
            'product_id',
            'price',
            'data',
        ]

    def get_data(self, obj):
        data = []
        for size in Size.objects.filter(series_type=obj.series_type):
            has_size = getattr(obj, 'has_{}'.format(size.code))
            if has_size:
                data.append({
                    'size': size.code_1c,
                    'quantity': obj.quantity * size.multiplier
                })
        return data

    def get_product_id(self, obj):
        return obj.product_id

    def get_price(self, obj):
        if obj.price is not None:
            return str(obj.price)
        return None


class WarehouseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Warehouse
        fields = [
            'code',
        ]


class ManufacturingStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = ManufacturingStatus
        fields = [
            'code',
        ]


class FactorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Factory
        fields = [
            'id',
            'name',

        ]


class ManufacturingOrderSerializer(serializers.ModelSerializer):
    status = ManufacturingStatusSerializer()
    factory = FactorySerializer()
    warehouse = WarehouseSerializer()
    items = ManufacturingOrderItemSerializer(many=True, read_only=True)

    class Meta:
        model = ManufacturingOrder

        fields = [
            'id',
            'status',
            'created',
            'factory',
            'warehouse',
            'items',
        ]


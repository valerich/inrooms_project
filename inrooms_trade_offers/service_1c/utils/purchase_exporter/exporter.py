from purchases.models import Purchase
from .serializers import PurchaseSerializer
from ...models import DIRECTION_CHOICE, KIND_CHOICE, Document


class PurchaseExporter(object):
    """Создание документов с данными о закупке для выгрузки в 1с"""

    def __init__(self, order_id: int):
        self.purchase = Purchase.objects.get(id=order_id)

    def get_purchase_data(self):
        data = PurchaseSerializer(self.purchase).data
        return data

    def create_document(self):
        purchase_data = self.get_purchase_data()
        return Document.objects.create(
            kind=KIND_CHOICE.purchase,
            direction=DIRECTION_CHOICE.outgoing,
            data=purchase_data
        )

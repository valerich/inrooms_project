from rest_framework import serializers

from products.models import Size
from purchases.models import Purchase, PurchaseItem, PurchaseStatus
from warehouses.models import Warehouse


class PurchaseItemSerializer(serializers.ModelSerializer):
    product_id = serializers.SerializerMethodField()
    sizes = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()

    class Meta:
        model = PurchaseItem
        fields = [
            'product_id',
            'price',
            'sizes',
        ]

    def get_sizes(self, obj):
        data = []
        for size in Size.objects.filter(series_type=obj.series_type):
            data.append({
                'size': size.code_1c,
                'quantity': getattr(obj, 'count_{}'.format(size.code), 0),
            })
        return data

    def get_product_id(self, obj):
        return obj.product_id

    def get_price(self, obj):
        if obj.price is not None:
            return str(obj.price)
        return None


class WarehouseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Warehouse
        fields = [
            'code',
        ]


class PurchaseStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = PurchaseStatus
        fields = [
            'code',
        ]


class PurchaseSerializer(serializers.ModelSerializer):
    status = PurchaseStatusSerializer()
    delivery = serializers.SerializerMethodField()
    items = PurchaseItemSerializer(many=True, read_only=True)

    class Meta:
        model = Purchase

        fields = [
            'id',
            'status',
            'created',
            'delivery',
            'items',
        ]

    def get_status_code(self, obj):
        return obj.status.code

    def get_delivery(self, obj):
        return {
            'date': obj.delivery_date.strftime('%d.%m.%Y'),
            'client': obj.delivery_client_id,
            'warehouse_to': WarehouseSerializer(obj.delivery_warehouse_to).data,
        }

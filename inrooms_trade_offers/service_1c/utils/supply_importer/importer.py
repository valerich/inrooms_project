import datetime
from collections import defaultdict
from django.db import transaction

from accounts.models import User
from bot.utils import send_telegram_message
from products.models import Product, Size
from supply.models import Supply, SupplyStatus, SupplyItem
from warehouses.models import Warehouse
from ...models import Document


class SupplyImporter(object):

    def __init__(self):
        self.document_qs = self.get_document_queryset()

    def run(self):
        with transaction.atomic():
            self._get_documents()
            self._set_documents_process()
        self.process_documents()

    def get_document_queryset(self):
        return Document.objects.filter(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.supply_import,
            status=Document.STATUS.new,
        ).order_by('created')

    def _get_documents(self):
        """Получаем не обработанные документы от 1с"""

        self._documents = list(self.document_qs)

    def _set_documents_process(self):
        """Выставляем документам статус "В обработке" """

        self.document_qs.update(status=Document.STATUS.process)

    def process_documents(self):
        for document in self._documents:
            self.process_document(document)

    def process_document(self, document: Document):
        parse_document_data = self.parse_document(document)
        if parse_document_data['is_valid']:
            self._update_order(parse_document_data['data'])
            self.set_document_complete(document)
        else:
            self.set_document_error(document, ',\n'.join(parse_document_data['errors']))

    @transaction.atomic()
    def _update_order(self, parse_document_data: dict):
        supply = parse_document_data['supply']
        creator = parse_document_data['supply']
        if creator is None:
            creator = User.objects.get(email='service@chn.inrooms.club')
        if supply is None:
            supply, created = Supply.objects.update_or_create(
                number_1c=parse_document_data['number_1c'],
                date_1c=parse_document_data['datadoc'],
                defaults={
                    'delivery_warehouse_to': parse_document_data['delivery_warehouse_to'],
                    'creator': creator
                }
            )
        else:
            supply.delivery_warehouse_to = parse_document_data['delivery_warehouse_to']
            supply.save()

        status = parse_document_data['status']
        if status:
            supply.status = status
            supply.save()

        items = parse_document_data['items']

        SupplyItem.objects.filter(supply=supply).update(
            count_xxs=0,
            count_xs=0,
            count_s=0,
            count_m=0,
            count_l=0,
            count_xl=0,
            count_xxl=0,
            count_xxxl=0,
            count_34=0,
            count_35=0,
            count_36=0,
            count_37=0,
            count_38=0,
            count_39=0,
            count_40=0,
            count_41=0,
        )

        for product_id, item_data in items.items():
            try:
                supply_item = SupplyItem.objects.get(
                    supply=supply,
                    product_id=product_id
                )
            except SupplyItem.DoesNotExist:
                supply_item = SupplyItem(
                    supply=supply,
                    product_id=product_id
                )
                supply_item.save()
            for size_data in item_data:
                setattr(supply_item, 'count_{}'.format(size_data['size'].code), size_data['quantity'])
            supply_item.save()

    def parse_document(self, document: Document) -> dict:
        """Разбираем документ

        :param document: Обрабатываемый документ
        :rtype: dict
        :return: Признак валидности документа, подготовленные данные для сохранения в базу, список ошибок

        """
        raw_data = document.data
        errors = []
        creator = None
        supply = None
        status = None
        delivery_warehouse_to = None
        number_1c = None
        items = None
        datadoc = None

        if isinstance(raw_data, dict):
            supply_id = raw_data.get('id', None)
            if supply_id and isinstance(supply_id, int):
                try:
                    supply = Supply.objects.get(id=supply_id)
                except Supply.DoesNotExist:
                    errors.append('Не найдена поставка с id: {}'.format(supply_id))

            creator_id = raw_data.get('creator', {}).get('id', None)
            if creator_id and isinstance(creator_id, int):
                try:
                    creator = User.objects.get(id=creator_id)
                except User.DoesNotExist:
                    errors.append('Не найден пользователь с id: {}'.format(creator_id))

            status_code = raw_data.get('status', None)
            if status_code and isinstance(status_code, str):
                try:
                    status = SupplyStatus.objects.get(code=status_code)
                except SupplyStatus.DoesNotExist:
                    errors.append('Не найден статус с code: {}'.format(status_code))

            warehouse_code = raw_data.get('warehouse', {}).get('code', None)
            if warehouse_code and isinstance(warehouse_code, str):
                try:
                    delivery_warehouse_to = Warehouse.objects.get(code=warehouse_code)
                except Warehouse.DoesNotExist:
                    errors.append('Не найден склад с кодом: {}'.format(warehouse_code))
            else:
                errors.append('Не передан warehouse_code')

            number_1c = raw_data.get('number_1c', None)
            if number_1c and isinstance(number_1c, str):
                pass
            else:
                errors.append('Не передан number_1c')

            datadoc = raw_data.get('datadoc', None)
            if datadoc and isinstance(datadoc, str):
                try:
                    datadoc = datetime.datetime.strptime(datadoc, '%Y-%m-%d').date()
                except ValueError:
                    errors.append('Не верный формат даты: {}'.format(datadoc))
            else:
                errors.append('Не передан datadoc')

            items = defaultdict(list)

            for item in raw_data.get("items", []):
                product_id = item.get("product_id", None)
                if product_id:
                    try:
                        product = Product.objects.get(id=product_id)
                    except Product.DoesNotExist:
                        errors.append('Не найден товар с id: {}'.format(product_id))
                    else:
                        for size_data in item.get("data", []):
                            size = None

                            size_code = size_data.get("size", None)
                            if size_code:
                                try:
                                    size = Size.objects.get(code_1c=size_code)
                                except Size.DoesNotExist:
                                    errors.append('Не найден размер с 1с_code: {}'.format(size_code))
                            else:
                                errors.append("Не передан size")

                            quantity = size_data.get("quantity", None)
                            if not isinstance(quantity, int):
                                errors.append("Не верный формат quantity: {}".format(quantity))

                            if not errors:
                                items[product.id].append({
                                    'size': size,
                                    'quantity': quantity
                                })
                else:
                    errors.append('Не передан product_id')

        else:
            errors.append('Не верный формат документа')
        return {'is_valid': False if errors else True,
                'data': {
                    'supply': supply,
                    'creator': creator,
                    'number_1c': number_1c,
                    'delivery_warehouse_to': delivery_warehouse_to,
                    'status': status,
                    'items': items,
                    'datadoc': datadoc
                },
                'errors': errors}

    def set_document_error(self, document: Document, result: str):
        """Пометить документ как обработанный с ошибкой

        :type result: str
        :param result: Описание ошибки
        :param document: Обрабатываемый документ

        """

        document.result = result
        document.status = Document.STATUS.error
        document.save()

        send_telegram_message(message=str(document), url=document.get_absolute_url())

    def set_document_complete(self, document: Document):
        """Пометить документ как обработанный

        :param document: Обрабатываемый документ

        """

        document.status = Document.STATUS.complete
        document.save()

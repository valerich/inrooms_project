from django.db import transaction

from bot.utils import send_telegram_message
from manufacturing.models import ManufacturingOrder, ManufacturingStatus
from ...models import Document


class ManufacturingOrderStatusParser(object):

    def __init__(self):
        self.document_qs = self.get_document_queryset()
        self.status_codes = {s.code: s for s in ManufacturingStatus.objects.all()}

    def run(self):
        with transaction.atomic():
            self._get_documents()
            self._set_documents_process()
        self.process_documents()

    def get_document_queryset(self):
        return Document.objects.filter(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.manufacturing_order_status_change,
            status=Document.STATUS.new,
        ).order_by('created')

    def _get_documents(self):
        """Получаем не обработанные документы на изменение статуса заказа от 1с"""

        self._documents = list(self.document_qs)

    def _set_documents_process(self):
        """Выставляем документам статус "В обработке" """

        self.document_qs.update(status=Document.STATUS.process)

    def process_documents(self):
        for document in self._documents:
            self.process_document(document)

    def process_document(self, document: Document):
        parse_document_data = self.parse_document(document)
        if parse_document_data['is_valid']:
            self._update_order_status(parse_document_data['data'])
            self.set_document_complete(document)
        else:
            self.set_document_error(document, ',\n'.join(parse_document_data['errors']))

    def _update_order_status(self, parse_document_data: dict):
        order = parse_document_data['order']
        new_status = parse_document_data['status']
        order.status = new_status
        order.save()
        order.process_order()

    def parse_document(self, document: Document) -> dict:
        """Разбираем документ

        :param document: Обрабатываемый документ
        :rtype: dict
        :return: Признак валидности документа, подготовленные данные для сохранения в базу, список ошибок

        """
        raw_data = document.data
        errors = []
        order = None
        status = None

        if isinstance(raw_data, dict):
            order_id = raw_data.get('manufacturing_order_id', None)
            if order_id and isinstance(order_id, int):
                try:
                    order = ManufacturingOrder.objects.get(id=order_id)
                except ManufacturingOrder.DoesNotExist:
                    errors.append('Не найден заказ на фабрику с id: {}'.format(order_id))
            else:
                errors.append('Не передан manufacturing_order_id')

            status_code = raw_data.get('status_code', None)
            if status_code and isinstance(status_code, str):
                try:
                    status = ManufacturingStatus.objects.get(code=status_code)
                except ManufacturingStatus.DoesNotExist:
                    errors.append('Не найден статус с code: {}'.format(status_code))
            else:
                errors.append('Не передан status_code')
        else:
            errors.append('Не верный формат документа')
        return {'is_valid': False if errors else True,
                'data': {
                    'order': order,
                    'status': status,
                },
                'errors': errors}

    def set_document_error(self, document: Document, result: str):
        """Пометить документ как обработанный с ошибкой

        :type result: str
        :param result: Описание ошибки
        :param document: Обрабатываемый документ

        """

        document.result = result
        document.status = Document.STATUS.error
        document.save()

        send_telegram_message(message=str(document), url=document.get_absolute_url())

    def set_document_complete(self, document: Document):
        """Пометить документ как обработанный

        :param document: Обрабатываемый документ

        """

        document.status = Document.STATUS.complete
        document.save()

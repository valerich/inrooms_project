from orders.models import DefectAct
from .serializers import DefectActSerializer
from ...models import DIRECTION_CHOICE, KIND_CHOICE, Document


class DefectActExporter(object):
    """Создание документов с данными о акте о браке для выгрузки в 1с"""

    def __init__(self, defect_act_id: int):
        self.object = DefectAct.objects.get(id=defect_act_id)

    def get_object_data(self):
        data = DefectActSerializer(self.object).data
        return data

    def create_document(self):
        object_data = self.get_object_data()
        return Document.objects.create(
            kind=KIND_CHOICE.defect_act,
            direction=DIRECTION_CHOICE.outgoing,
            data=object_data
        )

from rest_framework import serializers

from clients.models import Client
from orders.models import DefectAct, DefectActStatus


class DefectActStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = DefectActStatus
        fields = [
            'code',
        ]


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = [
            'id',
        ]


class DefectActSerializer(serializers.ModelSerializer):
    client = ClientSerializer()
    status = DefectActStatusSerializer()
    items = serializers.SerializerMethodField()

    class Meta:
        model = DefectAct

        fields = [
            'id',
            'client',
            'status',
            'created',
            'items',
        ]

    def get_items(self, obj):
        data = []
        for item in obj.items.all():
            data.append({
                'product_id': item.order_item.product_id,
                'order_id': item.order_item.order_id,
                'price': str(item.order_item.price),
                'size': item.size.code_1c,
            })
        return data
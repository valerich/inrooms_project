from django.db import transaction

from bot.utils import send_telegram_message
from supply.models import Supply, SupplyStatus
from ...models import Document


class SupplyStatusParser(object):

    def __init__(self):
        self.document_qs = self.get_document_queryset()
        self.status_codes = {s.code: s for s in SupplyStatus.objects.all()}

    def run(self):
        with transaction.atomic():
            self._get_documents()
            self._set_documents_process()
        self.process_documents()

    def get_document_queryset(self):
        return Document.objects.filter(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.supply_status_change,
            status=Document.STATUS.new,
        ).order_by('created')

    def _get_documents(self):
        """Получаем не обработанные документы на изменение статуса поставки от 1с"""

        self._documents = list(self.document_qs)

    def _set_documents_process(self):
        """Выставляем документам статус "В обработке" """

        self.document_qs.update(status=Document.STATUS.process)

    def process_documents(self):
        for document in self._documents:
            self.process_document(document)

    def process_document(self, document: Document):
        parse_document_data = self.parse_document(document)
        if parse_document_data['is_valid']:
            self._update_supply_status(parse_document_data['data'])
            self.set_document_complete(document)
        else:
            self.set_document_error(document, ',\n'.join(parse_document_data['errors']))

    def _update_supply_status(self, parse_document_data: dict):
        supply = parse_document_data['supply']
        new_status = parse_document_data['status']
        supply.status = new_status
        supply.save()
        supply.process_supply()

    def parse_document(self, document: Document) -> dict:
        """Разбираем документ

        :param document: Обрабатываемый документ
        :rtype: dict
        :return: Признак валидности документа, подготовленные данные для сохранения в базу, список ошибок

        """
        raw_data = document.data
        errors = []
        supply = None
        status = None

        if isinstance(raw_data, dict):
            supply_id = raw_data.get('supply_id', None)
            if supply_id and isinstance(supply_id, int):
                try:
                    supply = Supply.objects.get(id=supply_id)
                except Supply.DoesNotExist:
                    errors.append('Не найдена поставка с id: {}'.format(supply_id))
            else:
                errors.append('Не передан supply_id')

            status_code = raw_data.get('status_code', None)
            if status_code and isinstance(status_code, str):
                try:
                    status = SupplyStatus.objects.get(code=status_code)
                except SupplyStatus.DoesNotExist:
                    errors.append('Не найден статус с code: {}'.format(status_code))
            else:
                errors.append('Не передан status_code')

        else:
            errors.append('Не верный формат документа')
        return {'is_valid': False if errors else True,
                'data': {
                    'supply': supply,
                    'status': status,
                },
                'errors': errors}

    def set_document_error(self, document: Document, result: str):
        """Пометить документ как обработанный с ошибкой

        :type result: str
        :param result: Описание ошибки
        :param document: Обрабатываемый документ

        """

        document.result = result
        document.status = Document.STATUS.error
        document.save()

        send_telegram_message(message=str(document), url=document.get_absolute_url())

    def set_document_complete(self, document: Document):
        """Пометить документ как обработанный

        :param document: Обрабатываемый документ

        """

        document.status = Document.STATUS.complete
        document.save()

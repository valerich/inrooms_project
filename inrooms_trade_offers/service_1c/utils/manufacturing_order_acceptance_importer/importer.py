from django.db import transaction

from bot.utils import send_telegram_message
from manufacturing.models import ManufacturingOrder, ManufacturingOrderItem
from products.models import Product, Size
from ...models import Document


class ManufacturingOrderAcceptanceImporter(object):

    def __init__(self):
        self.document_qs = self.get_document_queryset()

    def run(self):
        with transaction.atomic():
            self._get_documents()
            self._set_documents_process()
        self.process_documents()

    def get_document_queryset(self):
        return Document.objects.filter(
            direction=Document.DIRECTION.incoming,
            kind=Document.KIND.manufacturing_order_acceptance_import,
            status=Document.STATUS.new,
        ).order_by('created')

    def _get_documents(self):
        """Получаем не обработанные документы от 1с"""

        self._documents = list(self.document_qs)

    def _set_documents_process(self):
        """Выставляем документам статус "В обработке" """

        self.document_qs.update(status=Document.STATUS.process)

    def process_documents(self):
        for document in self._documents:
            self.process_document(document)

    def process_document(self, document: Document):
        parse_document_data = self.parse_document(document)
        if parse_document_data['is_valid']:
            self._update_order(parse_document_data['data'])
            self.set_document_complete(document)
        else:
            self.set_document_error(document, ',\n'.join(parse_document_data['errors']))

    @transaction.atomic()
    def _update_order(self, parse_document_data: dict):
        order = parse_document_data['order']
        ManufacturingOrderItem.objects.filter(order=order).update(
            received_xxs_count=0,
            received_xs_count=0,
            received_s_count=0,
            received_m_count=0,
            received_l_count=0,
            received_xl_count=0,
            received_xxl_count=0,
            received_xxxl_count=0,
            received_34_count=0,
            received_35_count=0,
            received_36_count=0,
            received_37_count=0,
            received_38_count=0,
            received_39_count=0,
            received_40_count=0,
            received_41_count=0,
        )
        for product_id, product_data in parse_document_data['items'].items():
            order_item, c = ManufacturingOrderItem.objects.get_or_create(
                order=order,
                product_id=product_id,
            )
            for size_data in product_data['sizes']:
                setattr(order_item, 'received_{}_count'.format(size_data['size'].code), size_data['quantity'])
            order_item.save()

    def parse_document(self, document: Document) -> dict:
        """Разбираем документ

        :param document: Обрабатываемый документ
        :rtype: dict
        :return: Признак валидности документа, подготовленные данные для сохранения в базу, список ошибок

        """
        raw_data = document.data
        errors = []
        order = None
        items = {}

        if isinstance(raw_data, dict):
            order_id = raw_data.get('id', None)
            if order_id and isinstance(order_id, int):
                try:
                    order = ManufacturingOrder.objects.get(id=order_id)
                except ManufacturingOrder.DoesNotExist:
                    errors.append('Не найден заказ на фабрику с id: {}'.format(order_id))
            else:
                errors.append('Не передан id')

            for item in raw_data.get('items', []):
                product_id = item.get("product_id", None)
                if product_id:
                    try:
                        product = Product.objects.get(id=product_id)
                    except Product.DoesNotExist:
                        errors.append('Не найден товар с id: {}'.format(product_id))
                    else:
                        items[product.id] = {
                            'product': product,
                            'sizes': []
                        }
                        for size_data in item.get('sizes', []):
                            size = None
                            size_code = size_data.get("size", None)
                            if size_code:
                                try:
                                    size = Size.objects.get(code_1c=size_code)
                                except Size.DoesNotExist:
                                    errors.append('Не найден размер с 1с_code: {}'.format(size_code))
                            else:
                                errors.append("Не передан size")

                            quantity = size_data.get("quantity", None)
                            if not isinstance(quantity, int):
                                errors.append("Не верный формат quantity: {}".format(quantity))

                            if not errors:
                                items[product.id]['sizes'].append({
                                    'size': size,
                                    'quantity': quantity
                                })
                else:
                    errors.append('Не передан product_id')
        else:
            errors.append('Не верный формат документа')
        return {'is_valid': False if errors else True,
                'data': {
                    'order': order,
                    'items': items,
                },
                'errors': errors}

    def set_document_error(self, document: Document, result: str):
        """Пометить документ как обработанный с ошибкой

        :type result: str
        :param result: Описание ошибки
        :param document: Обрабатываемый документ

        """

        document.result = result
        document.status = Document.STATUS.error
        document.save()

        send_telegram_message(message=str(document), url=document.get_absolute_url())

    def set_document_complete(self, document: Document):
        """Пометить документ как обработанный

        :param document: Обрабатываемый документ

        """

        document.status = Document.STATUS.complete
        document.save()

from rest_framework import serializers

from stores.models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    date_birthday = serializers.SerializerMethodField()
    date_employment = serializers.SerializerMethodField()
    client = serializers.SerializerMethodField()

    class Meta:
        model = Employee
        fields = [
            'id',
            # 'store',
            # На 14.11.2016 12:28 в результате конф кола с Мишей и Денисом решили
            # что до нового года партнеры у магазинов меняться не будут
            # и заменять в выгрузке store на client безопасно
            'client',
            'first_name',
            'middle_name',
            'last_name',
            'phone',
            'email',
            'date_birthday',
            'date_employment',
            'is_working',
            'position',
        ]

    def get_date_birthday(self, obj):
        if not obj.date_birthday:
            return ''
        else:
            return obj.date_birthday.strftime('%Y-%m-%d')

    def get_date_employment(self, obj):
        if not obj.date_employment:
            return ''
        else:
            return obj.date_employment.strftime('%Y-%m-%d')

    def get_client(self, obj):
        return obj.store.client_id
import logging

from stores.models import Employee
from ..mixins import SoapExporterMixin
from .serializers import EmployeeSerializer

logger = logging.getLogger(__name__)


class EmployeeExporter(SoapExporterMixin):
    model = Employee
    serializer_class = EmployeeSerializer
    soap_wsdl_name = 'op_inrooms_1c_wsdl'
    soap_service_method = 'LoadEmployee'

    def get_bot_error_message(self):
        return 'Не удалось выгрузить сотрудника(ов) #{} в 1с'.format(self.object_ids)

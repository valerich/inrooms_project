import factory
import factory.django
import factory.fuzzy

from .models import DIRECTION_CHOICE, KIND_CHOICE, Document


class DocumentFactory(factory.django.DjangoModelFactory):
    kind = factory.fuzzy.FuzzyChoice(KIND_CHOICE._db_values)
    direction = factory.fuzzy.FuzzyChoice(DIRECTION_CHOICE._db_values)
    data = {}

    class Meta:
        model = Document

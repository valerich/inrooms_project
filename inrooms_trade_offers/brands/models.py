import os
import uuid
from collections import OrderedDict

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django_resized.forms import ResizedImageField
from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

from core.models import CURRENCY_CHOICES, DeleteMixin


CLIENT_KIND_CHOICES = Choices(
    (1, 'partner', 'Партнер'),
    (2, 'provider', 'Поставщик'),
)


LEGAL_FORM_CHOICES = Choices(
    (1, 'ip', 'Индивидуальный предприниматель'),
    (2, 'ooo', 'Общество с ограниченной ответственностью'),
)


def generate_brand_image_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'brand/fullsize/{}{}'.format(uuid.uuid4(), ext)


class Brand(TimeStampedModel):
    name = models.CharField('Название', max_length=100, unique=True)
    code = models.CharField('Код', max_length=20, unique=True)

    product_export_url = models.CharField('Урл для выгрузки товаров в 1с', max_length=500, blank=True, null=True)
    product_export_login = models.CharField('Логин для выгрузки товаров в 1с', max_length=50, blank=True, null=True)
    product_export_pass = models.CharField('Пароль для выгрузки товаров в 1с', max_length=100, blank=True, null=True)
    image = ResizedImageField('Изображение', upload_to=generate_brand_image_filename, max_length=755, blank=True)

    class Meta:
        verbose_name = 'Бренд'
        verbose_name_plural = 'Бренды'
        ordering = ('name', )

    def __str__(self):
        return 'id:{}, {}'.format(self.id, self.name)

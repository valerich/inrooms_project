from django.contrib import admin

# from config.celery import app
from .models import Brand


class BrandAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'code')
    search_fields = ('name', '=id', 'code')

    # actions = ('send_to_1c_action',)
    #
    # def send_to_1c_action(self, request, queryset):
    #     product_ids = list(queryset.values_list('id', flat=True))
    #     app.send_task('service_1c.notify_1c_client_change', args=(product_ids,))
    #     self.message_user(request, 'Отправлено на выгрузку в 1с')
    #
    # send_to_1c_action.short_description = 'Выгрузить в 1с'

for model_or_iterable, admin_class in (
    (Brand, BrandAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

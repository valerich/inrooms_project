import datetime

import factory
import factory.django
import factory.fuzzy

from .models import Brand


class BrandFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText(length=12)
    code = factory.fuzzy.FuzzyText(length=20)

    class Meta:
        model = Brand

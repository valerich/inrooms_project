from django.test import TestCase

from ..factories import BrandFactory
from ..models import Brand


class BrandFactoryTestCase(TestCase):
    def setUp(self):
        Brand.objects.all().delete()

    def test_factory(self):
        object = BrandFactory(code='brand_1')
        object_from_db = Brand.objects.get(code='brand_1')
        self.assertEqual(object, object_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(BrandFactory())

        self.assertEqual(len(objects), 10)
        self.assertEqual(len(objects), len(Brand.objects.all()))

import datetime

from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from clients.factories import ClientFactory
from ..factories import BrandFactory
from ..models import Brand


class BrandViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(BrandViewTestCase, cls).setUpClass()
        cls.list_url = reverse('api:brands:brand-list')

        UsersSetup()
        Brand.objects.all().delete()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')
        cls.store_client = ClientFactory(name='test_name')
        cls.store_1 = BrandFactory(
            name='ZaraCity',
            code='zaracity',
        )
        cls.store_2 = BrandFactory(
            name='Madii',
            code='madii',
        )

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_create(self):
        response = self.client_bm.post(self.list_url, {})
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_list(self):
        response = self.client_bm.get(self.list_url)
        self.assertEqual(response.data['count'], 2)
        response = self.client_pa.get(self.list_url)
        self.assertEqual(response.data['count'], 2)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client_bm.get(self.list_url)
        self.assertEqual(response.data['count'], 2)

        object_as_dict = response.data['results'][0]
        valid_list_item_dict = {
            "id": self.store_2.id,
            "name": 'Madii',
            "code": 'madii',
        }
        self.assertDictEqual(object_as_dict, valid_list_item_dict)

    def test_list_sort_by_fields(self):
        """Проверяем, что api сортируется по всем необходимым полям"""

        fields = [
            'id',
            'name',
        ]
        BrandFactory(
            name='Адин',
            code='aaa',
        )
        BrandFactory(
            name='ЯЯЯ',
            code='zzz',
        )
        for field in fields:
            for prefix in ['', '-']:
                filter_field = "{}{}".format(prefix, field)
                response = self.client_admin.get(self.list_url, {'sort_by': filter_field})
                object_dict = response.data['results'][0]
                object = Brand.objects.all().order_by(filter_field)[0]
                self.assertEqual(object_dict['id'], getattr(object, 'id'), 'Не сортируется по полю {} в {}'.format(filter_field, self.list_url))

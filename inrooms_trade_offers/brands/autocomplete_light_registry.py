from autocomplete_light import shortcuts as autocomplete_light

from .models import Brand


class BrandAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['=id', 'name', ]
    attrs = {
        'placeholder': u'=id, Название',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (Brand, BrandAutocomplete),
)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

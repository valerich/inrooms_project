from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'brands'
    verbose_name = 'Бренды'

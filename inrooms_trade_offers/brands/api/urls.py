from django.conf.urls import include, url
from rest_framework import routers

from .views import BrandViewSet

router = routers.SimpleRouter()

router.register(r'brand', BrandViewSet, base_name='brand')


urlpatterns = [
    url(r'^', include(router.urls)),
]

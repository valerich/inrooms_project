from rest_framework import filters
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin
from rest_framework.viewsets import GenericViewSet

from ..serializers import BrandSerializer
from ...models import Brand


class BrandViewSet(RetrieveModelMixin,
                   ListModelMixin,
                   GenericViewSet):
    serializer_class = BrandSerializer
    model = Brand
    queryset = Brand.objects.all()
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )
    search_fields = ('name', )
    ordering_fields = (
        'id',
        'name',
    )

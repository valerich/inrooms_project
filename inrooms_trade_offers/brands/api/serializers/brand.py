from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import Brand


class BrandSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = Brand
        fields = [
            'id',
            'name',
            'code',
            'image'
        ]

    def get_image(self, obj):
        if obj.image:
            try:
                _200x200 = get_thumbnail(obj.image, '200x200').url
            except IOError as e:
                return None
            request = self.context.get('request', None)
            src = obj.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200}
        else:
            return None

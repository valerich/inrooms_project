# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-07-17 12:39
from __future__ import unicode_literals

import brands.models
from django.db import migrations, models
import django.utils.timezone
import django_resized.forms
import model_utils.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Brand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('name', models.CharField(max_length=100, unique=True, verbose_name='Название')),
                ('code', models.CharField(max_length=20, unique=True, verbose_name='Код')),
                ('product_export_url', models.CharField(blank=True, max_length=500, null=True, verbose_name='Урл для выгрузки товаров в 1с')),
                ('product_export_login', models.CharField(blank=True, max_length=50, null=True, verbose_name='Логин для выгрузки товаров в 1с')),
                ('product_export_pass', models.CharField(blank=True, max_length=100, null=True, verbose_name='Пароль для выгрузки товаров в 1с')),
                ('image', django_resized.forms.ResizedImageField(blank=True, max_length=755, upload_to=brands.models.generate_brand_image_filename, verbose_name='Изображение')),
            ],
            options={
                'verbose_name': 'Бренд',
                'verbose_name_plural': 'Бренды',
                'ordering': ('name',),
            },
        ),
    ]

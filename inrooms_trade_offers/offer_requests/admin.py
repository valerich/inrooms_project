from django.contrib import admin

from .forms import OfferRequestItemAdminForm, ProcessingOfferRequestItemAdminForm
from .models import OfferRequest, OfferRequestItem, ProcessingOfferRequestItem, ProcessintOfferRequest


class ProcessingOfferRequestItemInline(admin.TabularInline):
    model = ProcessingOfferRequestItem
    form = ProcessingOfferRequestItemAdminForm
    extra = 0


class ProcessintOfferRequestAdmin(admin.ModelAdmin):
    list_display = ('id', 'created')
    inlines = [ProcessingOfferRequestItemInline, ]


class OfferRequestItemInline(admin.TabularInline):
    model = OfferRequestItem
    form = OfferRequestItemAdminForm
    extra = 0


class OfferRequestAdmin(admin.ModelAdmin):
    list_display = ('id', 'user')
    inlines = [OfferRequestItemInline, ]


for model_or_iterable, admin_class in (
    (ProcessintOfferRequest, ProcessintOfferRequestAdmin),
    (OfferRequest, OfferRequestAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

from collections import OrderedDict

from django.db import models

from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

OFFER_REQUEST_STATUS_CHOICE = Choices(
    (1, 'new', 'Новый'),
    (2, 'processed', 'Обработан'),
    (3, 'agreement', 'На согласовании'),
    (4, 'archive', 'Архив'),
    (0, 'closed', 'Закрыт'),
)


class ProcessintOfferRequest(TimeStampedModel):

    class Meta:
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявки'

    def __str__(self):
        return str(self.id)


class ProcessingOfferRequestItem(models.Model):
    processing_offer_request = models.ForeignKey(ProcessintOfferRequest, verbose_name='Заявка')
    offer = models.ForeignKey('offers.Offer', verbose_name='Предложение')
    count = models.IntegerField('Количество')
    color = models.ForeignKey('products.ProductColor', verbose_name='Цвет')

    class Meta:
        verbose_name = 'Элемент заявки'
        verbose_name_plural = 'Элементы заявок'


class OfferRequest(TimeStampedModel):
    STATUS = OFFER_REQUEST_STATUS_CHOICE
    status = models.PositiveSmallIntegerField('Статус', choices=STATUS, default=STATUS.new)
    client = models.ForeignKey('clients.CLient', verbose_name='Партнер', blank=True, null=True)
    user = models.ForeignKey('accounts.User', verbose_name='Создатель')
    processing_offer_request = models.ForeignKey(ProcessintOfferRequest, verbose_name='Заявка', blank=True, null=True)

    class Meta:
        verbose_name = 'Заявка на заказ предложений'
        verbose_name_plural = 'Заявки на заказ предложений'
        ordering = ('-created', )

    def get_amount_full(self):
        amount_full = 0
        for i in self.items.all():
            amount_full += i.get_amount()
        return amount_full

    def get_quantity(self):
        quantity = 0
        for i in self.items.all():
            quantity += i.get_ordered()
        return quantity


class OfferRequestItem(models.Model):
    offer_request = models.ForeignKey(OfferRequest, verbose_name='Заявка', related_name='items')
    offer = models.ForeignKey('offers.Offer', verbose_name='Предложение')
    color = models.ForeignKey('products.ProductColor', verbose_name='Цвет')
    series_count = models.PositiveSmallIntegerField('Количество серий', default=1)

    class Meta:
        verbose_name = 'Элемент заявки на заказ предложений'
        verbose_name_plural = 'Элементы заявок на заказ предложений'
        unique_together = ('offer_request', 'offer', 'color', )

    def get_ordered(self):
        ordered = 0
        for size_code, v in self.get_ordered_sizes_detail().items():
            ordered += v['ordered']
        return ordered

    def get_amount(self):
        if self.offer and self.offer.aggreement_price:
            return self.offer.aggreement_price * self.get_ordered()
        else:
            return 0

    def get_ordered_sizes_detail(self):
        ordered = OrderedDict()
        for size in self.offer.aggreement_sizes.all():
            ordered[size.code] = {
                'ordered': self.series_count,
                'multiplier': size.multiplier,
            }
        if len(ordered) == 1 and 'm' in ordered:
            for size_code in ordered:
                ordered[size_code]['ordered'] *= 6
        elif len(ordered) == 2 and all([item in ordered for item in ['s', 'm']]):
            for size_code in ordered:
                ordered[size_code]['ordered'] *= 3
        else:
            for size_code in ordered:
                ordered[size_code]['ordered'] *= ordered[size_code]['multiplier']
        return ordered

# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-07-18 09:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('offer_requests', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='offerrequestitem',
            name='series_count',
            field=models.PositiveSmallIntegerField(default=1, verbose_name='Количество серий'),
        ),
    ]

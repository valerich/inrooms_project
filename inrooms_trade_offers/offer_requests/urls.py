from django.conf.urls import url

from .views import OfferRequestExport

urlpatterns = [
    url(r'^offer_request/(?P<object_id>\d+)/export/$', OfferRequestExport.as_view(), name='offer-request-export'),
]

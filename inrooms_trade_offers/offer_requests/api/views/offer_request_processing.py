import logging

from django.contrib.contenttypes.models import ContentType
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from sorl.thumbnail.shortcuts import get_thumbnail

from comments.models import Comment
from offer_requests.models import OfferRequest, ProcessingOfferRequestItem, ProcessintOfferRequest
from offers.models import Offer, OfferStatus
from products.models import ProductColor

logger = logging.getLogger(__name__)


class OfferRequestProcessingView(APIView):

    def get(self, request, *args, **kwargs):
        self.request = request
        data = {}
        offer_requests = []
        for offer_request in OfferRequest.objects.filter(status=OfferRequest.STATUS.new):
            offer_requests.append(offer_request.id)
            for offer_request_item in offer_request.offerrequestitem_set.all():
                key = (offer_request_item.offer_id, offer_request_item.color_id)
                if key not in data:
                    data[key] = {
                        'offer_id': offer_request_item.offer_id,
                        'status_detail': {
                            'code': offer_request_item.offer.status.code,
                        },
                        'name': offer_request_item.offer.name,
                        'aggreement_code': offer_request_item.offer.aggreement_code,
                        'color_detail': {
                            'id': offer_request_item.color_id,
                            'name': offer_request_item.color.name,
                        },
                        'clients': [],
                        'image': self.resize_image(offer_request_item.offer.images.all()[0], offer_request_item.offer) if offer_request_item.offer.images.all().exists() else None,
                        'count': 0,
                        'aggreement_price': offer_request_item.offer.aggreement_price,
                    }
                if offer_request.user.client:
                    data[key]['count'] += offer_request.user.client.series_count
                    data[key]['clients'].append({
                        'name': offer_request.user.client.name,
                        'series_count': offer_request.user.client.series_count,
                    })

        response_data = []
        for key in sorted(data.keys()):
            response_data.append(data[key])
        return Response(data={'items': response_data,
                              'offer_requests': offer_requests})

    def post(self, request, *args, **kwargs):
        por = ProcessintOfferRequest.objects.create()
        offer_content_type = ContentType.objects.get_for_model(Offer)
        new_offer_status = OfferStatus.objects.get(code='approved')

        for item in request.data['items']:
            offer = Offer.objects.get(id=item['offer_id'])
            color = ProductColor.objects.get(id=item['color_id'])
            ProcessingOfferRequestItem.objects.create(processing_offer_request=por,
                                                      offer=offer,
                                                      color=color,
                                                      count=item['count'])
            if offer.status.code != 'models_created' and item['count']:
                offer.status = new_offer_status
                offer.save()
                Comment.objects.create(
                    content_type=offer_content_type,
                    object_id=offer.id,
                    body='Необходимо сделать заказ на {} - {} серий'.format(color.name, item['count']),
                    user=request.user,
                )

        offer_requests = OfferRequest.objects.filter(id__in=request.data['offer_requests'])
        offer_requests.update(processing_offer_request=por, status=OfferRequest.STATUS.processed)

        return Response(status=status.HTTP_200_OK)

    def resize_image(self, pi, obj):
        if pi:
            try:
                t = get_thumbnail(pi.image, '400x400', crop="center")
            except IOError as e:
                logger.warning('Проблемы с изображением {} продукта {}: {}'.format(pi.image, obj, e))
                return None
            request = self.request
            src = pi.image.url
            _200x200 = t.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,}
        else:
            return None

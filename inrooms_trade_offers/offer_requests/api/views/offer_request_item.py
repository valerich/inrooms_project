from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import OfferRequestItemSerializer
from ...models import OfferRequest, OfferRequestItem


class OfferRequestItemViewSet(ModelViewSet):
    serializer_class = OfferRequestItemSerializer
    model = OfferRequestItem
    queryset = OfferRequestItem.objects.all()
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )
    search_fields = ('=offer__id', )
    page_size = 1000

    def dispatch(self, request, offer_request_pk: int, *args, **kwargs):
        self.offer_request = get_object_or_404(OfferRequest, pk=offer_request_pk)
        return super(OfferRequestItemViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super(OfferRequestItemViewSet, self).get_queryset()
        qs = qs.filter(offer_request=self.offer_request)
        qs = qs.select_related('offer',
                               'color', )
        return qs

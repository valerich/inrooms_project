import django_filters
from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import OfferRequestSerializer
from ...models import OfferRequest


class OfferRequestFilterSet(django_filters.FilterSet):

    class Meta:
        model = OfferRequest
        fields = [
            'id',
            'user',
            'client',
            'status',
        ]


class OfferRequestViewSet(mixins.RetrieveModelMixin,
                          mixins.ListModelMixin,
                          mixins.UpdateModelMixin,
                          mixins.DestroyModelMixin,
                          GenericViewSet):
    serializer_class = OfferRequestSerializer
    model = OfferRequest
    queryset = OfferRequest.objects.all()
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, )
    filter_class = OfferRequestFilterSet
    search_fields = ('=id', 'client__name', 'user__first_name')

    def get_queryset(self):
        qs = super().get_queryset()
        if not self.request.user.has_perm('auth.can_all_offer_requests_view'):
            if self.request.user.client:
                qs = qs.filter(client=self.request.user.client)
                qs = qs.exclude(status=1)
            else:
                qs = qs.none()
        return qs

from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import OfferRequestItemViewSet, OfferRequestProcessingView, OfferRequestViewSet

router = DefaultRouter()
router.register(r'offer_request', OfferRequestViewSet)
router.register(r'offer_request/(?P<offer_request_pk>\d+)/items', OfferRequestItemViewSet, base_name='offer_request_items'),


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
    urls.url(r'^processing/$', OfferRequestProcessingView.as_view(), name='offer-request-processing-view'),
]

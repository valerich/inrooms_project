from rest_framework import serializers

from accounts.api.serializers import UserSerializer
from clients.api.serializers.client import ClientSerializer
from ...models import OfferRequest


class OfferRequestSerializer(serializers.ModelSerializer):
    user_detail = serializers.SerializerMethodField()
    status_detail = serializers.SerializerMethodField()
    client_detail = serializers.SerializerMethodField()
    quantity = serializers.SerializerMethodField()
    amount_full = serializers.SerializerMethodField()

    class Meta:
        model = OfferRequest
        fields = [
            'id',
            'user',
            'user_detail',
            'status',
            'status_detail',
            'created',
            'modified',
            'client',
            'client_detail',
            'quantity',
            'amount_full',
        ]

    def get_user_detail(self, obj):
        return UserSerializer(obj.user).data

    def get_status_detail(self, obj):
        return {
            'id': obj.status,
            'name': obj.get_status_display(),
        }

    def get_client_detail(self, obj):
        if obj.client:
            return ClientSerializer(obj.client, fields=['name', ]).data
        return None

    def get_quantity(self, obj):
        return obj.get_quantity()

    def get_amount_full(self, obj):
        return obj.get_amount_full()

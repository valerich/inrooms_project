import logging
from collections import OrderedDict

from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from ...models import OfferRequestItem

logger = logging.getLogger(__name__)


class OfferRequestItemSerializer(serializers.ModelSerializer):
    offer_detail = serializers.SerializerMethodField()
    color_detail = serializers.SerializerMethodField()
    ordered_sizes_detail = serializers.SerializerMethodField()
    ordered = serializers.SerializerMethodField()
    amount = serializers.SerializerMethodField()

    class Meta:
        model = OfferRequestItem
        fields = (
            'id',
            'offer_detail',
            'color_detail',
            'ordered',
            'ordered_sizes_detail',
            'amount',
            'series_count',  # количество серий
        )

    def get_ordered_sizes_detail(self, obj):
        return obj.get_ordered_sizes_detail()

    def get_ordered(self, obj):
        return obj.get_ordered()

    def get_amount(self, obj):
        return obj.get_amount()

    def get_offer_detail(self, obj):
        data = {
            'id': obj.offer.id,
            'name': obj.offer.name,
            'aggreement_code': obj.offer.aggreement_code,
            'aggreement_price': obj.offer.aggreement_price,
            'image': self.resize_image(obj.offer.images.all()[0], obj.offer) if obj.offer.images.all().exists() else None,
            'main_collection': None
        }
        if obj.offer.main_collection:
            data['main_collection'] = {
                'id': obj.offer.main_collection.id,
                'name': obj.offer.main_collection.name,
                'series_type': obj.offer.main_collection.series_type,
            }
        return data

    def get_color_detail(self, obj):
        return {
            'id': obj.color.id,
            'name': obj.color.name,
        }

    def resize_image(self, pi, obj):
        if pi:
            try:
                t = get_thumbnail(pi.image, '400x400', crop="center")
            except IOError as e:
                logger.warning('Проблемы с изображением {} продукта {}: {}'.format(pi.image, obj, e))
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            _200x200 = t.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,}
        else:
            return None

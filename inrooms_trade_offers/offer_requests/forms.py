from autocomplete_light import shortcuts as autocomplete_light
from django import forms


class ProcessingOfferRequestItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'color': autocomplete_light.ChoiceWidget('ProductColorAutocomplete'),
            'offer': autocomplete_light.ChoiceWidget('OfferAutocomplete'),
        }


class OfferRequestItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'color': autocomplete_light.ChoiceWidget('ProductColorAutocomplete'),
            'offer': autocomplete_light.ChoiceWidget('OfferAutocomplete'),
        }
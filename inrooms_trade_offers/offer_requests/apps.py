from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'offer_requests'
    verbose_name = 'Заявки на фабрику'

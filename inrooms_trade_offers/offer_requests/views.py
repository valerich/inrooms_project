import datetime

from django.shortcuts import get_object_or_404
from wkhtmltopdf.views import PDFTemplateView
from pytils import numeral

from offer_requests.models import OfferRequest


class OfferRequestExport(PDFTemplateView,):
    template_name = 'offer_requests/offer_request_pdf.html'
    filename = 'offer_request.pdf'

    def dispatch(self, request, object_id, *args, **kwargs):
        self.object = get_object_or_404(OfferRequest, id=object_id)
        self.with_size_quantity = request.GET.get('with_size_quantity', False) == 'true'
        self.delivery_date = request.GET.get('delivery_date', None)
        self.delivery_year = request.GET.get('delivery_year', None)
        self.with_date = request.GET.get('with_date', False) == 'true'
        self.document_number = request.GET.get('document_number', '')
        self.season_name = request.GET.get('season_name', '')
        self.season_year = request.GET.get('season_year', '')
        self.sort_by = request.GET.get('sort_by', None)
        if self.sort_by not in [
            'product__article',
            '-product__article',
            'product__main_collection__name',
            '-product__main_collection__name',
        ]:
            self.sort_by = None
        return super(OfferRequestExport, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(OfferRequestExport, self).get_context_data(*args, **kwargs)
        items = []
        items_part = []
        qs = self.object.items.all().select_related('offer')
        qs = qs.order_by('offer__aggreement_code', 'color__name')
        for item in qs:
            items_part.append(item)
            if (not items and len(items_part) >= 5) or (item and len(items_part) >= 6):
                items.append(items_part)
                items_part = []
        if items_part:
            items.append(items_part)
        context['order'] = self.object
        context['items'] = items
        context['delivery_year'] = self.delivery_year
        context['footer_on_new_page'] = False
        if len(items) > 1:
            if len(items[-1]) >= 4:
                context['footer_on_new_page'] = True
        elif len(items):
            if len(items[-1]) >= 3:
                context['footer_on_new_page'] = True
        context['pages_count'] = len(items) + (1 if context['footer_on_new_page'] else 0)
        context['with_size_quantity'] = self.with_size_quantity
        context['with_date'] = self.with_date
        context['document_number'] = self.document_number
        context['season_name'] = self.season_name
        context['season_year'] = self.season_year
        context['delivery_date'] = self.delivery_date
        return context

    def get_filename(self):
        today = datetime.date.today()
        return "Предзаказ {}, {}.pdf".format(self.object.user.client.name if self.object.user.client else '', today.strftime("%d.%m.%Y"))

from django.core.urlresolvers import reverse
from django.db import models
from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

from bot.utils import send_telegram_message
from .managers import CartManager


class Cart(models.Model):
    user = models.OneToOneField('accounts.User', verbose_name='Создатель')
    client = models.ForeignKey('clients.Client', verbose_name='Партнер', blank=True, null=True)

    objects = CartManager()

    class Meta:
        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзины'
        ordering = ('id', )

    def create_order(self):
        if self.items.count():
            preorder = Preorder(
                user=self.user,
                client=self.client
            )
            preorder.save()
            for item in self.items.all():
                preorder_item = PreorderItem(
                    preorder=preorder,
                    product=item.product
                )
                preorder_item.save()
            self.items.all().delete()

            send_telegram_message(message="{} cоздал предзаказ из каталога{}".format(
                self.user.get_full_name(),
                ' на {}'.format(self.client.name) if self.client else ''
            ), url=preorder.get_absolute_url())


class CartItem(models.Model):
    cart = models.ForeignKey(Cart, verbose_name='Корзина', related_name='items')
    product = models.ForeignKey('products.Product', verbose_name='Товар')

    class Meta:
        verbose_name = 'Элемент корзины'
        verbose_name_plural = 'Элементы корзины'
        ordering = ('id', )
        unique_together = ('cart', 'product', )


PREORDER_STATUS_CHOICES = Choices(
    (1, 'new', 'Новый'),
    (100, 'complete', 'Обработан'),
)


class Preorder(TimeStampedModel):
    STATUS = PREORDER_STATUS_CHOICES
    status = models.PositiveSmallIntegerField('Статус', choices=STATUS, default=STATUS.new)
    user = models.ForeignKey('accounts.User', verbose_name='Создатель')
    client = models.ForeignKey('clients.Client', verbose_name='Партнер', blank=True, null=True)

    class Meta:
        verbose_name = 'Предзаказ'
        verbose_name_plural = 'Предзаказы'
        ordering = ('-id', )

    def get_absolute_url(self):
        return reverse('admin:cart_preorder_change', args=(self.id,))


class PreorderItem(models.Model):
    preorder = models.ForeignKey(Preorder, verbose_name='Предзаказ', related_name='items')
    product = models.ForeignKey('products.Product', verbose_name='Товар')

    class Meta:
        verbose_name = 'Элемент предзаказа'
        verbose_name_plural = 'Элементы предзаказов'
        ordering = ('id', )
        unique_together = ('preorder', 'product', )

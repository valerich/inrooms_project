from django.contrib import admin
from nested_inline.admin import NestedTabularInline, NestedModelAdmin

from .forms import PreorderAdminForm, PreorderItemAdminForm
from .models import Cart, CartItem, Preorder, PreorderItem


class CartItemInline(admin.TabularInline):
    model = CartItem


class CartAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', ]
    inlines = [CartItemInline, ]


class PreorderItemAdminInline(NestedTabularInline):
    form = PreorderItemAdminForm
    model = PreorderItem
    extra = 0
    fk_name = 'preorder'


class PreorderAdmin(NestedModelAdmin):
    list_display = ('id',
                    'user',
                    'client', )
    search_fields = ('id', )
    form = PreorderAdminForm
    inlines = [PreorderItemAdminInline, ]


for model_or_iterable, admin_class in (
    (Cart, CartAdmin),
    (Preorder, PreorderAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

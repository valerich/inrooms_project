from autocomplete_light import shortcuts as autocomplete_light
from django import forms


class PreorderItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete')
        }


class PreorderAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'client': autocomplete_light.ChoiceWidget('ClientAutocomplete'),
            'user': autocomplete_light.ChoiceWidget('UserAutocomplete'),
        }

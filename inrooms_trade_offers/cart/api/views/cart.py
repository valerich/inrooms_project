from django.db.utils import IntegrityError
from rest_framework import filters, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from clients.models import Client
from products.models import Product
from ..serializers import CartItemSerializer, CartSerializer
from ...models import Cart, CartItem


class CartMixin(APIView):
    def dispatch(self, request, *args, **kwargs):
        self.cart = Cart.objects.get_for_user(request.user)
        return super(CartMixin, self).dispatch(request, *args, **kwargs)


class UserCartCreateOrderView(CartMixin):

    def post(self, request, *args, **kwargs):
        self.cart.create_order()
        return Response(status=status.HTTP_200_OK)


class UserCartView(CartMixin):
    def get(self, request, *args, **kwargs):
        return Response(data=CartSerializer(self.cart).data)

    def post(self, request, *args, **kwargs):
        self.cart.client_id = request.data.get('client', None)
        self.cart.save()
        return Response(data=CartSerializer(self.cart).data)

    def put(self, request, *args, **kwargs):
        client = request.data.get('client', None)
        if client:
            client = Client.objects.get(id=client)
        self.cart.client = client
        self.cart.save()
        return Response(data=CartSerializer(self.cart).data)


class UserCartItemsView(CartMixin):
    def get(self, request, *args, **kwargs):
        return Response(data=CartItemSerializer(self.cart.items.all(), many=True).data)


class UserCartItemsAddView(CartMixin):
    def post(self, request, *args, **kwargs):
        product_id = request.data.get('product_id', None)
        errors = []
        if product_id:
            try:
                product = Product.objects.get(id=product_id)
            except Product.DoesNotExist:
                errors.append('Товар не найден')
            else:
                try:
                    cart_item = CartItem(cart=self.cart, product=product)
                    cart_item.save()
                except IntegrityError:
                    errors.append('Товар уже добавлен в корзину')
                else:
                    return Response(status=status.HTTP_201_CREATED)
        else:
            errors.append('Не передан product_id')

        return Response(data={"error": ', '.join(errors)}, status=status.HTTP_400_BAD_REQUEST)


class UserCartItemsDeleteView(CartMixin):
    def post(self, request, *args, **kwargs):
        product_id = request.data.get('product_id', None)
        if product_id:
            CartItem.objects.filter(cart=self.cart, product_id=product_id).delete()
        return Response(status=status.HTTP_200_OK)


class UserCartItemsViewSet(CartMixin, ModelViewSet):
    model = CartItem
    serializer_class = CartItemSerializer
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)

    def get_queryset(self):
        return CartItem.objects.filter(cart=self.cart)

from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import PreorderItemSerializer
from ...models import PreorderItem, Preorder


class PreorderItemViewSet(ModelViewSet):
    serializer_class = PreorderItemSerializer
    model = PreorderItem
    queryset = PreorderItem.objects.all()
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )
    search_fields = ('=product__id', '^article', 'product__name')
    ordering_fields = (
        'product__article',
        'product__main_collection__name',
    )

    def dispatch(self, request, preorder_pk: int, *args, **kwargs):
        self.preorder = get_object_or_404(Preorder, pk=preorder_pk)
        return super(PreorderItemViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super(PreorderItemViewSet, self).get_queryset()
        qs = qs.filter(preorder=self.preorder)
        qs = qs.select_related('product',
                               'product__color', )
        qs = qs.prefetch_related('product__images')
        return qs

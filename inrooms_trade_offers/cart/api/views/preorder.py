import django_filters
from django.db import transaction
from rest_framework import filters
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from orders.models.order import Order, OrderItem
from products.models import Size
from products.services.product_remain_service import RemainService
from warehouses.models import Warehouse
from ..serializers import PreorderSerializer
from ...models import Preorder


class PreorderFilterSet(django_filters.FilterSet):

    class Meta:
        model = Preorder
        fields = [
            'client',
            'user',
        ]


class PreorderViewSet(ModelViewSet):
    serializer_class = PreorderSerializer
    filter_class = PreorderFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter)
    search_fields = ('=id', )
    queryset = Preorder.objects.all()
    ordering_fields = (
        'id',
        'modified',
    )

    @detail_route(methods=['post'])
    def create_order(self, request, pk=None):
        obj = self.get_object()
        if obj.client:
            series_count = obj.client.series_count

            with transaction.atomic():
                order = Order(
                    client=obj.client,
                    creator=request.user,
                )
                order.save()
                for item in obj.items.all():
                    oi = OrderItem(
                        order=order,
                        product=item.product,
                    )
                    oi.save()

                    for size in Size.objects.filter(series_type=oi.series_type):
                        oi.set_size_quantity_with_multiplier(size, series_count=series_count)

                order.process_order()
                obj.status = Preorder.STATUS.complete
                obj.save()

                return Response({
                    'status': 'ok',
                    'order_id': order.id
                })
        return Response({
            'status': 'ok',
        })


from django.conf.urls import include, url
from rest_framework import routers

from .views import UserCartItemsViewSet, UserCartItemsAddView, UserCartItemsDeleteView, UserCartView, UserCartItemsView, UserCartCreateOrderView, PreorderViewSet, PreorderItemViewSet

router = routers.SimpleRouter()

router.register(r'items', UserCartItemsViewSet, base_name='user-cart')

router2 = routers.SimpleRouter()
router2.register(r'preorder', PreorderViewSet, base_name='preorder')
router2.register(r'preorder/(?P<preorder_pk>\d+)/items', PreorderItemViewSet, base_name='preorder_items'),

urlpatterns = [
    url(r'^', include(router2.urls)),
    url(r'^cart/items/add/$', UserCartItemsAddView.as_view(), name='user-cart-items-add'),
    url(r'^cart/items/delete/$', UserCartItemsDeleteView.as_view(), name='user-cart-items-delete'),
    url(r'^cart/$', UserCartView.as_view(), name='user-cart'),
    url(r'^cart/create_order/$', UserCartCreateOrderView.as_view(), name='user-cart-create-order'),
    url(r'^cart/', include(router.urls)),
    url(r'^cart/items/$', UserCartItemsView.as_view(), name='user-cart-items'),
]

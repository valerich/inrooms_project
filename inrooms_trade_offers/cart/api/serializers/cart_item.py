from rest_framework import serializers

from products.api.serializers import ProductSerializer
from ...models import CartItem


class CartItemSerializer(serializers.ModelSerializer):
    product_detail = serializers.SerializerMethodField()

    class Meta:
        model = CartItem
        fields = (
            'id',
            'product_detail',
        )

    def get_product_detail(self, obj):
        return ProductSerializer(obj.product).data

from rest_framework import serializers

from accounts.api.serializers import UserSerializer
from clients.api.serializers.client import ClientSerializer
from ...models import Preorder


class PreorderSerializer(serializers.ModelSerializer):
    user_detail = serializers.SerializerMethodField()
    client_detail = serializers.SerializerMethodField()
    product_item_count = serializers.SerializerMethodField()
    status_detail = serializers.SerializerMethodField()

    class Meta:
        model = Preorder
        fields = (
            'id',
            'product_item_count',
            'client',
            'client_detail',
            'user',
            'user_detail',
            'status',
            'status_detail',
            'created',
            'modified',
        )

    def get_product_item_count(self, obj):
        return obj.items.count()

    def get_client_detail(self, obj):
        if obj.client:
            return ClientSerializer(obj.client, fields=['id', 'name', ]).data
        return None

    def get_user_detail(self, obj):
        return UserSerializer(obj.user, fields=['id', 'name']).data

    def get_status_detail(self, obj):
        return {
            'id': obj.status,
            'name': obj.get_status_display(),
        }

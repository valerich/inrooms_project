from rest_framework import serializers

from products.api.serializers import ProductSerializer
from ...models import PreorderItem


class PreorderItemSerializer(serializers.ModelSerializer):
    product_detail = serializers.SerializerMethodField()

    class Meta:
        model = PreorderItem
        fields = (
            'id',
            'product_detail',
        )

    def get_product_detail(self, obj):
        return ProductSerializer(obj.product).data

from rest_framework import serializers

from ...models import Cart


class CartSerializer(serializers.ModelSerializer):
    product_item_count = serializers.SerializerMethodField()

    class Meta:
        model = Cart
        fields = (
            'id',
            'product_item_count',
            'client',
        )

    def get_product_item_count(self, obj):
        return obj.items.count()

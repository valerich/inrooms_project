from wkhtmltopdf.views import PDFTemplateView

from .models import Cart


class CartExport(PDFTemplateView):  # (ExportDocxViewMixin, PDFTemplateView):
    template_name = 'cart/cart_pdf.html'
    filename = 'product.pdf'

    def dispatch(self, request, object_id, *args, **kwargs):
        self.object = Cart.objects.get_for_user(request.user)
        return super(CartExport, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(CartExport, self).get_context_data(*args, **kwargs)
        context['cart'] = self.object
        return context

    # def write_docx_document(self, document):
    #     document.add_heading('Товар: {}'.format(self.object.name), 0)
    #
    #     if self.has_common:
    #         document.add_heading('Основное', level=1)
    #         table = document.add_table(rows=3, cols=2)
    #         cells = table.rows[0].cells
    #         cells[0].text = 'Артикул:'
    #         cells[1].text = self.object.article
    #         cells = table.rows[1].cells
    #         cells[0].text = 'Коллекция:'
    #         cells[1].text = self.object.main_collection.name if self.object.main_collection else '-'
    #
    #     if self.has_fields:
    #         document.add_heading('Характеристики', level=1)
    #         table = document.add_table(rows=1, cols=2)
    #         hdr_cells = table.rows[0].cells
    #         hdr_cells[0].text = 'Характеристика'
    #         hdr_cells[1].text = 'Значение'
    #         for category, intags in self.get_intags().items():
    #             row_cells = table.add_row().cells
    #             p = row_cells[0].text = category.name.upper()
    #             for intag_data in intags:
    #                 row_cells = table.add_row().cells
    #                 row_cells[0].text = intag_data['intag'].name
    #                 row_cells[1].text = intag_data['value']
    #
    #     if self.has_images:
    #         document.add_heading('Изображения', level=1)
    #         for item in self.object.images.all():
    #             document.add_picture(str(item.image.file), width=Mm(100))
    #
    #     if self.has_description:
    #         document.add_heading('Описание', level=1)
    #         document.add_paragraph(strip_tags(self.object.description).replace('&nbsp;', ' '))
    #
    #     if self.has_comments:
    #         document.add_heading('Комментарии', level=1)
    #         table = document.add_table(rows=1, cols=3)
    #         hdr_cells = table.rows[0].cells
    #         hdr_cells[0].text = 'Пользователь'
    #         hdr_cells[1].text = 'Дата'
    #         hdr_cells[2].text = 'Текст'
    #         for item in self.object.comments.all():
    #             row_cells = table.add_row().cells
    #             row_cells[0].text = item.user.name
    #             row_cells[1].text = item.created.strftime('%d.%m.%Y %H:%M:%S')
    #             row_cells[2].text = strip_tags(item.body).replace('&nbsp;', ' ')
    #
    #     document.add_page_break()
    #     return document

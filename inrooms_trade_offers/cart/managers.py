from django.db import models


class CartManager(models.Manager):

    def get_for_user(self, user):
        from .models import Cart
        return Cart.objects.update_or_create(user=user, defaults={'client': user.client})[0]

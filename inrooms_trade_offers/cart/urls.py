from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^cart/(?P<object_id>\d+)/export/$', views.CartExport.as_view(), name='cart-export'),
]

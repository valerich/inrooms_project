from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'payments'
    verbose_name = 'Расчетные счета'

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from accounts.api.serializers import UserSerializer
from payments.models import ExpenseItem
from ...models import PaymentAccount


class PaymentAccountSerializer(serializers.ModelSerializer):
    currency_detail = serializers.SerializerMethodField()
    user_detail = serializers.SerializerMethodField()

    class Meta:
        model = PaymentAccount

        fields = [
            'id',
            'name',
            'currency',
            'currency_detail',
            'user_detail',
            'balance',
        ]

    def get_currency_detail(self, obj):
        return {
            'id': obj.currency,
            'name': obj.get_currency_display(),
        }

    def get_user_detail(self, obj):
        return UserSerializer(obj.user, fields=['id', 'name', ]).data


class DebitSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=20, decimal_places=2)
    expense_item = serializers.CharField(max_length=100)
    is_test = serializers.BooleanField(required=False)
    description = serializers.CharField(max_length=1000, required=False)

    class Meta:
        fields = [
            'amount',
            'expense_item',
            'is_test',
        ]

    def validate_expense_item(self, value):
        try:
            ei = ExpenseItem.objects.get(code=value)
        except ExpenseItem.DoesNotExist:
            raise ValidationError('Не найдена причина списания с данным кодом')
        else:
            return ei

    def validate_amount(self, value):
        if value <= 0:
            raise ValidationError('Сумма должна быть больше нуля')
        return value

    def validate(self, data):
        if self.context['payment_account'].balance < data['amount']:
            raise ValidationError('Не достаточно средств на счете')
        return data



from decimal import Decimal

import django_filters
from django.db import transaction
from model_utils.choices import Choices
from rest_framework import filters, mixins, status
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from core.models import CURRENCY_CHOICES
from history.models import HistoryItem
from payments.api_public.serializers.payment_account import DebitSerializer
from payments.models import PaymentDocument
from ..serializers import PaymentAccountSerializer
from ...models import PaymentAccount


class PaymentAccountFilterSet(django_filters.FilterSet):

    class Meta:
        model = PaymentAccount
        fields = [
            'currency',
            'user',
        ]


class PaymentAccountViewSet(mixins.RetrieveModelMixin,
                            mixins.ListModelMixin,
                            GenericViewSet):
    serializer_class = PaymentAccountSerializer
    model = PaymentAccount
    queryset = PaymentAccount.objects.all()
    filter_class = PaymentAccountFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)
    search_fields = ('name', )

    @detail_route(methods=['post'])
    @transaction.atomic()
    def debit(self, request, pk=None):
        obj = self.get_object()
        serializer = DebitSerializer(data=request.data, context={'payment_account': obj})
        serializer.is_valid(raise_exception=True)

        data = self._debit_payment_account(obj, request.user, serializer.validated_data)

        return Response(data=data)

    def _debit_payment_account(self, payment_account, user, data):
        amount_rur = payment_account.get_amount_rur(data['amount'])
        is_test = data.get('is_test', True)
        payment_document = None
        if not is_test:
            payment_document = PaymentDocument(
                kind=PaymentDocument.KIND.debit,
                payment_account=payment_account,
                destination_payment_account=payment_account,
                amount=data['amount'],
                amount_rur=amount_rur,
                user=user,
                description='Списание от 1с. ' + data.get('description', ''),
                expense_item=data['expense_item'],
            )

            payment_document.save()
            payment_document.process()
        return {
            'status': 'ok',
            'payment_account': {
                'id': payment_account.id,
                'currency': payment_account.currency,
            },
            'payment_document': {
                'id': payment_document.id
            } if payment_document else None,
            'amount': str(data['amount']),
            'amount_full_rur': str(amount_rur),
            'is_test': is_test
        }
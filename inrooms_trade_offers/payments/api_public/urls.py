from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import PaymentAccountViewSet


router = DefaultRouter()
router.register(r'payment_account', PaymentAccountViewSet, base_name='payment_account')



urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
]

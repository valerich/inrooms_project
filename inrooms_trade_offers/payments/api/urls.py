from django.conf import urls
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'payment_account', views.PaymentAccountViewSet, base_name='payment_account')
router.register(r'payment_document', views.PaymentDocumentViewSet, base_name='payment_document')
router.register(r'payment_method', views.PaymentMethodViewSet)
router.register(r'payment_user', views.PaymentUserViewSet)
router.register(r'expense_item', views.ExpenseItemViewSet)


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
    urls.url(r'currency-month/$', views.CurrencyMonth.as_view(), name='currency-month'),
    urls.url(r'transfer/(?P<transfer_type>rur_usd|rur_usd_cny|rur_cny)/$', views.PaymentAccountTransferView.as_view())
]

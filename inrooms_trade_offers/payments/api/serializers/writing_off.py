from rest_framework import serializers

from payments.models import ExpenseItem


def amount_validator(value):
        if value <= 0:
            raise serializers.ValidationError('Сумма должна быть положительной')


class WritingOffSerializer(serializers.Serializer):
    expense_item = serializers.PrimaryKeyRelatedField(queryset=ExpenseItem.objects.all())
    amount = serializers.DecimalField(max_digits=20, decimal_places=10, validators=[amount_validator, ])
    comment = serializers.CharField()

    def __init__(self, from_payment_account, *args, **kwargs):
        self.from_payment_account = from_payment_account
        super().__init__(*args, **kwargs)

    def validate(self, attrs):
        amount = attrs['amount']
        if amount > self.from_payment_account.balance:
            raise serializers.ValidationError("Не достаточно средств на счете")
        return attrs

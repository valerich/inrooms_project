from rest_framework import serializers

from accounts.models import User


class PaymentUserSerializer(serializers.ModelSerializer):
    payment_accounts = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()

    class Meta:
        model = User

        fields = [
            'id',
            'name',
            'payment_accounts',
        ]

    def get_payment_accounts(self, obj):
        data = []
        for pa in obj.paymentaccount_set.filter(is_active=True):
            data.append({
                'id': pa.id,
                'name': pa.name,
                'balance': pa.balance,
                'balance_rur': pa.balance_rur,
                'currency': pa.currency,
                'currency_name': pa.get_currency_display(),
            })
        return data

    def get_name(self, obj):
        return obj.get_full_name()

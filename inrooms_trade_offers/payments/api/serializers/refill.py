from rest_framework import serializers


def amount_validator(value):
    if value <= 0:
        raise serializers.ValidationError('Сумма должна быть положительной')


class RefillSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=20, decimal_places=10, validators=[amount_validator, ])

    class Meta:
        fields = [
            'amount',
        ]

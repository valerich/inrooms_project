from rest_framework import serializers

from accounts.api.serializers import UserSerializer
from ...models import PaymentAccount


class PaymentAccountSerializer(serializers.ModelSerializer):
    currency_detail = serializers.SerializerMethodField()
    user_detail = serializers.SerializerMethodField()
    has_self_transfer = serializers.SerializerMethodField()
    has_self_currency_transfer = serializers.SerializerMethodField()
    has_user_transfer = serializers.SerializerMethodField()

    class Meta:
        model = PaymentAccount

        fields = [
            'id',
            'name',
            'currency',
            'currency_detail',
            'balance',
            'balance_rur',
            'user_detail',
            'has_self_transfer',
            'has_self_currency_transfer',
            'has_user_transfer',
        ]

    def get_currency_detail(self, obj):
        return {
            'id': obj.currency,
            'name': obj.get_currency_display(),
        }

    def get_user_detail(self, obj):
        return UserSerializer(obj.user, fields=['id', 'name', ]).data

    def get_has_self_transfer(self, obj):
        return True if obj.transfers.filter(currency=obj.currency, user=obj.user) else False

    def get_has_self_currency_transfer(self, obj):
        return True if obj.transfers.filter(user=obj.user).exclude(currency=obj.currency) else False

    def get_has_user_transfer(self, obj):
        return True if obj.transfers.filter(currency=obj.currency).exclude(user=obj.user) else False

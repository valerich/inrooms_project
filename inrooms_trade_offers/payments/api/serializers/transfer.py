from model_utils.choices import Choices
from rest_framework import serializers

from payments.models import PaymentAccount

TRANSFER_TYPE_CHOICES = Choices(
    ('self_transfer', 'Перевод между своими счетами в одной валюте'),
    ('self_currency_transfer', 'Перевод между своими счетами в разной валюте'),
    ('user_transfer', 'Перевод другому пользователю'),
)

def amount_validator(value):
    if value <= 0:
        raise serializers.ValidationError('Значение должно быть положительным')


class TransferSerializer(serializers.Serializer):
    to_payment_account = serializers.PrimaryKeyRelatedField(queryset=PaymentAccount.objects.all())
    currency_rate = serializers.DecimalField(max_digits=20, decimal_places=10, required=False)
    transfer_type = serializers.ChoiceField(choices=TRANSFER_TYPE_CHOICES)
    amount = serializers.DecimalField(max_digits=20, decimal_places=10, validators=[amount_validator, ])

    class Meta:
        fields = [
            'amount',
        ]

    def __init__(self, from_payment_account, *args, **kwargs):
        self.from_payment_account = from_payment_account
        super().__init__(*args, **kwargs)
        self.fields['to_payment_account'].queryset = self.from_payment_account.transfers.all()

    def validate(self, attrs):
        amount = attrs['amount']
        transfer_type = attrs['transfer_type']
        if amount > self.from_payment_account.balance:
            raise serializers.ValidationError("Не достаточно средств на счете")
        if transfer_type == TRANSFER_TYPE_CHOICES.self_currency_transfer:
            currency_rate = attrs['currency_rate']
            if not currency_rate:
                raise serializers.ValidationError("Не передан курс")
        return attrs


class TransferRurUsdSerializer(serializers.Serializer):
    usd_payment_account = serializers.PrimaryKeyRelatedField(queryset=PaymentAccount.objects.all())
    rur = serializers.IntegerField(validators=[amount_validator, ])
    usd = serializers.IntegerField(validators=[amount_validator, ])
    additional_costs = serializers.IntegerField()
    comment = serializers.CharField(required=False)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)
        self.fields['usd_payment_account'].queryset = PaymentAccount.objects.filter(
            # user=user,
            currency=PaymentAccount.CURRENCY.USD)


class TransferRurUsdCnySerializer(serializers.Serializer):
    usd_payment_account = serializers.PrimaryKeyRelatedField(queryset=PaymentAccount.objects.all())
    cny_payment_account = serializers.PrimaryKeyRelatedField(queryset=PaymentAccount.objects.all())
    rur = serializers.IntegerField(validators=[amount_validator, ])
    usd = serializers.IntegerField(validators=[amount_validator, ])
    cny = serializers.IntegerField(validators=[amount_validator, ])
    additional_costs = serializers.IntegerField()
    comment = serializers.CharField(required=False)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)
        self.fields['usd_payment_account'].queryset = PaymentAccount.objects.filter(
            # user=user,
            currency=PaymentAccount.CURRENCY.USD)

        self.fields['cny_payment_account'].queryset = PaymentAccount.objects.filter(
            # user=user,
            currency=PaymentAccount.CURRENCY.CNY)


class TransferRurCnySerializer(serializers.Serializer):
    cny_payment_account = serializers.PrimaryKeyRelatedField(queryset=PaymentAccount.objects.all())
    rur = serializers.IntegerField(validators=[amount_validator, ])
    cny = serializers.IntegerField(validators=[amount_validator, ])
    additional_costs = serializers.IntegerField()
    comment = serializers.CharField(required=False)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

        self.fields['cny_payment_account'].queryset = PaymentAccount.objects.filter(
            # user=user,
            currency=PaymentAccount.CURRENCY.CNY)

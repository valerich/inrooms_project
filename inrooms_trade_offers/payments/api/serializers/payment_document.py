from rest_framework import serializers

from ...models import PaymentDocument


class PaymentDocumentSerializer(serializers.ModelSerializer):
    content_object_detail = serializers.SerializerMethodField()
    pa_kind = serializers.SerializerMethodField()
    account_balance = serializers.SerializerMethodField()
    account_balance_rur = serializers.SerializerMethodField()
    transfer_detail = serializers.SerializerMethodField()
    kind_detail = serializers.SerializerMethodField()

    class Meta:
        model = PaymentDocument
        fields = [
            'id',
            'kind',
            'kind_detail',
            'pa_kind',
            'account_balance',
            'account_balance_rur',
            'transfer_detail',
            'status',
            'payment_account',
            'destination_payment_account',
            'amount',
            'created',
            'modified',
            'currency_rate',
            'amount_rur',
            'description',
            'content_object_detail',
        ]

    def get_content_object_detail(self, obj):
        if obj.content_type and obj.object_id:
            return {
                'content_type': {
                    'app_label': obj.content_type.app_label,
                    'model': obj.content_type.model,
                    'name': obj.content_type.name,
                },
                'object_id': obj.object_id,
            }
        return None

    def get_pa_kind(self, obj):
        c_payment_account = self.context.get('payment_account', None)
        if obj.kind == obj.KIND.credit:
            return 'credit'
        elif obj.kind == obj.KIND.debit:
            return 'debit'
        elif obj.kind == obj.KIND.transfer and c_payment_account:
            if obj.payment_account == c_payment_account:
                return 'debit'
            elif obj.destination_payment_account == c_payment_account:
                return 'credit'
        return 'unknown'

    def get_account_balance(self, obj):
        c_payment_account = self.context.get('payment_account', None)
        if c_payment_account:
            if obj.payment_account == c_payment_account:
                return obj.payment_account_balance
            elif obj.destination_payment_account == c_payment_account:
                return obj.destination_payment_account_balance
        return None

    def get_account_balance_rur(self, obj):
        c_payment_account = self.context.get('payment_account', None)
        if c_payment_account:
            if obj.payment_account == c_payment_account:
                return obj.payment_account_balance_rur
            elif obj.destination_payment_account == c_payment_account:
                return obj.destination_payment_account_balance_rur
        return None

    def get_transfer_detail(self, obj):
        c_payment_account = self.context.get('payment_account', None)
        if obj.kind == obj.KIND.transfer:
            if c_payment_account:
                if obj.payment_account == c_payment_account:
                    return 'На счет "{}"'.format(obj.destination_payment_account.name)
                elif obj.destination_payment_account == c_payment_account:
                    return 'Со счета "{}"'.format(obj.payment_account.name)
            else:
                return 'Со счета "{}" на счет "{}"'.format(
                    obj.payment_account.name,
                    obj.destination_payment_account.name
                )
        return ''

    def get_kind_detail(self, obj):
        return {'name': obj.get_kind_display()}
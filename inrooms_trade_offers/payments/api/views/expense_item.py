from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from ..serializers import ExpenseItemSerializer
from ...models import ExpenseItem


class ExpenseItemViewSet(ModelViewSet):
    serializer_class = ExpenseItemSerializer
    model = ExpenseItem
    queryset = ExpenseItem.objects.all()
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend)
    filter_fields = ('kind', )
    search_fields = ('name', )

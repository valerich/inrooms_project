from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import PaymentMethodSerializer
from ...models import PaymentMethod


class PaymentMethodViewSet(mixins.RetrieveModelMixin,
                           mixins.ListModelMixin,
                           GenericViewSet):
    serializer_class = PaymentMethodSerializer
    model = PaymentMethod
    queryset = PaymentMethod.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )

from decimal import Decimal

import django_filters
from django.db import transaction
from model_utils.choices import Choices
from rest_framework import filters, mixins, status
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from core.models import CURRENCY_CHOICES
from payments.models import PaymentDocument
from ..serializers import PaymentAccountSerializer, RefillSerializer, TransferSerializer, WritingOffSerializer
from ...models import PaymentAccount

TRANSFER_TYPE_CHOICES = Choices(
    ('self_transfer', 'Перевод между своими счетами в одной валюте'),
    ('self_currency_transfer', 'Перевод между своими счетами в разной валюте'),
    ('user_transfer', 'Перевод другому пользователю'),
)


def filter_transfer_type(queryset, value):

    return queryset


class PaymentAccountFilterSet(django_filters.FilterSet):
    transfer_type = django_filters.ChoiceFilter(choices=TRANSFER_TYPE_CHOICES, action=filter_transfer_type)

    class Meta:
        model = PaymentAccount
        fields = [
            'transfer_type',
            'currency',
            'user',
        ]


class PaymentAccountViewSet(mixins.RetrieveModelMixin,
                            mixins.ListModelMixin,
                            GenericViewSet):
    serializer_class = PaymentAccountSerializer
    model = PaymentAccount
    queryset = PaymentAccount.objects.filter(is_active=True)
    filter_class = PaymentAccountFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)
    search_fields = ('name', )

    # def get_queryset(self):
    #     qs = super().get_queryset()
    #     qs = qs.filter(user=self.request.user)
    #     return qs

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        transfer_from_id = request.GET.get('transfer_from', None)
        if transfer_from_id:
            transfer_from = PaymentAccount.objects.get(id=transfer_from_id)
            transfer_type = request.GET.get('transfer_type', None)
            if transfer_type == TRANSFER_TYPE_CHOICES.self_transfer:
                queryset = PaymentAccount.objects.filter(
                    transfers_from=transfer_from,
                    user=transfer_from.user,
                    currency=transfer_from.currency,
                )
            elif transfer_type == TRANSFER_TYPE_CHOICES.self_currency_transfer:
                queryset = PaymentAccount.objects.filter(
                    transfers_from=transfer_from,
                    user=transfer_from.user,
                ).exclude(
                    currency=transfer_from.currency,
                )
            elif transfer_type == TRANSFER_TYPE_CHOICES.user_transfer:
                queryset = PaymentAccount.objects.filter(
                    transfers_from=transfer_from,
                    currency=transfer_from.currency,
                ).exclude(
                    user=transfer_from.user,
                )
            else:
                queryset = queryset.filter(user=self.request.user)
        else:
            if not self.request.user.has_perm('auth.can_all_payment_accounts_view'):
                queryset = queryset.filter(user=self.request.user)
        queryset = queryset.filter(is_active=True)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @detail_route(methods=['post'])
    @transaction.atomic()
    def refill(self, request, pk=None):
        obj = self.get_object()
        if obj.currency == obj.CURRENCY.RUR:
            serializer = RefillSerializer(data=request.data)
            serializer.is_valid(True)
            document = PaymentDocument(
                kind=PaymentDocument.KIND.credit,
                payment_account=obj,
                destination_payment_account=obj,
                amount=serializer.validated_data['amount'],
                amount_rur=serializer.validated_data['amount'],
                user=self.request.user,
                description="Пополнение счета через веб интерфейс"
            )
            document.save()
            document.process()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Вы не можете пополнять не рублевый счет'},
                            status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'])
    @transaction.atomic()
    def writing_off(self, request, pk=None):
        obj = self.get_object()
        serializer = WritingOffSerializer(from_payment_account=obj, data=request.data)
        serializer.is_valid(True)
        document = PaymentDocument(
            kind=PaymentDocument.KIND.debit,
            payment_account=obj,
            destination_payment_account=obj,
            amount=serializer.validated_data['amount'],
            amount_rur=obj.get_amount_rur(serializer.validated_data['amount']),
            user=self.request.user,
            description="Списание через веб интерфейс. Статья расходов: {}, комментарий: {}".format(
                serializer.validated_data['expense_item'].name,
                serializer.validated_data['comment']
            ),
            expense_item=serializer.validated_data['expense_item'],
        )
        document.save()
        document.process()
        return Response(status=status.HTTP_200_OK)

    @detail_route(methods=['post'])
    @transaction.atomic()
    def transfer(self, request, pk=None):
        obj = self.get_object()
        serializer = TransferSerializer(from_payment_account=obj, data=request.data)
        serializer.is_valid(True)
        if serializer.validated_data['transfer_type'] == TRANSFER_TYPE_CHOICES.self_transfer:
            document = PaymentDocument(
                kind=PaymentDocument.KIND.transfer,
                payment_account=obj,
                destination_payment_account=serializer.validated_data['to_payment_account'],
                amount=serializer.validated_data['amount'],
                amount_rur=obj.get_amount_rur(serializer.validated_data['amount']),
                user=self.request.user,
                description="Перевод между своими счетами"
            )
            document.save()
            document.process()
        elif serializer.validated_data['transfer_type'] == TRANSFER_TYPE_CHOICES.user_transfer:
            document = PaymentDocument(
                kind=PaymentDocument.KIND.transfer,
                payment_account=obj,
                destination_payment_account=serializer.validated_data['to_payment_account'],
                amount=serializer.validated_data['amount'],
                amount_rur=obj.get_amount_rur(serializer.validated_data['amount']),
                user=self.request.user,
                description="Перевод другому пользователю"
            )
            document.save()
            document.process()
        elif serializer.validated_data['transfer_type'] == TRANSFER_TYPE_CHOICES.self_currency_transfer:
            currency_rate = serializer.validated_data['currency_rate']
            if obj.currency == CURRENCY_CHOICES.USD and serializer.validated_data['to_payment_account'].currency == CURRENCY_CHOICES.CNY:
                currency_rate = Decimal('1.0000000000') / currency_rate
            document = PaymentDocument(
                kind=PaymentDocument.KIND.transfer,
                payment_account=obj,
                destination_payment_account=serializer.validated_data['to_payment_account'],
                amount=serializer.validated_data['amount'],
                currency_rate=currency_rate,
                amount_rur=obj.get_amount_rur(serializer.validated_data['amount']),
                user=self.request.user,
                description="Перевод между своими счетами в разной валюте"
            )
            document.save()
            document.process()
        return Response(status=status.HTTP_200_OK)

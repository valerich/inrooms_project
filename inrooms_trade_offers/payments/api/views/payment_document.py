import datetime

import django_filters
from django.db.models import Sum
from django.db.models.query_utils import Q
from pytils import numeral
from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from core.utils.numbers import format_number
from core.utils.views.additional_data_pagination import AdditionalDataPaginatedViewMixin
from payments.models import PaymentAccount
from ..serializers import PaymentDocumentSerializer
from ...models import PaymentDocument


def filter_created_gte(queryset, value):
    if value:
        queryset = queryset.filter(created__gte=value)
    return queryset


def filter_current_payment_account(queryset, value):
    if value:
        queryset = queryset.filter(Q(payment_account_id=value) | Q(destination_payment_account_id=value))
    return queryset


def filter_created_lte(queryset, value):
    if value:
        queryset = queryset.filter(created__lte=datetime.datetime.combine(value, datetime.datetime.max.time()))
    return queryset


class PaymentDocumentFilterSet(django_filters.FilterSet):
    created_gte = django_filters.DateFilter(action=filter_created_gte)
    created_lte = django_filters.DateFilter(action=filter_created_lte)
    current_payment_account = django_filters.NumberFilter(action=filter_current_payment_account)

    class Meta:
        model = PaymentDocument
        fields = [
            'user',
            'destination_payment_account',
            'created_gte',
            'created_lte',
            'expense_item',
        ]


class PaymentDocumentViewSet(AdditionalDataPaginatedViewMixin,
                             mixins.RetrieveModelMixin,
                             mixins.ListModelMixin,
                             GenericViewSet):
    serializer_class = PaymentDocumentSerializer
    model = PaymentDocument
    queryset = PaymentDocument.objects.all()
    filter_class = PaymentDocumentFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)
    search_fields = ('user__first_name', 'description', )

    def dispatch(self, request, *args, **kwargs):
        self.current_payment_account = None
        current_payment_account_id = request.GET.get('current_payment_account', None)
        if current_payment_account_id:
            try:
                self.current_payment_account = PaymentAccount.objects.get(id=current_payment_account_id)
            except PaymentAccount.DoesNotExist:
                pass
        return super().dispatch(request, *args, **kwargs)

    def get_additional_data(self, queryset):
        data = queryset.aggregate(Sum('amount'), Sum('amount_rur'))
        return {
            'amount_sum': data['amount__sum'] or 0,
            'amount_sum_display': format_number(data['amount__sum'] or 0),
            'amount_rur_sum': data['amount_rur__sum'] or 0,
            'amount_rur_sum_display': format_number(data['amount_rur__sum'] or 0),
        }

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['payment_account'] = self.current_payment_account
        return context


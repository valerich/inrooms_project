import datetime

from dateutil.relativedelta import relativedelta
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import CURRENCY_CHOICES
from core.utils.date import date_range
from ...models import CurrencyRate


class CurrencyMonth(APIView):

    def get(self, request, *args, **kwargs):
        to_date = datetime.date.today()
        from_date = to_date - datetime.timedelta(days=30)
        currency_list = [
            {'id': currency[0],
             'name': currency[1],
             'rate': 0,
             'data': [],
             'last_month_delta': None,
             'last_day_delta': None} for currency in CURRENCY_CHOICES if currency[0] != CURRENCY_CHOICES.RUR
        ]
        dates = date_range(from_date, to_date)
        data = {(cr.currency, cr.date): cr.rate for cr in CurrencyRate.objects.filter(
            date__gte=from_date,
            date__lte=to_date, ).order_by('date')}
        for currency in currency_list:
            cur_id = currency['id']
            for date in dates:
                currency['data'].append([date.strftime('%d.%m.%Y'), data.get((cur_id, date), None)])
            currency['rate'] = data.get((cur_id, to_date), None)
            try:
                currency['last_month_delta'] = currency['rate'] - data.get((cur_id, from_date), None)
            except TypeError:
                pass
            try:
                currency['last_day_delta'] = currency['rate'] - data.get((cur_id, to_date - relativedelta(days=1)), None)
            except TypeError:
                pass
        return Response(currency_list)

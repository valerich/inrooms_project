from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from accounts.models import User
from ..serializers import PaymentUserSerializer


class PaymentUserViewSet(mixins.RetrieveModelMixin,
                         mixins.ListModelMixin,
                         GenericViewSet):
    serializer_class = PaymentUserSerializer
    model = User
    queryset = User.objects.filter(paymentaccount__isnull=False).order_by().distinct()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('first_name', 'last_name', 'middle_name')

    # def get_queryset(self):
    #     qs = super().get_queryset()
    #     qs = qs.filter(user=self.request.user)
    #     return qs

    # def list(self, request, *args, **kwargs):
    #     queryset = self.filter_queryset(self.get_queryset())
    #
    #     transfer_from_id = request.GET.get('transfer_from', None)
    #     if transfer_from_id:
    #         transfer_from = PaymentAccount.objects.get(id=transfer_from_id)
    #         transfer_type = request.GET.get('transfer_type', None)
    #         if transfer_type == TRANSFER_TYPE_CHOICES.self_transfer:
    #             queryset = PaymentAccount.objects.filter(
    #                 transfers_from=transfer_from,
    #                 user=self.request.user,
    #                 currency=transfer_from.currency,
    #             )
    #         elif transfer_type == TRANSFER_TYPE_CHOICES.self_currency_transfer:
    #             queryset = PaymentAccount.objects.filter(
    #                 transfers_from=transfer_from,
    #                 user=self.request.user,
    #             ).exclude(
    #                 currency=transfer_from.currency,
    #             )
    #         elif transfer_type == TRANSFER_TYPE_CHOICES.user_transfer:
    #             queryset = PaymentAccount.objects.filter(
    #                 transfers_from=transfer_from,
    #                 currency=transfer_from.currency,
    #             ).exclude(
    #                 user=self.request.user,
    #             )
    #         else:
    #             queryset = queryset.filter(user=self.request.user)
    #     else:
    #         if not self.request.user.has_perm('auth.can_all_payment_accounts_view'):
    #             queryset = queryset.filter(user=self.request.user)
    #     page = self.paginate_queryset(queryset)
    #     if page is not None:
    #         serializer = self.get_serializer(page, many=True)
    #         return self.get_paginated_response(serializer.data)
    #
    #     serializer = self.get_serializer(queryset, many=True)
    #     return Response(serializer.data)

from decimal import Decimal

from django.db import transaction
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from payments.models import PaymentDocument
from ..serializers import TransferRurCnySerializer, TransferRurUsdCnySerializer, TransferRurUsdSerializer


class PaymentAccountTransferView(APIView):
    serializers = {
        'rur_usd': TransferRurUsdSerializer,
        'rur_usd_cny': TransferRurUsdCnySerializer,
        'rur_cny': TransferRurCnySerializer
    }

    def post(self, request, transfer_type, *args, **kwargs):
        serializer_class = self.serializers[transfer_type]
        serializer = serializer_class(user=request.user, data=request.data)
        serializer.is_valid(True)
        self.process_transfer(transfer_type, serializer.validated_data)
        return Response(status=status.HTTP_200_OK)

    def process_transfer(self, transfer_type, validated_data):
        if transfer_type == 'rur_usd':
            self.process_transfer_rur_usd(validated_data)
        elif transfer_type == 'rur_usd_cny':
            self.process_transfer_rur_usd_cny(validated_data)
        elif transfer_type == 'rur_cny':
            self.process_transfer_rur_cny(validated_data)

    @transaction.atomic()
    def process_transfer_rur_usd(self, validated_data):
        v_rur = Decimal(validated_data['rur'])
        v_usd = Decimal(validated_data['usd'])
        v_additional_costs = Decimal(validated_data['additional_costs'])
        rur = v_rur + v_additional_costs
        rur_usd_rate = v_usd / v_rur
        description = "Пополнение счета через веб интерфейс. "
        comment = validated_data.get('comment', '')
        if comment:
            description += '\n' + comment
        document = PaymentDocument(
            kind=PaymentDocument.KIND.credit,
            payment_account=validated_data['usd_payment_account'],
            destination_payment_account=validated_data['usd_payment_account'],
            amount=validated_data['usd'],
            amount_rur=rur,
            currency_rate=rur_usd_rate,
            user=self.request.user,
            description=description
        )
        document.save()
        document.process()

    @transaction.atomic()
    def process_transfer_rur_usd_cny(self, validated_data):
        v_rur = Decimal(validated_data['rur'])
        v_usd = Decimal(validated_data['usd'])
        v_cny = Decimal(validated_data['cny'])
        v_additional_costs = Decimal(validated_data['additional_costs'])
        rur = v_rur + v_additional_costs
        rur_usd_rate = v_usd / v_rur
        usd_cny_rate = v_usd / v_cny
        description = "Пополнение счета через веб интерфейс. "
        comment = validated_data.get('comment', '')
        if comment:
            description += '\n' + comment
        document = PaymentDocument(
            kind=PaymentDocument.KIND.credit,
            payment_account=validated_data['usd_payment_account'],
            destination_payment_account=validated_data['usd_payment_account'],
            amount=v_usd,
            amount_rur=rur,
            currency_rate=rur_usd_rate,
            user=self.request.user,
            description=description
        )
        document.save()
        document.process()

        description = "Перевод между своими счетами при пополнений через веб интерфейс. "
        comment = validated_data.get('comment', '')
        if comment:
            description += '\n' + comment
        document2 = PaymentDocument(
            kind=PaymentDocument.KIND.transfer,
            payment_account=validated_data['usd_payment_account'],
            destination_payment_account=validated_data['cny_payment_account'],
            amount=v_usd,
            amount_rur=rur,
            currency_rate=usd_cny_rate,
            user=self.request.user,
            description=description
        )
        document2.save()
        document2.process()

    @transaction.atomic()
    def process_transfer_rur_cny(self, validated_data):
        v_rur = Decimal(validated_data['rur'])
        v_cny = Decimal(validated_data['cny'])
        v_additional_costs = Decimal(validated_data['additional_costs'])
        rur = v_rur + v_additional_costs
        rur_cny_rate = v_rur / v_cny
        description = "Пополнение счета через веб интерфейс. "
        comment = validated_data.get('comment', '')
        if comment:
            description += '\n' + comment
        document = PaymentDocument(
            kind=PaymentDocument.KIND.credit,
            payment_account=validated_data['cny_payment_account'],
            destination_payment_account=validated_data['cny_payment_account'],
            amount=v_cny,
            amount_rur=rur,
            currency_rate=rur_cny_rate,
            user=self.request.user,
            description=description
        )
        document.save()
        document.process()

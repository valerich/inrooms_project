import datetime

import factory
import factory.django
import factory.fuzzy

from accounts.factories import UserFactory
from .models import CurrencyRate, PaymentAccount, PaymentMethod


class CurrencyRateFactory(factory.django.DjangoModelFactory):
    date = factory.fuzzy.FuzzyDate(datetime.date(2016, 1, 1))
    rate = factory.fuzzy.FuzzyDecimal(0.5)

    class Meta:
        model = CurrencyRate


class PaymentAccountFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    name = factory.fuzzy.FuzzyText()

    class Meta:
        model = PaymentAccount


class PaymentMethodFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = PaymentMethod

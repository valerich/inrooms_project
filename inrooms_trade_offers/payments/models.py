from decimal import Decimal

from autoslug.fields import AutoSlugField
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models, transaction
from django_extensions.db.models import TimeStampedModel
from model_utils.choices import Choices

from core.models import CURRENCY_CHOICES


EXPENSE_ITEM_KIND_CHOICE = Choices(
    (1, 'handmade', 'Создан в ручную'),
    (2, 'auto', 'Создан автоматически'),
)


class ExpenseItem(models.Model):
    KIND = EXPENSE_ITEM_KIND_CHOICE
    name = models.CharField('Название', max_length=255, unique=True)
    code = AutoSlugField('Код', unique=True, populate_from='name',
                         max_length=300, editable=False, always_update=False)
    kind = models.PositiveSmallIntegerField('Тип', choices=KIND, default=KIND.handmade)

    class Meta:
        verbose_name = 'Статья расходов'
        verbose_name_plural = 'Статьи расходов'
        ordering = ('id', )

    def __str__(self):
        return self.name

    @classmethod
    def get_amount_sawing_labels_item(cls):
        return ExpenseItem.objects.get_or_create(
            code='amount_sawing_labels_item',
            defaults={
                'name': 'Перешив этикеток',
                'kind': ExpenseItem.KIND.auto,
            }
        )[0]

    @classmethod
    def get_manufacturing_order_internal_label_payment(cls):
        return ExpenseItem.objects.get_or_create(
            code='manufacturing_order_internal_label_payment',
            defaults={
                'name': 'Оплата заказа внутренних этикеток в заказе на фабрику',
                'kind': ExpenseItem.KIND.auto,
            },
        )[0]

    @classmethod
    def get_manufacturing_order_payment(cls):
        return ExpenseItem.objects.get_or_create(
            code='manufacturing_order_payment',
            defaults={
                'name': 'Оплата заказа на фабрику',
                'kind': ExpenseItem.KIND.auto,
            },
        )[0]

    @classmethod
    def get_manufacturing_order_payment_cancel(cls):
        return ExpenseItem.objects.get_or_create(
            code='manufacturing_order_payment_cancel',
            defaults={
                'name': 'Отмена по заказу на фабрику',
                'kind': ExpenseItem.KIND.auto,
            },
        )[0]

    @classmethod
    def get_manufacturing_order_prepayment(cls):
        return ExpenseItem.objects.get_or_create(
            code='manufacturing_order_prepayment',
            defaults={
                'name': 'Аванс по заказу на фабрику',
                'kind': ExpenseItem.KIND.auto,
            },
        )[0]

    @classmethod
    def get_manufacturing_order_refund(cls):
        return ExpenseItem.objects.get_or_create(
            code='manufacturing_order_refund',
            defaults={
                'name': 'Возврат по заказу на фабрику',
                'kind': ExpenseItem.KIND.auto,
            },
        )[0]

    @classmethod
    def get_manufacturing_order_label_payment(cls):
        return ExpenseItem.objects.get_or_create(
            code='manufacturing_order_label_payment',
            defaults={
                'name': 'Оплата заказа этикеток',
                'kind': ExpenseItem.KIND.auto,
            },
        )[0]

    @classmethod
    def get_offer_prepayment(cls):
        return ExpenseItem.objects.get_or_create(
            code='offer_prepayment',
            defaults={
                'name': 'Аванс за предложение',
                'kind': ExpenseItem.KIND.auto,
            },
        )[0]

    @classmethod
    def get_offer_payment(cls):
        return ExpenseItem.objects.get_or_create(
            code='offer_payment',
            defaults={
                'name': 'Оплата за предложение',
                'kind': ExpenseItem.KIND.auto,
            },
        )[0]

    @classmethod
    def get_offer_return(cls):
        return ExpenseItem.objects.get_or_create(
            code='offer_return',
            defaults={
                'name': 'Возврат предложения',
                'kind': ExpenseItem.KIND.auto,
            },
        )[0]

    @classmethod
    def get_supply_delivery_payment(cls):
        return ExpenseItem.objects.get_or_create(
            code='supply_delivery_payment',
            defaults={
                'name': 'Оплата доставки',
                'kind': ExpenseItem.KIND.auto,
            },
        )[0]


class PaymentAccount(models.Model):
    CURRENCY = CURRENCY_CHOICES

    is_active = models.BooleanField('Активен?', default=True)

    user = models.ForeignKey('accounts.User', verbose_name='Пользователь')

    name = models.CharField('Название', max_length=255)
    code = AutoSlugField('Код', unique=True, populate_from='name',
                         max_length=300, editable=False, always_update=False)

    currency = models.CharField('Валюта', choices=CURRENCY, max_length=3)

    balance = models.DecimalField('Баланс', default=Decimal('0.0000000000'), max_digits=20, decimal_places=10)
    balance_rur = models.DecimalField('Баланс (р)', default=Decimal('0.0000000000'), max_digits=20, decimal_places=10)

    weight = models.PositiveSmallIntegerField('Вес', default=0)

    transfers = models.ManyToManyField('self', verbose_name='Доступные для перевода',
                                       blank=True, symmetrical=False, related_name="transfers_from")

    class Meta:
        verbose_name = 'Расчетный счет'
        verbose_name_plural = 'Расчетные счета'
        ordering = ('-weight', )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.balance_rur < Decimal('0.00'):
            self.balance_rur = Decimal('0.00')
        super().save(*args, **kwargs)

    def get_amount_rur(self, amount):
        if self.balance and self.balance_rur:
            currency_rate = (self.balance_rur / self.balance).quantize(Decimal('0.0000000000'))
            amount_rur = (amount * currency_rate).quantize(Decimal('0.0000000000'))
            return amount_rur
        return Decimal('0.00')

    def credit(self, amount, currency_rate=None, amount_rur=None):
        """Поступление денежных средств на счет"""
        if not isinstance(amount, Decimal):
            raise ValueError
        if currency_rate is not None and not isinstance(currency_rate, Decimal):
            raise ValueError
        if amount_rur is not None and not isinstance(amount_rur, Decimal):
            raise ValueError
        if self.currency == self.CURRENCY.RUR:
            self.balance += amount
            self.balance_rur += amount
        elif currency_rate is not None:
            self.balance += amount
            balance_rur = amount * currency_rate
            self.balance_rur += balance_rur.quantize(Decimal('0.0000000000'))
        elif amount_rur is not None:
            self.balance += amount
            self.balance_rur += amount_rur
        else:
            raise ValueError()

    def debit(self, amount):
        """Списание денежных средств"""
        if not isinstance(amount, Decimal):
            raise ValueError
        if amount > self.balance:
            raise ValueError
        currency_rate = (self.balance_rur / self.balance).quantize(Decimal('0.0000000000'))
        self.balance -= amount.quantize(Decimal('0.0000000000'))
        self.balance_rur = currency_rate * self.balance

    def transfer(self, to_payment_account, amount):
        if to_payment_account not in self.transfers.all():
            raise ValueError
        if self.currency == amount.currency:
            if self.balance < amount:
                raise ValueError
            self.balance -= amount
            self.balance_rur -= amount
            to_payment_account.balance += amount
            to_payment_account.balance_rur += amount


PAYMENT_DOCUMENT_KIND_CHOICES = Choices(
    (1, 'credit', 'Пополнение'),
    (2, 'debit', 'Списание'),
    (3, 'transfer', 'Перевод'),
)


STATUS_CHOICE = Choices(
    (0, 'new', 'Новый'),
    (200, 'complete', 'Обработан'),
    (-100, 'error', 'Обработан с ошибкой'),
)


class PaymentDocument(TimeStampedModel):
    KIND = PAYMENT_DOCUMENT_KIND_CHOICES
    STATUS = STATUS_CHOICE

    kind = models.PositiveSmallIntegerField('Тип документа', choices=KIND)
    status = models.PositiveSmallIntegerField('Статус', choices=STATUS, default=STATUS.new)

    user = models.ForeignKey('accounts.User', verbose_name='Пользователь')

    payment_account = models.ForeignKey(PaymentAccount, verbose_name='Счет источник', related_name='payment_documents_from')
    destination_payment_account = models.ForeignKey(PaymentAccount, verbose_name='Счет назначения', related_name='payment_focuments')
    expense_item = models.ForeignKey(ExpenseItem, verbose_name='Статья расходов', blank=True, null=True, on_delete=models.PROTECT)

    payment_account_balance = models.DecimalField('Баланс на счете источнике после совершения операции',
                                                  max_digits=20, decimal_places=10, default=Decimal('0.00'))
    payment_account_balance_rur = models.DecimalField('Баланс на счете источнике после совершения операции (руб)',
                                                      max_digits=20, decimal_places=10, default=Decimal('0.00'))
    destination_payment_account_balance = models.DecimalField('Баланс на счете назначения после совершения операции',
                                                              max_digits=20, decimal_places=10, default=Decimal('0.00'))
    destination_payment_account_balance_rur = models.DecimalField('Баланс на счете назначения после совершения операции (руб)',
                                                                  max_digits=20, decimal_places=10, default=Decimal('0.00'))

    amount = models.DecimalField('Сумма', max_digits=20, decimal_places=10)
    currency_rate = models.DecimalField('Курс валюты', default=Decimal('1.00'), max_digits=20, decimal_places=10)
    amount_rur = models.DecimalField('Сумма (р)', max_digits=20, decimal_places=10)

    description = models.TextField('Описание', blank=True)

    content_type = models.ForeignKey(ContentType, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id', )

    class Meta:
        verbose_name = 'Документ'
        verbose_name_plural = 'Документы'
        ordering = ('-created', )

    @transaction.atomic()
    def process(self):
        if self.status == self.STATUS.new:
            if self.kind == self.KIND.credit:
                self.destination_payment_account.balance += self.amount
                self.destination_payment_account.balance_rur += self.amount_rur
            elif self.kind == self.KIND.debit:
                self.destination_payment_account.balance -= self.amount
                self.destination_payment_account.balance_rur -= self.amount_rur
            elif self.kind == self.KIND.transfer:
                self.payment_account.balance -= self.amount
                self.payment_account.balance_rur -= self.amount_rur
                self.destination_payment_account.balance += self.amount / self.currency_rate
                self.destination_payment_account.balance_rur += self.amount_rur
                self.payment_account.save()
            self.destination_payment_account.save()
            self.payment_account_balance = self.payment_account.balance
            self.payment_account_balance_rur = self.payment_account_balance_rur
            self.destination_payment_account_balance = self.destination_payment_account.balance
            self.destination_payment_account_balance_rur = self.destination_payment_account_balance_rur
            self.status = self.STATUS.complete
            self.save()


class CurrencyRate(models.Model):
    CURRENCY = CURRENCY_CHOICES
    date = models.DateField('Дата')
    currency = models.CharField('Валюта', choices=CURRENCY, default=CURRENCY.CNY, max_length=3)
    rate = models.DecimalField('Курс (к рублю)', max_digits=20, decimal_places=10)
    show_in_dashboard = models.BooleanField('Показывать на dashboard', default=True)

    class Meta:
        verbose_name = 'Курс валюты'
        verbose_name_plural = 'Курсы валют'
        unique_together = ['date', 'currency']

    def __str__(self):
        return '{}: {} - {}'.format(self.date.strftime('%d.%m.%Y'), self.currency, self.rate)


class PaymentMethod(models.Model):
    name = models.CharField('Название', max_length=255)

    class Meta:
        verbose_name = 'Тип оплаты'
        verbose_name_plural = 'Типы оплаты'
        ordering = ('name', )

    def __str__(self):
        return self.name

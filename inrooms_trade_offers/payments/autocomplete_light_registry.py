from autocomplete_light import shortcuts as autocomplete_light

from .models import ExpenseItem, PaymentAccount, PaymentMethod


class PaymentAccountAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', 'code']
    attrs = {
        'placeholder': u'Название, код',
        'data-autocomplete-minimum-characters': 0,
    }


class PaymentMethodAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


class ExpenseItemAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (PaymentAccount, PaymentAccountAutocomplete),
    (PaymentMethod, PaymentMethodAutocomplete),
    (ExpenseItem, ExpenseItemAutocomplete),
)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

from django.contrib import admin

from .forms import PaymentAccountAdminForm
from .models import CurrencyRate, ExpenseItem, PaymentAccount, PaymentDocument, PaymentMethod


class CurrencyRateAdmin(admin.ModelAdmin):
    list_display = ['date', 'currency', 'rate']
    list_filter = ['currency', ]


class PaymentAccountAdmin(admin.ModelAdmin):
    list_display = ['name', 'is_active', 'code', 'currency', 'balance', 'balance_rur']
    list_filter = ['currency', 'is_active', ]
    form = PaymentAccountAdminForm


class PaymentMethodAdmin(admin.ModelAdmin):
    list_display = ['name', ]
    search_fields = ['name']


class PaymentDocumentAdmin(admin.ModelAdmin):
    list_display = ['created', 'kind', 'payment_account', 'destination_payment_account', 'amount', 'amount_rur', 'currency_rate', 'user', 'status', 'description', 'expense_item']
    list_filter = ['expense_item', ]


class ExpenseItemAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', ]


for model_or_iterable, admin_class in (
    (CurrencyRate, CurrencyRateAdmin),
    (PaymentAccount, PaymentAccountAdmin),
    (PaymentMethod, PaymentMethodAdmin),
    (PaymentDocument, PaymentDocumentAdmin),
    (ExpenseItem, ExpenseItemAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

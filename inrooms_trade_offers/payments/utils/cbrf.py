import datetime
import urllib.request as urllib
from decimal import Decimal, InvalidOperation
from urllib.error import URLError

import lxml.etree


class CBRCurrencyParser:
    """Парсер курсов валют с сайта cbr.ru"""

    api_url = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req='  # 14/10/2014
    xml_tree = None
    raw_xml = None

    def load_data(self, date: datetime.date, no_cache=False):
        try:
            self.raw_xml = urllib.urlopen(self.api_url + date.strftime("%d/%m/%Y")).read()
        except URLError:
            raise Exception("Server www.cbr.ru not responding")
        else:
            self._create_tree()

    def _create_tree(self):
        self.xml_tree = lxml.etree.XML(self.raw_xml)

    def get_currency(self, charcode):
        xpath_str = "./Valute[CharCode[.='{}']]".format(charcode)

        element = self.xml_tree.xpath(xpath_str)

        if element is not None:
            value = [el for el in element[0].getchildren() if el.tag == 'Value']
            nominal = [el for el in element[0].getchildren() if el.tag == 'Nominal']

            if len(value) > 0 and value[0] is not None and len(nominal) > 0 and nominal[0] is not None:
                try:
                    nominal = int(nominal[0].text)
                except (ValueError, TypeError):
                    raise Exception("Nominal tag not found!")
                try:
                    return Decimal(value[0].text.replace(',', '.')) / nominal
                except InvalidOperation:
                    raise Exception("Value tag not found!")

            raise Exception("Value tag not found!")

        raise Exception("Valute tag not found!")

from decimal import Decimal

from django.test import TestCase

from ..factories import CurrencyRateFactory, PaymentAccountFactory


class FactoryTestCase(TestCase):

    def test_payment_account_factory(self):
        payment_account = PaymentAccountFactory()
        self.assertIsNotNone(payment_account.id)

    def test_new_payments_account_balance(self):
        """У нового расчетного счета баланс должен быть равен нулю"""

        payment_account = PaymentAccountFactory()
        self.assertEqual(payment_account.balance, Decimal('0.0'))

    def test_currency_factory(self):
        currency = CurrencyRateFactory()
        self.assertIsNotNone(currency.id)

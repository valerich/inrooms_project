from decimal import Decimal

from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from ..factories import PaymentAccountFactory
from ..models import PaymentAccount


class PaymentAccountViewTestCase(APITestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.list_url = reverse('api:payments:payment_account-list')
        UsersSetup()

        cls.client_admin = APIClient()
        cls.client_admin.login(email='admin@example.com', password='password')

        cls.client_bm = APIClient()
        cls.client_bm.login(email='brandmanager@example.com', password='password')

        cls.client_pa = APIClient()
        cls.client_pa.login(email='processadministrator@example.com', password='password')

        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')
        cls.admin_user = User.objects.get(email='admin@example.com')

    def test_list(self):
        PaymentAccountFactory(user=self.admin_user)
        response = self.client_admin.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

    def test_list_permissions(self):
        """Каждый видит свои счета"""
        PaymentAccountFactory(user=self.admin_user)

        response = self.client_admin.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

        response = self.client_bm.get(self.list_url)
        self.assertEqual(response.data['count'], 0)

        # Администратор процесса видит все счета
        response = self.client_pa.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        object = PaymentAccountFactory(
            user=self.admin_user,
            name="test_name",
            code="tst",
            currency=PaymentAccount.CURRENCY.USD,
            balance=Decimal('12.23'),
            balance_rur=Decimal('100.12'),
        )
        response = self.client_admin.get(self.list_url)
        object_dict = response.data['results'][0]
        valid_object_dict = {
            "id": object.id,
            "name": "test_name",
            "currency": PaymentAccount.CURRENCY.USD,
            "currency_detail": {
                "id": PaymentAccount.CURRENCY.USD,
                "name": "Доллары",
            },
            "balance": '12.2300000000',
            "balance_rur": '100.1200000000',
            "user_detail": {
                "id": self.admin_user.id,
                "name": self.admin_user.name,
            },
            "has_self_transfer": False,
            "has_self_currency_transfer": False,
            "has_user_transfer": False,
        }
        self.assertDictEqual(object_dict, valid_object_dict)

    def test_refill(self):
        """Пополнение счета"""

        object = PaymentAccountFactory(
            user=self.admin_user,
            currency=PaymentAccount.CURRENCY.RUR,
            balance=Decimal('10.10'),
            balance_rur=Decimal('10.10'),
        )

        url = reverse('api:payments:payment_account-refill', kwargs={'pk': object.id})

        response = self.client_admin.post(url, {'amount': '22.34'})
        self.assertEqual(response.status_code, 200)
        object.refresh_from_db()
        self.assertEqual(object.balance, Decimal('32.44'))
        self.assertEqual(object.balance_rur, Decimal('32.44'))

    def test_refill_non_rur(self):
        """Пополнять можно только рублевый счет"""

        object = PaymentAccountFactory(
            user=self.admin_user,
            currency=PaymentAccount.CURRENCY.USD,
        )

        url = reverse('api:payments:payment_account-refill', kwargs={'pk': object.id})

        response = self.client_admin.post(url, {'amount': '22.34'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {
                'error': 'Вы не можете пополнять не рублевый счет',
            }
        )

    def test_refill_amount_fail(self):
        """Сумма при пополнении должна быть положительная"""

        object = PaymentAccountFactory(
            user=self.admin_user,
            currency=PaymentAccount.CURRENCY.RUR,
        )

        url = reverse('api:payments:payment_account-refill', kwargs={'pk': object.id})

        response = self.client_admin.post(url, {'amount': '0.00'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {
                'amount': ['Сумма должна быть положительной', ],
            }
        )

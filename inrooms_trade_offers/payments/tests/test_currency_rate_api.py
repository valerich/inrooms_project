import datetime

from django.core.urlresolvers import reverse
from freezegun import freeze_time
from rest_framework.test import APIClient, APITestCase

from accounts.tests.setup import UsersSetup
from core.models import CURRENCY_CHOICES
from core.utils.date import date_range
from payments.factories import CurrencyRateFactory


class CurrencyRateApiViewTestCase(APITestCase):

    @classmethod
    def setUpClass(cls):
        super(CurrencyRateApiViewTestCase, cls).setUpClass()
        cls.currency_month_url = reverse('api:payments:currency-month')

        date_from = datetime.date(2015, 12, 28)
        date_to = datetime.date(2016, 2, 10)
        for date in date_range(date_from, date_to):
            for currency in CURRENCY_CHOICES:
                CurrencyRateFactory(currency=currency[0], rate=date.day, date=date)
        UsersSetup()

    @freeze_time("2016-1-31")
    def test_currency_rate(self):
        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')
        response = self.client_bm.get(self.currency_month_url)
        # Тестиреем на CNY. Он идет вторым. Первый RUR, по которому api не возвращает данные
        test_currency = CURRENCY_CHOICES._doubles[1]
        # Вернулись данные по 3-м валютам (RUR не возвращаем)
        self.assertEqual(len(response.data), len(CURRENCY_CHOICES)-1)
        test_currency_data = response.data[0]
        self.assertEqual(test_currency_data['name'], test_currency[1])
        self.assertEqual(test_currency_data['id'], test_currency[0])
        self.assertEqual(test_currency_data['last_month_delta'], 30)
        self.assertEqual(test_currency_data['last_day_delta'], 1)
        self.assertEqual(len(test_currency_data['data']), 31)
        self.assertEqual(test_currency_data['data'][0][0], "01.01.2016")
        self.assertEqual(test_currency_data['data'][0][1], 1)

        self.assertEqual(test_currency_data['data'][-1][0], "31.01.2016")
        self.assertEqual(test_currency_data['data'][-1][1], 31)
        self.client_bm.session.clear()

    @freeze_time("2016-2-20")
    def test_currency_rate_none(self):
        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')
        response = self.client_bm.get(self.currency_month_url)
        # Тестиреем на CNY. Он идет вторым. Первый RUR, по которому api не возвращает данные
        test_currency = CURRENCY_CHOICES._doubles[1]
        # Вернулись данные по 3-м валютам (RUR не возвращаем)
        self.assertEqual(len(response.data), len(CURRENCY_CHOICES)-1)
        test_currency_data = response.data[0]
        self.assertEqual(test_currency_data['name'], test_currency[1])
        self.assertEqual(test_currency_data['id'], test_currency[0])
        self.assertEqual(test_currency_data['last_month_delta'], None)
        self.assertEqual(test_currency_data['last_day_delta'], None)
        self.assertEqual(len(test_currency_data['data']), 31)
        self.assertEqual(test_currency_data['data'][0][0], "21.01.2016")
        self.assertEqual(test_currency_data['data'][0][1], 21)

        self.assertEqual(test_currency_data['data'][-1][0], "20.02.2016")
        self.assertEqual(test_currency_data['data'][-1][1], None)
        self.client_bm.session.clear()

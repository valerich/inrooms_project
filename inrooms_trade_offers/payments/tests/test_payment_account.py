from decimal import Decimal

from django.test import TestCase

from accounts.factories import UserFactory
from ..factories import PaymentAccountFactory
from ..models import PaymentAccount


class PaymentAccountTestCase(TestCase):

    def test_new_payment_account_balance(self):
        """У нового расчетного счета баланс должен быть равен нулю"""

        payment_account = PaymentAccount.objects.create(user=UserFactory(), name='Счет 1')
        self.assertEqual(payment_account.balance, Decimal('0.0'))

    def test_credit(self):
        """Зачисление денег на счет"""

        payment_account = PaymentAccountFactory(currency=PaymentAccount.CURRENCY.RUR)
        payment_account.credit(Decimal('2.2'))
        self.assertEqual(payment_account.balance, Decimal('2.2'))
        self.assertEqual(payment_account.balance_rur, Decimal('2.2'))

        payment_account = PaymentAccountFactory(currency=PaymentAccount.CURRENCY.USD)
        payment_account.credit(Decimal('2'), currency_rate=Decimal('74.44'))
        self.assertEqual(payment_account.balance, Decimal('2.00'))
        self.assertEqual(payment_account.balance_rur, Decimal('148.88'))

        payment_account = PaymentAccountFactory(currency=PaymentAccount.CURRENCY.USD)
        payment_account.credit(Decimal('2'), amount_rur=Decimal('74.44'))
        self.assertEqual(payment_account.balance, Decimal('2.00'))
        self.assertEqual(payment_account.balance_rur, Decimal('74.44'))

    def test_debit(self):
        """Списание денег"""

        payment_account = PaymentAccountFactory(
            currency=PaymentAccount.CURRENCY.RUR,
            balance=Decimal('100.00'),
            balance_rur=Decimal('100.00')
        )
        payment_account.debit(Decimal('2.2'))
        self.assertEqual(payment_account.balance, Decimal('97.80'))
        self.assertEqual(payment_account.balance_rur, Decimal('97.80'))

        payment_account = PaymentAccountFactory(
            currency=PaymentAccount.CURRENCY.USD,
            balance=Decimal('100.00'),
            balance_rur=Decimal('200.00')
        )
        payment_account.debit(Decimal('2.2'))
        self.assertEqual(payment_account.balance, Decimal('97.80'))
        self.assertEqual(payment_account.balance_rur, Decimal('195.60'))

from decimal import Decimal

from django.test import TestCase

from ..utils.cbrf import CBRCurrencyParser


class CBRCurrencyParserTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(CBRCurrencyParserTestCase, cls).setUpClass()
        cls.cbfr_response = """
        <ValCurs Date="23.01.2016" name="Foreign Currency Market">
            <Valute ID="R01375">
                <NumCode>156</NumCode>
                <CharCode>CNY</CharCode>
                <Nominal>1</Nominal>
                <Name>Китайский юань</Name>
                <Value>12,2456</Value>
            </Valute>
        </ValCurs>
        """

    def test_get_currency(self):
        """Тестируем парсинг валюты"""

        parser = CBRCurrencyParser()
        parser.raw_xml = self.cbfr_response
        parser._create_tree()
        self.assertEqual(parser.get_currency('CNY'), Decimal('12.2456'))

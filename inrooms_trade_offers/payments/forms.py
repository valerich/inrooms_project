from autocomplete_light import shortcuts as autocomplete_light
from django import forms


class PaymentAccountAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'transfers': autocomplete_light.MultipleChoiceWidget('PaymentAccountAutocomplete'),
        }

import datetime

from config.celery import app
from core.models import CURRENCY_CHOICES
from .models import CurrencyRate
from .utils.cbrf import CBRCurrencyParser


@app.task(name="payments.get_currency")
def get_currency(date=None):
    if date:
        today = date
    else:
        today = datetime.date.today()
    cbrf_parser = CBRCurrencyParser()
    cbrf_parser.load_data(today)
    for currency in CURRENCY_CHOICES:
        if currency[0] != 'RUR':
            cr, created = CurrencyRate.objects.get_or_create(date=today, currency=currency[0], defaults={
                'rate': 0
            })
            try:
                rate = cbrf_parser.get_currency(currency[0])
            except Exception:
                pass
            else:
                cr.rate = rate
                cr.save()
        else:
            CurrencyRate.objects.get_or_create(date=today, currency=currency[0], defaults={
                'rate': 1
            })

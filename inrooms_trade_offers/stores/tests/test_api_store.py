from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from clients.factories import ClientFactory
from ..factories import StoreFactory
from ..models import Store


class StoreViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(StoreViewTestCase, cls).setUpClass()
        cls.list_url = reverse('api:stores:store-list')

        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')
        cls.store_client = ClientFactory(name='test_name')
        cls.store = StoreFactory(
            client=cls.store_client,
            name='Название',
            address='Адрес магазина',
            phone='+79991112233',
            opening='пн-вс',
            total_area=123,
            trade_area=234,
            employees_num=10,
            it_info='Тестовое инфо',
        )

        cls.valid_create_data = {
            'client': cls.store_client.id,
            'name': 'Тестовое название'
        }

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_create(self):
        response = self.client_bm.post(self.list_url, self.valid_create_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client_pa.post(self.list_url, self.valid_create_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_list(self):
        response = self.client_bm.get(self.list_url)
        self.assertEqual(response.data['count'], 1)
        response = self.client_pa.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client_bm.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

        object_as_dict = response.data['results'][0]
        valid_list_item_dict = {
            "id": self.store.id,
            "name": 'Название',
            "address": 'Адрес магазина',
            "phone": '+79991112233',
            "opening": 'пн-вс',
            "total_area": 123,
            "trade_area": 234,
            "employees_num": 10,
            "it_info": 'Тестовое инфо',
            "client": self.store_client.id,
            "client_detail": {
                "id": self.store_client.id,
                "name": "test_name",
            }
        }
        self.assertDictEqual(object_as_dict, valid_list_item_dict)

    def test_list_sort_by_fields(self):
        """Проверяем, что api сортируется по всем необходимым полям"""

        fields = [
            'id',
            'name',
        ]
        StoreFactory(name='один')
        StoreFactory(name='два')
        StoreFactory(name='Три')
        for field in fields:
            for prefix in ['', '-']:
                filter_field = "{}{}".format(prefix, field)
                response = self.client_admin.get(self.list_url, {'sort_by': filter_field})
                object_dict = response.data['results'][0]
                object = Store.objects.all().order_by(filter_field)[0]
                self.assertEqual(object_dict['id'], getattr(object, 'id'), 'Не сортируется по полю {} в {}'.format(filter_field, self.list_url))

from django.test import TestCase

from ..factories import EmployeeFactory, StoreFactory
from ..models import Employee, Store


class StoreFactoryTestCase(TestCase):
    def setUp(self):
        Store.objects.all().delete()

    def test_size_factory(self):
        store = StoreFactory()
        store_from_db = Store.objects.all()[0]
        self.assertEqual(store, store_from_db)


class EmployeeFactoryTestCase(TestCase):
    def setUp(self):
        Employee.objects.all().delete()

    def test_factory(self):
        object = EmployeeFactory()
        object_from_db = Employee.objects.all()[0]
        self.assertEqual(object, object_from_db)

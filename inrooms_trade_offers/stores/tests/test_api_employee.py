import datetime

from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from clients.factories import ClientFactory
from ..factories import EmployeeFactory, StoreFactory
from ..models import Employee


class EmployeeViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(EmployeeViewTestCase, cls).setUpClass()
        cls.list_url = reverse('api:stores:employee-list')

        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')
        cls.store_client = ClientFactory(name='test_name')
        cls.store = StoreFactory(
            client=cls.store_client,
            name='Название',
            address='Адрес магазина',
            phone='+79991112233',
            opening='пн-вс',
            total_area=123,
            trade_area=234,
            employees_num=10,
            it_info='Тестовое инфо',
        )
        cls.employee = EmployeeFactory(
            store=cls.store,
            first_name='Иван',
            middle_name='Иванович',
            last_name='Иванов',
            phone='9991112233',
            email='ivan@example.com',
            date_birthday=datetime.date(2016, 12, 23),
            date_employment=datetime.date(2016, 11, 22),
            is_working=True,
            position='Должность1',
        )

        cls.valid_create_data = {
            'store': cls.store.id,
            'first_name': 'Олег',
        }

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_create(self):
        response = self.client_bm.post(self.list_url, self.valid_create_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client_pa.post(self.list_url, self.valid_create_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_list(self):
        response = self.client_bm.get(self.list_url)
        self.assertEqual(response.data['count'], 1)
        response = self.client_pa.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client_bm.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

        object_as_dict = response.data['results'][0]
        valid_list_item_dict = {
            "id": self.employee.id,
            "first_name": 'Иван',
            "middle_name": 'Иванович',
            "last_name": 'Иванов',
            "phone": '9991112233',
            "email": 'ivan@example.com',
            "store": self.store.id,
            "store_detail": {
                "id": self.store.id,
                "name": "Название",
                "client_detail": {
                    "id": self.store_client.id,
                    "name": self.store_client.name,
                }
            },
            "user": None,
            "date_employment": "22.11.2016",
            "date_birthday": "23.12.2016",
            "is_working": True,
            "position": "Должность1"
        }
        self.assertDictEqual(object_as_dict, valid_list_item_dict)

    def test_list_sort_by_fields(self):
        """Проверяем, что api сортируется по всем необходимым полям"""

        fields = [
            'id',
            'first_name',
            'middle_name',
            'last_name',
            'phone',
            'email',
        ]
        EmployeeFactory(
            first_name='Иван',
            middle_name='Иванович',
            last_name='Иванов',
            phone='9991112233',
            email='ivan@example.com',
        )
        EmployeeFactory(
            first_name='Аристарх',
            middle_name='Аристархович',
            last_name='Аристархов',
            phone='1112223344',
            email='arist@example.com',
        )
        EmployeeFactory(
            first_name='Яков',
            middle_name='Яковлевич',
            last_name='Яковлев',
            phone='9999999999',
            email='yakov@example.com',
        )
        for field in fields:
            for prefix in ['', '-']:
                filter_field = "{}{}".format(prefix, field)
                response = self.client_admin.get(self.list_url, {'sort_by': filter_field})
                object_dict = response.data['results'][0]
                object = Employee.objects.all().order_by(filter_field)[0]
                self.assertEqual(object_dict['id'], getattr(object, 'id'), 'Не сортируется по полю {} в {}'.format(filter_field, self.list_url))

import factory
import factory.django
import factory.fuzzy

from clients.factories import ClientFactory
from .models import Employee, Store


class StoreFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    client = factory.SubFactory(ClientFactory)

    class Meta:
        model = Store


class EmployeeFactory(factory.django.DjangoModelFactory):
    first_name = factory.fuzzy.FuzzyText()
    store = factory.SubFactory(StoreFactory)

    class Meta:
        model = Employee

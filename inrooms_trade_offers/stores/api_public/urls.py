from django.conf.urls import url

from .views import TestSView, EmployeeWorkFactItemView


urlpatterns = [
    url(r'^test_s/$', TestSView.as_view(), name='test_s'),
    url(r'^employee_work_fact/$', EmployeeWorkFactItemView.as_view(), name='employee_work_fact')
]

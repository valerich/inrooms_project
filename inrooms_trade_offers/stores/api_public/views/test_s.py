from rest_framework.response import Response
from rest_framework.views import APIView


class TestSView(APIView):
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        data = {
            "count": 8,
            "next": None,
            "previous": None,
            "results": [{
                "id": 1,
                "name": "Mizonu wave ignitus",
                "price": 380,
                "type": "Soccer shoes",
                "image": "/media/1.png"
            }, {
                "id": 2,
                "name": "Mizonu wave ignitus1",
                "price": 381,
                "type": "Soccer shoes",
                "image": "/media/1.png"
            }, {
                "id": 3,
                "name": "Mizonu wave ignitus2",
                "price": 382,
                "type": "Soccer shoes",
                "image": "/media/1.png"
            }, {
                "id": 4,
                "name": "Mizonu wave ignitus3",
                "price": 383,
                "type": "Soccer shoes",
                "image": "/media/1.png"
            }, {
                "id": 5,
                "name": "Mizonu wave ignitus4",
                "price": 384,
                "type": "Soccer shoes",
                "image": "/media/1.png"
            }, {
                "id": 6,
                "name": "Mizonu wave ignitus5",
                "price": 385,
                "type": "Soccer shoes",
                "image": "/media/1.png"
            }, {
                "id": 7,
                "name": "Mizonu wave ignitus6",
                "price": 386,
                "type": "Soccer shoes",
                "image": "/media/1.png"
            }, {
                "id": 8,
                "name": "Mizonu wave ignitus7",
                "price": 387,
                "type": "Soccer shoes",
                "image": "/media/1.png"
            }]
        }
        return Response(data=data)
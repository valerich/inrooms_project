from django.db import transaction
from rest_framework.response import Response
from rest_framework.status import HTTP_403_FORBIDDEN
from rest_framework.views import APIView
from rest_framework import serializers

from stores.models import Store, Employee, EmployeeWorkFactItem


class EmployeeFactSerializer(serializers.Serializer):
    employee = serializers.PrimaryKeyRelatedField(queryset=Employee.objects.all())
    hours = serializers.IntegerField()

    class Meta:
        fields = [
            'employee',
            'hours',
        ]


class EmployeeWorkFactSerializer(serializers.Serializer):
    date = serializers.DateField()
    store = serializers.PrimaryKeyRelatedField(queryset=Store.objects.all())
    employees = EmployeeFactSerializer(many=True)

    class Meta:
        fields = [
            'date',
            'store',
            'employees',
        ]


class EmployeeWorkFactItemView(APIView):

    def post(self, request, *args, **kwargs):
        if request.user.has_perm('auth.api_public_employee_work_fact_post'):
            serializer = EmployeeWorkFactSerializer(data=request.data, many=True)
            serializer.is_valid(True)
            self.save_data(serializer.validated_data)
            return Response()
        else:
            return Response(data={'error': 'Не достаточно прав'}, status=HTTP_403_FORBIDDEN)

    @transaction.atomic()
    def save_data(self, data):
        for item in data:
            saved_ids = set()
            store = item['store']
            date = item['date']
            for employee_item in item['employees']:
                employee = employee_item['employee']
                hours = employee_item['hours']

                if hours:
                    ewfi, c = EmployeeWorkFactItem.objects.update_or_create(
                        store=store,
                        date=date,
                        employee=employee,
                        defaults={
                            'hours': hours
                        }
                    )
                    saved_ids.add(ewfi.id)
            EmployeeWorkFactItem.objects.filter(
                store=store,
                date=date,
            ).exclude(
                id__in=saved_ids
            ).delete()

from autocomplete_light import shortcuts as autocomplete_light

from .models import Employee, Store


class EmployeeAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['first_name', 'last_name', 'email', 'phone']
    attrs = {
        'placeholder': 'ФИО, телефон, email',
        'data-autocomplete-minimum-characters': 0,
    }


class StoreAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['=id', 'name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (Employee, EmployeeAutocomplete),
    (Store, StoreAutocomplete),
)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

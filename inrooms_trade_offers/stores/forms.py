from autocomplete_light import shortcuts as autocomplete_light
from django import forms


class EmployeeForm(forms.ModelForm):
    class Meta:
        widgets = {
            'user': autocomplete_light.ChoiceWidget('UserAutocomplete'),
            'store': autocomplete_light.ChoiceWidget('StoreAutocomplete'),
        }


class StoreAdminForm(forms.ModelForm):
    class Meta:
        widgets = {
            'client': autocomplete_light.ChoiceWidget('ClientPartnerAutocomplete'),
        }

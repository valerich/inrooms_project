import django_filters
from django.conf import settings
from rest_framework import filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from config.celery import app
from stores.api.serializers.employee_file import EmployeeFileSerializer
from stores.models import EmployeeImage, EmployeeFile
from ..serializers import EmployeeSerializer, EmployeeImageSerializer
from ...models import Employee


class EmployeeFilterSet(django_filters.FilterSet):

    class Meta:
        model = Employee
        fields = [
            'store',
            'store__client',
            'is_working',
        ]


class EmployeeViewSet(ModelViewSet):
    serializer_class = EmployeeSerializer
    model = Employee
    filter_class = EmployeeFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', 'first_name', 'middle_name', 'last_name', 'phone', 'email')
    ordering_fields = (
        'id',
        'first_name',
        'middle_name',
        'last_name',
        'phone',
        'email',
        'store__client__name',
    )

    def get_queryset(self):
        qs = Employee.objects.all().select_related('store', 'store__client').order_by('store__client__name', 'first_name', 'middle_name', 'last_name')
        return qs

    def perform_update(self, serializer):
        super(EmployeeViewSet, self).perform_update(serializer)
        if not settings.IS_TESTING:
            app.send_task('service_1c.notify_1c_employee_change', args=((serializer.instance.id,),))

    def perform_create(self, serializer):
        obj = serializer.save()
        if not settings.IS_TESTING:
            app.send_task('service_1c.notify_1c_employee_change', args=((obj.id,),))

    @detail_route(methods=['post'])
    def image_upload(self, request, pk=None):
        obj = self.get_object()
        file_obj = request.FILES['file']

        _file = EmployeeImage.objects.create(
            image=file_obj,
            employee=obj
        )
        return Response({
            'status': 'ok'
        })

    @list_route(methods=['post'])
    def image_delete(self, request):
        file_id = request.data.get('file_id', None)

        try:
            _file = EmployeeImage.objects.get(
                id=file_id
            )
        except EmployeeImage.DoesNotExist:
            pass
        else:
            _file.delete()
        return Response({
            'status': 'ok'
        })

    @detail_route(methods=['get'])
    def images(self, request, pk=None):
        obj = self.get_object()
        return Response(EmployeeImageSerializer(obj.images.all(), many=True).data)

    @detail_route(methods=['post'])
    def file_upload(self, request, pk=None):
        obj = self.get_object()
        file_obj = request.FILES['file']

        _file = EmployeeFile.objects.create(
            filename=file_obj.name,
            file=file_obj,
        )
        obj.files.add(_file)
        return Response({
            'status': 'ok'
        })

    @list_route(methods=['post'])
    def file_delete(self, request):
        # obj = self.get_object()
        file_id = request.data.get('file_id', None)

        try:
            _file = EmployeeFile.objects.get(
                id=file_id
            )
        except EmployeeFile.DoesNotExist:
            pass
        else:
            _file.delete()
        return Response({
            'status': 'ok'
        })

    @detail_route(methods=['get'])
    def files(self, request, pk=None):
        obj = self.get_object()
        return Response(EmployeeFileSerializer(obj.files.all(), many=True).data)

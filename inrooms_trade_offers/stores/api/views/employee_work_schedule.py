import calendar
import datetime
import locale

from django.db import transaction
from rest_framework.status import HTTP_400_BAD_REQUEST

from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from stores.models import Store, EmployeeWorkScheduleItem, Employee


class EmployeeWorkScheduleView(APIView):

    def get(self, request, store_pk, year, month, *args, **kwargs):
        self.store = get_object_or_404(Store, pk=store_pk)
        self.year = int(year)
        self.month = int(month)
        data = self.get_data()
        return Response(data=data)

    def post(self, request, store_pk, year, month, *args, **kwargs):
        self.store = get_object_or_404(Store, pk=store_pk)
        self.year = int(year)
        self.month = int(month)
        try:
            self.save_data(request.data)
        except Exception as e:
            return Response(data={'error': str(e)}, status=HTTP_400_BAD_REQUEST)
        data = self.get_data()
        return Response(data=data)

    @transaction.atomic()
    def save_data(self, to_save_data):
        saved_ids = set()
        for field in [key for key in to_save_data.keys() if key.startswith('empl_')]:
            temp = field.replace('empl_', '').split('_day_')
            if len(temp) == 2 and temp[0].isdigit() and temp[1].isdigit():
                employee_id = int(temp[0])
                day = int(temp[1])
                hours = to_save_data[field]
                if hours:
                    hours = int(hours)
                    date = datetime.date(self.year, self.month, day)
                    ews, c = EmployeeWorkScheduleItem.objects.update_or_create(
                        date=date,
                        employee_id=employee_id,
                        store=self.store,
                        defaults={
                            'hours': hours
                        }
                    )
                    saved_ids.add(ews.id)
            else:
                raise Exception('Не верные данные')
        EmployeeWorkScheduleItem.objects.filter(
            date__year=self.year,
            date__month=self.month,
            store=self.store,
        ).exclude(
            id__in=saved_ids
        ).delete()

    def get_data(self):
        try:
            locale.setlocale(locale.LC_ALL, 'ru_RU')
        except Exception:
            pass
        weekdays = list(calendar.day_abbr)
        plan_data = self.get_current_plan_data()

        data = {
            'store_name': self.store.name,
            'year': self.year,
            'month': self.month,
            'employees': [{
                'id': e.id,
                'name': e.get_full_name()
            } for e in Employee.objects.filter(store=self.store)],
            'days': [{
                'num': d,
                'weekday': datetime.date(self.year, self.month, d).weekday(),
                'weekday_name': weekdays[datetime.date(self.year, self.month, d).weekday()],
            } for d in range(1, calendar.monthrange(self.year, self.month)[1] + 1)]
        }

        for e in data['employees']:
            employee_current_plan = plan_data.get(e['id'], {})
            for day in data['days']:
                data['empl_{}_day_{}'.format(e['id'], day['num'])] = employee_current_plan.get(day['num'], '')
        return data

    def get_current_plan_data(self):
        plan_data = {}
        for i in EmployeeWorkScheduleItem.objects.filter(
                store=self.store,
                date__year=self.year,
                date__month=self.month,
        ):
            if i.employee_id not in plan_data:
                plan_data[i.employee_id] = {}
            plan_data[i.employee_id][i.date.day] = i.hours
        return plan_data

from django.conf import settings

import django_filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response

from clients.models import Client
from config.celery import app
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from stores.models import StoreFile
from ..serializers import StoreSerializer, StoreFileSerializer
from ...models import Store


def filter_is_own(queryset, value):
    client = Client.objects.get_own()
    if value:
        queryset = queryset.filter(client=client)
    else:
        queryset = queryset.exclude(client=client)
    return queryset


class StoreFilterSet(django_filters.FilterSet):
    is_own = django_filters.BooleanFilter(action=filter_is_own)

    class Meta:
        model = Store
        fields = [
            'client',
            'is_own',
            'is_active',
        ]


class StoreViewSet(ModelViewSet):
    serializer_class = StoreSerializer
    model = Store
    filter_class = StoreFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', 'name', )
    ordering_fields = (
        'id',
        'name',
    )

    def get_queryset(self):
        qs = Store.objects.all().select_related('client', )
        return qs

    def perform_update(self, serializer):
        super(StoreViewSet, self).perform_update(serializer)
        if not settings.IS_TESTING:
            app.send_task('service_1c.notify_1c_store_change', args=((serializer.instance.id,),))

    def perform_create(self, serializer):
        if serializer.validated_data.pop('is_own', None):
            client = Client.objects.get_own()
            obj = serializer.save(client=client)
        else:
            obj = serializer.save()
        if not settings.IS_TESTING:
            app.send_task('service_1c.notify_1c_store_change', args=((obj.id,),))

    @detail_route(methods=['post'])
    def file_upload(self, request, pk=None):
        obj = self.get_object()
        file_obj = request.FILES['file']

        _file = StoreFile.objects.create(
            filename=file_obj.name,
            file=file_obj,
        )
        obj.files.add(_file)
        return Response({
            'status': 'ok'
        })

    @list_route(methods=['post'])
    def file_delete(self, request):
        # obj = self.get_object()
        file_id = request.data.get('file_id', None)

        try:
            _file = StoreFile.objects.get(
                id=file_id
            )
        except StoreFile.DoesNotExist:
            pass
        else:
            _file.delete()
        return Response({
            'status': 'ok'
        })

    @detail_route(methods=['get'])
    def files(self, request, pk=None):
        obj = self.get_object()
        return Response(StoreFileSerializer(obj.files.all(), many=True).data)

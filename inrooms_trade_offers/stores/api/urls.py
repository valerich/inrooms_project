from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import EmployeeViewSet, StoreViewSet, EmployeeWorkScheduleView

router = DefaultRouter()
router.register(r'employee', EmployeeViewSet, base_name='employee')
router.register(r'store', StoreViewSet, base_name='store')


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
    urls.url(r'^store/(?P<store_pk>\d+)/work_schedule/(?P<year>\d+)/(?P<month>\d+)/$', EmployeeWorkScheduleView.as_view(), name='employee-work-schedule'),
]

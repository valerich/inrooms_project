from rest_framework import serializers

from clients.api.serializers.client import ClientSerializer
from clients.models import Client
from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import Store


class StoreSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    client_detail = serializers.SerializerMethodField()
    is_own = serializers.BooleanField(write_only=True, required=False)
    client = serializers.PrimaryKeyRelatedField(queryset=Client.objects.all(), required=False)

    class Meta:
        model = Store

        fields = [
            'id',
            'is_active',
            'is_own',
            'name',
            'client',
            'client_detail',
            'address',
            'address_description',
            'phone',
            'opening',
            'total_area',
            'trade_area',
            'employees_num',
            'it_info',
            'competitors',
            'products',
            'target_audience',
            'plan_avg_cheque',
            'plan_conversion',
            'emporium_url',
            'emporium_contacts',
        ]

    def get_client_detail(self, obj):
        if obj.client_id:
            return ClientSerializer(obj.client, fields=['id', 'name']).data
        else:
            return None

    def validate(self, data):
        if not data.get('is_own', False) and not data.get('client', None):
            raise serializers.ValidationError({"client": "Обязательное поле"})
        return data

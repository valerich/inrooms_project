from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import StoreFile


class StoreFileSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = StoreFile
        fields = [
            'id',
            'filename',
            'file',
        ]

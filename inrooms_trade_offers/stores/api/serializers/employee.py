from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ..serializers.store import StoreSerializer
from ...models import Employee


class EmployeeSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    store_detail = serializers.SerializerMethodField()

    class Meta:
        model = Employee

        fields = [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'email',
            'phone',
            'store',
            'store_detail',
            'user',
            'date_birthday',
            'date_employment',
            'is_working',
            'position',
            'description',
        ]

    def get_store_detail(self, obj):
        return StoreSerializer(obj.store, fields=[
            'id',
            'name',
            'client_detail',
        ]).data

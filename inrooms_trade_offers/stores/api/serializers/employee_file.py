from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import EmployeeFile


class EmployeeFileSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = EmployeeFile
        fields = [
            'id',
            'filename',
            'file',
        ]

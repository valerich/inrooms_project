import os
import uuid

from django.db import models
from django_resized.forms import ResizedImageField
from model_utils.models import TimeStampedModel


def generate_store_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'store/{}{}'.format(uuid.uuid4(), ext)


class StoreFile(TimeStampedModel):
    filename = models.CharField('Название файла', max_length=1000, blank=True)
    file = models.FileField('Файл', upload_to=generate_store_filename, max_length=755, blank=True)

    class Meta:
        verbose_name = 'Файл магазина'
        verbose_name_plural = 'Файлы магазинов'
        ordering = ('-id', )

    def get_file_name(self):
        file = self.file.name.split('/')[-1]
        return file[:file.rfind('.')]


class Store(models.Model):
    is_active = models.BooleanField('Активен?', default=True)
    client = models.ForeignKey('clients.Client', verbose_name='Партнер', on_delete=models.PROTECT)
    name = models.CharField('Название', max_length=255)
    address = models.CharField('Адрес', max_length=1000, blank=True)
    address_description = models.TextField('Расположение', blank=True)
    phone = models.CharField('Телефон', max_length=50, blank=True)
    opening = models.CharField('Часы работы', max_length=1000, blank=True)
    total_area = models.PositiveSmallIntegerField('Общая площадь', default=0)
    trade_area = models.PositiveSmallIntegerField('Торговая площадь', default=0)
    employees_num = models.PositiveSmallIntegerField('Количество сотрудников', default=0)
    it_info = models.TextField('Информация для установки и подключения IT оборудования', blank=True)
    competitors = models.TextField('Конкуренты', blank=True)
    products = models.TextField('Товар', blank=True)
    target_audience = models.TextField('Целевая аудитория', blank=True)

    plan_avg_cheque = models.PositiveSmallIntegerField('Средний чек по плану', default=0)
    plan_conversion = models.PositiveSmallIntegerField('Плановая конверсия', default=0)

    emporium_url = models.URLField('Url торгового центра', blank=True)
    emporium_contacts = models.TextField('Контакты торгового центра', blank=True)

    files = models.ManyToManyField(StoreFile, verbose_name='Файлы', blank=True)

    class Meta:
        verbose_name = 'Магазин'
        verbose_name_plural = 'Магазины'
        ordering = ('id', )

    def __str__(self):
        return self.name


def generate_employee_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'employee/{}{}'.format(uuid.uuid4(), ext)


class EmployeeFile(TimeStampedModel):
    filename = models.CharField('Название файла', max_length=1000, blank=True)
    file = models.FileField('Файл', upload_to=generate_employee_filename, max_length=755, blank=True)

    class Meta:
        verbose_name = 'Файл сотрудника'
        verbose_name_plural = 'Файлы сотрудников'
        ordering = ('-id', )

    def get_file_name(self):
        file = self.file.name.split('/')[-1]
        return file[:file.rfind('.')]


class Employee(models.Model):
    user = models.OneToOneField('accounts.User', verbose_name='Учетная запись', on_delete=models.PROTECT,
                                blank=True, null=True)
    store = models.ForeignKey(Store, verbose_name='Магазин', on_delete=models.PROTECT)

    first_name = models.CharField('Имя', max_length=100)
    middle_name = models.CharField('Отчество', max_length=100, blank=True)
    last_name = models.CharField('Фамилия', max_length=100, blank=True)
    phone = models.CharField('Телефон', max_length=100, blank=True)
    email = models.EmailField('email', blank=True)
    date_birthday = models.DateField('Дата рождения', blank=True, null=True)
    date_employment = models.DateField('Дата трудоустройства', blank=True, null=True)

    description = models.TextField('Описание', blank=True)

    is_working = models.BooleanField('Работает?', default=True)
    position = models.CharField('Должность', max_length=250, blank=True)

    files = models.ManyToManyField(EmployeeFile, verbose_name='Файлы', blank=True)

    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'
        ordering = ('-first_name', '-middle_name', '-last_name', )

    def get_full_name(self):
        data = []
        for attr in [
            'last_name',
            'first_name',
            'middle_name',
        ]:
            i = getattr(self, attr)
            if i:
                data.append(i)
        return ' '.join(data)

    def get_short_name(self):
        name = ''
        if self.last_name:
            name = self.last_name
            if self.middle_name:
                name += ' {}.{}.'.format(self.first_name[0], self.middle_name[0])
            else:
                name += ' {}.'.format(self.first_name[0])
        else:
            name = self.first_name
        return name

    @property
    def main_im(self):
        try:
            return self.images.all()[0].image
        except IndexError:
            return None


def generate_employee_image_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'employee/fullsize/{}{}'.format(uuid.uuid4(), ext)


class EmployeeImage(TimeStampedModel):
    image = ResizedImageField('Изображение', upload_to=generate_employee_image_filename, max_length=755, blank=True)
    employee = models.ForeignKey(Employee, related_name='images', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Изображение сотрудника'
        verbose_name_plural = 'Изображения сотрудников'
        ordering = ['-id', ]

    def __str__(self):
        return 'id: {} - employee_id: {}'.format(self.pk, self.employee_id)


class EmployeeWorkScheduleItem(models.Model):
    date = models.DateField('Дата')
    store = models.ForeignKey(Store, verbose_name='Магазин')
    employee = models.ForeignKey(Employee, verbose_name='Сотрудник')
    hours = models.PositiveSmallIntegerField('Часы')

    class Meta:
        verbose_name = 'План работы на день'
        verbose_name_plural = 'Планы работы на день'
        unique_together = ('date', 'store', 'employee')


class EmployeeWorkFactItem(models.Model):
    date = models.DateField('Дата')
    store = models.ForeignKey(Store, verbose_name='Магазин')
    employee = models.ForeignKey(Employee, verbose_name='Сотрудник')
    hours = models.PositiveSmallIntegerField('Часы')

    class Meta:
        verbose_name = 'Факт работы за день'
        verbose_name_plural = 'Факты работ за дни'
        unique_together = ('date', 'store', 'employee')

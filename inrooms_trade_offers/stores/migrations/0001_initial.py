# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-07-17 12:58
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import django_resized.forms
import model_utils.fields
import stores.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('clients', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=100, verbose_name='Имя')),
                ('middle_name', models.CharField(blank=True, max_length=100, verbose_name='Отчество')),
                ('last_name', models.CharField(blank=True, max_length=100, verbose_name='Фамилия')),
                ('phone', models.CharField(blank=True, max_length=100, verbose_name='Телефон')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email')),
                ('date_birthday', models.DateField(blank=True, null=True, verbose_name='Дата рождения')),
                ('date_employment', models.DateField(blank=True, null=True, verbose_name='Дата трудоустройства')),
                ('description', models.TextField(blank=True, verbose_name='Описание')),
                ('is_working', models.BooleanField(default=True, verbose_name='Работает?')),
                ('position', models.CharField(blank=True, max_length=250, verbose_name='Должность')),
            ],
            options={
                'verbose_name': 'Сотрудник',
                'verbose_name_plural': 'Сотрудники',
                'ordering': ('-first_name', '-middle_name', '-last_name'),
            },
        ),
        migrations.CreateModel(
            name='EmployeeFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('filename', models.CharField(blank=True, max_length=1000, verbose_name='Название файла')),
                ('file', models.FileField(blank=True, max_length=755, upload_to=stores.models.generate_employee_filename, verbose_name='Файл')),
            ],
            options={
                'verbose_name': 'Файл сотрудника',
                'verbose_name_plural': 'Файлы сотрудников',
                'ordering': ('-id',),
            },
        ),
        migrations.CreateModel(
            name='EmployeeImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('image', django_resized.forms.ResizedImageField(blank=True, max_length=755, upload_to=stores.models.generate_employee_image_filename, verbose_name='Изображение')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='stores.Employee')),
            ],
            options={
                'verbose_name': 'Изображение сотрудника',
                'verbose_name_plural': 'Изображения сотрудников',
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='EmployeeWorkFactItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Дата')),
                ('hours', models.PositiveSmallIntegerField(verbose_name='Часы')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='stores.Employee', verbose_name='Сотрудник')),
            ],
            options={
                'verbose_name': 'Факт работы за день',
                'verbose_name_plural': 'Факты работ за дни',
            },
        ),
        migrations.CreateModel(
            name='EmployeeWorkScheduleItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Дата')),
                ('hours', models.PositiveSmallIntegerField(verbose_name='Часы')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='stores.Employee', verbose_name='Сотрудник')),
            ],
            options={
                'verbose_name': 'План работы на день',
                'verbose_name_plural': 'Планы работы на день',
            },
        ),
        migrations.CreateModel(
            name='Store',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_active', models.BooleanField(default=True, verbose_name='Активен?')),
                ('name', models.CharField(max_length=255, verbose_name='Название')),
                ('address', models.CharField(blank=True, max_length=1000, verbose_name='Адрес')),
                ('address_description', models.TextField(blank=True, verbose_name='Расположение')),
                ('phone', models.CharField(blank=True, max_length=50, verbose_name='Телефон')),
                ('opening', models.CharField(blank=True, max_length=1000, verbose_name='Часы работы')),
                ('total_area', models.PositiveSmallIntegerField(default=0, verbose_name='Общая площадь')),
                ('trade_area', models.PositiveSmallIntegerField(default=0, verbose_name='Торговая площадь')),
                ('employees_num', models.PositiveSmallIntegerField(default=0, verbose_name='Количество сотрудников')),
                ('it_info', models.TextField(blank=True, verbose_name='Информация для установки и подключения IT оборудования')),
                ('competitors', models.TextField(blank=True, verbose_name='Конкуренты')),
                ('products', models.TextField(blank=True, verbose_name='Товар')),
                ('target_audience', models.TextField(blank=True, verbose_name='Целевая аудитория')),
                ('plan_avg_cheque', models.PositiveSmallIntegerField(default=0, verbose_name='Средний чек по плану')),
                ('plan_conversion', models.PositiveSmallIntegerField(default=0, verbose_name='Плановая конверсия')),
                ('emporium_url', models.URLField(blank=True, verbose_name='Url торгового центра')),
                ('emporium_contacts', models.TextField(blank=True, verbose_name='Контакты торгового центра')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='clients.Client', verbose_name='Партнер')),
            ],
            options={
                'verbose_name': 'Магазин',
                'verbose_name_plural': 'Магазины',
                'ordering': ('id',),
            },
        ),
        migrations.CreateModel(
            name='StoreFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('filename', models.CharField(blank=True, max_length=1000, verbose_name='Название файла')),
                ('file', models.FileField(blank=True, max_length=755, upload_to=stores.models.generate_store_filename, verbose_name='Файл')),
            ],
            options={
                'verbose_name': 'Файл магазина',
                'verbose_name_plural': 'Файлы магазинов',
                'ordering': ('-id',),
            },
        ),
        migrations.AddField(
            model_name='store',
            name='files',
            field=models.ManyToManyField(blank=True, to='stores.StoreFile', verbose_name='Файлы'),
        ),
        migrations.AddField(
            model_name='employeeworkscheduleitem',
            name='store',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='stores.Store', verbose_name='Магазин'),
        ),
        migrations.AddField(
            model_name='employeeworkfactitem',
            name='store',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='stores.Store', verbose_name='Магазин'),
        ),
        migrations.AddField(
            model_name='employee',
            name='files',
            field=models.ManyToManyField(blank=True, to='stores.EmployeeFile', verbose_name='Файлы'),
        ),
        migrations.AddField(
            model_name='employee',
            name='store',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='stores.Store', verbose_name='Магазин'),
        ),
        migrations.AddField(
            model_name='employee',
            name='user',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Учетная запись'),
        ),
        migrations.AlterUniqueTogether(
            name='employeeworkscheduleitem',
            unique_together=set([('date', 'store', 'employee')]),
        ),
        migrations.AlterUniqueTogether(
            name='employeeworkfactitem',
            unique_together=set([('date', 'store', 'employee')]),
        ),
    ]

from django.contrib import admin

from config.celery import app
from .forms import StoreAdminForm
from .models import Employee, Store


class EmployeeAdmin(admin.ModelAdmin):
    search_fields = (
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'phone',
    )
    list_display = (
        'first_name',
        'middle_name',
        'last_name',
        'store',
        'user',
        'email',
        'phone',
    )

    actions = ('send_to_1c_action',)

    def send_to_1c_action(self, request, queryset):
        product_ids = list(queryset.values_list('id', flat=True))
        app.send_task('service_1c.notify_1c_employee_change', args=(product_ids,))
        self.message_user(request, 'Отправлено на выгрузку в 1с')

    send_to_1c_action.short_description = 'Выгрузить в 1с'


class StoreAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'client')
    list_filter = ('client', )

    form = StoreAdminForm

    actions = ('send_to_1c_action',)

    def send_to_1c_action(self, request, queryset):
        product_ids = list(queryset.values_list('id', flat=True))
        app.send_task('service_1c.notify_1c_store_change', args=(product_ids,))
        self.message_user(request, 'Отправлено на выгрузку в 1с')

    send_to_1c_action.short_description = 'Выгрузить в 1с'


for model_or_iterable, admin_class in (
    (Employee, EmployeeAdmin),
    (Store, StoreAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

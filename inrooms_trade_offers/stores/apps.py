from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'stores'
    verbose_name = 'Магазины'

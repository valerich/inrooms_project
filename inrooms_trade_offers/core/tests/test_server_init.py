from django.contrib.auth.models import Group, Permission
from django.test import TestCase

from core.server_init import ServerInit
from offers.models import OfferStatus
from orders.models import OrderStatus
from products.models import SERIES_TYPE_CHOICES, Size
from purchases.models import PurchaseStatus
from supply.models import SupplyStatus
from warehouses.models import Warehouse


class ServerInitTestCase(TestCase):

    def setUp(self):
        self.si = ServerInit()

    def test_create_groups(self):
        self.assertEqual(Group.objects.count(), 0)
        self.si.create_groups()
        self.assertEqual(Group.objects.count(), 9)

    def test_permissions_list(self):
        self.si.create_groups()
        self.si.set_permissions()
        perms = set([perm[0] for perm in self.si.permissions])
        self.assertIn('view_all_client_statistic', perms)       # Может видеть статистику по всем клиентам
        self.assertIn('view_self_client_statistic', perms)      # Может видеть статистику клиента для себя
        self.assertIn('create_all_client_statistic', perms)     # Может создавать статистику для любого клиента
        self.assertIn('create_self_client_statistic', perms)    # Может создавать статистику для себя
        self.assertIn('can_set_all_passwords', perms)           # Может менять пароли пользователей
        self.assertIn('can_offers_blank_worker_create', perms)  # Может создавать заказы с пустым исполнителем Issue #31
        self.assertIn('can_get_offers_blank_worker', perms)     # Может забирать себе предложения с не заполненным исполнителем Issue #31
        self.assertIn('can_save_offers_blank_worker', perms)    # Может сохранять предложения с не заполненным исполнителем Issue #31
        self.assertIn('can_view_comments_widget', perms)        # Может видеть виджет "комментарии"
        self.assertIn('can_view_currency_widget', perms)        # Может видеть виджет "курсы валют"
        self.assertIn('can_view_offers_widget', perms)          # Может видеть виджет "предложения"

    def test_create_permissions(self):
        self.si.create_groups()
        self.si.set_permissions()
        self.si.create_permissions()
        for perm in self.si.permissions:
            self.assertEquals(Permission.objects.filter(content_type=self.si.content_type, codename=perm[0]).count(), 1)

    def test_create_supply_statuses(self):
        self.si.create_groups()
        self.si.create_supply_statuses()
        statuses = [
            'new',
            'sent_to_warehouse',
            'delivery_waiting',
            'delivery_accepted',
            'set_prices',
        ]
        self.assertEqual(SupplyStatus.objects.count(), len(statuses))
        for status in statuses:
            self.assertEqual(SupplyStatus.objects.filter(code=status).exists(),
                             True,
                             'SupplyStatus "{}" does not exist'.format(status))

    def test_create_purchase_statuses(self):
        self.si.create_groups()
        self.si.create_purchase_statuses()
        statuses = [
            'new',
            'sent_to_warehouse',
            'delivery_waiting',
            'delivery_accepted',
        ]
        self.assertEqual(PurchaseStatus.objects.count(), len(statuses))
        for status in statuses:
            self.assertEqual(PurchaseStatus.objects.filter(code=status).exists(),
                             True,
                             'PurchaseStatus "{}" does not exist'.format(status))

    def test_create_order_statuses(self):
        self.si.create_groups()
        self.si.create_order_statuses()
        statuses = [
            'new',
            'reserved',
            'client_agreement',
            'client_agreed',
            'client_not_agreed',
            'formed',
            'shipment_preparing',
            'ready_to_send',
            'shipped',
            'received',
        ]
        self.assertEqual(OrderStatus.objects.count(), len(statuses))
        for status in statuses:
            self.assertEqual(OrderStatus.objects.filter(code=status).exists(),
                             True,
                             'OrderStatus "{}" does not exist'.format(status))

    def test_create_offer_statuses(self):
        self.si.create_groups()
        self.si.create_offer_statuses()
        statuses = ['approved',
                    'sample_correction',
                    'sample_returned',
                    'sample_returned_fail',
                    'return_sample',
                    'sample_fail',
                    'sample_ready',
                    'sample_ordered',
                    'sample_ordered_cor',
                    'order_sample',
                    'order_sample_cor',
                    'approving',
                    'correction',
                    'archive',
                    'draft',
                    'models_created', ]
        self.assertEqual(OfferStatus.objects.count(), len(statuses))
        for status in statuses:
            self.assertEqual(OfferStatus.objects.filter(code=status).exists(),
                             True,
                             'OfferStatus "{}" does not exist'.format(status))

    def test_size_create(self):
        self.si.create_product_sizes()
        for series_type, size_codes in (
                (SERIES_TYPE_CHOICES.shoes, ('34',
                                             '35',
                                             '36',
                                             '37',
                                             '38',
                                             '39',
                                             '40',
                                             '41', )),
                (SERIES_TYPE_CHOICES.clothes, ('xxs',
                                               'xs',
                                               's',
                                               'm',
                                               'l',
                                               'xl',
                                               'xxl',
                                               'xxxl', ))):
            for size_code in size_codes:
                size = Size.objects.get(code=size_code)
                self.assertEqual(size.series_type,
                                 series_type)

    def test_warehouse_create(self):
        Warehouse.objects.all().delete()
        self.si.create_warehouses()
        self.assertEqual(Warehouse.objects.count(), 3)
        self.assertEqual(Warehouse.objects.filter(code__in=['showroom', 'msk', 'chn']).count(), 3)

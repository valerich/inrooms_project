from django.conf import settings


def base(request):

    return {
        'DEBUG': settings.DEBUG,
        'JS_DSN': settings.JS_DSN,
    }

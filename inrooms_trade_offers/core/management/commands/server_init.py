from django.core.management.base import BaseCommand

from core.server_init import ServerInit


class Command(BaseCommand):
    help = 'Создание групп, прав, и т.д. Всё что нужно для начала работы системы'

    def handle(self, *args, **options):
        si = ServerInit()
        si.create_all_data()

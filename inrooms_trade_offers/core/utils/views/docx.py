from io import BytesIO

from django.http.response import HttpResponse
from docx import Document


class ExportDocxViewMixin(object):
    filename_docx = 'file.docx'

    def dispatch(self, request, *args, **kwargs):
        file_format = request.GET.get('as', None)
        if file_format == 'docx':
            return self.get_docx_response()
        else:
            return super(ExportDocxViewMixin, self).dispatch(request, *args, **kwargs)

    def get_docx_response(self):
        document = Document()
        document = self.write_docx_document(document)
        f = BytesIO()
        document.save(f)
        length = f.tell()
        f.seek(0)
        response = HttpResponse(
            f.getvalue(),
            content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        )
        response['Content-Disposition'] = 'attachment; filename=' + self.filename_docx
        response['Content-Length'] = length
        return response

    def write_docx_document(self, document):
        return document

import os
import tempfile

from django.http.response import HttpResponse

from core.utils.excel import NewExcelHelper


class ExportClassMixin(object):

    def __init__(self, data):
        self.data = data

    def get_data(self):
        return self.get_xls_helper()

    def get_xls_helper(self):
        xls_helper = NewExcelHelper()
        return xls_helper


class ExportXlsxViewMixin():
    filename_xlsx = 'export.xlsx'
    export_class = ExportClassMixin

    def list(self, request, *args, **kwargs):

        if request.GET.get('as', None) == 'xls':
            queryset = self.filter_queryset(self.get_queryset())

            serializer = self.get_serializer(queryset, many=True)
            xls_helper = self.export_class(data=serializer.data).get_data()

            fd, fn = tempfile.mkstemp()
            os.close(fd)
            xls_helper.book.save(fn)
            fh = open(fn, 'rb')
            resp = fh.read()
            fh.close()
            response = HttpResponse(resp, content_type='application/ms-excel')
            response['Content-Disposition'] = 'attachment; filename={}'.format(self.filename_xlsx)
            return response

        return super().list(request, *args, **kwargs)

from rest_framework.response import Response

from core.utils.paginator import MyAdditionalDataApiPagination


class AdditionalDataPaginatedViewMixin():
    pagination_class = MyAdditionalDataApiPagination

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            additional_data = self.get_additional_data(queryset)
            return self.get_paginated_response(serializer.data, additional_data=additional_data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_paginated_response(self, data, additional_data=None):
        return self.paginator.get_paginated_response(data, additional_data)

    def get_additional_data(self, queryset):
        return {}

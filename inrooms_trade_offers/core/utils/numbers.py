def format_number(number, num_decimals=2):
    integer_part, _, decimal_part = str(float(number)).partition('.')
    reversed_digits = ''.join(reversed(integer_part))
    parts = []
    while reversed_digits:
        parts.append(reversed_digits[:3])
        reversed_digits = reversed_digits[3:]
    formatted_number = ''.join(reversed(' '.join(parts)))
    decimals_to_add = decimal_part[:num_decimals].rstrip('0')
    if decimals_to_add:
        formatted_number += '.' + decimals_to_add
    return formatted_number
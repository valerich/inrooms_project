def group(lst, n):
    return [lst[i:i + n] for i in range(0, len(lst), n)]


def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i+n]


def extended_grouping(lst, n, first_n=None):
    data = []
    if first_n:
        data.append(lst[:first_n])
        lst = lst[first_n:]

    for i in range(0, len(lst), n):
        data.append(lst[i:i+n])
    return data


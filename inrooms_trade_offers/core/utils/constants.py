from model_utils.choices import Choices

TRUE_VALUES = {'Да', 'да', 'ДА', 'true', 'True', 'TRUE'}
FALSE_VALUES = {'Нет', 'нет', 'НЕТ', 'false', 'False', 'FALSE'}

ORDER_CATEGORY_CHOICE = Choices(
    (1, 'white', 'Первая'),
    (2, 'black', 'Вторая'),
)


DEPARTMENT_CHOICE = Choices(
    (1, 'development', 'Развития'),
    (2, 'escort', 'Сопровождения'),
)


INVOICE_KIND_CHOICES = Choices(
    (1, 'default', 'Товар'),
    (2, 'marketing', 'Маркетинговый'),
    (3, 'travel_expenses', 'Командировочные расходы'),
    (4, 'other_expenses', 'Прочие расходы'),
)
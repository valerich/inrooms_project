import datetime

from dateutil.relativedelta import relativedelta


def date_range(start_date: datetime.date, end_date: datetime.date) -> list:
    """Список дат за промежуток start_date-end_date

    :param start_date: Начало отрезка времени
    :param end_date: Окончание отрезка времени
    """

    end_date = end_date + relativedelta(days=+1)
    return [start_date + relativedelta(days=+n) for n in range(int((end_date - start_date).days))]

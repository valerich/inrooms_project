from openpyxl.styles.alignment import Alignment
from openpyxl.utils import get_column_letter
from openpyxl.workbook import Workbook


class NewExcelHelper(object):

    def __init__(self):
        self.book = Workbook()
        self.page = None

    def add_page(self, title):
        if self.page is None:
            self.page = 0
            self.sheet = self.book.active
        else:
            self.sheet = self.book.create_sheet()
        self.sheet.title = title
        self.page += 1

    def write_cell(self, value, row, col, wrap_text=False):
        if not isinstance(col, str):
            col = get_column_letter(col + 1)
        index = '%s%s' % (col, row + 1)
        self.sheet[index] = value
        if wrap_text:
            self.sheet[index].alignment = Alignment(wrapText=True)

    def merge_cells(self, start_row, start_column, end_row, end_column):
        self.sheet.merge_cells(
            start_row=start_row + 1,
            start_column=start_column + 1,
            end_row=end_row + 1,
            end_column=end_column + 1
        )

import datetime

from django.test import TestCase

from ..date import date_range


class DateUtilsTestCase(TestCase):

    def test_daterange(self):
        from_date = datetime.date(2015, 12, 28)
        to_date = datetime.date(2016, 1, 26)
        dates = date_range(from_date, to_date)

        self.assertEqual(len(dates), 30)
        self.assertEqual(dates[0], from_date)
        self.assertEqual(dates[-1], to_date)
        self.assertEqual(dates[10], datetime.date(2016, 1, 7))

    def test_daterange_inverse_dates(self):
        """Когда начало интервала больше, чем окончание.

        Возвращаем пустой массив
        """

        from_date = datetime.date(2015, 12, 28)
        to_date = datetime.date(2016, 1, 26)
        self.assertEqual(len(date_range(to_date, from_date)), 0)

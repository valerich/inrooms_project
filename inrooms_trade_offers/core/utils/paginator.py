from collections import OrderedDict

from django.core.paginator import Page, Paginator

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class MyPaginator(Paginator):

    def __init__(self, object_list, per_page, orphans=0, allow_empty_first_page=True, locality=2):
        Paginator.__init__(self, object_list, per_page, orphans=0, allow_empty_first_page=True)
        self.locality = locality

    def page(self, number):
        "Returns a Page object for the given 1-based page number."
        number = self.validate_number(number)
        bottom = (number - 1) * self.per_page
        top = bottom + self.per_page
        if top + self.orphans >= self.count:
            top = self.count
        return MyPage(self.object_list[bottom:top], number, self)


class MyPage(Page):
    def __init__(self, object_list, number, paginator):
        Page.__init__(self, object_list, number, paginator)
    def previous_page_list(self):
        return [page for page in range(self.number - self.paginator.locality, self.number) if page > 0]
    def next_page_list(self):
        return [page for page in range(self.number + 1, self.number + self.paginator.locality + 1) if page <= self.paginator.num_pages]
    def show_first_page(self):
        return self.number > self.paginator.locality + 1
    def show_last_page(self):
        if self.paginator.num_pages:
            if self.number < self.paginator.num_pages - self.paginator.locality:
                return True
        return False


class MyApiPagination(PageNumberPagination):
    page_size_query_param = 'page_size'
    page_size = 10


class MyAdditionalDataApiPagination(MyApiPagination):

    def get_paginated_response(self, data, additional_data=None):
        r_data = [
            ('count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]
        for key, value in additional_data.items():
            r_data.append((key, value))

        return Response(OrderedDict(r_data))


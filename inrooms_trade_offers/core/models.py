import os
import uuid

from django.db import models
from django_resized.forms import ResizedImageField

from model_utils.choices import Choices


class NotDeleteManager(models.Manager):
    def get_queryset(self):
        return super(NotDeleteManager, self).get_queryset().filter(is_deleted=False)


class DeleteManager(models.Manager):
    def get_queryset(self):
        return super(DeleteManager, self).get_queryset().filter(is_deleted=True)


class DeleteMixin(models.Model):
    is_deleted = models.BooleanField(default=False)

    objects = NotDeleteManager()
    all_objects = models.Manager()
    deleted_objects = DeleteManager()

    class Meta:
        abstract = True

    def delete(self):
        self.is_deleted = True
        self.save()


CURRENCY_CHOICES = Choices(
    ('RUR', 'Рубли'),
    ('CNY', 'Юани'),
    ('USD', 'Доллары'),
    ('EUR', 'Евро'),
    ('KZT', 'Тенге'),
    ('KGS', 'Сомы'),
)

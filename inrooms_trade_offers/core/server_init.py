from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType

from accounts.models import User
from payments.models import ExpenseItem
from warehouses.models import Warehouse


class ServerInit(object):
    def __init__(self):
        self.content_type = ContentType.objects.get(model='permission', app_label='auth')
        self.permissions = []

    def create_all_data(self):
        self.create_groups()
        self.create_users()
        self.set_permissions()
        self.create_expense_items()
        self.create_offer_statuses()
        self.create_supply_statuses()
        self.create_product_sizes()
        self.create_purchase_statuses()
        self.create_order_statuses()
        self.create_defect_act_statuses()
        self.create_refund_act_statuses()
        self.create_manufacturing_order_statuses()
        self.create_manufacturing_internal_label_order_statuses()
        self.create_manufacturing_order_label_statuses()
        self.create_warehouses()
        self.clear_permissions()
        self.create_permissions()
        self.create_message_templates()

    def create_message_templates(self):
        from sms.models import MessageTemplate
        MessageTemplate.objects.create(
            title='Отправка номера карты клиенту'
        )

    def create_groups(self):
        self.group_admins = Group.objects.get_or_create(name=u'Администраторы')[0]
        self.group_brand_manager = Group.objects.get_or_create(name='Бренд менеджер')[0]
        self.group_process_administrator = Group.objects.get_or_create(name='Администратор процесса')[0]
        self.group_process_administrator_liza = Group.objects.get_or_create(name='Администратор процесса (Лиза)')[0]
        self.group_moskow_manager = Group.objects.get_or_create(name='Менеджер Москва')[0]
        self.group_moskow_manager_shipment = Group.objects.get_or_create(name='Менеджер Москва (отгрузка)')[0]
        self.group_client = Group.objects.get_or_create(name='Партнеры')[0]
        self.group_seller = Group.objects.get_or_create(name='Продавцы')[0]
        self.group_merchandiser = Group.objects.get_or_create(name='Мерчендайзер')[0]
        self.group_handbook_access = Group.objects.get_or_create(name='Доступ к справочнику')[0]
        self.group_contract_sale = Group.objects.get_or_create(name='Продажа контракта')[0]

    def create_users(self):
        self.user_service = User.objects.get_or_create(
            email='service@chn.inrooms.club',
            defaults={
                'description': 'Системный пользователь, используется в тех местах, '
                               'где необходимо создавать в автоматическом режиме документы',
                'first_name': 'Системный пользователь',
                'is_active': False,
            }
        )

    def create_expense_items(self):
        ExpenseItem.objects.get_or_create(
            code='amount_sawing_labels_item',
            defaults={
                'name': 'Перешив этикеток',
            }
        )

    def create_warehouses(self):
        for data in [
            {
                'name': 'Шоурум',
                'code': 'showroom',
                'to_orders': True,
            },
            {
                'name': 'Склад Москва',
                'code': 'msk',
                'to_orders': True,
            },
            {
                'name': 'Склад Китай',
                'code': 'chn',
                'to_orders': False,
            }
        ]:
            warehouse, c = Warehouse.objects.get_or_create(
                code=data['code'],
                defaults={
                    'name': data['name'],
                    'to_orders': data['to_orders'],
                }
            )

    def clear_permissions(self):
        for i in dir(self):
            if i.startswith('group_'):
                getattr(self, i).permissions = Permission.objects.none()

    def create_permissions(self):
        for permission in self.permissions:
            try:
                ct = permission[2]
            except IndexError:
                ct = self.content_type
            perm = Permission.objects.get_or_create(
                codename=permission[0],
                content_type=ct,
                defaults={
                    "name": permission[1]
                }
            )[0]
            try:
                for group in permission[3]:
                    group.permissions.add(perm)
            except IndexError:
                pass

    def create_offer_statuses(self):
        offer_statuses = {
            'draft': {
                'name': 'Новый',
                'icon': 'fa-file-o',
                'color': 'aero',
                'weight': 100,
                'next': ['rejected', 'approving', 'archive'],
                'groups_view': [self.group_admins, self.group_brand_manager],
                'groups_set': [self.group_brand_manager, self.group_process_administrator, self.group_process_administrator_liza]
            },
            'correction': {
                'name': 'Доработка',
                'icon': 'fa-pencil-square-o',
                'color': 'orange',
                'weight': 300,
                'next': ['approving', ],
                'groups_view': [self.group_admins, self.group_brand_manager, ],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza]
            },
            'approving': {
                'name': 'Утверждение',
                'icon': 'fa-gavel',
                'color': 'blue',
                'weight': 400,
                'next': ['correction', 'order_sample', 'archive', 'approved'],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza],
                'groups_set': [self.group_admins, self.group_brand_manager, self.group_process_administrator, self.group_process_administrator_liza]
            },
            'order_sample': {
                'name': 'Заказать образец',
                'icon': 'fa-shopping-cart',
                'color': 'aero',
                'weight': 500,
                'next': ['sample_fail', 'sample_ordered', 'order_sample_cor', 'archive'],
                'groups_view': [self.group_admins, self.group_brand_manager, ],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza]
            },
            'order_sample_cor': {
                'name': 'Уточнение перед заказом образца',
                'icon': 'fa-shopping-cart',
                'color': 'aero',
                'weight': 550,
                'next': ['archive', 'order_sample', ],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza],
                'groups_set': [self.group_admins, self.group_brand_manager, ]
            },
            'sample_ordered': {
                'name': 'Образец заказан',
                'icon': 'fa-shopping-cart',
                'color': 'orange',
                'weight': 600,
                'next': ['archive', 'sample_ready', 'sample_ordered_cor', 'approved'],
                'groups_view': [self.group_admins, self.group_brand_manager, ],
                'groups_set': [self.group_admins, self.group_brand_manager, ]
            },
            'sample_ordered_cor': {
                'name': 'Уточнение по заказанному образцу',
                'icon': 'fa-shopping-cart',
                'color': 'aero',
                'weight': 650,
                'next': ['sample_ordered', ],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza],
                'groups_set': [self.group_admins, self.group_brand_manager, ]
            },
            'sample_ready': {
                'name': 'Образец получен',
                'icon': 'fa-shopping-cart',
                'color': 'green',
                'weight': 700,
                'next': ['archive', 'return_sample', 'sample_correction', 'approved'],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza],
                'groups_set': [self.group_admins, self.group_brand_manager, ]
            },
            'sample_fail': {
                'name': 'Образец не может быть заказан',
                'icon': 'fa-shopping-cart',
                'color': 'red',
                'weight': 160,
                'next': ['archive', ],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza],
                'groups_set': [self.group_admins, self.group_brand_manager, ]
            },
            'return_sample': {
                'name': 'Вернуть образец',
                'icon': 'fa-cart-arrow-down',
                'color': 'red',
                'weight': 900,
                'next': ['sample_returned', 'sample_returned_fail'],
                'groups_view': [self.group_admins, self.group_brand_manager, ],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza]
            },
            'sample_returned': {
                'name': 'Образец возвращен',
                'icon': 'fa-cart-arrow-down',
                'color': 'blue-dark',
                'weight': 1000,
                'next': ['archive', ],
                'groups_view': [self.group_admins, self.group_brand_manager, self.group_process_administrator, self.group_process_administrator_liza],
                'groups_set': [self.group_admins, self.group_brand_manager, ]
            },
            'sample_returned_fail': {
                'name': 'Образец не может быть возвращен',
                'icon': 'fa-hand-o-up',
                'color': 'red',
                'weight': 1100,
                'next': ['archive', ],
                'groups_view': [self.group_admins, self.group_brand_manager, self.group_process_administrator, self.group_process_administrator_liza],
                'groups_set': [self.group_admins, self.group_brand_manager, ]
            },
            'sample_correction': {
                'name': 'Уточнения по образцу',
                'icon': 'fa-question-circle',
                'color': 'orange',
                'weight': 1100,
                'next': ['sample_ready', ],
                'groups_view': [self.group_admins, self.group_brand_manager, ],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza]
            },
            'archive': {
                'name': 'Архив',
                'icon': 'fa-archive',
                'color': 'purple',
                'weight': 150,
                'next': ['draft', 'approving'],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, ]
            },
            'approved': {
                'name': 'Создать заказ',
                'icon': 'fa-thumbs-o-up',
                'color': 'green',
                'weight': 2000,
                'next': ['models_created', 'return_sample', 'archive'],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, ],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, ]
            },
            'models_created': {
                'name': 'Модели созданы',
                'icon': 'fa-check-square',
                'color': 'green',
                'weight': 3000,
                'next': [],
                'groups_view': [self.group_admins, ],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, ]
            }
        }
        from offers.models import OfferStatus
        for status_code, status_data in offer_statuses.items():
            offer_status = OfferStatus.objects.get_or_create(
                code=status_code,
                defaults={
                    'weight': status_data['weight'],
                    'name': status_data['name'],
                    'icon': status_data['icon'],
                    'color': status_data['color'],
                }
            )[0]
            self.permissions.append((
                'can_offers_edit_{}'.format(status_code),
                'Может редактировать предложение в статусе: {}'.format(status_data['name']),
                self.content_type,
                status_data['groups_view'],
            ))
            self.permissions.append((
                'can_offers_set_{}'.format(status_code),
                'Может выставлять предложению статус: {}'.format(status_data['name']),
                self.content_type,
                status_data['groups_set'],
            ))

        for status_code, status_data in offer_statuses.items():
            ps = OfferStatus.objects.get(code=status_code)
            next_statuses = OfferStatus.objects.filter(code__in=status_data['next'])
            ps.next_status.clear()
            ps.next_status = next_statuses

    def create_product_sizes(self):
        """Размеров у нас ограниченное количество. Предполагается, что это вечный список."""

        from products.models import SERIES_TYPE_CHOICES, Size
        weight = 0
        for series_type, size_codes in (
                (SERIES_TYPE_CHOICES.shoes, ('34',
                                             '35',
                                             '36',
                                             '37',
                                             '38',
                                             '39',
                                             '40',
                                             '41', )),
                (SERIES_TYPE_CHOICES.clothes, ('xxs',
                                               'xs',
                                               's',
                                               'm',
                                               'l',
                                               'xl',
                                               'xxl',
                                               'xxxl', ))):
            for size_code in size_codes:
                weight += 1
                size, c = Size.objects.get_or_create(code=size_code,
                                                     defaults={
                                                         'name': size_code,
                                                         'code_1c': size_code.replace('xx', '2x'),
                                                         'series_type': series_type,
                                                         'multiplier': 1 if size_code not in ['s', 'm', '37', '38'] else 2,
                                                         'weight': weight, })

    def create_refund_act_statuses(self):
        refund_act_statuses = {
            'new': {
                'name': 'Новый',
                'button_name': 'Новый',
                'weight': 1,
                'next': ['agreement', 'agreement_office'],
                'create_1c_document': False,
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client]
            },
            'agreement_office': {
                'name': 'Согласовано для рассмотрения в офисе',
                'button_name': 'Согласовать для рассмотрения в офисе',
                'weight': 50,
                'create_1c_document': False,
                'next': ['denial', 'agreed', ],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client]
            },
            'agreement': {
                'name': 'Передан на согласование',
                'button_name': 'Передать на согласование',
                'weight': 50,
                'create_1c_document': False,
                'next': ['denial', 'agreed', ],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client]
            },
            'denial': {
                'name': 'Отказ',
                'button_name': 'Отказ',
                'weight': 60,
                'create_1c_document': False,
                'next': [],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, ],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, ]
            },
            'agreed': {
                'name': 'Согласован',
                'button_name': 'Согласован',
                'weight': 70,
                'create_1c_document': True,
                'next': [],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, ],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, ]
            },
            'formed': {
                'name': 'Сформирован',
                'button_name': 'Сформирован',
                'weight': 80,
                'create_1c_document': False,
                'next': [],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client]
            },
            'sent': {
                'name': 'Отправлен',
                'button_name': 'Отправлен',
                'weight': 100,
                'next': [],
                'create_1c_document': False,
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client]
            },
        }

        from orders.models import RefundActStatus
        for status_code, status_data in refund_act_statuses.items():
            status = RefundActStatus.objects.get_or_create(
                code=status_code,
                defaults={
                    'weight': status_data['weight'],
                    'name': status_data['name'],
                    'button_name': status_data['button_name'],
                    'create_1c_document': status_data['create_1c_document'],
                }
            )[0]

            self.permissions.append((
                'can_refund_act_edit_{}'.format(status_code),
                'Может редактировать возврат в статусе: {}'.format(status_data['name']),
                self.content_type,
                status_data['groups_view'],
            ))
            self.permissions.append((
                'can_refund_act_set_{}'.format(status_code),
                'Может выставлять возврату статус: {}'.format(status_data['name']),
                self.content_type,
                status_data['groups_set'],
            ))

        for status_code, status_data in refund_act_statuses.items():
            ps = RefundActStatus.objects.get(code=status_code)
            next_statuses = RefundActStatus.objects.filter(code__in=status_data['next'])
            ps.next_status.clear()
            ps.next_status = next_statuses

    def create_defect_act_statuses(self):
        defect_act_statuses = {
            'new': {
                'name': 'Новый',
                'button_name': 'Новый',
                'weight': 1,
                'next': ['agreement', ],
                'create_1c_document': False,
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client]
            },
            'agreement': {
                'name': 'Передан на согласование',
                'button_name': 'Передать на согласование',
                'weight': 50,
                'create_1c_document': False,
                'next': ['denial', 'agreed', ],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client]
            },
            'denial': {
                'name': 'Отказ',
                'button_name': 'Отказ',
                'weight': 60,
                'create_1c_document': False,
                'next': [],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, ],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, ]
            },
            'agreed': {
                'name': 'Согласован',
                'button_name': 'Согласован',
                'weight': 70,
                'create_1c_document': True,
                'next': ['refused_to_acceptance', 'consideration_replacing_identical', 'consideration_replacing_simular', 'consideration_repair', 'consideration_cleaning'],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, ],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, ]
            },
            'refused_to_acceptance': {
                'name': 'Отказано к приемке. Товар будет возвращен',
                'button_name': 'Отказано к приемке',
                'weight': 200,
                'next': [],
                'create_1c_document': False,
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ],
            },
            'consideration_replacing_identical': {
                'name': 'Принят к рассмотрению. Замена на идентичный',
                'button_name': 'Замена на идентичный',
                'weight': 201,
                'next': [],
                'create_1c_document': False,
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ]
            },
            'consideration_replacing_simular': {
                'name': 'Принят к рассмотрению. Замена на аналогичный',
                'button_name': 'Замена на аналогичный',
                'weight': 202,
                'next': [],
                'create_1c_document': False,
                'groups_view': [self.group_admins, self.group_process_administrator,
                                self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ],
                'groups_set': [self.group_admins, self.group_process_administrator,
                               self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ]
            },
            'consideration_repair': {
                'name': 'Принят к рассмотрению. Ремонт',
                'button_name': 'Ремонт',
                'weight': 203,
                'next': ['repair_complete', 'consideration_replacing_simular'],
                'create_1c_document': False,
                'groups_view': [self.group_admins, self.group_process_administrator,
                                self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ],
                'groups_set': [self.group_admins, self.group_process_administrator,
                               self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ]
            },
            'repair_complete': {
                'name': 'Ремонт выполнен',
                'button_name': 'Ремонт выполнен',
                'weight': 205,
                'next': [],
                'create_1c_document': False,
                'groups_view': [self.group_admins, self.group_process_administrator,
                                self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ],
                'groups_set': [self.group_admins, self.group_process_administrator,
                               self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ]
            },
            'consideration_cleaning': {
                'name': 'Принят к рассмотрению. Чистка',
                'button_name': 'Чистка',
                'weight': 204,
                'next': ['cleaning_complete', 'consideration_replacing_simular'],
                'create_1c_document': False,
                'groups_view': [self.group_admins, self.group_process_administrator,
                                self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ],
                'groups_set': [self.group_admins, self.group_process_administrator,
                               self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ]
            },
            'cleaning_complete': {
                'name': 'Чистка выполнена',
                'button_name': 'Чистка выполнена',
                'weight': 206,
                'next': [],
                'create_1c_document': False,
                'groups_view': [self.group_admins, self.group_process_administrator,
                                self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ],
                'groups_set': [self.group_admins, self.group_process_administrator,
                               self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, ]
            },
        }

        from orders.models import DefectActStatus
        for status_code, status_data in defect_act_statuses.items():
            status = DefectActStatus.objects.get_or_create(
                code=status_code,
                defaults={
                    'weight': status_data['weight'],
                    'name': status_data['name'],
                    'button_name': status_data['button_name'],
                    'create_1c_document': status_data['create_1c_document'],
                }
            )[0]

            self.permissions.append((
                'can_defect_act_edit_{}'.format(status_code),
                'Может редактировать брак в статусе: {}'.format(status_data['name']),
                self.content_type,
                status_data['groups_view'],
            ))
            self.permissions.append((
                'can_defect_act_set_{}'.format(status_code),
                'Может выставлять браку статус: {}'.format(status_data['name']),
                self.content_type,
                status_data['groups_set'],
            ))

        for status_code, status_data in defect_act_statuses.items():
            ps = DefectActStatus.objects.get(code=status_code)
            next_statuses = DefectActStatus.objects.filter(code__in=status_data['next'])
            ps.next_status.clear()
            ps.next_status = next_statuses

    def create_order_statuses(self):
        order_statuses = {
            'new': {
                'name': 'Новый',
                'button_name': 'Новый',
                'weight': 1,
                'next': ['reserved', ],
                'create_1c_document': False,
                'has_local_reserve': False,
                'groups_view': [self.group_admins,
                                self.group_process_administrator,
                                self.group_process_administrator_liza,
                                self.group_moskow_manager,
                                self.group_moskow_manager_shipment],
                'groups_set': [self.group_admins,
                               self.group_process_administrator,
                               self.group_process_administrator_liza,
                               self.group_moskow_manager,
                               self.group_moskow_manager_shipment],
                'groups_defect_view': [self.group_admins, self.group_process_administrator,
                                       self.group_process_administrator_liza, self.group_moskow_manager,
                                       self.group_moskow_manager_shipment,
                                       self.group_client, ],
                'groups_defect_set': [self.group_admins, self.group_process_administrator,
                                      self.group_process_administrator_liza, self.group_moskow_manager,
                                      self.group_moskow_manager_shipment,
                                      self.group_client, ],
                'groups_ten_percent_view': [self.group_admins, self.group_process_administrator,
                                            self.group_process_administrator_liza, self.group_moskow_manager,
                                            self.group_moskow_manager_shipment,
                                            self.group_client, ],
                'groups_ten_percent_set': [self.group_admins, self.group_process_administrator,
                                           self.group_process_administrator_liza, self.group_moskow_manager,
                                           self.group_moskow_manager_shipment,
                                           self.group_client, ]
            },
            'reserved': {
                'name': 'Зарезервировано',
                'button_name': 'Зарезервирован',
                'weight': 50,
                'create_1c_document': False,
                'has_local_reserve': True,
                'next': ['formed', 'new', 'client_agreement', ],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager, self.group_moskow_manager_shipment],
            },
            'client_agreement': {
                'name': 'На согласовании',
                'button_name': 'Согласование у партнера',
                'weight': 60,
                'create_1c_document': False,
                'has_local_reserve': True,
                'next': ['client_agreed', 'client_not_agreed'],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager, self.group_moskow_manager_shipment],
            },
            'client_agreed': {
                'name': 'Согласовано партнером',
                'button_name': 'Согласовать',
                'weight': 70,
                'create_1c_document': False,
                'has_local_reserve': True,
                'next': ['formed', 'new'],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_client, self.group_moskow_manager_shipment, ]
            },
            'client_not_agreed': {
                'name': 'Не согласовано партнером',
                'button_name': 'Не согласовать',
                'weight': 80,
                'create_1c_document': False,
                'has_local_reserve': True,
                'next': ['formed', 'new'],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_client, self.group_moskow_manager_shipment, ]
            },
            'formed': {
                'name': 'Отгрузка сформирована',
                'button_name': 'Сформировать отгрузку',
                'weight': 100,
                'next': [],
                'create_1c_document': True,
                'has_local_reserve': True,
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment]
            },
            'shipment_preparing': {
                'name': 'Готовится к отгрузке',
                'button_name': 'Готовится к отгрузке',
                'weight': 200,
                'next': [],
                'create_1c_document': False,
                'has_local_reserve': False,
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment]
            },
            'ready_to_send': {
                'name': 'Готов к отправке',
                'button_name': 'Готов к отправке',
                'weight': 210,
                'next': [],
                'create_1c_document': False,
                'has_local_reserve': False,
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment]
            },
            'shipped': {
                'name': 'Отправлен',
                'button_name': 'Отправлен',
                'weight': 300,
                'next': [],
                'create_1c_document': False,
                'has_local_reserve': False,
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment]
            },
            'received': {
                'name': 'Получено',
                'button_name': 'Получено',
                'weight': 400,
                'next': [],
                'create_1c_document': False,
                'has_local_reserve': False,
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment, self.group_client],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment]
            }
        }

        from orders.models import OrderStatus
        for status_code, status_data in order_statuses.items():
            status = OrderStatus.objects.get_or_create(
                code=status_code,
                defaults={
                    'weight': status_data['weight'],
                    'name': status_data['name'],
                    'button_name': status_data['button_name'],
                    'create_1c_document': status_data['create_1c_document'],
                    'has_local_reserve': status_data['has_local_reserve'],
                }
            )[0]

            self.permissions.append((
                'can_order_edit_{}'.format(status_code),
                'Может редактировать заказ в статусе: {}'.format(status_data['name']),
                self.content_type,
                status_data['groups_view'],
            ))
            self.permissions.append((
                'can_order_set_{}'.format(status_code),
                'Может выставлять заказу статус: {}'.format(status_data['name']),
                self.content_type,
                status_data['groups_set'],
            ))
            self.permissions.append((
                'can_order_defect_edit_{}'.format(status_code),
                'Может редактировать заказ по браку в статусе: {}'.format(status_data['name']),
                self.content_type,
                status_data.get('groups_defect_view', []) or status_data['groups_view'],
            ))
            self.permissions.append((
                'can_order_defect_set_{}'.format(status_code),
                'Может выставлять заказу по браку статус: {}'.format(status_data['name']),
                self.content_type,
                status_data.get('groups_defect_set', []) or status_data['groups_set'],
            ))
            self.permissions.append((
                'can_order_ten_percent_edit_{}'.format(status_code),
                'Может редактировать заказ по обмену 10%в статусе: {}'.format(status_data['name']),
                self.content_type,
                status_data.get('groups_ten_percent_view', []) or status_data['groups_view'],
            ))
            self.permissions.append((
                'can_order_ten_percent_set_{}'.format(status_code),
                'Может выставлять заказу по обмену 10% статус: {}'.format(status_data['name']),
                self.content_type,
                status_data.get('groups_ten_percent_set', []) or status_data['groups_set'],
            ))

        for status_code, status_data in order_statuses.items():
            ps = OrderStatus.objects.get(code=status_code)
            next_statuses = OrderStatus.objects.filter(code__in=status_data['next'])
            ps.next_status.clear()
            ps.next_status = next_statuses

    def create_manufacturing_order_statuses(self):
        manufacturing_order_statuses = (
            ('new', 'Новый', 1, ['sent_to_factory', ], [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager]),
            ('sent_to_factory', 'Отправлен на фабрику', 500, ['cancel', ], [self.group_admins, self.group_brand_manager]),
            ('cancel', 'Отмена заказа', 500, [], [self.group_admins, self.group_brand_manager]),
            ('sewing_labels', 'Пошив этикеток', 500, [], [self.group_admins, self.group_brand_manager]),
            ('received', 'Получен', 1000, [], [self.group_admins, self.group_brand_manager, self.group_moskow_manager]),
            ('refund', 'Возвращен', 2000, [], [self.group_admins, self.group_brand_manager, self.group_moskow_manager]),
        )

        from manufacturing.models import ManufacturingStatus
        for status_data in manufacturing_order_statuses:
            status = ManufacturingStatus.objects.get_or_create(
                code=status_data[0],
                defaults={
                    'weight': status_data[2],
                    'name': status_data[1],
                    'button_name': status_data[1],
                }
            )[0]

            self.permissions.append((
                'can_orders_edit_{}'.format(status_data[0]),
                'Может редактировать заказ на фабрику в статусе: {}'.format(status_data[1]),
                self.content_type,
                status_data[4],
            ))

        for status_data in manufacturing_order_statuses:
            ps = ManufacturingStatus.objects.get(code=status_data[0])
            next_statuses = ManufacturingStatus.objects.filter(code__in=status_data[3])
            ps.next_status.clear()
            ps.next_status = next_statuses

    def create_manufacturing_internal_label_order_statuses(self):
        statuses = (
            ('new', 'Не выбран', 1, ['sent_to_factory', 'cancel'], [self.group_admins, self.group_process_administrator, self.group_brand_manager, self.group_moskow_manager]),
            ('cancel', 'Не заказываем', 2, [], [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager]),
            ('sent_to_factory', 'Отправлен на фабрику', 500, ['received', ], [self.group_admins, self.group_brand_manager]),
            ('received', 'Получен', 1000, [], [self.group_admins, self.group_brand_manager, self.group_moskow_manager]),
        )

        from manufacturing.models import ManufacturingInternalLabelStatus
        for status_data in statuses:
            status = ManufacturingInternalLabelStatus.objects.get_or_create(
                code=status_data[0],
                defaults={
                    'weight': status_data[2],
                    'name': status_data[1],
                    'button_name': status_data[1],
                }
            )[0]

            self.permissions.append((
                'can_m_intern_label_orders_edit_{}'.format(status_data[0]),
                'Может редактировать заказ внутренних этикеток на фабрику в статусе: {}'.format(status_data[1]),
                self.content_type,
                status_data[4],
            ))

        for status_data in statuses:
            ps = ManufacturingInternalLabelStatus.objects.get(code=status_data[0])
            next_statuses = ManufacturingInternalLabelStatus.objects.filter(code__in=status_data[3])
            ps.next_status.clear()
            ps.next_status = next_statuses

    def create_manufacturing_order_label_statuses(self):
        manufacturing_order_statuses = (
            ('new', 'Новый', 1, ['sent_to_factory', ], [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager]),
            ('sent_to_factory', 'Отправлен на фабрику', 500, ['received', 'sewing_labels'], [self.group_admins, self.group_brand_manager]),
            ('received', 'Получен', 1000, ['in_work', 'formed'], [self.group_admins, self.group_brand_manager, self.group_moskow_manager]),
        )

        from manufacturing.models import ManufacturingLabelStatus
        for status_data in manufacturing_order_statuses:
            status = ManufacturingLabelStatus.objects.get_or_create(
                code=status_data[0],
                defaults={
                    'weight': status_data[2],
                    'name': status_data[1],
                    'button_name': status_data[1],
                }
            )[0]

            self.permissions.append((
                'can_orders_label_edit_{}'.format(status_data[0]),
                'Может редактировать заказ этикеток на фабрику в статусе: {}'.format(status_data[1]),
                self.content_type,
                status_data[4],
            ))

        for status_data in manufacturing_order_statuses:
            ps = ManufacturingLabelStatus.objects.get(code=status_data[0])
            next_statuses = ManufacturingLabelStatus.objects.filter(code__in=status_data[3])
            ps.next_status.clear()
            ps.next_status = next_statuses

    def create_supply_statuses(self):
        supply_statuses = {
            'new': {
                'name': 'Новый',
                'button_name': 'Новый',
                'weight': 1,
                'next': ['set_prices', ],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager, self.group_moskow_manager_shipment],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager, self.group_moskow_manager_shipment]
            },
            'set_prices': {
                'name': 'Назначение цен',
                'button_name': 'Отправить в Москву',
                'weight': 100,
                'next': ['sent_to_warehouse', ],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza]
            },
            'sent_to_warehouse': {
                'name': 'Отправлен в Москву',
                'button_name': 'Цены назначены',
                'weight': 200,
                'next': ['delivery_waiting', ],
                'groups_view': [self.group_admins, self.group_process_administrator,
                                self.group_process_administrator_liza, self.group_moskow_manager,
                                self.group_moskow_manager_shipment],
                'groups_set': [self.group_admins, self.group_brand_manager, self.group_moskow_manager_shipment]
            },
            'delivery_waiting': {
                'name': 'Ожидание поставки',
                'button_name': 'Доставка оплачена',
                'weight': 300,
                'next': [],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager, self.group_moskow_manager_shipment],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_moskow_manager, self.group_moskow_manager_shipment]
            },
            'delivery_accepted': {
                'name': 'Поставка принята',
                'button_name': 'Принять поставку',
                'weight': 1000,
                'next': [],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager, self.group_moskow_manager_shipment],
                'groups_set': []
            },
        }

        from supply.models import SupplyStatus
        for status_code, status_data in supply_statuses.items():
            status = SupplyStatus.objects.get_or_create(
                code=status_code,
                defaults={
                    'weight': status_data['weight'],
                    'name': status_data['name'],
                    'button_name': status_data['button_name'],
                }
            )[0]

            self.permissions.append((
                'can_supply_edit_{}'.format(status_code),
                'Может редактировать поставку в статусе: {}'.format(status_data['name']),
                self.content_type,
                status_data['groups_view'],
            ))
            self.permissions.append((
                'can_supply_set_{}'.format(status_code),
                'Может выставлять поставке статус: {}'.format(status_data['name']),
                self.content_type,
                status_data['groups_set'],
            ))

        for status_code, status_data in supply_statuses.items():
            ps = SupplyStatus.objects.get(code=status_code)
            next_statuses = SupplyStatus.objects.filter(code__in=status_data['next'])
            ps.next_status.clear()
            ps.next_status = next_statuses

    def create_purchase_statuses(self):
        purchase_statuses = {
            'new': {
                'name': 'Новый',
                'button_name': 'Новый',
                'weight': 1,
                'create_1c_document': False,
                'set_product_price': False,
                'next': ['sent_to_warehouse', ],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager]
            },
            'sent_to_warehouse': {
                'name': 'Отправлен в Москву',
                'button_name': 'Отправить в Москву',
                'weight': 100,
                'create_1c_document': False,
                'set_product_price': False,
                'next': ['delivery_waiting', ],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager]
            },
            'delivery_waiting': {
                'name': 'Ожидание поставки',
                'button_name': 'Цены назначены',
                'weight': 300,
                'create_1c_document': True,
                'set_product_price': True,
                'next': [],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager],
                'groups_set': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager]
            },
            'delivery_accepted': {
                'name': 'Поставка принята',
                'button_name': 'Принять поставку',
                'weight': 1000,
                'create_1c_document': False,
                'set_product_price': False,
                'next': [],
                'groups_view': [self.group_admins, self.group_process_administrator, self.group_process_administrator_liza, self.group_brand_manager, self.group_moskow_manager],
                'groups_set': []
            },
        }

        from purchases.models import PurchaseStatus
        for status_code, status_data in purchase_statuses.items():
            status = PurchaseStatus.objects.get_or_create(
                code=status_code,
                defaults={
                    'create_1c_document': status_data['create_1c_document'],
                    'set_product_price': status_data['set_product_price'],
                    'weight': status_data['weight'],
                    'name': status_data['name'],
                    'button_name': status_data['button_name'],
                }
            )[0]

            self.permissions.append((
                'can_purchase_edit_{}'.format(status_code),
                'Может редактировать закупку в статусе: {}'.format(status_data['name']),
                self.content_type,
                status_data['groups_view'],
            ))
            self.permissions.append((
                'can_purchase_set_{}'.format(status_code),
                'Может выставлять закупке статус: {}'.format(status_data['name']),
                self.content_type,
                status_data['groups_set'],
            ))

        for status_code, status_data in purchase_statuses.items():
            ps = PurchaseStatus.objects.get(code=status_code)
            next_statuses = PurchaseStatus.objects.filter(code__in=status_data['next'])
            ps.next_status.clear()
            ps.next_status = next_statuses

    def set_permissions(self):
        self.permissions += [
            # Права для обмена данными с 1с
            (
                'api_public_remain_history_post',
                'Может загружать историю остатков',
                self.content_type,
                ( ),
            ),
            (
                'api_public_employee_work_fact_post',
                'Может загружать факт отработанного времени',
                self.content_type,
                ( ),
            ),
            # Прочие права
            (
                'can_order_from_site_view',
                'Видит заказы с сайта',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,)
            ),
            (
                'can_subscriber_view',
                'Видит подписчиков',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,)
            ),
            (
                'can_paying_amount_edit',
                'Может редактировать сумму в платежах',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,)
            ),
            (
                'can_supply_moderate_product',
                'Может модерировать продукты в перемещениях',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,)
            ),
            (
                'can_employees_work_schedule_view',
                'Видит план работы сотрудников',
                self.content_type,
                (self.group_admins,
                 self.group_moskow_manager,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_season_product_collection_plan_view',
                'Видит % сезонность',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_all_shop_season_report_view',
                'Видит все сезонные отчеты',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,)
            ),
            (
                'can_self_shop_season_report_view',
                'Видит свои сезонные отчеты',
                self.content_type,
                (
                    self.group_client,
                )
            ),
            (
                'view_partner_day_statistic',
                'Видит отчет "Статистика партнеров по дням"',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'view_employee_day_statistic',
                'Видит отчет "Статистика продавцов по дням"',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,)
            ),
            (
                'can_is_new_product_view',
                'Видит признак устаревшей модели',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment, )
            ),
            (
                'view_report_consolidated',
                'Видит сводный отчет',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager, )
            ),
            (
                'view_report_mutual_payments',
                'Видит отчет по взаиморасчетам',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_client,
                 self.group_moskow_manager, )
            ),
            (
                'view_report_mutual_payments_all',
                'Видит отчет по взаиморасчетам для всех клиентов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager, )
            ),
            (
                'view_report_office_plan',
                'Видит отчет Офис-План',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager, )
            ),
            (
                'can_preorder_view',
                'Видит предзаказы',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_paying_view',
                'Видит платежи',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_newsletter_view',
                'Видит рассылки',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_sales_plan_view',
                'Видит планы продаж',
                self.content_type,
                (self.group_admins,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'view_report_sales_plan',
                'Видит отчет мониторинг продаж',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_client, )
            ),
            (
                'view_report_all_sales_plan',
                'Видит отчет мониторинг продаж',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment, )
            ),
            (
                'view_report_employee_sale',
                'Видит отчет по своим продавцам',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_client, )
            ),
            (
                'view_report_all_employee_sale',
                'Видит отчет по всем продавцам',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment, )
            ),
            (
                'view_report_sales_plan_project',
                'Видит отчет Проект плана',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment, )
            ),
            (
                'can_price_set_view',
                'Видит документы установки цен',
                self.content_type,
                (self.group_admins,
                 self.group_moskow_manager,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_view_buyer_profiles',
                'Видит анкеты клиентов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_client,
                 self.group_merchandiser,
                 self.group_moskow_manager,
                 self.group_handbook_access, )
            ),
            (
                'can_view_all_buyer_profiles',
                'Видит все анкеты клиентов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_merchandiser,
                 self.group_moskow_manager, )
            ),
            (
                'can_view_buyer_cards',
                'Видит карты клиентов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_client,
                 self.group_merchandiser,
                 self.group_handbook_access, )
            ),
            (
                'can_view_all_buyer_cards',
                'Видит все карты клиентов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_merchandiser, )
            ),
            (
                'can_set_offer_new_product_price',
                'Может устанавливать цены при создании модели из предложения',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_offers_links_view',
                'Видит ссылки у предложений',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_brand_manager,  )
            ),
            (
                'can_music_view',
                'Видит музыку',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager, )
            ),
            (
                'can_defect_act_view',
                'Видит акты на брак',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_merchandiser,
                 self.group_client,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,)
            ),
            (
                'can_all_defect_act_view',
                'Видит все акты на брак',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_merchandiser,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,)
            ),
            (
                'can_product_public_tab_view',
                'Видит вкладку публичные данные товаров',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,)
            ),
            (
                'can_defect_act_history_tab_view',
                'Видит вкладку история актов на брак',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_merchandiser,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,)
            ),
            (
                'can_refund_act_history_tab_view',
                'Видит вкладку история актов на возврат',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_merchandiser,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,)
            ),
            (
                'can_document_scan_view',
                'Видит документы',
                self.content_type,
                (self.group_admins,
                 self.group_client,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_all_document_scan_view',
                'Видит все документы',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_document_scan_add',
                'Добавляет документы',
                self.content_type,
                (self.group_admins,
                 self.group_client,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_document_scan_add_with_client',
                'Добавляет документы, указывая парнтера',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_document_scan_status_change',
                'Может менять статусы у документов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_refund_act_view',
                'Видит акты на возврат',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_merchandiser,
                 self.group_client,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,)
            ),
            (
                'can_all_refund_act_view',
                'Видит все акты на возврат',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_merchandiser,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,)
            ),
            (
                'can_video_view',
                'Видит видео',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_client,
                 self.group_merchandiser, )
            ),
            (
                'can_task_view',
                'Видит задачи',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_client,
                 self.group_merchandiser, )
            ),
            (
                'can_all_task_view',
                'Видит все задачи',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_merchandiser, )
            ),
            (
                'can_add_task',
                'Может добавлять задачи',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_merchandiser, )
            ),
            (
                'can_view_task_partner',
                'Может видеть партнера в задаче',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_merchandiser, )
            ),
            (
                'can_edit_task_date_plan',
                'Может редактировать дату план в задаче',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_merchandiser, )
            ),
            (
                'can_view_task_widget',
                'Видит виджет задач',
                self.content_type,
                (self.group_client, )
            ),
            (
                'can_view_order_filter_client',
                'Видит фильтр "клиенты" в бланк-заказах',
                self.content_type,
                (self.group_admins,
                 self.group_brand_manager,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser, )
            ),
            (
                'can_view_order_filter_products',
                'Видит фильтр "модели" в бланк-заказах',
                self.content_type,
                (self.group_admins,
                 self.group_brand_manager,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser, )
            ),
            (
                'can_export_order_pdf_with_size_quantity',
                'Выгрузка бланк-заказов в pdf с количеством размеров',
                self.content_type,
                (self.group_admins,
                 self.group_brand_manager,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser, )
            ),
            (
                'can_view_order_b2c_filter_client',
                'Видит фильтр "клиенты" в отчете "продажи"',
                self.content_type,
                (self.group_admins,
                 self.group_brand_manager,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser, )
            ),
            (
                'can_view_order_b2c_filter_products',
                'Видит фильтр "модели" в отчете "продажи"',
                self.content_type,
                (self.group_admins,
                 self.group_brand_manager,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser, )
            ),
            (
                'can_view_order_return_b2c_filter_products',
                'Видит фильтр "модели" в возвратах покупателей',
                self.content_type,
                (self.group_admins,
                 self.group_brand_manager,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser, )
            ),
            (
                'can_main_menu_view',
                'Видит обычное меню',
                self.content_type,
                (self.group_admins,
                 self.group_brand_manager,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 self.group_handbook_access,
                 self.group_contract_sale, )
            ),
            (
                'can_client_menu_view',
                'Видит обычное меню',
                self.content_type,
                (self.group_client, )
            ),
            (
                'can_processing_button_view',
                'Видит кнопку обработки заявок на предложения',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_offer_requests_view',
                'Просмотр заявок на предложения',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 self.group_client, )
            ),
            (
                'can_all_offer_requests_view',
                'Просмотр всех заявок на предложения',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser, )
            ),
            (
                'can_change_status_offer_requests',
                'Может менять статусы в предзаказов на новые модели',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_aggreement_offer_view',
                'Просмотр новых моделей',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_merchandiser,
                 self.group_moskow_manager,
                 self.group_handbook_access,
                 self.group_client, )
            ),
            (
                'view_reports_menu',
                'Доступно меню "Отчеты"',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment, )
            ),
            (
                'payment_has_writing_off',
                'Может списывать деньги со счета',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager,)
            ),
            (
                'view_report_client_statistic',
                'Отчет "статистика по клиентам"',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_client, )
            ),
            (
                'view_report_products_not_sold',
                'Отчет "не проданные товары"',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser, )
            ),
            (
                'view_report_remain',
                'Отчет "Остатки партнеров"',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser, )
            ),
            (
                'view_report_movement_of_goods',
                'Отчет "По движению товаров"',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_client,
                 self.group_merchandiser, )
            ),
            (
                'view_all_report_movement_of_goods',
                'Отчет "По движению товаров" видит всех клиентов',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser, )
            ),
            (
                'can_set_all_passwords',
                'Может менять пароли пользователей',
                self.content_type,
                ()
            ),
            (
                'can_payment_accounts_view',
                'Может видеть счета',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager,)
            ),
            (
                'can_invoice_view',
                'Может видеть счета2',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_admins,
                 self.group_merchandiser,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_contract_sale, )
            ),
            (
                'can_change_invoice_status',
                'Может менять статус у счетов',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_admins, )
            ),
            (
                'can_expense_item_view',
                'Может видеть статьи расходов',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,)
            ),
            (
                'can_all_payment_accounts_view',
                'Может видеть все счета',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,)
            ),
            (
                'can_transfer_payment_accounts_view',
                'Может пополнять счета',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,)
            ),
            (
                'view_all_client_statistic',
                'Может видеть статистику по всем клиентам',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment, )
            ),
            (
                'view_self_client_statistic',
                'Может видеть статистику клиента для себя',
                self.content_type,
                (self.group_client, ),
            ),
            (
                'create_all_client_statistic',
                'Может создавать статистику по всем клиентам',
                self.content_type,
                (self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'create_self_client_statistic',
                'Может создавать статистику клиента для себя',
                self.content_type,
                (self.group_client, )
            ),
            (
                'can_groups_view',
                'Просмотр групп пользователей',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 )
            ),
            (
                'can_users_view',
                'Просмотр пользователей',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_users_create',
                'Создание пользователей',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, ),
            ),
            # Заказы
            (
                'can_orders_view',
                'Просмотр заказов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_client,
                 self.group_merchandiser,
                 )
            ),
            # Заказы
            (
                'can_all_orders_view',
                'Просмотр всех заказов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 )
            ),
            (
                'can_set_client_on_order_create',
                'Выбор партнера при создании заказа',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 )
            ),
            (
                'can_all_free_defect_summ_view',
                'Просмотр свободных сумм по браку для всех партнеров',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 )
            ),
            (
                'can_all_free_refund_summ_view',
                'Просмотр свободных сумм по возврату для всех партнеров',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 )
            ),
            (
                'can_invoice_history_tab_view',
                'Просмотр вкладки история у счетов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 )
            ),
            (
                'can_order_category_tab_view',
                'Просмотр вкладки категория у заказов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 )
            ),
            (
                'can_order_history_tab_view',
                'Просмотр вкладки история у заказов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 )
            ),
            (
                'can_order_category_set',
                'Распледеление по категориям у заказов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 )
            ),
            (
                'can_orders_edit',
                'Редактирование заказов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 )
            ),
            (
                'can_orders_delete',
                'Удаление заказов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 )
            ),
            (
                'can_orders_main_create',
                'Создание основных заказов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 )
            ),
            (
                'can_orders_defect_create',
                'Создание заказы по обмену брака',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 self.group_client,
                 )
            ),
            (
                'can_orders_ten_percent_create',
                'Создание заказы по обмену 10%',
                self.content_type,
                (
                # (self.group_admins,
                #  self.group_process_administrator,
                #  self.group_process_administrator_liza,
                #  self.group_moskow_manager,
                #  self.group_moskow_manager_shipment,
                #  self.group_merchandiser,
                #  self.group_client,
                 )
            ),
            (
                'can_order_item_all_delete',
                'Удаление любых позиций из заказов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 )
            ),
            (
                'can_order_item_self_delete',
                'Удаление своих позиций из заказов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 self.group_client,
                 )
            ),
            (
                'can_order_items_sizes_view',
                'Редактирование заказов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 )
            ),
            (
                'can_orders_b2с_view',
                'Просмотр заказов b2с',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_client,
                 self.group_merchandiser,
                 self.group_handbook_access,
                 )
            ),
            (
                'can_all_orders_b2с_view',
                'Просмотр всех заказов b2с',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 )
            ),
            (
                'can_orders_return_b2с_view',
                'Просмотр возвратов b2с',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_client,
                 self.group_merchandiser,
                 )
            ),
            (
                'can_all_orders_return_b2с_view',
                'Просмотр всех возвратов b2с',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 )
            ),
            (
                'can_orders_return_view',
                'Просмотр возвратов поставщикам',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 self.group_client,
                 )
            ),
            (
                'can_all_orders_return_view',
                'Просмотр возвратов поставщикам',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 )
            ),
            (
                'can_orders_return_delete',
                'Удаление возвратов поставщикам',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 )
            ),
            (
                'can_holder_print_view',
                'Видит печать холдеров',
                self.content_type,
                (self.group_admins,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 )
            ),
            # (
            #     'can_orders_all_view',
            #     'Просмотр всех заказов в системе',
            #     self.content_type,
            #     (self.group_admins,
            #      self.group_process_administrator)
            # ),
            # (
            #     'can_orders_create',
            #     'Создание заказов',
            #     self.content_type,
            #     (self.group_admins,
            #      self.group_process_administrator,
            #      self.group_brand_manager, ),
            # ),
            # (
            #     'can_view_orders_widget',
            #     'Просмотр виджета "Заказы"',
            #     self.content_type,
            #     (self.group_admins,
            #      self.group_process_administrator,
            #      self.group_brand_manager, )
            # ),
            # Заказы
            (
                'can_manufacturing_orders_view',
                'Просмотр заказов на фабрику',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager,
                 self.group_moskow_manager, )
            ),
            (
                'can_view_orders_widget2',
                'Просмотр виджета заказов',
                self.content_type,
                (self.group_client, )
            ),

            # Поставки
            (
                'can_supply_view',
                'Просмотр поставок',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager,
                 self.group_moskow_manager, )
            ),
            (
                'can_view_supply_widget',
                'Просмотр виджета "Поставки"',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager, )
            ),
            # Закупки
            (
                'can_purchase_view',
                'Просмотр закупок',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager,
                 self.group_moskow_manager, )
            ),
            (
                'can_view_purchase_widget',
                'Просмотр виджета "Закупки"',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager,
                 self.group_moskow_manager, )
            ),
            # Предложения
            (
                'can_offers_view',
                'Просмотр предложений',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager, )
            ),
            (
                'can_offers_all_view',
                'Просмотр всех предложений в системе',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager, )
            ),
            (
                'can_edit_offer_aggreement',
                'Редактирование предложений в согласованиях',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            (
                'can_offers_create',
                'Создание предложений',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager, ),
            ),
            (
                'can_offers_blank_worker_create',
                'Создание предложений c не заполненным ответственным',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, ),
            ),
            (
                'can_get_offers_blank_worker',
                'Может забирать себе в работу предложения',
                self.content_type,
                (self.group_admins,
                 self.group_brand_manager, ),
            ),
            (
                'can_save_offers_blank_worker',
                'Может сохранять не назначенные на других предложения',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, ),
            ),
            (
                'can_view_offers_widget',
                'Просмотр виджета "Предложения"',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager, )
            ),
            (
                'can_view_offers_link_in_product_list_item',
                'Видит предложение из которого создан товар в списке товаров',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza, )
            ),
            # Товары
            (
                'can_products_view',
                'Просмотр моделей',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_handbook_access, )
            ),
            (
                'can_product_price_edit',
                'Может редактировать цены товаров',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_client, )
            ),
            (
                'can_product_price_view',
                'Может видеть цены товаров',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_client,
                 self.group_handbook_access, )
            ),
            (
                'can_product_is_active_view',
                'Может видеть признак активности товаров',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_handbook_access, )
            ),
            (
                'can_product_has_remains_view',
                'Может видеть наличие на остатках товаров',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_client, )
            ),
            # Этикетки
            (
                'can_product_labels_view',
                'Просмотр этикеток',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager,
                 self.group_moskow_manager, )
            ),
            # Фабрики
            (
                'can_factories_view',
                'Просмотр фабрик',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager, )
            ),
            # Каталог
            (
                'cat_shop_view',
                'Просмотр заказов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_merchandiser,
                 self.group_handbook_access,
                 self.group_client, )
            ),
            # Курсы валют
            (
                'can_view_currency_widget',
                'Просмотр виджета "курсы валют"',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager, )
            ),
            # Комментарии
            (
                'can_view_comments_widget',
                'Просмотр виджета "Комментарии"',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager, )
            ),
            # Склады
            (
                'can_warehouses_view',
                'Просмотр складов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager,
                 self.group_moskow_manager, )
            ),
            # Партнеры
            (
                'can_clients_view',
                'Просмотр клиентов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_brand_manager,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_handbook_access, )
            ),
            # Поставщики
            (
                'can_providers_view',
                'Просмотр поставщиков',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager_shipment, )
            ),
            # Магазины
            (
                'can_store_view',
                'Просмотр магазинов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_handbook_access, )
            ),
            (
                'can_own_store_view',
                'Просмотр своих магазинов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_handbook_access, )
            ),
            # Сотрудники магазинов
            (
                'can_employees_view',
                'Просмотр сотрудников магазинов',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment,
                 self.group_handbook_access, )
            ),
            (
                'can_employees_description_view',
                'Видит комментарии для сотрудников',
                self.content_type,
                (self.group_admins,
                 self.group_process_administrator,
                 self.group_process_administrator_liza,
                 self.group_moskow_manager,
                 self.group_moskow_manager_shipment, )
            ),
        ]

from django.http.response import HttpResponseRedirect
from django.views.generic import TemplateView


class HomeView(TemplateView):
    template_name = 'base2.html'

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if user.groups.count() == 1 and user.groups.all()[0].name == 'Продавцы':
            return HttpResponseRedirect('/multimedia/player/')
        return super().dispatch(request, *args, **kwargs)

from django.db.models import QuerySet
from django.db import models
from django.db.utils import IntegrityError


class DeleteCheckQuerySet(QuerySet):

    def delete(self):
        """Удалять можно только документы в статусе, отличном от "одобрен" issue #321 """
        if self.filter(status=2):
            raise IntegrityError
        super().delete()


class DeleteCheckManager(models.Manager):

    def get_queryset(self):
        return DeleteCheckQuerySet(self.model, using=self._db)

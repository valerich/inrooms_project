import os
import uuid

from django.db import models
from django.db.utils import IntegrityError
from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

from .managers import DeleteCheckManager


class DocumentScanKind(TimeStampedModel):
    name = models.CharField(verbose_name='Название', max_length=200, unique=True)
    group = models.CharField(verbose_name='Группа документов', max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тип документа'
        verbose_name_plural = 'Типы документов'
        ordering = ('name', )


STATUS_CHOICES = Choices(
    (1, 'new', 'Новый'),
    (2, 'approved', 'Одобрен'),
    (10, 'cancel', 'Отклонен'),
)


class DocumentScan(TimeStampedModel):
    STATUS = STATUS_CHOICES

    kind = models.ForeignKey(DocumentScanKind, verbose_name='Тип документа')
    status = models.SmallIntegerField('Статус', choices=STATUS, default=STATUS.new)
    client = models.ForeignKey('clients.Client', verbose_name='Партнер')

    objects = DeleteCheckManager()

    def __str__(self):
        return '{}, {}, {}, {}'.format(self.kind, self.client, self.status, self.created, )

    class Meta:
        verbose_name = 'Документ'
        verbose_name_plural = 'Документы'
        ordering = ('-id', )

    def delete(self, *args, **kwargs):
        """Удалять можно только документы в статусе, отличном от "одобрен" issue #321 """
        if self.status == 2:
            raise IntegrityError
        super().delete(*args, **kwargs)


def generate_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'documents/{}{}'.format(uuid.uuid4(), ext)


class DocumentScanFile(TimeStampedModel):
    document_scan = models.ForeignKey(DocumentScan, verbose_name='Документ', related_name='files')
    filename = models.CharField('Название файла', max_length=1000, blank=True)
    file = models.FileField('Файл', upload_to=generate_filename, max_length=755, blank=True)

    class Meta:
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'
        ordering = ('-id', )

    def get_file_name(self):
        file = self.file.name.split('/')[-1]
        return file[:file.rfind('.')]

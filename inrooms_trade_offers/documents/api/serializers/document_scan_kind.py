from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import DocumentScanKind


class DocumentScanKindSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    group_detail = serializers.SerializerMethodField()

    class Meta:
        model = DocumentScanKind
        fields = [
            'id',
            'name',
            'group',
            'group_detail'
        ]

    def get_group_detail(self, obj):
        return {
            'name': obj.group
        }

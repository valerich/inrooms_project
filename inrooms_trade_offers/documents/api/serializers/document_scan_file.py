from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import DocumentScanFile


class DocumentScanFileSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = DocumentScanFile
        fields = [
            'id',
            'filename',
            'file',
        ]

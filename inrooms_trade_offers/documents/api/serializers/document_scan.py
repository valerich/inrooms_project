from rest_framework import serializers

from clients.api.serializers.client import ClientSerializer
from .document_scan_kind import DocumentScanKindSerializer
from ...models import DocumentScan


class DocumentScanSerializer(serializers.ModelSerializer):
    client_detail = serializers.SerializerMethodField()
    kind_detail = serializers.SerializerMethodField()
    status_detail = serializers.SerializerMethodField()

    class Meta:
        model = DocumentScan

        fields = [
            'id',
            'status',
            'status_detail',
            'kind',
            'kind_detail',
            'client',
            'client_detail',
            'created',
            'modified',
        ]

    def get_client_detail(self, obj):
        return ClientSerializer(obj.client, fields=['id', 'name']).data

    def get_status_detail(self, obj):
        return {
            'id': obj.status,
            'name': obj.get_status_display(),
        }

    def get_kind_detail(self, obj):
        return DocumentScanKindSerializer(obj.kind, fields=['id', 'name']).data
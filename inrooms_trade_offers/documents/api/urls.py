from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import DocumentScanViewSet, DocumentScanKindViewSet

router = DefaultRouter()
router.register(r'document_scan', DocumentScanViewSet, base_name='document_scan')
router.register(r'document_scan_kind', DocumentScanKindViewSet, base_name='document_scan_kind')


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
]

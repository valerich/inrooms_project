from django.db import transaction
from rest_framework import filters, status
from rest_framework.decorators import detail_route, list_route
from rest_framework.exceptions import ValidationError
from rest_framework.filters import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ..serializers import DocumentScanFileSerializer, DocumentScanSerializer
from ...models import DocumentScan, DocumentScanFile


class DocumentScanViewSet(ModelViewSet):
    serializer_class = DocumentScanSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend,)
    filter_fields = ('status', 'client', 'kind')
    search_fields = ('=id', )
    queryset = DocumentScan.objects.all()
    ordering_fields = (
        'id',
        'client__name',
        'kind__name',
        'created',
        'modified',
    )

    def get_queryset(self):
        qs = DocumentScan.objects.all()
        if not self.request.user.has_perm('auth.can_all_document_scan_view'):
            if self.request.user.has_perm('auth.can_document_scan_view'):
                if self.request.user.client:
                    qs = qs.filter(client=self.request.user.client)
                else:
                    qs = qs.none()
            else:
                qs = qs.none()
        return qs

    def destroy(self, request, *args, **kwargs):
        with transaction.atomic():
            instance = self.get_object()
            if instance.status != DocumentScan.STATUS.approved:
                self.perform_destroy(instance)
            else:
                return Response({'error': 'Нельзя удалять документы в статусе "Одобрен"'},
                                status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_create(self, serializer):
        if self.request.user.has_perm('auth.can_document_scan_add_with_client'):
            if not serializer.validated_data.get('client', None):
                if self.request.user.client:
                    serializer.save(client=self.request.user.client)
                else:
                    raise ValidationError({'client': ['Обязательное поле']})
        elif self.request.user.has_perm('auth.can_document_scan_add'):
            if not serializer.validated_data.get('client', None):
                raise ValidationError({'client': ['Обязательное поле']})
        else:
            raise ValidationError({'client': ['Обязательное поле']})
        serializer.save()

    @detail_route(methods=['post'])
    def file_upload(self, request, pk=None):
        obj = self.get_object()
        if obj.status == obj.STATUS.approved:
            return Response({'error': 'Нельзя добавлять файлы для документа в статусе "Одобрен"'},
                            status=status.HTTP_400_BAD_REQUEST)
        file_obj = request.FILES['file']

        _file = DocumentScanFile.objects.create(
            document_scan=obj,
            filename=file_obj.name,
            file=file_obj,
        )
        return Response({
            'status': 'ok'
        })

    @list_route(methods=['post'])
    def file_delete(self, request):
        file_id = request.data.get('file_id', None)

        try:
            _file = DocumentScanFile.objects.get(
                id=file_id
            )
            if _file.document_scan.status == _file.document_scan.STATUS.approved:
                return Response({'error': 'Нельзя удалять файлы документа в статусе "Одобрен"'},
                                status=status.HTTP_400_BAD_REQUEST)

        except DocumentScanFile.DoesNotExist:
            pass
        else:
            _file.delete()
        return Response({
            'status': 'ok'
        })

    @detail_route(methods=['get'])
    def files(self, request, pk=None):
        obj = self.get_object()
        return Response(DocumentScanFileSerializer(obj.files.all(), many=True).data)

    @detail_route(methods=['post'])
    @transaction.atomic()
    def change_status(self, request, pk=None):
        """Изменение статуса документа
        """
        obj = self.get_object()
        status_code = request.data.get('code', None)
        if status_code:
            if isinstance(status_code, int) or status_code.isdigit():
                status_code = int(status_code)
                if status_code in DocumentScan.STATUS._db_values:
                    if obj.status == DocumentScan.STATUS.new:
                        if request.user.has_perm('auth.can_document_scan_status_change'):
                            obj.status = status_code
                            obj.save()
                            return Response(status=status.HTTP_200_OK)
                        else:
                            return Response({'error': 'Не достаточно прав'},
                                            status=status.HTTP_403_FORBIDDEN)
                    else:
                        return Response({'error': 'Статус можно изменить только у нового документа'},
                                        status=status.HTTP_400_BAD_REQUEST)
        return Response({'error': 'Не верный статус'},
                        status=status.HTTP_400_BAD_REQUEST)

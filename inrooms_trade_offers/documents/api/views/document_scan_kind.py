from rest_framework import filters, status
from rest_framework.decorators import detail_route, list_route
from rest_framework.filters import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ..serializers import DocumentScanKindSerializer
from ...models import DocumentScanKind


class DocumentScanKindViewSet(ModelViewSet):
    serializer_class = DocumentScanKindSerializer
    filter_backends = (filters.SearchFilter, DjangoFilterBackend,)
    filter_fields = ('group', )
    search_fields = ('=id', )
    queryset = DocumentScanKind.objects.all()
    ordering_fields = (
        'id',
    )

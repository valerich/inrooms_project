import factory
import factory.django
import factory.fuzzy
from django.core.files.base import ContentFile

from clients.factories import ClientFactory
from .models import (
    DocumentScan,
    DocumentScanFile,
    DocumentScanKind,
    STATUS_CHOICES,
)


class DocumentScanKindFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText(length=200)
    group = factory.fuzzy.FuzzyText(length=100)

    class Meta:
        model = DocumentScanKind


class DocumentScanFactory(factory.django.DjangoModelFactory):
    kind = factory.SubFactory(DocumentScanKindFactory)
    client = factory.SubFactory(ClientFactory)
    status = factory.fuzzy.FuzzyChoice(STATUS_CHOICES._db_values)

    class Meta:
        model = DocumentScan


class DocumentScanFileFactory(factory.django.DjangoModelFactory):
    document_scan = factory.SubFactory(DocumentScanFactory)
    filename = factory.LazyAttribute(
        lambda _: ContentFile(
            factory.django.FileField()._make_data(
                {'data': b'filedata'}
            ), 'example.tmp'
        )
    )

    class Meta:
        model = DocumentScanFile

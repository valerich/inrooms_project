from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'documents'
    verbose_name = 'Документооборот'

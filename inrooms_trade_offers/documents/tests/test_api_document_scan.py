from django.core.urlresolvers import reverse
from freezegun.api import freeze_time
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.tests.setup import UsersSetup
from clients.factories import ClientFactory
from ..factories import DocumentScanFactory, DocumentScanKindFactory
from ..models import DocumentScan


class DocumentScanViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.list_url = reverse('api:documents:document_scan-list')
        UsersSetup()
        cls.client_admin = APIClient()
        cls.client_admin.login(email='admin@example.com', password='password')

    def test_list(self):
        response = self.client_admin.get(self.list_url)
        self.assertEqual(response.data['count'], 0)

        DocumentScanFactory()
        response = self.client_admin.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

    @freeze_time("2016-1-31")
    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        DocumentScan.objects.all().delete()
        kind = DocumentScanKindFactory(
            name='kind_name',
            group='kind_group'
        )
        client = ClientFactory(
            name='client_name'
        )
        object = DocumentScanFactory(
            kind=kind,
            client=client,
            status=DocumentScan.STATUS.new,
        )

        response = self.client_admin.get(self.list_url)
        object_dict = response.data['results'][0]
        valid_object_dict = {
            "id": object.id,
            "status": 1,
            "status_detail": {
                "name": "Новый",
                "id": 1
            },
            "kind": kind.id,
            "kind_detail": {
                "id": kind.id,
                "name": "kind_name"
            },
            "client": client.id,
            "client_detail": {
                "id": client.id,
                "name": "client_name"
            },
            "created": "2016-01-31T00:00:00Z",
            "modified": "2016-01-31T00:00:00Z"
        }
        self.assertDictEqual(object_dict, valid_object_dict)

    def test_list_filter(self):
        """Фильтрация"""


        DocumentScan.objects.all().delete()
        kind1 = DocumentScanKindFactory(
            name='kind_name1'
        )
        kind2 = DocumentScanKindFactory(
            name='kind_name2'
        )
        client1 = ClientFactory(
            name='client_name1'
        )
        client2 = ClientFactory(
            name='client_name2'
        )
        object1 = DocumentScanFactory(
            kind=kind1,
            client=client1,
            status=DocumentScan.STATUS.new,
        )
        object2 = DocumentScanFactory(
            kind=kind2,
            client=client2,
            status=DocumentScan.STATUS.approved,
        )

        # фильтрация по типу документа
        response = self.client_admin.get(self.list_url, data={'kind': kind1.id})
        object_dict = response.data['results'][0]
        self.assertEqual(object_dict['id'], object1.id)

        response = self.client_admin.get(self.list_url, data={'kind': kind2.id})
        object_dict = response.data['results'][0]
        self.assertEqual(object_dict['id'], object2.id)

        # фильтрация по клиенту
        response = self.client_admin.get(self.list_url, data={'client': client1.id})
        object_dict = response.data['results'][0]
        self.assertEqual(object_dict['id'], object1.id)

        response = self.client_admin.get(self.list_url, data={'client': client2.id})
        object_dict = response.data['results'][0]
        self.assertEqual(object_dict['id'], object2.id)

        # фильтрация по статусу
        response = self.client_admin.get(self.list_url, data={'status': DocumentScan.STATUS.new})
        object_dict = response.data['results'][0]
        self.assertEqual(object_dict['id'], object1.id)

        response = self.client_admin.get(self.list_url, data={'status': DocumentScan.STATUS.approved})
        object_dict = response.data['results'][0]
        self.assertEqual(object_dict['id'], object2.id)

    def test_delete(self):
        """Удаление документов

        Удалять можно в любом статусе, кроме "Одобрен"
        """

        DocumentScan.objects.all().delete()
        object1 = DocumentScanFactory(
            status=DocumentScan.STATUS.new,
        )
        object2 = DocumentScanFactory(
            status=DocumentScan.STATUS.approved,
        )
        object3 = DocumentScanFactory(
            status=DocumentScan.STATUS.cancel,
        )

        # Можно удалять во всех статусах
        response = self.client_admin.delete(
            reverse('api:documents:document_scan-detail', args=[object1.id])
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(DocumentScan.objects.count(), 2)

        response = self.client_admin.delete(
            reverse('api:documents:document_scan-detail', args=[object3.id])
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(DocumentScan.objects.count(), 1)

        # кроме "одобрен"
        response = self.client_admin.delete(
            reverse('api:documents:document_scan-detail', args=[object2.id])
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['error'], 'Нельзя удалять документы в статусе "Одобрен"')
        self.assertEqual(DocumentScan.objects.count(), 1)

from django.test import TestCase

from ..factories import DocumentScanFactory, DocumentScanKindFactory, DocumentScanFileFactory
from ..models import DocumentScanFile, DocumentScan, DocumentScanKind


class DocumentScanFactoryTestCase(TestCase):
    def setUp(self):
        DocumentScan.objects.all().delete()

    def test_factory(self):
        obj = DocumentScanFactory()
        obj_from_db = DocumentScan.objects.all().first()
        self.assertEqual(obj, obj_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(DocumentScanFactory())

        self.assertEqual(len(objects), len(DocumentScan.objects.all()))


class DocumentScanKindFactoryTestCase(TestCase):
    def setUp(self):
        DocumentScanKind.objects.all().delete()

    def test_factory(self):
        obj = DocumentScanKindFactory()
        obj_from_db = DocumentScanKind.objects.all().first()
        self.assertEqual(obj, obj_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(DocumentScanKindFactory())

        self.assertEqual(len(objects), len(DocumentScanKind.objects.all()))


class DocumentScanFileFactoryTestCase(TestCase):
    def setUp(self):
        DocumentScanFile.objects.all().delete()

    def test_factory(self):
        obj = DocumentScanFileFactory()
        obj_from_db = DocumentScanFile.objects.all().first()
        self.assertEqual(obj, obj_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(DocumentScanFileFactory())

        self.assertEqual(len(objects), len(DocumentScanFile.objects.all()))

from django.db.utils import IntegrityError
from django.test import TestCase

from ..factories import DocumentScanFactory
from ..models import DocumentScan


class DocumentScanDeleteTestCase(TestCase):
    def setUp(self):
        DocumentScan.objects.all().delete()

    def test_objects_delete(self):
        """Массовое удаление должно работать,
        но только если в выборку не попадают документы в статусе "одобрен" issue #321
        """

        DocumentScanFactory(status=DocumentScan.STATUS.new)
        DocumentScanFactory(status=DocumentScan.STATUS.cancel)
        DocumentScanFactory(status=DocumentScan.STATUS.approved)
        self.assertEqual(DocumentScan.objects.count(), 3)

        # Не даем удалить документы в статусе "одобрен"
        with self.assertRaises(IntegrityError):
            DocumentScan.objects.all().delete()

        self.assertEqual(DocumentScan.objects.count(), 3)

        # А остальные можно удалять
        DocumentScan.objects.exclude(status=DocumentScan.STATUS.approved).delete()

        self.assertEqual(DocumentScan.objects.count(), 1)

    def test_one_object_delete(self):
        doc1 = DocumentScanFactory(status=DocumentScan.STATUS.new)
        self.assertEqual(DocumentScan.objects.count(), 1)
        doc1.delete()
        self.assertEqual(DocumentScan.objects.count(), 0)

        doc1 = DocumentScanFactory(status=DocumentScan.STATUS.cancel)
        self.assertEqual(DocumentScan.objects.count(), 1)
        doc1.delete()
        self.assertEqual(DocumentScan.objects.count(), 0)

        doc1 = DocumentScanFactory(status=DocumentScan.STATUS.approved)
        self.assertEqual(DocumentScan.objects.count(), 1)
        with self.assertRaises(IntegrityError):
            doc1.delete()
        self.assertEqual(DocumentScan.objects.count(), 1)

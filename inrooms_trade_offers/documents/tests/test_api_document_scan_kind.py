from django.core.urlresolvers import reverse
from freezegun.api import freeze_time
from rest_framework.test import APIClient, APITestCase

from accounts.tests.setup import UsersSetup
from ..factories import DocumentScanKindFactory
from ..models import DocumentScanKind


class DocumentScanKindViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.list_url = reverse('api:documents:document_scan_kind-list')
        UsersSetup()
        cls.client_admin = APIClient()
        cls.client_admin.login(email='admin@example.com', password='password')

    def test_list(self):
        response = self.client_admin.get(self.list_url)
        self.assertEqual(response.data['count'], 0)

        DocumentScanKindFactory()
        response = self.client_admin.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

    @freeze_time("2016-1-31")
    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        DocumentScanKind.objects.all().delete()
        object = DocumentScanKindFactory(
            name="test_name",
            group="test_group",

        )
        response = self.client_admin.get(self.list_url)
        object_dict = response.data['results'][0]
        valid_object_dict = {
            "id": object.id,
            "name": "test_name",
            "group": "test_group",
            "group_detail": {
                "name": "test_group",
            }
        }
        self.assertDictEqual(object_dict, valid_object_dict)

    def test_list_filter_groups(self):
        """Фильтрация по группам"""

        DocumentScanKind.objects.all().delete()
        object1 = DocumentScanKindFactory(
            name="test_name",
            group="test_group",
        )
        object2 = DocumentScanKindFactory(
            name="test_name2",
            group="test_group2",
        )
        response = self.client_admin.get(self.list_url, data={'group': "test_group"})
        object_dict = response.data['results'][0]
        self.assertEqual(object_dict['id'], object1.id)

        response = self.client_admin.get(self.list_url, data={'group': "test_group2"})
        object_dict = response.data['results'][0]
        self.assertEqual(object_dict['id'], object2.id)

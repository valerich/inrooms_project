from django.contrib import admin

from .models import DocumentScanKind, DocumentScan


class DocumentScanKindAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'group', )
    list_filter = ('group', )
    search_fields = ('=id', 'name', )


class DocumentScanAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'client', 'status', 'kind')
    list_filter = ('status', 'kind', )
    search_fields = ('=id', 'client__name')


for model_or_iterable, admin_class in (
    (DocumentScanKind, DocumentScanKindAdmin),
    (DocumentScan, DocumentScanAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

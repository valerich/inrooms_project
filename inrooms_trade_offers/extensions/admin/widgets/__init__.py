from django.contrib.admin.widgets import ForeignKeyRawIdWidget
from django.core.urlresolvers import reverse


class ForeignKeyRawIdLinkInNewTab(ForeignKeyRawIdWidget):
    """
    При клике на foreign key открывается эта модель в новой вкладке браузера
    """
    # Переписываем верстку raw_id_fields чтобы была ссылка на выходе
    def label_for_value(self, value):
        try:
            rel_to = self.rel.to

            related_url = reverse(
                'admin:%s_%s_changelist' % (
                    rel_to._meta.app_label,
                    rel_to._meta.model_name,
                ),
                current_app=self.admin_site.name,
            )
            url = '<a href="{}{}/" target="_blank" class="order_links">Посмотреть в новом окне</a>'.format(
                related_url,
                value
            )
            return url
        except ValueError:
            return ''

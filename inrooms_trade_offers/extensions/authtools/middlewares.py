import datetime
import re

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect

from history.models import HistoryItem

EXEMPT_URLS = [re.compile(settings.LOGIN_URL.lstrip('/'))]
if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
    EXEMPT_URLS += [re.compile(expr) for expr in settings.LOGIN_EXEMPT_URLS]


class LoginRequiredMiddleware(object):
    @staticmethod
    def process_request(request):
        if not request.user.is_authenticated():
            path = request.path_info.lstrip('/')
            if not any(m.match(path) for m in EXEMPT_URLS):
                if request.is_ajax():
                    raise PermissionDenied()
                else:
                    url = settings.LOGIN_URL
                    url += '?next={}'.format(path)
                    return HttpResponseRedirect(url)
        else:
            today = datetime.date.today()
            if request.user.last_login_date is None or request.user.last_login_date < today:
                request.user.last_login_date = today
                request.user.save()
                HistoryItem.objects.create(
                    kind=HistoryItem.KIND.user_login,
                    content_object=request.user,
                    body='Пользователь зашел в систему',
                )

from django.core.urlresolvers import reverse
from django.test import TestCase

from accounts.factories import UserFactory


class LoginRequireMisslewareTestCase(TestCase):
    def setUp(self):
        """ Create a user and log in """
        self.user = UserFactory(email='test@example.com')

    def test_login_require(self):
        response = self.client.get(reverse('core:home'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/login/?next=')

    def test_login_ok(self):
        self.client.login(email='test@example.com', password='password')
        response = self.client.get(reverse('core:home'))
        self.assertEqual(response.status_code, 200)

class MultiSerializerViewSetMixin(object):
    serializers = {
        'default': None
    }

    def get_serializer_class(self):
        serializer = self.serializers.get(self.action,
                                          self.serializers['default'])
        return serializer


class DynamicFieldsViewSetMixin(object):
    serializer_fields = {}

    def get_serializer(self, *args, **kwargs):
        kwargs['fields'] = self.serializer_fields.get(self.action, [])
        return super().get_serializer(*args, **kwargs)

from collections import defaultdict


class DynamicFieldsSerializerMixin(object):

    def __init__(self, *args, **kwargs):
        self.set_serializer_fields(kwargs.pop('fields', []))

        super().__init__(*args, **kwargs)

        if 'request' in self.context:
            request_fields = self.context['request'].query_params.get('_fields')
            if request_fields:
                self.set_serializer_fields(request_fields.split(','))

        if self._serializer_fields:
            allowed = set(self._serializer_fields.keys())
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)

    def set_serializer_fields(self, fields):
        f_dict = lambda: defaultdict(f_dict)
        self._serializer_fields = f_dict()

        if isinstance(fields, dict):
            self._serializer_fields = fields
        else:
            for field in fields:
                data = None
                for item in field.split('__'):
                    if data == None:
                        data = self._serializer_fields[item]
                    else:
                        data = data[item]

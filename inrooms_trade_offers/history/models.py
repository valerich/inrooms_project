from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

HISTORY_KIND_CHOICES = Choices(
    (1, 'payments', 'Оплата'),
    (2, 'user_login', 'Вход в систему'),
    (3, 'user_logout', 'Выход из системы'),
    (100, 'status_change', 'Смена статуса предложения'),
    (200, 'order_status_change', 'Смена статуса заказа'),
    (300, 'defect_act_status_change', 'Смена статуса акта на брак'),
    (400, 'refund_act_status_change', 'Смена статуса акта на возврат'),
    (500, 'invoice_create', 'Создание счета'),
)


class HistoryItem(TimeStampedModel):
    KIND = HISTORY_KIND_CHOICES
    kind = models.PositiveSmallIntegerField('Тип', choices=KIND)
    user = models.ForeignKey('accounts.User', verbose_name='Пользователь', blank=True, null=True)
    body = models.TextField('Сообщение')
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = 'Лог'
        verbose_name_plural = 'Логи'
        ordering = ('-created', )

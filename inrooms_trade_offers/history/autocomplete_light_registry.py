from autocomplete_light import shortcuts as autocomplete_light

from offers.models import Offer


class AutocompleteHistoryItems(autocomplete_light.AutocompleteGenericBase):
    choices = (
        Offer.objects.all(),
    )

    search_fields = (
        ('id', ),
    )

    attrs = {
        'data-autocomplete-minimum-characters': 0
    }


autocomplete_light.register(AutocompleteHistoryItems)

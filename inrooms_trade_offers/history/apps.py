from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'history'
    verbose_name = 'Логирование'

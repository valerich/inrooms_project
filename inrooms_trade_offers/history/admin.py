from django.contrib import admin

from .forms import HistoryItemAdminForm
from .models import HistoryItem


class HistoryItemAdmin(admin.ModelAdmin):
    list_display = ('user', 'created', 'modified', 'kind', 'content_object')
    list_filter = ('kind', )
    form = HistoryItemAdminForm


for model_or_iterable, admin_class in (
    (HistoryItem, HistoryItemAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

from rest_framework import serializers

from ...models import HistoryItem


class HistoryItemObjectRelatedField(serializers.RelatedField):
    """
    A custom field to use for the `tagged_object` generic relationship.
    """

    def to_representation(self, value):
        """
        Serialize tagged objects to a simple textual representation.
        """
        if isinstance(value, Product):
            return {
                'id': value.id,
                'name': value.name
            }
        raise Exception('Unexpected type of tagged object')


class HistoryItemSerializer(serializers.ModelSerializer):
    user_detail = serializers.SerializerMethodField()
    created = serializers.DateTimeField(read_only=True)
    modified = serializers.DateTimeField(read_only=True)
    kind_detail = serializers.SerializerMethodField()
    content_object_detail = serializers.SerializerMethodField()

    class Meta:
        model = HistoryItem

        fields = [
            'id',
            'created',
            'modified',
            'kind',
            'kind_detail',
            'body',
            'user_detail',
            # 'content_type',
            'content_object_detail',
        ]

    def __init__(self, *args, **kwargs):
        if 'request_user' in kwargs:
            self.request_user = kwargs.pop('request_user')
        super(HistoryItemSerializer, self).__init__(*args, **kwargs)

    def get_user_detail(self, obj):
        if obj.user:
            return {'id': obj.user_id,
                    'name': obj.user.name}
        else:
            return None

    def get_content_object_detail(self, obj):
        if obj.content_type.app_label == 'manufacturing' and obj.content_type.model == 'manufacturingorder':
            content_object_name = 'Заказ #{}'.format(obj.object_id)
        elif obj.content_type.app_label == 'orders' and obj.content_type.model == 'order':
            content_object_name = 'Бланк-заказ #{}'.format(obj.object_id)
        elif obj.content_type.app_label == 'orders' and obj.content_type.model == 'defectact':
            content_object_name = 'Акт о браке #{}'.format(obj.object_id)
        elif hasattr(obj.content_object, 'name'):
            content_object_name = obj.content_object.name
        else:
            return str(obj.content_object)
        return {
            'object': {
                'id': obj.object_id,
                'name': content_object_name, },
            'content_type': {
                'id': obj.content_type_id,
                'app_label': obj.content_type.app_label,
                'model': obj.content_type.model, }}

    def get_kind_detail(self, obj):
        return {
            'id': obj.kind,
            'name': obj.get_kind_display(),
        }

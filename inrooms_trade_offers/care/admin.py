from django.contrib import admin
from sorl.thumbnail.shortcuts import get_thumbnail

from .models import CareItem


class CareItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'kind', 'zara_code', 'weight', 'get_thumbnail_html')
    list_filter = ('kind', )

    def get_thumbnail_html(self, obj):
        img = obj.image1
        img_resize_url = None

        if img is not None and img.name:
            try:
                img_resize_url = str(get_thumbnail(img, '100x100').url)
            except IOError:
                pass

        if not img:
            return '<a class="image-picker" href=""><img src="" alt=""/></a>'
        html = '''<a class="image-picker" href="%s"><img src="%s"/></a>''' % (img.url, img_resize_url)
        return html

    get_thumbnail_html.short_description = 'Миниатюра'
    get_thumbnail_html.allow_tags = True


for model_or_iterable, admin_class in (
    (CareItem, CareItemAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

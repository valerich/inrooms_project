from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from ..serializers import CareItemSerializer
from ...models import CareItem


class CareItemViewSet(ModelViewSet):
    serializer_class = CareItemSerializer
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)
    filter_fields = ('kind', )
    search_fields = ('description', )
    ordering_fields = (
        'weight',
        'id',
    )
    queryset = CareItem.objects.all()

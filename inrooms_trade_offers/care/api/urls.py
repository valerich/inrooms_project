from django.conf.urls import include, url
from rest_framework import routers

from . import views

router = routers.SimpleRouter()

router.register(r'care_item', views.CareItemViewSet, base_name='care_item')


urlpatterns = [
    url(r'^', include(router.urls)),
]

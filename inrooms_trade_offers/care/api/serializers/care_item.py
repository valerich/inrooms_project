from rest_framework import serializers

from ...models import CareItem


class CareItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = CareItem

        fields = [
            'id',
            'description',
            'image1',
            'image2',
            'image3',
            'image4',
            'kind',
        ]

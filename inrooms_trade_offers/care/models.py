import os
import uuid

from django.db import models
from django_resized.forms import ResizedImageField

from model_utils.choices import Choices


def generate_care_item_image_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'care_item/fullsize/{}{}'.format(uuid.uuid4(), ext)


KIND_CHOICES = Choices(
    (1, 'wash', 'Стирка'),
    (2, 'bleach', 'Отбеливание'),
    (3, 'drying', 'Сушка'),
    (4, 'ironing', 'Глажение'),
    (5, 'professional_care', 'Профессиональный уход'),
)


class CareItem(models.Model):
    KIND = KIND_CHOICES
    kind = models.PositiveSmallIntegerField('Тип', choices=KIND)
    image1 = ResizedImageField('Изображение 1', upload_to=generate_care_item_image_filename, max_length=755, blank=True)
    image2 = ResizedImageField('Изображение 2', upload_to=generate_care_item_image_filename, max_length=755, blank=True)
    image3 = ResizedImageField('Изображение 3', upload_to=generate_care_item_image_filename, max_length=755, blank=True)
    image4 = ResizedImageField('Изображение 4', upload_to=generate_care_item_image_filename, max_length=755, blank=True)
    description = models.TextField('Описание', blank=True)
    weight = models.PositiveSmallIntegerField('Вес', help_text='Чем больше, тем выше при выборке', default=0)
    zara_code = models.CharField('Код зары', max_length=100, blank=True)

    class Meta:
        verbose_name = "Вариант ухода"
        verbose_name_plural = "Варианты уходов"
        ordering = ['kind', '-weight', ]

    def __str__(self):
        return "{}".format(self.id)


class CareMixin(models.Model):
    wash = models.ForeignKey(CareItem, verbose_name='Стирка', blank=True, null=True, related_name='%(class)s_wash', on_delete=models.PROTECT)

    bleach = models.ForeignKey(CareItem, verbose_name='Отбеливание', blank=True, null=True, related_name='%(class)s_bleach', on_delete=models.PROTECT)

    drying = models.ForeignKey(CareItem, verbose_name='Сушка', blank=True, null=True, related_name='%(class)s_drying', on_delete=models.PROTECT)

    ironing = models.ForeignKey(CareItem, verbose_name='Глажение', blank=True, null=True, related_name='%(class)s_ironing', on_delete=models.PROTECT)

    professional_care = models.ForeignKey(CareItem, verbose_name='Профессиональный уход за текстилем',
                                          blank=True, null=True, related_name='%(class)s_professional_care', on_delete=models.PROTECT)

    class Meta:
        abstract = True

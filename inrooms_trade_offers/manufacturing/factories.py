import factory
import factory.django
import factory.fuzzy

from accounts.factories import UserFactory
from factories.factories import FactoryFactory
from products.factories import ProductFactory
from .models import ManufacturingInternalLabelStatus, ManufacturingOrder, ManufacturingOrderItem, ManufacturingStatus


class ManufacturingInternalLabelStatusFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    code = factory.fuzzy.FuzzyText()

    class Meta:
        model = ManufacturingInternalLabelStatus


class ManufacturingStatusFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    code = factory.fuzzy.FuzzyText()

    class Meta:
        model = ManufacturingStatus


class ManufacturingOrderFactory(factory.django.DjangoModelFactory):
    status = factory.SubFactory(ManufacturingStatusFactory)
    creator = factory.SubFactory(UserFactory)
    performer = factory.SubFactory(UserFactory)
    factory = factory.SubFactory(FactoryFactory)

    class Meta:
        model = ManufacturingOrder


class ManufacturingOrderItemFactory(factory.django.DjangoModelFactory):
    order = factory.SubFactory(ManufacturingOrderFactory)
    product = factory.SubFactory(ProductFactory)
    price = factory.fuzzy.FuzzyInteger(low=10000)

    class Meta:
        model = ManufacturingOrderItem

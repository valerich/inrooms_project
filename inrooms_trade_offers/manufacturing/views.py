from django.shortcuts import get_object_or_404
from wkhtmltopdf.views import PDFTemplateView

from orders.models.order import Order
from products.models import Product
from .models import ManufacturingOrder


class ManufacturingOrderExportInternalLabels(PDFTemplateView,):
    template_name = 'manufacturing/manufacturing_order_pdf.html'
    filename = 'manufacturing_order.pdf'
    filename_docx = 'manufacturing_order.docx'

    def dispatch(self, request, object_id, *args, **kwargs):
        self.object = get_object_or_404(ManufacturingOrder, id=object_id)
        return super(ManufacturingOrderExportInternalLabels, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(ManufacturingOrderExportInternalLabels, self).get_context_data(*args, **kwargs)
        items = []
        items_part = []
        qs = self.object.items.all()
        qs = qs.select_related('product',
                               'product__color',
                               'product__country',
                               'product__brand',
                               'product__professional_care',
                               'product__ironing',
                               'product__drying',
                               'product__bleach',
                               'product__wash',
                               'product__consistency_type1',
                               'product__consistency_type2',
                               'product__consistency_type3',
                               'product__consistency_type4',
                               )
        qs = qs.prefetch_related('product__images')
        qs = qs.order_by('product__article', 'product__color__code')
        for item in qs:
            items_part.append(item)
            if item and len(items_part) >= 2:
                items.append(items_part)
                items_part = []
        if items_part:
            items.append(items_part)
        context['manufacturing_order'] = self.object
        context['items'] = items
        context['pages_count'] = len(items)
        return context

    def get_filename(self):
        return "Заказ на фабрику №{}.pdf".format(self.object.id)

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^manufacturing_order/(?P<object_id>\d+)/export_internal_labels/$',
        views.ManufacturingOrderExportInternalLabels.as_view(), name='manufacturing-order-export-internal-labels'),
]

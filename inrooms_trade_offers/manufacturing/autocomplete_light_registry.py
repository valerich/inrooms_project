from autocomplete_light import shortcuts as autocomplete_light

from .models import ManufacturingInternalLabelStatus, ManufacturingLabelOrder, ManufacturingLabelStatus, ManufacturingOrder, ManufacturingStatus


class ManufacturingInternalLabelStatusAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


class ManufacturingOrderAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['id', ]
    attrs = {
        'placeholder': 'Номер',
        'data-autocomplete-minimum-characters': 0,
    }


class ManufacturingLabelOrderAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['id', ]
    attrs = {
        'placeholder': 'Номер',
        'data-autocomplete-minimum-characters': 0,
    }


class ManufacturingStatusAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


class ManufacturingLabelStatusAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (ManufacturingInternalLabelStatus, ManufacturingInternalLabelStatusAutocomplete),
    (ManufacturingOrder, ManufacturingOrderAutocomplete),
    (ManufacturingStatus, ManufacturingStatusAutocomplete),
    (ManufacturingLabelOrder, ManufacturingLabelOrderAutocomplete),
    (ManufacturingLabelStatus, ManufacturingLabelStatusAutocomplete),
)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

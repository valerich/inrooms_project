from decimal import Decimal

from django.db import models
from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

from factories.models import Factory
from payments.models import PaymentDocument
from products.models import DOUBLE_SIZES, SERIES_TYPE_CHOICES, SIZES, LabelRemain, Size
from warehouses.models import Warehouse


class ManufacturingStatus(models.Model):
    name = models.CharField('Название', max_length=100, unique=True)
    code = models.CharField('Код', max_length=20, unique=True, db_index=True)
    button_name = models.CharField('Название на кнопке', max_length=100, blank=True)
    is_reserve_creator = models.BooleanField('Создать остатки при переходе в данный статус?', default=False)
    next_status = models.ManyToManyField('self', verbose_name=u'Следующие статусы', blank=True, symmetrical=False)
    weight = models.PositiveSmallIntegerField('Вес', default=0)
    check_internal_label_status_is_order_locked = models.BooleanField(
        'Проверять, что заказ не заблокирован статусом этикеток?',
        default=False)
    create_1c_document = models.BooleanField('Отправлять документ в 1с?', default=False)

    class Meta:
        verbose_name = 'Статус'
        verbose_name_plural = 'Статусы'
        ordering = ('-weight', 'name')

    def __str__(self):
        return "{} ({})".format(self.name, self.code)

    @staticmethod
    def get_default_status():
        return ManufacturingStatus.objects.get_or_create(code='new', defaults={'name': 'Новый'})[0].id


class ManufacturingLabelStatus(models.Model):
    name = models.CharField('Название', max_length=100, unique=True)
    code = models.CharField('Код', max_length=20, unique=True, db_index=True)
    button_name = models.CharField('Название на кнопке', max_length=100, blank=True)
    is_reserve_creator = models.BooleanField('Создать остатки при переходе в данный статус?', default=False)
    next_status = models.ManyToManyField('self', verbose_name=u'Следующие статусы', blank=True, symmetrical=False)
    weight = models.PositiveSmallIntegerField('Вес', default=0)

    class Meta:
        verbose_name = 'Статус этикеток'
        verbose_name_plural = 'Статусы этикеток'
        ordering = ('-weight', 'name')

    def __str__(self):
        return "{} ({})".format(self.name, self.code)

    @staticmethod
    def get_default_status():
        return ManufacturingStatus.objects.get_or_create(code='new', defaults={'name': 'Новый'})[0].id


class ManufacturingInternalLabelStatus(models.Model):
    name = models.CharField('Название', max_length=100, unique=True)
    code = models.CharField('Код', max_length=20, unique=True, db_index=True)
    button_name = models.CharField('Название на кнопке', max_length=100, blank=True)
    next_status = models.ManyToManyField('self', verbose_name=u'Следующие статусы', blank=True, symmetrical=False)
    weight = models.PositiveSmallIntegerField('Вес', default=0)
    is_order_locked = models.BooleanField('Проверять, что заказ не заблокирован статусом этикеток?', default=False)

    class Meta:
        verbose_name = 'Статус заказа внутренних этикеток'
        verbose_name_plural = 'Статусы заказов внутренних этикеток'
        ordering = ('-weight', 'name')

    def __str__(self):
        return "{} ({})".format(self.name, self.code)

    @staticmethod
    def get_default_status():
        return ManufacturingInternalLabelStatus.objects.get_or_create(code='new', defaults={'name': 'Новый'})[0].id


KIND_CHOICES = Choices(
    ('f', 'factory', 'На фабрику'),
    ('o', 'offer', 'По образцам'),
)


PRODUCT_KIND_CHOICES = Choices(
    (1, 'product', 'Модели'),
    (2, 'product_label', 'Этикетки'),
)


def get_default_warehouse():
    return Warehouse.objects.get_or_create(code='chn', defaults={'name': 'Склад Китая'})[0].id


class ManufacturingOrderMixin(TimeStampedModel):

    factory = models.ForeignKey(Factory, verbose_name='Фабрика', blank=True, null=True,
                                on_delete=models.PROTECT)
    description = models.TextField('Описание', blank=True)

    payment_method = models.ForeignKey('payments.PaymentMethod', verbose_name='Тип оплаты', blank=True, null=True,
                                       on_delete=models.PROTECT)
    amount_full = models.PositiveIntegerField('Полная стоимость', default=0)
    amount_paid = models.PositiveIntegerField('Оплаченная сумма', default=0)

    warehouse = models.ForeignKey(Warehouse, verbose_name='Склад', default=get_default_warehouse,
                                  on_delete=models.PROTECT)

    class Meta:
        abstract = True

    def __str__(self):
        return '№:{}'.format(self.id)


class ManufacturingOrder(ManufacturingOrderMixin):
    KIND = KIND_CHOICES
    kind = models.CharField('Тип заказа', max_length=1, choices=KIND, default=KIND.offer)
    status = models.ForeignKey(ManufacturingStatus, verbose_name='Статус', default=ManufacturingStatus.get_default_status,
                               on_delete=models.PROTECT)
    internal_label_status = models.ForeignKey(ManufacturingInternalLabelStatus, verbose_name='Статус заказа внутренних этикеток', default=ManufacturingInternalLabelStatus.get_default_status,
                                              on_delete=models.PROTECT)
    creator = models.ForeignKey('accounts.User', verbose_name='Создатель', related_name='manufacturing_order_creator',
                                on_delete=models.PROTECT)
    performer = models.ForeignKey('accounts.User', verbose_name='Исполнитель', related_name='manufacturing_order_performer',
                                  blank=True, null=True, on_delete=models.PROTECT)
    payment_account = models.ForeignKey('payments.PaymentAccount', verbose_name='Счет оплаты',
                                        blank=True, null=True,
                                        related_name='manufacturing_order_payment_accounts')
    internal_label_payment_account = models.ForeignKey('payments.PaymentAccount', verbose_name='Счет оплаты', blank=True, null=True, related_name='manufacturing_order_internal_label_payment_account')
    amount_received = models.DecimalField('Cумма получено', default=Decimal('0.00'), max_digits=20, decimal_places=10)
    transport_costs = models.DecimalField('Транспортные расходы', default=Decimal('0.00'), max_digits=20, decimal_places=10)
    refund_payment_account = models.ForeignKey('payments.PaymentAccount', verbose_name='Счет оплаты',
                                               blank=True, null=True,
                                               related_name='manufacturing_order_refund_payment_accounts')
    refund_transport_costs = models.DecimalField('Транспортные расходы на возврат', default=Decimal('0.00'), max_digits=20, decimal_places=10)
    is_sawing_labels = models.BooleanField('Требуется ли пошив этикеток', default=False)
    amount_sawing_labels = models.DecimalField('Перешив этикеток (юани)', default=Decimal('0.00'), max_digits=20, decimal_places=10)
    amount_sawing_labels_rur = models.DecimalField('Перешив этикеток (рубли)', default=Decimal('0.00'), max_digits=20, decimal_places=10)
    prepayment = models.DecimalField('Аванс (юани)', default=Decimal('0.00'), max_digits=20, decimal_places=10)
    prepayment_rur = models.DecimalField('Аванс (рубли)', default=Decimal('0.00'), max_digits=20, decimal_places=10)

    class Meta:
        verbose_name = 'Заказ на фабрику'
        verbose_name_plural = 'Заказы на фабрику'
        ordering = ('-id', )

    # def process_order(self):
    #     from products.models import Remain
    #
    #     if self.status.is_reserve_creator:
    #         for item in self.items.all():
    #             if not item.remains_created:
    #                 with transaction.atomic():
    #                     for size in Size.objects.all():
    #                         received = getattr(item, 'received_{}_count'.format(size.code), 0)
    #                         if received:
    #                             remain, created = Remain.objects.get_or_create(
    #                                 warehouse=self.warehouse,
    #                                 product=item.product,
    #                                 size=size,
    #                                 defaults={
    #                                     'value': received,
    #                                 }
    #                             )
    #                             if not created:
    #                                 remain.value += received
    #                                 remain.save()
    #                     item.remains_created = True
    #                     item.save()

    def get_ordered_amount(self):
        amount = 0
        for item in self.items.all():
            amount += item.get_ordered_amount()
        return amount

    # def get_received_amount(self):
    #     amount = 0
    #     for item in self.items.all():
    #         amount += item.get_received_amount()
    #     return amount

    def get_received_count(self):
        received_count = 0
        for item in self.items.all():
            received_count += item.get_received_count()
        return received_count

    def get_avg_amount(self):
        return self.get_full_amount() / self.get_received_count()

    def get_full_amount(self):
        return self.amount_received + self.transport_costs

    def get_internal_label_amount_full(self):
        amount_full = Decimal('0.00')
        for i in self.items.all():
            amount_full += i.internal_label_price
        return amount_full

    def get_item_transport_costs(self):
        received_count = self.get_received_count()
        if received_count:
            item_transport_costs = self.transport_costs / received_count
        else:
            item_transport_costs = Decimal('0.0000000000')
        return item_transport_costs

    def get_one_item_received_price(self):
        received_count = self.get_received_count()
        if received_count:
            amount = self.amount_received + self.transport_costs
            return amount / self.get_received_count()
        else:
            return Decimal('0.0000000000')

    def process_order(self, create_1c_document=True):
        if self.status.create_1c_document and create_1c_document:
            self.create_1c_document()

    def create_1c_document(self):
        """Создание документа для синхронизации с 1с"""

        from service_1c.utils.manufacturing_order_exporter import ManufacturingOrderExporter
        document = ManufacturingOrderExporter(self.id).create_document()
        return document


class ManufacturingLabelOrder(ManufacturingOrderMixin):
    status = models.ForeignKey(ManufacturingLabelStatus, verbose_name='Статус', default=ManufacturingLabelStatus.get_default_status,
                               on_delete=models.PROTECT)
    creator = models.ForeignKey('accounts.User', verbose_name='Создатель', related_name='manufacturing_label_order_creator',
                                on_delete=models.PROTECT)
    performer = models.ForeignKey('accounts.User', verbose_name='Исполнитель', related_name='manufacturing_label_order_performer',
                                  blank=True, null=True, on_delete=models.PROTECT)
    payment_account = models.ForeignKey('payments.PaymentAccount', verbose_name='Счет оплаты', blank=True, null=True)
    transport_costs = models.DecimalField('Транспортные расходы', default=Decimal('0.00'), max_digits=20, decimal_places=10)

    class Meta:
        verbose_name = 'Заказ этикеток на фабрику'
        verbose_name_plural = 'Заказы этикеток на фабрику'
        ordering = ('-id', )

    def get_ordered_amount(self):
        amount = 0
        for item in self.items.all():
            amount += item.get_ordered_amount()
        return amount

    def get_received_amount(self):
        amount = 0
        for item in self.items.all():
            amount += item.get_received_amount()
        return amount

    def get_received_count(self):
        received_count = 0
        for item in self.items.all():
            received_count += item.get_received_count()
        return received_count

    def get_avg_amount(self):
        return self.get_full_amount() / self.get_received_count()

    def get_full_amount(self):
        return self.get_received_amount() + self.transport_costs

    def get_item_transport_costs(self):
        received_count = self.get_received_count()
        if received_count:
            item_transport_costs = self.transport_costs / received_count
        else:
            item_transport_costs = Decimal('0.0000000000')
        return item_transport_costs


class ManufacturingOrderItem(models.Model):
    SERIES_TYPE = SERIES_TYPE_CHOICES
    order = models.ForeignKey(ManufacturingOrder, verbose_name='Заказ на фабрику', related_name="items",
                              on_delete=models.PROTECT)
    product = models.ForeignKey('products.Product', verbose_name='Товар', on_delete=models.PROTECT)

    quantity = models.PositiveSmallIntegerField('Количество', default=1)
    series_type = models.CharField('Тип серии', max_length=1, choices=SERIES_TYPE)
    price = models.DecimalField('Цена', default=Decimal('0.00'), max_digits=20, decimal_places=10)
    internal_label_price = models.DecimalField('Цена за внутренние этикетки', max_digits=20, decimal_places=10, default=Decimal('0.00'))

    remains_created = models.BooleanField('Резерв создан?', default=False)

    has_xxs = models.BooleanField('XXS', default=False)
    has_xs = models.BooleanField('XS', default=False)
    has_s = models.BooleanField('S', default=False)
    has_m = models.BooleanField('M', default=False)
    has_l = models.BooleanField('L', default=False)
    has_xl = models.BooleanField('XL', default=False)
    has_xxl = models.BooleanField('XXL', default=False)
    has_xxxl = models.BooleanField('XXXL', default=False)
    has_34 = models.BooleanField('34', default=False)
    has_35 = models.BooleanField('35', default=False)
    has_36 = models.BooleanField('36', default=False)
    has_37 = models.BooleanField('37', default=False)
    has_38 = models.BooleanField('38', default=False)
    has_39 = models.BooleanField('39', default=False)
    has_40 = models.BooleanField('40', default=False)
    has_41 = models.BooleanField('41', default=False)
    received_xxs_count = models.PositiveSmallIntegerField('Получено XXS', default=0)
    received_xs_count = models.PositiveSmallIntegerField('Получено XS', default=0)
    received_s_count = models.PositiveSmallIntegerField('Получено S', default=0)
    received_m_count = models.PositiveSmallIntegerField('Получено M', default=0)
    received_l_count = models.PositiveSmallIntegerField('Получено L', default=0)
    received_xl_count = models.PositiveSmallIntegerField('Получено XL', default=0)
    received_xxl_count = models.PositiveSmallIntegerField('Получено XXL', default=0)
    received_xxxl_count = models.PositiveSmallIntegerField('Получено XXXL', default=0)
    received_34_count = models.PositiveSmallIntegerField('Получено 34', default=0)
    received_35_count = models.PositiveSmallIntegerField('Получено 35', default=0)
    received_36_count = models.PositiveSmallIntegerField('Получено 36', default=0)
    received_37_count = models.PositiveSmallIntegerField('Получено 37', default=0)
    received_38_count = models.PositiveSmallIntegerField('Получено 38', default=0)
    received_39_count = models.PositiveSmallIntegerField('Получено 39', default=0)
    received_40_count = models.PositiveSmallIntegerField('Получено 40', default=0)
    received_41_count = models.PositiveSmallIntegerField('Получено 41', default=0)
    received_amount = models.DecimalField('Сумма получено', default=Decimal('0.00'), max_digits=20, decimal_places=10)

    label_main = models.ForeignKey('products.ProductLabel', verbose_name='Этикетка основная',
                                   blank=True, null=True, related_name='manufacturing_main_labels')
    label_main_xxs_count = models.PositiveSmallIntegerField('XXS', default=0)
    label_main_xs_count = models.PositiveSmallIntegerField('XS', default=0)
    label_main_s_count = models.PositiveSmallIntegerField('S', default=0)
    label_main_m_count = models.PositiveSmallIntegerField('M', default=0)
    label_main_l_count = models.PositiveSmallIntegerField('L', default=0)
    label_main_xl_count = models.PositiveSmallIntegerField('XL', default=0)
    label_main_xxl_count = models.PositiveSmallIntegerField('XXL', default=0)
    label_main_xxxl_count = models.PositiveSmallIntegerField('XXXL', default=0)
    label_main_summ_rur = models.DecimalField('Стоимость основных этикеток (р)', default=Decimal('0.00'), max_digits=20, decimal_places=10)

    label_internal = models.ForeignKey('products.ProductLabel', verbose_name='Этикетка внутренняя',
                                       blank=True, null=True, related_name='manufacturing_internal_labels')
    label_internal_count = models.PositiveSmallIntegerField('Количество врутренних этикеток', default=0)
    label_internal_summ_rur = models.DecimalField('Стоимость врутренних этикеток (р)', default=Decimal('0.00'), max_digits=20, decimal_places=10)

    label_internal2 = models.ForeignKey('products.ProductLabel', verbose_name='Этикетка внутренняя',
                                       blank=True, null=True, related_name='manufacturing_internal_labels2')
    label_internal2_count = models.PositiveSmallIntegerField('Количество врутренних этикеток', default=0)
    label_internal2_summ_rur = models.DecimalField('Стоимость врутренних этикеток (р)', default=Decimal('0.00'), max_digits=20, decimal_places=10)


    label_cardboard = models.ForeignKey('products.ProductLabel', verbose_name='Этикетка картон',
                                        blank=True, null=True, related_name='manufacturing_cardboard_labels')
    label_cardboard_count = models.PositiveSmallIntegerField('Количество картонных этикеток', default=0)
    label_cardboard_summ_rur = models.DecimalField('Стоимость картонных этикеток (р)', default=Decimal('0.00'), max_digits=20, decimal_places=10)
    has_offer = models.BooleanField('Добавить предложение', default=False)

    class Meta:
        verbose_name = 'Товар в заказе'
        verbose_name_plural = 'Товары в заказе'
        ordering = ('-id', )
        unique_together = ('order', 'product')

    def get_full_amount(self, item_transport_costs=None):
        summ = self.get_received_amount()
        if item_transport_costs is None:
            item_transport_costs = self.order.get_item_transport_costs()
        received_count = self.get_received_count()
        summ += item_transport_costs * received_count
        # summ += self.label_main_summ_rur + self.label_internal_summ_rur + self.label_cardboard_summ_rur
        return summ

    def get_avg_amount(self, item_transport_costs=None):
        received_count = self.get_received_count()
        if received_count:
            return self.get_full_amount(item_transport_costs) / received_count
        else:
            return Decimal('0.0000000000')

    def reserve_labels(self, warehouse):
        for label_type in ['main', 'internal', 'internal2', 'cardboard']:
            label = getattr(self, 'label_{}'.format(label_type))
            if label_type == 'main':
                label_summ = Decimal('0.0000000000')
                for size in Size.objects.filter(series_type=Size.SERIES_TYPE.clothes):
                    label_count = getattr(self, 'label_main_{}_count'.format(size.code))
                    if label and label_count:
                        value, amount_rur = LabelRemain.remove_remain(label, warehouse, label_count, size=size)
                        label_summ += amount_rur
                setattr(self, 'label_{}_summ_rur'.format(label_type), label_summ)
            else:
                label_count = getattr(self, 'label_{}_count'.format(label_type))
                if label and label_count:
                    value, amount_rur = LabelRemain.remove_remain(label, warehouse, label_count, size=None)
                    setattr(self, 'label_{}_summ_rur'.format(label_type), amount_rur)

    def get_ordered_amount(self):
        count = self.get_ordered_size_quantity()
        return self.price * count

    def get_ordered_size_quantity(self):
        count = 0
        for size in SIZES:
            if getattr(self, 'has_{}'.format(size), 0):
                if size in DOUBLE_SIZES:
                    count += self.quantity * 2
                else:
                    count += self.quantity
        return count

    def get_received_amount(self):
        return self.price * self.get_received_count()

    def get_received_count(self):
        count = 0
        for size in SIZES:
            count += getattr(self, 'received_{}_count'.format(size), 0)
        return count


class ManufacturingLabelOrderItem(models.Model):
    order = models.ForeignKey(ManufacturingLabelOrder, verbose_name='Заказ на фабрику', related_name="items",
                              on_delete=models.PROTECT)
    product_label = models.ForeignKey('products.ProductLabel', verbose_name='Этикетка', on_delete=models.PROTECT)

    price = models.DecimalField('Цена', max_digits=20, decimal_places=10, default=Decimal('0.00'))

    remains_created = models.BooleanField('Резерв создан?', default=False)

    # Этикетки с типом "основная" заказываются по размерам (ordered_{})
    # Остальные без размера (ordered)
    ordered = models.PositiveSmallIntegerField('Заказано', default=0)
    ordered_xxs = models.PositiveSmallIntegerField('Заказано XXS', default=0)
    ordered_xs = models.PositiveSmallIntegerField('Заказано XS', default=0)
    ordered_s = models.PositiveSmallIntegerField('Заказано S', default=0)
    ordered_m = models.PositiveSmallIntegerField('Заказано M', default=0)
    ordered_l = models.PositiveSmallIntegerField('Заказано L', default=0)
    ordered_xl = models.PositiveSmallIntegerField('Заказано XL', default=0)
    ordered_xxl = models.PositiveSmallIntegerField('Заказано XXL', default=0)
    ordered_xxxl = models.PositiveSmallIntegerField('Заказано XXXL', default=0)

    received = models.PositiveSmallIntegerField('Получено', default=0)
    received_xxs = models.PositiveSmallIntegerField('Получено XXS', default=0)
    received_xs = models.PositiveSmallIntegerField('Получено XS', default=0)
    received_s = models.PositiveSmallIntegerField('Получено S', default=0)
    received_m = models.PositiveSmallIntegerField('Получено M', default=0)
    received_l = models.PositiveSmallIntegerField('Получено L', default=0)
    received_xl = models.PositiveSmallIntegerField('Получено XL', default=0)
    received_xxl = models.PositiveSmallIntegerField('Получено XXL', default=0)
    received_xxxl = models.PositiveSmallIntegerField('Получено XXXL', default=0)

    class Meta:
        verbose_name = 'Этикетка в заказе'
        verbose_name_plural = 'Этикетки в заказе'
        ordering = ('-id', )
        unique_together = ('order', 'product_label')

    def get_full_amount(self, item_transport_costs=None):
        summ = self.get_received_amount()
        if item_transport_costs is None:
            item_transport_costs = self.order.get_item_transport_costs()
        received_count = self.get_received_count()
        summ += item_transport_costs * received_count
        return summ

    def get_avg_amount(self, item_transport_costs=None):
        received_count = self.get_received_count()
        if received_count:
            return self.get_full_amount(item_transport_costs) / received_count
        else:
            return Decimal('0.0000000000')

    def get_ordered_amount(self):
        count = 0
        for field in [field_name for field_name in dir(self) if field_name.startswith('ordered')]:
            count += getattr(self, field, 0)
        return self.price * count

    def get_received_amount(self):
        return self.price * self.get_received_count()

    def get_received_count(self):
        count = 0
        for field in [field_name for field_name in dir(self) if field_name.startswith('received')]:
            count += getattr(self, field, 0)
        return count


def order_item_set_fields_by_product(sender, instance, **kwargs):
    if instance.product_id:
        if not instance.series_type:
            instance.series_type = instance.product.main_collection.series_type
        if instance.price is None:
            if instance.product.offer and instance.product.offer.price is not None:
                instance.price = instance.product.offer.price
            else:
                instance.price = 0


models.signals.pre_save.connect(order_item_set_fields_by_product, sender=ManufacturingOrderItem)

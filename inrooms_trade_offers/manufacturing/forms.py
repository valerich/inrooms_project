from autocomplete_light import shortcuts as autocomplete_light
from django import forms
from django_summernote.widgets import SummernoteWidget


class ManufacturingInternalLabelStatusAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'next_status': autocomplete_light.MultipleChoiceWidget('ManufacturingInternalLabelStatusAutocomplete'),
        }


class ManufacturingOrderItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete')
        }


class ManufacturingLabelOrderItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'product_label': autocomplete_light.ChoiceWidget('ProductLabelAutocomplete')
        }


class ManufacturingStatusAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'next_status': autocomplete_light.MultipleChoiceWidget('ManufacturingStatusAutocomplete'),
        }


class ManufacturingLabelStatusAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'next_status': autocomplete_light.MultipleChoiceWidget('ManufacturingLabelStatusAutocomplete'),
        }


class ManufacturingOrderAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'creator': autocomplete_light.ChoiceWidget('UserAutocomplete'),
            'description': SummernoteWidget(),
            'performer': autocomplete_light.ChoiceWidget('UserAutocomplete'),
            'status': autocomplete_light.ChoiceWidget('ManufacturingStatusAutocomplete'),
            'factory': autocomplete_light.ChoiceWidget('FactoryAutocomplete'),
        }


class ManufacturingLabelOrderAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'creator': autocomplete_light.ChoiceWidget('UserAutocomplete'),
            'description': SummernoteWidget(),
            'performer': autocomplete_light.ChoiceWidget('UserAutocomplete'),
            'status': autocomplete_light.ChoiceWidget('ManufacturingLabelStatusAutocomplete'),
            'factory': autocomplete_light.ChoiceWidget('FactoryAutocomplete'),
        }

from decimal import Decimal
from django.utils import timezone
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from history.models import HistoryItem
from manufacturing.api_public.serializers.manufacturing_order_refund import ManufacturingOrderRefundSerializer
from manufacturing.models import ManufacturingOrderItem, ManufacturingStatus
from payments.models import ExpenseItem, PaymentDocument


class ViewException(Exception):
    pass


class ManufacturingOrderRefundView(APIView):

    def post(self, request, *args, **kwargs):
        serializer_class = ManufacturingOrderRefundSerializer
        self.serializer = serializer_class(data=request.data)
        self.serializer.is_valid(True)
        self.manufacturing_order = self.serializer.validated_data['manufacturing_order']
        try:
            self.update_order()
            self.payment()
        except ViewException as e:
            return Response(status=HTTP_400_BAD_REQUEST, data={
                'error': str(e)
            })
        return Response(status=HTTP_200_OK)

    def update_order(self):
        refund_status = ManufacturingStatus.objects.get(code='refund')
        self.manufacturing_order.status = refund_status
        self.manufacturing_order.refund_transport_costs = self.serializer.validated_data['refund_transport_costs']
        self.manufacturing_order.refund_payment_account = self.serializer.validated_data['refund_payment_account']
        self.manufacturing_order.save()

    def payment(self):
        amount_chn = self.manufacturing_order.amount_received - self.serializer.validated_data.get('refund_transport_costs', 0)
        amount_rur = self.manufacturing_order.refund_payment_account.get_amount_rur(amount_chn)
        expense_item = ExpenseItem.get_manufacturing_order_refund()
        payment_document = PaymentDocument(
            kind=PaymentDocument.KIND.credit,
            payment_account=self.manufacturing_order.refund_payment_account,
            destination_payment_account=self.manufacturing_order.refund_payment_account,
            amount=amount_chn,
            amount_rur=amount_rur,
            user=self.request.user,
            description="Возврат заказа на фабрику №{}".format(self.manufacturing_order.id),
            expense_item=expense_item,
            content_object=self.manufacturing_order,
        )

        history_item = HistoryItem.objects.create(
            kind=HistoryItem.KIND.payments,
            content_object=self.manufacturing_order,
            body="Возврат заказа на фабрику {} {} ю. {} руб.".format(
                timezone.now().strftime('%d.%m.%Y'),
                amount_chn,
                amount_rur,
            ),
        )

        payment_document.save()
        payment_document.process()

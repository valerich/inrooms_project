from decimal import Decimal
from django.utils import timezone
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from history.models import HistoryItem
from manufacturing.api_public.serializers.manufacturing_order_acceptance import ManufacturingOrderAcceptanceSerializer
from manufacturing.models import ManufacturingOrderItem
from payments.models import ExpenseItem, PaymentDocument


class ViewException(Exception):
    pass


class ManufacturingOrderAcceptanceView(APIView):

    def post(self, request, *args, **kwargs):
        serializer_class = ManufacturingOrderAcceptanceSerializer
        self.serializer = serializer_class(data=request.data)
        self.serializer.is_valid(True)
        self.manufacturing_order = self.serializer.validated_data['manufacturing_order']
        try:
            self.check_balance()
            self.update_order()
            self.set_price()
            self.payment()
        except ViewException as e:
            return Response(status=HTTP_400_BAD_REQUEST, data={
                'error': str(e)
            })
        return Response(status=HTTP_200_OK, data={
            'amount_full_rur': str(self.serializer.validated_data['payment_account'].get_amount_rur(self.serializer.validated_data.get('amount_full_chn', 0)))
        })

    def check_balance(self):
        manufacturing_order = self.serializer.validated_data['manufacturing_order']
        payment_account = self.serializer.validated_data['payment_account']
        amount_received = self.serializer.validated_data['amount_received']

        full_amount = manufacturing_order.transport_costs + amount_received - manufacturing_order.prepayment
        if payment_account.balance < full_amount:
            raise ViewException('Не достаточно средств на счете')

    def update_order(self):
        self.manufacturing_order.payment_account = self.serializer.validated_data['payment_account']
        self.manufacturing_order.amount_received = self.serializer.validated_data['amount_received']
        self.manufacturing_order.transport_costs = self.serializer.validated_data.get('transport_costs', Decimal('0.00'))
        self.manufacturing_order.status = self.serializer.validated_data['status']
        self.manufacturing_order.amount_full = self.manufacturing_order.get_full_amount()
        self.manufacturing_order.save()
        ManufacturingOrderItem.objects.filter(order=self.manufacturing_order).update(
            received_xxs_count=0,
            received_xs_count=0,
            received_s_count=0,
            received_m_count=0,
            received_l_count=0,
            received_xl_count=0,
            received_xxl_count=0,
            received_xxxl_count=0,
            received_34_count=0,
            received_35_count=0,
            received_36_count=0,
            received_37_count=0,
            received_38_count=0,
            received_39_count=0,
            received_40_count=0,
            received_41_count=0,
        )
        for item in self.serializer.validated_data['items']:
            order_item, c = ManufacturingOrderItem.objects.get_or_create(
                order=self.manufacturing_order,
                product=item['product'],
            )
            for received in item['received']:
                setattr(order_item, 'received_{}_count'.format(received['size'].code), received['quantity'])
            order_item.save()

    def set_price(self):
        received_count = self.manufacturing_order.get_received_count()
        if received_count:
            one_item_received_price = self.manufacturing_order.get_one_item_received_price()
            ManufacturingOrderItem.objects.filter(order=self.manufacturing_order).update(price=one_item_received_price)
        else:
            ManufacturingOrderItem.objects.filter(order=self.manufacturing_order).update(price=0)

    def payment(self):
        amount_received_chn = self.serializer.validated_data.get('amount_full_chn', 0)
        amount_received_rur = self.manufacturing_order.payment_account.get_amount_rur(amount_received_chn)
        expense_item = ExpenseItem.get_manufacturing_order_payment()
        payment_document = PaymentDocument(
            kind=PaymentDocument.KIND.debit,
            payment_account=self.manufacturing_order.payment_account,
            destination_payment_account=self.manufacturing_order.payment_account,
            amount=amount_received_chn - self.manufacturing_order.prepayment,
            amount_rur=amount_received_rur - self.manufacturing_order.prepayment_rur,
            user=self.request.user,
            description="Оплата заказа на фабрику №{}".format(self.manufacturing_order.id),
            expense_item=expense_item,
            content_object=self.manufacturing_order,
        )

        history_item = HistoryItem.objects.create(
            kind=HistoryItem.KIND.payments,
            content_object=self.manufacturing_order,
            body="Оплата заказа на фабрику {} {} ю. {} руб.".format(
                timezone.now().strftime('%d.%m.%Y'),
                amount_received_chn - self.manufacturing_order.prepayment,
                amount_received_rur - self.manufacturing_order.prepayment_rur,
            ),
        )

        payment_document.save()
        payment_document.process()

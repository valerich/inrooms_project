from rest_framework import serializers

from manufacturing.models import ManufacturingOrder, ManufacturingStatus
from payments.models import PaymentAccount
from products.models import Size, Product


class ItemReceivedSerializer(serializers.Serializer):
    size = serializers.SlugRelatedField(queryset=Size.objects.all(), slug_field='code_1c')
    quantity = serializers.IntegerField()

    class Meta:
        fields = (
            'size',
            'quantity',
        )


class ManufacturingOrderItemSerializer(serializers.Serializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())
    received = ItemReceivedSerializer(many=True)

    class Meta:
        fields = (
            'product',
            'received',
        )


class ManufacturingOrderAcceptanceSerializer(serializers.Serializer):
    manufacturing_order = serializers.PrimaryKeyRelatedField(queryset=ManufacturingOrder.objects.all())
    status = serializers.SlugRelatedField(queryset=ManufacturingStatus.objects.all(), slug_field='code')
    items = ManufacturingOrderItemSerializer(many=True)
    amount_received = serializers.DecimalField(max_digits=20, decimal_places=10)
    transport_costs = serializers.DecimalField(max_digits=20, decimal_places=10, required=False)
    payment_account = serializers.PrimaryKeyRelatedField(
        queryset=PaymentAccount.objects.filter(currency=PaymentAccount.CURRENCY.CNY))
    amount_full_chn = serializers.DecimalField(max_digits=20, decimal_places=10, required=False)

    class Meta:
        fields = (
            'manufacturing_order',
            'status',
            'received_items',
            'amount_received',
            'payment_account_id',
        )

from rest_framework import serializers

from manufacturing.models import ManufacturingOrder, ManufacturingStatus
from payments.models import PaymentAccount
from products.models import Size, Product


class ManufacturingOrderRefundSerializer(serializers.Serializer):
    manufacturing_order = serializers.PrimaryKeyRelatedField(queryset=ManufacturingOrder.objects.all())
    refund_transport_costs = serializers.DecimalField(max_digits=20, decimal_places=10, required=False)
    refund_payment_account = serializers.PrimaryKeyRelatedField(
        queryset=PaymentAccount.objects.filter(currency=PaymentAccount.CURRENCY.CNY))

    class Meta:
        fields = (
            'manufacturing_order',
            'refund_transport_costs',
            'refund_payment_account',
        )

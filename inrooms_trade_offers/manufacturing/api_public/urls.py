from django.conf import urls

from .views import ManufacturingOrderAcceptanceView, ManufacturingOrderRefundView


urlpatterns = [
    urls.url(r'manufacturing_order/acceptance/$', ManufacturingOrderAcceptanceView.as_view(),
             name='manufacturing_order_acceptance'),
    urls.url(r'manufacturing_order/refund/$', ManufacturingOrderRefundView.as_view(),
             name='manufacturing_order_refund'),
]

from django.test import TestCase

from ..factories import ManufacturingInternalLabelStatusFactory, ManufacturingOrderFactory, ManufacturingOrderItemFactory, ManufacturingStatusFactory
from ..models import ManufacturingInternalLabelStatus, ManufacturingOrder, ManufacturingOrderItem, ManufacturingStatus


class ManufacturingInternalLabelStatusFactoryTestCase(TestCase):
    def setUp(self):
        ManufacturingInternalLabelStatus.objects.all().delete()

    def test_factory(self):
        object = ManufacturingInternalLabelStatusFactory()
        object_from_db = ManufacturingInternalLabelStatus.objects.all()[0]
        self.assertEqual(object, object_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(ManufacturingInternalLabelStatusFactory())

        self.assertEqual(len(objects), len(ManufacturingInternalLabelStatus.objects.all()))


class ManufacturingOrderFactoryTestCase(TestCase):
    def setUp(self):
        ManufacturingOrder.objects.all().delete()

    def test_factory(self):
        object = ManufacturingOrderFactory()
        object_from_db = ManufacturingOrder.objects.all()[0]
        self.assertEqual(object, object_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(ManufacturingOrderFactory())

        self.assertEqual(len(objects), len(ManufacturingOrder.objects.all()))


class ManufacturingOrderItemFactoryTestCase(TestCase):
    def setUp(self):
        ManufacturingOrderItem.objects.all().delete()

    def test_factory(self):
        object = ManufacturingOrderItemFactory()
        object_from_db = ManufacturingOrderItem.objects.all()[0]
        self.assertEqual(object, object_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(ManufacturingOrderItemFactory())

        self.assertEqual(len(objects), len(ManufacturingOrderItem.objects.all()))


class ManufacturingStatusFactoryTestCase(TestCase):
    def setUp(self):
        ManufacturingStatus.objects.all().delete()

    def test_factory(self):
        object = ManufacturingStatusFactory()
        object_from_db = ManufacturingStatus.objects.all()[0]
        self.assertEqual(object, object_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(ManufacturingStatusFactory())

        self.assertEqual(len(objects), len(ManufacturingStatus.objects.all()))

from decimal import Decimal

from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from manufacturing.factories import ManufacturingOrderFactory, ManufacturingOrderItemFactory
from manufacturing.models import ManufacturingOrder
from payments.factories import PaymentAccountFactory
from products.factories import ProductFactory


class ManufacturingOrderViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(ManufacturingOrderViewTestCase, cls).setUpClass()
        cls.order_url = reverse('api:manufacturing:order-list')

        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')
        cls.product = ProductFactory()
        payment_account = PaymentAccountFactory(balance=1000, balance_rur=10000)
        cls.order = ManufacturingOrderFactory(creator=cls.user_bm, payment_account=payment_account)
        ManufacturingOrderFactory(creator=cls.user_pa)

        cls.valid_create_order_data = {}

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_create_order(self):
        response = self.client_bm.post(self.order_url, self.valid_create_order_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client_pa.post(self.order_url, self.valid_create_order_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_list_order(self):
        response = self.client_bm.get(self.order_url)
        self.assertEqual(response.data['count'], 2)
        response = self.client_pa.get(self.order_url)
        self.assertEqual(response.data['count'], 2)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client_admin.get(self.order_url)
        order_dict = response.data['results'][0]
        fields = [
            'id',
            'factory_detail.name',
            'created',
            'modified',
            'status_detail.name',
            'amount_full',
            'kind',
            'kind_display',
            'items',
        ]
        for field in fields:
            try:
                field_container = order_dict
                for i in field.split('.'):
                    field_container = field_container[i]
            except (KeyError, TypeError):
                self.fail('Не передается поле {} в {}'.format(field, self.order_url))

    def test_list_sort_by_fields(self):
        """Проверяем, что api сортируется по всем необходимым полям"""

        fields = [
            'id',
            'factory__name',
            'created',
            'modified',
        ]
        for field in fields:
            for prefix in ['', '-']:
                filter_field = "{}{}".format(prefix, field)
                response = self.client_admin.get(self.order_url, {'sort_by': filter_field})
                order_dict = response.data['results'][0]
                order = ManufacturingOrder.objects.all().order_by(filter_field)[0]
                self.assertEqual(order_dict['id'], getattr(order, 'id'), 'Не сортируется по полю {} в {}'.format(filter_field, self.order_url))

    def test_change_status(self):
        change_status_url = reverse('api:manufacturing:order-change-status', args=[self.order.id, ])
        response = self.client_admin.post(change_status_url, {'code': 'sent_to_factory'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_change_status_received(self):
        """Мы не можем менять статус заказа на "Получен" до тех пор, пока не будет получено хотя бы одного товара

        https://bitbucket.org/valerich/inrooms_project/issues/109/--------------------
        """
        manufacturing_order_item = ManufacturingOrderItemFactory(
            order=self.order,
            product=self.product,
            price=100,
        )
        self.order.amount_received = Decimal('10.0')
        self.order.save()

        change_status_url = reverse('api:manufacturing:order-change-status', args=[self.order.id, ])
        response = self.client_admin.post(change_status_url, {'code': 'received'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        manufacturing_order_item.received_xxl_count = 1
        manufacturing_order_item.save()

        response = self.client_admin.post(change_status_url, {'code': 'received'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

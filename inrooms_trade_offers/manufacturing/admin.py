from django.contrib import admin

from core.admin import NotDeleteModelAdmin
from .forms import (
    ManufacturingInternalLabelStatusAdminForm,
    ManufacturingLabelOrderAdminForm,
    ManufacturingLabelOrderItemAdminForm,
    ManufacturingLabelStatusAdminForm,
    ManufacturingOrderAdminForm,
    ManufacturingOrderItemAdminForm,
    ManufacturingStatusAdminForm,
)
from .models import (
    ManufacturingInternalLabelStatus,
    ManufacturingLabelOrder,
    ManufacturingLabelOrderItem,
    ManufacturingLabelStatus,
    ManufacturingOrder,
    ManufacturingOrderItem,
    ManufacturingStatus,
)


class ManufacturingInternalLabelStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name', 'weight')
    search_fields = ('name', 'slug',)
    form = ManufacturingInternalLabelStatusAdminForm


class ManufacturingOrderAdmin(NotDeleteModelAdmin):
    list_display = ('id', 'status', 'created', 'modified', )
    search_fields = ('id', )
    list_filter = ('status', )
    form = ManufacturingOrderAdminForm


class ManufacturingLabelOrderAdmin(NotDeleteModelAdmin):
    list_display = ('id', 'status', 'created', 'modified', )
    search_fields = ('id', )
    list_filter = ('status', )
    form = ManufacturingLabelOrderAdminForm


class ManufacturingOrderItemAdmin(admin.ModelAdmin):
    list_display = ('order', 'product', 'price', )
    search_fields = ('=order__id', '=product__id', '^product__article', 'product__name')
    form = ManufacturingOrderItemAdminForm


class ManufacturingLabelOrderItemAdmin(admin.ModelAdmin):
    list_display = ('order', 'product_label', 'price', )
    search_fields = ('=order__id', '=product_label__id', '^product_label__article', 'product_label__name')
    form = ManufacturingLabelOrderItemAdminForm


class ManufacturingStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name', 'weight')
    search_fields = ('name', 'slug',)
    form = ManufacturingStatusAdminForm


class ManufacturingLabelStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name', 'weight')
    search_fields = ('name', 'slug',)
    form = ManufacturingLabelStatusAdminForm


for model_or_iterable, admin_class in (
    (ManufacturingOrder, ManufacturingOrderAdmin),
    (ManufacturingOrderItem, ManufacturingOrderItemAdmin),
    (ManufacturingLabelOrder, ManufacturingLabelOrderAdmin),
    (ManufacturingLabelOrderItem, ManufacturingLabelOrderItemAdmin),
    (ManufacturingLabelStatus, ManufacturingLabelStatusAdmin),
    (ManufacturingStatus, ManufacturingStatusAdmin),
    (ManufacturingInternalLabelStatus, ManufacturingInternalLabelStatusAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

from django.conf import urls
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'manufacturing_order', views.ManufacturingOrderViewSet, base_name='order')
router.register(r'manufacturing_label_order', views.ManufacturingLabelOrderViewSet, base_name='order_label')
router.register(r'manufacturing_order/(?P<manufacturing_order_pk>\d+)/items', views.ManufacturingOrderItemViewSet, base_name='order_items'),
router.register(r'manufacturing_label_order/(?P<manufacturing_label_order_pk>\d+)/items', views.ManufacturingLabelOrderItemViewSet, base_name='order_label_items'),
router.register(r'manufacturing_status', views.ManufacturingStatusViewSet)
router.register(r'manufacturing_status', views.ManufacturingLabelStatusViewSet)


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
]

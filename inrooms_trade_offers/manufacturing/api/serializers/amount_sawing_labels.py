from rest_framework import serializers


def amount_validator(value):
        if value <= 0:
            raise serializers.ValidationError('Сумма должна быть положительной')


class AmountSawingSerializer(serializers.Serializer):
    amount_sawing_labels = serializers.DecimalField(max_digits=20, decimal_places=10)

    def __init__(self, manufacturing_order, *args, **kwargs):
        self.manufacturing_order = manufacturing_order
        super().__init__(*args, **kwargs)

    def validate(self, attrs):
        if not self.manufacturing_order.payment_account:
            raise serializers.ValidationError("Не заполнен расчетный счет")
        if self.manufacturing_order.payment_account.balance < attrs['amount_sawing_labels']:
            raise serializers.ValidationError("Не достаточно средств на счете")
        return attrs

from rest_framework import serializers

from ...models import ManufacturingLabelStatus, ManufacturingStatus


class ManufacturingStatusSerializer(serializers.ModelSerializer):
    next_status_detail = serializers.SerializerMethodField()

    class Meta:
        model = ManufacturingStatus
        fields = [
            'id',
            'name',
            'code',
            'next_status_detail'
        ]

    def get_next_status_detail(self, obj):
        return [{'id': s.id,
                 'name': s.name,
                 'button_name': s.button_name,
                 'code': s.code} for s in obj.next_status.all()]


class ManufacturingLabelStatusSerializer(serializers.ModelSerializer):
    next_status_detail = serializers.SerializerMethodField()

    class Meta:
        model = ManufacturingLabelStatus
        fields = [
            'id',
            'name',
            'code',
            'next_status_detail'
        ]

    def get_next_status_detail(self, obj):
        return [{'id': s.id,
                 'name': s.name,
                 'button_name': s.button_name,
                 'code': s.code} for s in obj.next_status.all()]

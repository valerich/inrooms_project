from rest_framework import serializers

from factories.api.serializers import FactorySerializer
from products.models import Size
from .manufacturing_order_item import ManufacturingLabelOrderItemSerializer, ManufacturingOrderItemSerializer
from ...models import ManufacturingLabelOrder, ManufacturingOrder


class ManufacturingOrderSerializer(serializers.ModelSerializer):
    status_detail = serializers.SerializerMethodField()
    internal_label_status_detail = serializers.SerializerMethodField()
    factory_detail = serializers.SerializerMethodField()
    items = ManufacturingOrderItemSerializer(many=True, read_only=True)
    kind_display = serializers.SerializerMethodField()
    is_good_received = serializers.SerializerMethodField()
    has_products_received = serializers.SerializerMethodField()
    ordered_amount = serializers.SerializerMethodField()
    left_to_pay = serializers.SerializerMethodField()

    class Meta:
        model = ManufacturingOrder

        fields = [
            'id',
            'status',
            'status_detail',
            'internal_label_status',
            'internal_label_status_detail',
            'kind',
            'kind_display',
            'created',
            'modified',
            'description',
            'factory',
            'factory_detail',
            'items',
            'amount_full',
            'payment_method',
            'amount_paid',
            'amount_received',
            'is_good_received',
            'has_products_received',
            'transport_costs',
            'ordered_amount',
            'payment_account',
            'internal_label_payment_account',
            'prepayment',
            'is_sawing_labels',
            'left_to_pay',
        ]

    def get_left_to_pay(self, obj):
        """Осталось заплатить

        Стоимость получено - аванс

        https://bitbucket.org/denisenterprise/inrooms_project/issues/220/--------------------------
        """
        return obj.amount_received - obj.prepayment

    def get_kind_display(self, obj):
        return obj.get_kind_display()

    def get_status_detail(self, obj):
        return {'id': obj.status_id,
                'name': obj.status.name,
                'code': obj.status.code,
                'check_internal_label_status_is_order_locked': obj.status.check_internal_label_status_is_order_locked,
                'next_statuses': [{
                    'id': s.id,
                    'name': s.name,
                    'button_name': s.button_name,
                    'code': s.code,
                } for s in obj.status.next_status.all()]}

    def get_internal_label_status_detail(self, obj):
        return {'id': obj.internal_label_status_id,
                'name': obj.internal_label_status.name,
                'code': obj.internal_label_status.code,
                'is_order_locked': obj.internal_label_status.is_order_locked,
                'next_statuses': [{
                    'id': s.id,
                    'name': s.name,
                    'button_name': s.button_name,
                    'code': s.code,
                } for s in obj.internal_label_status.next_status.all()]}

    def get_factory_detail(self, obj):
        if obj.factory_id:
            return FactorySerializer(obj.factory).data
        else:
            return None

    def get_is_good_received(self, obj):
        for item in obj.items.all():
            quantity = item.quantity
            for size in Size.objects.all():
                size_quantity = 0
                if getattr(item, 'has_{}'.format(size.code), False):
                    size_quantity = quantity * size.multiplier
                if size_quantity != getattr(item, 'received_{}_count'.format(size), 0):
                    return False
        return True

    def get_has_products_received(self, obj):
        for item in obj.items.all():
            for size in Size.objects.all():
                if getattr(item, 'received_{}_count'.format(size), 0):
                    return True
        return False

    def get_ordered_amount(self, obj):
        return obj.get_ordered_amount()


class ManufacturingLabelOrderSerializer(serializers.ModelSerializer):
    status_detail = serializers.SerializerMethodField()
    factory_detail = serializers.SerializerMethodField()
    items = ManufacturingLabelOrderItemSerializer(many=True, read_only=True)
    is_good_received = serializers.SerializerMethodField()
    has_products_received = serializers.SerializerMethodField()
    received_amount = serializers.SerializerMethodField()
    ordered_amount = serializers.SerializerMethodField()

    class Meta:
        model = ManufacturingLabelOrder

        fields = [
            'id',
            'status',
            'status_detail',
            'created',
            'modified',
            'description',
            'factory',
            'factory_detail',
            'items',
            'amount_full',
            'payment_method',
            'amount_paid',
            'is_good_received',
            'has_products_received',
            'transport_costs',
            'ordered_amount',
            'received_amount',
            'payment_account',
        ]

    def get_kind_display(self, obj):
        return obj.get_kind_display()

    def get_status_detail(self, obj):
        return {'id': obj.status_id,
                'name': obj.status.name,
                'code': obj.status.code,
                'next_statuses': [{
                    'id': s.id,
                    'name': s.name,
                    'button_name': s.button_name,
                    'code': s.code,
                } for s in obj.status.next_status.all()]}

    def get_factory_detail(self, obj):
        if obj.factory_id:
            return FactorySerializer(obj.factory).data
        else:
            return None

    def get_is_good_received(self, obj):
        for item in obj.items.all():
            for field in [field_name for field_name in dir(item) if field_name.startswith('received')]:
                if getattr(item, field, 0) != getattr(item, field.replace('ordered', 'received'), 0):
                    return False
        return True

    def get_has_products_received(self, obj):
        for item in obj.items.all():
            for field in [field_name for field_name in dir(item) if field_name.startswith('received')]:
                if getattr(item, field, 0):
                    return True
        return False

    def get_ordered_amount(self, obj):
        return obj.get_ordered_amount()

    def get_received_amount(self, obj):
        return obj.get_received_amount()

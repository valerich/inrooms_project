from rest_framework import serializers

from products.api.serializers import ProductLabelSerializer, ProductSerializer
from ...models import ManufacturingLabelOrderItem, ManufacturingOrderItem


class ManufacturingOrderItemSerializer(serializers.ModelSerializer):
    series_type = serializers.CharField(required=False)
    price = serializers.DecimalField(required=False, max_digits=20, decimal_places=10)
    product_detail = serializers.SerializerMethodField()
    ordered_size_quantity = serializers.SerializerMethodField()

    class Meta:
        model = ManufacturingOrderItem
        fields = [
            'id',
            'order',
            'product',
            'product_detail',
            'series_type',
            'price',
            'quantity',
            'ordered_size_quantity',
            'has_xxs',
            'has_xs',
            'has_s',
            'has_m',
            'has_l',
            'has_xl',
            'has_xxl',
            'has_xxxl',
            'has_34',
            'has_35',
            'has_36',
            'has_37',
            'has_38',
            'has_39',
            'has_40',
            'has_41',
            'received_xxs_count',
            'received_xs_count',
            'received_s_count',
            'received_m_count',
            'received_l_count',
            'received_xl_count',
            'received_xxl_count',
            'received_xxxl_count',
            'received_34_count',
            'received_35_count',
            'received_36_count',
            'received_37_count',
            'received_38_count',
            'received_39_count',
            'received_40_count',
            'received_41_count',
            'label_main',
            'label_main_xxs_count',
            'label_main_xs_count',
            'label_main_s_count',
            'label_main_m_count',
            'label_main_l_count',
            'label_main_xl_count',
            'label_main_xxl_count',
            'label_main_xxxl_count',
            'label_internal',
            'label_internal_count',
            'label_internal2',
            'label_internal2_count',
            'label_cardboard',
            'label_cardboard_count',
            'has_offer',
            'internal_label_price',
        ]

    def get_product_detail(self, obj):
        if obj.product_id:
            return ProductSerializer(obj.product).data
        else:
            return None

    def get_ordered_size_quantity(self, obj):
        return obj.get_ordered_size_quantity()


class ManufacturingLabelOrderItemSerializer(serializers.ModelSerializer):
    product_label_detail = serializers.SerializerMethodField()

    class Meta:
        model = ManufacturingLabelOrderItem
        fields = [
            'id',
            'order',
            'product_label',
            'product_label_detail',
            'price',
            'ordered',
            'ordered_xxs',
            'ordered_xs',
            'ordered_s',
            'ordered_m',
            'ordered_l',
            'ordered_xl',
            'ordered_xxl',
            'ordered_xxxl',
            'received',
            'received_xxs',
            'received_xs',
            'received_s',
            'received_m',
            'received_l',
            'received_xl',
            'received_xxl',
            'received_xxxl',
        ]

    def get_product_label_detail(self, obj):
        if obj.product_label_id:
            return ProductLabelSerializer(obj.product_label).data
        else:
            return None

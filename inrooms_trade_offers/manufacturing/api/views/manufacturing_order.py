from decimal import Decimal

import django_filters
from django.db import transaction
from django.utils import timezone
from rest_framework import filters, status
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from extensions.drf.views import MultiSerializerViewSetMixin
from history.models import HistoryItem
from manufacturing.models import ManufacturingLabelStatus, ManufacturingOrderItem, ManufacturingStatus, \
    ManufacturingInternalLabelStatus
from offers.models import Offer
from payments.models import ExpenseItem, PaymentDocument
from products.models import SIZES, LabelRemain, Remain, Size
from .. import serializers
from ...models import ManufacturingLabelOrder, ManufacturingOrder


class ManufacturingOrderFilterSet(django_filters.FilterSet):

    class Meta:
        model = ManufacturingOrder
        fields = [
            'creator',
            'performer',
            'items__product',
            'factory',
            'status',
            'created',
            'modified',
            'kind',
        ]


class ManufacturingLabelOrderFilterSet(django_filters.FilterSet):

    class Meta:
        model = ManufacturingLabelOrder
        fields = [
            'creator',
            'performer',
            'items__product_label',
            'factory',
            'status',
            'created',
            'modified',
        ]


class ManufacturingOrderMixin(MultiSerializerViewSetMixin, ModelViewSet):
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    ordering_fields = (
        'id',
        'created',
        'modified',
        'factory__name',
    )

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)

    def perform_update(self, serializer):
        obj = serializer.instance
        # self.old_obj = Order.objects.get(pk=obj.pk)

        super(ManufacturingOrderMixin, self).perform_update(serializer)


class ManufacturingOrderViewSet(ManufacturingOrderMixin):
    filter_class = ManufacturingOrderFilterSet
    model = ManufacturingOrder
    serializers = {
        'default': serializers.ManufacturingOrderSerializer,
    }
    search_fields = (
        '=id',
        '^items__product__article',
        'items__product__name'
    )

    def get_queryset(self):
        qs = ManufacturingOrder.objects.all()
        return qs

    @detail_route(methods=['post'])
    @transaction.atomic()
    def change_status(self, request, pk=None):
        manufacturing_order = self.get_object()
        status_code = request.data.get('code', None)
        if status_code:
            try:
                new_status = ManufacturingStatus.objects.get(code=status_code)
            except ManufacturingStatus.DoesNotExist:
                pass
            else:
                if not new_status.check_internal_label_status_is_order_locked or not manufacturing_order.internal_label_status.is_order_locked:
                    if status_code in ['cancel', 'received']:
                        # ref #554 необходимо переставать выводить в новых моделях предложения, по которым уже был заказ
                        aggreement_offer_ids = manufacturing_order.items.filter(
                            product__offer__is_manufacturing_aggreement=True
                        ).values_list(
                            'product__offer_id', flat=True)

                        if aggreement_offer_ids.exists():
                            Offer.objects.filter(id__in=aggreement_offer_ids).update(to_aggreement=False)

                    if status_code == 'sent_to_factory':
                        if manufacturing_order.prepayment > 0:

                            if not manufacturing_order.payment_account:
                                return Response({'error': 'Не заполнено поле "Счет оплаты"'},
                                                status=status.HTTP_400_BAD_REQUEST)
                            if manufacturing_order.prepayment > manufacturing_order.payment_account.balance:
                                return Response({'error': 'Не достаточно средств для оплаты'},
                                                status=status.HTTP_400_BAD_REQUEST)
                            prepayment_rur = manufacturing_order.payment_account.get_amount_rur(manufacturing_order.prepayment)
                            expense_item = ExpenseItem.get_manufacturing_order_prepayment()
                            payment_document = PaymentDocument(
                                kind=PaymentDocument.KIND.debit,
                                payment_account=manufacturing_order.payment_account,
                                destination_payment_account=manufacturing_order.payment_account,
                                amount=manufacturing_order.prepayment,
                                amount_rur=prepayment_rur,
                                user=self.request.user,
                                description="Аванс по заказу на фабрику №{}".format(manufacturing_order.id),
                                content_object=manufacturing_order,
                                expense_item=expense_item,
                            )
                            payment_document.save()
                            payment_document.process()

                            history_item = HistoryItem.objects.create(
                                kind=HistoryItem.KIND.payments,
                                content_object=manufacturing_order,
                                body="Аванс по заказу на фабрику {} {} ю. {} руб.".format(
                                    timezone.now().strftime('%d.%m.%Y'),
                                    manufacturing_order.prepayment,
                                    prepayment_rur,
                                ),
                            )

                            manufacturing_order.prepayment_rur = prepayment_rur
                            manufacturing_order.save()

                    if status_code == 'cancel':
                        if manufacturing_order.prepayment > 0:
                            if manufacturing_order.payment_account:
                                expense_item = ExpenseItem.get_manufacturing_order_payment_cancel()
                                payment_document = PaymentDocument(
                                    kind=PaymentDocument.KIND.credit,
                                    payment_account=manufacturing_order.payment_account,
                                    destination_payment_account=manufacturing_order.payment_account,
                                    amount=manufacturing_order.prepayment,
                                    amount_rur=manufacturing_order.prepayment_rur,
                                    user=self.request.user,
                                    description="Отмена по заказу на фабрику №{}".format(manufacturing_order.id),
                                    content_object=manufacturing_order,
                                    expense_item=expense_item,
                                )

                                history_item = HistoryItem.objects.create(
                                    kind=HistoryItem.KIND.payments,
                                    content_object=manufacturing_order,
                                    body="Отмена по заказу на фабрику {} {} ю. {} руб.".format(
                                        timezone.now().strftime('%d.%m.%Y'),
                                        manufacturing_order.prepayment,
                                        manufacturing_order.prepayment_rur,
                                    ),
                                )

                                payment_document.save()
                                payment_document.process()

                    if status_code == 'received':
                        if not manufacturing_order.payment_account:
                            return Response({'error': 'Не заполнено поле "Счет оплаты"'},
                                            status=status.HTTP_400_BAD_REQUEST)
                        elif manufacturing_order.payment_account.balance < manufacturing_order.get_full_amount() - manufacturing_order.prepayment:
                            return Response({'error': 'Не достаточно средств для оплаты'},
                                            status=status.HTTP_400_BAD_REQUEST)
                        has_products_received = False
                        for item in manufacturing_order.items.all():
                            for size in SIZES:
                                if getattr(item, 'received_{}_count'.format(size), 0):
                                    has_products_received = True
                        if not has_products_received:
                            return Response({'error': 'По заказу не получено ни одного товара'},
                                     status=status.HTTP_400_BAD_REQUEST)

                    if status_code in ['received', 'sewing_labels']:
                        if not manufacturing_order.amount_received:
                            return Response({'error': 'Введите стоимость полученного товара'},
                                            status=status.HTTP_400_BAD_REQUEST)
                        else:
                            received_count = manufacturing_order.get_received_count()
                            if received_count:
                                one_item_received_price = manufacturing_order.get_one_item_received_price()
                                ManufacturingOrderItem.objects.filter(order=manufacturing_order).update(price=one_item_received_price)
                            else:
                                ManufacturingOrderItem.objects.filter(order=manufacturing_order).update(price=0)

                    if status_code == 'sewing_labels':
                        manufacturing_order.is_sawing_labels = True

                    elif status_code == 'received':

                        payment_amount_rur = Decimal('0.0000000000')
                        payment_amount_chn = Decimal('0.0000000000')

                        if manufacturing_order.get_received_count():
                            item_amount_sawing_labels_rur = manufacturing_order.amount_sawing_labels_rur / manufacturing_order.get_received_count()
                        else:
                            item_amount_sawing_labels_rur = Decimal('0.00')
                        for item in manufacturing_order.items.all():
                            if not item.remains_created and item.get_received_count():
                                try:
                                    item.reserve_labels(manufacturing_order.warehouse)
                                except ValueError:
                                    return Response({'error': 'Не достаточно этикеток'},
                                                    status=status.HTTP_400_BAD_REQUEST)
                                avg_amount = item.price
                                item_label_amount_rur = (item.label_main_summ_rur + item.label_internal_summ_rur + item.label_internal2_summ_rur + item.label_cardboard_summ_rur) / item.get_received_count()
                                if item.has_offer and not item.product.offer.is_suplied:
                                    offer_cost_rur = item.product.offer.cost_rur / item.get_received_count()
                                    item.product.offer.is_suplied = True
                                    item.product.offer.save()
                                else:
                                    offer_cost_rur = Decimal('0.0000000000')
                                for size in SIZES:
                                    received = getattr(item, 'received_{}_count'.format(size), 0)
                                    if received:
                                        size = Size.objects.get(code=size)

                                        amount_chn = avg_amount * received
                                        amount_rur = manufacturing_order.payment_account.get_amount_rur(amount_chn)
                                        Remain.add_remain(
                                            product=item.product,
                                            warehouse=manufacturing_order.warehouse,
                                            amount_rur=amount_rur + offer_cost_rur + item_label_amount_rur * received + item_amount_sawing_labels_rur * received,
                                            value=received,
                                            size=size,
                                        )
                                        payment_amount_rur += amount_rur
                                        payment_amount_chn += amount_chn
                                item.remains_created = True
                                item.save()
                        expense_item = ExpenseItem.get_manufacturing_order_payment()
                        payment_document = PaymentDocument(
                            kind=PaymentDocument.KIND.debit,
                            payment_account=manufacturing_order.payment_account,
                            destination_payment_account=manufacturing_order.payment_account,
                            amount=payment_amount_chn - manufacturing_order.prepayment,
                            amount_rur=payment_amount_rur - manufacturing_order.prepayment_rur,
                            user=self.request.user,
                            description="Оплата заказа на фабрику №{}".format(manufacturing_order.id),
                            expense_item=expense_item,
                            content_object=manufacturing_order,
                        )

                        history_item = HistoryItem.objects.create(
                            kind=HistoryItem.KIND.payments,
                            content_object=manufacturing_order,
                            body="Оплата заказа на фабрику {} {} ю. {} руб.".format(
                                timezone.now().strftime('%d.%m.%Y'),
                                payment_amount_chn - manufacturing_order.prepayment,
                                payment_amount_rur - manufacturing_order.prepayment_rur,
                            ),
                        )

                        payment_document.save()
                        payment_document.process()

                    manufacturing_order.status = new_status
                    manufacturing_order.amount_full = manufacturing_order.get_full_amount()
                    manufacturing_order.save()
                    manufacturing_order.process_order()
                    return Response(status=status.HTTP_200_OK)
                else:
                    return Response({'error': 'Заказ этикеток не закончен'},
                                    status=status.HTTP_400_BAD_REQUEST)

        return Response({'error': 'Не верный статус'},
                        status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'])
    @transaction.atomic()
    def change_internal_label_status(self, request, pk=None):
        manufacturing_order = self.get_object()
        status_code = request.data.get('code', None)
        if status_code:
            try:
                new_status = ManufacturingInternalLabelStatus.objects.get(code=status_code)
            except ManufacturingInternalLabelStatus.DoesNotExist:
                pass
            else:
                if status_code == 'received':
                    payment_amount_chn = manufacturing_order.get_internal_label_amount_full()
                    payment_amount_rur = manufacturing_order.payment_account.get_amount_rur(payment_amount_chn)
                    if payment_amount_chn:
                        if not manufacturing_order.internal_label_payment_account:
                            return Response({'error': 'Не заполнено поле "Счет оплаты этикеток"'},
                                            status=status.HTTP_400_BAD_REQUEST)
                        elif manufacturing_order.internal_label_payment_account.balance < payment_amount_chn:
                            return Response({'error': 'Не достаточно средств для оплаты'},
                                            status=status.HTTP_400_BAD_REQUEST)

                        expense_item = ExpenseItem.get_manufacturing_order_internal_label_payment()
                        payment_document = PaymentDocument(
                            kind=PaymentDocument.KIND.debit,
                            payment_account=manufacturing_order.internal_label_payment_account,
                            destination_payment_account=manufacturing_order.internal_label_payment_account,
                            amount=payment_amount_chn,
                            amount_rur=payment_amount_rur,
                            user=self.request.user,
                            description="Оплата заказа внутренних этикеток в заказе на фабрику №{}".format(manufacturing_order.id),
                            expense_item=expense_item,
                            content_object=manufacturing_order,
                        )

                        history_item = HistoryItem.objects.create(
                            kind=HistoryItem.KIND.payments,
                            content_object=manufacturing_order,
                            body="Оплата заказа внутренних этикеток в заказе на фабрику {} {} ю. {} руб.".format(
                                timezone.now().strftime('%d.%m.%Y'),
                                payment_amount_chn,
                                payment_amount_rur,
                            ),
                        )

                        payment_document.save()
                        payment_document.process()

                manufacturing_order.internal_label_status = new_status
                manufacturing_order.save()

                return Response(status=status.HTTP_200_OK)
        return Response({'error': 'Не верный статус'},
                        status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'])
    @transaction.atomic()
    def set_amount_sawing_labels(self, request, pk=None):
        obj = self.get_object()
        serializer = serializers.AmountSawingSerializer(manufacturing_order=obj, data=request.data)
        serializer.is_valid(True)

        expense_item = ExpenseItem.get_amount_sawing_labels_item()
        payment_account = obj.payment_account
        amount = serializer.validated_data['amount_sawing_labels']
        amount_rur = payment_account.get_amount_rur(amount)

        document = PaymentDocument(
            kind=PaymentDocument.KIND.debit,
            payment_account=obj.payment_account,
            destination_payment_account=obj.payment_account,
            amount=amount,
            amount_rur=amount_rur,
            user=self.request.user,
            description="Списание за работы по пошиву этикеток заказа на фабрику №{}. Статья расходов: {}".format(
                obj.id,
                expense_item
            ),
            expense_item=expense_item,
            content_object=obj,
        )
        document.save()
        document.process()

        history_item = HistoryItem.objects.create(
            kind=HistoryItem.KIND.payments,
            content_object=obj,
            body="Стоимость перешива {} {} ю. {} руб.".format(
                timezone.now().strftime('%d.%m.%Y'),
                amount,
                amount_rur,
            ),
        )

        obj.amount_sawing_labels = amount
        obj.amount_sawing_labels_rur = amount_rur
        obj.save()

        return Response(status=status.HTTP_200_OK)


class ManufacturingLabelOrderViewSet(ManufacturingOrderMixin):
    filter_class = ManufacturingLabelOrderFilterSet
    model = ManufacturingLabelOrder
    serializers = {
        'default': serializers.ManufacturingLabelOrderSerializer,
    }
    search_fields = (
        '=id',
        'items__product_label__name'
    )

    def get_queryset(self):
        qs = ManufacturingLabelOrder.objects.all()
        return qs

    @detail_route(methods=['post'])
    @transaction.atomic()
    def change_status(self, request, pk=None):
        manufacturing_order = self.get_object()
        status_code = request.data.get('code', None)
        if status_code:
            try:
                new_status = ManufacturingLabelStatus.objects.get(code=status_code)
            except ManufacturingLabelStatus.DoesNotExist:
                pass
            else:
                if status_code == 'received':
                    if not manufacturing_order.payment_account:
                        return Response({'error': 'Не заполнено поле "Счет оплаты"'},
                                        status=status.HTTP_400_BAD_REQUEST)
                    elif manufacturing_order.payment_account.balance < manufacturing_order.get_full_amount():
                        return Response({'error': 'Не достаточно средств для оплаты'},
                                        status=status.HTTP_400_BAD_REQUEST)
                    has_products_received = False
                    for item in manufacturing_order.items.all():
                        for field in [field_name for field_name in dir(item) if field_name.startswith('received')]:
                            if getattr(item, field, 0):
                                has_products_received = True
                    if not has_products_received:
                        return Response({'error': 'По заказу не получено ни одного товара'},
                                 status=status.HTTP_400_BAD_REQUEST)
                manufacturing_order.status = new_status
                manufacturing_order.amount_full = manufacturing_order.get_full_amount()
                manufacturing_order.save()

                if status_code == 'received':
                    item_transport_costs = manufacturing_order.get_item_transport_costs()

                    payment_amount_rur = Decimal('0.0000000000')
                    payment_amount_chn = Decimal('0.0000000000')
                    for item in manufacturing_order.items.all():
                        if not item.remains_created:
                            avg_amount = item.get_avg_amount(item_transport_costs)
                            for field in [field_name for field_name in dir(item) if field_name.startswith('received')]:
                                received = getattr(item, field, 0)
                                if received:
                                    if len(field) == 8:
                                        size = None
                                    else:
                                        size = Size.objects.get(code=field[9:])
                                    amount_chn = avg_amount * received
                                    amount_rur = manufacturing_order.payment_account.get_amount_rur(amount_chn)
                                    LabelRemain.add_remain(
                                        product_label=item.product_label,
                                        warehouse=manufacturing_order.warehouse,
                                        amount_rur=amount_rur,
                                        value=received,
                                        size=size,
                                    )
                                    payment_amount_rur += amount_rur
                                    payment_amount_chn += amount_chn
                                item.remains_created = True
                                item.save()

                    expense_item = ExpenseItem.get_manufacturing_order_label_payment()
                    payment_document = PaymentDocument(
                        kind=PaymentDocument.KIND.debit,
                        payment_account=manufacturing_order.payment_account,
                        destination_payment_account=manufacturing_order.payment_account,
                        amount=payment_amount_chn,
                        amount_rur=payment_amount_rur,
                        user=self.request.user,
                        description="Оплата заказа этикеток №{}".format(manufacturing_order.id),
                        expense_item=expense_item,
                        content_object=manufacturing_order,
                    )
                    payment_document.save()
                    payment_document.process()

                return Response(status=status.HTTP_200_OK)
        return Response({'error': 'Не верный статус'},
                        status=status.HTTP_400_BAD_REQUEST)

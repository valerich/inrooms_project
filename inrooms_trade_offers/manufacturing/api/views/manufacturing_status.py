from django.db.models.query_utils import Q
from rest_framework import filters, mixins
from rest_framework.exceptions import ParseError
from rest_framework.viewsets import GenericViewSet

from .. import serializers
from ...models import ManufacturingLabelOrder, ManufacturingLabelStatus, ManufacturingOrder, ManufacturingStatus


class ManufacturingStatusViewSet(mixins.RetrieveModelMixin,
                                 mixins.ListModelMixin,
                                 GenericViewSet):
    serializer_class = serializers.ManufacturingStatusSerializer
    queryset = ManufacturingStatus.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )


class ManufacturingStatusNextViewSet(ManufacturingStatusViewSet):
    """Для вывода автокомплита в поле "статус" при редактировании заказов"""

    serializer_class = serializers.ManufacturingStatusSerializer
    queryset = ManufacturingStatus.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )

    def dispatch(self, request, manufacturing_order_id, *args, **kwargs):
        try:
            self.manufacturing_order = ManufacturingOrder.objects.get(pk=manufacturing_order_id)
        except ManufacturingOrder.DoesNotExist:
            raise ParseError('wrong order id')
        return super(ManufacturingStatusNextViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        status_ids = self.manufacturing_order.status.next_status.all().values_list('id', flat=True)
        return ManufacturingStatus.objects.filter(Q(id=self.manufacturing_order.status_id) | Q(id__in=status_ids))


class ManufacturingLabelStatusViewSet(mixins.RetrieveModelMixin,
                                 mixins.ListModelMixin,
                                 GenericViewSet):
    serializer_class = serializers.ManufacturingLabelStatusSerializer
    queryset = ManufacturingLabelStatus.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )


class ManufacturingLabelStatusNextViewSet(ManufacturingStatusViewSet):
    """Для вывода автокомплита в поле "статус" при редактировании заказов"""

    serializer_class = serializers.ManufacturingLabelStatusSerializer
    queryset = ManufacturingLabelStatus.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )

    def dispatch(self, request, manufacturing_order_id, *args, **kwargs):
        try:
            self.manufacturing_order = ManufacturingLabelOrder.objects.get(pk=manufacturing_order_id)
        except ManufacturingOrder.DoesNotExist:
            raise ParseError('wrong order id')
        return super(ManufacturingLabelStatusNextViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        status_ids = self.manufacturing_order.status.next_status.all().values_list('id', flat=True)
        return ManufacturingStatus.objects.filter(Q(id=self.manufacturing_order.status_id) | Q(id__in=status_ids))

from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from .. import serializers
from ...models import ManufacturingLabelOrder, ManufacturingLabelOrderItem, ManufacturingOrder, ManufacturingOrderItem


class ManufacturingOrderItemViewSet(ModelViewSet):
    serializer_class = serializers.ManufacturingOrderItemSerializer
    model = ManufacturingOrderItem
    queryset = ManufacturingOrderItem.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('^product__article', 'product__name')
    ordering_fields = (
        'product__article',
        'product__name',
    )

    def dispatch(self, request, manufacturing_order_pk: int, *args, **kwargs):
        self.manufacturing_order = get_object_or_404(ManufacturingOrder, pk=manufacturing_order_pk)
        return super(ManufacturingOrderItemViewSet, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(order=self.manufacturing_order)

    # def perform_update(self, serializer):
    #     obj = serializer.instance
    #     self.old_obj = Order.objects.get(pk=obj.pk)
    #
    #     super(OrderViewSet, self).perform_update(serializer)

    def get_queryset(self):
        qs = super(ManufacturingOrderItemViewSet, self).get_queryset()
        qs = qs.filter(order=self.manufacturing_order)
        return qs


class ManufacturingLabelOrderItemViewSet(ModelViewSet):
    serializer_class = serializers.ManufacturingLabelOrderItemSerializer
    model = ManufacturingLabelOrderItem
    queryset = ManufacturingLabelOrderItem.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('^product_label__name')
    ordering_fields = (
        'product_label__name',
    )

    def dispatch(self, request, manufacturing_label_order_pk: int, *args, **kwargs):
        self.manufacturing_label_order = get_object_or_404(ManufacturingLabelOrder, pk=manufacturing_label_order_pk)
        return super(ManufacturingLabelOrderItemViewSet, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(order=self.manufacturing_label_order)

    def get_queryset(self):
        qs = super(ManufacturingLabelOrderItemViewSet, self).get_queryset()
        qs = qs.filter(order=self.manufacturing_label_order)
        return qs

from django.db import models, transaction
from model_utils.choices import Choices

from model_utils.models import TimeStampedModel


SOURCE_CHOICES = Choices(
    (1, 'auto', 'Создан автоматически'),
    (2, 'handmade', 'Создан руками'),
)

PRICE_SET_KIND_CHOICES = Choices(
    (1, 'from_order_item', 'От стомости отгрузки партнеру'),
    (2, 'from_product', 'От текщей установленной цены'),
)

PRICE_SET_STATUS_CHOICES = Choices(
    (1, 'new', 'Новый'),
    (2, 'complete', 'Отправлен в 1с'),
)


class PriceSet(TimeStampedModel):
    SOURCE = SOURCE_CHOICES
    PRICE_SET_KIND = PRICE_SET_KIND_CHOICES
    STATUS = PRICE_SET_STATUS_CHOICES
    creator = models.ForeignKey('accounts.User', verbose_name='Создатель')
    order = models.OneToOneField('orders.Order', verbose_name='Бланк-заказ', blank=True, null=True, unique=True, on_delete=models.PROTECT)
    source = models.PositiveSmallIntegerField('Источник', choices=SOURCE, default=SOURCE.auto)
    status = models.PositiveSmallIntegerField('Статус', choices=STATUS, default=STATUS.new)
    client = models.ForeignKey('clients.Client', verbose_name='Партнер', on_delete=models.PROTECT)
    price_set_kind = models.PositiveSmallIntegerField('Тип установки цены', choices=PRICE_SET_KIND,
                                                      default=PRICE_SET_KIND.from_order_item)
    markup_percentage = models.SmallIntegerField('Процент наценки', default=0)
    created_document_1c = models.BooleanField('Документ 1с создан?', default=False)

    class Meta:
        verbose_name = 'Документ установки цен'
        verbose_name_plural = 'Документы установки цен'
        ordering = ('-id', )

    def __str__(self):
        return 'id: {}'.format(self.id)

    def process(self):
        if not self.created_document_1c:
            self.create_1c_document()

    def create_1c_document(self):
        """Создание документа для синхронизации с 1с"""

        from service_1c.utils.price_set_exporter import PriceSetExporter

        with transaction.atomic():
            document = PriceSetExporter(self.id).create_document()
            self.created_document_1c = True
            self.save()
        return document


class PriceSetItem(models.Model):
    price_set = models.ForeignKey(PriceSet, verbose_name='Документ установки цен', related_name='items')
    order_item = models.ForeignKey('orders.OrderItem', verbose_name='Товар в бланк-заказе', blank=True, null=True,
                                   on_delete=models.SET_NULL)
    product = models.ForeignKey('products.Product', verbose_name='Модель')
    old_price = models.DecimalField('Старая цена', max_digits=10, decimal_places=2)
    new_price = models.DecimalField('Новая цена', max_digits=10, decimal_places=2)

    class Meta:
        verbose_name = 'Оцениваемый товар'
        verbose_name_plural = 'Оцениваемые товары'
        ordering = ('id', )
        unique_together = ('price_set', 'product')

    def __str__(self):
        return 'id: {}'.format(self.id)


def price_set_item_set_fields_by_order_item(sender, instance, **kwargs):
    if instance.order_item_id:
        if not instance.product_id:
            instance.product = instance.order_item.product
        if instance.old_price is None:
            instance.old_price = instance.order_item.price
        if instance.new_price is None:
            instance.new_price = instance.order_item.recommended_retail_price


models.signals.pre_save.connect(price_set_item_set_fields_by_order_item, sender=PriceSetItem)

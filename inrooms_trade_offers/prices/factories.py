import factory
import factory.django
import factory.fuzzy
from decimal import Decimal

from accounts.factories import UserFactory
from clients.factories import ClientFactory
from orders.factories import OrderItemFactory
from products.factories import ProductFactory
from .models import (
    PriceSet,
    PriceSetItem,
)


class PriceSetFactory(factory.django.DjangoModelFactory):
    client = factory.SubFactory(ClientFactory)
    creator = factory.SubFactory(UserFactory)

    class Meta:
        model = PriceSet


class PriceSetItemFactory(factory.django.DjangoModelFactory):
    price_set = factory.SubFactory(PriceSetFactory)
    order_item = factory.SubFactory(OrderItemFactory)
    product = factory.SubFactory(ProductFactory)
    old_price = factory.fuzzy.FuzzyDecimal(Decimal('00.00'), Decimal('10000.00'))
    new_price = factory.fuzzy.FuzzyDecimal(Decimal('00.00'), Decimal('10000.00'))

    class Meta:
        model = PriceSetItem

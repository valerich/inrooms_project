from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'prices'
    verbose_name = 'Цены'

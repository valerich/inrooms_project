# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-07-17 12:44
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('orders', '0001_initial'),
        ('products', '__first__'),
        ('clients', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='PriceSet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('source', models.PositiveSmallIntegerField(choices=[(1, 'Создан автоматически'), (2, 'Создан руками')], default=1, verbose_name='Источник')),
                ('status', models.PositiveSmallIntegerField(choices=[(1, 'Новый'), (2, 'Отправлен в 1с')], default=1, verbose_name='Статус')),
                ('price_set_kind', models.PositiveSmallIntegerField(choices=[(1, 'От стомости отгрузки партнеру'), (2, 'От текщей установленной цены')], default=1, verbose_name='Тип установки цены')),
                ('markup_percentage', models.SmallIntegerField(default=0, verbose_name='Процент наценки')),
                ('created_document_1c', models.BooleanField(default=False, verbose_name='Документ 1с создан?')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='clients.Client', verbose_name='Партнер')),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Создатель')),
                ('order', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='orders.Order', verbose_name='Бланк-заказ')),
            ],
            options={
                'verbose_name': 'Документ установки цен',
                'verbose_name_plural': 'Документы установки цен',
                'ordering': ('-id',),
            },
        ),
        migrations.CreateModel(
            name='PriceSetItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('old_price', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Старая цена')),
                ('new_price', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Новая цена')),
                ('order_item', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='orders.OrderItem', verbose_name='Товар в бланк-заказе')),
                ('price_set', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='prices.PriceSet', verbose_name='Документ установки цен')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='products.Product', verbose_name='Модель')),
            ],
            options={
                'verbose_name': 'Оцениваемый товар',
                'verbose_name_plural': 'Оцениваемые товары',
                'ordering': ('id',),
            },
        ),
        migrations.AlterUniqueTogether(
            name='pricesetitem',
            unique_together=set([('price_set', 'product')]),
        ),
    ]

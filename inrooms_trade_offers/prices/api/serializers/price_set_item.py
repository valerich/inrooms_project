from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from ...models import PriceSetItem


class PriceSetItemSerializer(serializers.ModelSerializer):
    order_item_detail = serializers.SerializerMethodField()
    product_detail = serializers.SerializerMethodField()

    class Meta:
        model = PriceSetItem
        fields = [
            'id',
            'order_item_detail',
            'old_price',
            'new_price',
            'product_detail',
        ]

    def get_order_item_detail(self, obj):
        if obj.order_item:
            data = {
                'id': obj.id,
                'order_detail': {
                    'id': obj.order_item.order_id,
                },
                'price': obj.order_item.price,
            }
        else:
            data = None
        return data

    def get_product_detail(self, obj):
        return {
            'id': obj.product_id,
            'name': obj.product.name,
            'article': obj.product.article,
            'color': {
                'id': obj.product.color_id,
                'name': obj.product.color.name,
            },
            'image': self.get_image(obj.product)
        }

    def get_image(self, obj):
        pi = obj.images.all().first()
        return self.resize_image(pi, obj)

    def resize_image(self, pi, obj):
        if pi:
            try:
                _200x200 = get_thumbnail(pi.image, '400x400', crop="center").url
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
            except IOError as e:
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1500 = request.build_absolute_uri(_950x1200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,
                    '950x1200': _950x1200}
        else:
            return None
from rest_framework import serializers

from products.models import ProductCollection


class ProductCollectionSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = ProductCollection

        fields = [
            'id',
            'name',
        ]

    def get_name(self, obj):
        return str(obj)

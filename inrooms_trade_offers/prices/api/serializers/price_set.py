from rest_framework import serializers

from clients.api.serializers.client import ClientSerializer
from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import PriceSet


class PriceSetSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    client_detail = serializers.SerializerMethodField()
    source_detail = serializers.SerializerMethodField()
    price_set_kind_detail = serializers.SerializerMethodField()
    creator_detail = serializers.SerializerMethodField()
    order_detail = serializers.SerializerMethodField()
    status_detail = serializers.SerializerMethodField()

    class Meta:
        model = PriceSet

        fields = [
            'id',
            'client',
            'client_detail',
            'source',
            'source_detail',
            'price_set_kind',
            'price_set_kind_detail',
            'created',
            'modified',
            'creator_detail',
            'order_detail',
            'status_detail',
            'markup_percentage',
        ]

    def get_status_detail(self, obj):
        return {
            'id': obj.status,
            'name': obj.get_status_display(),
        }

    def get_order_detail(self, obj):
        if obj.order_id:
            return {
                'id': obj.order_id
            }
        return None

    def get_client_detail(self, obj):
        return ClientSerializer(obj.client, fields=[
            'id',
            'name',
        ]).data

    def get_source_detail(self, obj):
        return {
            'id': obj.source,
            'name': obj.get_source_display(),
        }

    def get_price_set_kind_detail(self, obj):
        return {
            'id': obj.price_set_kind,
            'name': obj.get_price_set_kind_display(),
        }

    def get_creator_detail(self, obj):
        return {
            'id': obj.creator_id,
            'name': obj.creator.get_full_name()
        }

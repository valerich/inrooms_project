import django_filters
from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet

from orders.models.order import OrderStatus, OrderItem
from prices.models import PriceSet, PriceSetItem
from products.models import ProductCollection, Remain, Product
from ..serializers import ProductCollectionSerializer


class ProductCollectionFilterSet(django_filters.FilterSet):

    class Meta:
        model = ProductCollection
        fields = [
        ]


class ProductCollectionViewSet(ListModelMixin,
                               GenericViewSet):

    serializer_class = ProductCollectionSerializer
    filter_class = ProductCollectionFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter, )
    search_fields = ('name', '=id')
    queryset = ProductCollection.objects.order_by('-modified')
    ordering_fields = (
        'id',
        'name',
    )

    def dispatch(self, request, price_set_pk: int, *args, **kwargs):
        self.price_set = get_object_or_404(PriceSet, pk=price_set_pk)
        return super(ProductCollectionViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        product_ids = Remain.objects.filter(
            warehouse__client=self.price_set.client,
            value__gt=0
        ).values_list(
            'product_id', flat=True
        ).distinct()

        good_statuses = OrderStatus.objects.filter(
            code__in=[
                'ready_to_send',
                'shipped',
                'received',
            ]
        )
        in_order_ids = OrderItem.objects.filter(
            order__client=self.price_set.client,
            order__status__in=good_statuses
        ).values_list(
            'product_id', flat=True
        ).distinct()

        exclude_ids = PriceSetItem.objects.filter(
            price_set=self.price_set).values_list('product_id', flat=True)

        product_qs = Product.objects.filter(id__in=product_ids)
        product_qs = product_qs.filter(id__in=in_order_ids)
        if exclude_ids:
            product_qs = product_qs.exclude(id__in=exclude_ids)
        main_collection_ids = product_qs.values_list('main_collection', flat=True).distinct()
        return ProductCollection.objects.filter(id__in=main_collection_ids)

import datetime
import django_filters
from decimal import Decimal
from django.db import transaction
from django.db.utils import IntegrityError
import math
from rest_framework import filters, status
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from openpyxl.reader.excel import load_workbook

from core.models import CURRENCY_CHOICES
from orders.models.order import OrderItem, OrderStatus
from payments.models import CurrencyRate
from prices.models import PriceSetItem
from products.models import Product, ProductCollection, Remain
from ..serializers import PriceSetSerializer
from ...models import PriceSet


class PriceSetFilterSet(django_filters.FilterSet):

    class Meta:
        model = PriceSet
        fields = [
            'client',
            'items__order_item__product',
            'source',
            'price_set_kind',
        ]


class PriceSetViewSet(ModelViewSet):
    serializer_class = PriceSetSerializer
    model = PriceSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    filter_class = PriceSetFilterSet
    search_fields = ('=id', '=items__order_item__product__id', '=items__order_item__product__article', )
    ordering_fields = (
        'id',
        'client__name',
        'created',
        'modified',
    )

    def get_queryset(self):
        qs = PriceSet.objects.all().select_related('client')
        return qs

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user,
                        source=PriceSet.SOURCE.handmade, )

    @detail_route(methods=['post'])
    @transaction.atomic()
    def change_status(self, request, pk=None):
        """Изменение статуса

        Формат запроса: {'status_code': 'new'}
        Параметры:
            status_code: (обязательный) новый статус

        """

        price_set = self.get_object()
        status_code = request.data.get('code', None)
        if status_code:
            try:
                new_status = getattr(PriceSet.STATUS, status_code)
            except AttributeError:
                pass
            else:
                price_set.status = new_status
                price_set.save()
                price_set.process()
                return Response(status=status.HTTP_200_OK)
        return Response({'error': 'Не верный статус'},
                        status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'])
    def add_product(self, request, pk=None):
        obj = self.get_object()
        product_id = int(request.data.get('product_id', None))

        try:
            product = Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            return Response({'error': 'Товара с таким id не существует'},
                            status=status.HTTP_400_BAD_REQUEST)

        good_statuses = OrderStatus.objects.filter(
            code__in=[
                'ready_to_send',
                'shipped',
                'received',
            ]
        )
        oi = OrderItem.objects.filter(
            order__client=obj.client,
            order__status__in=good_statuses,
            product_id=product.id
        ).order_by('order__created').first()

        if obj.price_set_kind == obj.PRICE_SET_KIND.from_order_item:
            currency = obj.client.order_b2c_currency
            if currency == CURRENCY_CHOICES.RUR:
                currency = 1
            else:
                try:
                    currency = CurrencyRate.objects.get(date=datetime.date.today(),
                                                        currency=obj.client.order_b2c_currency).rate
                except CurrencyRate.DoesNotExist:
                    currency = 1
            old_price = oi.price
            new_price = (oi.price + (oi.price / 100) * obj.markup_percentage) / currency
        else:
            old_price = oi.recommended_retail_price
            new_price = oi.recommended_retail_price + (oi.recommended_retail_price / 100) * obj.markup_percentage

        new_price = math.ceil(new_price / 100) * 100
        if new_price:
            new_price -= 1

        item = PriceSetItem(
            price_set=obj,
            order_item=oi,
            new_price=new_price,
            old_price=old_price,
        )
        item.save()
        return Response({
            'status': 'ok'
        })

    @detail_route(methods=['post'])
    def re_price_set(self, request, pk=None):
        obj = self.get_object()

        currency = obj.client.order_b2c_currency
        if currency == CURRENCY_CHOICES.RUR:
            currency = 1
        else:
            try:
                currency = CurrencyRate.objects.get(date=datetime.date.today(),
                                                    currency=obj.client.order_b2c_currency).rate
            except CurrencyRate.DoesNotExist:
                currency = 1

        for item in obj.items.all():
            oi = item.order_item
            if oi:
                if obj.price_set_kind == obj.PRICE_SET_KIND.from_order_item:
                    old_price = oi.price
                    new_price = (oi.price + (oi.price / 100) * obj.markup_percentage) / currency
                else:
                    old_price = oi.recommended_retail_price
                    new_price = oi.recommended_retail_price + (oi.recommended_retail_price / 100) * obj.markup_percentage

                new_price = math.ceil(new_price / 100) * 100
                if new_price:
                    new_price -= 1

                item.new_price = new_price
                item.old_price = old_price
                item.save()
        return Response({
            'status': 'ok'
        })

    @detail_route(methods=['post'])
    def add_product_collection(self, request, pk=None):
        obj = self.get_object()
        product_collection_id = int(request.data.get('product_collection_id', None))

        try:
            product_collection = ProductCollection.objects.get(id=product_collection_id)
        except ProductCollection.DoesNotExist:
            return Response({'error': 'Коллекции с таким id не существует'},
                            status=status.HTTP_400_BAD_REQUEST)

        product_ids = Remain.objects.filter(
            warehouse__client=obj.client,
            value__gt=0
        ).values_list(
            'product_id', flat=True
        ).distinct()

        good_statuses = OrderStatus.objects.filter(
            code__in=[
                'ready_to_send',
                'shipped',
                'received',
            ]
        )
        in_order_ids = OrderItem.objects.filter(
            order__client=obj.client,
            order__status__in=good_statuses
        ).values_list(
            'product_id', flat=True
        ).distinct()

        exclude_ids = PriceSetItem.objects.filter(
            price_set=obj).values_list('product_id', flat=True)

        product_qs = Product.objects.filter(id__in=product_ids, main_collection=product_collection)
        product_qs = product_qs.filter(id__in=in_order_ids)
        if exclude_ids:
            product_qs = product_qs.exclude(id__in=exclude_ids)

        for product in product_qs:
            oi = OrderItem.objects.filter(
                order__client=obj.client,
                order__status__in=good_statuses,
                product_id=product.id
            ).order_by('order__created').first()

            if obj.price_set_kind == obj.PRICE_SET_KIND.from_order_item:
                old_price = oi.price
                new_price = oi.price + (oi.price / 100) * obj.markup_percentage
            else:
                old_price = oi.recommended_retail_price
                new_price = oi.recommended_retail_price + (oi.recommended_retail_price / 100) * obj.markup_percentage

            new_price = math.ceil(new_price / 100) * 100
            if new_price:
                new_price -= 1

            item = PriceSetItem(
                price_set=obj,
                order_item=oi,
                new_price=new_price,
                old_price=old_price,
            )
            item.save()
        return Response({
            'status': 'ok'
        })

    @detail_route(methods=['post'])
    def file_upload(self, request, pk=None):
        obj = self.get_object()
        file_obj = request.FILES['file']

        errors = []

        try:
            wb = load_workbook(file_obj)

            data = {}
            i = 0
            for row in wb.worksheets[0].rows:
                try:
                    data[row[0].value] = Decimal(row[1].value)
                except (UnicodeEncodeError, ValueError, TypeError):
                    errors.append('Строка: {}. Данные не соответствуют ожидаемым'.format(i))
                i += 1
        except Exception as e:
            return Response({'error': 'Проблема с загрузкой товаров.'},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            for product_id, new_price in data.items():
                try:
                    oi = OrderItem.objects.filter(order__client=obj.client, product_id=product_id)[0]
                except IndexError:
                    errors.append('Товар {}. Не было бланк-заказов.'.format(product_id))
                    continue
                try:
                    new_price = math.ceil(new_price / 100) * 100
                    if new_price:
                        new_price -= 1
                    psi = PriceSetItem(
                        price_set=obj,
                        order_item=oi,
                        new_price=new_price
                    )
                    psi.save()
                except IntegrityError:
                    errors.append('Товар {}. Уже добавлен в текущий документ установки цен.'.format(product_id))
                except Exception:
                    errors.append('Товар {}. По неизвестной причине добавить не удалось.'.format(product_id))
        return Response({
            'status': 'ok',
            'errors': errors
        })
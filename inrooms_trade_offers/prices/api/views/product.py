import django_filters
from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet

from clients.models import Client
from orders.models.order import OrderStatus, OrderItem
from prices.models import PriceSet, PriceSetItem
from products.models import Product, Remain
from warehouses.models import Warehouse
from ..serializers import ProductSerializer


class ProductFilterSet(django_filters.FilterSet):

    class Meta:
        model = Product
        fields = [
            'main_collection',
        ]


class ProductViewSet(ListModelMixin,
                     GenericViewSet):

    serializer_class = ProductSerializer
    filter_class = ProductFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter, )
    search_fields = ('=article', 'name', '=id')
    queryset = Product.objects.order_by('-modified')
    ordering_fields = (
        'id',
        'name',
        'article',
        'created',
        'main_collection__name',
        'modified',
        'price',
    )

    def dispatch(self, request, price_set_pk: int, *args, **kwargs):
        self.price_set = get_object_or_404(PriceSet, pk=price_set_pk)
        return super(ProductViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        product_ids = Remain.objects.filter(
            warehouse__client=self.price_set.client,
            value__gt=0
        ).values_list(
            'product_id', flat=True
        ).distinct()

        good_statuses = OrderStatus.objects.filter(
            code__in=[
                'ready_to_send',
                'shipped',
                'received',
            ]
        )
        in_order_ids = OrderItem.objects.filter(
            order__client=self.price_set.client,
            order__status__in=good_statuses
        ).values_list(
            'product_id', flat=True
        ).distinct()

        exclude_ids = PriceSetItem.objects.filter(
            price_set=self.price_set).values_list('product_id', flat=True)

        qs = Product.objects.filter(id__in=product_ids)
        qs = qs.filter(id__in=in_order_ids)
        if exclude_ids:
            qs = qs.exclude(id__in=exclude_ids)
        qs = qs.select_related(
            'color',
        )
        qs = qs.prefetch_related(
            'images'
        )
        qs = qs.order_by('-modified')
        return qs

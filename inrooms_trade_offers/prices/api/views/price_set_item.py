from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import PriceSetItemSerializer
from ...models import PriceSetItem, PriceSet


class PriceSetItemViewSet(ModelViewSet):
    serializer_class = PriceSetItemSerializer
    model = PriceSetItem
    queryset = PriceSetItem.objects.all()
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )
    search_fields = ('=order_item__product__id', '^order_item__product__article', 'order_item__product__name')
    ordering_fields = (
        'order_item__product__article',
        'order_item__product__main_collection__name',
    )

    def dispatch(self, request, price_set_pk: int, *args, **kwargs):
        self.price_set = get_object_or_404(PriceSet, pk=price_set_pk)
        return super(PriceSetItemViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super(PriceSetItemViewSet, self).get_queryset()
        qs = qs.filter(price_set=self.price_set)
        qs = qs.select_related('order_item',
                               'order_item__product',
                               'order_item__product__color', )
        qs = qs.prefetch_related('order_item__product__images')
        return qs

from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import PriceSetViewSet, PriceSetItemViewSet, ProductViewSet, ProductCollectionViewSet

router = DefaultRouter()
router.register(r'price_set/(?P<price_set_pk>\d+)/items', PriceSetItemViewSet, base_name='price_set_items'),
router.register(r'price_set/(?P<price_set_pk>\d+)/product_search', ProductViewSet, base_name='product_search'),
router.register(r'price_set/(?P<price_set_pk>\d+)/product_collection_search', ProductCollectionViewSet, base_name='product_collection_search'),
router.register(r'price_set', PriceSetViewSet, base_name='price_set')


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
]

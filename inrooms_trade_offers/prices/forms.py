from autocomplete_light import shortcuts as autocomplete_light
from django import forms


class PriceSetAdminForm(forms.ModelForm):
    class Meta:
        widgets = {
            'client': autocomplete_light.ChoiceWidget('ClientPartnerAutocomplete'),
        }


class PriceSetItemAdminForm(forms.ModelForm):
    class Meta:
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
        }

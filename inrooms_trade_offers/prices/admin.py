from django.contrib import admin

from .forms import PriceSetAdminForm, PriceSetItemAdminForm
from .models import PriceSet, PriceSetItem


class PriceSetItemInline(admin.TabularInline):
    model = PriceSetItem
    form = PriceSetItemAdminForm
    raw_id_fields = ['order_item', ]


class PriceSetAdmin(admin.ModelAdmin):
    list_display = ('id', 'client', 'source', 'price_set_kind', 'created', 'modified', )
    list_filter = ('price_set_kind', 'price_set_kind', )
    form = PriceSetAdminForm
    inlines = [PriceSetItemInline, ]


for model_or_iterable, admin_class in (
    (PriceSet, PriceSetAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

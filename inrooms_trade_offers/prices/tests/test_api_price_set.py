from django.core.urlresolvers import reverse
from freezegun.api import freeze_time
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from clients.factories import ClientFactory
from ..factories import PriceSetFactory
from ..models import PriceSet


@freeze_time("2016-1-31")
class PriceSetViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(PriceSetViewTestCase, cls).setUpClass()
        cls.list_url = reverse('api:prices:price_set-list')

        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')

        cls.client_1 = ClientFactory(name='client name')
        cls.price_set = PriceSetFactory(
            client=cls.client,
        )

        cls.valid_create_data = {
            'client': cls.client_1.id,
        }

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_create(self):
        response = self.client_bm.post(self.list_url, self.valid_create_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client_pa.post(self.list_url, self.valid_create_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_list(self):
        response = self.client_bm.get(self.list_url)
        self.assertEqual(response.data['count'], 1)
        response = self.client_pa.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client_bm.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

        object_as_dict = response.data['results'][0]
        valid_list_item_dict = {
            "id": self.price_set.id,
            "client": self.client_1.id,
            "client_detail": {
                "id": self.client_1.id,
                "name": "client name"
            },
            "source": 1,
            "source_detail": {
                "name": "Создан автомитически",
                "id": 1
            },
            "price_set_kind": 1,
            "price_set_kind_detail": {
                "name": "От стомости отгрузки партнеру",
                "id": 1
            },
            "created": "2016-01-31T00:00:00Z",
            "modified": "2016-01-31T00:00:00Z"
        }
        self.assertDictEqual(object_as_dict, valid_list_item_dict)

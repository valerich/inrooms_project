from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^invoice/(?P<object_id>\d+)/export/$', views.InvoiceExport.as_view(), name='invoice-export'),
]

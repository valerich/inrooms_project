import datetime

from django.shortcuts import get_object_or_404
from pytils import numeral
from wkhtmltopdf.views import PDFTemplateView

from .models import Invoice


class InvoiceExport(PDFTemplateView,):
    template_name = 'invoices/invoice_pdf.html'
    filename = 'invoice.pdf'
    filename_docx = 'invoice.docx'

    def dispatch(self, request, object_id, *args, **kwargs):
        self.object = get_object_or_404(Invoice, id=object_id)
        self.print_pechat = True if request.GET.get('print_pechat', None) else False
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        items = []
        items_part = []
        qs = self.object.items.all()
        for item in qs:
            items_part.append(item)
            if (not items and len(items_part) >= 5) or (item and len(items_part) >= 6):
                items.append(items_part)
                items_part = []
        if items_part:
            items.append(items_part)
        context['print_pechat'] = self.print_pechat
        context['invoice'] = self.object
        context['items'] = items
        context['vat_price'] = self.object.get_vat_price()
        context['total_price'] = self.object.get_price() + context['vat_price']
        context['total_price_left'] = int(context['total_price'])
        context['total_price_right'] = int((context['total_price'] - int(context['total_price'])) * 100)
        context['total_price_string'] = numeral.rubles(context['total_price'], zero_for_kopeck=True)
        context['footer_on_new_page'] = False
        if len(items) > 1:
            if len(items[-1]) >= 6:
                context['footer_on_new_page'] = True
        elif len(items):
            if len(items[-1]) >= 5:
                context['footer_on_new_page'] = True
        context['pages_count'] = len(items) + (1 if context['footer_on_new_page'] else 0)
        return context

    def get_filename(self):
        today = datetime.date.today()
        return "Счет {}, {}.pdf".format(self.object.number, today.strftime("%d.%m.%Y"))

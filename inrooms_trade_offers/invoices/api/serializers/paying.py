from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import Paying


class PayingSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):

    invoice_detail = serializers.SerializerMethodField()
    department_detail = serializers.SerializerMethodField()

    class Meta:
        model = Paying
        fields = [
            'id',
            'number',
            'invoice',
            'date',
            'amount',
            'invoice_detail',
            'comment',
            'department',
            'include_to_office_plan',
            'created',
            'modified',
            'department',
            'department_detail',
        ]

    def get_invoice_detail(self, obj):
        return {
            'id': obj.invoice.id,
            'number': obj.invoice.number,
            'customer': {
                'id': obj.invoice.customer.id,
                'name': obj.invoice.customer.name,
            },
            'category_detail': {
                'id': obj.invoice.category,
                'name': obj.invoice.get_category_display(),
            },
            'kind_detail': {
                'id': obj.invoice.kind,
                'name': obj.invoice.get_kind_display(),
            }
        }

    def get_department_detail(self, obj):
        return {
            'id': obj.department,
            'name': obj.get_department_display(),
            'short_name': obj.get_department_display()[0]
        }
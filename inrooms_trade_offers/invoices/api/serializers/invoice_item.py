from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import InvoiceItem


class InvoiceItemSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = InvoiceItem
        fields = [
            'id',
            'product_name',
            'count',
            'price',
            'unit',
        ]

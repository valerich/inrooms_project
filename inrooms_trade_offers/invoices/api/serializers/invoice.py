from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from clients.api.serializers import ClientSerializer
from invoices.models import INVOICE_STATUS
from ...models import Invoice


class InvoiceSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    provider_detail = serializers.SerializerMethodField()
    customer_detail = serializers.SerializerMethodField()
    status_detail = serializers.SerializerMethodField()
    total_price = serializers.SerializerMethodField()
    category_detail = serializers.SerializerMethodField()
    kind_detail = serializers.SerializerMethodField()

    class Meta:
        model = Invoice
        fields = [
            'id',
            'number',
            'provider',
            'provider_detail',
            'customer',
            'customer_detail',
            'vat_rate',
            'total_price',
            'created',
            'modified',
            'status',
            'status_detail',
            'amount_paid',
            'category',
            'category_detail',
            'comment',
            'kind',
            'kind_detail',
            'payment_perion_month',
            'payment_perion_year',
        ]

    def get_provider_detail(self, obj):
        return ClientSerializer(obj.provider).data

    def get_customer_detail(self, obj):
        return ClientSerializer(obj.customer).data

    def get_status_detail(self, obj):
        return {
            'id': obj.status,
            'name': obj.get_status_display(),
        }

    def get_total_price(self, obj):
        return str(obj.total_price)

    def validate_status(self, data):
        if self.instance:
            if self.instance.status != data:
                if not self.context['request'].user.has_perm('auth.can_change_invoice_status'):
                    raise ValidationError('Вы не имеете права выставлять данный статус')
        else:
            if data != INVOICE_STATUS.not_paid:
                raise ValidationError('Вы не имеете права выставлять данный статус')
        return data

    def validate_amount_paid(self, data):
        if self.instance:
            if self.instance.amount_paid != data:
                if not self.context['request'].user.has_perm('auth.can_change_invoice_status'):
                    raise ValidationError('Вы не имеете менять сумму оплачено')
        else:
            if data > 0:
                raise ValidationError('Вы не имеете менять сумму оплачено')
        return data

    def get_category_detail(self, obj):
        return {
            'id': obj.category,
            'name': obj.get_category_display(),
        }

    def get_kind_detail(self, obj):
        return {
            'id': obj.kind,
            'name': obj.get_kind_display(),
        }

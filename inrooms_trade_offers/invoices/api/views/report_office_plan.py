import calendar
import datetime
import os
import tempfile
from collections import defaultdict

from django.http.response import HttpResponse
from decimal import Decimal

from rest_framework import filters, serializers
from rest_framework.response import Response
from rest_framework.status import HTTP_403_FORBIDDEN
from rest_framework.views import APIView

from core.utils.constants import DEPARTMENT_CHOICE
from core.utils.excel import NewExcelHelper
from invoices.models import Paying, Invoice, OfficePlan


class ReportSerializer(serializers.Serializer):
    year = serializers.IntegerField(required=False)


class ReportOfficePlanView(APIView):

    def get(self, request, *args, **kwargs):
        if self.request.user.has_perm('auth.view_report_office_plan'):
            serializer = ReportSerializer(data=request.GET)
            serializer.is_valid(True)
            self.user = request.user
            self.year = serializer.validated_data.get('year', datetime.date.today().year)
            data = self.get_report_data()
            return Response(data=data)
        return Response(status=HTTP_403_FORBIDDEN)

    def get_report_data(self):
        self.today = datetime.date.today()
        office_plan = self.get_office_plan_data()
        office_plan = self.get_office_fact_data(office_plan)
        office_plan = self.get_percent_data(office_plan)
        return office_plan

    def get_percent_data(self, office_plan):
        for department, data in office_plan.items():
            for i in range(1, 13):
                plan = data['month_plan_{}'.format(i)]
                fact = data['month_fact_{}'.format(i)]
                if plan:
                    data['month_percent_{}'.format(i)] = fact / plan * 100
                else:
                    data['month_percent_{}'.format(i)] = None
            if data['today_plan']:
                data['today_percent'] = data['today_fact'] / data['today_plan'] * 100
            else:
                data['today_fact']
                data['today_percent'] = None
            if data['plan'] != 0:
                data['percent'] = data['fact'] / data['plan'] * 100
            else:
                data['fact']
                data['percent'] = None
        return office_plan

    def get_office_fact_data(self, office_plan):
        for paying in Paying.objects.filter(date__year=self.year, include_to_office_plan=True):
            office_plan[paying.department]['month_fact_{}'.format(paying.date.month)] += paying.amount
            office_plan[paying.department]['fact'] += paying.amount
            if self.today.month > paying.date.month:
                office_plan[paying.department]['today_fact'] += paying.amount
            elif self.today.month == paying.date.month and self.today.day >= paying.date.day:
                office_plan[paying.department]['today_fact'] += paying.amount
        return office_plan

    def get_office_plan_data(self):
        data = {}
        for department in DEPARTMENT_CHOICE:
            department_data = defaultdict(Decimal)
            department_plan = Decimal('0.00')
            today_plan = Decimal('0.00')
            try:
                office_plan = OfficePlan.objects.get(year=self.year, department=department[0])
            except OfficePlan.DoesNotExist:
                pass
            else:
                for i in range(1, 13):
                    month_plan = getattr(office_plan, 'month_{}'.format(i))
                    department_data['month_plan_{}'.format(i)] = month_plan
                    department_plan += month_plan
                    if self.today.year == office_plan.year:
                        if self.today.month > i:
                            today_plan += month_plan
                        elif self.today.month == i:
                            today_plan += (month_plan / calendar.monthrange(office_plan.year, i)[1]) * self.today.day
            department_data['plan'] = department_plan
            department_data['today_plan'] = today_plan
            data[department[0]] = department_data
        return data

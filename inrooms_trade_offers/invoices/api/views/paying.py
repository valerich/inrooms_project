import django_filters
from django.db.models.aggregates import Sum
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from core.utils.views.additional_data_pagination import AdditionalDataPaginatedViewMixin
from ..serializers import PayingSerializer
from ...models import Paying


def filter_date_year(queryset, value):
    if value:
        queryset = queryset.filter(date__year=value)
    return queryset


def filter_date_month(queryset, value):
    if value:
        queryset = queryset.filter(date__month=value)
    return queryset


class PayingFilterSet(django_filters.FilterSet):
    date_year = django_filters.NumberFilter(action=filter_date_year)
    date_month = django_filters.NumberFilter(action=filter_date_month)

    class Meta:
        model = Paying
        fields = (
            'invoice',
            'invoice__customer',
            'department',
            'date_year',
            'date_month',
            'include_to_office_plan',
            'invoice__kind'
        )


class PayingViewSet(AdditionalDataPaginatedViewMixin, ModelViewSet):
    serializer_class = PayingSerializer
    filter_class = PayingFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter)
    search_fields = ('=id', 'number')
    queryset = Paying.objects.all().order_by('-date')
    ordering_fields = (
        'id',
        'number',
        'created',
        'modified',
        'date',
    )

    def get_additional_data(self, queryset):
        data = queryset.aggregate(Sum('amount'))
        return {
            'amount_sum': data['amount__sum'],
        }

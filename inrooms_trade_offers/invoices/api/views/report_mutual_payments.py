import datetime
import os
import tempfile
import operator
from collections import defaultdict

from django.http.response import HttpResponse
from decimal import Decimal

from pytils import numeral
from rest_framework import filters, serializers
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.status import HTTP_403_FORBIDDEN, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from wkhtmltopdf.views import PDFTemplateView

from clients.models import Client
from core.utils.constants import ORDER_CATEGORY_CHOICE, INVOICE_KIND_CHOICES
from core.utils.excel import NewExcelHelper
from core.utils.lists import extended_grouping
from invoices.models import Paying, Invoice
from orders.models.order_b2c import OrderReturn


class ReportSerializer(serializers.Serializer):
    client = serializers.IntegerField(required=False)
    category = serializers.ChoiceField(choices=ORDER_CATEGORY_CHOICE, required=False)
    kind = serializers.ChoiceField(choices=INVOICE_KIND_CHOICES, required=False)
    include_returns = serializers.BooleanField(required=False)

    def validate(self, data):
        client = data.get('client', None)
        user = self.context['user']
        if user.has_perm('auth.view_report_mutual_payments_all') and client:
            pass
        elif user.has_perm('auth.view_report_mutual_payments') and user.client:
            data['client'] = user.client_id
        else:
            raise ValidationError({'client': 'Партнер обязателен для заполнения'})
        return data


class ReportMixin():

    def set_params(self, request):
        self.serializer = ReportSerializer(data=request.GET, context={'user': request.user})
        self.serializer.is_valid(True)
        self.user = request.user
        self.client = self.serializer.validated_data['client']
        self.category = self.serializer.validated_data.get('category', None)
        self.kind = self.serializer.validated_data.get('kind', None)
        self.include_returns = self.serializer.validated_data.get('include_returns', True)

    def get_client_obj(self):
        try:
            return Client.objects.get(id=self.client)
        except Client.DoesNotExist:
            return None

    def get_raw_payings_data(self):
        amoung_summ = Decimal('0.00')
        raw_payings = defaultdict(list)

        qs = Paying.objects.filter(invoice__customer=self.client).select_related('invoice')
        if self.category:
            qs = qs.filter(invoice__category=self.category)
        if self.kind:
            qs = qs.filter(invoice__kind=self.kind)
        for paying in qs:
            amount = paying.amount
            if paying.comment:
                comment = paying.comment
            else:
                comment = "Предоплата за товар {}".format(paying.date.strftime('%d.%m.%Y'))
            raw_payings[paying.date].append({'date': paying.date.strftime('%d.%m.%Y'), 'amount': amount, 'comment': comment, 'category': paying.invoice.category})
            amoung_summ += amount

        return raw_payings, amoung_summ

    def get_raw_returns_data(self):
        amoung_summ = Decimal('0.00')
        raw_returns = defaultdict(list)
        if self.include_returns:
            qs = OrderReturn.objects.filter(client=self.client, category__isnull=False)
            if self.category:
                qs = qs.filter(category=self.category)
            if self.kind and int(self.kind) != INVOICE_KIND_CHOICES.default:
                qs = qs.none()
            for order_return in qs:
                amount = order_return.amount_full
                comment = "Возврат №{} от {}".format(order_return.number, order_return.date.strftime('%d.%m.%Y'))
                raw_returns[order_return.date].append(
                    {'date': order_return.date.strftime('%d.%m.%Y'), 'amount': amount, 'comment': comment, 'category': order_return.category})
                amoung_summ += amount
        return raw_returns, amoung_summ

    def get_raw_invoices_data(self):
        amoung_summ = Decimal('0.00')
        raw_invoices = defaultdict(list)
        qs = Invoice.objects.filter(customer=self.client).order_by('-created')
        qs = qs.prefetch_related('items')
        if self.category:
            qs = qs.filter(category=self.category)
        if self.kind:
            qs = qs.filter(kind=self.kind)
        for invoice in qs:
            amount = invoice.get_price()
            if invoice.comment:
                comment = invoice.comment
            else:
                comment = "Бланк-заказ №{} от {}".format(invoice.number, invoice.created.strftime('%d.%m.%Y'))
            raw_invoices[invoice.created.date()].append({'date': invoice.created.strftime('%d.%m.%Y'), 'amount': amount, 'comment': comment, 'category': invoice.category})
            amoung_summ += amount

        return raw_invoices, amoung_summ


class ReportMutualPaymentsView(ReportMixin, APIView):

    def get(self, request, *args, **kwargs):
        if self.request.user.has_perm('auth.view_report_mutual_payments'):
            self.set_params(request)
            data = self.get_report_data()
            if request.GET.get('as', None) == 'xls':
                xls_helper = Report(data=data).get_data()
                fd, fn = tempfile.mkstemp()
                os.close(fd)
                xls_helper.book.save(fn)
                fh = open(fn, 'rb')
                resp = fh.read()
                fh.close()
                response = HttpResponse(resp, content_type='application/ms-excel')
                response['Content-Disposition'] = 'attachment; filename=report.xlsx'
                return response
            return Response(data=data)
        return Response(status=HTTP_403_FORBIDDEN)

    def get_report_data(self):
        payings, paying_sum = self.get_payings_data()
        invoices, invoice_sum = self.get_invoices_data()
        itogo = paying_sum - invoice_sum
        return {
            'payings': payings,
            'payings_sum': paying_sum,
            'invoices': invoices,
            'invoices_sum': invoice_sum,
            'itogo': itogo
        }

    def get_payings_data(self):
        payings = []

        raw_payings, payings_amoung_summ = self.get_raw_payings_data()
        raw_returns, returns_amount_summ = self.get_raw_returns_data()

        payings_amoung_summ += returns_amount_summ

        for date in sorted(set(raw_payings.keys()).union(set(raw_returns.keys())), reverse=True):
            for item in raw_payings[date]:
                payings.append(item)
            if raw_returns[date]:
                return_item = {
                    'amount': 0,
                    'comment': 'Возврат',
                    'date': date.strftime('%d.%m.%Y'),
                    'items': []
                }
                for item in raw_returns[date]:
                    return_item['amount'] += item['amount']
                    return_item['items'].append(item)
                payings.append(return_item)

        return payings, payings_amoung_summ

    def get_invoices_data(self):
        invoices = []

        raw_invoices, amoung_summ = self.get_raw_invoices_data()

        for date in sorted(raw_invoices.keys(), reverse=True):
            for item in raw_invoices[date]:
                invoices.append(item)
        return invoices, amoung_summ


class Report(object):

    def __init__(self, data, with_images=False):
        self.data = data

    def get_data(self):
        return self.get_xls_helper()

    def get_xls_helper(self):
        xls_helper = NewExcelHelper()
        xls_helper.add_page('Взаиморасчеты')

        xls_helper.sheet.column_dimensions['B'].width = 50
        xls_helper.sheet.column_dimensions['D'].width = 50

        xls_helper.write_cell('Приход средств', 0, 0)
        xls_helper.write_cell('Основание', 0, 1)
        xls_helper.write_cell('Расход средств', 0, 2)
        xls_helper.write_cell('Основание', 0, 3)

        row = 1

        for item in self.data['payings']:
            xls_helper.write_cell(item['amount'], row, 0)
            xls_helper.write_cell(item['comment'], row, 1)
            row += 1

        row = 1

        for item in self.data['invoices']:
            xls_helper.write_cell(item['amount'], row, 2)
            xls_helper.write_cell(item['comment'], row, 3)

            row += 1

        row = max([
            len(self.data['payings']),
            len(self.data['invoices']),
        ]) + 2

        xls_helper.write_cell(self.data['payings_sum'], row, 0)
        xls_helper.write_cell(self.data['invoices_sum'], row, 2)

        row += 3

        xls_helper.write_cell('Итог', row, 1)
        xls_helper.write_cell(self.data['itogo'], row, 2)
        if self.data['itogo'] < 0:
            xls_helper.write_cell('Клиент должен', row, 3)
        elif self.data['itogo'] > 0:
            xls_helper.write_cell('Мы должны', row, 3)

        return xls_helper


class ReportMutualPaymentsExport(ReportMixin, PDFTemplateView,):
    template_name = 'invoices/mutual_payment_pdf.html'
    filename = 'mutual_payment.pdf'

    cmd_options = {
        'orientation': 'landscape',
        'zoom': 1.3
    }

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.has_perm('auth.view_report_mutual_payments'):
            try:
                self.set_params(request)
            except ValidationError:
                return Response(data=self.serializer.errors, status=HTTP_400_BAD_REQUEST)
            self.print_pechat = True if request.GET.get('print_pechat', None) else False
            self.data = self.get_report_data()
            return super().dispatch(request, *args, **kwargs)
        return Response(status=HTTP_403_FORBIDDEN)

    def get_report_data(self):
        payings, paying_sum = self.get_raw_payings_data()
        returns, returns_sum = self.get_raw_returns_data()
        invoices, invoice_sum = self.get_raw_invoices_data()

        dates = set(payings.keys())
        dates.update(set(invoices.keys()))
        dates.update(set(returns.keys()))

        items = []
        for date in sorted(dates):
            for invoice in invoices[date]:
                items.append({
                    'obj': invoice,
                    'kind': 'invoice',
                    'date': date,
                })
            for paying in payings[date]:
                items.append({
                    'obj': paying,
                    'kind': 'paying',
                    'date': date,
                })
            for order_return in returns[date]:
                items.append({
                    'obj': order_return,
                    'kind': 'paying',
                    'date': date,
                })

        itogo = returns_sum + paying_sum - invoice_sum
        return {
            'items': items,
            'payings_sum': paying_sum + returns_sum,
            'invoices_sum': invoice_sum,
            'itogo': itogo
        }

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        # context.update(self.data)
        context['client'] = self.get_client_obj()
        context['print_pechat'] = self.print_pechat

        context['payings_sum'] = self.data['payings_sum']
        context['invoices_sum'] = self.data['invoices_sum']

        context['itogo'] = self.data['itogo']
        context['positive_itogo'] = self.data['itogo'] if self.data['itogo'] >= 0 else -1 * self.data['itogo']
        context['itogo_string'] = numeral.rubles(context['positive_itogo'], zero_for_kopeck=True)
        context['items'] = extended_grouping(self.data['items'], 36, 26)
        context['footer_on_new_page'] = False
        if len(context['items']) > 1:
            if len(context['items'][-1]) >= 23:
                context['footer_on_new_page'] = True
        elif len(context['items']):
            if len(context['items'][-1]) >= 10:
                context['footer_on_new_page'] = True
        context['pages_count'] = len(context['items']) + (1 if context['footer_on_new_page'] else 0)

        return context

    # def get_filename(self):
    #     today = datetime.date.today()
    #     return "Счет {}, {}.pdf".format(self.object.number, today.strftime("%d.%m.%Y"))

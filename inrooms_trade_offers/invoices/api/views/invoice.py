import django_filters
from django.db.models.aggregates import Sum
from django.utils import timezone
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from core.utils.views.additional_data_pagination import AdditionalDataPaginatedViewMixin
from history.models import HistoryItem
from ..serializers import InvoiceSerializer
from ...models import Invoice


def filter_to_create_paying(queryset, value):
    if value:
        queryset = queryset.filter(status__in=[Invoice.STATUS.not_paid, Invoice.STATUS.partially_paid])
    else:
        queryset = queryset.exclude(status__in=[Invoice.STATUS.not_paid, Invoice.STATUS.partially_paid])
    return queryset


class InvoiceFilterSet(django_filters.FilterSet):
    to_create_paying = django_filters.BooleanFilter(action=filter_to_create_paying)

    class Meta:
        model = Invoice
        fields = [
            'to_create_paying',
            'provider',
            'customer',
            'status',
            'kind',
        ]


class InvoiceViewSet(AdditionalDataPaginatedViewMixin, ModelViewSet):
    serializer_class = InvoiceSerializer
    filter_class = InvoiceFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter)
    search_fields = ('=id', 'number', 'customer__name')
    ordering_fields = (
        'id',
        'number',
        'created',
        'modified',
        'provider__name',
        'customer__name',
    )

    def get_queryset(self):
        qs = Invoice.objects.filter(is_active=True)
        qs = qs.select_related('customer', 'provider')
        return qs

    def perform_create(self, serializer):
        serializer.save()
        history_item = HistoryItem.objects.create(
            kind=HistoryItem.KIND.invoice_create,
            content_object=serializer.instance,
            body='Пользователь {} {} создал счет №{}'.format(
                self.request.user.name,
                timezone.now().strftime('%d.%m.%Y %H:%M'),
                serializer.instance.id,
            ),
        )

    def get_additional_data(self, queryset):
        data = queryset.aggregate(Sum('total_price'))
        return {
            'total_price_sum': data['total_price__sum'],
        }

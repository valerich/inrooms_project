from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import InvoiceItemSerializer
from ...models import Invoice, InvoiceItem


class InvoiceItemViewSet(ModelViewSet):
    serializer_class = InvoiceItemSerializer
    filter_backends = (filters.SearchFilter, )
    search_fields = ('product_name', )
    queryset = InvoiceItem.objects.all()
    ordering_fields = (
        'id',
        'product_name',
        'quantity',
        'price'
    )

    def dispatch(self, request, invoice_pk: int, *args, **kwargs):
        self.invoice = get_object_or_404(Invoice, pk=invoice_pk)
        return super(InvoiceItemViewSet, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        instance = serializer.save(invoice=self.invoice)
        self.invoice.save()
        return instance

    def perform_update(self, serializer):
        super().perform_update(serializer)
        self.invoice.save()

    def perform_destroy(self, instance):
        super().perform_destroy()
        self.invoice.save()

    def get_queryset(self):
        qs = super(InvoiceItemViewSet, self).get_queryset()
        qs = qs.filter(invoice=self.invoice)
        return qs

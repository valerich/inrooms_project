from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import (
    InvoiceItemViewSet,
    InvoiceViewSet,
    PayingViewSet,
    ReportMutualPaymentsView,
    ReportOfficePlanView,
    ReportMutualPaymentsExport,
)

router = DefaultRouter()
router.register(r'invoice', InvoiceViewSet, base_name='invoice')
router.register(r'invoice/(?P<invoice_pk>\d+)/items', InvoiceItemViewSet, base_name='invoice_item'),
router.register(r'paying', PayingViewSet, base_name='paying')


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
    urls.url(r'^reports/mutual_payments/export/$', ReportMutualPaymentsExport.as_view(), name='report_mutual_payments_export'),
    urls.url(r'^reports/mutual_payments/$', ReportMutualPaymentsView.as_view(), name='report_mutual_payments'),
    urls.url(r'^reports/office_plan/$', ReportOfficePlanView.as_view(), name='report_office_plan'),
]

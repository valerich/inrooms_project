from autocomplete_light import shortcuts as autocomplete_light
from django import forms


class InvoiceAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'provider': autocomplete_light.ChoiceWidget('ClientAutocomplete'),
            'customer': autocomplete_light.ChoiceWidget('ClientAutocomplete'),
        }


class PayingAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'invoice':  autocomplete_light.ChoiceWidget('InvoiceAutocomplete')
        }

from django.contrib import admin

from core.admin import NotDeleteModelAdmin
from .forms import InvoiceAdminForm, PayingAdminForm
from .models import Invoice, InvoiceItem, Paying, OfficePlan


class InvoiceItemInline(admin.TabularInline):
    model = InvoiceItem
    extra = 0


class InvoiceAdmin(NotDeleteModelAdmin):
    list_display = ('id', 'number', 'provider', 'customer', 'created', 'modified', )
    search_fields = ('=id', 'number', )
    form = InvoiceAdminForm
    inlines = [InvoiceItemInline, ]


class PayingAdmin(admin.ModelAdmin):
    list_display = ('id', 'number', 'invoice', 'created', 'modified', )
    search_fields = ('=id', 'number', )
    form = PayingAdminForm


class OfficePlanAdmin(admin.ModelAdmin):
    list_display = ('year', 'department')
    search_fields = ('year', )
    list_filter = ('department', )


for model_or_iterable, admin_class in (
    (Invoice, InvoiceAdmin),
    (Paying, PayingAdmin),
    (OfficePlan, OfficePlanAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'invoices'
    verbose_name = 'Счета'

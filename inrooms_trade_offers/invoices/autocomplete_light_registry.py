from autocomplete_light import shortcuts as autocomplete_light

from .models import (
    Invoice,
)


class InvoiceAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['id', 'number']
    attrs = {
        'placeholder': 'id, Номер',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (Invoice, InvoiceAutocomplete),
)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

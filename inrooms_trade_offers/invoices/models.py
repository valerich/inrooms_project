from decimal import Decimal

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from django.utils import timezone
from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

from core.utils.constants import ORDER_CATEGORY_CHOICE, DEPARTMENT_CHOICE, INVOICE_KIND_CHOICES

VAT_RATE_CHOICES = Choices(
    (0, '_0', 'Без НДС'),
    (18, '_18', '18%'),
)


INVOICE_STATUS = Choices(
    (0, 'not_paid', 'Не оплачен'),
    (10, 'partially_paid', 'Частично оплачен'),
    (100, 'paid', 'Оплачен'),
)


def default_invoice_month():
    return timezone.now().month


def default_invoice_year():
    return timezone.now().year


class Invoice(TimeStampedModel):
    STATUS = INVOICE_STATUS
    VAT_RATE = VAT_RATE_CHOICES
    ORDER_CATEGORY = ORDER_CATEGORY_CHOICE
    KIND = INVOICE_KIND_CHOICES

    kind = models.PositiveSmallIntegerField('Тип', choices=KIND, default=KIND.default)
    payment_perion_month = models.PositiveSmallIntegerField('Платежный период. Месяц',
                                                            default=default_invoice_month,
                                                            validators=[
                                                                MaxValueValidator(12),
                                                                MinValueValidator(1)
                                                            ])
    payment_perion_year = models.PositiveSmallIntegerField('Платежный период. Год',
                                                           default=default_invoice_year,
                                                           validators=[
                                                               MaxValueValidator(3000),
                                                               MinValueValidator(2000)
                                                           ])
    is_active = models.BooleanField('Активен?', default=True)
    status = models.PositiveSmallIntegerField('Статус', choices=STATUS, default=STATUS.not_paid)
    amount_paid = models.DecimalField('Сумма оплачено', max_digits=20, decimal_places=2, default=Decimal('0.00'))
    number = models.CharField('Номер', max_length=50, blank=True, default='')
    provider = models.ForeignKey('clients.Client', verbose_name='Поставщик',
                                   related_name='invoice_provider', on_delete=models.PROTECT)
    customer = models.ForeignKey('clients.Client', verbose_name='Получатель',
                                 related_name='invoice_customer', on_delete=models.PROTECT)
    vat_rate = models.PositiveSmallIntegerField('Ставка НДС', choices=VAT_RATE, default=VAT_RATE._0)
    category = models.PositiveSmallIntegerField('Категория', choices=ORDER_CATEGORY, default=ORDER_CATEGORY.white)

    total_price = models.DecimalField('Cумма', max_digits=20, decimal_places=10, default=0)

    comment = models.TextField('Комментарий', blank=True)

    class Meta:
        verbose_name = 'Счет'
        verbose_name_plural = 'Счета'
        ordering = ('-id', )

    def __str__(self):
        return 'Счет №:{}, id:{}'.format(self.number, self.id)

    def save(self, *args, **kwargs):
        self.set_number()
        self.total_price = self.get_total_price()
        super().save(*args, **kwargs)

    def set_number(self):
        if not self.number:
            if int(self.category) == self.ORDER_CATEGORY.white:
                numbers = [
                    int(n) for n in Invoice.objects.filter(
                        category=self.ORDER_CATEGORY.white).values_list('number', flat=True) if n.isdigit()
                ]
                if numbers:
                    self.number = str(sorted(numbers)[-1] + 1)
                else:
                    self.number = '1'
            else:
                numbers = [
                    int(n[1:]) for n in Invoice.objects.filter(
                        category=self.ORDER_CATEGORY.black).values_list('number', flat=True).exclude(number='') if n[0] == 'R' and n[1:].isdigit()
                ]
                if numbers:
                    self.number = 'R{}'.format(str(sorted(numbers)[-1] + 1))
                else:
                    self.number = 'R1'

    def get_price(self):
        price = Decimal('0.00')
        for i in self.items.all():
            price += i.get_full_price()
        return price

    def get_vat_price(self):
        price = self.get_price()
        return (price * self.vat_rate) / 100

    def get_total_price(self):
        return self.get_price() + self.get_vat_price()


UNIT_CHOICE = Choices(
    ('1', 'units', '1'),
    ('pieces', 'pieces', 'шт'),
    ('packings', 'packings', 'уп'),
)


class InvoiceItem(models.Model):
    UNIT = UNIT_CHOICE
    invoice = models.ForeignKey(Invoice, verbose_name='Счет', related_name='items', on_delete=models.CASCADE)
    product_name = models.CharField('Название товара', max_length=500)
    count = models.PositiveIntegerField('Количество')
    price = models.DecimalField('Цена (шт)', max_digits=20, decimal_places=10)
    unit = models.CharField('Единица измерения', max_length=50, choices=UNIT, default=UNIT.units)

    class Meta:
        verbose_name = 'Товар в счете'
        verbose_name_plural = 'Товары в счете'
        ordering = ('-id', )

    def __str__(self):
        return 'Товар в счете {}, id:{}'.format(self.invoice_id, self.id, )

    def get_full_price(self):
        return self.count * self.price


class Paying(TimeStampedModel):
    invoice = models.ForeignKey(Invoice, verbose_name='Счёт (Основание)')
    department = models.PositiveSmallIntegerField('Отдел', choices=DEPARTMENT_CHOICE, default=DEPARTMENT_CHOICE.development)

    number = models.CharField('Номер платежа', max_length=100)
    date = models.DateField('Дата платежа')

    include_to_office_plan = models.BooleanField('Добавлять в офис план?', default=True)
    include_to_dashboard = models.BooleanField('Включать в дашборд', default=True)

    amount = models.DecimalField('Сумма', max_digits=12, decimal_places=2)

    comment = models.TextField('Комментарий', blank=True)

    class Meta:
        verbose_name = 'Платеж'
        verbose_name_plural = 'Платежи'
        ordering = ('id', )

    def __str__(self):
        return '{}'.format(self.id)


class OfficePlan(models.Model):
    year = models.PositiveSmallIntegerField('Год')
    department = models.PositiveSmallIntegerField('Отдел', choices=DEPARTMENT_CHOICE)
    month_1 = models.DecimalField('Январь', max_digits=10, decimal_places=2, default=Decimal('0.00'))
    month_2 = models.DecimalField('Февраль', max_digits=10, decimal_places=2, default=Decimal('0.00'))
    month_3 = models.DecimalField('Март', max_digits=10, decimal_places=2, default=Decimal('0.00'))
    month_4 = models.DecimalField('Апрель', max_digits=10, decimal_places=2, default=Decimal('0.00'))
    month_5 = models.DecimalField('Май', max_digits=10, decimal_places=2, default=Decimal('0.00'))
    month_6 = models.DecimalField('Июнь', max_digits=10, decimal_places=2, default=Decimal('0.00'))
    month_7 = models.DecimalField('Июль', max_digits=10, decimal_places=2, default=Decimal('0.00'))
    month_8 = models.DecimalField('Август', max_digits=10, decimal_places=2, default=Decimal('0.00'))
    month_9 = models.DecimalField('Сентябрь', max_digits=10, decimal_places=2, default=Decimal('0.00'))
    month_10 = models.DecimalField('Октябрь', max_digits=10, decimal_places=2, default=Decimal('0.00'))
    month_11 = models.DecimalField('Ноябрь', max_digits=10, decimal_places=2, default=Decimal('0.00'))
    month_12 = models.DecimalField('Декабрь', max_digits=10, decimal_places=2, default=Decimal('0.00'))

    class Meta:
        verbose_name = 'Офис-план'
        verbose_name_plural = 'Офис-планы'
        ordering = ('-year', )

    def __str__(self):
        return '{}, {}'.format(self.year, self.get_department_display())

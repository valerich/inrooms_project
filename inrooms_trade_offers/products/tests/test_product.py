from django.db import transaction
from django.db.utils import IntegrityError
from django.test import TestCase

from accounts.factories import UserFactory
from brands.factories import BrandFactory
from products.factories import ProductCollectionFactory, ProductColorFactory, ProductFactory, RemainFactory
from ..models import Product


class ProductTestCase(TestCase):
    def setUp(self):
        Product.objects.all().delete()

    def test_product_unique_together(self):
        """Товар уникален по связке полей "Артикул", "цвет" и "Бренд" """
        color = ProductColorFactory()
        color2 = ProductColorFactory()
        brand = BrandFactory()
        user = UserFactory()
        main_collection = ProductCollectionFactory()
        Product.objects.create(article='test1', color=color, brand=brand,
                               main_collection=main_collection, user=user, name='Товар 1', )
        # С такими же артикулом, цветом и брендом товар создать нельзя
        with self.assertRaises(IntegrityError):
            with transaction.atomic():
                Product.objects.create(article='test1', color=color, brand=brand,
                                       main_collection=main_collection, user=user, name='Товар 1', )
        # А с другими можно
        Product.objects.create(article='test2', color=color, brand=brand,
                               main_collection=main_collection, user=user, name='Товар 1', )
        Product.objects.create(article='test1', color=color2, brand=brand,
                               main_collection=main_collection, user=user, name='Товар 1', )

    def test_product_remain_sum(self):
        """Поле Product.remain_sum"""
        product = ProductFactory()
        product2 = ProductFactory()
        product3 = ProductFactory()
        for i in range(2):
            RemainFactory(product=product, value=10)
            RemainFactory(product=product2, value=5)
        self.assertEqual(product.remain_sum, 20)
        self.assertEqual(product3.remain_sum, 0)

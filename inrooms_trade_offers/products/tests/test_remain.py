from django.db import transaction
from django.db.utils import IntegrityError
from django.test import TestCase

from warehouses.factories import WarehouseFactory
from ..factories import ProductFactory, SizeFactory
from ..models import Remain


class RemainTestCase(TestCase):
    def setUp(self):
        Remain.objects.all().delete()

    def test_unique_together_fields(self):
        """Остаток храним для определенного размера товара на точке"""
        product = ProductFactory()
        size = SizeFactory()
        size2 = SizeFactory()
        warehouse = WarehouseFactory()
        warehouse2 = WarehouseFactory()
        Remain.objects.create(product=product, warehouse=warehouse, size=size, value=10)
        Remain.objects.create(product=product, warehouse=warehouse2, size=size, value=10)
        Remain.objects.create(product=product, warehouse=warehouse, size=size2, value=10)
        with self.assertRaises(IntegrityError):
            with transaction.atomic():
                Remain.objects.create(product=product, warehouse=warehouse, size=size, value=10)
        with self.assertRaises(IntegrityError):
            with transaction.atomic():
                Remain.objects.create(product=product, warehouse=warehouse2, size=size, value=10)
        with self.assertRaises(IntegrityError):
            with transaction.atomic():
                Remain.objects.create(product=product, warehouse=warehouse, size=size2, value=10)

from django.test import TestCase

from ..factories import RemainFactory, SizeFactory
from ..models import Remain, Size


class SizeFactoryTestCase(TestCase):
    def setUp(self):
        Size.objects.all().delete()

    def test_size_factory(self):
        size = SizeFactory()
        size_from_db = Size.objects.all()[0]
        self.assertEqual(size, size_from_db)


class RemainFactoryTestCase(TestCase):
    def setUp(self):
        Remain.objects.all().delete()

    def test_remain_factory(self):
        remain = RemainFactory()
        remain_from_db = Remain.objects.all()[0]
        self.assertEqual(remain, remain_from_db)

from copy import copy

from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from brands.factories import BrandFactory
from offers.factories import OfferFactory
from warehouses.models import Warehouse
from ..factories import ProductCollectionFactory, ProductColorFactory, ProductFactory, RemainFactory
from ..models import Product


class ProductViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(ProductViewTestCase, cls).setUpClass()
        cls.list_url = reverse('api:products:product-list')
        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')
        ProductFactory()
        product_collection = ProductCollectionFactory()
        color = ProductColorFactory()
        brand = BrandFactory()
        cls.valid_product_data = {
            'article': '1233/123',
            'name': 'test product',
            'main_collection': product_collection.id,
            'color': color.id,
            'brand': brand.id
        }

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')
        pass

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_authentication_fail(self):
        self.client.force_authenticate(user=None)
        response = self.client.post(self.list_url)
        self.assertIn(response.status_code, [status.HTTP_403_FORBIDDEN, status.HTTP_302_FOUND])

    def test_list_product(self):
        response = self.client_bm.get(self.list_url)
        self.assertEqual(response.data['count'], 1)
        response = self.client_pa.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client_bm.get(self.list_url)
        object_dict = response.data['results'][0]
        fields = [
            'id',
            'name',
            'article',
            'color',
            'main_collection',
            'images',
            'offer',
            'price',
            'has_remains',
            'is_active',
            'remains',
            'intag_weight',
            'small_description',
        ]
        for field in fields:
            self.assertIn(field, object_dict, 'Не передается поле {} в {}'.format(field, self.list_url))

    def test_create_product(self):
        product1_data = copy(self.valid_product_data)
        product1_data['article'] = '1231/111'
        product2_data = copy(self.valid_product_data)
        product2_data['article'] = '1231/222'
        response = self.client_bm.post(self.list_url, product1_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client_pa.post(self.list_url, product2_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_product_article_mask(self):
        product1_data = copy(self.valid_product_data)
        product1_data['article'] = '1231/111'
        product2_data = copy(self.valid_product_data)
        product2_data['article'] = 'fuck'
        response = self.client_bm.post(self.list_url, product1_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client_pa.post(self.list_url, product2_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_offer_product(self):
        offer = OfferFactory()
        product1_data = copy(self.valid_product_data)
        product1_data['article'] = '1231/111'
        product1_data['offer'] = offer.id
        product2_data = copy(self.valid_product_data)
        product2_data['article'] = '1231/112'
        product2_data['offer'] = offer.id
        response = self.client_bm.post(self.list_url, product1_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client_pa.post(self.list_url, product2_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Product.objects.filter(offer_id=offer.id).count(), 2)

    def test_blank_request_data(self):
        response = self.client_bm.post(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_warehouse_remains_filter(self):
        warehouse = Warehouse.objects.get(code='chn')
        product = ProductFactory()

        # Если товаров нет на остатках точки, то не возвращаем их в ответе
        response = self.client_bm.get(self.list_url, data={'warehouse_remains': warehouse.code})
        self.assertEqual(response.data['count'], 0)

        # Если передана не существующая точка, то возвращаем пустой список
        response = self.client_bm.get(self.list_url, data={'warehouse_remains': 'fail_code!'})
        self.assertEqual(response.data['count'], 0)

        # Если на точке есть остатки, то возвращаем эти товары
        RemainFactory(product=product, warehouse=warehouse, value=1)
        response = self.client_bm.get(self.list_url, data={'warehouse_remains': warehouse.code})
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(response.data['results'][0]['id'], product.id)

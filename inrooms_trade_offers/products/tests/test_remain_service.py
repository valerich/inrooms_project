from django.test import TestCase

from core.server_init import ServerInit
from warehouses.models import Warehouse
from ..factories import ProductFactory, RemainFactory
from ..models import Size
from ..services import RemainService


class RemainServiceTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(RemainServiceTestCase, cls).setUpClass()
        si = ServerInit()
        si.create_product_sizes()
        si.create_warehouses()

        cls.warehouse_data = {warehouse.code: warehouse for warehouse in Warehouse.objects.all()}
        cls.size_data = {size.code: size for size in Size.objects.all()}

        cls.product1 = ProductFactory()
        cls.product2 = ProductFactory()
        cls.product3 = ProductFactory()

        RemainFactory(product=cls.product1, size=cls.size_data['xxs'], warehouse=cls.warehouse_data['msk'], value=1)
        RemainFactory(product=cls.product1, size=cls.size_data['s'], warehouse=cls.warehouse_data['msk'], value=2)
        RemainFactory(product=cls.product1, size=cls.size_data['m'], warehouse=cls.warehouse_data['msk'], value=3)
        RemainFactory(product=cls.product1, size=cls.size_data['xxs'], warehouse=cls.warehouse_data['chn'], value=4)
        RemainFactory(product=cls.product1, size=cls.size_data['s'], warehouse=cls.warehouse_data['chn'], value=5)
        RemainFactory(product=cls.product1, size=cls.size_data['xxl'], warehouse=cls.warehouse_data['chn'], value=6)

        RemainFactory(product=cls.product2, size=cls.size_data['xxs'], warehouse=cls.warehouse_data['msk'], value=11)
        RemainFactory(product=cls.product2, size=cls.size_data['s'], warehouse=cls.warehouse_data['msk'], value=12)
        RemainFactory(product=cls.product2, size=cls.size_data['m'], warehouse=cls.warehouse_data['msk'], value=13)
        RemainFactory(product=cls.product2, size=cls.size_data['xxs'], warehouse=cls.warehouse_data['chn'], value=14)
        RemainFactory(product=cls.product2, size=cls.size_data['s'], warehouse=cls.warehouse_data['chn'], value=15)
        RemainFactory(product=cls.product2, size=cls.size_data['xxl'], warehouse=cls.warehouse_data['chn'], value=16)
        RemainFactory(product=cls.product2, size=cls.size_data['xxs'], warehouse=cls.warehouse_data['showroom'], value=17)
        RemainFactory(product=cls.product2, size=cls.size_data['xl'], warehouse=cls.warehouse_data['showroom'], value=18)
        RemainFactory(product=cls.product2, size=cls.size_data['xxl'], warehouse=cls.warehouse_data['showroom'], value=19)

        RemainFactory(product=cls.product3, size=cls.size_data['xxs'], warehouse=cls.warehouse_data['msk'], value=21)
        RemainFactory(product=cls.product3, size=cls.size_data['s'], warehouse=cls.warehouse_data['msk'], value=22)
        RemainFactory(product=cls.product3, size=cls.size_data['m'], warehouse=cls.warehouse_data['msk'], value=23)
        RemainFactory(product=cls.product3, size=cls.size_data['xxs'], warehouse=cls.warehouse_data['showroom'], value=27)
        RemainFactory(product=cls.product3, size=cls.size_data['xl'], warehouse=cls.warehouse_data['showroom'], value=28)
        RemainFactory(product=cls.product3, size=cls.size_data['xxl'], warehouse=cls.warehouse_data['showroom'], value=29)


    def test_all_warehouses(self):
        """При инициализации не уточняли инфу по точкам. Значит получаем данные по всем точкам"""

        rs = RemainService([self.product1.id, ])
        warehouses = rs._get_warehouses()
        self.assertEqual(warehouses.count(), len(self.warehouse_data))

    def test_warehouse_ids(self):
        """При инициализации передали id складов"""

        warehouse_ids = [self.warehouse_data['msk'].id, self.warehouse_data['chn'].id]
        rs = RemainService([self.product1.id, ], warehouse_ids=warehouse_ids)
        warehouses = rs._get_warehouses()

        ids = {w.id for w in warehouses}
        self.assertSetEqual(ids, set(warehouse_ids))

    def test_warehouse_codes(self):
        """При инициализации передали code складов"""

        warehouse_codes = ['msk', 'chn']
        rs = RemainService([self.product1.id, ], warehouse_codes=warehouse_codes)
        warehouses = rs._get_warehouses()

        codes = {w.code for w in warehouses}
        self.assertSetEqual(codes, set(warehouse_codes))

    def test_remains(self):
        """Развернутые данные по остаткам в формате: {product_id: {warehouse_code: {size_code: value}}}"""

        warehouse_codes = ['msk', 'showroom']
        product_ids = [self.product1.id, self.product2.id]

        rs = RemainService(product_ids, warehouse_codes=warehouse_codes)
        remains = rs.get_remains()

        # Получили данные по 2-м товарам
        self.assertSetEqual(set(remains.keys()), set(product_ids))

        # Точки
        warehouses1 = remains[self.product1.id]
        self.assertSetEqual(set(warehouses1.keys()), {'msk'})

        warehouses2 = remains[self.product2.id]
        self.assertSetEqual(set(warehouses2.keys()), {'msk', 'showroom'})

        # Размеры
        sizes1 = warehouses1['msk']
        self.assertSetEqual(set(sizes1.keys()), {'xxs', 's', 'm'})
        self.assertEqual(sizes1['xxs'], 1)
        self.assertEqual(sizes1['m'], 3)

        sizes2 = warehouses2['showroom']
        self.assertSetEqual(set(sizes2.keys()), {'xxs', 'xl', 'xxl'})
        self.assertEqual(sizes2['xl'], 18)
        self.assertEqual(sizes2['xxl'], 19)

    def test_flat_remains(self):
        """Остатки в формате: {product_id: value}"""

        warehouse_codes = ['msk', 'showroom']
        product_ids = [self.product1.id, self.product2.id]

        rs = RemainService(product_ids, warehouse_codes=warehouse_codes)
        remains = rs.get_flat_remains()

        # Получили данные по 2-м товарам
        self.assertSetEqual(set(remains.keys()), set(product_ids))

        self.assertEqual(remains[self.product1.id], 6)
        self.assertEqual(remains[self.product2.id], 90)

    def test_remains_by_warehouse(self):
        """Остатки формате: {product_id: {warehouse_code: value}}"""

        warehouse_codes = ['msk', 'chn']
        product_ids = [self.product1.id, self.product2.id]

        rs = RemainService(product_ids, warehouse_codes=warehouse_codes)
        remains = rs.get_remains_by_warehouse()

        # Получили данные по 2-м товарам
        self.assertSetEqual(set(remains.keys()), set(product_ids))

        # Точки
        warehouses1 = remains[self.product1.id]
        self.assertSetEqual(set(warehouses1.keys()), {'msk', 'chn'})

        warehouses2 = remains[self.product2.id]
        self.assertSetEqual(set(warehouses2.keys()), {'msk', 'chn', })

        # Остатки
        self.assertEqual(warehouses1['msk'], 6)
        self.assertEqual(warehouses2['chn'], 45)

    def test_remains_by_size(self):
        """Остатки формате: {product_id: {size_code: value}}"""

        warehouse_codes = ['msk', 'chn']
        product_ids = [self.product1.id, self.product2.id]

        rs = RemainService(product_ids, warehouse_codes=warehouse_codes)
        remains = rs.get_remains_by_size()

        # Получили данные по 2-м товарам
        self.assertSetEqual(set(remains.keys()), set(product_ids))

        # Размеры
        sizes1 = remains[self.product1.id]
        self.assertSetEqual(set(sizes1.keys()), {'xxs', 's', 'm', 'xxl', })

        sizes2 = remains[self.product2.id]
        self.assertSetEqual(set(sizes2.keys()), {'xxs', 's', 'm', 'xxl', })

        # Остатки
        self.assertEqual(sizes1['m'], 3)
        self.assertEqual(sizes2['s'], 27)
        self.assertEqual(sizes2['xxl'], 16)

from copy import copy

from django.core.urlresolvers import reverse
from freezegun.api import freeze_time
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from ..factories import ProductLabelFactory
from ..models import PRODUCT_LABEL_KIND_CHOICES, ProductLabel


class ProductLabelViewTestCase(APITestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super(ProductLabelViewTestCase, cls).setUpClass()
        cls.list_url = reverse('api:products:product_label-list')
        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')
        cls.valid_create_data = {
            'name': 'test product',
            'kind': PRODUCT_LABEL_KIND_CHOICES.main,
        }

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')
        pass

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_authentication_fail(self):
        self.client.force_authenticate(user=None)
        response = self.client.post(self.list_url)
        self.assertIn(response.status_code, [status.HTTP_403_FORBIDDEN, status.HTTP_302_FOUND])

    def test_list_product(self):
        ProductLabelFactory()
        response = self.client_bm.get(self.list_url)
        self.assertEqual(response.data['count'], 1)
        response = self.client_pa.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

    @freeze_time("2016-1-31")
    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        product_label = ProductLabelFactory(
            name='Название этикетки',
            kind=ProductLabel.KIND.internal,
            user=self.user_bm,
            country='Россия',
            structure='Хлопок',
            more_information='Дополнительная информация',
        )

        response = self.client_bm.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

        object_as_dict = response.data['results'][0]
        valid_list_item_dict = {
            "id": product_label.id,
            "kind": 2,
            "kind_detail": {
                "id": 2,
                "name": "Внутренняя",
            },
            "name": "Название этикетки",
            "user_detail": {
                "id": self.user_bm.id,
                "name": "",
            },
            "remains": {},
            "image": None,
            "images": [],
            'created': '2016-01-31T00:00:00Z',
            'modified': '2016-01-31T00:00:00Z',
            "country": 'Россия',
            "structure": 'Хлопок',
            "more_information": 'Дополнительная информация',
        }
        self.assertDictEqual(object_as_dict, valid_list_item_dict)

    # def test_list_sort_by_fields(self):
    #     """Проверяем, что api сортируется по всем необходимым полям"""
    #
    #     fields = [
    #         'id',
    #         'name',
    #         'kind',
    #         'modified'
    #     ]
    #     ProductLabelFactory(name='один', kind=ProductLabel.KIND.internal)
    #     ProductLabelFactory(name='два', kind=ProductLabel.KIND.main,)
    #     ProductLabelFactory(name='Три', kind=ProductLabel.KIND.cardboard)
    #     for field in fields:
    #         for prefix in ['', '-']:
    #             filter_field = "{}{}".format(prefix, field)
    #             response = self.client_admin.get(self.list_url, {'sort_by': filter_field})
    #             object_dict = response.data['results'][0]
    #             object = ProductLabel.objects.all().order_by(filter_field)[0]
    #             self.assertEqual(object_dict['id'], getattr(object, 'id'), 'Не сортируется по полю {} в {}'.format(filter_field, self.list_url))

    def test_create_product(self):
        product1_data = copy(self.valid_create_data)
        response = self.client_bm.post(self.list_url, product1_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_blank_request_data(self):
        response = self.client_bm.post(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

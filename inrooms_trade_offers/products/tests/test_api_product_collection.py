from django.core.urlresolvers import reverse
from rest_framework.test import APIClient, APITestCase

from accounts.tests.setup import UsersSetup
from products.factories import ProductCollectionFactory


class ProductCollectionViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(ProductCollectionViewTestCase, cls).setUpClass()
        cls.collection_list_url = reverse('api:products:collection-list')
        UsersSetup()
        for i in range(10):
            ProductCollectionFactory()

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

    def tearDown(self):
        self.client_admin.session.clear()

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client_admin.get(self.collection_list_url)
        objects_dict = response.data['results'][0]
        fields = [
            'id',
            'name',
            'series_type',
        ]
        for field in fields:
            try:
                field_container = objects_dict
                for i in field.split('.'):
                    field_container = field_container[i]
            except (KeyError, TypeError):
                self.fail('Не передается поле {} в {}'.format(field, self.collection_list_url))

from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from ..factories import ProductColorFactory
from ..models import ProductColor


class ProductColorViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(ProductColorViewTestCase, cls).setUpClass()
        cls.list_url = reverse('api:products:product_color-list')
        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        ProductColor.objects.all().delete()
        for i in range(10):
            ProductColorFactory()

    def setUp(self):
        self.client = APIClient()
        self.client.login(email='admin@example.com', password='password')
        pass

    def tearDown(self):
        self.client.session.clear()

    def test_list_count(self):
        """Проверяем, что api рабочее"""
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 10)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client.get(self.list_url)
        object_dict = response.data['results'][0]
        fields = [
            'id',
            'name',
        ]
        for field in fields:
            self.assertIn(field, object_dict, 'Не передается поле {} в {}'.format(field, self.list_url))

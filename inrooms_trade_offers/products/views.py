from collections import OrderedDict

from django.shortcuts import get_object_or_404
from django.utils.html import strip_tags
from docx.shared import Mm
from wkhtmltopdf.views import PDFTemplateView

from core.utils.views.docx import ExportDocxViewMixin
from .models import Product


class ProductExport(ExportDocxViewMixin, PDFTemplateView):
    template_name = 'products/product_pdf.html'
    filename = 'product.pdf'

    def dispatch(self, request, object_id, *args, **kwargs):
        self.object = get_object_or_404(Product, id=object_id)
        self.has_common = request.GET.get('has_common', False) == 'true'
        self.has_images = request.GET.get('has_images', False) == 'true'
        self.has_description = request.GET.get('has_description', False) == 'true'
        self.has_fields = request.GET.get('has_fields', False) == 'true'
        self.has_comments = request.GET.get('has_comments', False) == 'true'
        return super(ProductExport, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(ProductExport, self).get_context_data(*args, **kwargs)
        context['product'] = self.object
        context['has_common'] = self.has_common
        context['has_images'] = self.has_images
        context['has_description'] = self.has_description
        context['has_fields'] = self.has_fields
        if self.has_fields:
            context['intags'] = self.get_intags()
        context['has_comments'] = self.has_comments
        if self.has_comments:
            context['comments'] = self.get_comments()
        return context

    def get_intags(self):
        data = OrderedDict()
        for pf in self.object.productfield_set.all().order_by(
                'intag__category__weight',
                'intag__category__name',
                'intag__weight',
                'intag__name'):
            if pf.intag.category not in data:
                data[pf.intag.category] = []
            value = pf.value
            if isinstance(value, list):
                value = u', '.join(value)
            if value is None:
                value = ''
            data[pf.intag.category].append({'intag': pf.intag,
                                            'value': value})
        return data

    def get_comments(self):
        return self.object.comments.all()

    def write_docx_document(self, document):
        document.add_heading('Товар: {}'.format(self.object.name), 0)

        if self.has_common:
            document.add_heading('Основное', level=1)
            table = document.add_table(rows=3, cols=2)
            cells = table.rows[0].cells
            cells[0].text = 'Артикул:'
            cells[1].text = self.object.article
            cells = table.rows[1].cells
            cells[0].text = 'Коллекция:'
            cells[1].text = self.object.main_collection.name if self.object.main_collection else '-'

        if self.has_fields:
            document.add_heading('Характеристики', level=1)
            table = document.add_table(rows=1, cols=2)
            hdr_cells = table.rows[0].cells
            hdr_cells[0].text = 'Характеристика'
            hdr_cells[1].text = 'Значение'
            for category, intags in self.get_intags().items():
                row_cells = table.add_row().cells
                p = row_cells[0].text = category.name.upper()
                for intag_data in intags:
                    row_cells = table.add_row().cells
                    row_cells[0].text = intag_data['intag'].name
                    row_cells[1].text = intag_data['value']

        if self.has_images:
            document.add_heading('Изображения', level=1)
            for item in self.object.images.all():
                document.add_picture(str(item.image.file), width=Mm(100))

        if self.has_description:
            document.add_heading('Описание', level=1)
            document.add_paragraph(strip_tags(self.object.description).replace('&nbsp;', ' '))

        if self.has_comments:
            document.add_heading('Комментарии', level=1)
            table = document.add_table(rows=1, cols=3)
            hdr_cells = table.rows[0].cells
            hdr_cells[0].text = 'Пользователь'
            hdr_cells[1].text = 'Дата'
            hdr_cells[2].text = 'Текст'
            for item in self.object.comments.all():
                row_cells = table.add_row().cells
                row_cells[0].text = item.user.name
                row_cells[1].text = item.created.strftime('%d.%m.%Y %H:%M:%S')
                row_cells[2].text = strip_tags(item.body).replace('&nbsp;', ' ')

        document.add_page_break()
        return document

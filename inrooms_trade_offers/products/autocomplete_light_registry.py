from autocomplete_light import shortcuts as autocomplete_light

from .models import Intag, IntagCategory, IntagChoice, IntagUnit, Product, ProductCollection, ProductColor, ProductLabel, Size

autocomplete_light.register(
    Intag,
    search_fields=['name', ],
    attrs={
        'placeholder': u'Название',
        'data-autocomplete-minimum-characters': 0, })


autocomplete_light.register(
    IntagCategory,
    search_fields=['name', ],
    attrs={
        'placeholder': u'Название',
        'data-autocomplete-minimum-characters': 0, })


autocomplete_light.register(
    IntagChoice,
    search_fields=['value', ],
    attrs={
        'placeholder': u'Название',
        'data-autocomplete-minimum-characters': 0, })


autocomplete_light.register(
    IntagUnit,
    search_fields=['name', ],
    attrs={
        'placeholder': u'Название',
        'data-autocomplete-minimum-characters': 0, })


autocomplete_light.register(
    ProductCollection,
    search_fields=['name', ],
    attrs={
        'placeholder': u'Название',
        'data-autocomplete-minimum-characters': 0,  })


class ProductColorAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': u'Название',
        'data-autocomplete-minimum-characters': 0,
    }


class ProductCollectionParentAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': u'Название',
        'data-autocomplete-minimum-characters': 0, }
    choices = ProductCollection.objects.filter(parent__isnull=True)


class ProductAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['=id', 'name', 'article']
    attrs = {
        'placeholder': u'Название',
        'data-autocomplete-minimum-characters': 0,
    }


class ProductLabelAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['=id', 'name', ]
    attrs = {
        'placeholder': u'Название',
        'data-autocomplete-minimum-characters': 0,
    }


class SizeAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', 'code']
    attrs = {
        'placeholder': u'Название',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (Product, ProductAutocomplete),
    (ProductColor, ProductColorAutocomplete),
    (ProductLabel, ProductLabelAutocomplete),
    (Size, SizeAutocomplete),
    (ProductCollection, ProductCollectionParentAutocomplete),

)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'products'
    verbose_name = 'Продукты'

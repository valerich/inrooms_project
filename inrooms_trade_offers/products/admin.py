from django.contrib import admin
from sorl.thumbnail.shortcuts import get_thumbnail

from config.celery import app
from products.forms import SeasonProductCollectionPlanItemAdminForm
from products.models import SeasonProductCollectionPlanItem
from .forms import (
    IntagAdminForm,
    IntagChoiceAdminForm,
    LabelRemainAdminForm,
    ProductAdminForm,
    ProductCollectionAdminForm,
    ProductCollectionItemAdminForm,
    ProductFieldAdminForm,
    ProductImageAdminForm,
    RemainAdminForm,
    SeasonProductCollectionPlanAdminForm,
)
from .models import (
    Country,
    Intag,
    IntagCategory,
    IntagChoice,
    IntagUnit,
    LabelRemain,
    Product,
    ProductCollection,
    ProductCollectionItem,
    ProductColor,
    ProductField,
    ProductImage,
    ProductLabel,
    ProductLabelImage,
    Remain,
    SeasonProductCollectionPlan,
    Size,
    TnvedGroup,
    TnvedMaterial,
    TnvedCode,
)


class TnvedGroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class TnvedMaterialAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class TnvedCodeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class IntagAdmin(admin.ModelAdmin):
    list_display = ['name', 'is_for_order']
    list_filter = ['is_for_order', ]
    form = IntagAdminForm


class IntagChoiceAdmin(admin.ModelAdmin):
    list_display = ['intag', 'value']
    search_fields = ['intag__name', 'value']
    form = IntagChoiceAdminForm


class IntagUnitAdmin(admin.ModelAdmin):
    list_display = ('name', 'unit_name', )


class ProductImageAdmin(admin.ModelAdmin):
    form = ProductImageAdminForm


class ProductColorAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'code']
    search_fields = ['=code', 'name']


class CountryAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    search_fields = ['name', ]


class ProductAdmin(admin.ModelAdmin):
    list_display = ('article', 'name', 'is_deleted')
    list_filter = ('is_template', 'is_deleted')
    search_fields = ('name', 'article', )
    form = ProductAdminForm
    actions = ('send_to_1c_action',)

    def send_to_1c_action(self, request, queryset):
        product_ids = list(queryset.values_list('id', flat=True))
        app.send_task('service_1c.notify_1c_product_change', args=(product_ids,))
        self.message_user(request, 'Отправлено на выгрузку в 1с')

    send_to_1c_action.short_description = 'Выгрузить в 1с'

    def get_thumbnail_html(self, obj):
        img = obj.main_im
        img_resize_url = None

        if img is not None and img.name:
            try:
                img_resize_url = str(get_thumbnail(img, '100x100').url)
            except IOError:
                pass

        if not img:
            return '<a class="image-picker" href=""><img src="" alt=""/></a>'
        html = '''<a class="image-picker" href="%s"><img src="%s"
                alt="%s"/></a>''' % (img.url, img_resize_url, obj.name)
        return html

    get_thumbnail_html.short_description = 'Миниатюра'
    get_thumbnail_html.allow_tags = True

    def get_queryset(self, request):
        qs = Product.all_objects.get_queryset()
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs.prefetch_related('images')


class ProductLabelAdmin(admin.ModelAdmin):
    list_display = ('name', 'kind')
    list_filter = ('kind', )
    search_fields = ('name', )


class ProductCollectionAdmin(admin.ModelAdmin):
    list_display = ('id', 'kind', 'parent', 'name', 'series_type', 'order', 'cat_partner_self_sale', 'is_include_in_shop_season_report', )
    list_filter = ('series_type', 'kind', 'cat_partner_self_sale', 'is_include_in_shop_season_report', )
    search_fields = ('name', )
    form = ProductCollectionAdminForm


class SeasonProductCollectionPlanItemAdminInline(admin.TabularInline):
    model = SeasonProductCollectionPlanItem
    extra = 0
    form = SeasonProductCollectionPlanItemAdminForm


class SeasonProductCollectionPlanAdmin(admin.ModelAdmin):
    list_display = ('year', 'season', )
    list_filter = ('year', 'season', )
    search_fields = ('year', )
    form = SeasonProductCollectionPlanAdminForm
    inlines = [SeasonProductCollectionPlanItemAdminInline, ]


class ProductCollectionItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'product', 'product_collection')
    list_filter = ('product_collection',)
    form = ProductCollectionItemAdminForm


class ProductFieldAdmin(admin.ModelAdmin):
    list_display = ('product', 'intag', 'value')
    form = ProductFieldAdminForm


class SizeAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'multiplier', 'series_type')
    list_filter = ('series_type', )
    search_fields = ('name', 'code')


class RemainAdmin(admin.ModelAdmin):
    list_display = ('product', 'size', 'warehouse', 'value', 'amount_rur')
    list_filter = ('size', 'warehouse', )
    search_fields = ('^product__article', )
    form = RemainAdminForm


class LabelRemainAdmin(admin.ModelAdmin):
    list_display = ('product_label', 'size', 'warehouse', 'value', 'amount_rur')
    list_filter = ('size', 'warehouse', )
    search_fields = ('^product_label__name', )
    form = LabelRemainAdminForm


for model_or_iterable, admin_class in (
    (Country, CountryAdmin),
    (Intag, IntagAdmin),
    (IntagCategory, None),
    (IntagChoice, IntagChoiceAdmin),
    (IntagUnit, IntagUnitAdmin),
    (Product, ProductAdmin),
    (ProductCollection, ProductCollectionAdmin),
    (ProductCollectionItem, ProductCollectionItemAdmin),
    (ProductColor, ProductColorAdmin),
    (ProductField, ProductFieldAdmin),
    (ProductImage, ProductImageAdmin),
    (ProductLabel, ProductLabelAdmin),
    (ProductLabelImage, None),
    (Size, SizeAdmin),
    (SeasonProductCollectionPlan, SeasonProductCollectionPlanAdmin),
    (Remain, RemainAdmin),
    (LabelRemain, LabelRemainAdmin),
    (TnvedCode, TnvedCodeAdmin),
    (TnvedMaterial, TnvedMaterialAdmin),
    (TnvedGroup, TnvedGroupAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

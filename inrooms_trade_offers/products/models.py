import os
import uuid
from datetime import time
from decimal import Decimal

from autoslug.fields import AutoSlugField
from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models, transaction
from django.db.models.aggregates import Sum
from django_resized import ResizedImageField
from barcode import generate
from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

from accounts.models import User
from brands.models import Brand
from comments.models import Comment
from consistency.models import ConsistencyMixin
from care.models import CareMixin
from core.models import CURRENCY_CHOICES, DeleteMixin
from core.utils.constants import FALSE_VALUES, TRUE_VALUES
from offers.models import Offer
from warehouses.models import Warehouse

SIZES = [
    'xxs',
    'xs',
    's',
    'm',
    'l',
    'xl',
    'xxl',
    'xxxl',
    '34',
    '35',
    '36',
    '37',
    '38',
    '39',
    '40',
    '41',
]

DOUBLE_SIZES = [
    's',
    'm',
    '37',
    '38'
]


PRODUCT_KIND = Choices(
    (1, 'clothing', 'Одежда'),
    (2, 'shop_equipment', 'Торговое оборудование'),
    (3, 'promo', 'Рекламная продукция'),
    (4, 'lighting', 'Осветительные приборы'),
)


SERIES_TYPE_CHOICES = Choices(
    ('c', 'clothes', 'Одежда'),
    ('s', 'shoes', 'Обувь'),
)


class ProductCollection(TimeStampedModel):
    SERIES_TYPE = SERIES_TYPE_CHOICES
    KIND = PRODUCT_KIND

    name = models.CharField('Название', max_length=300)
    parent = models.ForeignKey('self', verbose_name='Родитель', blank=True, null=True)
    kind = models.PositiveSmallIntegerField('Тип', choices=KIND, default=KIND.clothing)
    order = models.PositiveSmallIntegerField('Позиция', default=0, db_index=True)

    series_type = models.CharField('Тип серии', max_length=1, choices=SERIES_TYPE, default=SERIES_TYPE.clothes)

    products = models.ManyToManyField('Product', through='ProductCollectionItem')

    cat_partner_self_sale = models.BooleanField('Разрешен к продаже партнерам самостоятельно', default=False)
    is_include_in_shop_season_report = models.BooleanField('Включать в сезонный отчет', default=True, help_text='Дашборд партнеров')

    def __str__(self):
        if self.parent_id:
            return '{} \ {}'.format(self.name,
                                    self.parent.name, )
        else:
            return self.name

    class Meta:
        verbose_name = 'Коллекция продуктов'
        verbose_name_plural = 'Коллекции продуктов'
        ordering = ('-order', 'name')


SEASON_CHOICES = Choices(
    (1, 'spring_summer', 'Весна/Лето'),
    (2, 'autumn_winter', 'Осень/Зима'),
)


class SeasonProductCollectionPlan(models.Model):
    SEASON = SEASON_CHOICES
    year = models.PositiveSmallIntegerField('Год')
    season = models.PositiveSmallIntegerField('Сезон', choices=SEASON)

    units_per_s_m = models.PositiveSmallIntegerField('Кол-во единиц на кв/м', default=0)
    avg_unit_cost = models.PositiveSmallIntegerField('Средняя стоимость единицы товара', default=0)

    class Meta:
        verbose_name = 'Сезонный план коллекции'
        verbose_name_plural = 'Сезонный план коллекций'
        ordering = ('-year', '-season')
        unique_together = ('year', 'season')

    def __str__(self):
        return '{}, {}'.format(self.year, self.get_season_display(), )

    def create_items(self):
        for collection in ProductCollection.objects.filter(parent__isnull=True):
            SeasonProductCollectionPlanItem.objects.get_or_create(
                season_product_collection_plan=self,
                product_collection=collection
            )


class SeasonProductCollectionPlanItem(models.Model):
    season_product_collection_plan = models.ForeignKey(SeasonProductCollectionPlan, verbose_name='План', related_name='items')
    product_collection = models.ForeignKey(ProductCollection, verbose_name='Коллекция товаров')
    plan = models.DecimalField('Процентное отношение', max_digits=4, decimal_places=2, default=0)

    class Meta:
        verbose_name = 'Элемент сезонного плана'
        verbose_name_plural = 'Элементы сезонного плана'
        unique_together = ('season_product_collection_plan', 'product_collection')


class ProductCollectionItem(models.Model):
    product_collection = models.ForeignKey('ProductCollection')
    product = models.ForeignKey('Product')
    order = models.IntegerField(default=0, db_index=True)

    def __str__(self):
        return '{}. pr {}. col {}'.format(self.id, self.product_id, self.product_collection_id)

    class Meta:
        verbose_name = 'Связь Продукт-Коллекция'
        verbose_name_plural = 'Связи Продукт-Коллекция'
        ordering = ('order',)
        unique_together = ('product_collection', 'product')


INTAG_KIND_CHOICES = Choices(
    (1, 'string', 'String'),
    (2, 'float', 'Float'),
    (3, 'bool', 'Boolean'),
    (4, 'choice', 'Choice'),
)

INTAG_CONTROL_TYPE = Choices(
    (1, 'radio', 'radio'),
    (2, 'color', 'color'),
    (3, 'checkbox', 'checkbox'),
    (4, 'radio_multiple', 'radio_multiple'),
)


class IntagCategory(TimeStampedModel):
    name = models.CharField(verbose_name='Название', max_length=127)
    weight = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория характеристик'
        verbose_name_plural = 'Категории характеристик'
        ordering = ('-weight',)


class IntagUnit(models.Model):
    name = models.CharField(verbose_name='Название', max_length=128, default='',
                            help_text='Например, "Размер в миллиметрах"')
    unit_name = models.CharField(verbose_name='Название единицы измерения', max_length=128, default='',
                                 help_text='Например, "мм"')

    class Meta:
        verbose_name = 'Единица измерения'
        verbose_name_plural = 'Единицы измерения'

    def __str__(self):
        return '{} - {}'.format(self.name, self.unit_name)


class Intag(TimeStampedModel):
    KIND = INTAG_KIND_CHOICES
    CONTROL_TYPE = INTAG_CONTROL_TYPE

    category = models.ForeignKey('IntagCategory', verbose_name='Категория')

    name = models.CharField('Название', max_length=300)
    slug = AutoSlugField('slug', populate_from='name', editable=False)

    functional_description = models.CharField('Функциональное описание', max_length=500, blank=True)
    control_type = models.SmallIntegerField('Тип элемента управления', choices=CONTROL_TYPE, default=3)
    kind = models.SmallIntegerField('Тип', choices=KIND, default=2)

    unit = models.ManyToManyField('IntagUnit', blank=True, verbose_name='Единица измерения')

    is_many = models.BooleanField('Множественный выбор', default=False)
    is_for_order = models.BooleanField('используется для создания заказа', default=False)

    weight = models.IntegerField('Вес', default=0, db_index=True)

    class Meta:
        verbose_name = 'характеристика'
        verbose_name_plural = 'характеристики'
        ordering = ('-weight',)

    def __str__(self):
        if self.category_id:
            return '{} \ {}'.format(self.category, self.name)
        return '{}'.format(self.name)

    def get_correct_value(self, value):
        """
        Функция пытается привести value к типу self.kind

        value - Значение для интега

        Если приведение типа не удалось, то возвращает значение,
        переданное в функцию

        """

        try:
            if self.kind == self.KIND.string:
                if type(value) != str:
                    value = str(value)
            elif self.kind == self.KIND.int:
                if type(value) != int:
                    value = int(value)
            elif self.kind == self.KIND.float:
                if type(value) != float:
                    value = float(value)
            elif self.kind == self.KIND.bool:
                if type(value) != bool:
                    if value in TRUE_VALUES:
                        value = True
                    elif value in FALSE_VALUES:
                        value = False
        except TypeError:
            pass
        return value


class IntagChoice(TimeStampedModel):
    """
    Значение характеристики (Intag).

    Более точным именем могло бы быть IntagOption.
    """
    intag = models.ForeignKey('Intag', null=True, related_name='choices', verbose_name='Характеристика')
    value = models.CharField(max_length=450, blank=False, verbose_name='значение')
    weight = models.SmallIntegerField('вес (сортировка)', default=0, db_index=True)

    class Meta:
        verbose_name = 'значение характеристики'
        verbose_name_plural = 'значения характеристики'
        ordering = ('-weight',)
        unique_together = ('value', 'intag')

    def __str__(self):
        return self.value


PRODUCT_LABEL_KIND_CHOICES = Choices(
    (1, 'main', 'Основная'),
    (2, 'internal', 'Внутренняя'),
    (3, 'cardboard', 'Картон'),
)


class ProductLabel(TimeStampedModel):
    KIND = PRODUCT_LABEL_KIND_CHOICES

    user = models.ForeignKey(User, verbose_name='Создатель')
    kind = models.PositiveSmallIntegerField('Тип', choices=KIND, default=KIND.main)
    name = models.CharField('Название', max_length=255)
    country = models.CharField('Страна', max_length=100, blank=True)
    structure = models.TextField('Состав', blank=True)
    more_information = models.TextField('Дополнительная информация', blank=True)

    class Meta:
        verbose_name = 'Этикетка'
        verbose_name_plural = 'Этикетки'
        ordering = ('-id',)

    def __str__(self):
        return '№:{}. {}'.format(self.id, self.name)

    @property
    def main_im(self):
        try:
            return self.images.all()[0].image
        except IndexError:
            return None


class ProductField(TimeStampedModel):
    """
    Конкретное значение (или несколько значений) конкретной характеристики
    для конкретного продукта. Например: «у продукта 12345 цвет: чёрный».
    """
    product = models.ForeignKey('Product')
    ordering = models.SmallIntegerField(verbose_name='сортировка', null=True, default=0)
    value_str = models.TextField('Значение (строка)', blank=True)
    value_float = models.FloatField('Значение (число)', blank=True, null=True)
    value_bool = models.NullBooleanField('Значение (bool)', blank=True, null=True)

    intag = models.ForeignKey(Intag, null=True, blank=True, verbose_name='Харастеристика')
    intag_choices = models.ManyToManyField(IntagChoice, blank=True, verbose_name='Значение характеристики')
    weight = models.IntegerField(default=0, blank=True)

    class Meta:
        verbose_name = 'Поле в продукте'
        verbose_name_plural = 'Поля в продукте'
        unique_together = ('product', 'intag')
        ordering = ('-weight', '-ordering')

    def __str__(self):
        return '{}, product_id={}, intag_id={}'.format(self.id, self.product_id, self.intag_id)

    @property
    def value(self):
        # if self.intag.kind == self.intag.KIND.choice:
        if self.intag and self.intag.is_many:
            data = [c.value for c in self.intag_choices.all()]
            if data:
                return data
            else:
                return None
        else:
            try:
                return self.intag_choices.all()[0].value
            except IndexError:
                return None
                # else:
                #     return ProductField._get_value_helper(
                #         intag_kind=self.intag.kind, product_field=self)

    @staticmethod
    def _get_value_helper(intag_kind, product_field):
        if intag_kind == INTAG_KIND_CHOICES.string:
            return product_field.value_str
        elif intag_kind == INTAG_KIND_CHOICES.float:
            return product_field.value_float
        elif intag_kind == INTAG_KIND_CHOICES.bool:
            return product_field.value_bool
        else:
            raise ValueError('Wrong intag kind')

    @value.setter
    def value(self, value):
        if value is None:
            raise TypeError
        self.value_str = self.value_float = self.value_bool = None
        c_value = self.clean_value(value)
        if self.intag.kind == self.intag.KIND.string:
            self.value_str = c_value
        elif self.intag.kind == self.intag.KIND.float:
            self.value_float = c_value
        elif self.intag.kind == self.intag.KIND.bool:
            self.value_bool = c_value

    def clean_value(self, value):
        if self.intag.kind == self.intag.KIND.string:
            return str(value)
        elif self.intag.kind == self.intag.KIND.float:
            return float(value)
        elif self.intag.kind == self.intag.KIND.bool:
            return bool(value)


class ProductColor(models.Model):
    name = models.CharField('Название', max_length=255, unique=True)
    code = models.CharField('Код', blank=True, max_length=20)

    class Meta:
        verbose_name = 'Цвет'
        verbose_name_plural = 'Цвета'
        ordering = ("name", )

    def __str__(self):
        return self.name


class Country(models.Model):
    name = models.CharField('Название', max_length=255, unique=True)
    label_print_text = models.TextField('Текст для печати на этикетках', blank=True)

    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'
        ordering = ("name", )

    def __str__(self):
        return self.name


class TnvedGroup(models.Model):
    name = models.CharField('Название', max_length=100)

    class Meta:
        verbose_name = 'ТНВЭД Группа'
        verbose_name_plural = 'ТНВЭД Группы'
        ordering = ('name', )

    def __str__(self):
        return self.name


class TnvedMaterial(models.Model):
    name = models.CharField('Название', max_length=100, unique=True)

    class Meta:
        verbose_name = 'ТНВЭД материал'
        verbose_name_plural = 'ТНВЭД материалы'
        ordering = ('name', )

    def __str__(self):
        return self.name


class TnvedCode(models.Model):
    group = models.ForeignKey(TnvedGroup, verbose_name='Группа ')
    code = models.CharField('Код', max_length=20, unique=True)
    name = models.CharField('Название', max_length=100)
    material = models.ForeignKey(TnvedMaterial, verbose_name='Материал', blank=True, null=True)

    class Meta:
        verbose_name = 'ТНВЭД код'
        verbose_name_plural = 'ТНВЭД коды'
        ordering = ('code', )

    def __str__(self):
        return '{}, {}'.format(self.name, self.material.name if self.material else '-')


def get_default_article():
    return uuid.uuid4().hex


SEASONALITY_CHOICES = Choices(
    (1, 'spring_summer', 'Весна/Лето'),
    (2, 'autumn_winter', 'Осень/Зима'),
    (3, 'basic', 'Basic'),
)


class Product(ConsistencyMixin, CareMixin, DeleteMixin, TimeStampedModel):
    CURRENCY = CURRENCY_CHOICES
    KIND = PRODUCT_KIND
    SEASONALITY = SEASONALITY_CHOICES
    is_new_collection = models.BooleanField('Новинка', default=False)
    offer = models.ForeignKey(Offer, verbose_name='Предложение', blank=True, null=True,
                              help_text='Заполняется, если модель была создана на основании предложения')
    user = models.ForeignKey(User, verbose_name='Создатель')
    kind = models.PositiveSmallIntegerField('Тип', choices=KIND, default=KIND.clothing)
    main_collection = models.ForeignKey('ProductCollection', verbose_name='Основная коллекция')
    name = models.CharField('Название', max_length=255)
    business_name = models.CharField('Коммерческое наимерование', max_length=255, blank=True)
    seasonality = models.PositiveSmallIntegerField('Сезонность', choices=SEASONALITY, default=SEASONALITY.basic)
    article = models.CharField('Артикул', max_length=100, db_index=True, blank=True, null=True)
    description = models.TextField('Описание', blank=True)
    fields = models.ManyToManyField('Intag', through='ProductField', verbose_name='Поля')
    links = models.TextField('Ссылки', blank=True)

    color = models.ForeignKey(ProductColor, verbose_name='Цвет')

    is_template = models.BooleanField('Шаблон', default=False)

    is_active = models.BooleanField('Обработано', default=False)
    is_public = models.BooleanField('Публичный', default=False, help_text='Выгружается на сайты')

    price = models.DecimalField('Цена', blank=True, null=True, max_digits=10, decimal_places=2)
    public_price = models.DecimalField('Цена на сайте', default=0, max_digits=10, decimal_places=2)
    public_old_price = models.DecimalField('Перечеркнутая цена на сайте', default=0, max_digits=10, decimal_places=2)

    related_products = models.ManyToManyField('self', verbose_name='Связанные товары', blank=True)

    tnved_code = models.ForeignKey(TnvedCode, verbose_name='Код ТНВЭД', blank=True, null=True)

    # Ниже характеристики и потом мы вынесем их в IntagChoice
    intag_weight = models.PositiveSmallIntegerField('Вес', blank=True, null=True)
    small_description = models.TextField("Краткое описание", blank=True)

    brand = models.ForeignKey(Brand, verbose_name='Бренд')
    country = models.ForeignKey(Country, verbose_name='Страна', blank=True, null=True)

    comments = GenericRelation(Comment)

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
        ordering = ('-id',)
        unique_together = ('article', 'color', 'brand')

    def __str__(self):
        return '№:{}. {}'.format(self.id, self.name)

    @property
    def vendor(self):
        try:
            return ProductField.objects.get(product=self, intag__slug='proizvoditel').intag_choices.first()
        except ProductField.DoesNotExist:
            return None

    def save(self, *args, **kwargs):
        is_created = not self.id
        # генерируем артикул для шаблона
        if is_created and not self.article and self.is_template:
            self.article = 'template{}'.format(int(time.time()))

        super(Product, self).save(*args, **kwargs)

        if self.main_collection:
            ProductCollectionItem.objects.get_or_create(product_collection=self.main_collection, product=self)

    @property
    def main_im(self):
        try:
            return self.images.all()[0].image
        except IndexError:
            return None

    @property
    def remain_sum(self):
        """Сумма остатков товар по всем размерам"""

        remains = self.remain_set.aggregate(Sum('value'))['value__sum']
        return remains if remains is not None else 0

    def get_barcode(self):
        generate(
            'EAN13',
            '0{}{}{}'.format(
                self.article.replace('/', ''),
                self.color.code,
                self.id
            ),
            output='{}/product/barcodes/{}'.format(settings.MEDIA_ROOT, self.id),
            writer_options={'module_height': 5.0, 'module_width': 0.4})
        return '/media/product/barcodes/{}.svg'.format(self.id)


def generate_product_image_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'product/fullsize/{}{}'.format(uuid.uuid4(), ext)


def generate_product_label_image_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'product_label/fullsize/{}{}'.format(uuid.uuid4(), ext)


class ProductImage(TimeStampedModel):
    image = ResizedImageField('Изображение', upload_to=generate_product_image_filename, max_length=755, blank=True)
    visual_order = models.PositiveSmallIntegerField('Порядок отображения', default=0)
    product = models.ForeignKey(Product, related_name='images')

    class Meta:
        verbose_name = 'Изображение этикетки'
        verbose_name_plural = 'Изображения этикеток'
        ordering = ['visual_order', '-id']

    def __str__(self):
        return 'id: {} - product_label_id: {}'.format(self.pk, self.product_id)


class ProductLabelImage(TimeStampedModel):
    image = ResizedImageField('Изображение', upload_to=generate_product_label_image_filename, max_length=755, blank=True)
    visual_order = models.PositiveSmallIntegerField('Порядок отображения', default=0)
    product_label = models.ForeignKey(ProductLabel, related_name='images')

    class Meta:
        verbose_name = 'изображение продукта'
        verbose_name_plural = 'изображения продуктов'
        ordering = ['visual_order', '-id']

    def __str__(self):
        return 'id: {} - product_id: {}'.format(self.pk, self.product_id)


class Size(models.Model):
    """Размер товара

    series_type - тип серии. Примеры типа: одежда, обувь.
    """
    SERIES_TYPE = SERIES_TYPE_CHOICES
    series_type = models.CharField('Тип серии', max_length=1, choices=SERIES_TYPE, db_index=True)
    name = models.CharField('Название', unique=True, max_length=10)
    code = models.CharField('Код', unique=True, db_index=True, max_length=10)
    code_1c = models.CharField('Код 1с', unique=True, db_index=True, max_length=10)
    weight = models.PositiveSmallIntegerField('Вес', default=0)
    multiplier = models.PositiveSmallIntegerField('Множитель', default=1)

    class Meta:
        verbose_name = 'Размер'
        verbose_name_plural = 'Размеры'
        ordering = ('series_type', 'weight')

    def __str__(self):
        return self.name


class Remain(models.Model):
    product = models.ForeignKey(Product, verbose_name='Товар')
    warehouse = models.ForeignKey(Warehouse, verbose_name='Склад')
    size = models.ForeignKey(Size, verbose_name='Размер')
    value = models.SmallIntegerField('Количество')
    amount_rur = models.DecimalField('Сумма (р)', max_digits=20, decimal_places=10, default=Decimal('0.00'))

    class Meta:
        verbose_name = 'Остатки'
        verbose_name_plural = 'Остатки'
        unique_together = ('product', 'size', 'warehouse')
        ordering = ('id', )

    def __str__(self):
        return '{}, {}: {}'.format(self.product, self.size, self.value)

    @staticmethod
    @transaction.atomic()
    def add_remain(product, warehouse, value, amount_rur, size):
        remain, c = Remain.objects.get_or_create(
            product=product,
            warehouse=warehouse,
            size=size,
            defaults={
                'value': value,
                'amount_rur': amount_rur
            }
        )
        if not c:
            remain.value += value
            remain.amount_rur += amount_rur
            remain.save()

    @staticmethod
    @transaction.atomic()
    def remove_remain(product, warehouse, value, size):
        amount_rur = Decimal('0.0000000000')
        try:
            remain = Remain.objects.get(
                product=product,
                warehouse=warehouse,
                size=size,
            )
        except Remain.DoesNotExist:
            raise ValueError
        else:
            if remain.value < value:
                raise ValueError
            if value:
                item_amount_rur = remain.amount_rur / remain.value
                amount_rur = item_amount_rur * value
                remain.amount_rur -= amount_rur
                remain.value -= value
                remain.save()
        return value, amount_rur


class RemainHistory(models.Model):
    year = models.PositiveSmallIntegerField('Год', db_index=True)
    month = models.PositiveSmallIntegerField('Месяц', db_index=True)
    product = models.ForeignKey(Product, verbose_name='Модель')
    warehouse = models.ForeignKey(Warehouse, verbose_name='Склад')
    value_avg = models.DecimalField('Среднее количество', max_digits=8, decimal_places=3)

    class Meta:
        verbose_name = 'История остатков'
        verbose_name_plural = 'История остатков'
        unique_together = (
            'year',
            'month',
            'product',
            'warehouse',
        )

    def __str__(self):
        return '{}.{}, p: {}, w: {}'.format(
            self.month,
            self.year,
            self.product_id,
            self.warehouse_id, )


class LabelRemain(models.Model):
    product_label = models.ForeignKey(ProductLabel, verbose_name='Этикетка')
    warehouse = models.ForeignKey(Warehouse, verbose_name='Склад')
    size = models.ForeignKey(Size, verbose_name='Размер', blank=True, null=True)
    value = models.SmallIntegerField('Количество')
    amount_rur = models.DecimalField('Сумма (р)', max_digits=20, decimal_places=10, default=Decimal('0.00'))

    class Meta:
        verbose_name = 'Остатки этикетки'
        verbose_name_plural = 'Остатки этикеток'
        unique_together = ('product_label', 'size', 'warehouse')
        ordering = ('id', )

    def __str__(self):
        return '{}, {}: {}'.format(self.product_label, self.size, self.value)

    @staticmethod
    @transaction.atomic()
    def add_remain(product_label, warehouse, value, amount_rur, size=None):
        label_remain, c = LabelRemain.objects.get_or_create(
            product_label=product_label,
            warehouse=warehouse,
            size=size,
            defaults={
                'value': value,
                'amount_rur': amount_rur
            }
        )
        if not c:
            label_remain.value += value
            label_remain.amount_rur += amount_rur
            label_remain.save()

    @staticmethod
    @transaction.atomic()
    def remove_remain(product_label, warehouse, value, size=None):
        amount_rur = Decimal('0.0000000000')
        try:
            label_remain = LabelRemain.objects.get(
                product_label=product_label,
                warehouse=warehouse,
                size=size,
            )
        except LabelRemain.DoesNotExist:
            raise ValueError
        else:
            if label_remain.value < value:
                raise ValueError
            if value:
                item_amount_rur = label_remain.amount_rur / label_remain.value
                amount_rur = item_amount_rur * value
                label_remain.amount_rur -= amount_rur
                label_remain.value -= value
                label_remain.save()
        return value, amount_rur

from config.celery import app

from .utils import RemainHistoryImporter, IsNewCollectionHelper


@app.task(name='products.import_remain_history')
def import_remain_history(data):
    """Загрузка истории остатков
    """
    rp = RemainHistoryImporter(data)
    rp.run()


@app.task(name='products.set_is_new_collection')
def set_is_new_collection():
    """Установка флага активности
    """

    iah = IsNewCollectionHelper()
    iah.run()

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^product/(?P<object_id>\d+)/export/$', views.ProductExport.as_view(), name='product-export'),
]

from decimal import Decimal
import datetime

from django.db import transaction
from django.db.models.aggregates import Sum
from django.db.models.expressions import F

from products.models import Size, RemainHistory, Product
from bot.utils import send_telegram_message
from purchases.models import PurchaseItem
from supply.models import SupplyItem
from warehouses.models import Warehouse


class RemainHistoryImporter():

    def __init__(self, data):
        self.data = data

    def run(self):
        try:
            self.save_history()
        except Exception as e:
            message = 'Проблема с обновлением истории остатков'
            if str(e):
                message += '\nДополнительные данные: ' + str(e)
            send_telegram_message(message=message)
        else:
            send_telegram_message(message='История остатков обновлена. Всего обработано {} записей'.format(len(self.data)))

    @transaction.atomic()
    def save_history(self):
        count = 0
        warehouses = {w.code: w.id for w in Warehouse.objects.all()}
        for i in self.data:
            value_avg = Decimal(i['value_avg'])
            if value_avg:
                RemainHistory.objects.update_or_create(
                    year=i['year'],
                    month=i['month'],
                    product_id=i['product'],
                    warehouse_id=warehouses[i['warehouse']],
                    defaults={
                        'value_avg': value_avg
                    }
                )
            else:
                RemainHistory.objects.filter(
                    year=i['year'],
                    month=i['month'],
                    product_id=i['product'],
                    warehouse_id=warehouses[i['warehouse']],
                ).delete()


class IsNewCollectionHelper():

    def run(self):
        today = datetime.date.today()
        two_month_ago = today - datetime.timedelta(days=60)

        product_ids = set(SupplyItem.objects.filter(
            supply__delivery_date__gte=two_month_ago,
            supply__status__code='delivery_accepted',
        ).values_list('product_id', flat=True))

        product_ids.update(set(PurchaseItem.objects.filter(
            purchase__delivery_date__gte=two_month_ago,
            purchase__status__code='delivery_accepted',
        ).values_list('product_id', flat=True)))

        old_product_ids = set(SupplyItem.objects.filter(
            supply__delivery_date__lt=two_month_ago,
            supply__status__code='delivery_accepted',
            product_id__in=product_ids,
        ).values('product_id').annotate(
            count=Sum(
                F('count_xxs') +
                F('count_xs') +
                F('count_s') +
                F('count_m') +
                F('count_l') +
                F('count_xl') +
                F('count_xxl') +
                F('count_xxxl') +
                F('count_34') +
                F('count_35') +
                F('count_36') +
                F('count_37') +
                F('count_38') +
                F('count_39') +
                F('count_40') +
                F('count_41')
            )
        ).filter(count__gt=1).values_list('product_id', flat=True))

        old_product_ids.update(set(PurchaseItem.objects.filter(
            purchase__delivery_date__lt=two_month_ago,
            purchase__status__code='delivery_accepted',
            product_id__in=product_ids,
        ).values('product_id').annotate(
            count=Sum(
                F('count_xxs') +
                F('count_xs') +
                F('count_s') +
                F('count_m') +
                F('count_l') +
                F('count_xl') +
                F('count_xxl') +
                F('count_xxxl') +
                F('count_34') +
                F('count_35') +
                F('count_36') +
                F('count_37') +
                F('count_38') +
                F('count_39') +
                F('count_40') +
                F('count_41')
            )
        ).filter(count__gt=1).values_list('product_id', flat=True)))

        product_ids -= old_product_ids

        Product.objects.filter(id__in=product_ids).update(is_new_collection=True)
        Product.objects.filter(is_active=True).exclude(id__in=product_ids).update(is_new_collection=False)

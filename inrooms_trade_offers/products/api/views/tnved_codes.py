from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from .. import serializers
from ...models import TnvedCode


class TnvedCodeViewSet(ModelViewSet):
    serializer_class = serializers.TnvedCodeSerializer
    queryset = TnvedCode.objects.all().select_related('material', )
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', 'material__name', )


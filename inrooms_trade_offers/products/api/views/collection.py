from rest_framework.exceptions import ParseError
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from .. import serializers
from ...models import Product, ProductCollection, ProductCollectionItem


class ProductMixin(object):
    def dispatch(self, request, *args, **kwargs):
        try:
            self.product = Product.objects.get(pk=self.kwargs.get('pk'))
        except Product.DoesNotExist:
            raise ParseError('wrong product id')
        return super(ProductMixin, self).dispatch(request, *args, **kwargs)


class ProductCollectionListView(ProductMixin, ListAPIView):
    serializer_class = serializers.ProductCollectionDetailSerializer
    queryset = ProductCollection.objects.all().order_by('-parent', '-order')

    def get_serializer_context(self):
        ctx = super(ProductCollectionListView, self).get_serializer_context()
        ctx['productcollection_id_set'] = set(self.product.productcollection_set.values_list('id', flat=True))
        return ctx

    def post(self, request, **kwargs):
        collection_id = request.data.get('collection_id')
        try:
            pc = ProductCollection.objects.get(id=collection_id)
        except ProductCollection.DoesNotExist:
            raise ParseError('wrong product collection id')
        lookup = {
            'product_collection': pc,
            'product': self.product,
        }
        if ProductCollectionItem.objects.filter(**lookup).exists():
            ProductCollectionItem.objects.filter(**lookup).delete()
        else:
            ProductCollectionItem.objects.create(**lookup)
        return Response({})

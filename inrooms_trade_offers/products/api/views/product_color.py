from rest_framework import filters, status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .. import serializers
from ...models import ProductColor


class ProductColorViewSet(ModelViewSet):
    serializer_class = serializers.ProductColorSerializer
    model = ProductColor
    queryset = ProductColor.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )
    ordering_fields = (
        'name',
    )

    def create(self, request, *args, **kwargs):
        if request.data.get('name', None):
            try:
                color = ProductColor.objects.get(name=request.data['name'])
                return Response(self.get_serializer(color).data, status=status.HTTP_200_OK)
            except ProductColor.DoesNotExist:
                pass
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

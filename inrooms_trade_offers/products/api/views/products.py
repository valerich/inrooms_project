import django_filters
from django.conf import settings
from django.db.models.aggregates import Sum
from django.db.models.query_utils import Q
from rest_framework import filters
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from config.celery import app
from products.models import Product, ProductImage, Remain, ProductCollection
from products.services.product_remain_service import RemainService
from warehouses.models import Warehouse
from .. import serializers
from ... import models


def filter_is_consistency_move(queryset, value):
    if value:
        queryset = queryset.filter(Q(consistency_type1__isnull=False) | Q(small_description=''))
    else:
        queryset = queryset.exclude(small_description='').filter(consistency_type1__isnull=True)
    return queryset


def filter_has_price(queryset, value):
    if value:
        queryset = queryset.filter(price__isnull=False)
    else:
        queryset = queryset.filter(price__isnull=True)
    return queryset


def filter_has_product_images(queryset, value):
    if value:
        queryset = queryset.filter(images__isnull=False)
    else:
        queryset = queryset.filter(images__isnull=True)
    return queryset


def filter_has_offer(queryset, value):
    if value:
        queryset = queryset.filter(offer_id__isnull=False)
    else:
        queryset = queryset.filter(offer_id__isnull=True)
    return queryset


def filter_has_remains(queryset, value):
    product_ids = {
        p_id for p_id, value in RemainService(warehouse_codes=['msk', 'showroom'],
                                              product_ids=queryset.values_list('id',
                                                                               flat=True)).get_flat_remains().items()
        if value > 0}
    if value:
        queryset = queryset.filter(id__in=product_ids)
    else:
        queryset = queryset.exclude(id__in=product_ids)
    return queryset


def filter_warehouse_remains(queryset, value):
    if value:
        codes = value.split(',')
        product_ids = {p_id for p_id, value in
                       RemainService(warehouse_codes=codes,
                                     product_ids=queryset.values_list('id', flat=True)).get_flat_remains().items()
                       if value > 0}
        queryset = queryset.filter(id__in=product_ids)
    return queryset


def filter_warehouse_no_remains(queryset, value):
    if value:
        try:
            warehouse = Warehouse.objects.get(code=value)
        except Warehouse.DoesNotExist:
            pass
        else:
            product_ids = set(Remain.objects.filter(warehouse=warehouse, value__gt=0).values_list('product_id', flat=True))
            product_ids_without_remains = set(Product.objects.all().exclude(remain__warehouse=warehouse).value_list('id', flat=True))
            product_ids = product_ids.union(product_ids_without_remains)

            queryset = queryset.exclude(id__in=product_ids)
    return queryset


def filter_warehouse_id_remains(queryset, value):
    if value:
        try:
            warehouse = Warehouse.objects.get(id=value)
        except Warehouse.DoesNotExist:
            queryset = queryset.none()
        else:
            product_ids = Remain.objects.filter(warehouse=warehouse, value__gt=0).values_list('product_id', flat=True)
            queryset = queryset.filter(id__in=product_ids)
    return queryset


def filter_warehouse_id_no_remains(queryset, value):
    if value:
        try:
            warehouse = Warehouse.objects.get(id=value)
        except Warehouse.DoesNotExist:
            pass
        else:
            product_ids = Remain.objects.filter(warehouse=warehouse, value__gt=0).values_list('product_id', flat=True)
            queryset = queryset.exclude(id__in=product_ids)
    return queryset


def filter_exclude_ids(queryset, value):
    if value:
        exclude_ids = []
        for raw_id in value.split(','):
            raw_id = raw_id.strip()
            try:
                exclude_ids.append(int(raw_id))
            except (ValueError, TypeError):
                pass
        if exclude_ids:
            queryset = queryset.exclude(id__in=exclude_ids)
    return queryset


def filter_include_ids(queryset, value):
    if value:
        include_ids = []
        for raw_id in value.split(','):
            raw_id = raw_id.strip()
            try:
                include_ids.append(int(raw_id))
            except (ValueError, TypeError):
                pass
        if include_ids:
            queryset = queryset.filter(id__in=include_ids)
    return queryset


def filter_main_collection_r(queryset, value):
    if value:
        queryset = queryset.filter(Q(main_collection_id=value) | Q(main_collection__parent_id=value))
    return queryset


class ProductFilterSet(django_filters.FilterSet):
    main_collection_r = django_filters.NumberFilter(action=filter_main_collection_r)
    has_images = django_filters.BooleanFilter(action=filter_has_product_images)
    has_offer = django_filters.BooleanFilter(action=filter_has_offer)
    has_remains = django_filters.BooleanFilter(action=filter_has_remains)
    warehouse_remains = django_filters.CharFilter(action=filter_warehouse_remains)
    warehouse_no_remains = django_filters.CharFilter(action=filter_warehouse_no_remains)
    warehouse_id_remains = django_filters.CharFilter(action=filter_warehouse_id_remains)
    warehouse_id_no_remains = django_filters.CharFilter(action=filter_warehouse_id_no_remains)
    exclude_ids = django_filters.CharFilter(action=filter_exclude_ids)
    include_ids = django_filters.CharFilter(action=filter_include_ids)
    has_price = django_filters.BooleanFilter(action=filter_has_price)
    min_price = django_filters.NumberFilter(name="price", lookup_expr='gte')
    max_price = django_filters.NumberFilter(name="price", lookup_expr='lte')
    is_consistency_move = django_filters.BooleanFilter(action=filter_is_consistency_move)

    class Meta:
        model = models.Product
        fields = [
            'is_public',
            'is_new_collection',
            'brand',
            'main_collection',
            'main_collection_r',
            'is_template',
            'offer',
            'is_active',
            'has_remains',
            'has_images',
            'has_offer',
            'warehouse_remains',
            'warehouse_no_remains',
            'warehouse_id_remains',
            'warehouse_id_no_remains',
            'exclude_ids',
            'include_ids',
            'kind',
            'has_price',
            'price',
            'min_price',
            'max_price',
            'is_consistency_move',
            'brand__code',
            'seasonality',
        ]


class ProductViewSet(ModelViewSet):
    serializer_class = serializers.ProductSerializer
    filter_class = ProductFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter)
    search_fields = ('^article', 'name', '=id')
    queryset = models.Product.objects.order_by('-modified')
    ordering_fields = (
        'id',
        'is_new_collection',
        'name',
        'article',
        'modified',
        'price',
    )

    def get_queryset(self):
        qs = self.queryset
        qs = qs.select_related(
            'main_collection',
            'user',
            'color',
            'brand',
        )
        qs = qs.prefetch_related(
            'images'
        )
        return qs

    def perform_update(self, serializer):
        super(ProductViewSet, self).perform_update(serializer)
        if not settings.IS_TESTING:
            app.send_task('service_1c.notify_1c_product_change', args=((serializer.instance.id,),))

    def perform_create(self, serializer):
        obj = serializer.save(user=self.request.user)
        if obj.offer:
            if self.request.data.get('copy_images', False):
                for image in obj.offer.images.all():
                    ProductImage.objects.create(
                        image=image.image,
                        visual_order=image.visual_order,
                        product=obj,
                    )
        if not settings.IS_TESTING:
            app.send_task('service_1c.notify_1c_product_change', args=((obj.id,),))

    def get_serializer_context(self):
        context = super().get_serializer_context()

        context['remains_service'] = RemainService(
            product_ids=self.get_queryset().values('id'),
            exclude_reserves_for_order_ids=self.request.data.get('exclude_reserves_for_order_ids', None),
        ).get_remains()

        return context

    @detail_route(methods=['post'])
    def copy_characteristics(self, request, pk=None):
        obj = self.get_object()

        if obj.offer:
            queryset = Product.objects.filter(offer=obj.offer).exclude(id=obj.id)
            for another_product in queryset:
                for field_name in [
                    'consistency_type1',
                    'consistency_value1',
                    'consistency_type2',
                    'consistency_value2',
                    'consistency_type3',
                    'consistency_value3',
                    'consistency_type4',
                    'consistency_value4',
                    'consistency_type5',
                    'consistency_value5',
                    'wash',
                    'bleach',
                    'drying',
                    'ironing',
                    'professional_care',
                    'country',
                ]:
                    value = getattr(obj, field_name)
                    setattr(another_product, field_name, value)
                another_product.save()

            if not settings.IS_TESTING:
                product_ids = list(queryset.values_list('id', flat=True))
                app.send_task('service_1c.notify_1c_product_change', args=(product_ids,))

        return Response()

    @detail_route(methods=['post'])
    def set_related_products(self, request, pk=None):
        obj = self.get_object()

        obj.related_products = Product.objects.filter(article=obj.article, color=obj.color).exclude(brand=obj.brand)

        return Response()

    @detail_route(methods=['get'])
    def related_products(self, request, pk=None):
        obj = self.get_object()
        return Response(
            data=serializers.ProductSerializer(
                obj.related_products.all(),
                many=True,
                fields=['id', 'name', 'article', 'color_detail', 'brand_detail', 'image'],
            ).data)

import django_filters
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from ..serializers import RemainSerializer
from ...models import Remain


def filter_has_client(queryset, value):
    if value:
        queryset = queryset.filter(warehouse__client__isnull=False)
    else:
        queryset = queryset.filter(warehouse__client__isnull=True)
    return queryset


class RemainFilterSet(django_filters.FilterSet):
    has_client = django_filters.BooleanFilter(action=filter_has_client)
    value = django_filters.NumberFilter(lookup_type='gt')

    class Meta:
        model = Remain
        fields = [
            'product',
            'warehouse',
            'size',
            'value',
            'has_client',
            'warehouse__client',
        ]


class RemainViewSet(ModelViewSet):
    serializer_class = RemainSerializer
    filter_class = RemainFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)
    search_fields = ('^product__article', 'product__name', '=product__id')
    queryset = Remain.objects.all()
    ordering_fields = (
        'id',
        'product__name',
        'warehouse__name',
    )

    def get_queryset(self):
        qs = self.queryset
        qs = qs.select_related(
            'product',
            'warehouse',
            'size',
        )
        return qs

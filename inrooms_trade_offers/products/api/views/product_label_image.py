from base64 import b64decode

from django.core.files.base import ContentFile
from rest_framework import filters
from rest_framework.exceptions import ParseError
from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from .. import serializers
from ...models import ProductLabel, ProductLabelImage


class ProductLabelMixin(object):
    def dispatch(self, request, *args, **kwargs):
        try:
            self.product_label = ProductLabel.objects.get(pk=self.kwargs.get('pk'))
        except ProductLabel.DoesNotExist:
            raise ParseError('wrong product label id')
        return super(ProductLabelMixin, self).dispatch(request, *args, **kwargs)


class ProductLabelImageListView(ProductLabelMixin, ListAPIView):
    serializer_class = serializers.ProductLabelImageSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = (
        'name',
        'article',
    )

    def get_queryset(self):
        return self.product_label.images.all()


class ProductLabelImageDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.ProductLabelImageSerializer
    queryset = ProductLabelImage.objects.all()


class ProductLabelImageReloadView(APIView):
    def post(self, request, pk, *args, **kwargs):
        image = ProductLabelImage.objects.get(id=pk)
        image_data = request.data['image']
        format, imgstr = image_data.split(';base64,')
        ext = format.split('/')[-1]
        data = ContentFile(b64decode(imgstr), name='image.' + ext)
        image.image = data
        image.save()
        return Response(serializers.ProductImageSerializer(image).data)


class ProductLabelImageUploadView(ProductLabelMixin, APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request, **kwargs):
        image = request.FILES['image']

        ProductLabelImage.objects.create(
            product_label=self.product_label,
            image=image,
        )
        return Response({
            'status': 'ok'
        })


class ProductLabelImageOrderView(APIView):
    def post(self, request, **kwargs):
        for item in request.data:
            ProductLabelImage.objects.filter(
                id=item.get('id')
            ).update(
                visual_order=item.get('visual_order')
            )
        return Response({})

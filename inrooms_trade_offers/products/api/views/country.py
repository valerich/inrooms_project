from rest_framework import filters
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin
from rest_framework.viewsets import GenericViewSet

from ..serializers import CountrySerializer
from ...models import Country


class CountryViewSet(RetrieveModelMixin,
                   ListModelMixin,
                   GenericViewSet):
    serializer_class = CountrySerializer
    model = Country
    queryset = Country.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )
    ordering_fields = (
        'name',
    )

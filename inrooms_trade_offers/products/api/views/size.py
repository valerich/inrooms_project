import django_filters
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework import filters

from products.api.serializers.size import SizeSerializer
from products.models import Size


class SizeFilterSet(django_filters.FilterSet):

    class Meta:
        model = Size
        fields = [
            'series_type',
        ]


class SizeViewSet(RetrieveModelMixin,
                  ListModelMixin,
                  GenericViewSet, ):
    model = Size
    serializer_class = SizeSerializer
    queryset = Size.objects.all()
    filter_class = SizeFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter)
    search_fields = ('name', '=id', 'code')
    ordering_fields = (
        'id',
        'name',
        'wight',
    )
import django_filters
from django.db.models.aggregates import Sum
from django.db.models.query_utils import Q
from rest_framework import filters, mixins
from rest_framework.exceptions import ParseError
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from products.models import Remain, Product
from products.services.product_remain_service import RemainService
from warehouses.models import Warehouse
from .collection import (
    ProductCollectionListView,
)
from .country import CountryViewSet
from .field import (
    IntagApplyView,
    IntagChoiceView,
    IntagListView,
    TemplateListView,
    TemplateApplyView,
    ProductFieldAllClearView,
    ProductFieldChoiceView,
    ProductFieldClearView,
    ProductFieldDetailView,
    ProductFieldListView,
    ProductFieldRemoveView,
    ProductFieldUpdateView,
    TemplateApplyView,
    # TemplateListView,
)
from .image import (
    ProductImageListView,
    ProductImageDetailView,
    ProductImageUploadView,
    ProductImageOrderView,
    ProductImageReloadView,
)
from .product_color import ProductColorViewSet
from .product_label import ProductLabelViewSet
from .product_label_image import (
    ProductLabelImageListView,
    ProductLabelImageDetailView,
    ProductLabelImageUploadView,
    ProductLabelImageOrderView,
    ProductLabelImageReloadView,
)
from .products import (
    ProductViewSet,
)
from .tnved_codes import TnvedCodeViewSet
from .remain import RemainViewSet
from .season_product_collection_plan import SeasonProductCollectionPlanViewSet
from .season_product_collection_plan_item import SeasonProductCollectionPlanItemViewSet
from .size import SizeViewSet
from .. import serializers
from ... import models


class ProductMixin(object):
    def dispatch(self, request, *args, **kwargs):
        try:
            self.product = models.Product.objects.get(pk=self.kwargs.get('pk'))
        except models.Product.DoesNotExist:
            raise ParseError('wrong product id')
        return super(ProductMixin, self).dispatch(request, *args, **kwargs)


def filter_warehouse_remains(queryset, value):
    if value:
        try:
            warehouse = Warehouse.objects.get(code=value)
        except Warehouse.DoesNotExist:
            queryset = queryset.none()
        else:
            product_ids = Remain.objects.filter(warehouse=warehouse, value__gt=0).values_list('product_id', flat=True)
            products = Product.objects.filter(is_active=True, id__in=product_ids)
            queryset = queryset.filter(product__in=products).distinct()
    return queryset


def filter_warehouse_no_remains(queryset, value):
    if value:
        try:
            warehouse = Warehouse.objects.get(code=value)
        except Warehouse.DoesNotExist:
            pass
        else:
            product_ids = Remain.objects.filter(warehouse=warehouse, value__gt=0).values_list('product_id', flat=True)
            queryset = queryset.exclude(product__id__in=product_ids).distinct()
    return queryset


def filter_warehouse_id_remains(queryset, value):
    if value:
        try:
            warehouse = Warehouse.objects.get(id=value)
        except Warehouse.DoesNotExist:
            queryset = queryset.none()
        else:
            product_ids = Remain.objects.filter(warehouse=warehouse, value__gt=0).values_list('product_id', flat=True)
            queryset = queryset.filter(product__id__in=product_ids).distinct()
    return queryset


def filter_warehouse_id_no_remains(queryset, value):
    if value:
        try:
            warehouse = Warehouse.objects.get(id=value)
        except Warehouse.DoesNotExist:
            pass
        else:
            product_ids = Remain.objects.filter(warehouse=warehouse, value__gt=0).values_list('product_id', flat=True)
            queryset = queryset.exclude(product__id__in=product_ids).distinct()
    return queryset


def filter_is_parent(queryset, value):
    if value:
        queryset = queryset.filter(parent__isnull=True)
    else:
        queryset = queryset.filter(parent__isnull=False)
    return queryset


def filter_for_catalog(queryset, value):
    if value:
        queryset = queryset.filter(parent__isnull=True)
        products = Product.objects.filter(
            is_active=True,
            brand__code='zaracity',
        )
        product_ids = {p_id for p_id, value in
                       RemainService(warehouse_codes=['msk', 'showroom'],
                                     product_ids=products.values_list('id', flat=True)).get_flat_remains().items()
                       if value > 0}
        products = products.filter(id__in=product_ids)
        queryset = queryset.filter(Q(product__in=products) | Q(productcollection__product__in=products)).distinct()
    return queryset


class CollectionFilterSet(django_filters.FilterSet):
    warehouse_remains = django_filters.CharFilter(action=filter_warehouse_remains)
    warehouse_no_remains = django_filters.CharFilter(action=filter_warehouse_no_remains)
    warehouse_id_remains = django_filters.CharFilter(action=filter_warehouse_id_remains)
    warehouse_id_no_remains = django_filters.CharFilter(action=filter_warehouse_id_no_remains)
    is_parent = django_filters.BooleanFilter(action=filter_is_parent)
    for_catalog = django_filters.BooleanFilter(action=filter_for_catalog)

    class Meta:
        model = models.ProductCollection
        fields = [
            'warehouse_remains',
            'warehouse_no_remains',
            'warehouse_id_remains',
            'warehouse_id_no_remains',
            'kind',
            'is_parent',
            'for_catalog',
        ]


class CollectionViewSet(ModelViewSet):
    model = models.ProductCollection
    serializer_class = serializers.ProductCollectionSerializer
    queryset = model.objects.all()
    filter_class = CollectionFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)
    search_fields = ('name', )

    def get_serializer_context(self):
        context = super().get_serializer_context()
        queryset = models.ProductCollection.objects.filter(parent__isnull=False)
        products = Product.objects.filter(
            is_active=True,
            brand__code='zaracity',
        )
        product_ids = {p_id for p_id, value in
                       RemainService(warehouse_codes=['msk', 'showroom'],
                                     product_ids=products.values_list('id', flat=True)).get_flat_remains().items()
                       if value > 0}
        products = products.filter(id__in=product_ids)
        queryset = queryset.filter(product__in=products).distinct()
        context['childs_ids'] = queryset.values_list('id', flat=True)
        return context


class ProductFieldViewSet(ModelViewSet):
    model = models.ProductField
    serializer_class = serializers.ProductFieldSerializer
    queryset = model.objects.all()


class IntagChoiceFilterSet(django_filters.FilterSet):
    class Meta:
        model = models.IntagChoice
        fields = [
            'intag',
            'intag__is_for_order',
            'productfield__product',
        ]

    
class IntagChoicesViewSet(mixins.RetrieveModelMixin,
                          mixins.ListModelMixin,
                          GenericViewSet):
    model = models.IntagChoice
    serializer_class = serializers.IntagChoice2Serializer
    queryset = model.objects.all()
    filter_class = IntagChoiceFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, )
    search_fields = ('value', )
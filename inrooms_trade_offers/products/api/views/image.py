from base64 import b64decode

from django.core.files.base import ContentFile
from rest_framework import filters
from rest_framework.exceptions import ParseError
from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from .. import serializers
from ...models import Product, ProductImage


class ProductMixin(object):
    def dispatch(self, request, *args, **kwargs):
        try:
            self.product = Product.objects.get(pk=self.kwargs.get('pk'))
        except Product.DoesNotExist:
            raise ParseError('wrong product id')
        return super(ProductMixin, self).dispatch(request, *args, **kwargs)


class ProductImageListView(ProductMixin, ListAPIView):
    serializer_class = serializers.ProductImageSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = (
        'name',
        'article',
    )

    def get_queryset(self):
        return self.product.images.all()


class ProductImageDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.ProductImageSerializer
    queryset = ProductImage.objects.all()


class ProductImageReloadView(APIView):
    def post(self, request, pk, *args, **kwargs):
        image = ProductImage.objects.get(id=pk)
        image_data = request.data['image']
        format, imgstr = image_data.split(';base64,')
        ext = format.split('/')[-1]
        data = ContentFile(b64decode(imgstr), name='image.' + ext)
        image.image = data
        image.save()
        return Response(serializers.ProductImageSerializer(image).data)


class ProductImageUploadView(ProductMixin, APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request, **kwargs):
        image = request.FILES['image']

        ProductImage.objects.create(
            product=self.product,
            image=image,
        )
        return Response({
            'status': 'ok'
        })


class ProductImageOrderView(APIView):
    def post(self, request, **kwargs):
        for item in request.data:
            ProductImage.objects.filter(
                id=item.get('id')
            ).update(
                visual_order=item.get('visual_order')
            )
        return Response({})

import django_filters
from rest_framework import filters
from rest_framework.viewsets import ViewSet, ModelViewSet

from products.api.serializers.season_product_collection_plan import SeasonProductCollectionPlanSerializer
from products.models import SeasonProductCollectionPlan


class SeasonProductCollectionPlanFilterSet(django_filters.FilterSet):

    class Meta:
        model = SeasonProductCollectionPlan
        fields = [
            'year',
            'season',
        ]


class SeasonProductCollectionPlanViewSet(ModelViewSet):
    serializer_class = SeasonProductCollectionPlanSerializer
    filter_class = SeasonProductCollectionPlanFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)
    search_fields = ('=year', )
    queryset = SeasonProductCollectionPlan.objects.all()

    def perform_create(self, serializer):
        instance = serializer.save()
        instance.create_items()


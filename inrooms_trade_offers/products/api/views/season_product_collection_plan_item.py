from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import SeasonProductCollectionPlanItemSerializer
from ...models import SeasonProductCollectionPlan, SeasonProductCollectionPlanItem


class SeasonProductCollectionPlanItemViewSet(ModelViewSet):
    serializer_class = SeasonProductCollectionPlanItemSerializer
    model = SeasonProductCollectionPlanItem
    queryset = SeasonProductCollectionPlanItem.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('^product_collection__name', )
    ordering_fields = (
        'product_collection__name',
    )

    def dispatch(self, request, plan_pk: int, *args, **kwargs):
        self.plan = get_object_or_404(SeasonProductCollectionPlan, pk=plan_pk)
        return super(SeasonProductCollectionPlanItemViewSet, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(season_product_collection_plan=self.plan)

    def get_queryset(self):
        qs = super(SeasonProductCollectionPlanItemViewSet, self).get_queryset()
        qs = qs.filter(season_product_collection_plan=self.plan)
        return qs

import django_filters
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from products.services.label_remain_service import LabelRemainService
from .. import serializers
from ... import models


def filter_has_images(queryset, value):
    if value:
        queryset = queryset.filter(images__isnull=False)
    else:
        queryset = queryset.filter(images__isnull=True)
    return queryset


def filter_exclude_ids(queryset, value):
    if value:
        exclude_ids = []
        for raw_id in value.split(','):
            raw_id = raw_id.strip()
            try:
                exclude_ids.append(int(raw_id))
            except (ValueError, TypeError):
                pass
        if exclude_ids:
            queryset = queryset.exclude(id__in=exclude_ids)
    return queryset


class ProductLabelFilterSet(django_filters.FilterSet):
    has_images = django_filters.BooleanFilter(action=filter_has_images)
    exclude_ids = django_filters.CharFilter(action=filter_exclude_ids)

    class Meta:
        model = models.ProductLabel
        fields = [
            'kind',
            'has_images',
            'exclude_ids',
        ]


class ProductLabelViewSet(ModelViewSet):
    serializer_class = serializers.ProductLabelSerializer
    filter_class = ProductLabelFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)
    search_fields = ('name', '=id')
    queryset = models.ProductLabel.objects.order_by('-modified')
    ordering_fields = (
        'id',
        'name',
        'kind',
        'modified',
    )

    def get_queryset(self):
        qs = self.queryset
        qs = qs.select_related(
            'user'
        )
        qs = qs.prefetch_related(
            'images'
        )
        return qs

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['remains_service'] = LabelRemainService(
            product_label_ids=self.get_queryset().values('id'),
        ).get_remains()

        return context

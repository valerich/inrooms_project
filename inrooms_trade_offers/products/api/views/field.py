from rest_framework import filters
from rest_framework.exceptions import ParseError
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from .. import serializers
from ...models import Intag, IntagChoice, Product, ProductField


# from abe.utils.rest.pagination import PageNumberPaginationBy10andPageSize


class TemplateListView(ListCreateAPIView):
    serializer_class = serializers.TemplateSerializer
    queryset = Product.objects.filter(is_template=True)
    filter_backends = (filters.SearchFilter,)
    search_fields = (
        'name',
    )


class TemplateApplyView(APIView):
    def post(self, request):
        try:
            product = Product.objects.get(id=request.data.get('product'))
        except Product.DoesNotExist:
            raise ParseError('wrong product id')
        try:
            product_template = Product.objects.get(id=request.data.get('template'))
        except Product.DoesNotExist:
            raise ParseError('wrong product template id')

        current_product_fields = product.productfield_set.all().values_list('intag_id', flat=True)
        for pf in product_template.productfield_set.all().prefetch_related('intag'):
            if pf.intag.id not in current_product_fields:
                choices = pf.intag_choices.all()
                pf.id = None
                pf.product = product
                pf.save()
                pf.intag_choices.clear()
                pf.intag_choices.add(*list(choices))

        return Response({})


class ProductFieldMixin(object):
    def get_queryset(self):
        try:
            obj = Product.objects.get(pk=self.kwargs.get('pk'))
        except Product.DoesNotExist:
            raise ParseError('wrong product id')
        return obj.productfield_set.all().prefetch_related(
            'intag_choices',
        ).select_related(
            'intag',
        )


class ProductFieldListView(ProductFieldMixin, ListCreateAPIView):
    serializer_class = serializers.ProductFieldSerializer


class ProductFieldAllClearView(ProductFieldMixin, APIView):
    def post(self, request, **kwargs):
        qs = self.get_queryset()
        for pf in qs:
            pf.intag_choices.clear()
        return Response({})


class ProductFieldRemoveView(ProductFieldMixin, APIView):
    def post(self, request, **kwargs):
        qs = self.get_queryset()
        qs.delete()
        return Response({})


class ProductFieldUpdateView(ProductFieldMixin, APIView):
    def post(self, request, **kwargs):
        qs = self.get_queryset()
        return Response({})


class ProductFieldDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.ProductFieldSerializer
    queryset = ProductField.objects.all()


class ProductFieldChoiceView(APIView):
    def get_pf(self):
        try:
            obj = ProductField.objects.get(id=self.kwargs.get('pk'))
            return obj
        except ProductField.DoesNotExist:
            raise ParseError('wrong productfield id')

    def get_ic(self):
        try:
            ic = IntagChoice.objects.get(id=self.request.data.get('choice'))
            return ic
        except ProductField.DoesNotExist:
            raise ParseError('wrong productfield id')

    def post(self, request, **kwargs):
        pf = self.get_pf()
        ic = self.get_ic()
        if not pf.intag.is_many:
            pf.intag_choices.clear()
        pf.intag_choices.add(ic)
        return Response({})

    def put(self, request, **kwargs):
        pf = self.get_pf()
        if not pf.intag.is_many:
            pf.intag_choices.clear()
        ic = IntagChoice.objects.create(
            intag=pf.intag,
            value=self.request.data.get('choice'),
        )
        pf.intag_choices.add(ic)
        return Response({})

    def delete(self, request, **kwargs):
        pf = self.get_pf()
        ic = self.get_ic()
        pf.intag_choices.remove(ic)
        return Response({})


class ProductFieldClearView(APIView):
    def get_pf(self):
        try:
            obj = ProductField.objects.get(id=self.kwargs.get('pk'))
            return obj
        except ProductField.DoesNotExist:
            raise ParseError('wrong productfield id')

    def post(self, request, **kwargs):
        pf = self.get_pf()
        pf.intag_choices.clear()
        return Response({})


class IntagListView(ListAPIView):
    serializer_class = serializers.IntagSerializer
    queryset = Intag.objects.all()
    filter_backends = (filters.SearchFilter,)
    search_fields = (
        'name',
    )


class IntagChoiceView(ListAPIView):
    serializer_class = serializers.IntagChoiceSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = (
        'value',
    )

    def get_queryset(self):
        try:
            intag = Intag.objects.get(pk=self.kwargs.get('pk'))
        except Intag.DoesNotExist:
            raise ParseError('wrong intag id')
        return intag.choices.all()


class IntagApplyView(APIView):
    def post(self, request):
        try:
            product = Product.objects.get(id=request.data.get('product'))
        except Product.DoesNotExist:
            raise ParseError('wrong product id')
        try:
            intag = Intag.objects.get(id=request.data.get('intag'))
        except Intag.DoesNotExist:
            raise ParseError('wrong product template id')
        ProductField.objects.get_or_create(product=product, intag=intag)
        return Response({})


class TemplateApplyView(APIView):
    def post(self, request):
        try:
            product = Product.objects.get(id=request.data.get('product'))
        except Product.DoesNotExist:
            raise ParseError('wrong product id')
        try:
            product_template = Product.objects.get(id=request.data.get('template'))
        except Product.DoesNotExist:
            raise ParseError('wrong product template id')

        current_product_fields = product.productfield_set.all().values_list('intag_id', flat=True)
        for pf in product_template.productfield_set.all().prefetch_related('intag'):
            if pf.intag.id not in current_product_fields:
                choices = pf.intag_choices.all()
                pf.id = None
                pf.product = product
                pf.save()
                pf.intag_choices.clear()
                pf.intag_choices.add(*list(choices))

        return Response({})

from django.conf.urls import include, url
from rest_framework import routers

from . import views

router = routers.SimpleRouter()

router.register(r'country', views.CountryViewSet, base_name='country')
router.register(r'product', views.ProductViewSet, base_name='product')
router.register(r'product_label', views.ProductLabelViewSet, base_name='product_label')
router.register(r'collection', views.CollectionViewSet, base_name='collection')
router.register(r'productfield', views.ProductFieldViewSet, base_name='productfield')
router.register(r'intag_choices', views.IntagChoicesViewSet, base_name='intag_choices')
router.register(r'color', views.ProductColorViewSet, base_name='product_color')
router.register(r'remain', views.RemainViewSet, base_name='remain')
router.register(r'season_product_collection_plan/(?P<plan_pk>\d+)/items', views.SeasonProductCollectionPlanItemViewSet, base_name='season_product_collection_plan_items'),
router.register(r'season_product_collection_plan', views.SeasonProductCollectionPlanViewSet, base_name='season_product_collection_plan')
router.register(r'tnved_code', views.TnvedCodeViewSet, base_name='tnved_code')
router.register(r'size', views.SizeViewSet, base_name='size')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^product/(?P<pk>\d+)/collection/$', views.ProductCollectionListView.as_view(), name='product-collection-list'),
    url(r'^product/(?P<pk>\d+)/image/$', views.ProductImageListView.as_view(), name='image-list'),
    url(r'^product/(?P<pk>\d+)/image-upload/$', views.ProductImageUploadView.as_view(), name='image-upload'),
    url(r'^product_label/(?P<pk>\d+)/image/$', views.ProductLabelImageListView.as_view(), name='product_label_image-list'),
    url(r'^product_label/(?P<pk>\d+)/image-upload/$', views.ProductLabelImageUploadView.as_view(), name='product_label_image-upload'),
    url(r'^product/(?P<pk>\d+)/productfield/$', views.ProductFieldListView.as_view(), name='productfield-list'),
    url(r'^product/(?P<pk>\d+)/productfield/clear/$', views.ProductFieldAllClearView.as_view(), name='productfield-all-clear'),
    url(r'^product/(?P<pk>\d+)/productfield/remove/$', views.ProductFieldRemoveView.as_view(), name='productfield-remove'),
    url(r'^product/(?P<pk>\d+)/productfield/update/$', views.ProductFieldUpdateView.as_view(), name='productfield-update'),
    url(r'^intag/$', views.IntagListView.as_view(), name='intag-list'),
    url(r'^intag/apply/$', views.IntagApplyView.as_view(), name='intag-apply'),
    url(r'^intag/(?P<pk>\d+)/choices/$', views.IntagChoiceView.as_view(), name='intag-choice'),
    url(r'^template/$', views.TemplateListView.as_view(), name='template-list'),
    url(r'^template/apply/$', views.TemplateApplyView.as_view(), name='template-apply'),
    url(r'^image/(?P<pk>\d+)/$', views.ProductImageDetailView.as_view(), name='image-detail'),
    url(r'^image/(?P<pk>\d+)/reload/$', views.ProductImageReloadView.as_view(), name='image-reload'),
    url(r'^image-order/$', views.ProductImageOrderView.as_view(), name='image-order'),
    url(r'^product_label_image/(?P<pk>\d+)/$', views.ProductLabelImageDetailView.as_view(), name='product_label_image-detail'),
    url(r'^product_label_image/(?P<pk>\d+)/reload/$', views.ProductLabelImageReloadView.as_view(), name='product_label_image-reload'),
    url(r'^product_label_image-order/$', views.ProductLabelImageOrderView.as_view(), name='product_label_image-order'),
    url(r'^productfield/(?P<pk>\d+)/choice/$', views.ProductFieldChoiceView.as_view(), name='productfield-choice'),
    url(r'^productfield/(?P<pk>\d+)/clear/$', views.ProductFieldClearView.as_view(), name='productfield-clear'),
]

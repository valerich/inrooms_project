from rest_framework import serializers

from products.models import SeasonProductCollectionPlanItem


class SeasonProductCollectionPlanItemSerializer(serializers.ModelSerializer):
    product_collection_detail = serializers.SerializerMethodField()

    class Meta:
        model = SeasonProductCollectionPlanItem
        fields = (
            'id',
            'product_collection',
            'plan',
            'product_collection_detail',
        )

    def get_product_collection_detail(self, obj):
        return {
            'id': obj.product_collection.id,
            'name': obj.product_collection.name,
        }

from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from products.services.label_remain_service import LabelRemainService
from ...models import ProductLabel


class ProductLabelSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    user_detail = serializers.SerializerMethodField()
    kind_detail = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()
    remains = serializers.SerializerMethodField()

    class Meta:
        model = ProductLabel

        fields = [
            'id',
            'kind',
            'kind_detail',
            'name',
            'user_detail',
            'image',
            'images',
            'created',
            'modified',
            'country',
            'structure',
            'more_information',
            'remains',
        ]

    def get_kind_detail(self, obj):
        return {
            'id': obj.kind,
            'name': obj.get_kind_display(),
        }

    def get_user_detail(self, obj):
        return {
            'id': obj.user_id,
            'name': obj.user.name,
        }

    def get_image(self, obj):
        pi = obj.images.all().first()
        return self.resize_image(pi, obj)

    def get_images(self, obj):
        data = []
        for image in obj.images.all():
            item = self.resize_image(image, obj)
            if item:
                data.append(item)
        return data

    def resize_image(self, pi, obj):
        if pi:
            try:
                _200x200 = get_thumbnail(pi.image, '400x400', crop="center").url
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
            except IOError as e:
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1500 = request.build_absolute_uri(_950x1200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,
                    '950x1200': _950x1200}
        else:
            return None

    def get_has_remains(self, obj):
        return True if obj.remain_sum else False

    def get_remains(self, obj):
        if 'remain_service' in self.context:
            remain_service = self.context['remains_service']
        else:
            remain_service = LabelRemainService(
                product_label_ids=[obj.id, ],
            ).get_remains()
        return remain_service.get(obj.id, {})

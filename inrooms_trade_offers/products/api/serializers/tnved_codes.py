from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import TnvedGroup, TnvedCode, TnvedMaterial


class TnvedGroupSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = TnvedGroup
        fields = [
            'id',
            'name',
        ]


class TnvedMaterialSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = TnvedMaterial
        fields = [
            'id',
            'name',
        ]


class TnvedCodeSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    material_detail = serializers.SerializerMethodField()

    class Meta:
        model = TnvedCode
        fields = [
            'id',
            'name',
            'code',
            'material_detail',
        ]

    def get_material_detail(self, obj):
        if obj.material:
            return TnvedMaterialSerializer(obj.material).data
        return None

from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import Size


class SizeSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = Size
        fields = [
            'id',
            'name',
            'code',
            'code_1c',
            'weight',
            'multiplier',
        ]

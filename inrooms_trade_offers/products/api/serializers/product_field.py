from rest_framework import serializers

from .intag import IntagChoiceSerializer, IntagDetailSerializer
from ...models import ProductField


class ProductFieldSerializer(serializers.ModelSerializer):
    intag = IntagDetailSerializer(read_only=True)
    intag_choices = IntagChoiceSerializer(many=True, read_only=True)

    class Meta:
        model = ProductField
        fields = [
            'id',
            'intag',
            'intag_choices',
            'value',
        ]

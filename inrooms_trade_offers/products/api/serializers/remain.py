from rest_framework import serializers

from products.api.serializers.product import ProductSerializer
from products.api.serializers.size import SizeSerializer
from warehouses.api.serializers import WarehouseSerializer
from ...models import Remain


class RemainSerializer(serializers.ModelSerializer):
    product_detail = serializers.SerializerMethodField()
    size_detail = serializers.SerializerMethodField()
    warehouse_detail = serializers.SerializerMethodField()

    class Meta:
        model = Remain
        fields = [
            'id',
            'warehouse_detail',
            'product_detail',
            'size_detail',
            'value',
        ]

    def get_warehouse_detail(self, obj):
        return WarehouseSerializer(obj.warehouse).data

    def get_product_detail(self, obj):
        return ProductSerializer(obj.product, fields=['id', 'name', 'article']).data

    def get_size_detail(self, obj):
        if obj.size:
            return SizeSerializer(obj.size, fields=['id', 'name', 'code']).data
        return obj.size

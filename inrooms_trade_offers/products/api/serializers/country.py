from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import Country


class CountrySerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = [
            'id',
            'name',
        ]

from rest_framework import serializers

from products.models import SeasonProductCollectionPlan


class SeasonProductCollectionPlanSerializer(serializers.ModelSerializer):
    season_detail = serializers.SerializerMethodField()

    class Meta:
        model = SeasonProductCollectionPlan
        fields = (
            'id',
            'year',
            'season',
            'season_detail',
            'units_per_s_m',
            'avg_unit_cost',
        )

    def get_season_detail(self, obj):
        return {
            'id': obj.season,
            'name': obj.get_season_display(),
        }

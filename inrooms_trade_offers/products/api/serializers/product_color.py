from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import ProductColor


class ProductColorSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = ProductColor
        fields = [
            'id',
            'name',
        ]

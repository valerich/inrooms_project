from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from brands.api.serializers.brand import BrandSerializer
from care.api.serializers.care_item import CareItemSerializer
from comments.api.serializers import CommentSerializer
from extensions.drf.serializers import DynamicFieldsSerializerMixin
from products.api.serializers.country import CountrySerializer
from products.services import RemainService
from .product_collection import ProductCollectionSerializer
from ...models import Product


class ArticleValidator(object):

    def __call__(self, value):
        if len(value) == 8 and value[:4].isdigit() and value[4] == '/' and value[5:].isdigit():
            pass
        else:
            raise serializers.ValidationError('Артикул должен соответствовать маске 9999/999.')


class ProductSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    main_collection_detail = serializers.SerializerMethodField()
    user_detail = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    comments = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()
    color_detail = serializers.SerializerMethodField()
    kind_detail = serializers.SerializerMethodField()
    has_remains = serializers.SerializerMethodField()
    remains = serializers.SerializerMethodField()
    offer_detail = serializers.SerializerMethodField()
    consistency_type1_detail = serializers.SerializerMethodField()
    consistency_type2_detail = serializers.SerializerMethodField()
    consistency_type3_detail = serializers.SerializerMethodField()
    consistency_type4_detail = serializers.SerializerMethodField()
    consistency_type5_detail = serializers.SerializerMethodField()
    brand_detail = serializers.SerializerMethodField()
    country_detail = serializers.SerializerMethodField()
    full_consistency = serializers.SerializerMethodField()
    wash_detail = serializers.SerializerMethodField()
    bleach_detail = serializers.SerializerMethodField()
    drying_detail = serializers.SerializerMethodField()
    ironing_detail = serializers.SerializerMethodField()
    professional_care_detail = serializers.SerializerMethodField()

    class Meta:
        model = Product

        fields = [
            'id',
            'is_new_collection',
            'brand',
            'brand_detail',
            'country',
            'country_detail',
            'offer',
            'offer_detail',
            'article',
            'name',
            'business_name',
            'main_collection',
            'main_collection_detail',
            'description',
            'image',
            'user_detail',
            'created',
            'modified',
            'comments',
            'images',
            'links',
            'is_active',
            'is_public',
            'color',
            'color_detail',
            'kind',
            'kind_detail',
            'has_remains',
            'price',
            'public_price',
            'public_old_price',
            'remains',
            'intag_weight',
            'small_description',
            # Уход
            'wash',
            'wash_detail',
            'bleach',
            'bleach_detail',
            'drying',
            'drying_detail',
            'ironing',
            'ironing_detail',
            'professional_care',
            'professional_care_detail',
            # Состав
            'consistency_type1',
            'consistency_type1_detail',
            'consistency_value1',
            'consistency_type2',
            'consistency_type2_detail',
            'consistency_value2',
            'consistency_type3',
            'consistency_type3_detail',
            'consistency_value3',
            'consistency_type4',
            'consistency_type4_detail',
            'consistency_value4',
            'consistency_type5',
            'consistency_type5_detail',
            'consistency_value5',
            'tnved_code',
            'full_consistency',
            'seasonality',
        ]
        extra_kwargs = {
            "article": {
                "validators": [ArticleValidator()],
            },
        }

    def get_status_detail(self, obj):
        return {'id': obj.status_id,
                'name': obj.status.name,
                'code': obj.status.code,
                'icon': obj.status.icon,
                'color': obj.status.color,
                'to_edit_product_data': obj.status.to_edit_product_data,
                'next_statuses': [{
                    'id': s.id,
                    'name': s.name,
                    'code': s.code,
                    'icon': s.icon,
                    'color': s.color,
                } for s in obj.status.next_status.all()]}

    def get_main_collection_detail(self, obj):
        if obj.main_collection:
            return ProductCollectionSerializer(obj.main_collection).data
        else:
            return None

    def get_brand_detail(self, obj):
        if obj.brand:
            return BrandSerializer(obj.brand).data
        return None

    def get_country_detail(self, obj):
        if obj.country:
            return CountrySerializer(obj.country).data
        return None

    def get_consistency_type1_detail(self, obj):
        if obj.consistency_type1:
            return {
                'id': obj.consistency_type1.id,
                'name': obj.consistency_type1.name
            }
        return None

    def get_consistency_type2_detail(self, obj):
        if obj.consistency_type2:
            return {
                'id': obj.consistency_type2.id,
                'name': obj.consistency_type2.name
            }
        return None

    def get_consistency_type3_detail(self, obj):
        if obj.consistency_type3:
            return {
                'id': obj.consistency_type3.id,
                'name': obj.consistency_type3.name
            }
        return None

    def get_consistency_type4_detail(self, obj):
        if obj.consistency_type4:
            return {
                'id': obj.consistency_type4.id,
                'name': obj.consistency_type4.name
            }
        return None

    def get_consistency_type5_detail(self, obj):
        if obj.consistency_type5:
            return {
                'id': obj.consistency_type5.id,
                'name': obj.consistency_type5.name
            }
        return None

    def get_user_detail(self, obj):
        return {'id': obj.user_id,
                'name': obj.user.name}

    def get_kind_detail(self, obj):
        return {'id': obj.kind,
                'name': obj.get_kind_display()}

    def get_image(self, obj):
        pi = obj.images.all().first()
        return self.resize_image(pi, obj)

    def get_images(self, obj):
        data = []
        for image in obj.images.all():
            item = self.resize_image(image, obj)
            if item:
                data.append(item)
        return data

    def resize_image(self, pi, obj):
        if pi:
            try:
                _200x200 = get_thumbnail(pi.image, '400x400', crop="center").url
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
            except IOError as e:
                logger.warning('Проблемы с изображением {} продукта {}: {}'.format(pi.image, obj, e))
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1500 = request.build_absolute_uri(_950x1200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,
                    '950x1200': _950x1200}
        else:
            return None

    def get_comments(self, obj):
        data = []
        for comment in obj.comments.all():
            data.append(CommentSerializer(comment).data)
        return data

    def get_color_detail(self, obj):
        return {'id': obj.color_id,
                'name': obj.color.name,
                'code': obj.color.code, }

    def get_has_remains(self, obj):
        return True if obj.remain_sum else False

    def get_remains(self, obj):
        if 'remain_service' in self.context:
            remain_service = self.context['remains_service']
        else:
            remain_service = RemainService(
                product_ids=[obj.id, ],
            ).get_remains()
        return remain_service.get(obj.id, {})

    def get_offer_detail(self, obj):
        if obj.offer:
            return {
                'id': obj.offer.id,
                'cost': obj.offer.cost,
                'delivery_cost': obj.offer.delivery_cost,
                'cost_rur': obj.offer.cost_rur,
                'cost_chn': obj.offer.delivery_cost + obj.offer.cost,
                'price': obj.offer.price
            }
        else:
            return None

    def get_full_consistency(self, obj):
        return obj.get_full_consistency()

    def get_wash_detail(self, obj):
        if obj.wash:
            return CareItemSerializer(obj.wash).data
        return None

    def get_bleach_detail(self, obj):
        if obj.bleach:
            return CareItemSerializer(obj.bleach).data
        return None

    def get_drying_detail(self, obj):
        if obj.drying:
            return CareItemSerializer(obj.drying).data
        return None

    def get_ironing_detail(self, obj):
        if obj.ironing:
            return CareItemSerializer(obj.ironing).data
        return None

    def get_professional_care_detail(self, obj):
        if obj.professional_care:
            return CareItemSerializer(obj.professional_care).data
        return None


class TemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'id',
            'name'
        ]

from rest_framework import serializers

from ...models import Intag, IntagChoice, IntagUnit


class IntagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Intag
        fields = [
            'id',
            'name',
            'functional_description'
        ]


class IntagUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = IntagUnit
        fields = [
            'id',
            'name',
            'unit_name',
        ]


class IntagDetailSerializer(serializers.ModelSerializer):
    unit = IntagUnitSerializer(many=True, read_only=True)

    class Meta:
        model = Intag
        fields = [
            'id',
            'kind',
            'weight',
            'category',
            'name',
            'is_many',
            'unit',
            # 'is_required',
            'functional_description'
        ]


class IntagChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = IntagChoice
        fields = [
            'id',
            'value',
        ]


class IntagChoice2Serializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = IntagChoice
        fields = [
            'id',
            'name',
        ]

    def get_name(self, obj):
        return '{} / {}'.format(obj.intag.name, obj.value)

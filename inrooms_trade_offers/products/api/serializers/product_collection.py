from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import ProductCollection


class ProductCollectionSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()
    childs = serializers.SerializerMethodField()

    @staticmethod
    def get_full_name(pc):
        if pc.parent_id:
            return u'{} \ {}'.format(pc.parent.name, pc.name)
        return pc.name

    class Meta:
        model = ProductCollection
        fields = [
            'id',
            'name',
            'full_name',
            'series_type',
            'kind',
            'childs',
        ]

    def get_childs(self, obj):
        data = []
        lookup = {}
        if self.context.get('childs_ids', None):
            lookup['id__in'] = self.context['childs_ids']
        for pc in obj.productcollection_set.filter(**lookup):
            data.append(ProductCollectionSerializer(pc, fields=['id', 'name', 'full_name']).data)
        return data


class ProductCollectionDetailSerializer(serializers.ModelSerializer):
    active = serializers.SerializerMethodField()

    def get_active(self, obj):
        return obj.id in self.context['productcollection_id_set']

    class Meta:
        model = ProductCollection
        fields = [
            'id',
            'name',
            'parent',
            'active',
        ]

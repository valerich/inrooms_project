import factory
import factory.django
import factory.fuzzy

from accounts.factories import UserFactory
from brands.factories import BrandFactory
from warehouses.factories import WarehouseFactory
from .models import (
    Product,
    ProductCollection,
    ProductColor,
    ProductImage,
    ProductLabel,
    ProductLabelImage,
    Remain,
    Size,
)


class ProductColorFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()

    class Meta:
        model = ProductColor


class ProductCollectionFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()

    class Meta:
        model = ProductCollection


class SizeFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText(length=10)
    code = factory.fuzzy.FuzzyText(length=10)
    code_1c = factory.fuzzy.FuzzyText(length=10)

    class Meta:
        model = Size


class ProductFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    main_collection = factory.SubFactory(ProductCollectionFactory)
    user = factory.SubFactory(UserFactory)
    article = factory.Sequence(lambda n: n)
    color = factory.SubFactory(ProductColorFactory)
    brand = factory.SubFactory(BrandFactory)

    class Meta:
        model = Product


class ProductLabelFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = ProductLabel


class RemainFactory(factory.django.DjangoModelFactory):
    product = factory.SubFactory(ProductFactory)
    warehouse = factory.SubFactory(WarehouseFactory)
    size = factory.SubFactory(SizeFactory)
    value = factory.fuzzy.FuzzyInteger(1000)

    class Meta:
        model = Remain


class ProductImageFactory(factory.django.DjangoModelFactory):
    product = factory.SubFactory(ProductFactory)
    image = factory.django.ImageField(color='blue')

    class Meta:
        model = ProductImage


class ProductLabelImageFactory(factory.django.DjangoModelFactory):
    product_label = factory.SubFactory(ProductLabelFactory)
    image = factory.django.ImageField(color='blue')

    class Meta:
        model = ProductLabelImage

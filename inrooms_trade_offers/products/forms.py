from autocomplete_light import shortcuts as autocomplete_light
from django import forms


class SeasonProductCollectionPlanAdminForm(forms.ModelForm):

    class Meta:
        pass


class SeasonProductCollectionPlanItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'product_collection': autocomplete_light.ChoiceWidget('ProductCollectionParentAutocomplete')
        }


class IntagAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'category': autocomplete_light.ChoiceWidget('IntagCategoryAutocomplete'),
            'unit': autocomplete_light.MultipleChoiceWidget('IntagUnitAutocomplete'),
        }


class IntagChoiceAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'intag': autocomplete_light.ChoiceWidget('IntagAutocomplete'),
        }


class ProductAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'main_collection': autocomplete_light.ChoiceWidget('ProductCollectionAutocomplete'),
            'user': autocomplete_light.ChoiceWidget('UserAutocomplete'),
        }


class ProductCollectionAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'parent': autocomplete_light.ChoiceWidget('ProductCollectionAutocomplete'),
        }


class ProductCollectionItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
            'product_collection': autocomplete_light.ChoiceWidget('ProductCollectionAutocomplete'),
        }


class ProductFieldAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'intag': autocomplete_light.ChoiceWidget('IntagAutocomplete'),
            'intag_choices': autocomplete_light.MultipleChoiceWidget('IntagChoiceAutocomplete'),
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
        }


class ProductImageAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
        }


class ProductStatusForm(forms.ModelForm):

    class Meta:
        widgets = {
            'next_status': autocomplete_light.MultipleChoiceWidget('ProductStatusAutocomplete')
        }


class RemainAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
            'size': autocomplete_light.ChoiceWidget('SizeAutocomplete'),
            'warehouse': autocomplete_light.ChoiceWidget('WarehouseAutocomplete'),
        }


class LabelRemainAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'product_label': autocomplete_light.ChoiceWidget('ProductLabelAutocomplete'),
            'size': autocomplete_light.ChoiceWidget('SizeAutocomplete'),
            'warehouse': autocomplete_light.ChoiceWidget('WarehouseAutocomplete'),
        }

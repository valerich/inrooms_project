from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.status import HTTP_403_FORBIDDEN, HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from config.celery import app
from products.models import Product
from warehouses.models import Warehouse


class RemainHistoryView(APIView):

    def post(self, request):
        if request.user.has_perm('auth.api_public_remain_history_post'):
            if self.is_valid(request.data):
                # import_remain_history(request.data)
                app.send_task('products.import_remain_history', args=(request.data,))
                return Response(data={'message': 'Данные приняты к обработке'}, status=HTTP_200_OK)
            else:
                return Response(data={'error': ','.join(self.errors)}, status=HTTP_400_BAD_REQUEST)
        else:
            return Response(data={'error': 'Не достаточно прав'}, status=HTTP_403_FORBIDDEN)

    def is_valid(self, data):
        self.errors = []
        self.product_ids = set(Product.all_objects.values_list('id', flat=True))
        self.warehouse_codes = set(Warehouse.objects.values_list('code', flat=True))
        fields = {
            'year',
            'month',
            'product',
            'warehouse',
            'value_avg',
        }
        if isinstance(data, list):
            for i in data:
                for field_name in fields:
                    try:
                        if not getattr(self, 'validate_{}'.format(field_name))(i[field_name]):
                            return False
                    except KeyError:
                        self.errors.append('Не передано поле {}'.format(field_name))
                        return False
        else:
            self.errors.append('Ожидается список')
            return False
        return True

    def validate_year(self, value):
        if isinstance(value, int) and 2000 <= value <= 3000:
            return True
        self.errors.append('Не верное значение year: {}'.format(value))
        return False

    def validate_month(self, value):
        if isinstance(value, int) and 1 <= value <= 12:
            return True
        self.errors.append('Не верное значение month: {}'.format(value))
        return False

    def validate_product(self, value):
        if isinstance(value, int) and value in self.product_ids:
            return True
        self.errors.append('Не верное значение product: {}'.format(value))
        return False

    def validate_warehouse(self, value):
        if isinstance(value, str)and value in self.warehouse_codes:
            return True
        self.errors.append('Не верное значение warehouse: {}'.format(value))
        return False

    def validate_value_avg(self, value):
        if isinstance(value, (int, float)):
            return True
        elif isinstance(value, str) and value.replace('.', '', 1).isdigit():
            return True
        self.errors.append('Не верное значение value_avg: {}'.format(value))
        return False
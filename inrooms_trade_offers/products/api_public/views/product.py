import django_filters
from rest_framework.decorators import list_route
from rest_framework.mixins import CreateModelMixin, ListModelMixin, UpdateModelMixin
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework import filters

from extensions.drf.views import MultiSerializerViewSetMixin
from products.services.product_remain_service import RemainService
from ..serializers import ProductWeightSerializer, ProductSerializer
from ...models import Product


class ProductFilterSet(django_filters.FilterSet):

    class Meta:
        model = Product
        fields = [
            'brand',
        ]


class ProductViewSet(MultiSerializerViewSetMixin,
                     ListModelMixin,
                     GenericViewSet):
    serializers = {
        'default': ProductSerializer
    }
    model = Product
    queryset = Product.objects.all()
    filter_class = ProductFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter)
    search_fields = ('^article', 'name', '=id')
    ordering_fields = (
        'id',
        'name',
        'article',
        'price',
    )

    def get_queryset(self):
        qs = super().get_queryset()
        if self.action in ['list', 'retrieve']:
            qs.order_by('id')
            qs = qs.filter(is_active=True)
            qs = qs.filter(is_public=True)
            qs = qs.select_related(
                'main_collection',
                'main_collection__parent',
                'color',
                'brand',
                'country',
                'consistency_type1',
                'consistency_type2',
                'consistency_type3',
                'consistency_type4',
                'wash',
                'bleach',
                'drying',
                'ironing',
                'professional_care',
            )
            qs = qs.prefetch_related('images', 'related_products')
        return qs

    def get_serializer_context(self):
        context = super().get_serializer_context()

        product_ids = set(self.get_queryset().values_list('id', flat=True))
        related_ids = set(self.get_queryset().values_list('related_products', flat=True))
        product_ids.update(related_ids)

        context['remains_service'] = RemainService(
            product_ids=product_ids,
            warehouse_codes=['msk', 'showroom']
        ).get_remains_by_size()

        return context


    @list_route(methods=['post', 'get'])
    def weight(self, request, **kwargs):

        queryset = self.queryset

        if request.method == 'POST':

            serializer = ProductWeightSerializer(data=request.data, many=True)

            if serializer.is_valid():
                for i in request.data:
                    Product.objects.filter(id=i['id']).update(intag_weight=i.get('intag_weight', None))
                return Response(serializer.data, status=HTTP_200_OK)
            else:
                return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

        else:
            ids = self.request.GET.get('ids', '')
            if ids:
                queryset = queryset.filter(pk__in=ids.split(','))
            else:
                queryset = queryset.none()
            serializer = ProductWeightSerializer(queryset, many=True)

            return Response(serializer.data)

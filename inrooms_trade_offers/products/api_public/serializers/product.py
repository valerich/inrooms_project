from rest_framework import serializers

from brands.models import Brand
from care.models import CareItem
from consistency.models import ConsistencyItem
from prices.api.serializers.product_collection import ProductCollectionSerializer
from products.models import Country, ProductColor, ProductCollection, ProductImage
from products.services.product_remain_service import RemainService
from ...models import Product


class ConsistencySerializer(serializers.ModelSerializer):

    class Meta:
        model = ConsistencyItem
        fields = [
            'id',
            'name',
        ]


class CareItemSeralizer(serializers.ModelSerializer):

    class Meta:
        model = CareItem
        fields = [
            'id',
            'image1',
            'image2',
            'image3',
            'image4',
            'description',
        ]


class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = [
            'id',
            'name',
        ]


class BrandSeralizer(serializers.ModelSerializer):

    class Meta:
        model = Brand
        fields = [
            'id',
            'name',
            'image',
        ]


class ProductColorSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductColor
        fields = [
            'id',
            'name',
        ]


class ProductCollectionSerializer(serializers.ModelSerializer):
    parent = serializers.SerializerMethodField()

    class Meta:
        model = ProductCollection
        fields = [
            'id',
            'name',
            'parent',
        ]

    def get_parent(self, obj):
        if obj.parent:
            return {
                'id': obj.parent.id,
                'name': obj.parent.name,
            }
        return None


class ProductImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductImage
        fields = [
            'image'
        ]


class ProductSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    main_collection = ProductCollectionSerializer()
    seasonality = serializers.SerializerMethodField()
    color = ProductColorSerializer()
    brand = BrandSeralizer()
    country = CountrySerializer()
    consistency_type1 = ConsistencySerializer()
    consistency_type2 = ConsistencySerializer()
    consistency_type3 = ConsistencySerializer()
    consistency_type4 = ConsistencySerializer()
    wash = CareItemSeralizer()
    bleach = CareItemSeralizer()
    drying = CareItemSeralizer()
    ironing = CareItemSeralizer()
    professional_care = CareItemSeralizer()
    images = ProductImageSerializer(many=True)
    price = serializers.SerializerMethodField()
    old_price = serializers.SerializerMethodField()
    remains = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = [
            'id',
            'is_active',
            'name',
            'article',
            'main_collection',
            'seasonality',
            'description',
            'color',
            'brand',
            'country',
            'price',
            'old_price',

            'consistency_type1',
            'consistency_value1',
            'consistency_type2',
            'consistency_value2',
            'consistency_type3',
            'consistency_value3',
            'consistency_type4',
            'consistency_value4',

            'wash',
            'bleach',
            'drying',
            'ironing',
            'professional_care',

            'images',

            'remains',
        ]

    def get_name(self, obj):
        return obj.business_name

    def get_price(self, obj):
        return obj.public_price

    def get_old_price(self, obj):
        return obj.public_old_price

    def get_seasonality(self, obj):
        return {
            'id': obj.seasonality,
            'name': obj.get_seasonality_display(),
        }

    def get_images(self, obj):
        data = []
        for i in obj.images.all():
            data.append(i.image.url)
        return data

    def get_remains(self, obj):
        related_products_ids = [i.id for i in obj.related_products.all()]
        if 'remain_service' in self.context:
            remains_by_size = self.context['remains_service']
        else:
            product_ids = [obj.id, ] + related_products_ids
            remains_by_size = RemainService(product_ids=product_ids,
                                            warehouse_codes=['msk', 'showroom']).get_remains_by_size()

        remains = remains_by_size[obj.id]
        for id in related_products_ids:
            related_remains = remains_by_size[id]
            for size_code, value in related_remains.items():
                remains[size_code] += value

        return remains


class ProductWeightSerializer(serializers.ModelSerializer):
    intag_weight = serializers.IntegerField(min_value=0, max_value=32767)

    class Meta:
        model = Product
        fields = [
            'id',
            'intag_weight',
        ]

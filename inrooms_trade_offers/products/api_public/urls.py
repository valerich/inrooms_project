from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import (
    ProductViewSet,
    RemainHistoryView,
)

router = DefaultRouter()
router.register(r'product', ProductViewSet, base_name='product')

urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
    urls.url(r'^remain_history/$', RemainHistoryView.as_view(), name='remain_history'),
]

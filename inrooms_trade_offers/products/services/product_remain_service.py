from collections import defaultdict

from products.models import Remain
from warehouses.models import Warehouse


class RemainService(object):

    def __init__(self,
                 product_ids,
                 warehouse_ids=None,
                 warehouse_codes=None,
                 exclude_reserves_for_order_ids=None):
        self._product_ids = product_ids
        self._warehouse_ids = warehouse_ids
        self._warehouse_codes = warehouse_codes
        self._exclude_reserves_for_order_ids = exclude_reserves_for_order_ids

    def get_remains(self):
        """
        Возвращает остатки продуктов
        :return {product_id: {warehouse_code: {size_code: value}}}
        """
        return self._get_remains()

    def get_remains_by_warehouse(self):
        """
        Возвращает остатки продуктов
        :return {product_id: {warehouse_code: value}}
        """
        remains = self._get_remains()
        remains_by_warehouse = defaultdict(lambda: defaultdict(int))
        for product_id, warehouse_remains in remains.items():
            for warehouse_code, size_remains in warehouse_remains.items():
                remains_by_warehouse[product_id][warehouse_code] = sum(size_remains.values())
        return remains_by_warehouse

    def get_remains_by_size(self):
        """
        Возвращает остатки продуктов
        :return {product_id: {size_code: value}}
        """
        remains = self._get_remains()
        remains_by_size = defaultdict(lambda: defaultdict(int))
        for product_id, warehouse_remains in remains.items():
            for warehouse_code, size_remains in warehouse_remains.items():
                for size_code, value in size_remains.items():
                    remains_by_size[product_id][size_code] += value
        return remains_by_size

    def get_remains_by_size_with_supply(self, without_order_reserve=True):
        """
        Возвращает остатки продуктов с учетом товаров в пути
        :return {product_id: {size_code: value}}
        """

        remains_data = self.get_remains_by_size()
        supply_data = self._get_supply_helper().get_products_by_size(without_order_reserve=without_order_reserve)
        for product_id, size_data in supply_data.items():
            for size_code, quantity in size_data.items():
                remains_data[product_id][size_code] += quantity
        return remains_data

    def _get_supply_helper(self):
        from supply.helpers.supply import SupplyHelper
        return SupplyHelper(
            order_ids=self._exclude_reserves_for_order_ids,
            supply_statuses=[
                'sent_to_warehouse',
                'set_prices',
                'delivery_waiting',
            ],
            product_ids=self._product_ids
        )


    def get_flat_remains(self):
        """
        Возвращает остатки по продуктам без указания точек
        :return {product_id: value}
        """
        remains_by_warehouse = self.get_remains_by_warehouse()
        return {
            product_id: sum(warehouse_remains.values())
            for product_id, warehouse_remains in remains_by_warehouse.items()
        }

    def _get_remains_data(self):
        remains = Remain.objects.filter(
            warehouse__in=self._get_warehouses(),
            product_id__in=self._product_ids,
        ).order_by(
        ).values_list(
            'product_id',
            'size__code',
            'warehouse__code',
            'value',
        )

        return remains

    def _get_reserves_data(self):
        from orders.models import OrderItemSize
        reserves_data = defaultdict(int)

        # Резервы по заказам
        qs = OrderItemSize.objects.filter(is_local_reserve=True, is_delivery_reserve=False)
        qs = qs.filter(warehouse__in=self._get_warehouses())
        qs = qs.filter(order_item__product_id__in=self._product_ids)
        if self._exclude_reserves_for_order_ids:
            qs = qs.exclude(order_item__order_id__in=self._exclude_reserves_for_order_ids)
        qs = qs.values('order_item__product_id',
                       'warehouse__code',
                       'size__code',
                       'quantity')
        for ois in qs:
            reserves_data[(ois['order_item__product_id'],
                           ois['warehouse__code'],
                           ois['size__code'])] += ois['quantity']
        return reserves_data

    # @cached_method
    def _get_warehouses(self):
        qs = Warehouse.objects.all()
        if self._warehouse_ids is not None:
            qs = qs.filter(id__in=self._warehouse_ids)
        if self._warehouse_codes is not None:
            qs = qs.filter(code__in=self._warehouse_codes)
        return qs

    # @cached_method
    def _get_remains(self):
        remains_data = self._get_remains_data()
        reserves_data = self._get_reserves_data()

        remains = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))

        for product_id, size_code, warehouse_code, value in remains_data:
            reserve = reserves_data[(product_id, warehouse_code, size_code)]
            product_remain = value - reserve
            if product_remain < 0:
                product_remain = 0
            remains[product_id][warehouse_code][size_code] += product_remain

        for product_id in list(remains.keys()):
            remains_by_warehouse = remains[product_id]
            for warehouse_code in list(remains_by_warehouse.keys()):
                remains_by_size = remains_by_warehouse[warehouse_code]
                for size_code in list(remains_by_size.keys()):
                    if remains_by_size[size_code] <= 0:
                        del remains[product_id][warehouse_code][size_code]
                if not remains_by_warehouse[warehouse_code]:
                    del remains[product_id][warehouse_code]
        return remains

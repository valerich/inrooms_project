from collections import defaultdict

from products.models import LabelRemain
from warehouses.models import Warehouse


class LabelRemainService(object):

    def __init__(self, product_label_ids, warehouse_ids=None, warehouse_codes=None):
        self._product_label_ids = product_label_ids
        self._warehouse_ids = warehouse_ids
        self._warehouse_codes = warehouse_codes

    def get_remains(self):
        """
        Возвращает остатки продуктов
        :return {product_label_id: {warehouse_code: {size_code: value}}}
        """
        return self._get_remains()

    def get_remains_by_warehouse(self):
        """
        Возвращает остатки продуктов
        :return {product_label_id: {warehouse_code: value}}
        """
        remains_by_warehouse = defaultdict(lambda: defaultdict(int))

        for product_label_id, warehouse_remains in self._get_remains_data():
            for warehouse_code, size_remains in warehouse_remains.items():
                remains_by_warehouse[product_label_id][warehouse_code] = sum(size_remains.values())
        return remains_by_warehouse

    def get_remains_by_size(self):
        """
        Возвращает остатки продуктов
        :return {product_label_id: {size_code: value}}
        """
        remains = self._get_remains_data()
        remains_by_size = defaultdict(lambda: defaultdict(int))

        for product_label_id, warehouse_remains in remains.items():
            for warehouse_code, size_remains in warehouse_remains.items():
                for size_code, value in size_remains.items():
                    remains_by_size[product_label_id][size_code] += value
        return remains_by_size

    def get_flat_remains(self):
        """
        Возвращает остатки по продуктам без указания точек
        :return {product_label_id: value}
        """
        remains_by_warehouse = self.get_remains_by_warehouse()

        return {
            product_label_id: sum(warehouse_remains.values())
            for product_label_id, warehouse_remains in remains_by_warehouse.items()
        }

    def _get_remains_data(self):
        remains = LabelRemain.objects.filter(
            warehouse__in=self._get_warehouses(),
            product_label__in=self._product_label_ids,
        ).values_list(
            'product_label',
            'size__code',
            'warehouse__code',
            'value',
        )

        return remains

    def _get_warehouses(self):
        qs = Warehouse.objects.all()

        if self._warehouse_ids is not None:
            qs = qs.filter(id__in=self._warehouse_ids)
        if self._warehouse_codes is not None:
            qs = qs.filter(code__in=self._warehouse_codes)
        return qs

    def _get_remains(self):
        remains_data = self._get_remains_data()
        remains = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))

        for product_label_id, size_code, warehouse_code, value in remains_data:
            # TODO: может ли оно быть < 0?
            if value < 0:
                value = 0
            remains[product_label_id][warehouse_code][size_code] += value

        for product_label_id in list(remains.keys()):
            remains_by_warehouse = remains[product_label_id]
            for warehouse_code in list(remains_by_warehouse.keys()):
                remains_by_size = remains_by_warehouse[warehouse_code]
                for size_code in list(remains_by_size.keys()):
                    if remains_by_size[size_code] <= 0:
                        del remains[product_label_id][warehouse_code][size_code]
                if not remains_by_warehouse[warehouse_code]:
                    del remains[product_label_id][warehouse_code]
        return remains

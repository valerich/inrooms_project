from django.test import TestCase

from ..factories import PurchaseFactory, PurchaseItemFactory, PurchaseStatusFactory
from ..models import Purchase, PurchaseItem, PurchaseStatus


class PurchaseFactoryTestCase(TestCase):
    def setUp(self):
        Purchase.objects.all().delete()

    def test_factory(self):
        object = PurchaseFactory()
        object_from_db = Purchase.objects.all()[0]
        self.assertEqual(object, object_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(PurchaseFactory())

        self.assertEqual(len(objects), len(Purchase.objects.all()))


class PurchaseItemFactoryTestCase(TestCase):
    def setUp(self):
        PurchaseItem.objects.all().delete()

    def test_factory(self):
        object = PurchaseItemFactory()
        object_from_db = PurchaseItem.objects.all()[0]
        self.assertEqual(object, object_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(PurchaseItemFactory())

        self.assertEqual(len(objects), len(PurchaseItem.objects.all()))


class PurchaseStatusFactoryTestCase(TestCase):
    def setUp(self):
        PurchaseStatus.objects.all().delete()

    def test_factory(self):
        object = PurchaseStatusFactory()
        object_from_db = PurchaseStatus.objects.all()[0]
        self.assertEqual(object, object_from_db)

    def test_multiple_factory(self):
        objects = []
        for i in range(10):
            objects.append(PurchaseStatusFactory())

        self.assertEqual(len(objects), len(PurchaseStatus.objects.all()))

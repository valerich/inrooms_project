import datetime

from django.test import TestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from ..factories import PurchaseFactory
from ..models import Purchase, PurchaseStatus


class PurchaseManagerTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(PurchaseManagerTestCase, cls).setUpClass()

        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')

    def test_overdue(self):
        """ Просроченные закупки

        Это закупки, контрольная дата доставки которых меньше чем текущая и статус не конечный
        """

        overdue_date = datetime.date.today() - datetime.timedelta(days=1)
        status = PurchaseStatus.objects.filter(next_status__isnull=False)[0]
        PurchaseFactory(creator=self.user_bm, delivery_date=overdue_date, status=status)
        PurchaseFactory(creator=self.user_pa, delivery_date=overdue_date, status=status)

        # Без указания пользователя результат - все не выполненные закупки
        self.assertEqual(Purchase.objects.all().overdue().count(), 2)

from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'purchases'
    verbose_name = 'Закупки'

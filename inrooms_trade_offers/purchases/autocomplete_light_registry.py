from autocomplete_light import shortcuts as autocomplete_light

from .models import DeliveryType, Purchase, PurchaseStatus


class PurchaseDeliveryTypeAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


class PurchaseAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['id', ]
    attrs = {
        'placeholder': 'Номер',
        'data-autocomplete-minimum-characters': 0,
    }


class PurchaseStatusAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (DeliveryType, PurchaseDeliveryTypeAutocomplete),
    (Purchase, PurchaseAutocomplete),
    (PurchaseStatus, PurchaseStatusAutocomplete),
)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

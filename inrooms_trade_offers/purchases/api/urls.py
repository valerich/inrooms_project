from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import DeliveryTypeViewSet, LuggageSpaceViewSet, PurchaseItemViewSet, PurchaseStatusViewSet, PurchaseViewSet

router = DefaultRouter()
router.register(r'delivery_type', DeliveryTypeViewSet)
router.register(r'purchase', PurchaseViewSet, base_name='purchase')
router.register(r'purchase/(?P<purchase_pk>\d+)/items', PurchaseItemViewSet, base_name='purchase_items')
router.register(r'purchase/(?P<purchase_pk>\d+)/luggage_spaces', LuggageSpaceViewSet, base_name='purchase_luggagespaces')
router.register(r'status', PurchaseStatusViewSet)


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
]

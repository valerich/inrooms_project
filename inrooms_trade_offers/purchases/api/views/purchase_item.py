from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import PurchaseItemSerializer
from ...models import Purchase, PurchaseItem


class PurchaseItemViewSet(ModelViewSet):
    serializer_class = PurchaseItemSerializer
    model = PurchaseItem
    queryset = PurchaseItem.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('^product__article', 'product__name')
    ordering_fields = (
        'product__article',
        'product__name',
    )

    def dispatch(self, request, purchase_pk: int, *args, **kwargs):
        self.purchase = get_object_or_404(Purchase, pk=purchase_pk)
        return super(PurchaseItemViewSet, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(purchase=self.purchase)

    def get_queryset(self):
        qs = super(PurchaseItemViewSet, self).get_queryset()
        qs = qs.filter(purchase=self.purchase)
        return qs

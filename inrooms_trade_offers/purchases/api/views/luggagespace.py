from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import LuggageSpaceSerializer
from ...models import Purchase, LuggageSpace


class LuggageSpaceViewSet(ModelViewSet):
    serializer_class = LuggageSpaceSerializer
    queryset = LuggageSpace.objects.all()
    ordering_fields = (
        'id',
        'count',
        'weight',
    )

    def dispatch(self, request, purchase_pk: int, *args, **kwargs):
        self.purchase = get_object_or_404(Purchase, pk=purchase_pk)
        return super(LuggageSpaceViewSet, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        instance = serializer.save(purchase=self.purchase)
        return instance

    def get_queryset(self):
        qs = super(LuggageSpaceViewSet, self).get_queryset()
        qs = qs.filter(purchase=self.purchase)
        return qs

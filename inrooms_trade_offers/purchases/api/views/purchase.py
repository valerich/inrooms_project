import django_filters
from django.db import transaction
from rest_framework import filters, status
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from extensions.drf.views import MultiSerializerViewSetMixin
from ..serializers import PurchaseSerializer
from ...models import Purchase, PurchaseStatus


def filter_is_overdue(queryset, value):
    if value:
        queryset = queryset.overdue()
    else:
        queryset = queryset.not_overdue()
    return queryset


def filter_to_set_prices(queryset, value):
    if value:
        queryset = queryset.to_set_prices()
    return queryset


class PurchaseFilterSet(django_filters.FilterSet):
    is_overdue = django_filters.BooleanFilter(action=filter_is_overdue)
    to_set_prices = django_filters.BooleanFilter(action=filter_to_set_prices)

    class Meta:
        model = Purchase
        fields = [
            'is_overdue',
            'to_set_prices',
            'creator',
            'performer',
            'items__product',
            'delivery_client',
            'delivery_warehouse_to',
            'status',
            'created',
            'modified',
        ]


class PurchaseViewSet(MultiSerializerViewSetMixin, ModelViewSet):
    serializers = {
        'default': PurchaseSerializer,
        'list': PurchaseSerializer,
        'create': PurchaseSerializer,
        'retrieve': PurchaseSerializer,
    }
    model = Purchase
    filter_class = PurchaseFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', '^items__product__article', 'items__product__name')
    ordering_fields = (
        'id',
        'created',
        'modified',
    )

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)

    def get_queryset(self):
        qs = Purchase.objects.all().select_related(
            'delivery_type',
            'payment_method', )
        qs = qs.prefetch_related(
            'items',
        )
        return qs

    @detail_route(methods=['post'])
    @transaction.atomic()
    def change_status(self, request, pk=None):
        purchase = self.get_object()
        if purchase.delivery_client and purchase.delivery_date:
            status_code = request.data.get('code', None)
            if status_code:
                try:
                    new_status = PurchaseStatus.objects.get(code=status_code)
                except PurchaseStatus.DoesNotExist:
                    pass
                else:
                    psw = 'auth.can_purchase_set_'
                    approved_status_codes = [
                        perm[len(psw):] for perm in request.user.get_all_permissions() if perm.startswith(psw)]
                    if status_code in approved_status_codes:
                        purchase.status = new_status
                        purchase.save()
                        purchase.process_purchase()
                        return Response(status=status.HTTP_200_OK)
            return Response({'error': 'Не верный статус'},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            errors = []
            if not purchase.delivery_client:
                errors.append('Не заполнен партнер')
            if not purchase.delivery_date:
                errors.append('Не заполнена дата поставки')
            return Response({'error': '\n'.join(errors)},
                            status=status.HTTP_400_BAD_REQUEST)

    @list_route()
    def summary(self, request):
        data = {}
        qs = Purchase.objects.all()
        data['to_set_prices'] = qs.to_set_prices().count()
        return Response(data)

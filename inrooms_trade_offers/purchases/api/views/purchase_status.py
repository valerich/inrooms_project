from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import PurchaseStatusSerializer
from ...models import PurchaseStatus


class PurchaseStatusViewSet(mixins.RetrieveModelMixin,
                          mixins.ListModelMixin,
                          GenericViewSet):
    serializer_class = PurchaseStatusSerializer
    queryset = PurchaseStatus.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )

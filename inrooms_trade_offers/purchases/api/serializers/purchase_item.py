from rest_framework import serializers

from products.api.serializers import ProductSerializer
from ...models import PurchaseItem


class PurchaseItemSerializer(serializers.ModelSerializer):
    series_type = serializers.CharField(required=False)
    product_detail = serializers.SerializerMethodField()

    class Meta:
        model = PurchaseItem
        fields = [
            'id',
            'price',
            'product',
            'product_detail',
            'series_type',
            'count_xxs',
            'count_xs',
            'count_s',
            'count_m',
            'count_l',
            'count_xl',
            'count_xxl',
            'count_xxxl',
            'count_34',
            'count_35',
            'count_36',
            'count_37',
            'count_38',
            'count_39',
            'count_40',
            'count_41',
        ]

    def get_product_detail(self, obj):
        if obj.product_id:
            return ProductSerializer(obj.product).data
        else:
            return None

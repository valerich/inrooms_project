from rest_framework import serializers

from .purchase_item import PurchaseItemSerializer
from ...models import Purchase, PurchaseStatus


class PurchaseSerializer(serializers.ModelSerializer):
    status_detail = serializers.SerializerMethodField()
    items = PurchaseItemSerializer(many=True, read_only=True)
    prices_assigned = serializers.SerializerMethodField()
    delivery_client_detail = serializers.SerializerMethodField()
    delivery_warehouse_to_detail = serializers.SerializerMethodField()

    class Meta:
        model = Purchase

        fields = [
            'id',
            'status_detail',
            'created',
            'modified',
            'description',
            'amount_full',
            'payment_method',
            'amount_paid',
            'items',
            'prices_assigned',
            'delivery_number',
            'delivery_type',
            'delivery_date',
            'delivery_amount_full',
            'delivery_client',
            'delivery_client_detail',
            'delivery_warehouse_to',
            'delivery_warehouse_to_detail',
        ]

    def get_status_detail(self, obj):
        psw = 'auth.can_purchase_set_'
        work_status_codes = [
            perm[len(psw):] for perm in self.context['request'].user.get_all_permissions() if perm.startswith(psw)]
        user_status_ids = PurchaseStatus.objects.filter(code__in=work_status_codes).values_list('id', flat=True)
        return {'id': obj.status_id,
                'name': obj.status.name,
                'code': obj.status.code,
                'next_statuses': [{
                    'id': s.id,
                    'name': s.name,
                    'button_name': s.button_name,
                    'code': s.code,
                } for s in obj.status.next_status.filter(id__in=user_status_ids)]}

    def get_prices_assigned(self, obj):
        for item in obj.items.all():
            if not item.price:
                return False
        return True

    def get_delivery_client_detail(self, obj):
        if obj.delivery_client:
            return {
                'id': obj.delivery_client.id,
                'name': obj.delivery_client.name,
            }
        else:
            return None

    def get_delivery_warehouse_to_detail(self, obj):
        data = {
            'id': obj.delivery_warehouse_to.id,
            'name': obj.delivery_warehouse_to.name,
            'code': obj.delivery_warehouse_to.code,
            'brand_detail': None,
        }
        if obj.delivery_warehouse_to.brand:
            data['brand_detail'] = {
                'id': obj.delivery_warehouse_to.brand_id,
            }
        return data
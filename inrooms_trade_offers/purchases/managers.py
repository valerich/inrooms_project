from django.db import models
from django.utils import timezone


class PurchaseManager(models.Manager):

    def get_queryset(self):
        return PurchaseQuerySet(self.model)


class PurchaseQuerySet(models.query.QuerySet):

    def overdue(self):
        """Просроченные закупки

        Это закупки, контрольная дата доставки которых меньше чем текущая и статус не конечный
        """

        return self.filter(
            delivery_date__lt=timezone.now()
        ).filter(
            status__next_status__isnull=False
        )

    def not_overdue(self):
        """Не просроченные закупки
        """

        return self.exclude(
            delivery_date__lt=timezone.now()
        ).exclude(
            status__next_status__isnull=False
        )

    def to_set_prices(self):
        from purchases.models import PurchaseStatus
        status, c = PurchaseStatus.objects.get_or_create(
            code='sent_to_warehouse',
            defaults={
                'name': 'Отправлен на склад'
            }
        )
        return self.filter(status=status)

from django.contrib import admin

from core.admin import NotDeleteModelAdmin
from .forms import PurchaseAdminForm, PurchaseItemAdminForm, PurchaseStatusAdminForm
from .models import DeliveryType, LuggageSpace, Purchase, PurchaseItem, PurchaseStatus


class DeliveryTypeAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )


class LuggageSpaceInline(admin.TabularInline):
    model = LuggageSpace
    extra = 0


class PurchaseItemAdminInline(admin.TabularInline):
    model = PurchaseItem
    form = PurchaseItemAdminForm
    extra = 0


class PurchaseAdmin(NotDeleteModelAdmin):
    list_display = ('id', 'status', 'created', 'modified', )
    search_fields = ('id', )
    list_filter = ('status', )
    form = PurchaseAdminForm
    actions = ('send_to_1c_action',)
    inlines = (LuggageSpaceInline, PurchaseItemAdminInline)

    def send_to_1c_action(self, request, queryset):
        for purchase in queryset:
            purchase.create_1c_document()
        self.message_user(request, 'Отправлено на выгрузку в 1с')

    send_to_1c_action.short_description = 'Выгрузить в 1с'


class PurchaseStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name', 'weight')
    search_fields = ('name', 'slug',)
    form = PurchaseStatusAdminForm


for model_or_iterable, admin_class in (
    (DeliveryType, DeliveryTypeAdmin),
    (Purchase, PurchaseAdmin),
    (PurchaseStatus, PurchaseStatusAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

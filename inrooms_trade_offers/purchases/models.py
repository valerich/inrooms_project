from django.db import models, transaction
from django_extensions.db.models import TimeStampedModel

from clients.models import Client
from products.models import SERIES_TYPE_CHOICES, Remain, Size
from warehouses.models import Warehouse
from .managers import PurchaseManager


class PurchaseStatus(models.Model):
    name = models.CharField('Название', max_length=100, unique=True)
    code = models.CharField('Код', max_length=20, unique=True, db_index=True)
    button_name = models.CharField('Название на кнопке', max_length=100, blank=True)

    create_1c_document = models.BooleanField('Отправлять документ в 1с?', default=False)

    set_product_price = models.BooleanField('Изменять цены товаров?', default=False)

    next_status = models.ManyToManyField('self', verbose_name=u'Следующие статусы', blank=True, symmetrical=False)
    weight = models.PositiveSmallIntegerField('Вес', default=0)

    class Meta:
        verbose_name = 'Статус'
        verbose_name_plural = 'Статусы'
        ordering = ('-weight', )

    def __str__(self):
        return self.name


class DeliveryType(models.Model):
    name = models.CharField('Тип доставки', max_length=255, unique=True)

    class Meta:
        verbose_name = 'Тип доставки'
        verbose_name_plural = 'Типы доставки'
        ordering = ('name', )

    def __str__(self):
        return self.name


def get_default_warehouse_to():
    return Warehouse.objects.get_or_create(code='msk', defaults={'name': 'Склад Москвы', 'to_orders': True})[0].id


def get_default_status():
    return PurchaseStatus.objects.get_or_create(code='new', defaults={'name': 'Новый'})[0].id


class Purchase(TimeStampedModel):
    status = models.ForeignKey(PurchaseStatus, verbose_name='Статус', default=get_default_status)
    description = models.TextField('Описание', blank=True)
    payment_method = models.ForeignKey('payments.PaymentMethod', verbose_name='Тип оплаты', blank=True, null=True)
    amount_full = models.PositiveIntegerField('Полная стоимость', default=0)
    amount_paid = models.PositiveIntegerField('Оплаченная сумма', default=0)

    creator = models.ForeignKey('accounts.User', verbose_name='Создатель', related_name='purchase_creator')
    performer = models.ForeignKey('accounts.User', verbose_name='Исполнитель', related_name='purchase_performer',
                                  blank=True, null=True, )

    delivery_number = models.CharField('Номер поставки', max_length=7, blank=True)
    delivery_type = models.ForeignKey(DeliveryType, verbose_name='Тип доставки', blank=True, null=True)
    delivery_amount_full = models.DecimalField('Стоимость отправки ($)', default=0, max_digits=20, decimal_places=10)
    delivery_date = models.DateField('Дата доставки', blank=True, null=True)
    delivery_client = models.ForeignKey(Client, verbose_name='Поставщик', blank=True, null=True,
                                        on_delete=models.PROTECT)
    delivery_warehouse_to = models.ForeignKey(Warehouse, verbose_name='На склад', related_name='purchase_to',
                                              default=get_default_warehouse_to, on_delete=models.PROTECT)


    objects = PurchaseManager()

    class Meta:
        verbose_name = 'Поставка'
        verbose_name_plural = 'Поставки'
        ordering = ('-id', )

    def __str__(self):
        return '№:{}'.format(self.id)

    def create_1c_document(self):
        """Создание документа для синхронизации с 1с"""

        from service_1c.utils.purchase_exporter import PurchaseExporter
        document = PurchaseExporter(self.id).create_document()
        return document

    def process_purchase(self, create_1c_document=True):
        if self.status.set_product_price:
            for item in self.items.all():
                if not item.set_product_price:
                    with transaction.atomic():
                        item.product.price = item.price
                        item.product.save()
                        item.set_product_price = True
                        item.save()
        if self.status.create_1c_document and create_1c_document:
            self.create_1c_document()

    def get_delivery_weight(self):
        weight = 0
        for ls in self.luggagespace_set.all():
            weight += ls.weight
        return weight


class LuggageSpace(models.Model):
    purchase = models.ForeignKey(Purchase, verbose_name='Поставка')
    count = models.IntegerField('Количество', default=1)
    weight = models.DecimalField('Вес', default=0, max_digits=20, decimal_places=10)

    class Meta:
        verbose_name = 'Багажное место'
        verbose_name_plural = 'Багажные места'
        ordering = ('-id', )

    def __str__(self):
        return '{} - {} ({})'.format(
            self.purchase,
            self.id,
            self.weight, )


class PurchaseItem(models.Model):
    SERIES_TYPE = SERIES_TYPE_CHOICES
    purchase = models.ForeignKey(Purchase, verbose_name='Закупка', related_name="items")
    product = models.ForeignKey('products.Product', verbose_name='Товар')

    price = models.DecimalField('Цена', blank=True, null=True, max_digits=12, decimal_places=2)

    set_product_price = models.BooleanField('Цена товаров изменена?', default=False)

    series_type = models.CharField('Тип серии', max_length=1, choices=SERIES_TYPE)

    count_xxs = models.PositiveSmallIntegerField('XXS', default=0)
    count_xs = models.PositiveSmallIntegerField('XS', default=0)
    count_s = models.PositiveSmallIntegerField('S', default=0)
    count_m = models.PositiveSmallIntegerField('M', default=0)
    count_l = models.PositiveSmallIntegerField('L', default=0)
    count_xl = models.PositiveSmallIntegerField('XL', default=0)
    count_xxl = models.PositiveSmallIntegerField('XXL', default=0)
    count_xxxl = models.PositiveSmallIntegerField('XXXL', default=0)
    count_34 = models.PositiveSmallIntegerField('34', default=0)
    count_35 = models.PositiveSmallIntegerField('35', default=0)
    count_36 = models.PositiveSmallIntegerField('36', default=0)
    count_37 = models.PositiveSmallIntegerField('37', default=0)
    count_38 = models.PositiveSmallIntegerField('38', default=0)
    count_39 = models.PositiveSmallIntegerField('39', default=0)
    count_40 = models.PositiveSmallIntegerField('40', default=0)
    count_41 = models.PositiveSmallIntegerField('41', default=0)

    class Meta:
        verbose_name = 'Товар в закупке'
        verbose_name_plural = 'Товары в закупке'
        ordering = ('-id', )
        unique_together = ('purchase', 'product')

    def get_quantity(self):
        quantity = 0
        for field_name in [_ for _ in dir(self) if _.startswith('count_')]:
            quantity += getattr(self, field_name)
        return quantity


def purchase_item_set_fields_by_product(sender, instance, **kwargs):
    if instance.product_id:
        if not instance.series_type:
            instance.series_type = instance.product.main_collection.series_type


models.signals.pre_save.connect(purchase_item_set_fields_by_product, sender=PurchaseItem)

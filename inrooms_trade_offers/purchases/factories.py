import factory
import factory.django
import factory.fuzzy

from accounts.factories import UserFactory
from payments.factories import PaymentMethodFactory
from products.factories import ProductFactory
from .models import Purchase, PurchaseItem, PurchaseStatus


class PurchaseStatusFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    code = factory.fuzzy.FuzzyText()

    class Meta:
        model = PurchaseStatus


class PurchaseFactory(factory.django.DjangoModelFactory):
    status = factory.SubFactory(PurchaseStatusFactory)
    payment_method = factory.SubFactory(PaymentMethodFactory)
    creator = factory.SubFactory(UserFactory)
    performer = factory.SubFactory(UserFactory)

    class Meta:
        model = Purchase


class PurchaseItemFactory(factory.django.DjangoModelFactory):
    purchase = factory.SubFactory(PurchaseFactory)
    product = factory.SubFactory(ProductFactory)

    class Meta:
        model = PurchaseItem

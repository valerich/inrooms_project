from autocomplete_light import shortcuts as autocomplete_light
from django import forms
from django_summernote.widgets import SummernoteWidget


class PurchaseItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'purchase': autocomplete_light.ChoiceWidget('PurchaseAutocomplete'),
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete')
        }


class PurchaseStatusAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'next_status': autocomplete_light.MultipleChoiceWidget('PurchaseStatusAutocomplete'),
        }


class PurchaseAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'creator': autocomplete_light.ChoiceWidget('UserAutocomplete'),
            'description': SummernoteWidget(),
            'payment_method': autocomplete_light.ChoiceWidget('PaymentMethodAutocomplete'),
            'performer': autocomplete_light.ChoiceWidget('UserAutocomplete'),
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
            'status': autocomplete_light.ChoiceWidget('PurchaseStatusAutocomplete'),
            'delivery_type': autocomplete_light.ChoiceWidget('DeliveryTypePurchaseDeliveryTypeAutocomplete'),
            'delivery_client': autocomplete_light.ChoiceWidget('ClientAutocomplete'),
            'delivery_warehouse_to': autocomplete_light.ChoiceWidget('WarehouseAutocomplete'),
        }

from django.db import models

from model_utils.choices import Choices
from model_utils.models import TimeStampedModel
from phonenumber_field.modelfields import PhoneNumberField

VAT_RATE_CHOICES = Choices(
    (0, '_0', 'Без НДС'),
    (18, '_18', '18%'),
)


class BuyerCard(TimeStampedModel):
    number = models.CharField('Номер', max_length=20, unique=True)
    client = models.ForeignKey('clients.Client', verbose_name='Партнер', on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Карта покупателя'
        verbose_name_plural = 'Карты покупателей'
        ordering = ('number', )

    def __str__(self):
        return 'Карта №:{}, client:{}'.format(self.number, self.client_id)


GENDER_CHOICE = Choices(
    ('f', 'female', 'Ж'),
    ('m', 'male', 'М'),
)


class BuyerProfile(TimeStampedModel):
    GENDER = GENDER_CHOICE
    first_name = models.CharField('Имя', max_length=100)
    middle_name = models.CharField('Отчество', max_length=100, blank=True)
    last_name = models.CharField('Фамилия', max_length=100)

    birthday = models.DateField('Дата рождения')
    gender = models.CharField('Пол', max_length=1, choices=GENDER, default=GENDER.female)
    phone = PhoneNumberField()
    email = models.EmailField('email', blank=True)
    subscription_sms = models.BooleanField('Подписка на рассылку', default=False)

    city = models.CharField('Город', max_length=100)
    profession = models.CharField('Профессия', max_length=100, blank=True)

    card = models.OneToOneField(BuyerCard, verbose_name='Карта покупателя')

    class Meta:
        verbose_name = 'Покупатель'
        verbose_name_plural = 'Покупатель'
        ordering = ('-id', )

    def __str__(self):
        return '{} {} {}'.format(self.first_name, self.middle_name, self.last_name)

    def get_phone_display(self):
        try:
            return self.phone.as_national
        except Exception:
            return str(self.phone)

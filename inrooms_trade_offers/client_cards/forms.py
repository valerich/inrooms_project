from autocomplete_light import shortcuts as autocomplete_light
from django import forms


class BuyerCardAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'client': autocomplete_light.ChoiceWidget('ClientPartnerAutocomplete'),
        }


class BuyerProfileAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'card': autocomplete_light.ChoiceWidget('BuyerCardAutocomplete'),
        }
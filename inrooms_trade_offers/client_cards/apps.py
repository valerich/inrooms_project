from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'client_cards'
    verbose_name = 'Скидосы'

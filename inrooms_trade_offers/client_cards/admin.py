from django.contrib import admin

from core.admin import NotDeleteModelAdmin
from .forms import BuyerCardAdminForm, BuyerProfileAdminForm
from .models import BuyerCard, BuyerProfile


class BuyerCardAdmin(NotDeleteModelAdmin):
    list_display = ('id', 'number', 'client', )
    search_fields = ('=id', 'number', )
    form = BuyerCardAdminForm
    list_select_related = ('client', )
    list_filter = ('client', )


class BuyerProfileAdmin(NotDeleteModelAdmin):
    list_display = ('id', 'first_name', 'middle_name', 'last_name')
    search_fields = ('=id', 'first_name', 'middle_name', 'last_name', )
    form = BuyerProfileAdminForm


for model_or_iterable, admin_class in (
    (BuyerCard, BuyerCardAdmin),
    (BuyerProfile, BuyerProfileAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

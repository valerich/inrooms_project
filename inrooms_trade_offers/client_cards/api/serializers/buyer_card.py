from rest_framework import serializers

from clients.api.serializers import ClientSerializer
from ...models import BuyerCard


class BuyerCardSerializer(serializers.ModelSerializer):
    client_detail = serializers.SerializerMethodField()
    cut_number = serializers.SerializerMethodField()

    class Meta:
        model = BuyerCard
        fields = (
            'id',
            'number',
            'cut_number',
            'client',
            'client_detail',
        )

    def get_client_detail(self, obj):
        return ClientSerializer(obj.client).data

    def get_cut_number(self, obj):
        len_number = len(obj.number)
        return '{}{}'.format('*' * (len_number - 4) if len_number > 4 else '', obj.number[-4:])

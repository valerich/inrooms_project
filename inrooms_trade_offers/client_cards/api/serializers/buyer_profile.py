from decimal import Decimal
from django.db import models
from django.db.models.aggregates import Sum
from django.db.models.expressions import F
from phonenumber_field.formfields import PhoneNumberField
from rest_framework import serializers

from ..serializers.buyer_card import BuyerCardSerializer
from ...models import BuyerProfile


class BuyerProfileSerializer(serializers.ModelSerializer):
    card_detail = serializers.SerializerMethodField()
    order_b2c_amount_sum = serializers.SerializerMethodField()
    phone = serializers.CharField(validators=PhoneNumberField().validators)
    phone_display = serializers.SerializerMethodField()

    class Meta:
        model = BuyerProfile
        fields = (
            'id',
            'first_name',
            'middle_name',
            'last_name',
            'birthday',
            'phone',
            'phone_display',
            'email',
            'subscription_sms',
            'city',
            'profession',
            'card',
            'card_detail',
            'gender',
            'order_b2c_amount_sum',
        )

    def get_card_detail(self, obj):
        return BuyerCardSerializer(obj.card).data

    def get_order_b2c_amount_sum(self, obj):
        from orders.models import OrderB2CItemSize
        amount_sum = OrderB2CItemSize.objects.filter(
            order_item__cheque_1c__buyer_card=obj.card
        ).aggregate(
            total_price=Sum(F('quantity') * F('order_item__price'), output_field=models.DecimalField())
        )['total_price']
        if amount_sum is None:
            amount_sum = Decimal('0.00')
        amount_sum = amount_sum.quantize(Decimal('0.00'))
        return str(amount_sum)

    def get_phone_display(self, obj):
        return obj.get_phone_display()

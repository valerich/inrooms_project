import django_filters
from django.db.models.query_utils import Q
from rest_framework import filters, status
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from phonenumber_field.phonenumber import to_python

from config.celery import app
from core.utils.views.xls import ExportXlsxViewMixin, ExportClassMixin
from sms.models import MessageTemplate
from sms.tasks import create_template_message_sms
from ..serializers import BuyerProfileSerializer
from ...models import BuyerProfile


class BuyerProfilesExport(ExportClassMixin):

    def get_xls_helper(self):
        xls_helper = super().get_xls_helper()
        xls_helper.add_page('Профили')

        xls_helper.write_cell('Номер карты', 0, 0)
        xls_helper.write_cell('Фамилия', 0, 1)
        xls_helper.write_cell('Имя', 0, 2)
        xls_helper.write_cell('Отчество', 0, 3)
        xls_helper.write_cell('Дата рождения', 0, 4)
        xls_helper.write_cell('Пол', 0, 5)
        xls_helper.write_cell('Телефон', 0, 6)
        xls_helper.write_cell('email', 0, 7)
        xls_helper.write_cell('Подписался на новости?', 0, 8)
        xls_helper.write_cell('Город', 0, 9)
        xls_helper.write_cell('Профессия', 0, 10)

        row = 1

        for item in self.data:
            xls_helper.write_cell(item['card_detail']['number'], row, 0)
            xls_helper.write_cell(item['last_name'], row, 1)
            xls_helper.write_cell(item['first_name'], row, 2)
            xls_helper.write_cell(item['middle_name'], row, 3)
            xls_helper.write_cell(item['birthday'], row, 4)
            xls_helper.write_cell(item['gender'], row, 5)
            xls_helper.write_cell(item['phone_display'], row, 6)
            xls_helper.write_cell(item['email'], row, 7)
            xls_helper.write_cell('да' if item['subscription_sms'] else 'нет', row, 8)
            xls_helper.write_cell(item['city'], row, 9)
            xls_helper.write_cell(item['profession'], row, 10)

            row += 1

        return xls_helper


def filter_phone(queryset, value):
    if value:
        phone_number = to_python(value)
        queryset = queryset.filter(phone=phone_number)
    return queryset


class BuyerProfileFilterSet(django_filters.FilterSet):
    phone = django_filters.CharFilter(action=filter_phone)

    class Meta:
        model = BuyerProfile
        fields = [
            'card',
            'phone',
            'card__client',
        ]


class BuyerProfileViewSet(ExportXlsxViewMixin, ModelViewSet):
    model = BuyerProfile
    serializer_class = BuyerProfileSerializer
    filter_class = BuyerProfileFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', 'first_name', 'last_name', 'middle_name', 'card__number')
    ordering_fields = (
        'id',
        'first_name',
        'last_name',
        'middle_name',
    )
    queryset = BuyerProfile.objects.all().select_related('card')
    filename_xlsx = 'buyer_profiles.xlsx'
    export_class = BuyerProfilesExport

    def get_queryset(self):
        qs = super().get_queryset()
        if not self.request.user.has_perm('auth.can_view_all_buyer_profiles'):
            if self.request.user.has_perm('auth.can_view_buyer_profiles'):
                q_objects = Q()
                if self.request.user.client:
                    q_objects.add(Q(card__client=self.request.user.client), Q.OR)
                if self.request.user.clients.all():
                    q_objects.add(Q(card__client__in=self.request.user.clients.all()), Q.OR)
                if q_objects:
                    qs = qs.filter(q_objects)
                else:
                    qs = qs.none()
            else:
                qs = qs.none()
        return qs

    @detail_route(methods=['post'])
    def send_card_number_to_sms(self, request, pk=None):
        obj = self.get_object()

        template = MessageTemplate.objects.filter(
            title='Отправка номера карты клиенту',
        )

        try:
            template = template[0]
        except IndexError:
            return Response({'error': 'Не найден шаблон сообщения! Сообщите о проблеме администратору'},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            create_template_message_sms(template.id, obj.phone, {'buyer_profile': obj})
            app.send_task('sms.send_messages_sms')
        return Response()

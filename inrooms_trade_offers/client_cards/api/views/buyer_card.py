import django_filters
from django.db.models.query_utils import Q
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from ..serializers import BuyerCardSerializer
from ...models import BuyerCard


def filter_has_buyer_profile(queryset, value):
    if value:
        queryset = queryset.filter(buyerprofile__isnull=False)
    else:
        queryset = queryset.filter(buyerprofile__isnull=True)
    return queryset


class BuyerCardFilterSet(django_filters.FilterSet):
    has_buyer_profile = django_filters.BooleanFilter(action=filter_has_buyer_profile)

    class Meta:
        model = BuyerCard
        fields = [
            'client',
        ]


class BuyerCardViewSet(ModelViewSet):
    model = BuyerCard
    serializer_class = BuyerCardSerializer
    filter_class = BuyerCardFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('number', )
    ordering_fields = (
        'id',
        'number',
    )
    queryset = BuyerCard.objects.all()

    def get_queryset(self):
        qs = super().get_queryset()
        if not self.request.user.has_perm('auth.can_view_all_buyer_cards'):
            if self.request.user.has_perm('auth.can_view_buyer_cards'):
                q_objects = Q()
                if self.request.user.client:
                    q_objects.add(Q(client=self.request.user.client), Q.OR)
                if self.request.user.clients.all():
                    q_objects.add(Q(client__in=self.request.user.clients.all()), Q.OR)
                if q_objects:
                    qs = qs.filter(q_objects)
                else:
                    qs = qs.none()
            else:
                qs = qs.none()
        return qs

from django.conf.urls import include, url
from rest_framework import routers

from .views import BuyerCardViewSet, BuyerProfileViewSet

router = routers.SimpleRouter()

router.register(r'buyer_card', BuyerCardViewSet, base_name='buyer_card')
router.register(r'buyer_profile', BuyerProfileViewSet, base_name='buyer_profile')


urlpatterns = [
    url(r'^', include(router.urls)),
]

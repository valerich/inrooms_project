from autocomplete_light import shortcuts as autocomplete_light

from .models import BuyerCard


class BuyerCardAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['number', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (BuyerCard, BuyerCardAutocomplete),
)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

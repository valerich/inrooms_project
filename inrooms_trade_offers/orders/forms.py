from autocomplete_light import shortcuts as autocomplete_light
from django import forms


class OrderItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'order': autocomplete_light.ChoiceWidget('OrderAutocomplete'),
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete')
        }


class OrderB2CItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'order': autocomplete_light.ChoiceWidget('OrderB2CAutocomplete'),
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete')
        }


class OrderReturnItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'order': autocomplete_light.ChoiceWidget('OrderReturnAutocomplete'),
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete')
        }


class OrderReturnB2CItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'order': autocomplete_light.ChoiceWidget('OrderReturnB2CAutocomplete'),
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete')
        }


class OrderItemSizeAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'size': autocomplete_light.ChoiceWidget('SizeAutocomplete'),
            'warehouse': autocomplete_light.ChoiceWidget('WarehouseAutocomplete'),
        }


class OrderB2CItemSizeAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'size': autocomplete_light.ChoiceWidget('SizeAutocomplete'),
        }


class OrderReturnItemSizeAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'size': autocomplete_light.ChoiceWidget('SizeAutocomplete'),
        }


class OrderReturnB2CItemSizeAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'size': autocomplete_light.ChoiceWidget('SizeAutocomplete'),
        }


class OrderStatusAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'next_status': autocomplete_light.MultipleChoiceWidget('OrderStatusAutocomplete'),
        }


class DefectActAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'client': autocomplete_light.ChoiceWidget('ClientPartnerAutocomplete'),
            'status': autocomplete_light.ChoiceWidget('DefectActStatusAutocomplete'),
        }


class DefectActItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'size': autocomplete_light.ChoiceWidget('SizeAutocomplete'),
        }


class DefectActStatusAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'next_status': autocomplete_light.MultipleChoiceWidget('DefectActStatusAutocomplete'),
        }


class RefundActAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'client': autocomplete_light.ChoiceWidget('ClientPartnerAutocomplete'),
            'status': autocomplete_light.ChoiceWidget('RefundActStatusAutocomplete'),
        }


class RefundActItemAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'size': autocomplete_light.ChoiceWidget('SizeAutocomplete'),
        }


class RefundActStatusAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'next_status': autocomplete_light.MultipleChoiceWidget('RefundActStatusAutocomplete'),
        }


class OrderAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'client': autocomplete_light.ChoiceWidget('ClientPartnerAutocomplete'),
            'creator': autocomplete_light.ChoiceWidget('UserAutocomplete'),
            'status': autocomplete_light.ChoiceWidget('OrderStatusAutocomplete'),
        }


class OrderB2CAdminForm(forms.ModelForm):

    class Meta:
         widgets = {
            'client': autocomplete_light.ChoiceWidget('ClientPartnerAutocomplete'),
        }


class OrderReturnAdminForm(forms.ModelForm):

    class Meta:
         widgets = {
            'client': autocomplete_light.ChoiceWidget('ClientPartnerAutocomplete'),
        }


class OrderReturnB2CAdminForm(forms.ModelForm):

    class Meta:
         widgets = {
            'client': autocomplete_light.ChoiceWidget('ClientPartnerAutocomplete'),
        }


class ScannerDataAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'products': autocomplete_light.MultipleChoiceWidget('ProductAutocomplete')
        }


class ChequeAdminForm(forms.ModelForm):

    class Meta:
        widgets = {
            'client': autocomplete_light.ChoiceWidget('ClientAutocomplete'),
            'seller': autocomplete_light.ChoiceWidget('EmployeeAutocomplete'),
            'buyer_card': autocomplete_light.ChoiceWidget('BuyerCardAutocomplete'),
        }
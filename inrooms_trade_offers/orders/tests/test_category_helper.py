from django.db.models.aggregates import Sum
from django.test import TestCase

from core.server_init import ServerInit
from core.utils.constants import ORDER_CATEGORY_CHOICE
from products.factories import ProductFactory
from products.models import Size
from warehouses.models import Warehouse
from ..factories import OrderFactory, OrderItemFactory, OrderItemSizeFactory
from ..helpers import CategoryHelper
from ..models import OrderItemSize


class CategoryHelperTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(CategoryHelperTestCase, cls).setUpClass()

        si = ServerInit()
        si.create_product_sizes()
        si.create_warehouses()

        cls.warehouse_data = {warehouse.code: warehouse for warehouse in Warehouse.objects.all()}
        cls.size_data = {size.code: size for size in Size.objects.all()}

    def setUp(self):
        super().setUp()
        self.product1 = ProductFactory()
        self.product2 = ProductFactory()

        self.order = OrderFactory()
        self.order2 = OrderFactory()
        self.order_item1 = OrderItemFactory(order=self.order, product=self.product1, price=10)
        self.order_item2 = OrderItemFactory(order=self.order, product=self.product2, price=20)
        self.order_item3 = OrderItemFactory(order=self.order2, product=self.product1, price=10)
        self.order_item4 = OrderItemFactory(order=self.order2, product=self.product2, price=20)

        for category in ORDER_CATEGORY_CHOICE._db_values:
            for i in range(1, 5):
                OrderItemSizeFactory(
                    order_item=getattr(self, 'order_item{}'.format(i)),
                    size=self.size_data['m'],
                    warehouse=self.warehouse_data['msk'],
                    category=category,
                    quantity=10
                )
                OrderItemSizeFactory(
                    order_item=getattr(self, 'order_item{}'.format(i)),
                    size=self.size_data['m'],
                    warehouse=self.warehouse_data['chn'],
                    category=category,
                    quantity=10
                )
                OrderItemSizeFactory(
                    order_item=getattr(self, 'order_item{}'.format(i)),
                    size=self.size_data['s'],
                    warehouse=self.warehouse_data['msk'],
                    category=category,
                    quantity=10
                )

    def test_set_category_to_all_items(self):
        old_order_quantity = OrderItemSize.objects.filter(order_item__order=self.order).aggregate(Sum('quantity'))['quantity__sum']
        helper = CategoryHelper(order_id=self.order.id)
        helper._set_category_to_all_items(ORDER_CATEGORY_CHOICE.white)

        self.assertFalse(OrderItemSize.objects.filter(order_item__order=self.order,
                                                      category=ORDER_CATEGORY_CHOICE.black).exists())
        new_order_quantity = OrderItemSize.objects.filter(order_item__order=self.order).aggregate(Sum('quantity'))['quantity__sum']
        self.assertEqual(old_order_quantity, new_order_quantity)

    def test_get_category_sum_distribution(self):
        old_order_sum = 0
        for order_item_data in OrderItemSize.objects.filter(
                    order_item__order=self.order2,
                ).values('order_item__price').annotate(Sum('quantity')):
            if order_item_data['order_item__price'] and order_item_data['quantity__sum']:
                old_order_sum += order_item_data['order_item__price'] * order_item_data['quantity__sum']
        self.assertEqual(old_order_sum, 1800)

        old_order_category_sum = 0
        for order_item_data in OrderItemSize.objects.filter(
                    order_item__order=self.order2,
                    category=ORDER_CATEGORY_CHOICE.white,
                ).values('order_item__price').annotate(Sum('quantity')):
            if order_item_data['order_item__price'] and order_item_data['quantity__sum']:
                old_order_category_sum += order_item_data['order_item__price'] * order_item_data['quantity__sum']
        self.assertEqual(old_order_category_sum, 900)

        helper = CategoryHelper(order_id=self.order2.id)
        helper.category_sum_distribution(ORDER_CATEGORY_CHOICE.white, 30)

        new_order_sum = 0
        for order_item_data in OrderItemSize.objects.filter(
                    order_item__order=self.order2
                ).values('order_item__price').annotate(Sum('quantity')):
            if order_item_data['order_item__price'] and order_item_data['quantity__sum']:
                new_order_sum += order_item_data['order_item__price'] * order_item_data['quantity__sum']

        new_order_category_sum = 0
        for order_item_data in OrderItemSize.objects.filter(
                    order_item__order=self.order2,
                    category=ORDER_CATEGORY_CHOICE.white,
                ).values('order_item__price').annotate(Sum('quantity')):
            if order_item_data['order_item__price'] and order_item_data['quantity__sum']:
                new_order_category_sum += order_item_data['order_item__price'] * order_item_data['quantity__sum']

        # Проверяем, что общая сумма заказа не изменилась
        self.assertEqual(old_order_sum, new_order_sum)

        # И что данные распределились
        self.assertTrue(new_order_category_sum <= 30)

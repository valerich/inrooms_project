from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from clients.factories import ClientFactory
from core.utils.constants import ORDER_CATEGORY_CHOICE
from orders.factories import OrderItemSizeFactory
from products.factories import ProductFactory, RemainFactory
from products.models import Size
from warehouses.models import Warehouse
from ..factories import OrderFactory, OrderItemFactory
from ..models import OrderItemSize


class OrderChageStatusViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(OrderChageStatusViewTestCase, cls).setUpClass()
        UsersSetup()

        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')

        cls.warehouses = {w.code: w for w in Warehouse.objects.all()}
        cls.sizes = {s.code: s for s in Size.objects.all()}

        cls.order_client = ClientFactory()
        cls.product = ProductFactory(article='1111/111', name='test_product')
        cls.order = OrderFactory(client=cls.order_client, creator=cls.user_bm)
        cls.order_item = OrderItemFactory(order=cls.order, product=cls.product)
        OrderFactory(client=cls.order_client, creator=cls.user_pa)

        cls.change_status_url = reverse('api:orders:order-change-status', args=[cls.order.id, ])

        cls.valid_create_order_data = {
            'client': cls.order_client.id,
        }

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_change_status(self):
        """Изменение статуса заказа. У заказа нет резервов"""
        response = self.client_admin.post(self.change_status_url, {'code': 'reserved'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_fail_reserves(self):
        """Изменение статуса заказа. У заказа резервы не соответствуют остаткам"""
        order_item_size = OrderItemSizeFactory(order_item=self.order_item,
                                               warehouse=self.warehouses['msk'],
                                               size=self.sizes['s'],
                                               quantity=1)
        response = self.client_admin.post(self.change_status_url, {'code': 'reserved'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {
                'error': 'Проблемы с резервами',
                'error_code': 'reserve_fail',
                'data': [
                    {
                        'product': {'article': '1111/111', 'id': self.product.id, 'name': 'test_product'},
                        'warehouse': {'code': 'msk', 'id': self.warehouses['msk'].id, 'name': 'Склад Москвы'},
                        'size': {'code': 's', 'id': self.sizes['s'].id, 'name': 's'},
                        'remain': 0,
                        'reserve': 1,
                    }
                ],
            }
        )

    def test_fail_reserves_re_reserve_confirmed(self):
        """Изменение статуса заказа. У заказа резервы не соответствуют остаткам,
        но передан флаг проведения перерезерва "re_reserve_confirmed"

        В заказе один товар, остатков под него нет
        """

        order_item_size = OrderItemSizeFactory(order_item=self.order_item,
                                               warehouse=self.warehouses['msk'],
                                               size=self.sizes['s'],
                                               category=ORDER_CATEGORY_CHOICE.white,
                                               quantity=1)
        response = self.client_admin.post(self.change_status_url, {'code': 'reserved', 're_reserve_confirmed': True})
        # статус поменялся
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Резервов нет. Т.к. не было остатков
        self.assertEqual(OrderItemSize.objects.filter(order_item__order=self.order).count(), 0)


    def test_fail_reserves_re_reserve_confirmed2(self):
        """Изменение статуса заказа. У заказа резервы не соответствуют остаткам,
        но передан флаг проведения перерезерва "re_reserve_confirmed"

        В заказе один товар c разными категориями продаж.
        """

        order_item_size_white = OrderItemSizeFactory(
            order_item=self.order_item,
            warehouse=self.warehouses['msk'],
            size=self.sizes['s'],
            category=ORDER_CATEGORY_CHOICE.white,
            quantity=5)
        order_item_size_black = OrderItemSizeFactory(
            order_item=self.order_item,
            warehouse=self.warehouses['msk'],
            size=self.sizes['s'],
            category=ORDER_CATEGORY_CHOICE.black,
            quantity=5)
        RemainFactory(product=self.product,
                      warehouse=self.warehouses['msk'],
                      size=self.sizes['s'],
                      value=7)
        # Резервы на категорию "черная" остались
        self.assertEqual(OrderItemSize.objects.get(id=order_item_size_black.id).quantity, 5)
        # Резервы на категорию "белая" уменьшились т.к. на них остатков не хватило
        self.assertEqual(OrderItemSize.objects.get(id=order_item_size_white.id).quantity, 5)
        response = self.client_admin.post(self.change_status_url, {'code': 'reserved', 're_reserve_confirmed': True})
        # статус поменялся
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Резервы на категорию "черная" остались
        self.assertEqual(OrderItemSize.objects.get(id=order_item_size_black.id).quantity, 5)
        # Резервы на категорию "белая" уменьшились т.к. на них остатков не хватило
        self.assertEqual(OrderItemSize.objects.get(id=order_item_size_white.id).quantity, 2)


    def test_reserves_ok(self):
        """Изменение статуса заказа. У заказа резервы соответствуют остаткам"""
        RemainFactory(product=self.product,
                      warehouse=self.warehouses['msk'],
                      size=self.sizes['s'],
                      value=1)

        order_item_size = OrderItemSizeFactory(order_item=self.order_item,
                                               warehouse=self.warehouses['msk'],
                                               size=self.sizes['s'],
                                               quantity=1)
        response = self.client_admin.post(self.change_status_url, {'code': 'reserved'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

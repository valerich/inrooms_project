from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.tests.setup import UsersSetup
from clients.factories import ClientFactory
from orders.factories import OrderFactory, OrderItemFactory
from orders.models import OrderStatus
from products.factories import ProductColorFactory, ProductFactory, RemainFactory
from products.models import Size
from warehouses.models import Warehouse


class ProductSearchViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        UsersSetup()
        cls.msk_warehouse = Warehouse.objects.get(code='msk')
        cls.s_size = Size.objects.get(code='s')

        cls.order = OrderFactory()

        cls.api_url = reverse('api:orders:product_search-list', kwargs={'order_pk': cls.order.id})

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_list(self):
        response = self.client_bm.get(self.api_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client_pa.get(self.api_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_item_fields(self):
        color = ProductColorFactory(name='тестовый цвет')
        product = ProductFactory(
            is_active=True,
            article='1111/222',
            name='product1_name',
            price=1000,
            color=color,
        )
        RemainFactory(product=product, warehouse=self.msk_warehouse, size=self.s_size, value=10)

        response = self.client_bm.get(self.api_url)
        self.assertEqual(response.data['count'], 1)

        object_as_dict = response.data['results'][0]
        valid_list_item_dict = {
            'id': product.id,
            'article': '1111/222',
            'name': 'product1_name',
            'image': None,
            'color': color.id,
            'color_detail': {
                'id': color.id,
                'name': 'тестовый цвет'
            },
            'price': '1000.00',
            'remains': 10,
        }
        self.assertDictEqual(object_as_dict, valid_list_item_dict)

    def test_is_active(self):
        """Список товаров не должен зависеть от флага is_active

        В первой версии api должны были отдаваться только товары is_active=True
        В задаче Issue #173 эта логика поменялась
        """

        product_1 = ProductFactory(is_active=True)
        product_2 = ProductFactory(is_active=False)

        RemainFactory(product=product_1, warehouse=self.msk_warehouse, size=self.s_size, value=10)
        RemainFactory(product=product_2, warehouse=self.msk_warehouse, size=self.s_size, value=10)

        response = self.client_bm.get(self.api_url)
        self.assertEqual(response.data['count'], 2)

        response_product_ids = [item['id'] for item in response.data['results']]
        self.assertSetEqual(set(response_product_ids), {product_1.id, product_2.id})

    def test_client_not_order_filter(self):
        """Фильтр "остатки без отгрузки"

        Смысл в том, что нужно отфильтровать те товары, которые клиент никогда не покупал
        https://bitbucket.org/valerich/inrooms_project/issues/182/--------------------
        """

        # Подготовка данных для теста
        product_1 = ProductFactory()
        product_2 = ProductFactory()
        product_3 = ProductFactory()
        RemainFactory(product=product_1, warehouse=self.msk_warehouse, size=self.s_size, value=10)
        RemainFactory(product=product_2, warehouse=self.msk_warehouse, size=self.s_size, value=10)
        RemainFactory(product=product_3, warehouse=self.msk_warehouse, size=self.s_size, value=10)

        client = ClientFactory()
        order = OrderFactory(client=client)

        api_url = reverse('api:orders:product_search-list', kwargs={'order_pk': order.id})

        # Создаем старые заказы на клиента
        # Один в статусе "Новый". Товары в таком заказе участвуют в фильтрации, если значение фильтра заканчивается на +
        old_order = OrderFactory(client=client, status=OrderStatus.objects.get(code='new'))
        OrderItemFactory(order=old_order, product=product_2)
        # Один в статусе не "Новый". Товары в таком заказе участвуют в фильтрации всегда
        old_order = OrderFactory(client=client, status=OrderStatus.objects.get(code='reserved'))
        OrderItemFactory(order=old_order, product=product_1)

        # Без фильтра api отдает все товары
        response = self.client_bm.get(api_url)
        self.assertEqual(response.data['count'], 3)

        # Фильтруем товары, которые не участвовали в предыдущих заказах (кроме статуса new)
        response = self.client_bm.get(api_url, data={'client_not_order': '{}'.format(client.id)})
        self.assertEqual(response.data['count'], 2)
        self.assertSetEqual(set([item['id'] for item in response.data['results']]),
                            set([product_2.id, product_3.id]))

        # Фильтруем товары, которые участвовали в предыдущих заказах (кроме статуса new)
        response = self.client_bm.get(api_url, data={'client_not_order': '-{}'.format(client.id)})
        self.assertEqual(response.data['count'], 1)
        self.assertSetEqual(set([item['id'] for item in response.data['results']]),
                            set([product_1.id, ]))

        # Фильтруем товары, которые не участвовали в предыдущих заказах
        response = self.client_bm.get(api_url, data={'client_not_order': '{}+'.format(client.id)})
        self.assertEqual(response.data['count'], 1)
        self.assertSetEqual(set([item['id'] for item in response.data['results']]),
                            set([product_3.id, ]))

        # Фильтруем товары, которые участвовали в предыдущих заказах
        response = self.client_bm.get(api_url, data={'client_not_order': '-{}+'.format(client.id)})
        self.assertEqual(response.data['count'], 2)
        self.assertSetEqual(set([item['id'] for item in response.data['results']]),
                            set([product_1.id, product_2.id]))

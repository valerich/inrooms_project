from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from clients.factories import ClientFactory
from orders.factories import OrderFactory, OrderItemFactory, ScannerDataFactory
from orders.models import OrderItem, OrderItemSize
from products.factories import ProductFactory, RemainFactory
from products.models import Remain, Size
from warehouses.models import Warehouse


class OrderItemViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(OrderItemViewTestCase, cls).setUpClass()

        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')
        cls.client = ClientFactory()
        cls.product = ProductFactory()

        cls.order_bm = OrderFactory(client=cls.client, creator=cls.user_bm)
        cls.order_pa = OrderFactory(client=cls.client, creator=cls.user_pa)
        OrderItemFactory(order=cls.order_bm, product=cls.product)
        OrderItemFactory(order=cls.order_pa, product=cls.product)

        cls.order_item_bm_url = reverse('api:orders:order_items-list', kwargs={'order_pk': cls.order_bm.id})
        cls.order_item_pa_url = reverse('api:orders:order_items-list', kwargs={'order_pk': cls.order_pa.id})

        new_product = ProductFactory()
        cls.valid_create_order_item_bm_data = {
            'product': new_product.id
        }
        cls.valid_create_order_item_pa_data = {
            'product': new_product.id
        }

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_blank_request_data(self):
        response = self.client_bm.post(self.order_item_bm_url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_order_item(self):
        response = self.client_bm.post(self.order_item_bm_url, self.valid_create_order_item_bm_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client_pa.post(self.order_item_pa_url, self.valid_create_order_item_pa_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_series_count(self):
        """Тип серии при добавлении товара в отгрузку клиенту

        Issue #171
        Если в наличии только один размер M, то количество устанавливаемое для одной серии равно 6 на один размер.
        Если в наличии есть только размеры S и M, то устанавливаемое количество равно 3 для каждого размера.
        Если в наличии есть больше чем S и М размеры, то работает стандартная логика подбора серий 1122111.
        """
        new_product = ProductFactory()
        warehouse = Warehouse.objects.get(code='msk')
        size_s = Size.objects.get(code='s')
        size_m = Size.objects.get(code='m')
        size_l = Size.objects.get(code='l')

        data = {
            'product': new_product.id,
            'series_count': 1,
        }

        # Если в наличии только один размер M, то количество устанавливаемое для одной серии равно 6 на один размер.

        RemainFactory(product=new_product,
                      warehouse=warehouse,
                      size=size_m,
                      value=100)
        response = self.client_bm.post(self.order_item_bm_url, data)
        order_item = OrderItem.objects.get(id=response.data['id'])
        self.assertEqual(OrderItemSize.objects.filter(order_item=order_item).count(), 1)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_m).quantity, 6)
        order_item.delete()

        # Если в наличии есть только размеры S и M, то устанавливаемое количество равно 3 для каждого размера.

        RemainFactory(product=new_product,
                      warehouse=warehouse,
                      size=size_s,
                      value=100)
        response = self.client_bm.post(self.order_item_bm_url, data)
        order_item = OrderItem.objects.get(id=response.data['id'])
        self.assertEqual(OrderItemSize.objects.filter(order_item=order_item).count(), 2)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_s).quantity, 3)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_m).quantity, 3)
        order_item.delete()

        # Если в наличии есть больше чем S и М размеры, то работает стандартная логика подбора серий 1122111.

        RemainFactory(product=new_product,
                      warehouse=warehouse,
                      size=size_l,
                      value=100)
        response = self.client_bm.post(self.order_item_bm_url, data)
        order_item = OrderItem.objects.get(id=response.data['id'])
        self.assertEqual(OrderItemSize.objects.filter(order_item=order_item).count(), 3)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_s).quantity, 2)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_m).quantity, 2)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_l).quantity, 1)
        order_item.delete()

        # На остатках один размер, но не m

        Remain.objects.all().delete()

        RemainFactory(product=new_product,
                      warehouse=warehouse,
                      size=size_l,
                      value=100)
        response = self.client_bm.post(self.order_item_bm_url, data)
        order_item = OrderItem.objects.get(id=response.data['id'])
        self.assertEqual(OrderItemSize.objects.filter(order_item=order_item).count(), 1)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_l).quantity, 1)

    def test_create_on_scanner_data(self):
        """Добавление товаров в заказ на основании данных сканера штрих кодов

        Issue #171
        Если в наличии только один размер M, то количество устанавливаемое для одной серии равно 6 на один размер.
        Если в наличии есть только размеры S и M, то устанавливаемое количество равно 3 для каждого размера.
        Если в наличии есть больше чем S и М размеры, то работает стандартная логика подбора серий 1122111.
        """

        api_url = reverse('api:orders:order_items-create-on-scanner-data', kwargs={'order_pk': self.order_bm.id})

        new_product = ProductFactory()
        product = ProductFactory()
        warehouse = Warehouse.objects.get(code='msk')
        size_s = Size.objects.get(code='s')
        size_m = Size.objects.get(code='m')
        size_l = Size.objects.get(code='l')

        scanner_data = ScannerDataFactory(is_ordered=False)
        scanner_data.products.add(new_product)

        data = {
            'scanner_data_id': scanner_data.id,
            'series_count': 1,
        }

        # Если в наличии только один размер M, то количество устанавливаемое для одной серии равно 6 на один размер.

        RemainFactory(product=new_product,
                      warehouse=warehouse,
                      size=size_m,
                      value=100)
        response = self.client_bm.post(api_url, data)
        order_item = OrderItem.objects.get(order=self.order_bm, product=new_product)
        self.assertEqual(OrderItemSize.objects.filter(order_item=order_item).count(), 1)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_m).quantity, 6)
        order_item.delete()

        # Если в наличии есть только размеры S и M, то устанавливаемое количество равно 3 для каждого размера.

        RemainFactory(product=new_product,
                      warehouse=warehouse,
                      size=size_s,
                      value=100)
        response = self.client_bm.post(api_url, data)
        order_item = OrderItem.objects.get(order=self.order_bm, product=new_product)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_s).quantity, 3)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_m).quantity, 3)
        order_item.delete()

        # Если в наличии есть больше чем S и М размеры, то работает стандартная логика подбора серий 1122111.

        RemainFactory(product=new_product,
                      warehouse=warehouse,
                      size=size_l,
                      value=100)
        response = self.client_bm.post(api_url, data)
        order_item = OrderItem.objects.get(order=self.order_bm, product=new_product)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_s).quantity, 2)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_m).quantity, 2)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_l).quantity, 1)
        order_item.delete()

        # На остатках один размер, но не m

        Remain.objects.all().delete()

        RemainFactory(product=new_product,
                      warehouse=warehouse,
                      size=size_l,
                      value=100)
        response = self.client_bm.post(api_url, data)
        order_item = OrderItem.objects.get(order=self.order_bm, product=new_product)
        self.assertEqual(OrderItemSize.objects.get(order_item=order_item, size=size_l).quantity, 1)

    def test_list(self):
        response = self.client_bm.get(self.order_item_bm_url)
        self.assertEqual(response.data['count'], 1)
        response = self.client_pa.get(self.order_item_pa_url)
        self.assertEqual(response.data['count'], 1)

    # def test_list_fields(self):
    #     """Проверяем, что api возвращает все поля, необходимые для отображения списка"""
    #
    #     response = self.client_admin.get(self.order_url)
    #     order_dict = response.data['results'][0]
    #     fields = [
    #         'id',
    #         'amount_full',
    #         'created',
    #         'modified',
    #         'client_detail.name',
    #         'series_count',
    #         # 'items',
    #     ]
    #     for field in fields:
    #         try:
    #             field_container = order_dict
    #             for i in field.split('.'):
    #                 field_container = field_container[i]
    #         except (KeyError, TypeError):
    #             self.fail('Не передается поле {} в {}'.format(field, self.order_url))
    #
    # def test_list_sort_by_fields(self):
    #     """Проверяем, что api сортируется по всем необходимым полям"""
    #
    #     fields = [
    #         'id',
    #         'client__name',
    #         'created',
    #         'modified',
    #     ]
    #     for field in fields:
    #         for prefix in ['', '-']:
    #             filter_field = "{}{}".format(prefix, field)
    #             response = self.client_admin.get(self.order_url, {'sort_by': filter_field})
    #             order_dict = response.data['results'][0]
    #             order = Order.objects.all().order_by(filter_field)[0]
    #             self.assertEqual(order_dict['id'], getattr(order, 'id'), 'Не сортируется по полю {} в {}'.format(filter_field, self.order_url))

from django.core.urlresolvers import reverse
from freezegun.api import freeze_time
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.tests.setup import UsersSetup
from ..factories import ScannerDataFactory


@freeze_time("2016-1-31")
class ScannerDaraViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        UsersSetup()

        cls.scanner_data_not_ordered = ScannerDataFactory(is_ordered=False)
        cls.scanner_data_ordered = ScannerDataFactory(is_ordered=True)

        cls.api_url = reverse('api:orders:scanner_data-list')

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_list(self):
        response = self.client_bm.get(self.api_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client_pa.get(self.api_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_item_fields(self):
        response = self.client_bm.get(self.api_url)
        self.assertEqual(response.data['count'], 1)

        object_as_dict = response.data['results'][0]
        valid_list_item_dict = {
            'id': self.scanner_data_not_ordered.id,
            'created': '2016-01-31T00:00:00Z',
            'modified': '2016-01-31T00:00:00Z',
            'products_count': 0,
        }
        self.assertDictEqual(object_as_dict, valid_list_item_dict)

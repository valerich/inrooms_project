from django.test import TestCase

from orders.factories import OrderFactory
from orders.models import OrderItem
from products.factories import ProductCollectionFactory, ProductFactory


class OrderItemTestCase(TestCase):

    def test_series_type(self):
        """При создании OrderItem, тип серии должен либо явно указываться, либо браться из товара"""
        order = OrderFactory()
        c_collection = ProductCollectionFactory(series_type=OrderItem.SERIES_TYPE.clothes)
        s_collection = ProductCollectionFactory(series_type=OrderItem.SERIES_TYPE.shoes)
        c_product = ProductFactory(main_collection=c_collection)
        s_product = ProductFactory(main_collection=s_collection)

        order_item = OrderItem.objects.create(order=order, product=c_product, series_type=OrderItem.SERIES_TYPE.shoes)
        self.assertEqual(order_item.series_type, OrderItem.SERIES_TYPE.shoes)
        order_item = OrderItem.objects.create(order=order, product=s_product, series_type=OrderItem.SERIES_TYPE.clothes)
        self.assertEqual(order_item.series_type, OrderItem.SERIES_TYPE.clothes)

        OrderItem.objects.all().delete()

        order_item = OrderItem.objects.create(order=order, product=c_product)
        self.assertEqual(order_item.series_type, OrderItem.SERIES_TYPE.clothes)
        order_item = OrderItem.objects.create(order=order, product=s_product)
        self.assertEqual(order_item.series_type, OrderItem.SERIES_TYPE.shoes)

    def test_price(self):
        """При создании OrderItem, цена должна либо явно указываться, либо браться из товара"""
        order = OrderFactory()
        product = ProductFactory(price=100)

        order_item = OrderItem.objects.create(order=order, product=product, price=200)
        self.assertEqual(order_item.price, 200)

        OrderItem.objects.all().delete()

        order_item = OrderItem.objects.create(order=order, product=product)
        self.assertEqual(order_item.price, 100)

    def test_price_is_not_none(self):
        """Надо проставлять товару хоть какую-то цену"""
        order = OrderFactory()
        product = ProductFactory(price=None)

        order_item = OrderItem.objects.create(order=order, product=product, price=200)
        self.assertEqual(order_item.price, 200)

        OrderItem.objects.all().delete()

        order_item = OrderItem.objects.create(order=order, product=product)
        self.assertEqual(order_item.price, 0)

    def test_ordering(self):
        order = OrderFactory()
        product1 = ProductFactory(article='2222/222')
        product2 = ProductFactory(article='1111/111')
        product3 = ProductFactory(article='3333/333')

        order_item1 = OrderItem.objects.create(order=order, product=product1)
        order_item2 = OrderItem.objects.create(order=order, product=product2)
        order_item3 = OrderItem.objects.create(order=order, product=product3)

        # С одинаковым sort_index сортируем по -id
        self.assertEqual(order.items.all()[0].id, order_item3.id)
        self.assertEqual(order.items.all()[2].id, order_item1.id)

        order.sort_items()

        # При отличающихся sort_index сортируем по ним
        self.assertEqual(order.items.all()[0].id, order_item2.id)
        self.assertEqual(order.items.all()[2].id, order_item3.id)

from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.models import User
from accounts.tests.setup import UsersSetup
from clients.factories import ClientFactory
from orders.factories import OrderFactory
from products.factories import ProductFactory
from ..models import Order, OrderStatus


class OrderViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(OrderViewTestCase, cls).setUpClass()
        cls.order_url = reverse('api:orders:order-list')

        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')
        cls.order_client = ClientFactory()
        cls.product = ProductFactory()
        cls.order = OrderFactory(client=cls.order_client, creator=cls.user_bm)
        OrderFactory(client=cls.order_client, creator=cls.user_pa)

        cls.valid_create_order_data = {
            'client': cls.order_client.id,
        }

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_blank_request_data(self):
        response = self.client_bm.post(self.order_url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_order(self):
        response = self.client_bm.post(self.order_url, self.valid_create_order_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client_pa.post(self.order_url, self.valid_create_order_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_list_order(self):
        response = self.client_bm.get(self.order_url)
        self.assertEqual(response.data['count'], 0)
        response = self.client_pa.get(self.order_url)
        self.assertEqual(response.data['count'], 2)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client_admin.get(self.order_url)
        order_dict = response.data['results'][0]
        fields = [
            'id',
            'amount_full',
            'created',
            'modified',
            'client_detail.name',
            'series_count',
            # 'items',
        ]
        for field in fields:
            try:
                field_container = order_dict
                for i in field.split('.'):
                    field_container = field_container[i]
            except (KeyError, TypeError):
                self.fail('Не передается поле {} в {}'.format(field, self.order_url))

    def test_list_sort_by_fields(self):
        """Проверяем, что api сортируется по всем необходимым полям"""

        fields = [
            'id',
            'client__name',
            'created',
            'modified',
        ]
        for field in fields:
            for prefix in ['', '-']:
                filter_field = "{}{}".format(prefix, field)
                response = self.client_admin.get(self.order_url, {'sort_by': filter_field})
                order_dict = response.data['results'][0]
                order = Order.objects.all().order_by(filter_field)[0]
                self.assertEqual(order_dict['id'], getattr(order, 'id'), 'Не сортируется по полю {} в {}'.format(filter_field, self.order_url))

    def test_delete(self):
        # Заказы в статусе "Новый" должны удаляться
        new_status = OrderStatus.objects.get(code='new')
        order = OrderFactory(client=self.order_client, creator=self.user_pa, status=new_status)
        order_url = reverse('api:orders:order-detail', args=[order.id])
        response = self.client_admin.delete(order_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # В остальных статусах удаляться не должны
        reserved_status = OrderStatus.objects.get(code='reserved')
        order = OrderFactory(client=self.order_client, creator=self.user_pa, status=reserved_status)
        order_url = reverse('api:orders:order-detail', args=[order.id])
        response = self.client_admin.delete(order_url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

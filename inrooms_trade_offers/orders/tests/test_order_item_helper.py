from django.test import TestCase

from core.server_init import ServerInit
from products.factories import ProductFactory
from products.models import Size
from warehouses.models import Warehouse
from ..factories import OrderFactory, OrderItemFactory, OrderItemSizeFactory
from ..helpers import OrderItemSizeHelper


class OrderItemHelperTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(OrderItemHelperTestCase, cls).setUpClass()

        si = ServerInit()
        si.create_product_sizes()
        si.create_warehouses()

        cls.warehouse_data = {warehouse.code: warehouse for warehouse in Warehouse.objects.all()}
        cls.size_data = {size.code: size for size in Size.objects.all()}

    def setUp(self):
        super().setUp()
        self.product1 = ProductFactory()
        self.product2 = ProductFactory()

        self.order = OrderFactory()
        self.order_item1 = OrderItemFactory(order=self.order, product=self.product1, price=10)
        self.order_item2 = OrderItemFactory(order=self.order, product=self.product2, price=20)

    def test_get_quantity(self):
        OrderItemSizeFactory(order_item=self.order_item1, quantity=2, size=self.size_data['xxs'], warehouse=self.warehouse_data['msk'])
        OrderItemSizeFactory(order_item=self.order_item1, quantity=2, size=self.size_data['xxs'], warehouse=self.warehouse_data['showroom'])
        OrderItemSizeFactory(order_item=self.order_item1, quantity=10, size=self.size_data['s'])
        OrderItemSizeFactory(order_item=self.order_item2, quantity=10, size=self.size_data['s'])

        helper = OrderItemSizeHelper(order_item=self.order_item1)
        max_count = helper.get_quantity()
        self.assertEqual(max_count, 14)

    def test_get_size_quantity(self):
        OrderItemSizeFactory(order_item=self.order_item1, quantity=10, size=self.size_data['s'])
        OrderItemSizeFactory(order_item=self.order_item2, quantity=12, size=self.size_data['s'])

        helper = OrderItemSizeHelper(order_item=self.order_item1)
        self.assertEqual(helper.get_size_quantity('s'), 10)
        self.assertEqual(helper.get_size_quantity('m'), 0)

    def test_get_size_quantitys(self):
        OrderItemSizeFactory(order_item=self.order_item1, quantity=2, size=self.size_data['xxs'], warehouse=self.warehouse_data['msk'])
        OrderItemSizeFactory(order_item=self.order_item1, quantity=2, size=self.size_data['xxs'], warehouse=self.warehouse_data['showroom'])
        OrderItemSizeFactory(order_item=self.order_item1, quantity=10, size=self.size_data['s'])
        OrderItemSizeFactory(order_item=self.order_item2, quantity=12, size=self.size_data['s'])

        helper = OrderItemSizeHelper(order_item=self.order_item1)
        size_quantitys = helper.get_size_quantitys()
        self.assertEqual(len(size_quantitys), 2)
        size_quantity = size_quantitys[0]
        self.assertEqual(size_quantity['size'].id, self.size_data['xxs'].id)
        self.assertEqual(size_quantity['quantity'], 4)
        size_quantity = size_quantitys[1]
        self.assertEqual(size_quantity['size'].id, self.size_data['s'].id)
        self.assertEqual(size_quantity['quantity'], 10)

    def test_get_price_sum(self):
        OrderItemSizeFactory(order_item=self.order_item1, quantity=4, size=self.size_data['xxs'])
        OrderItemSizeFactory(order_item=self.order_item1, quantity=10, size=self.size_data['s'])
        OrderItemSizeFactory(order_item=self.order_item2, quantity=12, size=self.size_data['s'])

        helper = OrderItemSizeHelper(order_item=self.order_item1)
        self.assertEqual(helper.get_price_sum(), 140)

    def test_get_sizes(self):
        OrderItemSizeFactory(order_item=self.order_item1, quantity=4, size=self.size_data['xxs'])
        OrderItemSizeFactory(order_item=self.order_item1, quantity=10, size=self.size_data['s'])
        OrderItemSizeFactory(order_item=self.order_item2, quantity=12, size=self.size_data['s'])

        helper = OrderItemSizeHelper(order_item=self.order_item1)
        sizes = helper.get_sizes()
        self.assertEqual(len(sizes), 2)
        self.assertEqual(sizes[0].id, self.size_data['xxs'].id)
        self.assertEqual(sizes[1].id, self.size_data['s'].id)

    def test_max_series_count(self):
        helper = OrderItemSizeHelper(order_item=self.order_item1)
        max_count = helper.get_max_series_count()
        self.assertEqual(max_count, 0)

        OrderItemSizeFactory(order_item=self.order_item1, quantity=4, size=self.size_data['xxs'])
        helper = OrderItemSizeHelper(order_item=self.order_item1)
        max_count = helper.get_max_series_count()
        self.assertEqual(max_count, 4)

        OrderItemSizeFactory(order_item=self.order_item1, quantity=10, size=self.size_data['m'])
        helper = OrderItemSizeHelper(order_item=self.order_item1)
        max_count = helper.get_max_series_count()
        self.assertEqual(max_count, 5)

        OrderItemSizeFactory(order_item=self.order_item1, quantity=12, size=self.size_data['s'])
        helper = OrderItemSizeHelper(order_item=self.order_item1)
        max_count = helper.get_max_series_count()
        self.assertEqual(max_count, 6)

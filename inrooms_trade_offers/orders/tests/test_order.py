from django.test import TestCase

from accounts.factories import UserFactory
from clients.factories import ClientFactory
from orders.factories import OrderFactory, OrderItemFactory, OrderItemSizeFactory
from products.factories import ProductFactory
from ..models import Order, OrderItemSize


class OrderTestCase(TestCase):

    def test_set_series_count_on_order_create(self):
        """При создании заказа order.series_count = order.client.series_count"""

        client = ClientFactory(series_count=100)
        user = UserFactory()
        order = Order(client=client, creator=user)
        order.save()
        self.assertEqual(order.series_count, client.series_count)

    def test_create_1c_document(self):
        from service_1c.models import Document
        Document.objects.all().delete()

        order = OrderFactory()
        order.create_1c_document()

        self.assertEqual(Document.objects.count(), 1)
        self.assertEqual(Document.objects.all()[0].kind, Document.KIND.order)

    def test_set_disable_local_reserves(self):
        order = OrderFactory()
        order2 = OrderFactory()
        product = ProductFactory()
        order_item = OrderItemFactory(order=order, product=product)
        order_item2 = OrderItemFactory(order=order2, product=product)
        OrderItemSizeFactory(order_item=order_item, is_local_reserve=False)
        OrderItemSizeFactory(order_item=order_item2, is_local_reserve=False)

        self.assertEqual(OrderItemSize.objects.filter(is_local_reserve=False).count(), 2)

        order.set_local_reserves(True)
        self.assertEqual(OrderItemSize.objects.filter(order_item__order=order, is_local_reserve=False).count(), 0)
        self.assertEqual(OrderItemSize.objects.filter(order_item__order=order, is_local_reserve=True).count(), 1)
        self.assertEqual(OrderItemSize.objects.filter(order_item__order=order2, is_local_reserve=False).count(), 1)
        self.assertEqual(OrderItemSize.objects.filter(order_item__order=order2, is_local_reserve=True).count(), 0)

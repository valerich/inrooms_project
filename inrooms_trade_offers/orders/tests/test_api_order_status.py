from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.tests.setup import UsersSetup
from ..models import OrderStatus


class OrderStatusViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(OrderStatusViewTestCase, cls).setUpClass()
        cls.order_status_url = reverse('api:orders:order_status-list')

        UsersSetup()

    def setUp(self):
        self.client_admin = APIClient()
        self.client_admin.login(email='admin@example.com', password='password')

        self.client_bm = APIClient()
        self.client_bm.login(email='brandmanager@example.com', password='password')

        self.client_pa = APIClient()
        self.client_pa.login(email='processadministrator@example.com', password='password')

    def tearDown(self):
        self.client_admin.session.clear()
        self.client_bm.session.clear()
        self.client_pa.session.clear()

    def test_blank_request_data(self):
        response = self.client_bm.post(self.order_status_url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_create(self):
        response = self.client_bm.post(self.order_status_url, {})
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_list(self):
        response = self.client_bm.get(self.order_status_url)
        self.assertEqual(response.data['count'], 10)
        response = self.client_pa.get(self.order_status_url)
        self.assertEqual(response.data['count'], 10)

    def test_list_fields(self):
        """Проверяем, что api возвращает все поля, необходимые для отображения списка"""

        response = self.client_admin.get(self.order_status_url)
        order_dict = response.data['results'][0]
        fields = [
            'id',
            'name',
            'code',
            'next_status_detail',
        ]
        for field in fields:
            try:
                field_container = order_dict
                for i in field.split('.'):
                    field_container = field_container[i]
            except (KeyError, TypeError):
                self.fail('Не передается поле {} в {}'.format(field, self.order_status_url))

    def test_list_sort_by_fields(self):
        """Проверяем, что api сортируется по всем необходимым полям"""

        fields = [
            'name',
            'code',
        ]
        for field in fields:
            for prefix in ['', '-']:
                filter_field = "{}{}".format(prefix, field)
                response = self.client_admin.get(self.order_status_url, {'sort_by': filter_field})
                order_dict = response.data['results'][0]
                order = OrderStatus.objects.all().order_by(filter_field)[0]
                self.assertEqual(
                    order_dict['id'], getattr(order, 'id'),
                    'Не сортируется по полю {} в {}'.format(filter_field, self.order_status_url))

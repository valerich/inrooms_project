from django.test import TestCase

from core.server_init import ServerInit
from core.utils.constants import ORDER_CATEGORY_CHOICE
from products.factories import ProductFactory, RemainFactory
from products.models import Size
from warehouses.models import Warehouse
from ..factories import OrderFactory, OrderItemFactory, OrderItemSizeFactory
from ..helpers import OrderHelper
from ..models import OrderItemSize


class OrderHelperTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(OrderHelperTestCase, cls).setUpClass()

        si = ServerInit()
        si.create_product_sizes()
        si.create_warehouses()

        cls.warehouse_data = {warehouse.code: warehouse for warehouse in Warehouse.objects.all()}
        cls.size_data = {size.code: size for size in Size.objects.all()}

    def setUp(self):
        super().setUp()
        self.product1 = ProductFactory(article='0101/010')
        self.product2 = ProductFactory(article='1010/101')

        self.order = OrderFactory()
        self.order_item1 = OrderItemFactory(order=self.order, product=self.product1, price=10)
        self.order_item2 = OrderItemFactory(order=self.order, product=self.product2, price=20)

        self.order2 = OrderFactory()
        self.order2_item1 = OrderItemFactory(order=self.order2, product=self.product1, price=10)
        self.order2_item2 = OrderItemFactory(order=self.order2, product=self.product2, price=20)

    def test_check_reserves(self):
        """Проверка резервов. У заказа нет резервов"""

        oh = OrderHelper(order=self.order)
        is_reserves_ok, errors = oh.check_reserves()
        self.assertTrue(is_reserves_ok)
        self.assertEqual(len(errors), 0)

    def test_check_reserves_fail_reserves(self):
        """Проверка резервов. Резервы заказа не соответствуют остаткам"""

        # Тестируемый заказ
        ois_white = OrderItemSizeFactory(order_item=self.order_item1,
                                         warehouse=self.warehouse_data['msk'],
                                         size=self.size_data['s'],
                                         category=ORDER_CATEGORY_CHOICE.white,
                                         quantity=5)
        ois_black = OrderItemSizeFactory(order_item=self.order_item1,
                                         warehouse=self.warehouse_data['msk'],
                                         size=self.size_data['s'],
                                         category=ORDER_CATEGORY_CHOICE.black,
                                         quantity=5)
        oi2s = OrderItemSizeFactory(order_item=self.order_item2,
                                    warehouse=self.warehouse_data['msk'],
                                    size=self.size_data['s'],
                                    category=ORDER_CATEGORY_CHOICE.black,
                                    quantity=5)
        # Другой такой же заказ. Для проверки, что корректно работают выборки
        o2is_white = OrderItemSizeFactory(order_item=self.order2_item1,
                                          warehouse=self.warehouse_data['msk'],
                                          size=self.size_data['s'],
                                          category=ORDER_CATEGORY_CHOICE.white,
                                          quantity=5)
        o2is_black = OrderItemSizeFactory(order_item=self.order2_item1,
                                          warehouse=self.warehouse_data['msk'],
                                          size=self.size_data['s'],
                                          category=ORDER_CATEGORY_CHOICE.black,
                                          quantity=5)
        o2i2s = OrderItemSizeFactory(order_item=self.order2_item2,
                                     warehouse=self.warehouse_data['msk'],
                                     size=self.size_data['s'],
                                     category=ORDER_CATEGORY_CHOICE.black,
                                     quantity=5)

        RemainFactory(product=self.product1,
                      warehouse=self.warehouse_data['msk'],
                      size=self.size_data['s'],
                      value=7)

        RemainFactory(product=self.product2,
                      warehouse=self.warehouse_data['msk'],
                      size=self.size_data['s'],
                      value=5)

        oh = OrderHelper(order=self.order)
        is_reserves_ok, errors = oh.check_reserves()
        self.assertFalse(is_reserves_ok)
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0]['product'].id, self.product1.id)
        self.assertEqual(errors[0]['warehouse'].code, 'msk')
        self.assertEqual(errors[0]['size'].code, 's')
        self.assertEqual(errors[0]['remain'], 7)
        self.assertEqual(errors[0]['reserve'], 10)

    def test_re_reserve(self):
        """Проверка резервов. Резервы заказа не соответствуют остаткам"""

        # Тестируемый заказ
        ois_white = OrderItemSizeFactory(order_item=self.order_item1,
                                         warehouse=self.warehouse_data['msk'],
                                         size=self.size_data['s'],
                                         category=ORDER_CATEGORY_CHOICE.white,
                                         quantity=5)
        ois_black = OrderItemSizeFactory(order_item=self.order_item1,
                                         warehouse=self.warehouse_data['msk'],
                                         size=self.size_data['s'],
                                         category=ORDER_CATEGORY_CHOICE.black,
                                         quantity=5)
        oi2s = OrderItemSizeFactory(order_item=self.order_item2,
                                    warehouse=self.warehouse_data['msk'],
                                    size=self.size_data['s'],
                                    category=ORDER_CATEGORY_CHOICE.black,
                                    quantity=5)
        # Другой такой же заказ. Для проверки, что корректно работают выборки
        o2is_white = OrderItemSizeFactory(order_item=self.order2_item1,
                                          warehouse=self.warehouse_data['msk'],
                                          size=self.size_data['s'],
                                          category=ORDER_CATEGORY_CHOICE.white,
                                          quantity=5)
        o2is_black = OrderItemSizeFactory(order_item=self.order2_item1,
                                          warehouse=self.warehouse_data['msk'],
                                          size=self.size_data['s'],
                                          category=ORDER_CATEGORY_CHOICE.black,
                                          quantity=5)
        o2i2s = OrderItemSizeFactory(order_item=self.order2_item2,
                                     warehouse=self.warehouse_data['msk'],
                                     size=self.size_data['s'],
                                     category=ORDER_CATEGORY_CHOICE.black,
                                     quantity=5)

        RemainFactory(product=self.product1,
                      warehouse=self.warehouse_data['msk'],
                      size=self.size_data['s'],
                      value=7)

        oh = OrderHelper(order=self.order)
        oh.re_reserve()

        # Резерв не изменился т.к. хватило остатков
        new_ois_black = OrderItemSize.objects.get(id=ois_black.id)
        self.assertEqual(new_ois_black.quantity, 5)

        # Резерв изменился т.к. не хватило остатков
        new_ois_white = OrderItemSize.objects.get(id=ois_white.id)
        self.assertEqual(new_ois_white.quantity, 2)

        # Тут удалили резерв т.к. не было остатков под него
        with self.assertRaises(OrderItemSize.DoesNotExist):
            new_oi2s = OrderItemSize.objects.get(id=oi2s.id)

    def test_sort_items(self):
        oh = OrderHelper(order=self.order)
        oh.sort_items()

        self.assertEqual(self.order.items.get(product_id=self.product1).sort_index, 0)
        self.assertEqual(self.order.items.get(product_id=self.product2).sort_index, 1)

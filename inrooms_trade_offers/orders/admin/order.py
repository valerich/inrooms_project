from django.contrib import admin
from nested_inline.admin import NestedTabularInline, NestedModelAdmin, NestedStackedInline

from core.admin import NotDeleteModelAdmin
from ..forms import (
    OrderAdminForm,
    OrderItemAdminForm,
    OrderItemSizeAdminForm,
    OrderStatusAdminForm,
    ScannerDataAdminForm,
)
from ..models import OrderItem, OrderItemSize


class OrderItemSizeAdminInline(NestedTabularInline):
    form = OrderItemSizeAdminForm
    model = OrderItemSize
    extra = 0
    fk_name = 'order_item'
    raw_id_fields = ['warehouse', ]


class OrderItemAdminInline(NestedStackedInline):
    model = OrderItem
    form = OrderItemAdminForm
    raw_id_fields = ['product', ]
    extra = 0
    fk_name = 'order'
    inlines = [OrderItemSizeAdminInline, ]


class OrderAdmin(NestedModelAdmin, NotDeleteModelAdmin):
    list_display = ('id', 'status', 'client', 'date_shipped', 'date_recived', )
    list_editable = ('date_shipped', 'date_recived', )
    search_fields = ('id', )
    list_filter = ('status', )
    form = OrderAdminForm
    inlines = [OrderItemAdminInline, ]
    actions = ('send_to_1c_action',)

    def send_to_1c_action(self, request, queryset):
        for order in queryset:
            order.create_1c_document()
        self.message_user(request, 'Отправлено на выгрузку в 1с')

    send_to_1c_action.short_description = 'Выгрузить в 1с'


class OrderStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name', 'weight')
    search_fields = ('name', 'code',)
    form = OrderStatusAdminForm


class ScannerDataAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'is_ordered')
    list_filter = ('is_ordered', )
    form = ScannerDataAdminForm

from django.contrib import admin

from core.admin import NotDeleteModelAdmin
from ..forms import DefectActAdminForm, DefectActStatusAdminForm, DefectActItemAdminForm
from ..models import DefectActItem


class DefectActItemAdminInline(admin.TabularInline):
    model = DefectActItem
    form = DefectActItemAdminForm
    raw_id_fields = ['order_item', ]
    extra = 0


class DefectActAdmin(NotDeleteModelAdmin):
    list_display = ('id', 'client', )
    list_filter = ('status', )
    form = DefectActAdminForm

    actions = ('send_to_1c_action',)

    def send_to_1c_action(self, request, queryset):
        for object in queryset:
            object.create_1c_document()
        self.message_user(request, 'Отправлено на выгрузку в 1с')

    inlines = [DefectActItemAdminInline, ]


class DefectActStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name', 'weight', 'is_end_status')
    list_filter = ['is_end_status', ]
    search_fields = ('name', 'code',)
    form = DefectActStatusAdminForm

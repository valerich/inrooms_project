from django.contrib import admin

from nested_inline.admin import NestedStackedInline, NestedTabularInline, NestedModelAdmin

from core.admin import NotDeleteModelAdmin
from ..forms import (
    OrderB2CAdminForm,
    OrderB2CItemAdminForm,
    OrderB2CItemSizeAdminForm,
    OrderReturnAdminForm,
    OrderReturnB2CAdminForm,
    OrderReturnB2CItemAdminForm,
    OrderReturnB2CItemSizeAdminForm,
    OrderReturnItemAdminForm,
    OrderReturnItemSizeAdminForm,
)
from ..models import (
    Cheque1c,
    OrderB2CItem,
    OrderB2CItemSize,
    OrderReturnItem,
    OrderReturnItemSize,
    OrderReturnB2CItem,
    OrderReturnB2CItemSize,
)


class Cheque1cAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'order_id',
                    'number_1c',
                    'seller',
                    'buyer_card')
    search_fields = ('=id', '=order_id', '=number_1c')


class OrderB2CItemSizeAdminInline(NestedTabularInline):
    form = OrderB2CItemSizeAdminForm
    model = OrderB2CItemSize
    extra = 0
    fk_name = 'order_item'


class OrderB2CItemAdminInline(NestedStackedInline):
    model = OrderB2CItem
    form = OrderB2CItemAdminForm
    raw_id_fields = ['product', ]
    extra = 0
    fk_name = 'order'
    inlines = [OrderB2CItemSizeAdminInline, ]


class OrderB2CAdmin(NestedModelAdmin, NotDeleteModelAdmin):
    list_display = ('id',
                    'client', )
    search_fields = ('id', )
    form = OrderB2CAdminForm
    inlines = [OrderB2CItemAdminInline, ]


class OrderReturnItemSizeAdminInline(NestedTabularInline):
    form = OrderReturnItemSizeAdminForm
    model = OrderReturnItemSize
    extra = 0
    fk_name = 'order_item'


class OrderReturnItemAdminInline(NestedStackedInline):
    model = OrderReturnItem
    form = OrderReturnItemAdminForm
    raw_id_fields = ['product', ]
    extra = 0
    fk_name = 'order'
    inlines = [OrderReturnItemSizeAdminInline, ]


class OrderReturnAdmin(NestedModelAdmin, NotDeleteModelAdmin):
    list_display = ('id',
                    'client', )
    search_fields = ('id', )
    form = OrderReturnAdminForm
    inlines = [OrderReturnItemAdminInline, ]


class OrderReturnB2CItemSizeAdminInline(NestedTabularInline):
    form = OrderReturnB2CItemSizeAdminForm
    model = OrderReturnB2CItemSize
    extra = 0
    fk_name = 'order_item'


class OrderReturnB2CItemAdminInline(NestedStackedInline):
    model = OrderReturnB2CItem
    form = OrderReturnB2CItemAdminForm
    raw_id_fields = ['product', ]
    extra = 0
    fk_name = 'order'
    inlines = [OrderReturnB2CItemSizeAdminInline, ]


class OrderReturnB2CAdmin(NestedModelAdmin, NotDeleteModelAdmin):
    list_display = ('id',
                    'client', )
    search_fields = ('id', )
    form = OrderReturnB2CAdminForm
    inlines = [OrderReturnB2CItemAdminInline, ]

from core.admin import JsonAdmin
from ..forms import ChequeAdminForm


class ChequeAdmin(JsonAdmin):
    list_display = (
        'id',
        'number_1c',
        'date',
        'kind',
        'client',
        'buyer_card',
        'seller',
    )
    list_filter = ('kind', 'client')
    search_fields = ('number_1c', )
    form = ChequeAdminForm

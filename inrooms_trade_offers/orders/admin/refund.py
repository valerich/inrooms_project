from django.contrib import admin

from core.admin import NotDeleteModelAdmin
from ..forms import (
    RefundActAdminForm,
    RefundActItemAdminForm,
    RefundActStatusAdminForm,
)
from ..models import RefundActItem


class RefundActItemAdminInline(admin.TabularInline):
    model = RefundActItem
    form = RefundActItemAdminForm
    raw_id_fields = ['order_item', ]
    extra = 0


class RefundActAdmin(NotDeleteModelAdmin):
    list_display = ('id', 'status', 'client', 'created', 'modified', )
    search_fields = ('id', )
    list_filter = ('status', )
    form = RefundActAdminForm
    inlines = [RefundActItemAdminInline, ]
    actions = ('send_to_1c_action',)

    def send_to_1c_action(self, request, queryset):
        for object in queryset:
            object.create_1c_document()
        self.message_user(request, 'Отправлено на выгрузку в 1с')

    send_to_1c_action.short_description = 'Выгрузить в 1с'


class RefundActStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name', 'weight')
    search_fields = ('name', 'code',)
    form = RefundActStatusAdminForm

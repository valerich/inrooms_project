from django.contrib import admin


class SalesPlanAdmin(admin.ModelAdmin):
    list_display = ('id', 'year', 'month', 'store', )
    list_filter = ('year', 'month')

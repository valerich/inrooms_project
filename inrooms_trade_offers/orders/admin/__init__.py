from django.contrib import admin

from .cheque import ChequeAdmin
from .defect import DefectActAdmin, DefectActStatusAdmin
from .order import (
    OrderAdmin,
    OrderStatusAdmin,
    ScannerDataAdmin,
)
from .order_b2c import (
    Cheque1cAdmin,
    OrderB2CAdmin,
    OrderReturnAdmin,
    OrderReturnB2CAdmin,
)
from .refund import (
    RefundActAdmin,
    RefundActStatusAdmin,
)
from ..models import (
    Cheque,
    Cheque1c,
    DefectAct,
    DefectActStatus,
    Order,
    OrderB2C,
    OrderItem,
    OrderItemSize,
    OrderReturn,
    OrderReturnB2C,
    OrderStatus,
    RefundAct,
    RefundActStatus,
    ScannerData,
    SalesPlan,
)
from .sales_plan import (
    SalesPlanAdmin,
)


for model_or_iterable, admin_class in (
    (Cheque, ChequeAdmin),
    (Cheque1c, Cheque1cAdmin),
    (DefectAct, DefectActAdmin),
    (DefectActStatus, DefectActStatusAdmin),
    (Order, OrderAdmin),
    (OrderB2C, OrderB2CAdmin),
    (OrderReturn, OrderReturnAdmin),
    (OrderReturnB2C, OrderReturnB2CAdmin),
    (OrderStatus, OrderStatusAdmin),
    (RefundAct, RefundActAdmin),
    (RefundActStatus, RefundActStatusAdmin),
    (ScannerData, ScannerDataAdmin),
    (SalesPlan, SalesPlanAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

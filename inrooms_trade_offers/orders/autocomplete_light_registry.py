from autocomplete_light import shortcuts as autocomplete_light

from orders.models.order_b2c import OrderReturn
from .models import (
    DefectActStatus,
    Order,
    OrderB2C,
    OrderReturnB2C,
    OrderStatus,
    RefundActStatus,
)


class OrderAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['id', ]
    attrs = {
        'placeholder': 'Номер',
        'data-autocomplete-minimum-characters': 0,
    }


class OrderB2CAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['id', ]
    attrs = {
        'placeholder': 'Номер',
        'data-autocomplete-minimum-characters': 0,
    }


class OrderReturnAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['id', ]
    attrs = {
        'placeholder': 'Номер',
        'data-autocomplete-minimum-characters': 0,
    }


class OrderReturnB2CAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['id', ]
    attrs = {
        'placeholder': 'Номер',
        'data-autocomplete-minimum-characters': 0,
    }


class OrderStatusAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


class DefectActStatusAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


class RefundActStatusAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', ]
    attrs = {
        'placeholder': 'Название',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (DefectActStatus, DefectActStatusAutocomplete),
    (Order, OrderAutocomplete),
    (OrderB2C, OrderB2CAutocomplete),
    (OrderReturnB2C, OrderReturnB2CAutocomplete),
    (OrderReturn, OrderReturnAutocomplete),
    (OrderStatus, OrderStatusAutocomplete),
    (RefundActStatus, RefundActStatusAutocomplete),
)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

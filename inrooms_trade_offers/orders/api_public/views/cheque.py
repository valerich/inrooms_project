from rest_framework.mixins import CreateModelMixin
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from orders.api_public.serializers import ChequeSerializer
from orders.models.cheques import Cheque


class ChequeViewSet(CreateModelMixin,
                    GenericViewSet):
    serializer_class = ChequeSerializer
    model = Cheque

    def perform_create(self, serializer):
        instance = serializer.save(data_1c=self.request.data)
        return instance

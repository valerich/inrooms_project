from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from products.models import Product, Size
from ...models.order_from_site import *


class ImportOrderFromSite(APIView):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(ImportOrderFromSite, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        order_data = request.data
        # getting data
        basket_data = order_data.pop('basket')
        basket_owner = basket_data.get('owner')
        guest_email = order_data.pop('guest_email')
        order_items = basket_data.pop('lines')

        # get client info
        if basket_owner:
            client, cl_created = ClientFromSite.objects.get_or_create(email__exact=basket_owner.get('email'), defaults=basket_owner)
        elif guest_email:
            client, cl_created = ClientFromSite.objects.get_or_create(guest_email=guest_email, email=guest_email)

        # set order client
        order_data['client'] = client
        # set order shipping_address
        shipping_address = order_data.pop('shipping_address')
        shipping_address, sa_created = ShippingAddress.objects.get_or_create(shipping_address)
        order_data['shipping_address'] = shipping_address

        # create order
        order, o_created = OrderFromSite.objects.get_or_create(number__exact = order_data['number'], defaults=order_data)

        for order_item in order_items:
            # get quantity
            quantity = order_item.pop('quantity')
            # fet Size obj
            size_obj = Size.objects.get(name=order_item['product']['size'])
            # set product
            order_item['product'] =  Product.objects.get(id=order_item['product']['id'])
            # set order
            order_item['order'] = order
            # create Order obj
            order_item, oi_created = OrderFromSiteItem.objects.get_or_create(order_id__exact = order.id, product_id__exact=order_item['product'].id, defaults=order_item)
            size, sa_created = OrderFromSiteItemSize.objects.get_or_create(size=size_obj, order_item=order_item, quantity=quantity)

        return Response(data={'order': order.id, 'created': o_created})

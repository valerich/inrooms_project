from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import (
    ChequeViewSet,
    ImportOrderFromSite
)

router = DefaultRouter()
router.register(r'cheque', ChequeViewSet, base_name='cheque')

urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
    urls.url(r'^im_orders/order/', ImportOrderFromSite.as_view()),
]

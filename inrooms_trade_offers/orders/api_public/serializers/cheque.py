
from rest_framework import serializers, validators

from client_cards.models import BuyerCard
from orders.models.cheques import Cheque, ChequeItem, ChequeItemSize
from products.models import Size


class ChequeItemSizeSerializer(serializers.ModelSerializer):

    size = serializers.SlugRelatedField(queryset=Size.objects.all(), slug_field='code_1c')

    class Meta:
        model = ChequeItemSize

        fields = [
            'id',
            'size',
            'quantity',
        ]


class ChequeItemSerializer(serializers.ModelSerializer):
    sizes = ChequeItemSizeSerializer(many=True, required=False)
    summa = serializers.DecimalField(max_digits=20, decimal_places=10, required=False)

    class Meta:
        model = ChequeItem

        fields = [
            'id',
            'product',
            'price',
            'sizes',
            'summa',
        ]


class ChequeSerializer(serializers.ModelSerializer):
    items = ChequeItemSerializer(many=True, required=False)
    buyer_card = serializers.SlugRelatedField(queryset=BuyerCard.objects.all(), slug_field='number',
                                              required=False, allow_null=True)

    class Meta:
        model = Cheque
        fields = [
            'id',
            'date',
            'client',
            'buyer_card',
            'number_1c',
            'seller',
            'items',
            'kind',
        ]

    def run_validators(self, value):
        for validator in self.validators:
          if isinstance(validator, validators.UniqueTogetherValidator):
            self.validators.remove(validator)
        super(ChequeSerializer, self).run_validators(value)

    def create(self, validated_data):
        item_data = validated_data.pop('items', [])
        client = validated_data.pop('client', None)
        seller = validated_data.pop('seller', None)
        number_1c = validated_data.pop('number_1c', '')
        cheque, created = Cheque.objects.update_or_create(
            client=client,
            number_1c=number_1c,
            seller=seller,
            defaults=validated_data)
        ChequeItem.objects.filter(cheque=cheque).delete()
        for item in item_data:
            size_data = item.pop('sizes', [])
            d = dict(item)
            d["price_sum"] = d.pop('summa', 0)
            cheque_item = ChequeItem.objects.create(cheque=cheque, **d)
            for size_item in size_data:
                dd = dict(size_item)
                ChequeItemSize.objects.create(cheque_item=cheque_item, **dd)
            quantity = cheque_item.get_quantity()
            if quantity:
                cheque_item.price_discount = cheque_item.get_price_sum() / quantity
                cheque_item.save()
        return cheque

    def update(self, instance, validated_data):
        item_data = validated_data.pop('items', [])
        client = validated_data.pop('client', None)
        seller = validated_data.pop('seller', None)
        number_1c = validated_data.pop('number_1c', '')
        instance, created = Cheque.objects.update_or_create(
            client=client,
            number_1c=number_1c,
            seller=seller,
            defaults=validated_data)
        ChequeItem.objects.filter(cheque=instance).delete()
        for item in item_data:
            size_data = item.pop('sizes', [])
            d = dict(item)
            ChequeItem.objects.create(cheque=instance, **d)
            for size_item in size_data:
                dd = dict(size_item)
                ChequeItemSize.objects.create(cheque_item=cheque_item, **dd)
        instance.save()
        return instance

import factory
import factory.django
import factory.fuzzy

from accounts.factories import UserFactory
from clients.factories import ClientFactory
from products.factories import ProductFactory, SizeFactory
from warehouses.factories import WarehouseFactory
from .models import Order, OrderItem, OrderItemSize, OrderStatus, ScannerData


class OrderStatusFactory(factory.django.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText()
    code = factory.fuzzy.FuzzyText()

    class Meta:
        model = OrderStatus


class OrderFactory(factory.django.DjangoModelFactory):
    status = factory.SubFactory(OrderStatusFactory)
    client = factory.SubFactory(ClientFactory)
    creator = factory.SubFactory(UserFactory)

    class Meta:
        model = Order


class OrderItemFactory(factory.django.DjangoModelFactory):
    order = factory.SubFactory(OrderFactory)
    product = factory.SubFactory(ProductFactory)

    class Meta:
        model = OrderItem


class OrderItemSizeFactory(factory.django.DjangoModelFactory):
    order_item = factory.SubFactory(OrderItemFactory)
    warehouse = factory.SubFactory(WarehouseFactory)
    size = factory.SubFactory(SizeFactory)
    quantity = factory.fuzzy.FuzzyInteger(1, 1000)

    class Meta:
        model = OrderItemSize


class ScannerDataFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = ScannerData

from django.db import transaction
from django.db.models.aggregates import Sum

from products.models import Size
from products.services import RemainService
from supply.helpers.supply import SupplyHelper
from warehouses.models import Warehouse


class SetSizeQuantityException(Exception):
    pass


class OrderItemSizeHelper(object):

    def __init__(self, order_item):
        self.order_item = order_item
        self.sizes = {}
        self.warehouse_codes = [w.code for w in Warehouse.objects.filter(to_orders=True)]
        self.warehouses = {}

    def _set_sizes(self):
        self.sizes = {s.code: s for s in Size.objects.all()}

    def get_quantity(self, category=None):
        """Количество товара в заказе.

        Сумма количеств размеров товара.
        """
        qs = self.order_item.orderitemsize_set.all()
        if category:
            qs = qs.filter(category=category)
        quantity = qs.aggregate(Sum('quantity'))['quantity__sum']
        if quantity is None:
            return 0
        return quantity

    def get_size_quantity(self, size):
        """Количество определенного размера"""

        size = self._get_size_instance(size)
        quantity = self.order_item.orderitemsize_set.filter(size=size).aggregate(Sum('quantity'))['quantity__sum']
        if quantity is None:
            return 0
        return quantity

    def get_size_quantitys(self):
        """Количество товара по размерам

        return [
            {
                'size': Size(),
                'quantity': 1
            },
        ]
        """

        return [{'size': size,
                 'quantity': self.get_size_quantity(size)
                } for size in self.get_sizes()]

    def get_sizes(self):
        size_ids = self.order_item.orderitemsize_set.all().values_list('size_id', flat=True).distinct()
        return Size.objects.filter(id__in=size_ids)

    def get_price_sum(self, category=None):
        """Сумма за товар.

        Т.к. цену умеет товар, а количество - размеры, то умножаем цену товара на сумму количеств размеров
        """
        return self.get_quantity(category) * self.order_item.price

    def get_max_series_count(self):
        """Максимальное количество серий.

        В заказ добавляется произвольное количество размеров товара,
        но при распечатке документов необходимо отобразить количество серий.

        Количество серий размера - количество товара / множитель размера
        Количество серий товара - макс количество серий размеров
        """

        max_series_count = 0
        for ois in self.order_item.orderitemsize_set.all():
            if ois.quantity and ois.size.multiplier:
                series_count = int(ois.quantity / ois.size.multiplier)
                if series_count > max_series_count:
                    max_series_count = series_count
        return max_series_count

    def set_size_quantity(self, size, quantity, check_quantity=True):
        from ..models import OrderItemSize

        can_set_white_category = self.order_item.order.client.can_set_white_category
        if can_set_white_category:
            category = OrderItemSize.ORDER_CATEGORY.white
        else:
            category = OrderItemSize.ORDER_CATEGORY.black

        size = self._get_size_instance(size)
        saved_quantity = self.get_size_quantity(size)
        if saved_quantity != quantity:
            with transaction.atomic():
                OrderItemSize.objects.filter(order_item=self.order_item, size=size).delete()

                # Добавляем товары на основании настоящих остатков
                if quantity > 0:
                    rs = RemainService([self.order_item.product_id, ], warehouse_codes=self.warehouse_codes)
                    remains = rs.get_remains()[self.order_item.product_id]
                    for warehouse_code in self.warehouse_codes:
                        warehouse_remain = remains[warehouse_code][size.code]
                        if warehouse_remain > quantity:
                            quantity_to_create = quantity
                            quantity = 0
                        else:
                            quantity_to_create = warehouse_remain
                            quantity -= warehouse_remain
                        OrderItemSize.objects.create(
                            order_item=self.order_item,
                            size=size,
                            quantity=quantity_to_create,
                            warehouse=self._get_warehouse_instance(warehouse_code),
                            category=category,
                        )
                        if quantity == 0:
                            break

                # Добавляем товары которые находятся в пути
                if quantity > 0:
                    ss = SupplyHelper(product_ids=[self.order_item.product_id, ],
                                      order_ids=[self.order_item.order_id, ],
                                      supply_statuses=['delivery_waiting',
                                                       'sent_to_warehouse',
                                                       'set_prices', ])
                    supply_sizes = ss.get_products_by_size()[self.order_item.product_id]
                    delivery_remain = supply_sizes[size.code]
                    if delivery_remain > quantity:
                        quantity_to_create = quantity
                        quantity = 0
                    else:
                        quantity_to_create = delivery_remain
                        quantity -= delivery_remain
                    OrderItemSize.objects.create(
                        order_item=self.order_item,
                        size=size,
                        quantity=quantity_to_create,
                        warehouse=self._get_warehouse_instance('msk'),
                        category=category,
                        is_delivery_reserve=True,
                    )

                # Если не получилось, значит не хватает товаров
                if quantity > 0 and check_quantity:
                    raise SetSizeQuantityException('Не достаточно остатков')

    def set_size_quantity_with_multiplier(self, size, series_count=None):
        rs = RemainService(
            [self.order_item.product_id, ],
            warehouse_codes=[w.code for w in Warehouse.objects.filter(to_orders=True)],
            exclude_reserves_for_order_ids=[self.order_item.order_id, ]
        )
        remains = rs.get_remains_by_size()[self.order_item.product_id]

        ss = SupplyHelper(supply_statuses=['delivery_waiting',
                                           'sent_to_warehouse',
                                           'set_prices', ],
                          product_ids=[self.order_item.product_id, ],
                          order_ids=[self.order_item.order_id, ])
        for size_code, quantity in ss.get_products_by_size()[self.order_item.product_id].items():
            remains[size_code] += quantity

        multiplier_type = self._get_multiplier_type(remains)

        if not series_count:
            series_count = self.order_item.order.series_count

        if multiplier_type == 1:
            quantity = series_count * 6
        elif multiplier_type == 2:
            quantity = series_count * 3
        else:
            quantity = series_count * size.multiplier

        if quantity > remains[size.code]:
            quantity = remains[size.code]
        self.set_size_quantity(size, quantity, check_quantity=False)

    def _get_multiplier_type(self, remains):
        """Тип множителя

        Если в наличии только один размер M, то количество устанавливаемое для одной серии равно 6 на один размер.
        Если в наличии есть только размеры S и M, то устанавливаемое количество равно 3 для каждого размера.
        Если в наличии есть больше чем S и М размеры, то работает стандартная логика подбора серий 1122111.
        """
        available_sizes = set([size_code for size_code, remain in remains.items() if remain])

        if len(available_sizes) == 1 and 'm' in available_sizes:
            return 1
        elif len(available_sizes) == 2 and all([item in remains for item in ['s', 'm']]):
            return 2
        else:
            return 3

    def _get_size_instance(self, size):
        if isinstance(size, Size):
            return size
        elif isinstance(size, str):
            if not self.sizes:
                self._set_sizes()
            if size not in self.sizes:
                self.sizes[size] = Size.objects.get(code=size)
            return self.sizes[size]
        else:
            raise Size.DoesNotExist

    def _get_warehouse_instance(self, code):
        if code not in self.warehouses:
            self.warehouses[code] = Warehouse.objects.get(code=code)
        return self.warehouses[code]

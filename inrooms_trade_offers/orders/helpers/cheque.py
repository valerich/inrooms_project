# from django.db.models.aggregates import Sum
#
# from products.models import Size
# from warehouses.models import Warehouse
#
#
# class SetSizeQuantityException(Exception):
#     pass
#
#
# class ChequeItemSizeHelper(object):
#     item_size_field = 'chequeitemsize_set'
#
#     def __init__(self, cheque):
#         self.cheque = cheque
#         self.sizes = {}
#         # self.warehouse_codes = [w.code for w in Warehouse.objects.filter(to_orders=True)]
#         self.warehouses = {}
#
#     def _set_sizes(self):
#         self.sizes = {s.code: s for s in Size.objects.all()}
#
#     def get_quantity(self):
#         """Количество товара в заказе.
#
#         Сумма количеств размеров товара.
#         """
#         quantity = getattr(self.cheque, self.item_size_field).all().aggregate(Sum('quantity'))['quantity__sum']
#         if quantity is None:
#             return 0
#         return quantity
#
#     def get_size_quantity(self, size):
#         """Количество определенного размера"""
#
#         size = self._get_size_instance(size)
#         quantity = getattr(self.cheque, self.item_size_field).filter(size=size).aggregate(Sum('quantity'))['quantity__sum']
#         if quantity is None:
#             return 0
#         return quantity
#
#     def get_size_quantitys(self):
#         """Количество товара по размерам
#
#         return [
#             {
#                 'size': Size(),
#                 'quantity': 1
#             },
#         ]
#         """
#
#         return [{'size': size,
#                  'quantity': self.get_size_quantity(size)
#                 } for size in self.get_sizes()]
#
#     # def get_price_sum(self):
#     #     """Сумма за товар.
#     #
#     #     Т.к. цену умеет товар, а количество - размеры, то умножаем цену товара на сумму количеств размеров
#     #     """
#     #     return self.cheque.price * self.get_quantity()
#
#     def get_sizes(self):
#         size_ids = getattr(self.cheque, self.item_size_field).all().values_list('size_id', flat=True).distinct()
#         return Size.objects.filter(id__in=size_ids)
#
#     def _get_size_instance(self, size):
#         if isinstance(size, Size):
#             return size
#         elif isinstance(size, str):
#             if not self.sizes:
#                 self._set_sizes()
#             if size not in self.sizes:
#                 self.sizes[size] = Size.objects.get(code=size)
#             return self.sizes[size]
#         else:
#             raise Size.DoesNotExist
#
#     def _get_warehouse_instance(self, code):
#         if code not in self.warehouses:
#             self.warehouses[code] = Warehouse.objects.get(code=code)
#         return self.warehouses[code]

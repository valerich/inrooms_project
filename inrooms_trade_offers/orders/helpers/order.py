from collections import defaultdict

from decimal import Decimal
from django.db import transaction
from django.utils import timezone

from accounts.models import User
from core.server_init import ServerInit
from core.utils.constants import ORDER_CATEGORY_CHOICE
from history.models import HistoryItem
from invoices.models import Invoice, InvoiceItem
from prices.models import PriceSet, PriceSetItem
from products.models import Product
from products.services import RemainService
from supply.helpers.supply import SupplyHelper
from warehouses.models import Warehouse


class OrderHelper(object):

    def __init__(self, order, user=None):
        self.order = order
        self.user = user

    @transaction.atomic()
    def check_reserves(self):
        """Проверка на корректность резервов"""
        from orders.models import OrderItemSize

        reserves = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
        products = {}
        warehouses = {}
        sizes = {}
        is_reserves_ok = True
        errors = []

        for ois in OrderItemSize.objects.filter(order_item__order=self.order):
            product = ois.order_item.product
            warehouse = ois.warehouse
            size = ois.size

            reserves[product.id][warehouse.code][size.code] += ois.quantity

            products[product.id] = product
            warehouses[warehouse.code] = warehouse
            sizes[size.code] = size

        remains = RemainService(
            product_ids=products.keys(),
            warehouse_codes=warehouses.keys(),
            exclude_reserves_for_order_ids=[self.order.id]).get_remains()

        supply = SupplyHelper(
            product_ids=products.keys(),
            order_ids=[self.order.id],
            supply_statuses=[
                'sent_to_warehouse',
                'set_prices',
                'delivery_waiting',
            ]
        )
        for product_id, size_data in supply.get_products_by_size().items():
            for size_code, quantity in size_data.items():
                remains[product_id]['msk'][size_code] += quantity

        for product_id, product_data in reserves.items():
            for warehouse_code, warehouse_data in product_data.items():
                for size_code, reserve in warehouse_data.items():
                    remain = remains[product_id][warehouse_code][size_code]
                    if remain < reserve:
                        is_reserves_ok = False
                        errors.append(
                            {
                                'product': products[product_id],
                                'warehouse': warehouses[warehouse_code],
                                'size': sizes[size_code],
                                'reserve': reserve,
                                'remain': remain,
                            }
                        )

        return is_reserves_ok, errors

    @transaction.atomic()
    def re_reserve(self):
        from orders.models import OrderItem, OrderItemSize

        products = set(OrderItem.objects.filter(order=self.order).values_list('product_id', flat=True).distinct())
        warehouses = list(Warehouse.objects.filter(to_orders=True).values_list('code', flat=True))
        remains = RemainService(
            product_ids=products,
            warehouse_codes=warehouses,
            exclude_reserves_for_order_ids=[self.order.id]).get_remains()

        supply = SupplyHelper(
            product_ids=products,
            order_ids=[self.order.id],
            supply_statuses=[
                'sent_to_warehouse',
                'set_prices',
                'delivery_waiting',
            ]
        )

        for product_id, size_data in supply.get_products_by_size().items():
            for size_code, quantity in size_data.items():
                remains[product_id]['msk'][size_code] += quantity

        qs = OrderItemSize.objects.filter(order_item__order=self.order)
        qs = qs.select_related('order_item',
                               'warehouse',
                               'size', )
        qs = qs.order_by('-category')

        for ois in list(qs):
            product_id = ois.order_item.product_id
            warehouse_code = ois.warehouse.code
            size_code = ois.size.code
            remain = remains[product_id][warehouse_code][size_code]
            reserve = ois.quantity
            if remain < reserve:
                if remain == 0:
                    ois.delete()
                else:
                    ois.quantity = remain
                    ois.save()
                    remains[product_id][warehouse_code][size_code] = 0
            else:
                remains[product_id][warehouse_code][size_code] -= reserve

    @transaction.atomic()
    def sort_items(self):
        from orders.models import OrderItem

        for index, order_item in enumerate(OrderItem.objects.filter(order=self.order).order_by('product__article',
                                                                                               'product__name')):
            order_item.sort_index = index
            order_item.save()

    @transaction.atomic()
    def create_price_set_document(self):
        if not self.user:
            si = ServerInit()
            si.create_users()
            user = User.objects.get(email='service@chn.inrooms.club')
        else:
            user = self.user
        price_set, created = PriceSet.objects.get_or_create(
            order_id=self.order.id,
            defaults={
                'creator': user,
                'source': PriceSet.SOURCE.auto,
                'client': self.order.client,
                'price_set_kind': PriceSet.PRICE_SET_KIND.from_order_item,
            }
        )
        if created:
            for item in self.order.items.all():
                psi = PriceSetItem.objects.create(
                    price_set=price_set,
                    order_item=item,
                    old_price=item.price,
                    new_price=item.recommended_retail_price,
                )
        return price_set

    @transaction.atomic()
    def create_invoice_document(self, provider):
        invoices = []
        from orders.models import OrderItemSize
        for category_id in ORDER_CATEGORY_CHOICE._db_values:
            if OrderItemSize.objects.filter(order_item__order=self.order, category=category_id, quantity__gt=0).exists():
                invoice = Invoice(

                    customer=self.order.client,
                    provider=provider,
                    category=category_id,
                    comment="Бланк-заказ №{}, от {}".format(
                        self.order.id,
                        self.order.created.strftime('%d.%m.%Y'),
                    )
                )
                invoice.save()

                history_item = HistoryItem.objects.create(
                    kind=HistoryItem.KIND.invoice_create,
                    content_object=invoice,
                    body='Пользователь {} {} создал счет №{} из бланк-заказа №{}'.format(
                        self.user.name if self.user else '',
                        timezone.now().strftime('%d.%m.%Y %H:%M'),
                        invoice.id,
                        self.order.id,
                    ),
                )

                invoices.append(invoice)

                model_price = Decimal('0.00')
                for item in self.order.items.filter(product__kind=Product.KIND.clothing):
                    model_price += item.get_price_sum(category=category_id)

                if model_price:
                    invoice_item = InvoiceItem(
                        invoice=invoice,
                        product_name='Оплата за товар',
                        count=1,
                        price=model_price,
                        unit=InvoiceItem.UNIT.pieces
                    )
                    invoice_item.save()

                for item in self.order.items.filter(orderitemsize__category=category_id).exclude(product__kind=Product.KIND.clothing).order_by().distinct():
                    invoice_item = InvoiceItem(
                        invoice=invoice,
                        product_name=item.product.name,
                        count=item.get_quantity(category=category_id),
                        price=item.price,
                        unit=InvoiceItem.UNIT.pieces
                    )
                    invoice_item.save()
                invoice.save()
        return invoices
from collections import defaultdict, OrderedDict

from decimal import Decimal
from django.db.models.aggregates import Sum

from clients.models import Client
from orders.models.order import Order
from orders.models.order_b2c import OrderReturnItemSize, OrderReturn


class FreeSumm():

    def __init__(self, order_kind, user=None, client_id=None, is_detail=False):
        self.order_kind = order_kind
        self.user = user
        self.client_id = client_id
        self.client_qs = self._get_client_qs()
        self.is_detail = is_detail

    def get_data(self):
        if self.is_detail:
            return self.get_free_sum_detail()
        else:
            return self.get_free_sum()

    def get_free_sum(self):
        order_data = self._get_order_data()
        order_return_data = self._get_order_return_data()

        data = OrderedDict()

        for client in self.client_qs:
            order_return_sum = order_return_data.get(client.id, Decimal('0.0'))
            if order_return_sum is None:
                order_return_sum = Decimal('0.0')
            order_sum = order_data.get(client.id, Decimal('0.0'))
            if order_sum is None:
                order_sum = Decimal('0.0')

            free_defect_summ = order_return_sum - order_sum

            data[client.id] = {
                'free_summ': free_defect_summ,
                'name': client.name,
            }

        return data

    def get_free_sum_detail(self):
        order_data = self._get_order_data()
        order_return_data = self._get_order_return_data()

        data = OrderedDict()
        for client in self.client_qs:
            data[client.id] = {
                'name': client.name,
                'orders': order_data[client.id],
                'order_returns': order_return_data[client.id],
                'orders_sum': sum(item['amount_full'] if item['amount_full'] else 0 for item in order_data[client.id]),
                'order_returns_sum': sum(item['amount_full'] if item['amount_full'] else 0 for item in order_return_data[client.id]),
            }
        return data

    def _get_order_qs(self):
        qs = Order.objects.filter(kind=self.order_kind,
                                  client__in=self.client_qs)
        if self.is_detail:
            qs = qs.order_by('client', 'date_shipped', 'date_recived')
            qs = qs.values('id', 'client', 'amount_full', 'date_shipped', 'date_recived')
        else:
            qs = qs.values('client')
            qs = qs.order_by()
            qs = qs.distinct()
            qs = qs.annotate(Sum('amount_full'))
        return qs

    def _get_order_data(self):
        order_qs = self._get_order_qs()

        data = defaultdict(list)
        if self.is_detail:
            for o in order_qs:
                data[o['client']].append(
                    {
                        'id': o['id'],
                        'amount_full': o['amount_full'],
                        'date_shipped': o['date_shipped'],
                        'date_recived': o['date_recived'],
                    }
                )
        else:
            data = {o['client']: o['amount_full__sum'] for o in order_qs}

        return data

    def _get_order_return_qs(self):
        lookup = {
            'client__in': self.client_qs,
            'date__year__gte': 2017,
            'category__isnull': False,
        }
        if self.order_kind == 3:
            lookup['refund_act__isnull'] = False
        elif self.order_kind == 2:
            lookup['defect_act__isnull'] = False

        qs = OrderReturn.objects.filter(**lookup)

        if self.is_detail:
            qs = qs.order_by('client', 'date')
            qs = qs.values('id', 'client', 'amount_full', 'date')
        else:
            qs = qs.values('client')
            qs = qs.order_by()
            qs = qs.distinct()
            qs = qs.annotate(Sum('amount_full'))
        return qs

    def _get_order_return_data(self):
        order_return_qs = self._get_order_return_qs()

        if self.is_detail:
            data = defaultdict(list)
            for o in order_return_qs:
                data[o['client']].append(
                    {
                        'id': o['id'],
                        'amount_full': o['amount_full'],
                        'date': o['date'],
                    }
                )
        else:
            data = {o['client']: o['amount_full__sum'] for o in order_return_qs}

        return data


    def _get_client_qs(self):
        client_qs = Client.objects.filter(kind=Client.KIND.partner, is_active=True).order_by('name')
        if self.client_id:
            client_qs = client_qs.filter(id=self.client_id)
        if self.user:
            if self.order_kind == 3:
                perm_name = 'auth.can_all_free_refund_summ_view'
            elif self.order_kind == 2:
                perm_name = 'auth.can_all_free_defect_summ_view'

            if perm_name and not self.user.has_perm('auth.can_all_free_refund_summ_view'):
                if self.user.client:
                    client_qs = client_qs.filter(id=self.user.client_id)
                else:
                    client_qs = client_qs.none()
        return client_qs


def get_free_dr_summ(**kwargs):
    fs = FreeSumm(**kwargs)
    return fs.get_data()

from django.db import transaction

from core.utils.constants import ORDER_CATEGORY_CHOICE


class CategoryHelper(object):

    def __init__(self, order_id):
        self.order_id = order_id

    @transaction.atomic()
    def category_sum_distribution(self, category, max_sum):
        """Распределение товаров между черной/белой кассой

        """

        from ..models import OrderItemSize

        secondary_category = list(ORDER_CATEGORY_CHOICE._db_values.difference({category,}))[0]

        # Устанавливаем у всех размеров побочную категорию
        self._set_category_to_all_items(secondary_category)
        # И начинаем распределять
        for item_size in list(OrderItemSize.objects.filter(order_item__order_id=self.order_id)):
            item_price = item_size.order_item.price
            item_size_sum = item_size.quantity * item_price

            # Если сумма пачки размеров меньше, чем оставшаяся на распределение сумма,
            # то можно поменять категорию у всей пачки
            if item_size_sum <= max_sum:
                item_size.category = category
                item_size.save()
                max_sum -= item_size_sum
            # Иначе придется переводить в новую категорию по 1 штуке
            elif item_size.quantity > 1 and item_price < max_sum:
                new_item_size_quantity = 1
                while (new_item_size_quantity + 1) * item_price < max_sum:
                    new_item_size_quantity += 1

                item_size.quantity -= new_item_size_quantity
                item_size.save()

                OrderItemSize.objects.create(
                    order_item=item_size.order_item,
                    size=item_size.size,
                    warehouse=item_size.warehouse,
                    category=category,
                    quantity=new_item_size_quantity,
                )

                max_sum -= new_item_size_quantity * item_price

    @transaction.atomic()
    def _set_category_to_all_items(self, category):
        """Выставиление одной категории у всего заказа"""

        from ..models import OrderItemSize

        for item_size in list(OrderItemSize.objects.filter(order_item__order_id=self.order_id).exclude(category=category)):
            try:
                category_item_size = OrderItemSize.objects.get(
                    order_item_id=item_size.order_item_id,
                    warehouse_id=item_size.warehouse_id,
                    size_id=item_size.size_id,
                    category=category,
                )
            except OrderItemSize.DoesNotExist:
                item_size.category = category
                item_size.save()
            else:
                category_item_size.quantity += item_size.quantity
                category_item_size.save()
                item_size.delete()

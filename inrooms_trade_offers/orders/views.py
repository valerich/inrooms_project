import datetime

from django.shortcuts import get_object_or_404
from wkhtmltopdf.views import PDFTemplateView
from pytils import numeral

from .models import Order, RefundAct, DefectAct


class OrderExport(PDFTemplateView,):
    template_name = 'orders/order_pdf.html'
    filename = 'order.pdf'
    filename_docx = 'order.docx'

    def dispatch(self, request, object_id, *args, **kwargs):
        self.object = get_object_or_404(Order, id=object_id)
        self.with_size_quantity = request.GET.get('with_size_quantity', False) == 'true'
        self.delivery_year = request.GET.get('delivery_year', None)
        self.with_date = request.GET.get('with_date', False) == 'true'
        self.with_number = request.GET.get('with_number', False)  == 'true'
        self.sort_by = request.GET.get('sort_by', None)
        if self.sort_by not in [
            'product__article',
            '-product__article',
            'product__main_collection__name',
            '-product__main_collection__name',
        ]:
            self.sort_by = None
        return super(OrderExport, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(OrderExport, self).get_context_data(*args, **kwargs)
        items = []
        items_part = []
        qs = self.object.items.all()
        if self.sort_by:
            qs = qs.order_by(self.sort_by)
        else:
            qs = qs.order_by('product__article', 'product__color__name')
        for item in qs:
            max_series_count = 0
            for ois in item.orderitemsize_set.all():
                if ois.quantity and ois.size.multiplier:
                    series_count = ois.quantity % ois.size.multiplier
                    if series_count > max_series_count:
                        max_series_count = series_count
            item.max_series_count = max_series_count
            items_part.append(item)
            if (not items and len(items_part) >= 5) or (item and len(items_part) >= 6):
                items.append(items_part)
                items_part = []
        if items_part:
            items.append(items_part)
        context['order'] = self.object
        context['items'] = items
        context['delivery_year'] = self.delivery_year
        context['footer_on_new_page'] = False
        if len(items) > 1:
            if len(items[-1]) >= 6:
                context['footer_on_new_page'] = True
        elif len(items):
            if len(items[-1]) >= 5:
                context['footer_on_new_page'] = True
        context['pages_count'] = len(items) + (1 if context['footer_on_new_page'] else 0)
        context['document_number'] = Order.objects.filter(client=self.object.client, id__lte=self.object.id).count()
        context['with_size_quantity'] = self.with_size_quantity
        context['with_number'] = self.with_number
        context['with_date'] = self.with_date
        return context

    def get_filename(self):
        today = datetime.date.today()
        return "Бланк-заказ {}, {}.pdf".format(self.object.client.name, today.strftime("%d.%m.%Y"))


class RefundActExport(PDFTemplateView,):
    template_name = 'orders/refund_act_pdf.html'
    filename = 'refund_act.pdf'
    filename_docx = 'refund_act.docx'

    def dispatch(self, request, object_id, *args, **kwargs):
        self.object = get_object_or_404(RefundAct, id=object_id)
        return super(RefundActExport, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(RefundActExport, self).get_context_data(*args, **kwargs)
        items = {}
        items2 = []
        items_part = []
        orders = {}
        all_quantity = 0
        all_price = 0
        qs = self.object.items.all().order_by('-order_item__product__name', '-order_item__product__article', )
        for item in qs:
            orders[item.order_item.order.id] = item.order_item.order.created.date()
            if item.order_item.product_id not in items:
                items[item.order_item.product_id] = {
                    'product': item.order_item.product,
                    'prices': {},
                }
            if item.order_item.price not in items[item.order_item.product_id]['prices']:
                items[item.order_item.product_id]['prices'][item.order_item.price] = {
                    item.size.code: item.quantity
                }
            else:
                items[item.order_item.product_id]['prices'][item.order_item.price][item.size.code] = item.quantity

        for key in sorted(items.keys()):
            data = items[key]
            for price in sorted(data['prices'].keys()):
                row_item = {
                    'product': data['product'],
                    'price': price,
                    'sizes': [],
                    'all_quantity': 0,
                    'all_price': 0
                }
                for size_code in ['34','35','36','37','38','39','40','41','xs','s','m','l','xl','xxl','xxxl']:
                    if size_code in data['prices'][price]:
                        row_item['sizes'].append({
                            'size': size_code,
                            'quantity': data['prices'][price][size_code]
                        })
                        row_item['all_quantity'] += data['prices'][price][size_code]
                        row_item['all_price'] += price * data['prices'][price][size_code]
                        all_quantity += data['prices'][price][size_code]
                        all_price += price * data['prices'][price][size_code]
                items_part.append(row_item)
                if (not items2 and len(items_part) >= 5) or (row_item and len(items_part) >= 6):
                    items2.append(items_part)
                    items_part = []
        if items_part:
            items2.append(items_part)

        context['refund_act'] = self.object
        context['all_quantity'] = all_quantity
        context['all_price'] = all_price
        context['orders'] = orders
        context['items'] = items2
        context['total_price_string'] = numeral.rubles(context['all_price'], zero_for_kopeck=True)
        context['footer_on_new_page'] = False
        if len(items2) > 1:
            if len(items2[-1]) >= 6:
                context['footer_on_new_page'] = True
        elif len(items2):
            if len(items2[-1]) >= 5:
                context['footer_on_new_page'] = True
        context['pages_count'] = len(items2) + (1 if context['footer_on_new_page'] else 0)
        return context

    def get_filename(self):
        today = datetime.date.today()
        return "refund_act {}, {}.pdf".format(self.object.client.name, today.strftime("%d.%m.%Y"))


class DefectActExport(PDFTemplateView,):
    template_name = 'orders/defect_act_pdf.html'
    filename = 'defect_act.pdf'
    filename_docx = 'defect_act.docx'

    def dispatch(self, request, object_id, *args, **kwargs):
        self.object = get_object_or_404(DefectAct, id=object_id)
        return super(DefectActExport, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(DefectActExport, self).get_context_data(*args, **kwargs)

        items = []
        items_part = []
        all_price = 0
        all_quantity = 0

        qs = self.object.items.all()
        for item in qs:

            item_price = item.order_item.price
            item_all_price = item_price

            image = item.main_im
            if not image:
                image = item.order_item.product.main_im

            items_part.append({
                'product': item.order_item.product,
                'price': item_price,
                'size': item.size,
                'quantity': 1,
                'all_price': item_all_price,
                'comment': item.comment,
                'image': image,
            })

            all_price += item_all_price
            all_quantity += 1

            if (not items and len(items_part) >= 5) or (items and len(items_part) >= 6):
                items.append(items_part)
                items_part = []

        if items_part:
            items.append(items_part)

        context['defect_act'] = self.object
        context['all_price'] = all_price
        context['all_quantity'] = all_quantity
        context['items'] = items
        context['total_price_string'] = numeral.rubles(all_price, zero_for_kopeck=True)

        context['footer_on_new_page'] = False
        if len(items) > 1:
            if len(items[-1]) >= 6:
                context['footer_on_new_page'] = True
        elif len(items):
            if len(items[-1]) >= 5:
                context['footer_on_new_page'] = True

        context['pages_count'] = len(items) + (1 if context['footer_on_new_page'] else 0)
        return context

    def get_filename(self):
        today = datetime.date.today()
        return "Defect act {}, {}.pdf".format(self.object.client.name, today.strftime("%d.%m.%Y"))
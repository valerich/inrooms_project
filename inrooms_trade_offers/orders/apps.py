from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'orders'
    verbose_name = 'Заказы'

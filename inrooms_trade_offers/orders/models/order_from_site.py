import datetime
import math

from django.db.models.aggregates import Sum
from config.celery import app
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import ModificationDateTimeField
from phonenumber_field.modelfields import PhoneNumberField

from products.models import SERIES_TYPE_CHOICES, Size
from ..helpers import OrderHelper, OrderItemSizeHelper as CoreOrderItemSizeHelper

class OrderItemSizeHelper(CoreOrderItemSizeHelper):
    def get_size_quantity(self, size):
        """Количество определенного размера"""

        size = self._get_size_instance(size)
        quantity = self.order_item.orderfromsiteitemsize_set.filter(size=size).aggregate(Sum('quantity'))['quantity__sum']
        if quantity is None:
            return 0
        return quantity


class ClientFromSite(models.Model):
    id_from_site = models.PositiveIntegerField(blank=True, null=True)
    email = models.EmailField(blank=True)
    guest_email = models.EmailField(blank=True)


class ShippingAddress(models.Model):
    first_name = models.CharField(verbose_name="Имя", max_length=255, blank=True)
    last_name = models.CharField(verbose_name="Фамилия", max_length=255, blank=True)
    phone_number = PhoneNumberField()
    line1 = models.CharField(verbose_name="Первая строка адреса", max_length=255)
    line2 = models.CharField(verbose_name="Вторая строка адреса", max_length=255, blank=True)
    line3 = models.CharField(verbose_name="Третья строка адреса", max_length=255, blank=True)
    state = models.CharField(verbose_name="Район/Область", max_length=255, blank=True)
    city = models.CharField(verbose_name="Город", max_length=255, blank=True)
    postcode = models.PositiveIntegerField(verbose_name='Почтовый индекс', blank=True)


class OrderFromSite(models.Model):
    number = models.PositiveIntegerField()
    created = models.DateTimeField(_('created'), default=timezone.now)
    modified = ModificationDateTimeField(_('modified'))
    status = models.CharField(max_length=100, blank=True)
    client = models.ForeignKey('ClientFromSite', verbose_name='Клиент с сайта', on_delete=models.PROTECT)
    shipping_address = models.ForeignKey('ShippingAddress')
    message = models.TextField('Комментарий', blank=True)
    total = models.DecimalField('Полная стоимость', default=0, max_digits=10, decimal_places=2)

    date_shipped = models.DateTimeField('Время отправки', blank=True, null=True)
    date_recived = models.DateTimeField('Время получения', blank=True, null=True)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ('-created',)

    def __str__(self):
        return '№:{}'.format(self.number)

    def get_quantity(self):
        quantity = 0
        for item in self.items.all():
            quantity += item.get_quantity()
        return quantity



    def set_local_reserves(self, is_local_reserve: bool):
        """Выставляем флаги локального резерва.

        В статусе "новый" не нужно учитывать локальные резервы заказа. Это логика работы компании -
          набрать в заказ всё что можно, а потом продать только то, что есть
        В статусе "резерв создан" - учитываем
        В статусе "заказ отправлен" - учитываем
        В статусе "готов к отгрузке" - перестаем учитывать т.к. 1с уже знает о них
        """

        OrderFromSiteItemSize.objects.filter(order_item__order=self).update(is_local_reserve=is_local_reserve)

    def check_reserves(self):
        """Проверка на корректность локальных резервов"""

        order_helper = OrderHelper(order=self)
        return order_helper.check_reserves()

    def re_reserve(self):
        order_helper = OrderHelper(order=self)
        order_helper.re_reserve()

    def sort_items(self):
        order_helper = OrderHelper(order=self)
        return order_helper.sort_items()


class OrderFromSiteItem(models.Model):
    order = models.ForeignKey(OrderFromSite, verbose_name='Заказ', related_name="items")
    product = models.ForeignKey('products.Product', verbose_name='Товар')
    price = models.DecimalField('Цена', max_digits=10, decimal_places=2)
    quantity = models.PositiveSmallIntegerField('Количество', default=1)
    sort_index = models.PositiveSmallIntegerField('Сортировка', default=0, db_index=True)

    class Meta:
        verbose_name = 'Товар в заказе'
        verbose_name_plural = 'Товары в заказе'
        ordering = ('sort_index', '-id')
        unique_together = ('order', 'product',)

    def __str__(self):
        return "id: {}, order_id: {}, product_id: {}, price: {}".format(self.id, self.order_id, self.product_id,
                                                                        self.price, )

    def __getattr__(self, name):
        if name.startswith('get_count_'):
            return self.get_size_quantity(name[10:])
        if name == 'ois_helper':
            try:
                return self._ois_helper
            except AttributeError:
                self._ois_helper = OrderItemSizeHelper(order_item=self)
            return self._ois_helper
        return super().__getattribute__(name)

    def __setattr__(self, name, value):
        if name.startswith('get_count_'):
            self.set_size_quantity(name[10:], value)
        super().__setattr__(name, value)

    def get_size_quantity(self, size_code):
        """Количество заказанного товара определенного размера (шт)"""
        return getattr(self, 'ois_helper').get_size_quantity(size_code)

    def set_size_quantity(self, size, quantity, check_quantity=True):
        return getattr(self, 'ois_helper').set_size_quantity(size, quantity, check_quantity)

    def get_size_quantitys(self):
        """Количество товара по размерам

        return [
            {
                'size': Size(), 'quantity': 1
            }, ]
        """

        return getattr(self, 'ois_helper').get_size_quantitys()


    def get_sizes(self):
        """Заказанные размеры"""
        return getattr(self, 'ois_helper').get_sizes()


class OrderFromSiteItemSize(models.Model):
    order_item = models.ForeignKey(OrderFromSiteItem, verbose_name='Товар в заказе', on_delete=models.CASCADE)
    size = models.ForeignKey(Size, verbose_name='Размер', on_delete=models.PROTECT)
    quantity = models.PositiveSmallIntegerField('Количество')

    class Meta:
        verbose_name = 'Размер товара в заказе'
        verbose_name_plural = 'Размеры товаров в заказе'
        unique_together = ('order_item', 'size',)

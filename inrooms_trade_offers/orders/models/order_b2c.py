from decimal import Decimal

from django.db import models

from client_cards.models import BuyerCard
from clients.models import Client
from core.utils.constants import ORDER_CATEGORY_CHOICE
from orders.helpers.order_b2c_item_size import (
    OrderB2CItemSizeHelper,
    OrderReturnB2CItemSizeHelper,
    OrderReturnItemSizeHelper,
)
from .defect import DefectAct
from .refund import RefundAct
from payments.models import CurrencyRate
from products.models import SERIES_TYPE_CHOICES, Size
from stores.models import Employee


class OrderB2CAbstract(models.Model):
    number = models.CharField('Номер заказа', max_length=30)
    date = models.DateField('Дата')
    amount_full = models.DecimalField('Полная сумма заказа', default=Decimal('0.0000000000'), max_digits=30, decimal_places=10)
    client = models.ForeignKey(Client, verbose_name='Партнер',
                               on_delete=models.PROTECT)

    class Meta:
        abstract = True

    def __str__(self):
        return '№:{}'.format(self.id)

    def set_amount_full(self):
        amount = Decimal('0.0000000000')
        for item in self.items.all():
            amount += item.get_price_sum()
        self.amount_full = amount
        try:
            currency = CurrencyRate.objects.get(date=self.date, currency=self.client.order_b2c_currency)
        except CurrencyRate.DoesNotExist:
            self.amount_full_rur = self.amount_full
        else:
            self.amount_full_rur = self.amount_full * currency.rate


class OrderB2C(OrderB2CAbstract):
    seller = models.CharField('Продавец', max_length=30, blank=True)
    visits = models.IntegerField('Посещения', default=0)
    amount_full_rur = models.DecimalField('Полная сумма заказа (руб)', default=0, max_digits=30, decimal_places=10)
    amount_before_discount_full = models.DecimalField('Полная сумма заказа (руб) со скидкой', default=0, max_digits=30, decimal_places=10)
    amount_before_discount_full_rur = models.DecimalField('Полная сумма заказа (руб) со скидкой', default=0, max_digits=30,
                                               decimal_places=10)

    class Meta:
        verbose_name = 'Заказ b2c'
        verbose_name_plural = 'Заказы b2c'
        unique_together = ('number', 'date', )
        ordering = ('-date', 'number', 'client')

    def set_amount_full(self):
        amount = Decimal('0.0000000000')
        amount_before_discount = Decimal('0.0000000000')
        for item in self.items.all():
            amount += item.get_price_sum()
            amount_before_discount += item.get_price_before_discount_sum()
        self.amount_full = amount
        self.amount_before_discount_full = amount_before_discount
        try:
            currency = CurrencyRate.objects.get(date=self.date, currency=self.client.order_b2c_currency)
        except CurrencyRate.DoesNotExist:
            self.amount_full_rur = self.amount_full
            self.amount_before_discount_full_rur = self.amount_before_discount_full
            for i in self.items.all():
                i.price_rur = i.price
                i.price_before_discount_rur = i.price_before_discount
                i.save()
        else:
            self.amount_full_rur = self.amount_full * currency.rate
            self.amount_before_discount_full_rur = self.amount_before_discount_full * currency.rate
            for i in self.items.all():
                i.price_rur = i.price * currency.rate
                i.price_before_discount_rur = i.price_before_discount * currency.rate
                i.save()


class OrderReturn(OrderB2CAbstract):
    ORDER_CATEGORY = ORDER_CATEGORY_CHOICE
    seller = models.CharField('Продавец', max_length=30, blank=True)
    defect_act = models.ForeignKey(DefectAct, verbose_name='Акт на брак', blank=True, null=True)
    refund_act = models.ForeignKey(RefundAct, verbose_name='Акт на брак', blank=True, null=True)
    category = models.PositiveSmallIntegerField('Категория', choices=ORDER_CATEGORY, blank=True, null=True)

    class Meta:
        verbose_name = 'Возврат'
        verbose_name_plural = 'Возвраты'
        unique_together = ('number', 'date', )
        ordering = ('-date', 'number', )

    def set_amount_full(self):
        amount = Decimal('0.0000000000')
        for item in self.items.all():
            amount += item.get_price_sum()
        self.amount_full = amount

    def save(self, *args, **kwargs):
        if not self.category:
            if self.defect_act:
                self.category = self.defect_act.category
            elif self.refund_act:
                self.category = self.refund_act.category
        super().save(*args, **kwargs)


class OrderReturnB2C(OrderB2CAbstract):
    seller = models.ForeignKey(Employee, blank=True, null=True, on_delete=models.PROTECT)
    amount_full_rur = models.DecimalField('Полная сумма заказа (руб)', default=0, max_digits=30, decimal_places=10)

    class Meta:
        verbose_name = 'Возврат b2c'
        verbose_name_plural = 'Возвраты b2c'
        unique_together = ('number', 'date', )
        ordering = ('-date', 'number', )


class OrderB2CItemAbstract(models.Model):
    SERIES_TYPE = SERIES_TYPE_CHOICES
    product = models.ForeignKey('products.Product', verbose_name='Товар')

    series_type = models.CharField('Тип серии', max_length=1, choices=SERIES_TYPE)
    price = models.DecimalField('Полная сумма заказа', default=0, max_digits=20, decimal_places=10)
    sort_index = models.PositiveSmallIntegerField('Сортировка', default=0, db_index=True)

    class Meta:
        abstract = True

    def __getattr__(self, name):
        if name.startswith('get_count_'):
            return self.get_size_quantity(name[10:])
        if name == 'ois_helper':
            try:
                return self._ois_helper
            except AttributeError:
                self._ois_helper = self._item_size_helper(order_item=self)
            return self._ois_helper
        return super(OrderB2CItemAbstract, self).__getattr__(name)

    def get_size_quantity(self, size_code):
        """Количество заказанного товара определенного размера (шт)"""
        return getattr(self, 'ois_helper').get_size_quantity(size_code)

    def get_price_sum(self):
        """Стоимость товара"""
        return getattr(self, 'ois_helper').get_price_sum()

    def get_quantity(self):
        """Количество заказанного товара (шт)"""
        return getattr(self, 'ois_helper').get_quantity()


class Cheque1c(models.Model):
    order = models.ForeignKey(OrderB2C, verbose_name='Заказ b2c', related_name="cheques_1c")
    buyer_card = models.ForeignKey(BuyerCard, verbose_name='Карточка клиента', blank=True, null=True)
    number_1c = models.CharField('Номер в 1с', max_length=100, blank=True)
    seller = models.ForeignKey(Employee, blank=True, null=True, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Чек 1с'
        verbose_name_plural = 'Чеки 1с'
        ordering = ('order', 'number_1c')
        unique_together = ('order', 'buyer_card', 'number_1c', 'seller')

    def __str__(self):
        return '{}: order {}'.format(self.number_1c, self.order_id)


class OrderB2CItem(OrderB2CItemAbstract):
    order = models.ForeignKey(OrderB2C, verbose_name='Заказ b2c', related_name="items")
    cheque_1c = models.ForeignKey(Cheque1c, verbose_name='Чек 1с', blank=True, null=True, on_delete=models.SET_NULL, related_name="items")
    price_rur = models.DecimalField('Сумма в рублях', default=0, max_digits=20, decimal_places=10)
    price_before_discount = models.DecimalField('Сумма со скидкой', default=0, max_digits=20, decimal_places=10)
    price_before_discount_rur = models.DecimalField('Сумма со скидкой в рублях', default=0, max_digits=20, decimal_places=10)
    _item_size_helper = OrderB2CItemSizeHelper

    class Meta:
        verbose_name = 'Товар в заказе b2c'
        verbose_name_plural = 'Товары в заказе b2c'
        ordering = ('sort_index', '-id')

    def get_price_before_discount_sum(self):
        """Стоимость товара"""
        return getattr(self, 'ois_helper').get_price_before_discount_sum()


class OrderReturnItem(OrderB2CItemAbstract):
    order = models.ForeignKey(OrderReturn, verbose_name='Возврат', related_name="items")

    _item_size_helper = OrderReturnItemSizeHelper

    class Meta:
        verbose_name = 'Товар в возврате'
        verbose_name_plural = 'Товары в возврате'
        ordering = ('sort_index', '-id')
        unique_together = ('order', 'product', )


class OrderReturnB2CItem(OrderB2CItemAbstract):
    order = models.ForeignKey(OrderReturnB2C, verbose_name='Возврат b2c', related_name="items")

    _item_size_helper = OrderReturnB2CItemSizeHelper

    class Meta:
        verbose_name = 'Товар в возврате b2c'
        verbose_name_plural = 'Товары в возврате b2c'
        ordering = ('sort_index', '-id')
        unique_together = ('order', 'product', )


class OrderB2CItemSizeAbstract(models.Model):
    size = models.ForeignKey(Size, verbose_name='Размер',
                             on_delete=models.PROTECT)
    quantity = models.PositiveSmallIntegerField('Количество')

    class Meta:
        abstract = True


class OrderB2CItemSize(OrderB2CItemSizeAbstract):
    order_item = models.ForeignKey(OrderB2CItem, verbose_name='Товар в заказе',
                                   on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Размер товара в заказе b2c'
        verbose_name_plural = 'Размеры товаров в заказе b2c'
        unique_together = ('order_item', 'size', )


class OrderReturnItemSize(OrderB2CItemSizeAbstract):
    order_item = models.ForeignKey(OrderReturnItem, verbose_name='Товар в возврате',
                                   on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Размер товара в возврате'
        verbose_name_plural = 'Размеры товаров в возврате'
        unique_together = ('order_item', 'size', )


class OrderReturnB2CItemSize(OrderB2CItemSizeAbstract):
    order_item = models.ForeignKey(OrderReturnB2CItem, verbose_name='Товар в возврате',
                                   on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Размер товара в возврате b2c'
        verbose_name_plural = 'Размеры товаров в возврате b2c'
        unique_together = ('order_item', 'size', )

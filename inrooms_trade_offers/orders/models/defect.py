import os
import uuid

from decimal import Decimal
from django.db import models
from django_resized import ResizedImageField
from model_utils.models import TimeStampedModel

from core.utils.constants import ORDER_CATEGORY_CHOICE


class DefectActStatus(models.Model):
    name = models.CharField('Название', max_length=100, unique=True)
    button_name = models.CharField('Название на кнопке', max_length=100, blank=True)
    code = models.CharField('Код', max_length=50, unique=True, db_index=True)

    create_1c_document = models.BooleanField('Отправлять документ в 1с?', default=False)

    next_status = models.ManyToManyField('self', verbose_name=u'Следующие статусы', blank=True, symmetrical=False)
    weight = models.PositiveSmallIntegerField('Вес', default=0)

    is_end_status = models.BooleanField('Является конечным статусом?', default=False)

    class Meta:
        verbose_name = 'Статус акта о браке'
        verbose_name_plural = 'Статусы актов о браке'
        ordering = ('-weight', )

    def __str__(self):
        return self.name


def generate_defect_image_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'defect/fullsize/{}{}'.format(uuid.uuid4(), ext)


def get_default_status():
    return DefectActStatus.objects.get_or_create(code='new', defaults={'name': 'Новый'})[0].id


class DefectAct(TimeStampedModel):
    CATEGORY = ORDER_CATEGORY_CHOICE
    client = models.ForeignKey('clients.Client', verbose_name='Партнер',
                               on_delete=models.PROTECT)
    status = models.ForeignKey(DefectActStatus, verbose_name='Статус', default=get_default_status,
                               on_delete=models.PROTECT)
    comment = models.TextField('Комментарий', blank=True)

    category = models.PositiveSmallIntegerField('Категория', choices=CATEGORY, blank=True, null=True)

    class Meta:
        verbose_name = 'Акт о браке'
        verbose_name_plural = 'Акты о браке'
        ordering = ('-id', )

    def __str__(self):
        return 'id: {}'.format(self.id)

    def process_defect_act(self, create_1c_document=True):
        if self.status.create_1c_document and create_1c_document:
            self.create_1c_document()

    def create_1c_document(self):
        """Создание документа для синхронизации с 1с"""

        from service_1c.utils.defect_act_exporter import DefectActExporter
        document = DefectActExporter(self.id).create_document()
        return document

    def get_amount_full(self):
        amount_full = Decimal('0.00')
        for item in self.items.all():
            amount_full += item.order_item.price
        return amount_full


class DefectActItem(models.Model):
    defect_act = models.ForeignKey(DefectAct, verbose_name='Акт о браке', related_name="items")

    order_item = models.ForeignKey('orders.OrderItem', verbose_name='Товар в бланк заказе',
                                   on_delete=models.PROTECT)
    size = models.ForeignKey('products.Size', verbose_name='Размер',
                             on_delete=models.PROTECT)

    comment = models.TextField('Комментарий', blank=True)

    class Meta:
        verbose_name = 'Элемент акта о браке'
        verbose_name_plural = 'Элементы акта о браке'
        ordering = ('-id', )

    @property
    def main_im(self):
        try:
            return self.images.all()[0].image
        except IndexError:
            return None


class DefectImage(TimeStampedModel):
    image = ResizedImageField('Изображение', upload_to=generate_defect_image_filename, max_length=755, blank=True)
    defect_act_item = models.ForeignKey(DefectActItem, related_name='images', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Изображение акта на возврат'
        verbose_name_plural = 'Изображения актов на возврат'
        ordering = ['-id', ]

    def __str__(self):
        return 'id: {} - defect_act_item_id: {}'.format(self.pk, self.defect_act_item_id)

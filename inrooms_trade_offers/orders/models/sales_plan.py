from decimal import Decimal
from django.db import models
from model_utils.models import TimeStampedModel


class SalesPlan(TimeStampedModel):
    store = models.ForeignKey('stores.Store', verbose_name='Магазин')
    year = models.PositiveSmallIntegerField('Год')
    month = models.PositiveSmallIntegerField('Месяц')
    plan = models.IntegerField('План')
    week1 = models.DecimalField('Неделя 1', max_digits=5, decimal_places=2, default=Decimal('0.00'))
    week2 = models.DecimalField('Неделя 2', max_digits=5, decimal_places=2, default=Decimal('0.00'))
    week3 = models.DecimalField('Неделя 3', max_digits=5, decimal_places=2, default=Decimal('0.00'))
    week4 = models.DecimalField('Неделя 4', max_digits=5, decimal_places=2, default=Decimal('0.00'))
    week5 = models.DecimalField('Неделя 5', max_digits=5, decimal_places=2, default=Decimal('0.00'))
    week6 = models.DecimalField('Неделя 6', max_digits=5, decimal_places=2, default=Decimal('0.00'))
    day1 = models.DecimalField('Понедельник', max_digits=5, decimal_places=2, default=Decimal('0.00'))
    day2 = models.DecimalField('Вторник', max_digits=5, decimal_places=2, default=Decimal('0.00'))
    day3 = models.DecimalField('Среда', max_digits=5, decimal_places=2, default=Decimal('0.00'))
    day4 = models.DecimalField('Четверг', max_digits=5, decimal_places=2, default=Decimal('0.00'))
    day5 = models.DecimalField('Пятница', max_digits=5, decimal_places=2, default=Decimal('0.00'))
    day6 = models.DecimalField('Суббота', max_digits=5, decimal_places=2, default=Decimal('0.00'))
    day7 = models.DecimalField('Воскресенье', max_digits=5, decimal_places=2, default=Decimal('0.00'))

    class Meta:
        verbose_name = 'План продаж'
        verbose_name_plural = 'Планы продаж'
        unique_together = ('store', 'year', 'month')
        ordering = ('store', '-year', '-month')

    def __str__(self):
        return '{}.{} {}'.format(self.month, self.year, self.store)

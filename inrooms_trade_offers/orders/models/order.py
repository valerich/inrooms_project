import datetime
import math

from config.celery import app
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import ModificationDateTimeField
from django_extensions.db.models import TimeStampedModel
from model_utils.choices import Choices

from core.models import CURRENCY_CHOICES
from core.utils.constants import ORDER_CATEGORY_CHOICE
from payments.models import CurrencyRate
from products.models import SERIES_TYPE_CHOICES, Size
from warehouses.models import Warehouse
from ..helpers import OrderHelper, OrderItemSizeHelper


class OrderStatus(models.Model):
    name = models.CharField('Название', max_length=100, unique=True)
    button_name = models.CharField('Название на кнопке', max_length=100, blank=True)
    code = models.CharField('Код', max_length=20, unique=True, db_index=True)

    create_1c_document = models.BooleanField('Отправлять документ в 1с?', default=False)
    create_price_set_document = models.BooleanField('Создаем документ установки цен?', default=False)

    next_status = models.ManyToManyField('self', verbose_name=u'Следующие статусы', blank=True, symmetrical=False)
    weight = models.PositiveSmallIntegerField('Вес', default=0)

    has_local_reserve = models.BooleanField('Учитываются ли локальные резервы?', default=False)

    send_email_message = models.BooleanField('Отправлять email сообщения при переходе в данный статус?', default=False)
    email_template = models.ForeignKey('sms.MessageTemplate', verbose_name='Шаблон сообщения', blank=True, null=True)

    class Meta:
        verbose_name = 'Статус'
        verbose_name_plural = 'Статусы'
        ordering = ('-weight', )

    def __str__(self):
        return self.name


def get_default_status():
    return OrderStatus.objects.get_or_create(code='new', defaults={'name': 'Новый'})[0].id


ORDER_KIND_CHOICE = Choices(
    (1, 'main', 'Основной'),
    (2, 'defect', 'По обмену брака'),
    (3, 'ten_percent', 'По обмену 10%'),
)


class Order(models.Model):
    KIND = ORDER_KIND_CHOICE
    kind = models.PositiveSmallIntegerField('Тип', choices=KIND, default=KIND.main)
    created = models.DateTimeField(_('created'), default=timezone.now)
    torg_12_category_1 = models.CharField('Номер ТОРГ-12 категория 1', max_length=50, blank=True)
    torg_12_category_2 = models.CharField('Номер ТОРГ-12 категория 2', max_length=50, blank=True)
    modified = ModificationDateTimeField(_('modified'))
    status = models.ForeignKey(OrderStatus, verbose_name='Статус', default=get_default_status,
                               on_delete=models.PROTECT)
    client = models.ForeignKey('clients.Client', verbose_name='Партнер', on_delete=models.PROTECT)
    comment = models.TextField('Комментарий', blank=True)
    series_count = models.PositiveSmallIntegerField('Количество серий', default=0)
    amount_full = models.DecimalField('Полная стоимость', default=0, max_digits=10, decimal_places=2)

    creator = models.ForeignKey('accounts.User', verbose_name='Создатель', related_name='order_creator',
                                on_delete=models.PROTECT)
    date_shipped = models.DateTimeField('Время отправки', blank=True, null=True)
    date_recived = models.DateTimeField('Время получения', blank=True, null=True)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ('-created', )

    def __str__(self):
        return '№:{}'.format(self.id)

    def get_quantity(self):
        quantity = 0
        for item in self.items.all():
            quantity += item.get_quantity()
        return quantity

    def set_amount_full(self):
        amount = 0
        for item in self.items.all():
            amount += item.get_price_sum()
        self.amount_full = amount

    def get_amount_full(self):
        if self.status.code == 'new':
            self.set_amount_full()
            self.save()
        return self.amount_full

    def create_1c_document(self):
        """Создание документа для синхронизации с 1с"""

        from service_1c.utils.order_exporter import OrderExporter
        document = OrderExporter(self.id).create_document()
        return document

    def create_invoice(self, provider, user=None):
        order_helper = OrderHelper(order=self, user=user)
        return order_helper.create_invoice_document(provider=provider)

    def create_price_set_document(self, user):
        """Создание документа установки цен"""

        order_helper = OrderHelper(order=self, user=user)
        return order_helper.create_price_set_document()

    def set_local_reserves(self, is_local_reserve: bool):
        """Выставляем флаги локального резерва.

        В статусе "новый" не нужно учитывать локальные резервы заказа. Это логика работы компании -
          набрать в заказ всё что можно, а потом продать только то, что есть
        В статусе "резерв создан" - учитываем
        В статусе "заказ отправлен" - учитываем
        В статусе "готов к отгрузке" - перестаем учитывать т.к. 1с уже знает о них
        """

        OrderItemSize.objects.filter(order_item__order=self).update(is_local_reserve=is_local_reserve)

    def process_order(self, create_1c_document=True, create_price_set_document=True, user=None):
        if self.status.create_1c_document and create_1c_document:
            self.sort_items()
            self.create_1c_document()
        if self.status.create_price_set_document and create_price_set_document:
            self.create_price_set_document(user)
        self.set_local_reserves(self.status.has_local_reserve)
        if self.status.send_email_message:
            self.send_client_email()
        if self.status.code == 'shipped' and not self.date_shipped:
            self.date_shipped = timezone.now()
        self.set_amount_full()
        self.save()

    def send_client_email(self):
        if self.status.email_template and self.client.email and any([self.torg_12_category_1, self.torg_12_category_2]):
            app.send_task('sms.create_template_message', args=(
                self.status.email_template_id,
                self.client.email,
                {
                    'client_head': self.client.contact_person,
                    'email_date': timezone.now().strftime('%d.%m.%Y'),
                    'order_id': self.id,
                    'order_created': self.created.strftime('%d.%m.%Y'),
                    'client_contract_number': self.client.contract_number,
                    'client_contract_date': self.client.contract_date.strftime('%d.%m.%Y') if self.client.contract_date else '',
                    'torg_12_category_1': self.torg_12_category_1,
                    'torg_12_category_2': self.torg_12_category_2,
                    'client_name': self.client.name,
                }
            ))

    def check_reserves(self):
        """Проверка на корректность локальных резервов"""

        order_helper = OrderHelper(order=self)
        return order_helper.check_reserves()

    def re_reserve(self):
        order_helper = OrderHelper(order=self)
        order_helper.re_reserve()

    def sort_items(self):
        order_helper = OrderHelper(order=self)
        return order_helper.sort_items()


class OrderItem(models.Model):
    SERIES_TYPE = SERIES_TYPE_CHOICES
    order = models.ForeignKey(Order, verbose_name='Заказ', related_name="items")
    product = models.ForeignKey('products.Product', verbose_name='Товар')

    series_type = models.CharField('Тип серии', max_length=1, choices=SERIES_TYPE)
    price = models.DecimalField('Цена', max_digits=10, decimal_places=2)
    recommended_retail_price = models.DecimalField('Рекомендованная розничная цена', max_digits=10, decimal_places=2, default='0.00')
    quantity = models.PositiveSmallIntegerField('Количество', default=1)

    sort_index = models.PositiveSmallIntegerField('Сортировка', default=0, db_index=True)

    creator = models.ForeignKey('accounts.User', verbose_name='Создатель', blank=True, null=True)

    class Meta:
        verbose_name = 'Товар в заказе'
        verbose_name_plural = 'Товары в заказе'
        ordering = ('sort_index', '-id')
        unique_together = ('order', 'product', )

    def __str__(self):
        return "id: {}, order_id: {}, product_id: {}, price: {}".format(self.id, self.order_id, self.product_id, self.price, )

    def __getattr__(self, name):
        if name.startswith('get_count_'):
            return self.get_size_quantity(name[10:])
        if name == 'ois_helper':
            try:
                return self._ois_helper
            except AttributeError:
                self._ois_helper = OrderItemSizeHelper(order_item=self)
            return self._ois_helper
        return super().__getattribute__(name)

    def __setattr__(self, name, value):
        if name.startswith('get_count_'):
            self.set_size_quantity(name[10:], value)
        super().__setattr__(name, value)

    def get_size_quantity(self, size_code):
        """Количество заказанного товара определенного размера (шт)"""
        return getattr(self, 'ois_helper').get_size_quantity(size_code)

    def set_size_quantity(self, size, quantity, check_quantity=True):
        return getattr(self, 'ois_helper').set_size_quantity(size, quantity, check_quantity)

    def set_size_quantity_with_multiplier(self, size, series_count=None):
        return getattr(self, 'ois_helper').set_size_quantity_with_multiplier(size, series_count)

    def get_size_quantitys(self):
        """Количество товара по размерам

        return [
            {
                'size': Size(),
                'quantity': 1
            },
        ]
        """

        return getattr(self, 'ois_helper').get_size_quantitys()

    def get_quantity(self, category=None):
        """Количество заказанного товара (шт)"""
        return getattr(self, 'ois_helper').get_quantity(category=category)

    def get_price_sum(self, category=None):
        """Стоимость товара"""
        return getattr(self, 'ois_helper').get_price_sum(category=category)

    def get_sizes(self):
        """Заказанные размеры"""
        return getattr(self, 'ois_helper').get_sizes()

    def get_max_series_count(self):
        return getattr(self, 'ois_helper').get_max_series_count()

    def set_recommended_retail_price(self):
        """Расчет рекомендованной розничной цены

        issue #348 (Переписка с Денисом в телеграме 14.11.2016 15:40)

        Если валюта партнера - рубли, то
        self.price * 2 + 1

        Если валюта партнера не рубли, то
        Цену отгрузки умножаем на курс,
        умножаем на два,
        округляем вверх до сотни и
        отнимаем единицу

        Изменения 13.12.2016
        Если валюта не рубли, то
        Цену отгрузки делим на курс,
        умножаем на два,
        округляем вверх до сотни и
        отнимаем единицу
        """

        currency = self.order.client.order_b2c_currency
        if currency == CURRENCY_CHOICES.RUR:
            rrprice = self.price * 2 + 1
        else:
            try:
                currency = CurrencyRate.objects.get(date=datetime.date.today(), currency=self.order.client.order_b2c_currency).rate
            except CurrencyRate.DoesNotExist:
                currency = 1
            rrprice = (self.price / currency) * 2
            rrprice = math.ceil(rrprice / 100) * 100
            if rrprice:
                rrprice -= 1
        self.recommended_retail_price = rrprice


class OrderItemSize(models.Model):
    ORDER_CATEGORY = ORDER_CATEGORY_CHOICE
    order_item = models.ForeignKey(OrderItem, verbose_name='Товар в заказе',
                                   on_delete=models.CASCADE)
    size = models.ForeignKey(Size, verbose_name='Размер',
                             on_delete=models.PROTECT)
    warehouse = models.ForeignKey(Warehouse, verbose_name='Склад',
                                  on_delete=models.PROTECT)
    category = models.PositiveSmallIntegerField('Категория', choices=ORDER_CATEGORY, default=ORDER_CATEGORY.white)
    quantity = models.PositiveSmallIntegerField('Количество')
    is_local_reserve = models.BooleanField('Локальный резерв', default=False, db_index=True)
    is_delivery_reserve = models.BooleanField('Резерв в пути', default=False, db_index=True)

    class Meta:
        verbose_name = 'Размер товара в заказе'
        verbose_name_plural = 'Размеры товаров в заказе'
        unique_together = ('order_item', 'size', 'warehouse', 'category', 'is_delivery_reserve')


class ScannerData(TimeStampedModel):
    is_ordered = models.BooleanField('Заказано', default=False)
    products = models.ManyToManyField('products.Product', verbose_name='Товары')

    class Meta:
        verbose_name = 'Данные со сканера'
        verbose_name_plural = 'Данные со сканера'
        ordering = ('-id', )


def create_order_fields(sender, instance, **kwargs):
    if not instance.id:
        if instance.client:
            instance.series_count = instance.client.series_count


models.signals.pre_save.connect(create_order_fields, sender=Order)


def order_item_set_fields_by_product(sender, instance, **kwargs):
    if instance.product_id:
        if not instance.series_type:
            instance.series_type = instance.product.main_collection.series_type
        if instance.price is None:
            if instance.product.price:
                instance.price = instance.product.price
            else:
                instance.price = 0
            instance.set_recommended_retail_price()


models.signals.pre_save.connect(order_item_set_fields_by_product, sender=OrderItem)
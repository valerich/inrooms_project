from django.db import models

from model_utils.models import TimeStampedModel

from core.utils.constants import ORDER_CATEGORY_CHOICE


class RefundActStatus(models.Model):
    name = models.CharField('Название', max_length=100, unique=True)
    button_name = models.CharField('Название на кнопке', max_length=100, blank=True)
    code = models.CharField('Код', max_length=20, unique=True, db_index=True)

    create_1c_document = models.BooleanField('Отправлять документ в 1с?', default=False)

    next_status = models.ManyToManyField('self', verbose_name=u'Следующие статусы', blank=True, symmetrical=False)
    weight = models.PositiveSmallIntegerField('Вес', default=0)

    class Meta:
        verbose_name = 'Статус акта на возврат'
        verbose_name_plural = 'Статусы актов на возврат'
        ordering = ('-weight', )

    def __str__(self):
        return self.name


def get_default_status():
    return RefundActStatus.objects.get_or_create(code='new', defaults={'name': 'Новый'})[0].id


class RefundAct(TimeStampedModel):
    CATEGORY = ORDER_CATEGORY_CHOICE
    client = models.ForeignKey('clients.Client', verbose_name='Партнер',
                               on_delete=models.PROTECT)
    status = models.ForeignKey(RefundActStatus, verbose_name='Статус', default=get_default_status,
                               on_delete=models.PROTECT)

    category = models.PositiveSmallIntegerField('Категория', choices=CATEGORY, blank=True, null=True)

    comment = models.TextField('Комментарий', blank=True)

    class Meta:
        verbose_name = 'Акт на возврат'
        verbose_name_plural = 'Акты на возврат'
        ordering = ('-id', )

    def __str__(self):
        return 'id: {}'.format(self.id)

    def process_refund_act(self, create_1c_document=True):
        if self.status.create_1c_document and create_1c_document:
            self.create_1c_document()

    def create_1c_document(self):
        """Создание документа для синхронизации с 1с"""

        from service_1c.utils.refund_act_exporter import RefundActExporter
        document = RefundActExporter(self.id).create_document()
        return document

    def get_amount_full(self):
        amount = 0
        for i in self.items.all():
            amount += i.quantity * i.order_item.price
        return amount


class RefundActItem(models.Model):
    refund_act = models.ForeignKey(RefundAct, verbose_name='Акт на возврат', related_name="items")

    order_item = models.ForeignKey('orders.OrderItem', verbose_name='Товар в бланк заказе',
                                   on_delete=models.PROTECT)
    size = models.ForeignKey('products.Size', verbose_name='Размер',
                             on_delete=models.PROTECT)
    quantity = models.PositiveSmallIntegerField('Количество', default=1)

    comment = models.TextField('Комментарий', blank=True)

    class Meta:
        verbose_name = 'Элемент акта на возврат'
        verbose_name_plural = 'Элементы акта на возврат'
        ordering = ('-id', )
        unique_together = ('refund_act', 'order_item', 'size')

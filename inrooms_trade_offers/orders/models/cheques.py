from decimal import Decimal

from django.contrib.postgres.fields.jsonb import JSONField
from django.db import models
from django.utils import timezone
from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

from client_cards.models import BuyerCard
from products.models import SERIES_TYPE_CHOICES, Size
from stores.models import Employee


CHEQUE_KIND_CHOICES = Choices(
    ('sale', 'Продажа'),
    ('cancel', 'Отмена'),
)


class Cheque(TimeStampedModel):
    KIND = CHEQUE_KIND_CHOICES
    kind = models.CharField('Тип чека', choices=KIND, default=KIND.sale, max_length=10)
    date = models.DateTimeField('Дата продажи', default=timezone.now)
    client = models.ForeignKey('clients.Client', verbose_name='Партнер', on_delete=models.PROTECT)
    buyer_card = models.ForeignKey(BuyerCard, verbose_name='Карточка клиента', blank=True, null=True)
    number_1c = models.CharField('Номер в 1с', max_length=100, blank=True)
    seller = models.ForeignKey(Employee, blank=True, null=True, on_delete=models.PROTECT)
    data_1c = JSONField('Данные', blank=True, null=True)

    class Meta:
        verbose_name = 'Чек 1с'
        verbose_name_plural = 'Чеки 1с'
        ordering = ('-date', )
        unique_together = ('buyer_card', 'number_1c', 'seller')

    def get_price_sum(self):
        price_sum = Decimal("0.00")
        for i in self.items.all():
            price_sum += i.get_price_sum()
        return price_sum


class ChequeItem(models.Model):
    product = models.ForeignKey('products.Product', verbose_name='Товар')

    series_type = models.CharField('Тип серии', max_length=1, choices=SERIES_TYPE_CHOICES)
    price_sum = models.DecimalField('Сумма с учетом скидки', default=0, max_digits=20, decimal_places=10)
    price = models.DecimalField('Цена (шт)', default=0, max_digits=20, decimal_places=10)
    price_discount = models.DecimalField('Цена с учетом скидки', default=0, max_digits=20, decimal_places=10)
    sort_index = models.PositiveSmallIntegerField('Сортировка', default=0, db_index=True)

    cheque = models.ForeignKey(Cheque, verbose_name='Чек 1с', related_name="items")
    # _item_size_helper = ChequeItemSizeHelper

    class Meta:
        verbose_name = 'Товар в чеке'
        verbose_name_plural = 'Товары в чеках'
        ordering = ('sort_index', '-id')

    def get_price_sum(self):
        """Стоимость товара"""
        if self.price_sum:
            return self.price_sum
        else:
            return self.price * self.get_quantity()

    def get_quantity(self):
        """Количество заказанного товара (шт)"""
        quantity = 0
        for si in self.sizes.all():
            quantity += si.quantity
        return quantity


class ChequeItemSize(models.Model):
    cheque_item = models.ForeignKey(ChequeItem, verbose_name='Товар в чеке',
                                    on_delete=models.CASCADE, related_name='sizes')
    size = models.ForeignKey(Size, verbose_name='Размер',
                             on_delete=models.PROTECT)
    quantity = models.PositiveSmallIntegerField('Количество')

    class Meta:
        verbose_name = 'Размер товара в чеке'
        verbose_name_plural = 'Размеры товаров в чеках'
        unique_together = ('cheque_item', 'size', )

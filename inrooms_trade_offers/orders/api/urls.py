from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import (
    ChequeViewSet,
    ChequeDayReportView,
    DefectActItemViewSet,
    DefectActViewSet,
    DefectActStatusViewSet,
    DefectProductViewSet,
    OrderB2CCheque1cViewSet,
    OrderB2CCheque1cAllViewSet,
    OrderB2CItemAllViewSet,
    OrderB2CItemViewSet,
    OrderB2CViewSet,
    OrderItemViewSet,
    OrderReturnB2CItemViewSet,
    OrderReturnB2CViewSet,
    OrderReturnItemViewSet,
    OrderReturnViewSet,
    OrderFromSiteViewSet,
    OrderFromSiteItemViewSet,
    OrderStatusViewSet,
    OrderViewSet,
    ProductSupplyViewSet,
    ProductViewSet,
    RefundActItemViewSet,
    RefundActViewSet,
    RefundActStatusViewSet,
    RefundProductViewSet,
    ScannerDataViewSet,
    SalesPlanViewSet,
    ReportEmployeeSale,
    ReportSalesPlan,
    ReportSalesPlanProject,
)

router = DefaultRouter()
router.register(r'cheque', ChequeViewSet, base_name='cheque')
router.register(r'defect_act/(?P<defect_pk>\d+)/product_search', DefectProductViewSet, base_name='defect_products')
router.register(r'defect_act/(?P<defect_pk>\d+)/items', DefectActItemViewSet, base_name='defect_act_items'),
router.register(r'defect_act', DefectActViewSet, base_name='defect_act')
router.register(r'order_b2c/items', OrderB2CItemAllViewSet, base_name='order_b2c_item_all'),
router.register(r'order_b2c/cheque_1c', OrderB2CCheque1cAllViewSet, base_name='order_b2c_cheque_1c_all'),
router.register(r'order', OrderViewSet, base_name='order')
router.register(r'order_b2c', OrderB2CViewSet, base_name='order_b2c')
router.register(r'order_return_b2c', OrderReturnB2CViewSet, base_name='order_return_b2c')
router.register(r'order_return', OrderReturnViewSet, base_name='order_return')
router.register(r'order_from_site', OrderFromSiteViewSet, base_name='order_from_site'),
router.register(r'order_from_site/(?P<order_pk>\d+)/items', OrderFromSiteItemViewSet, base_name='order_from_site_items'),
router.register(r'order/(?P<order_pk>\d+)/items', OrderItemViewSet, base_name='order_items'),
router.register(r'order_b2c/(?P<order_pk>\d+)/cheque_1c', OrderB2CCheque1cViewSet, base_name='order_b2c_cheque_1c'),
router.register(r'order_b2c/(?P<order_pk>\d+)/items', OrderB2CItemViewSet, base_name='order_b2c_items'),
router.register(r'order_return_b2c/(?P<order_pk>\d+)/items', OrderReturnB2CItemViewSet, base_name='order_return_b2c_items'),
router.register(r'order_return/(?P<order_pk>\d+)/items', OrderReturnItemViewSet, base_name='order_return_items'),
router.register(r'order/(?P<order_pk>\d+)/product_search', ProductViewSet, base_name='product_search'),
router.register(r'order/(?P<order_pk>\d+)/product_supply_search', ProductSupplyViewSet, base_name='product_supply_search'),
router.register(r'scanner_data', ScannerDataViewSet, base_name='scanner_data')
router.register(r'status', OrderStatusViewSet, base_name='order_status')
router.register(r'defect_act_status', DefectActStatusViewSet, base_name='defect_act_status')
router.register(r'refund_act/(?P<refund_pk>\d+)/product_search', RefundProductViewSet, base_name='defect_products')
router.register(r'refund_act/(?P<refund_pk>\d+)/items', RefundActItemViewSet, base_name='refund_act_items'),
router.register(r'refund_act', RefundActViewSet, base_name='refund_act')
router.register(r'refund_act_status', RefundActStatusViewSet, base_name='refund_act_status')
router.register(r'sales_plan/(?P<store_pk>\d+)', SalesPlanViewSet, base_name='sales_plan')


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
    urls.url(r'^reports/cheque_day_report/$', ChequeDayReportView.as_view(), name='report_cheque_day_report'),
    urls.url(r'^reports/employee_sale/$', ReportEmployeeSale.as_view(), name='report_employee_sale'),
    urls.url(r'^reports/sales_plan/$', ReportSalesPlan.as_view(), name='report_sales_plan'),
    urls.url(r'^reports/sales_plan_project/$', ReportSalesPlanProject.as_view(), name='report_sales_plan_project'),
]

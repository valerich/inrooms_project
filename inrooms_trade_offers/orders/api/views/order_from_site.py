import django_filters
from django.db.models.aggregates import Sum
from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from core.utils.views.additional_data_pagination import AdditionalDataPaginatedViewMixin
from ..serializers import OrderFromSiteSerializer
from ...models import OrderFromSite


def filter_has_defect_act(queryset, value):
    if value:
        queryset = queryset.filter(defect_act__isnull=False)
    else:
        queryset = queryset.filter(defect_act__isnull=True)
    return queryset


def filter_has_refund_act(queryset, value):
    if value:
        queryset = queryset.filter(refund_act__isnull=False)
    else:
        queryset = queryset.filter(refund_act__isnull=True)
    return queryset


class OrderFromSiteFilterSet(django_filters.FilterSet):
    class Meta:
        model = OrderFromSite
        fields = [
            'items__product',
            'client',
        ]


class OrderFromSiteViewSet(AdditionalDataPaginatedViewMixin,
                           mixins.RetrieveModelMixin,
                           mixins.ListModelMixin,
                           GenericViewSet):
    serializer_class = OrderFromSiteSerializer
    model = OrderFromSite
    filter_class = OrderFromSiteFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', '^items__product__article', 'items__product__name')
    ordering_fields = (
        'id',
        'client__name',
    )

    def get_queryset(self):
        qs = OrderFromSite.objects.all()
        if self.request.user.has_perm('auth.can_order_from_site_view'):
            print('U has can_order_from_site_view permission')
            return qs
        else:
            return qs.none()


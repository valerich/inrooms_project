import operator
from collections import defaultdict
from functools import reduce

import django_filters
from decimal import Decimal

from django.contrib.auth.models import Permission
from django.db import transaction
from django.db.models.aggregates import Sum
from django.db.models.query_utils import Q
from django.utils import timezone
from rest_framework import filters, status
from rest_framework.decorators import detail_route, list_route
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from clients.models import Client
from core.utils.constants import ORDER_CATEGORY_CHOICE
from history.models import HistoryItem
from orders.api.serializers.order import OrderCreateInvoiceSerializer
from orders.helpers.free_dr_summ import get_free_dr_summ
from orders.models.defect import DefectActItem
from products.api.serializers import ProductSerializer
from products.api.serializers.size import SizeSerializer
from warehouses.api.serializers import WarehouseSerializer
from ..serializers import OrderSerializer
from ...helpers import CategoryHelper
from ...models import Order, OrderItemSize, OrderStatus, OrderReturnItemSize


def filter_exclude_client_pp(queryset, value):
    if value:
        queryset = queryset.exclude(client__legal_form=3)
    return queryset


class OrderFilterSet(django_filters.FilterSet):
    exclude_client_pp = django_filters.BooleanFilter(action=filter_exclude_client_pp)

    class Meta:
        model = Order
        fields = [
            'client',
            'creator',
            'items__product',
            'status',
            'created',
            'modified',
            'exclude_client_pp',
            'kind',
        ]


class OrderViewSet(ModelViewSet):
    serializer_class = OrderSerializer
    model = Order
    filter_class = OrderFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', '^items__product__article', 'items__product__name')
    ordering_fields = (
        'id',
        'created',
        'modified',
        'client__name',
    )

    def perform_create(self, serializer):
        kwargs = {
            'creator': self.request.user
        }
        if self.request.user.has_perm('auth.can_set_client_on_order_create'):
            if not serializer.validated_data.get('client', None):
                if self.request.user.client:
                    kwargs['client'] = self.request.user.client
                else:
                    raise ValidationError({'client': ['Обязательное поле']})
        elif not self.request.user.client:
            raise ValidationError({'client': ['Обязательное поле']})
        else:
            kwargs['client'] = self.request.user.client

        serializer.save(**kwargs)

        history_item = HistoryItem.objects.create(
            kind=HistoryItem.KIND.status_change,
            content_object=serializer.instance,
            body='Пользователь {} {} создал бланк-заказ №{} в статусе "{}"'.format(
                self.request.user.name,
                timezone.now().strftime('%d.%m.%Y %H:%M'),
                serializer.instance.id,
                serializer.instance.status.name
            ),
        )

    def perform_update(self, serializer):
        obj = serializer.instance
        self.old_obj = Order.objects.get(pk=obj.pk)

        super(OrderViewSet, self).perform_update(serializer)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.status.code == 'new':
            self.perform_destroy(instance)
            self.order.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(data={"errors": 'Отгрузки можно удалять только в статусе "Новая"'},
                            status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        qs = Order.objects.all()

        q_list = []

        for kind, kind_perm_prefix in (
            (1, ''),
            (2, 'defect_'),
            (3, 'ten_percent_'),
        ):
            psw = 'auth.can_order_{}edit_'.format(kind_perm_prefix)
            status_codes = [
                perm[len(psw):] for perm in self.request.user.get_all_permissions() if perm.startswith(psw)]
            q_list.append(Q(kind=kind, status__code__in=status_codes))

        qs = qs.filter(reduce(operator.or_, q_list))

        if self.action == 'list':
            qs = qs.select_related(
                'client',
                'status',
            )
            qs = qs.prefetch_related(
                'status__next_status',
                'items',
            )
        qs = qs.prefetch_related(
            'items__product',
            'items__product__main_collection',
            'items__product__main_collection__parent',
        )
        if not self.request.user.has_perm('auth.can_all_orders_view'):
            if self.request.user.client:
                qs = qs.filter(client=self.request.user.client)
            else:
                qs = qs.none()
        return qs

    @detail_route(methods=['get'])
    def category(self, request, pk=None):
        data = []
        for category_id, category_name in ORDER_CATEGORY_CHOICE:
            price_sum = 0
            for order_item_data in OrderItemSize.objects.filter(
                        order_item__order_id=pk,
                        category=category_id
                    ).values('order_item__price').annotate(Sum('quantity')):
                if order_item_data['order_item__price'] and order_item_data['quantity__sum']:
                    price_sum += order_item_data['order_item__price'] * order_item_data['quantity__sum']
            data.append({
                'name': 'Категория {}'.format(category_id, ),
                'price_sum': price_sum
            })
        return Response(data)

    @detail_route(methods=['post'])
    def set_category(self, request, pk=None):
        category_helper = CategoryHelper(pk)
        category_helper.category_sum_distribution(category=ORDER_CATEGORY_CHOICE.white,
                                                  max_sum=Decimal(request.data['category_1_max_sum']))
        return Response(status=status.HTTP_200_OK)

    @detail_route(methods=['post'])
    def create_invoice(self, request, pk=None):
        serializer = OrderCreateInvoiceSerializer(data=request.POST)
        serializer.is_valid(True)
        obj = self.get_object()
        obj.create_invoice(provider=serializer.validated_data['provider'], user=request.user)
        return Response(status=status.HTTP_200_OK)

    @detail_route(methods=['post'])
    @transaction.atomic()
    def change_status(self, request, pk=None):
        """Изменение статуса заказа

        Формат запроса: {'status_code': 'new',
                         're_reserve_confirmed': true}
        Параметры:
            status_code: (обязательный) новый статус заказа
            re_reserve_confirmed: (не обязательный) подтверждение того, что необходимо перерезервировать заказ
              с учетом текущих локальных резервов

        В случае, если устанавливаем заказу статус "reserved", будет проведена проверка на корректность резервов.
        Если каких-то остатков не хватает, то будет возвращен ответ 403 и описание проблемных резервов
        Если необходимо перерезервировать товары, то передаем флаг re_reserve_confirmed: true и в заказе останутся
        только те резервы, которые реально есть на складе Москвы и шоурума
        """
        order = self.get_object()
        status_code = request.data.get('code', None)
        if status_code:
            try:
                new_status = OrderStatus.objects.get(code=status_code)
            except OrderStatus.DoesNotExist:
                pass
            else:
                psw = 'auth.can_order_set_'
                approved_status_codes = [
                    perm[len(psw):] for perm in request.user.get_all_permissions() if perm.startswith(psw)]
                if status_code in approved_status_codes:
                    old_status_name = order.status.name
                    if status_code == 'reserved':
                        re_reserve_confirmed = request.data.get('re_reserve_confirmed', False)
                        if re_reserve_confirmed:
                            order.re_reserve()
                        else:
                            is_reserves_ok, error_data = self._check_order_reserves(order)
                            if not is_reserves_ok:
                                return Response({'error': 'Проблемы с резервами',
                                                 'error_code': 'reserve_fail',
                                                 'data': error_data},
                                                status=status.HTTP_400_BAD_REQUEST)
                    order.status = new_status
                    order.save()

                    history_item = HistoryItem.objects.create(
                        kind=HistoryItem.KIND.status_change,
                        content_object=order,
                        body='Пользователь {} {} изменил статус бланк-заказа №{} с "{}" на "{}"'.format(
                            request.user.name,
                            timezone.now().strftime('%d.%m.%Y %H:%M'),
                            order.id,
                            old_status_name,
                            new_status.name
                        ),
                    )

                    order.process_order(user=self.request.user)
                    return Response(status=status.HTTP_200_OK)
        return Response({'error': 'Не верный статус'},
                        status=status.HTTP_400_BAD_REQUEST)

    def _check_order_reserves(self, order):
        is_reserves_ok, errors = order.check_reserves()
        error_data = []
        for item in errors:
            error_data.append({
                'product': ProductSerializer(item['product'], fields=['id', 'name', 'article']).data,
                'warehouse': WarehouseSerializer(item['warehouse'], fields=['id', 'name', 'code']).data,
                'size': SizeSerializer(item['size'], fields=['id', 'code', 'name']).data,
                'remain': item['remain'],
                'reserve': item['reserve'],
            })
        return is_reserves_ok, error_data

    @list_route()
    def summary(self, request):
        data = {}
        qs = Order.objects.all()
        if not request.user.has_perm('auth.can_all_orders_view'):
            if request.user.client:
                qs = qs.filter(client=request.user.client)
            else:
                qs = qs.none()
        aggreement_qs = qs.filter(status__code='client_agreement')
        data['aggreement_to_work'] = aggreement_qs.count()
        return Response(data)

    @list_route()
    def free_dr_summ(self, request):
        filter_client_id = int(request.GET.get('client', 0))
        order_kind = int(request.GET['order_kind'])

        lookup = {
            'user': request.user,
            'order_kind': order_kind
        }

        if filter_client_id:
            lookup['client_id'] = filter_client_id

        free_dr_summ_data = get_free_dr_summ(**lookup)

        data = []
        for client_id, client_data in free_dr_summ_data.items():

            if client_data['free_summ'] or request.user.client_id == client_id or filter_client_id == client_id:
                data.append({
                    'client': {
                        'id': client_id,
                        'name': client_data['name'],
                    },
                    'free_summ': client_data['free_summ']
                })
        return Response(data)

    @list_route()
    def free_dr_summ_detail(self, request):
        filter_client_id = int(request.GET.get('client', 0))
        order_kind = int(request.GET['order_kind'])

        lookup = {
            'user': request.user,
            'order_kind': order_kind
        }

        if filter_client_id:
            lookup['client_id'] = filter_client_id

        lookup['is_detail'] = True

        free_dr_summ_data = get_free_dr_summ(**lookup)

        if filter_client_id:
            data = free_dr_summ_data[filter_client_id]
        else:
            data = []
            for client_id, client_data in free_dr_summ_data.items():

                if client_data['orders'] or client_data['order_returns'] or request.user.client_id == client_id or filter_client_id == client_id:
                    data.append({
                        'client': {
                            'id': client_id,
                            'name': client_data['name'],
                        },
                        'orders': client_data['orders'],
                        'order_returns': client_data['order_returns'],
                        'orders_sum': client_data['orders_sum'],
                        'order_returns_sum': client_data['order_returns_sum'],
                    })
        return Response(data)

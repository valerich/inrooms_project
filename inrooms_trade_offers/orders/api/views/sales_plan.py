import django_filters
from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from stores.models import Store
from ..serializers import SalesPlanSerializer
from ...models import SalesPlan


class SalesPlanFilterSet(django_filters.FilterSet):

    class Meta:
        model = SalesPlan
        fields = [
            'month',
            'year',
            'store',
        ]


class SalesPlanViewSet(ModelViewSet):
    serializer_class = SalesPlanSerializer
    model = SalesPlan
    filter_class = SalesPlanFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=month', '=year')
    ordering_fields = (
        'id',
        'month',
        'year',
        'store__name',
    )

    def dispatch(self, request, store_pk: int, *args, **kwargs):
        self.store = get_object_or_404(Store, pk=store_pk)
        return super(SalesPlanViewSet, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        if not serializer.validated_data.get('store', None):
            serializer.save(store=self.store)
        serializer.save()

    def get_queryset(self):
        qs = SalesPlan.objects.filter(store=self.store)
        return qs

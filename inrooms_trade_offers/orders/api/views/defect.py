import datetime
import django_filters
from django.db import transaction
from django.db.models.aggregates import Sum
from django.utils import timezone
from rest_framework import filters, status
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from core.utils.views.additional_data_pagination import AdditionalDataPaginatedViewMixin
from history.models import HistoryItem
from orders.models.order import Order, OrderStatus, OrderItemSize
from products.models import Size
from products.services.product_remain_service import RemainService
from warehouses.models import Warehouse
from ..serializers import DefectActSerializer, DefectImageSerializer
from ...models import DefectAct, DefectActItem, DefectImage, DefectActStatus, OrderItem


class DefectActFilterSet(django_filters.FilterSet):

    class Meta:
        model = DefectAct
        fields = [
            'client',
            'status',
            'created',
            'modified',
            'items__order_item__product',
            'status__is_end_status',
        ]


class DefectActViewSet(AdditionalDataPaginatedViewMixin,
                       ModelViewSet):
    serializer_class = DefectActSerializer
    model = DefectAct
    filter_class = DefectActFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', '^items__order_item__product__article', 'items__order_item__product__name')
    ordering_fields = (
        'id',
        'created',
        'modified',
        'client__name',
    )

    def perform_create(self, serializer):
        if not serializer.validated_data.get('client', None):
            if self.request.user.has_perm('auth.can_defect_act_view') and self.request.user.client:
                serializer.save(client=self.request.user.client)
        serializer.save()
        history_item = HistoryItem.objects.create(
            kind=HistoryItem.KIND.defect_act_status_change,
            content_object=serializer.instance,
            body='Пользователь {} {} создал акт на брак №{} в статусе "{}"'.format(
                self.request.user.name,
                timezone.now().strftime('%d.%m.%Y %H:%M'),
                serializer.instance.id,
                serializer.instance.status.name
            ),
        )

    def get_queryset(self):
        qs = DefectAct.objects.all()
        qs = qs.select_related('client', 'status')
        qs = qs.prefetch_related('items', 'items__order_item', 'orderreturn_set', 'status__next_status')
        if not self.request.user.has_perm('auth.can_all_defect_act_view'):
            if self.request.user.client:
                qs = qs.filter(client=self.request.user.client)
            else:
                qs = qs.none()
        return qs

    def get_additional_data(self, queryset):
        data = queryset.aggregate(Sum('items__order_item__price'))
        return {
            'amount_full_sum': data['items__order_item__price__sum'],
        }

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.status.code == 'new':
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(data={'error': 'Удалять можно только в статусе "новый"'},
                            status=status.HTTP_400_BAD_REQUEST, )

    @detail_route(methods=['post'])
    def add_product(self, request, pk=None):
        obj = self.get_object()
        order_id = int(request.data.get('order_id', None))
        product_id = int(request.data.get('product_id', None))
        size_code = request.data.get('size_code', None)

        try:
            size = Size.objects.get(code=size_code)
        except Size.DoesNotExist:
            size = None

        oi = OrderItem.objects.get(order_id=order_id, product_id=product_id)

        if DefectActItem.objects.filter(defect_act=obj):
            return Response(data={'error': 'В одном документе не может быть больше одного товара!'},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            rai = DefectActItem.objects.create(
                defect_act=obj,
                order_item=oi,
                size=size
            )
            return Response({
                'status': 'ok'
            })

    @detail_route(methods=['post'])
    @transaction.atomic()
    def change_status(self, request, pk=None):
        """Изменение статуса заказа

        Формат запроса: {'status_code': 'new'}
        Параметры:
            status_code: (обязательный) новый статус заказа

        """
        object = self.get_object()
        status_code = request.data.get('code', None)
        old_status_name = object.status.name
        if status_code:
            try:
                new_status = DefectActStatus.objects.get(code=status_code)
            except DefectActStatus.DoesNotExist:
                pass
            else:
                psw = 'auth.can_defect_act_set_'
                approved_status_codes = [
                    perm[len(psw):] for perm in request.user.get_all_permissions() if perm.startswith(psw)]
                if status_code in approved_status_codes:
                    if status_code == 'agreement':
                        now = timezone.now()
                        for i in object.items.all():
                            if not i.order_item.order.date_shipped:
                                error_message = 'Не известна дата отправки заказа для товара {}'.format(i.order_item.product_id)
                                self._create_history_item(
                                    object,
                                    'Пользователь {} {} пытался изменить статус акта на брак №{} с "{}" на "{}". Изменение статуса не удалось по причине: {}'.format(
                                        request.user.name,
                                        timezone.now().strftime('%d.%m.%Y %H:%M'),
                                        object.id,
                                        old_status_name,
                                        new_status.name,
                                        error_message
                                    )
                                )
                                return Response({'error': error_message},
                                                status=status.HTTP_400_BAD_REQUEST)
                            if i.order_item.order.date_shipped + datetime.timedelta(days=object.client.defect_act_expired_days) < now:
                                error_message = 'В соответствии с условием договора, претензии по качеству товара принимаются в течении 20-ти дней с даты отгрузки товара. Дата отгрузки данного товара - {}'.format(i.order_item.order.date_shipped.strftime('%d.%m.%Y'))
                                self._create_history_item(
                                    object,
                                    'Пользователь {} {} пытался изменить статус акта на брак №{} с "{}" на "{}". Изменение статуса не удалось по причине: {}'.format(
                                        request.user.name,
                                        timezone.now().strftime('%d.%m.%Y %H:%M'),
                                        object.id,
                                        old_status_name,
                                        new_status.name,
                                        error_message
                                    )
                                )
                                return Response({'error': error_message},
                                                status=status.HTTP_400_BAD_REQUEST)
                    if status_code in ['agreed',
                                       'refused_to_acceptance',
                                       'repair_complete',
                                       'cleaning_complete',
                                       'consideration_replacing_identical',
                                       'consideration_replacing_simular',
                                       'consideration_repair',
                                       'consideration_cleaning', ]:
                        if not object.category:
                            error_message = 'Необходимо заполнить категорию'
                            self._create_history_item(
                                object,
                                'Пользователь {} {} пытался изменить статус акта на брак №{} с "{}" на "{}". Изменение статуса не удалось по причине: {}'.format(
                                    request.user.name,
                                    timezone.now().strftime('%d.%m.%Y %H:%M'),
                                    object.id,
                                    old_status_name,
                                    new_status.name,
                                    error_message
                                )
                            )
                            return Response({'error': error_message},
                                            status=status.HTTP_400_BAD_REQUEST)
                    if status_code in ['refused_to_acceptance', 'repair_complete', 'cleaning_complete']:
                        self._create_order(object, 'defect')
                    if status_code == 'consideration_replacing_identical':
                        product_ids = set(object.items.values_list('order_item__product', flat=True))
                        remains = RemainService(
                            product_ids=product_ids,
                            warehouse_codes=['msk', 'showroom']
                        ).get_remains_by_size()
                        for item in object.items.all():
                            if remains[item.order_item.product_id][item.size.code] < 1:
                                return Response({'error': 'товара нет в наличии'},
                                                status=status.HTTP_400_BAD_REQUEST)
                        self._create_order(object, 'defect')
                    object.status = new_status
                    object.save()
                    self._create_history_item(
                        object,
                        'Пользователь {} {} изменил статус акта на брак №{} с "{}" на "{}"'.format(
                            request.user.name,
                            timezone.now().strftime('%d.%m.%Y %H:%M'),
                            object.id,
                            old_status_name,
                            new_status.name
                        )
                    )
                    object.process_defect_act()
                    return Response(status=status.HTTP_200_OK)
        error_message = 'Не верный статус'
        self._create_history_item(
            object,
            'Пользователь {} {} пытался изменить статус акта на брак №{} с "{}" на "{}". Изменение статуса не удалось по причине: {}'.format(
                request.user.name,
                timezone.now().strftime('%d.%m.%Y %H:%M'),
                object.id,
                old_status_name,
                status_code,
                error_message
            )
        )
        return Response({'error': error_message},
                        status=status.HTTP_400_BAD_REQUEST)

    def _create_history_item(self, defect_act, message):
        history_item = HistoryItem.objects.create(
            kind=HistoryItem.KIND.defect_act_status_change,
            content_object=defect_act,
            body=message,
        )

    @transaction.atomic()
    def _create_order(self, defect_act, order_kind_code):
        order_status = OrderStatus.objects.get(code='new')
        order_kind = getattr(Order.KIND, order_kind_code)
        warehouse = Warehouse.objects.get(code='msk')
        order = Order.objects.filter(client=defect_act.client, status=order_status, kind=order_kind).first()
        if not order:
            order = Order(client=defect_act.client,
                          status=order_status,
                          kind=order_kind,
                          creator=self.request.user, )
            order.save()

        for item in defect_act.items.all():
            try:
                order_item = OrderItem.objects.get(
                    order=order,
                    product=item.order_item.product,
                )
            except OrderItem.DoesNotExist:
                order_item = OrderItem(
                    order=order,
                    product=item.order_item.product,
                )
                order_item.save()
            ois, created = OrderItemSize.objects.get_or_create(
                order_item=order_item,
                size=item.size,
                warehouse=warehouse,
                category=defect_act.category,
                is_delivery_reserve=False,
                defaults={
                    'quantity': 1
                }
            )
            if not created:
                ois.quantity += 1
                ois.save()

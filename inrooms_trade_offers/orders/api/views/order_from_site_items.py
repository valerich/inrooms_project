from rest_framework import filters, permissions
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import OrderFromSiteItemSerializer
from ...models import OrderFromSite, OrderFromSiteItem


class OrderFromSiteItemPermission(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method == 'DELETE' and not request.user.has_perm('auth.can_orders_return_delete'):
            return False
        return True


class OrderFromSiteItemViewSet(ModelViewSet):
    serializer_class = OrderFromSiteItemSerializer
    model = OrderFromSiteItem
    queryset = OrderFromSiteItem.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('=product__id', '^product__article', 'product__name')
    ordering_fields = (
        'product__article',
        'product__name',
    )
    permission_classes = (OrderFromSiteItemPermission,)

    def dispatch(self, request, order_pk: int, *args, **kwargs):
        self.order = get_object_or_404(OrderFromSite, pk=order_pk)
        return super(OrderFromSiteItemViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super(OrderFromSiteItemViewSet, self).get_queryset()
        qs = qs.filter(order=self.order)
        qs = qs.select_related('product',
                               'product__main_collection',
                               'product__main_collection__parent',
                               'product__user',
                               'product__color', )
        qs = qs.prefetch_related('product__images',
                                 'product__remain_set',
                                 'product__remain_set__warehouse',
                                 'product__remain_set__size',
                                 'product__comments',)
        return qs

    def perform_destroy(self, instance):
        order_return = instance.order
        instance.delete()
        order_return.set_amount_full()
        order_return.save()
from collections import defaultdict

import django_filters
from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet

from clients.models import Client
from products.models import Product
from ..serializers import DefectProductSerializer
from ...models import (
    DefectAct,
    DefectActItem,
    Order,
    OrderItem,
    OrderItemSize,
    OrderStatus,
)


def filter_client_not_order(queryset, value):
    """Фильтрация по тому покупал ли клиент такой товар или нет

    https://bitbucket.org/valerich/inrooms_project/issues/182/--------------------

    Формат значения:
    {client_id} - отфильтрует товары, которые не входят в заказы клиента client_id (Не учитываются заказы в статусе "new")
    -{client_id} - отфильтрует товары, которые входят в заказы клиента client_id (Не учитываются заказы в статусе "new"
    {client_id}+ - отфильтрует товары, которые не входят во все заказы клиента client_id (в т.ч. заказы в статусе "new")
    -{client_id}+ - отфильтрует товары, которые входят во все заказы клиента client_id (в т.ч. заказы в статусе "new")
    """
    if value:
        if value[0] == '-':
            client_id = value[1:]
        else:
            client_id = value
        if value[-1] == '+':
            client_id = client_id[:-1]
            order_status = None
        else:
            try:
                order_status = OrderStatus.objects.get(code='new')
            except OrderStatus.DoesNotExist:
                pass
        try:
            client = Client.objects.get(id=client_id)
        except Client.DoesNotExist:
            return queryset
        else:
            order_qs = Order.objects.filter(client=client)
            if order_status:
                order_qs = order_qs.exclude(status=order_status)
            product_ids = OrderItem.objects.filter(order__in=order_qs).values_list('product_id', flat=True).distinct()
            if value[0] == '-':
                queryset = queryset.filter(id__in=product_ids)
            else:
                queryset = queryset.exclude(id__in=product_ids)
    return queryset


class ProductFilterSet(django_filters.FilterSet):
    client_not_order = django_filters.CharFilter(action=filter_client_not_order)

    class Meta:
        model = Product
        fields = [
            'main_collection',
            'client_not_order',
        ]


class DefectProductViewSet(ListModelMixin,
                           GenericViewSet):

    serializer_class = DefectProductSerializer
    # filter_class = ProductFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter, )
    search_fields = ('=article', 'name', '=id')
    queryset = Product.objects.order_by('-modified')
    ordering_fields = (
        'id',
        'name',
        'article',
        'modified',
        'created',
        'main_collection__name',
        'price',
    )

    def dispatch(self, request, defect_pk: int, *args, **kwargs):
        self.client = request.user.client
        self.defect = get_object_or_404(DefectAct, id=defect_pk)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        product_ids = defaultdict(lambda : defaultdict(int))
        ois_qs = OrderItemSize.objects.all().select_related('order_item')
        ois_qs = ois_qs.filter(order_item__order__client=self.defect.client)
        ois_qs = ois_qs.exclude(order_item__order__status__code__in=[
            'new',
            'reserved', ])

        for ois in ois_qs:
            product_ids[ois.order_item.product_id][ois.size_id] += ois.quantity

        defect_qs = DefectAct.objects.filter(client=self.defect.client).prefetch_related('items')

        for defect in defect_qs:
            for item in defect.items.all():
                if item.size_id in product_ids[item.order_item.product_id]:
                    product_ids[item.order_item.product_id][item.size_id] -= 1
                    count = sum(product_ids[item.order_item.product_id].values())
                    if count <= 0:
                        product_ids.pop(item.order_item.product_id)

        qs = Product.objects.all()
        qs = qs.filter(id__in=product_ids)
        qs = qs.select_related(
            'color',
        )
        qs = qs.prefetch_related(
            'images'
        )
        qs = qs.order_by('-modified')
        return qs

    def get_serializer_context(self):
        return {'defect': self.defect}
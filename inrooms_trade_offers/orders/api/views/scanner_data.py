from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import ScannerDataSerializer
from ...models import ScannerData


class ScannerDataViewSet(mixins.RetrieveModelMixin,
                         mixins.ListModelMixin,
                         GenericViewSet):
    serializer_class = ScannerDataSerializer
    queryset = ScannerData.objects.filter(is_ordered=False)

from collections import defaultdict

from decimal import Decimal

from django.db.models.query_utils import Q
from django.utils import timezone
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

import django_filters
from rest_framework import filters

from orders.models import Cheque
from payments.models import CurrencyRate
from ..serializers import ChequeSerializer


class ChequeFilterSet(django_filters.FilterSet):

    class Meta:
        model = Cheque
        fields = [
            'client',
            'seller',
            'buyer_card',
            'items__product',
        ]


class ChequeQuerysetMixin():

    def get_queryset(self):
        today = timezone.now().date()
        qs = Cheque.objects.filter(date__date=today)

        if not self.request.user.has_perm('auth.can_all_orders_b2с_view'):
            if self.request.user.has_perm('auth.can_orders_b2с_view'):
                q_objects = Q()
                if self.request.user.client:
                    q_objects.add(Q(client=self.request.user.client), Q.OR)
                if self.request.user.clients.all():
                    q_objects.add(Q(client__in=self.request.user.clients.all()), Q.OR)
                if q_objects:
                    qs = qs.filter(q_objects)
                else:
                    qs = qs.none()
            else:
                qs = qs.none()

        qs = qs.select_related('buyer_card', )
        qs = qs.prefetch_related('items', 'items__sizes', 'items__sizes__size')

        return qs


class ChequeViewSet(ChequeQuerysetMixin, ReadOnlyModelViewSet):

    serializer_class = ChequeSerializer
    model = Cheque
    filter_class = ChequeFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', '^items__product__article', 'items__product__name')
    ordering_fields = (
        'id',
        'created',
        'modified',
        'client__name',
    )


class ChequeDayReportView(ChequeQuerysetMixin, APIView):

    def get(self, request, *args, **kwargs):
        data = self.get_data()
        return Response(data={
            'client_order_b2c': data
        })

    def get_data(self):
        today = timezone.now().date()

        raw_data = defaultdict(Decimal)
        data = []
        currency_rates = {}

        qs = self.get_queryset()
        qs = qs.order_by('client__name')

        for c in qs:
            price_sum = c.get_price_sum()
            currency = c.client.order_b2c_currency

            if currency not in currency_rates:
                try:
                    currency_rates[currency] = CurrencyRate.objects.get(date=today, currency=currency).rate
                except CurrencyRate.DoesNotExist:
                    currency_rates[currency] = 1
            currency_rate = currency_rates[currency]

            price_sum_rur = price_sum * currency_rate

            if c.kind == c.KIND.sale:
                raw_data[c.client.name] += price_sum_rur
            else:
                raw_data[c.client.name] -= price_sum_rur

        for client_name in sorted(raw_data.keys(), reverse=True):
            data.append({
                'client_name': client_name,
                'price_sum': raw_data[client_name]
            })
        return data

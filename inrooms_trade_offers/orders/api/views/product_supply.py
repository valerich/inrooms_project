import django_filters
from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet

from clients.models import Client
from orders.models import Order, OrderItem, OrderStatus
from products.models import Product, Remain
from supply.helpers.supply import SupplyHelper
from supply.models import SupplyItem
from ..serializers import ProductSupplySerializer


def filter_client_not_order(queryset, value):
    """Фильтрация по тому покупал ли клиент такой товар или нет

    https://bitbucket.org/valerich/inrooms_project/issues/182/--------------------

    Формат значения:
    {client_id} - отфильтрует товары, которые не входят в заказы клиента client_id (Не учитываются заказы в статусе "new")
    -{client_id} - отфильтрует товары, которые входят в заказы клиента client_id (Не учитываются заказы в статусе "new"
    {client_id}+ - отфильтрует товары, которые не входят во все заказы клиента client_id (в т.ч. заказы в статусе "new")
    -{client_id}+ - отфильтрует товары, которые входят во все заказы клиента client_id (в т.ч. заказы в статусе "new")
    """
    if value:
        if value[0] == '-':
            client_id = value[1:]
        else:
            client_id = value
        if value[-1] == '+':
            client_id = client_id[:-1]
            order_status = None
        else:
            try:
                order_status = OrderStatus.objects.get(code='new')
            except OrderStatus.DoesNotExist:
                pass
        try:
            client = Client.objects.get(id=client_id)
        except Client.DoesNotExist:
            return queryset
        else:
            order_qs = Order.objects.filter(client=client)
            if order_status:
                order_qs = order_qs.exclude(status=order_status)
            product_ids = OrderItem.objects.filter(order__in=order_qs).values_list('product_id', flat=True).distinct()
            if value[0] == '-':
                queryset = queryset.filter(id__in=product_ids)
            else:
                queryset = queryset.exclude(id__in=product_ids)
    return queryset


class ProductFilterSet(django_filters.FilterSet):
    client_not_order = django_filters.CharFilter(action=filter_client_not_order)

    class Meta:
        model = Product
        fields = [
            'main_collection',
            'client_not_order',
        ]


class ProductSupplyViewSet(ListModelMixin,
                           GenericViewSet):

    serializer_class = ProductSupplySerializer
    filter_class = ProductFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter, )
    search_fields = ('=article', 'name', '=id')
    queryset = Product.objects.order_by('-modified')
    ordering_fields = (
        'id',
        'name',
        'article',
        'modified',
        'created',
        'main_collection__name',
        'price',
    )

    def dispatch(self, request, order_pk: int, *args, **kwargs):
        self.order = get_object_or_404(Order, pk=order_pk)
        self.supply_data = self.get_supply_data()
        return super(ProductSupplyViewSet, self).dispatch(request, *args, **kwargs)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['supply_data'] = self.supply_data
        return context

    def get_supply_data(self):
        supply_helper = SupplyHelper(
            supply_statuses=[
                'sent_to_warehouse',
                'set_prices',
                'delivery_waiting',
            ],
            order_ids=[
                self.order.id,
            ]
        )
        return supply_helper.get_products_by_size()

    def get_queryset(self):
        product_ids = self.supply_data.keys()

        in_order_ids = OrderItem.objects.filter(order_id=self.order).values_list('product_id', flat=True)

        qs = Product.objects.all()
        qs = qs.filter(id__in=product_ids)
        qs = qs.filter(brand__code='zaracity')

        if in_order_ids:
            qs = qs.exclude(id__in=in_order_ids)
        qs = qs.select_related(
            'color',
        )
        qs = qs.prefetch_related(
            'images'
        )
        qs = qs.order_by('-modified')
        return qs

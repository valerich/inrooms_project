from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import OrderStatusSerializer
from ...models import OrderStatus


class OrderStatusViewSet(mixins.RetrieveModelMixin,
                         mixins.ListModelMixin,
                         GenericViewSet):
    serializer_class = OrderStatusSerializer
    queryset = OrderStatus.objects.all()
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )
    search_fields = ('name', )
    ordering_fields = (
        'code',
        'name',
    )

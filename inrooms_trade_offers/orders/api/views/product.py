import datetime

from django.db.models.query_utils import Q
from django.utils import timezone
from dateutil.relativedelta import relativedelta
import django_filters
from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet

from clients.models import Client
from orders.models import Order, OrderItem, OrderStatus
from products.models import Product, Remain
from products.services.product_remain_service import RemainService
from purchases.models import PurchaseItem, PurchaseStatus
from supply.models import SupplyItem, SupplyStatus
from warehouses.models import Warehouse
from ..serializers import ProductSerializer


def filter_month_old_product(queryset, value):
    if value:
        try:
            value = int(value)
        except Exception:
            pass
        else:
            if 0 < value <= 100:
                date_from = timezone.now() - relativedelta(months=value)
                warehouse_to = Warehouse.objects.get(code='msk')
                supply_status = SupplyStatus.objects.get(code='delivery_accepted')
                supply_product_ids = SupplyItem.objects.filter(
                    supply__delivery_warehouse_to=warehouse_to,
                    supply__created__gte=date_from,
                    supply__status=supply_status
                ).values_list('product', flat=True).order_by().distinct()
                purchase_status = PurchaseStatus.objects.get(code='delivery_accepted')
                purchase_product_ids = PurchaseItem.objects.filter(
                    purchase__delivery_warehouse_to=warehouse_to,
                    purchase__created__gte=date_from,
                    purchase__status=purchase_status
                ).values_list('product', flat=True).order_by().distinct()
                queryset = queryset.exclude(id__in=supply_product_ids).exclude(id__in=purchase_product_ids)
    return queryset


def filter_warehouse_id_remains(queryset, value):
    if value:
        try:
            warehouse = Warehouse.objects.get(id=value)
        except Warehouse.DoesNotExist:
            queryset = queryset.none()
        else:
            product_ids = Remain.objects.filter(warehouse=warehouse, value__gt=0).values_list('product_id', flat=True)
            queryset = queryset.filter(id__in=product_ids)
    return queryset


def filter_client_not_order(queryset, value):
    """Фильтрация по тому покупал ли клиент такой товар или нет

    https://bitbucket.org/valerich/inrooms_project/issues/182/--------------------

    Формат значения:
    {client_id} - отфильтрует товары, которые не входят в заказы клиента client_id (Не учитываются заказы в статусе "new")
    -{client_id} - отфильтрует товары, которые входят в заказы клиента client_id (Не учитываются заказы в статусе "new"
    {client_id}+ - отфильтрует товары, которые не входят во все заказы клиента client_id (в т.ч. заказы в статусе "new")
    -{client_id}+ - отфильтрует товары, которые входят во все заказы клиента client_id (в т.ч. заказы в статусе "new")
    """
    if value:
        if value[0] == '-':
            client_id = value[1:]
        else:
            client_id = value
        if value[-1] == '+':
            client_id = client_id[:-1]
            order_status = None
        else:
            try:
                order_status = OrderStatus.objects.get(code='new')
            except OrderStatus.DoesNotExist:
                pass
        try:
            client = Client.objects.get(id=client_id)
        except Client.DoesNotExist:
            return queryset
        else:
            order_qs = Order.objects.filter(client=client)
            if order_status:
                order_qs = order_qs.exclude(status=order_status)
            product_ids = OrderItem.objects.filter(order__in=order_qs).values_list('product_id', flat=True).distinct()
            if value[0] == '-':
                queryset = queryset.filter(id__in=product_ids)
            else:
                queryset = queryset.exclude(id__in=product_ids)
    return queryset


class ProductFilterSet(django_filters.FilterSet):
    warehouse_id_remains = django_filters.CharFilter(action=filter_warehouse_id_remains)
    client_not_order = django_filters.CharFilter(action=filter_client_not_order)
    month_old_product = django_filters.NumberFilter(action=filter_month_old_product)

    class Meta:
        model = Product
        fields = [
            'main_collection',
            'warehouse_id_remains',
            'client_not_order',
            'month_old_product'
        ]


class ProductViewSet(ListModelMixin,
                     GenericViewSet):

    serializer_class = ProductSerializer
    filter_class = ProductFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter, )
    search_fields = ('=article', 'name', '=id')
    queryset = Product.objects.order_by('-modified')
    ordering_fields = (
        'id',
        'name',
        'article',
        'created',
        'main_collection__name',
        'modified',
        'price',
    )

    def dispatch(self, request, order_pk: int, *args, **kwargs):
        self.order = get_object_or_404(Order, pk=order_pk)
        return super(ProductViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        product_ids = Remain.objects.filter(
            warehouse__to_orders=True,
            value__gt=0
        ).values_list(
            'product_id', flat=True
        ).distinct()

        product_ids = RemainService(
            product_ids=product_ids,
            warehouse_codes=['msk', 'showroom'],
            exclude_reserves_for_order_ids=[self.order.id]
        ).get_flat_remains().keys()

        exclude_ids = OrderItem.objects.filter(order_id=self.order).values_list('product_id', flat=True)

        qs = Product.objects.filter(id__in=product_ids)
        if self.order.kind in [self.order.KIND.defect,
                               self.order.KIND.ten_percent, ]:
            qs = self.get_dr_product_search_ids(qs)

        # TODO: Когда будут собственные магазины что нибудь сделать(
        qs = qs.filter(brand__code='zaracity')
        if exclude_ids:
            qs = qs.exclude(id__in=exclude_ids)
        qs = qs.select_related(
            'color',
        )
        qs = qs.prefetch_related(
            'images'
        )
        qs = qs.order_by('-modified')
        return qs

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['exclude_reserves_for_order_ids'] = [self.order.id, ]
        context['new_product_ids'] = self.get_new_product_ids()
        return context

    def get_dr_product_search_ids(self, qs):
        """
        Для заказов по браку необходимо подбирать товары:
        - у коллекции которых стоит флаг cat_partner_self_sale
        - не новинки
        - товары должны быть обработаны

        """

        qs = qs.filter(is_active=True)
        qs = qs.filter(main_collection__cat_partner_self_sale=True)
        qs = qs.filter(is_new_collection=False)
        return qs


    def get_new_product_ids(self):
        date_from = timezone.now() - relativedelta(months=6)
        warehouse_to = Warehouse.objects.get(code='msk')
        supply_status = SupplyStatus.objects.get(code='delivery_accepted')
        new_product_ids = set(SupplyItem.objects.filter(
            supply__delivery_warehouse_to=warehouse_to,
            supply__created__gte=date_from,
            supply__status=supply_status
        ).values_list('product', flat=True).order_by().distinct())
        purchase_status = PurchaseStatus.objects.get(code='delivery_accepted')
        new_product_ids.update(set(PurchaseItem.objects.filter(
            purchase__delivery_warehouse_to=warehouse_to,
            purchase__created__gte=date_from,
            purchase__status=purchase_status
        ).values_list('product', flat=True).order_by().distinct()))
        return new_product_ids

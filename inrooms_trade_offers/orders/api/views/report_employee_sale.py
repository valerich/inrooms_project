from collections import defaultdict

from decimal import Decimal
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView
from orders.models.order_b2c import OrderB2CItem, OrderReturnB2CItem
from stores.models import Store, Employee


class ReportSerializer(serializers.Serializer):
    date_from = serializers.DateField()
    date_to = serializers.DateField()
    store = serializers.CharField(required=False)
    store_detail = serializers.BooleanField(required=False)


class ReportEmployeeSale(APIView):

    def get(self, request, *args, **kwargs):
        self.user = request.user
        serializer = ReportSerializer(data=request.GET)
        serializer.is_valid(True)
        self.store_ids = serializer.validated_data.get('store', None)
        if self.store_ids:
            self.store_ids = self.store_ids.split(',')

        self.date_from = serializer.validated_data['date_from']
        self.date_to = serializer.validated_data['date_to']

        self.store_detail = serializer.validated_data.get('store_detail', False)
        data = []
        if self.store_detail:
            for store in self.get_stores():
                report_data = self.get_report_data([store, ])
                if report_data['report_data']:
                    data.append(report_data)
        else:
            data.append(self.get_report_data(self.get_stores()))
        return Response(data=data)

    def get_stores(self):
        qs = Store.objects.all()
        if self.store_ids:
            qs = qs.filter(id__in=self.store_ids)
        if self.user.has_perm('auth.view_report_all_employee_sale'):
            pass
        elif self.user.has_perm('auth.view_report_employee_sale'):
            if self.user.client_id:
                qs = qs.filter(client=self.user.client)
            else:
                qs = qs.none()
        else:
            qs = qs.none()
        return qs

    def get_order_b2c_queryset(self, stores):
        client_ids = set([s.client_id for s in stores])
        qs = OrderB2CItem.objects.filter(
            order__client_id__in=client_ids,
            order__date__gte=self.date_from,
            order__date__lte=self.date_to,
            cheque_1c__seller__isnull=False
        )
        qs = qs.select_related('cheque_1c', 'order')
        qs = qs.prefetch_related('orderb2citemsize_set')
        if self.user.has_perm('auth.view_report_all_employee_sale'):
            pass
        elif self.user.has_perm('auth.view_report_employee_sale'):
            if self.user.client_id:
                qs = qs.filter(order__client=self.user.client)
            else:
                qs = qs.none()
        else:
            qs = qs.none()
        return qs

    def get_order_return_b2c_queryset(self, stores):
        client_ids = set([s.client_id for s in stores])
        qs = OrderReturnB2CItem.objects.filter(
            order__client_id__in=client_ids,
            order__date__gte=self.date_from,
            order__date__lte=self.date_to,
            order__seller__isnull=False
        )
        qs = qs.select_related('order')
        qs = qs.prefetch_related('orderreturnb2citemsize_set')
        if self.user.has_perm('auth.view_report_all_employee_sale'):
            pass
        elif self.user.has_perm('auth.view_report_employee_sale'):
            if self.user.client_id:
                qs = qs.filter(order__client=self.user.client)
            else:
                qs = qs.none()
        else:
            qs = qs.none()
        return qs

    def get_report_data(self, stores):
        order_amounts = defaultdict(Decimal)
        order_return_amounts = defaultdict(Decimal)
        checks = defaultdict(set)
        quantityes = defaultdict(int)
        without_buyer_cards = defaultdict(set)
        for oi in self.get_order_b2c_queryset(stores):
            checks[oi.cheque_1c.seller_id].add((oi.order.date.strftime('%d.%m.%Y'), oi.cheque_1c.number_1c if oi.cheque_1c else ''))
            order_amounts[oi.cheque_1c.seller_id] += oi.get_price_sum()
            quantityes[oi.cheque_1c.seller_id] += oi.get_quantity()
            if not oi.cheque_1c.buyer_card_id:
                without_buyer_cards[oi.cheque_1c.seller_id].add((oi.order.date.strftime('%d.%m.%Y'), oi.cheque_1c.number_1c if oi.cheque_1c else ''))

        for oi in self.get_order_return_b2c_queryset(stores):
            order_return_amounts[oi.order.seller_id] += oi.get_price_sum()

        employee_ids = set(order_amounts.keys())
        employee_ids.update(order_return_amounts.keys())

        data = []
        all_check_count = 0
        all_quantity = 0
        all_amount = Decimal('0.00')
        all_return_amount = Decimal('0.00')
        all_without_buyer_card = 0
        for employee in Employee.objects.filter(id__in=employee_ids).order_by('last_name', 'first_name', 'middle_name'):

            check_count = len(checks[employee.id])
            quantity = quantityes[employee.id]
            amount = order_amounts[employee.id]
            return_amount = order_return_amounts[employee.id]
            net_amount = amount - return_amount
            without_buyer_card = len(without_buyer_cards[employee.id])

            all_check_count += check_count
            all_quantity += quantity
            all_amount += amount
            all_return_amount += return_amount
            all_without_buyer_card += without_buyer_card

            data.append({
                'employee': {
                    'full_name': employee.get_full_name(),
                    'id': employee.id,
                },
                'amount': amount,
                'return_amount': return_amount,
                'net_amount': net_amount,
                'check_count': check_count,
                'without_buyer_card': without_buyer_card,
                'quantity': quantity,
                'quantity_avg': quantity / check_count if check_count else None,
                'amount_avg': amount / check_count if check_count else None
            })

        if self.store_detail:
            name = 'По {} ({} - {})'.format(
                ', '.join([s.name for s in stores]),
                self.date_from.strftime('%d.%m.%Y'),
                self.date_to.strftime('%d.%m.%Y')
            )
        else:
            name = 'Общий отчет ({} - {})'.format(self.date_from.strftime('%d.%m.%Y'), self.date_to.strftime('%d.%m.%Y'))

        return {
            'name': name,
            'report_data': data,
            'all_amount': all_amount,
            'all_return_amount': all_return_amount,
            'all_net_amount': all_amount - all_return_amount,
            'all_quantity': all_quantity,
            'all_check_count': all_check_count,
            'all_quantity_avg': all_quantity / all_check_count if all_check_count else None,
            'all_amount_avg': all_amount / all_check_count if all_check_count else None,
            'all_without_buyer_card': all_without_buyer_card
        }

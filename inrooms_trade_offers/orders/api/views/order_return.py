import django_filters
from django.db.models.aggregates import Sum
from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from core.utils.views.additional_data_pagination import AdditionalDataPaginatedViewMixin
from ..serializers import OrderReturnSerializer
from ...models import OrderReturn


def filter_has_defect_act(queryset, value):
    if value:
        queryset = queryset.filter(defect_act__isnull=False)
    else:
        queryset = queryset.filter(defect_act__isnull=True)
    return queryset


def filter_has_refund_act(queryset, value):
    if value:
        queryset = queryset.filter(refund_act__isnull=False)
    else:
        queryset = queryset.filter(refund_act__isnull=True)
    return queryset


class OrderReturnFilterSet(django_filters.FilterSet):
    has_defect_act = django_filters.BooleanFilter(action=filter_has_defect_act)
    has_refund_act = django_filters.BooleanFilter(action=filter_has_refund_act)

    class Meta:
        model = OrderReturn
        fields = [
            'items__product',
            'client',
            'category',
            'has_defect_act',
            'has_refund_act',
        ]


class OrderReturnViewSet(AdditionalDataPaginatedViewMixin,
                         mixins.RetrieveModelMixin,
                         mixins.ListModelMixin,
                         GenericViewSet):
    serializer_class = OrderReturnSerializer
    model = OrderReturn
    filter_class = OrderReturnFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', '^items__product__article', 'items__product__name')
    ordering_fields = (
        'id',
        'client__name',
    )

    def get_queryset(self):
        qs = OrderReturn.objects.all()
        if not self.request.user.has_perm('auth.can_all_orders_return_view'):
            if self.request.user.client:
                qs = qs.filter(client=self.request.user.client)
            else:
                qs = qs.none()
        return qs

    def get_additional_data(self, queryset):
        data = queryset.aggregate(Sum('amount_full'))
        return {
            'amount_full_sum': data['amount_full__sum'],
        }

import django_filters
from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import OrderReturnB2CSerializer
from ...models import OrderReturnB2C


class OrderReturnB2CFilterSet(django_filters.FilterSet):

    class Meta:
        model = OrderReturnB2C
        fields = [
            'items__product',
        ]


class OrderReturnB2CViewSet(mixins.RetrieveModelMixin,
                      mixins.ListModelMixin,
                      GenericViewSet):
    serializer_class = OrderReturnB2CSerializer
    model = OrderReturnB2C
    filter_class = OrderReturnB2CFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', '^items__product__article', 'items__product__name')
    ordering_fields = (
        'id',
        'client__name',
    )

    def get_queryset(self):
        qs = OrderReturnB2C.objects.all()
        if not self.request.user.has_perm('auth.can_all_orders_return_b2с_view'):
            if self.request.user.client:
                qs = qs.filter(client=self.request.user.client)
            else:
                qs = qs.none()
        return qs

from collections import defaultdict

import django_filters
from django.db import transaction
from django.utils import timezone
from rest_framework import filters, status
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from history.models import HistoryItem
from orders.models.order import OrderItem, OrderItemSize
from orders.models.refund import RefundActItem
from products.models import Size
from products.services.product_remain_service import RemainService
from warehouses.models import Warehouse
from ..serializers import RefundActSerializer
from ...models import RefundAct, RefundActStatus


class RefundActFilterSet(django_filters.FilterSet):

    class Meta:
        model = RefundAct
        fields = [
            'client',
            'items__order_item__product',
            'items__order_item__order',
            'items__size',
            'status',
            'created',
            'modified',
        ]


class RefundActViewSet(ModelViewSet):
    serializer_class = RefundActSerializer
    model = RefundAct
    filter_class = RefundActFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', )
    ordering_fields = (
        'id',
        'created',
        'modified',
        'client__name',
    )

    def perform_create(self, serializer):
        if not serializer.validated_data.get('client', None):
            if self.request.user.has_perm('auth.can_refund_act_view') and self.request.user.client:
                serializer.save(client=self.request.user.client)
        serializer.save()
        history_item = HistoryItem.objects.create(
            kind=HistoryItem.KIND.defect_act_status_change,
            content_object=serializer.instance,
            body='Пользователь {} {} создал акт на возврат №{} в статусе "{}"'.format(
                self.request.user.name,
                timezone.now().strftime('%d.%m.%Y %H:%M'),
                serializer.instance.id,
                serializer.instance.status.name
            ),
        )

    def get_queryset(self):
        qs = RefundAct.objects.all()
        if not self.request.user.has_perm('auth.can_all_refund_act_view'):
            if self.request.user.client:
                qs = qs.filter(client=self.request.user.client)
            else:
                qs = qs.none()
        return qs

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.status.code == 'new':
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(data={'error': 'Удалять можно только в статусе "новый"'},
                            status=status.HTTP_400_BAD_REQUEST, )

    @detail_route(methods=['post'])
    @transaction.atomic()
    def change_status(self, request, pk=None):
        """Изменение статуса заказа

        Формат запроса: {'status_code': 'new'}
        Параметры:
            status_code: (обязательный) новый статус заказа

        """
        object = self.get_object()
        status_code = request.data.get('code', None)
        old_status_name = object.status.name
        if status_code:
            try:
                new_status = RefundActStatus.objects.get(code=status_code)
            except RefundActStatus.DoesNotExist:
                pass
            else:
                psw = 'auth.can_refund_act_set_'
                approved_status_codes = [
                    perm[len(psw):] for perm in request.user.get_all_permissions() if perm.startswith(psw)]
                if status_code in approved_status_codes:
                    if status_code in ['agreement',
                                       'agreed',
                                       'refused_to_acceptance',
                                       'repair_complete',
                                       'cleaning_complete',
                                       'consideration_replacing_identical',
                                       'consideration_replacing_simular',
                                       'consideration_repair',
                                       'consideration_cleaning', ]:
                        if not object.category:
                            error_message = 'Необходимо заполнить категорию'
                            self._create_history_item(
                                object,
                                'Пользователь {} {} пытался изменить статус акта на возврат №{} с "{}" на "{}". Изменение статуса не удалось по причине: {}'.format(
                                    request.user.name,
                                    timezone.now().strftime('%d.%m.%Y %H:%M'),
                                    object.id,
                                    old_status_name,
                                    new_status.name,
                                    error_message
                                )
                            )
                            return Response({'error': error_message},
                                            status=status.HTTP_400_BAD_REQUEST)
                    object.status = new_status
                    object.save()
                    self._create_history_item(
                        object,
                        'Пользователь {} {} изменил статус акта на возврат №{} с "{}" на "{}"'.format(
                            request.user.name,
                            timezone.now().strftime('%d.%m.%Y %H:%M'),
                            object.id,
                            old_status_name,
                            new_status.name
                        )
                    )
                    object.process_refund_act()
                    return Response(status=status.HTTP_200_OK)

        error_message = 'Не верный статус'
        self._create_history_item(
            object,
            'Пользователь {} {} пытался изменить статус акта на возврат №{} с "{}" на "{}". Изменение статуса не удалось по причине: {}'.format(
                request.user.name,
                timezone.now().strftime('%d.%m.%Y %H:%M'),
                object.id,
                old_status_name,
                status_code,
                error_message
            )
        )

        return Response({'error': 'Не верный статус'},
                        status=status.HTTP_400_BAD_REQUEST)

    def _create_history_item(self, refund_act, message):
        history_item = HistoryItem.objects.create(
            kind=HistoryItem.KIND.refund_act_status_change,
            content_object=refund_act,
            body=message,
        )

    @detail_route(methods=['post'])
    def add_products(self, request, pk=None):
        obj = self.get_object()
        order_id = int(request.data.get('order_id', None))
        product_id = int(request.data.get('product_id', None))
        # size_quantityes = request.data.get('size_quantityes', None)
        all_quantityes = 0

        row_items = {}

        for key in ['34','35','36','37','38','39','40','41','xs','s','m','l','xl','xxl','xxxl']:
            val = int(request.data.get('size_quantity_{}'.format(key), 0))
            try:
                size = Size.objects.get(code=key)
            except Size.DoesNotExist:
                return Response(data={"error": 'Нет такого размера'}, status=status.HTTP_400_BAD_REQUEST)
            val = int(val)
            if val < 0:
                return Response(data={"error": 'Количество должно быть положительно'}, status=status.HTTP_400_BAD_REQUEST)
            elif val:
                row_items[size] = int(val)

        rs = RemainService(product_ids=[product_id, ],
                           warehouse_ids=Warehouse.objects.filter(client=obj.client).values_list('id', flat=True))
        remains = rs.get_remains_by_size().get(product_id, {})

        ois_qs = OrderItemSize.objects.all().select_related('order_item')
        ois_qs = ois_qs.filter(order_item__order__client=obj.client)
        ois_qs = ois_qs.exclude(order_item__order__status__code__in=[
            'new',
            'reserved',
        ])

        product_ids = defaultdict(lambda : defaultdict(int))

        for ois in ois_qs:
            product_ids[ois.order_item.product_id][ois.size_id] += ois.quantity

        refund_qs = RefundAct.objects.filter(client=obj.client).exclude(status__code='denial').prefetch_related('items')

        for refund in refund_qs:
            for item in refund.items.filter(order_item__product_id=product_id):
                if item.size_id in product_ids[item.order_item.product_id]:
                    product_ids[item.order_item.product_id][item.size_id] -= item.quantity
                    if item.size.code in remains:
                        remains[item.size.code] -= item.quantity
                    count = sum(product_ids[item.order_item.product_id].values())
                    if count <= 0:
                        product_ids.pop(item.order_item.product_id)

        for size, quantity in row_items.items():
            if product_ids[product_id][size.id] < quantity:
                return Response(data={"error": 'Товара не может быть больше, чем в бланк-заказе'}, status=status.HTTP_400_BAD_REQUEST)

            if remains.get(size.code, 0) < quantity:
                return Response(data={"error": 'На остатках столько нет'}, status=status.HTTP_400_BAD_REQUEST)

            oi = OrderItem.objects.get(order_id=order_id, product_id=product_id)
            rai, created = RefundActItem.objects.get_or_create(
                refund_act=obj,
                order_item=oi,
                size=size,
                defaults={
                    'quantity': quantity
                }
            )
            if not created:
                rai.quantity += quantity
                rai.save()

        return Response({
            'status': 'ok'
        })


    @detail_route(methods=['post'])
    def add_product(self, request, pk=None):
        obj = self.get_object()
        order_id = int(request.data.get('order_id', None))
        product_id = int(request.data.get('product_id', None))
        size_code = request.data.get('size_code', None)
        quantity = int(request.data.get('quantity', 1))

        if quantity <= 0:
            return Response(data={"error": 'Количество должно быть положительно'}, status=status.HTTP_400_BAD_REQUEST)

        rs = RemainService(product_ids=[product_id, ],
                           warehouse_ids=Warehouse.objects.filter(client=obj.client).values_list('id', flat=True))
        remains = rs.get_remains_by_size().get(product_id, {})

        try:
            size = Size.objects.get(code=size_code)
        except Size.DoesNotExist:
            size = None

        ois_qs = OrderItemSize.objects.all().select_related('order_item')
        ois_qs = ois_qs.filter(order_item__order__client=obj.client)
        ois_qs = ois_qs.exclude(order_item__order__status__code__in=[
            'new',
            'reserved',
        ])

        product_ids = defaultdict(lambda : defaultdict(int))

        for ois in ois_qs:
            product_ids[ois.order_item.product_id][ois.size_id] += ois.quantity

        refund_qs = RefundAct.objects.filter(client=obj.client).prefetch_related('items')

        for refund in refund_qs:
            for item in refund.items.filter(order_item__product_id=product_id):
                if item.size_id in product_ids[item.order_item.product_id]:
                    product_ids[item.order_item.product_id][item.size_id] -= item.quantity
                    if item.size.code in remains:
                        remains[item.size.code] -= item.quantity
                    count = sum(product_ids[item.order_item.product_id].values())
                    if count <= 0:
                        product_ids.pop(item.order_item.product_id)

        if product_ids[product_id][size.id] < quantity:
            return Response(data={"error": 'Товара не может быть больше, чем в бланк-заказе'}, status=status.HTTP_400_BAD_REQUEST)

        if remains.get(size.code, 0) < quantity:
            return Response(data={"error": 'На остатках столько нет'}, status=status.HTTP_400_BAD_REQUEST)

        oi = OrderItem.objects.get(order_id=order_id, product_id=product_id)
        rai, created = RefundActItem.objects.get_or_create(
            refund_act=obj,
            order_item=oi,
            size=size,
            defaults={
                'quantity': quantity
            }
        )
        if not created:
            rai.quantity += quantity
            rai.save()
        return Response({
            'status': 'ok'
        })

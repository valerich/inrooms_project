import django_filters
from django.db.models.query_utils import Q
from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import OrderB2CSerializer
from ...models import OrderB2C


class OrderB2CFilterSet(django_filters.FilterSet):

    class Meta:
        model = OrderB2C
        fields = [
            'items__product',
            'client',
        ]


class OrderB2CViewSet(mixins.RetrieveModelMixin,
                      mixins.ListModelMixin,
                      GenericViewSet):
    serializer_class = OrderB2CSerializer
    model = OrderB2C
    filter_class = OrderB2CFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', '^items__product__article', 'items__product__name')
    ordering_fields = (
        'id',
        'client__name',
    )

    def get_queryset(self):
        qs = OrderB2C.objects.all()
        if not self.request.user.has_perm('auth.can_all_orders_b2с_view'):
            if self.request.user.has_perm('auth.can_orders_b2с_view'):
                q_objects = Q()
                if self.request.user.client:
                    q_objects.add(Q(client=self.request.user.client), Q.OR)
                if self.request.user.clients.all():
                    q_objects.add(Q(client__in=self.request.user.clients.all()), Q.OR)
                if q_objects:
                    qs = qs.filter(q_objects)
                else:
                    qs = qs.none()
            else:
                qs = qs.none()
        return qs

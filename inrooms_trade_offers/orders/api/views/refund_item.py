from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import RefundActItemSerializer
from ...models import RefundActItem, RefundAct


class RefundActItemViewSet(ModelViewSet):
    serializer_class = RefundActItemSerializer
    model = RefundActItem
    queryset = RefundActItem.objects.all()
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )
    search_fields = ('=order_item__product__id', '^order_item__product__article', 'order_item__product__name')
    ordering_fields = (
        'order_item__product__article',
        'order_item__product__main_collection__name',
    )

    def dispatch(self, request, refund_pk: int, *args, **kwargs):
        self.refund_act = get_object_or_404(RefundAct, pk=refund_pk)
        return super(RefundActItemViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super(RefundActItemViewSet, self).get_queryset()
        qs = qs.filter(refund_act=self.refund_act)
        qs = qs.select_related('order_item',
                               'order_item__product',
                               'order_item__product__color',
                               'size', )
        qs = qs.prefetch_related('order_item__product__images')
        return qs

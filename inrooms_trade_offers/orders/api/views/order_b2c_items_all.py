import django_filters
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from ..serializers import OrderB2CItemAllSerializer
from ...models import OrderB2C, OrderB2CItem


class OrderB2CItemAllFilterSet(django_filters.FilterSet):

    class Meta:
        model = OrderB2CItem
        fields = [
            'cheque_1c__buyer_card',
        ]


class OrderB2CItemAllViewSet(ModelViewSet):
    serializer_class = OrderB2CItemAllSerializer
    model = OrderB2CItem
    queryset = OrderB2CItem.objects.all()
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend)
    filter_class = OrderB2CItemAllFilterSet
    search_fields = ('=product__id', '^product__article', 'product__name')
    ordering_fields = (
        'product__article',
        'product__name',
    )

    def get_queryset(self):
        qs = super(OrderB2CItemAllViewSet, self).get_queryset()
        qs = qs.select_related('product',
                               'product__main_collection',
                               'product__main_collection__parent',
                               'product__user',
                               'product__color', )
        qs = qs.prefetch_related('product__images',
                                 'product__remain_set',
                                 'product__remain_set__warehouse',
                                 'product__remain_set__size',
                                 'product__comments',)
        return qs

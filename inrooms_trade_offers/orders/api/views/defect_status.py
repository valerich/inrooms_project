from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import DefectActStatusSerializer
from ...models import DefectActStatus


class DefectActStatusViewSet(mixins.RetrieveModelMixin,
                         mixins.ListModelMixin,
                         GenericViewSet):
    serializer_class = DefectActStatusSerializer
    queryset = DefectActStatus.objects.all()
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )
    search_fields = ('name', )
    ordering_fields = (
        'code',
        'name',
    )

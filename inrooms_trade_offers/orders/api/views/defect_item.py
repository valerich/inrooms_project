from rest_framework import filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ..serializers import DefectActItemSerializer, DefectImageSerializer
from ...models import DefectActItem, DefectAct, DefectImage


class DefectActItemViewSet(ModelViewSet):
    serializer_class = DefectActItemSerializer
    model = DefectActItem
    queryset = DefectActItem.objects.all()
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )
    search_fields = ('=order_item__product__id', '^order_item__product__article', 'order_item__product__name')
    ordering_fields = (
        'order_item__product__article',
        'order_item__product__main_collection__name',
    )

    def dispatch(self, request, defect_pk: int, *args, **kwargs):
        self.defect_act = get_object_or_404(DefectAct, pk=defect_pk)
        return super(DefectActItemViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super(DefectActItemViewSet, self).get_queryset()
        qs = qs.filter(defect_act=self.defect_act)
        qs = qs.select_related('order_item',
                               'order_item__product',
                               'order_item__product__color',
                               'size', )
        qs = qs.prefetch_related('order_item__product__images')
        return qs

    @detail_route(methods=['post'])
    def image_upload(self, request, pk=None):
        obj = self.get_object()
        file_obj = request.FILES['file']

        _file = DefectImage.objects.create(
            image=file_obj,
            defect_act_item=obj
        )
        return Response({
            'status': 'ok'
        })

    @list_route(methods=['post'])
    def image_delete(self, request):
        file_id = request.data.get('file_id', None)

        try:
            _file = DefectImage.objects.get(
                id=file_id
            )
        except DefectImage.DoesNotExist:
            pass
        else:
            _file.delete()
        return Response({
            'status': 'ok'
        })

    @detail_route(methods=['get'])
    def images(self, request, pk=None):
        obj = self.get_object()
        return Response(DefectImageSerializer(obj.images.all(), many=True).data)
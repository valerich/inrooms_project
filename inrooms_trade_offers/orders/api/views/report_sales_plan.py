import datetime
from collections import defaultdict

from dateutil.relativedelta import relativedelta
from decimal import Decimal
from model_utils.choices import Choices
from rest_framework import filters, serializers
from rest_framework.response import Response
from rest_framework.views import APIView
from core.utils.date import date_range
from orders.models.order_b2c import OrderB2C
from orders.models.sales_plan import SalesPlan
from stores.models import Store


class ReportSerializer(serializers.Serializer):
    month = serializers.IntegerField()
    year = serializers.IntegerField()
    store = serializers.CharField(required=False)


class ReportSalesPlan(APIView):

    def get(self, request, *args, **kwargs):
        serializer = ReportSerializer(data=request.GET)
        serializer.is_valid(True)
        self.user = request.user
        self.store_ids = serializer.validated_data.get('store', None)
        if self.store_ids:
            self.store_ids = self.store_ids.split(',')
        self.month = serializer.validated_data['month']
        self.year = serializer.validated_data['year']
        data = self.get_report_data()
        return Response(data=data)

    def get_report_data(self):
        data = []
        first_day = datetime.date(self.year, self.month, 1)
        last_day = first_day + relativedelta(months=1) - datetime.timedelta(days=1)
        dates = date_range(first_day, last_day)
        dates_count = len(dates)
        all_report_data = {
            'data': [],
            'sales_plan': Decimal('0.00'),
            'month_amount_full': Decimal('0.00')
        }
        all_date_amounts = defaultdict(Decimal)
        for store in self.get_stores():
            try:
                sales_plan = SalesPlan.objects.get(store=store, month=self.month, year=self.year).plan
            except SalesPlan.DoesNotExist:
                sales_plan = 0

            day_sales_plan = Decimal(sales_plan / dates_count)
            all_report_data['sales_plan'] += sales_plan

            store_data = {
                'store': store.name,
                'sales_plan': sales_plan,
                'data': []
            }
            date_amounts = defaultdict(Decimal)
            for o in OrderB2C.objects.filter(
                    client=store.client,
                    date__gte=first_day,
                    date__lte=last_day,
            ):
                date_amounts[o.date.strftime('%d.%m.%Y')] += o.amount_full
                all_date_amounts[o.date.strftime('%d.%m.%Y')] += o.amount_full

            month_amount_full = Decimal('0.0')
            for date in dates:
                month_amount_full += date_amounts[date.strftime('%d.%m.%Y')]
                all_report_data['month_amount_full'] += month_amount_full
                store_data['data'].append({
                    'date': date.strftime('%d.%m.%Y'),
                    'amont_full': date_amounts[date.strftime('%d.%m.%Y')],
                    'day_plan': day_sales_plan,
                })
            store_data['month_amount_full'] = month_amount_full
            if sales_plan:
                store_data['execution_percentage'] = (month_amount_full * 100) / sales_plan
            else:
                store_data['execution_percentage'] = Decimal('0.00')
            if store_data['month_amount_full'] or store_data['sales_plan']:
                data.append(store_data)

        all_day_sales_plan = Decimal(all_report_data['sales_plan'] / dates_count)
        for date in dates:
            all_report_data['data'].append({
                'date': date.strftime('%d.%m.%Y'),
                'amount_full': all_date_amounts[date.strftime('%d.%m.%Y')],
                'day_plan': all_day_sales_plan,
            })
        if all_report_data['sales_plan']:
            all_report_data['execution_percentage'] = (all_report_data['month_amount_full'] * 100) / all_report_data['sales_plan']
        else:
            all_report_data['execution_percentage'] = Decimal('0.00')
        return {
            'report_data': data,
            'all_report_data': all_report_data,
            'dates': [date.day for date in dates]
        }

    def get_stores(self):
        qs = Store.objects.all()
        if self.store_ids:
            qs = qs.filter(id__in=self.store_ids)
        if self.user.has_perm('auth.view_report_all_sales_plan'):
            pass
        elif self.user.has_perm('auth.view_report_sales_plan'):
            if self.user.client_id:
                qs = qs.filter(client=self.user.client)
            else:
                qs = qs.none()
        else:
            qs = qs.none()
        return qs

from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import OrderReturnB2CItemSerializer
from ...models import OrderReturnB2C, OrderReturnB2CItem


class OrderReturnB2CItemViewSet(ModelViewSet):
    serializer_class = OrderReturnB2CItemSerializer
    model = OrderReturnB2CItem
    queryset = OrderReturnB2CItem.objects.all()
    filter_backends = (
        filters.SearchFilter,
    )
    search_fields = (
        '=product__id',
        '^product__article',
        'product__name',
    )
    ordering_fields = (
        'product__article',
        'product__name',
    )

    def dispatch(self, request, order_pk: int, *args, **kwargs):
        self.order = get_object_or_404(OrderReturnB2C, pk=order_pk)
        return super(OrderReturnB2CItemViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super(OrderReturnB2CItemViewSet, self).get_queryset()
        qs = qs.filter(order=self.order)
        qs = qs.select_related('product',
                               'product__main_collection',
                               'product__main_collection__parent',
                               'product__user',
                               'product__color', )
        qs = qs.prefetch_related('product__images',
                                 'product__remain_set',
                                 'product__remain_set__warehouse',
                                 'product__remain_set__size',
                                 'product__comments',)
        return qs

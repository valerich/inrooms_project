from rest_framework import filters
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from ..serializers import OrderB2CCheque1cSerializer
from ...models import OrderB2C, OrderB2CItem, Cheque1c


class OrderB2CCheque1cViewSet(ModelViewSet):
    serializer_class = OrderB2CCheque1cSerializer
    model = Cheque1c
    queryset = Cheque1c.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('=items__product__id', '^items__product__article', 'items__product__name')
    # ordering_fields = (
    #     'product__article',
    #     'product__name',
    # )

    def dispatch(self, request, order_pk: int, *args, **kwargs):
        self.order = get_object_or_404(OrderB2C, pk=order_pk)
        return super(OrderB2CCheque1cViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super(OrderB2CCheque1cViewSet, self).get_queryset()
        qs = qs.filter(order=self.order)
        # qs = qs.select_related('product',
        #                        'product__main_collection',
        #                        'product__main_collection__parent',
        #                        'product__user',
        #                        'product__color', )
        # qs = qs.prefetch_related('product__images',
        #                          'product__remain_set',
        #                          'product__remain_set__warehouse',
        #                          'product__remain_set__size',
        #                          'product__comments',)
        qs = qs.order_by('-number_1c', )
        return qs

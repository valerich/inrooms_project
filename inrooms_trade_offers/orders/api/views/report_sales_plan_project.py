import datetime
from collections import defaultdict

from dateutil.relativedelta import relativedelta
from decimal import Decimal
from rest_framework import filters, serializers
from rest_framework.response import Response
from rest_framework.views import APIView
from core.utils.date import date_range
from orders.models.order_b2c import OrderB2C
from orders.models.sales_plan import SalesPlan
from stores.models import Store


class ReportSerializer(serializers.Serializer):
    month = serializers.IntegerField()
    year = serializers.IntegerField()
    store = serializers.CharField(required=False)


class ReportSalesPlanProject(APIView):

    def get(self, request, *args, **kwargs):
        serializer = ReportSerializer(data=request.GET)
        serializer.is_valid(True)
        self.store_ids = serializer.validated_data.get('store', None)
        if self.store_ids:
            self.store_ids = self.store_ids.split(',')
        self.month = serializer.validated_data['month']
        self.year = serializer.validated_data['year']
        self.first_day = datetime.date(self.year, self.month, 1)
        self.last_day = self.first_day + relativedelta(months=1) - datetime.timedelta(days=1)
        self.dates = date_range(self.first_day, self.last_day)
        data = self.get_report_data()
        return Response(data=data)

    def get_order_b2c_queryset(self, store):
        return OrderB2C.objects.filter(
            client=store.client,
            date__gte=self.first_day,
            date__lte=self.last_day,
        )

    def get_order_b2c_date_amounts(self, store):
        date_amounts = defaultdict(Decimal)
        for o in self.get_order_b2c_queryset(store):
            date_amounts[o.date.strftime('%d.%m.%Y')] += o.amount_full
        return date_amounts

    def get_store_sales_plans(self):
        self.store_sales_plans = {}
        for store in self.stores:
            try:
                sales_plan = SalesPlan.objects.get(store=store, month=self.month, year=self.year).plan
            except SalesPlan.DoesNotExist:
                sales_plan = 0
            self.store_sales_plans[store.id] = sales_plan
        return self.store_sales_plans

    def get_store_month_sales_plans(self):
        store_sales_plans = self.get_store_sales_plans()
        self.store_month_sales_plans = {}
        for store in self.stores:
            store_sales_plan = store_sales_plans[store.id]
            try:
                month_sales_plan = SalesPlan.objects.get(store=store, month=self.month, year=self.year)
            except SalesPlan.DoesNotExist:
                pass
            else:
                self.store_month_sales_plans[store.id] = {
                    1: Decimal(store_sales_plan / 100) * month_sales_plan.week1,
                    2: Decimal(store_sales_plan / 100) * month_sales_plan.week2,
                    3: Decimal(store_sales_plan / 100) * month_sales_plan.week3,
                    4: Decimal(store_sales_plan / 100) * month_sales_plan.week4,
                    5: Decimal(store_sales_plan / 100) * month_sales_plan.week5,
                    6: Decimal(store_sales_plan / 100) * month_sales_plan.week6,
                }
        return self.store_month_sales_plans

    def get_store_week_sales_plans(self):
        month_sales_plans = self.get_store_month_sales_plans()
        self.store_week_sales_plans = {}
        for store in self.stores:
            try:
                month_sales_plan = month_sales_plans[store.id]
            except KeyError:
                pass
            else:
                try:
                    week_sales_plans = SalesPlan.objects.get(store=store, month=self.month, year=self.year)
                except SalesPlan.DoesNotExist:
                    pass
                else:
                    store_data = {}
                    week = 1
                    for date in self.dates:
                        weekday = date.weekday() + 1
                        if weekday == 1 and store_data:
                            week += 1
                        day_sales_plan = (month_sales_plan[week] / 100) * getattr(week_sales_plans, 'day{}'.format(weekday))
                        store_data[date.strftime('%d.%m.%Y')] = day_sales_plan
                    self.store_week_sales_plans[store.id] = store_data
        return self.store_week_sales_plans

    def get_report_data(self):
        self.get_stores()
        self.store_week_sales_plans = self.get_store_week_sales_plans()
        data = []
        for store in self.stores:
            swsp = self.store_week_sales_plans.get(store.id, {})
            store_data = []
            week_data = []
            week = 1
            week_amount_full = Decimal('0.00')
            amount_full = Decimal('0.00')
            sales_plan = self.store_sales_plans[store.id]

            orders_data = self.get_order_b2c_date_amounts(store)

            for date in self.dates:
                date_str = date.strftime('%d.%m.%Y')
                weekday = date.weekday() + 1
                if weekday == 1 and week_data:
                    store_data.append({
                        'week': week,
                        'amount_full': week_amount_full,
                        'sales_plan': self.store_month_sales_plans.get(store.id, {}).get(week, None),
                        'week_data': week_data
                    })
                    week_data = []
                    week_amount_full = Decimal('0.00')
                    week += 1
                day_sales_plan = swsp.get(date_str, None)
                week_amount_full += orders_data[date_str]
                amount_full += orders_data[date_str]
                week_data.append({
                    'date': date_str,
                    'amount_full': orders_data[date_str],
                    'sales_plan': day_sales_plan,
                    'execution_percentage': (orders_data[date_str] * 100) / day_sales_plan if day_sales_plan else None
                })
            if week_data:
                store_data.append({
                    'week': week,
                    'week_data': week_data,
                    'sales_plan': self.store_month_sales_plans.get(store.id, {}).get(week, None),
                    'amount_full': week_amount_full,
                })
            for i in store_data:
                i['execution_percentage'] = (i['amount_full'] * 100) / i['sales_plan'] if i['sales_plan'] else None
            if amount_full or sales_plan:
                data.append({
                    'store': store.name,
                    'amount_full': amount_full,
                    'sales_plan': sales_plan,
                    'execution_percentage': (amount_full * 100) / sales_plan if sales_plan else None,
                    'data': store_data,
                })

        return {
            'report_data': data,
            'dates': self.get_dates()
        }

    def get_dates(self):
        dates = []
        week_data = []
        week = 1
        for date in self.dates:
            weekday = date.weekday() + 1
            if weekday == 1 and week_data:
                dates.append({
                    'week': week,
                    'data': week_data
                })
                week_data = []
                week += 1
            week_data.append({
                'date': date.day,
                'date_str': date.strftime('%a'),
            })
        if week_data:
            dates.append({
                'week': week,
                'data': week_data
            })
        return dates

    def get_stores(self):
        self.stores = Store.objects.all()
        if self.store_ids:
            self.stores = self.stores.filter(id__in=self.store_ids)
        return self.stores

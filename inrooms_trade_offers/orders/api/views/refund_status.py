from rest_framework import filters, mixins
from rest_framework.viewsets import GenericViewSet

from ..serializers import RefundActStatusSerializer
from ...models import RefundActStatus


class RefundActStatusViewSet(mixins.RetrieveModelMixin,
                         mixins.ListModelMixin,
                         GenericViewSet):
    serializer_class = RefundActStatusSerializer
    queryset = RefundActStatus.objects.all()
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )
    search_fields = ('name', )
    ordering_fields = (
        'code',
        'name',
    )

import django_filters
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from ..serializers import OrderB2CCheque1cAllSerializer
from ...models import Cheque1c


class OrderB2CCheque1cAllFilterSet(django_filters.FilterSet):

    class Meta:
        model = Cheque1c
        fields = [
            'buyer_card',
        ]


class OrderB2CCheque1cAllViewSet(ModelViewSet):
    serializer_class = OrderB2CCheque1cAllSerializer
    model = Cheque1c
    queryset = Cheque1c.objects.all()
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend)
    filter_class = OrderB2CCheque1cAllFilterSet
    search_fields = ('=items__product__id', '^items__product__article', 'items__product__name')
    # ordering_fields = (
    #     'product__article',
    #     'product__name',
    # )

    def get_queryset(self):
        qs = super(OrderB2CCheque1cAllViewSet, self).get_queryset()
        qs = qs.select_related('seller',
                               'buyer_card', )
        qs = qs.prefetch_related('items',
                                 'items__product',
                                 'items__product__main_collection',
                                 'items__product__main_collection__parent',
                                 'items__product__user',
                                 'items__product__color',
                                 'items__product__images',
                                 'items__product__remain_set',
                                 'items__product__remain_set__warehouse',
                                 'items__product__remain_set__size',
                                 'items__product__comments',)
        return qs

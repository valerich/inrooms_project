from decimal import Decimal
from django.db import transaction
from rest_framework import filters, status, permissions
from rest_framework.decorators import list_route, detail_route
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from orders.helpers.free_dr_summ import get_free_dr_summ
from products.models import Size
from products.services import RemainService
from supply.helpers.supply import SupplyHelper
from warehouses.models import Warehouse
from ..serializers import OrderItemSerializer
from ...helpers import SetSizeQuantityException
from ...models import Order, OrderItem, ScannerData


class OrderItemPermission(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method == 'DELETE':
            if request.user.has_perm('auth.can_order_item_all_delete'):
                return True
            elif request.user.has_perm('auth.can_order_item_self_delete') and obj.creator == request.user:
                return True
            return False
        return True


class OrderItemViewSet(ModelViewSet):
    serializer_class = OrderItemSerializer
    model = OrderItem
    queryset = OrderItem.objects.all()
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )
    search_fields = ('=product__id', '^product__article', 'product__name')
    ordering_fields = (
        'product__article',
        'product__main_collection__name',
    )
    permission_classes = (OrderItemPermission, )

    def dispatch(self, request, order_pk: int, *args, **kwargs):
        self.order = get_object_or_404(Order, pk=order_pk)
        return super(OrderItemViewSet, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        try:
            series_count = int(self.request.data['series_count'])
        except Exception:
            series_count = self.order.series_count

        with transaction.atomic():
            instance = serializer.save(order=self.order, creator=self.request.user)

            for size in Size.objects.filter(series_type=instance.series_type):
                instance.set_size_quantity_with_multiplier(size, series_count=series_count)
            self.order.set_amount_full()
            self.order.save()
            if self.order.kind != 1:
                free_summ = self.get_free_dr_summ()
                if free_summ < 0:
                    raise SetSizeQuantityException('Не достаточно средств на свободном остатоке по актам')
        return instance

    def get_queryset(self):
        qs = super(OrderItemViewSet, self).get_queryset()
        qs = qs.filter(order=self.order)
        qs = qs.select_related('product',
                               'product__main_collection',
                               'product__main_collection__parent',
                               'product__user',
                               'product__color', )
        qs = qs.prefetch_related('product__images',
                                 'product__remain_set',
                                 'product__remain_set__warehouse',
                                 'product__remain_set__size',
                                 'product__comments',)
        return qs

    def get_free_dr_summ(self):
        data = get_free_dr_summ(client_id=self.order.client_id, order_kind=self.order.kind)
        return data[self.order.client_id]['free_summ']

    def create(self, request, *args, **kwargs):
        try:
            return super().create(request, *args, **kwargs)
        except SetSizeQuantityException as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        try:
            return super().update(request, *args, **kwargs)
        except SetSizeQuantityException as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['exclude_reserves_for_order_ids'] = [self.order.id, ]
        context['supply_data'] = self.get_supply_data()
        context['all_remains_data'] = self.get_all_remains_data()
        return context

    def get_supply_data(self):
        supply_helper = SupplyHelper(
            supply_statuses=[
                'sent_to_warehouse',
                'set_prices',
                'delivery_waiting',
            ],
            order_ids=[
                self.order.id,
            ]
        )
        return supply_helper.get_products_by_size()

    def get_all_remains_data(self):
        return RemainService(
            exclude_reserves_for_order_ids=[
                self.order.id,
            ],
            product_ids=self.get_queryset().values_list('product_id', flat=True).order_by().distinct(),
            warehouse_codes=['msk', 'showroom'],
        ).get_remains_by_size_with_supply()

    @detail_route(methods=['post'])
    @transaction.atomic()
    def set_product_price(self, request, pk=None):
        obj = self.get_object()
        data = request.data
        try:
            new_price = Decimal(data['price'])
        except Exception:
            return Response(data={'price': 'Цена должна быть численной'},
                            status=status.HTTP_400_BAD_REQUEST)
        obj.product.price = new_price
        obj.product.save()
        obj.price = new_price
        obj.save()
        obj.order.set_amount_full()
        obj.order.save()
        return Response()

    @list_route(methods=['post'])
    @transaction.atomic()
    def create_on_scanner_data(self, request):
        data = request.data
        try:
            scanner_data = ScannerData.objects.get(id=data['scanner_data_id'])
            scanner_data.is_ordered = True
            scanner_data.save()
        except ScannerData.DoesNotExist:
            return Response(data={'scanner_data_id': 'Не найдено данных сканера с таким id'},
                            status=status.HTTP_400_BAD_REQUEST)
        try:
            series_count = int(data['series_count'])
        except Exception:
            series_count = self.order.series_count

        with transaction.atomic():
            for product in scanner_data.products.all():
                try:
                    OrderItem.objects.get(order=self.order, product=product)
                except OrderItem.DoesNotExist:
                    oi = OrderItem(
                        order=self.order,
                        product=product,
                    )
                    oi.save()

                    for size in Size.objects.filter(series_type=oi.series_type):
                        oi.set_size_quantity_with_multiplier(size, series_count=series_count)
                else:
                    pass
        return Response(data)

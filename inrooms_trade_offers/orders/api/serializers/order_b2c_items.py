from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers

from products.api.serializers import ProductSerializer
from products.models import Size
from stores.api.serializers.employee import EmployeeSerializer
from ...models import OrderB2CItem


class OrderB2CItemSerializer(serializers.ModelSerializer):
    product_detail = serializers.SerializerMethodField()

    class Meta:
        model = OrderB2CItem
        fields = [
            'id',
            'price',
            'product_detail',
            'series_type',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        size_fields = {}
        for size in Size.objects.all():
            size_fields['count_{}'.format(size.code)] = serializers.IntegerField(
                source='get_count_{}'.format(size.code),
                required=False,
            )
        self.fields.update(size_fields)

    def get_product_detail(self, obj):
        if obj.product_id:
            return ProductSerializer(obj.product, context={'exclude_reserves_for_order_ids': [obj.order.id, ]}).data
        else:
            return None


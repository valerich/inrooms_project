from decimal import Decimal
from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import SalesPlan


class SalesPlanSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = SalesPlan

        fields = [
            'id',
            'month',
            'year',
            'plan',
            'week1',
            'week2',
            'week3',
            'week4',
            'week5',
            'week6',
            'day1',
            'day2',
            'day3',
            'day4',
            'day5',
            'day6',
            'day7',
        ]

    def validate(self, data):
        week_summ = Decimal('0.00')
        for i in [
            'week1',
            'week2',
            'week3',
            'week4',
            'week5',
            'week6',
        ]:
            week_summ += data.get(i, Decimal('0.00'))
        if week_summ and week_summ != Decimal('100'):
            raise serializers.ValidationError("Сумма коэффициентов по неделям должна быть равна 100")

        day_summ = Decimal('0.00')
        for i in [
            'day1',
            'day2',
            'day3',
            'day4',
            'day5',
            'day6',
            'day7',
        ]:
            day_summ += data.get(i, Decimal('0.00'))
        if day_summ and day_summ != Decimal('100'):
            raise serializers.ValidationError("Сумма коэффициентов по дням должна быть равна 100")

        return data

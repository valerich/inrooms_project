from rest_framework import serializers

from products.api.serializers import ProductSerializer
from products.models import Size
from ...models import OrderFromSiteItem


class OrderFromSiteItemSerializer(serializers.ModelSerializer):
    product_detail = serializers.SerializerMethodField()

    class Meta:
        model = OrderFromSiteItem
        fields = [
            'id',
            'price',
            'product_detail',
            'quantity',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        size_fields = {}
        for size in Size.objects.all():
            size_fields['count_{}'.format(size.code)] = serializers.IntegerField(
                source='get_count_{}'.format(size.code),
                required=False,
            )
        self.fields.update(size_fields)

    def get_product_detail(self, obj):
        if obj.product_id:
            return ProductSerializer(obj.product, context={'exclude_reserves_for_order_ids': [obj.order.id, ]}).data
        else:
            return None

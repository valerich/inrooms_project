from decimal import Decimal
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from stores.api.serializers.employee import EmployeeSerializer
from ...models import Cheque1c


class OrderB2CCheque1cSerializer(serializers.ModelSerializer):
    buyer_card_detail = serializers.SerializerMethodField()
    seller_detail = serializers.SerializerMethodField()
    items = serializers.SerializerMethodField()
    amount_full = serializers.SerializerMethodField()

    class Meta:
        model = Cheque1c
        fields = [
            'id',
            'order',
            'buyer_card_detail',
            'number_1c',
            'seller_detail',
            'items',
            'amount_full',
        ]

    def get_amount_full(self, obj):
        amount_full = Decimal('0.00')
        for item in obj.items.all():
            amount_full += item.get_price_sum()
        return str(amount_full)

    def get_buyer_card_detail(self, obj):
        if obj.buyer_card:
            data = {
                'id': obj.buyer_card.id,
                'number': obj.buyer_card.number,
            }
            try:
                data['buyer_profile_id'] = obj.buyer_card.buyerprofile.id
            except ObjectDoesNotExist:
                data['buyer_profile_id'] = None
            return data
        else:
            return None

    def get_seller_detail(self, obj):
        if obj.seller_id:
            return EmployeeSerializer(obj.seller, fields=['id', 'first_name', 'last_name', 'middle_name']).data
        return None

    def get_items(self, obj):
        data = []
        for item in obj.items.all():
            data.append(
                {
                    'product_detail': {
                        'id': item.product_id,
                        'name': item.product.name,
                        'article': item.product.article,
                        'color_detail': {
                            'name': item.product.color.name,
                        },
                        'image': self.resize_image(item.product.images.all().first(), item.product)
                    },
                    'price': item.price,
                    'price_before_discount': item.price_before_discount,
                    'discount_percent': self.discount_percent(item.price, item.price_before_discount),
                    'sizes': [
                        {
                            'name': size_item.size.name,
                            'quantity': size_item.quantity
                        } for size_item in item.orderb2citemsize_set.all()
                    ]
                }
            )
        return data

    def discount_percent(self, price, price_before_discount):
        if price_before_discount:
            return ((price_before_discount - price) / price_before_discount) * 100
        return 0

    def resize_image(self, pi, obj):
        if pi:
            try:
                _200x200 = get_thumbnail(pi.image, '400x400', crop="center").url
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
            except IOError as e:
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1500 = request.build_absolute_uri(_950x1200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,
                    '950x1200': _950x1200}
        else:
            return None

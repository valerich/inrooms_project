from collections import OrderedDict, defaultdict

from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from orders.models.defect import DefectActItem
from orders.models.order import OrderItem
from products.models import Product


class DefectProductSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    color_detail = serializers.SerializerMethodField()
    orders = serializers.SerializerMethodField()

    class Meta:
        model = Product

        fields = [
            'id',
            'article',
            'name',
            'image',
            'color',
            'color_detail',
            'orders',
        ]

    def get_image(self, obj):
        pi = obj.images.all().first()
        return self.resize_image(pi, obj)

    def resize_image(self, pi, obj):
        if pi:
            try:
                _200x200 = get_thumbnail(pi.image, '400x400', crop="center").url
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
            except IOError as e:
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1500 = request.build_absolute_uri(_950x1200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,
                    '950x1200': _950x1200}
        else:
            return None

    def get_color_detail(self, obj):
        return {'id': obj.color_id,
                'name': obj.color.name}

    def get_orders(self, obj):
        orders = OrderedDict()

        client = self.context['defect'].client

        defect_sizes = defaultdict(lambda : defaultdict(int))
        for da in DefectActItem.objects.filter(defect_act__client=client, order_item__product=obj):
            defect_sizes[da.order_item.order_id][da.size.code] += 1

        for order_item in OrderItem.objects.filter(
            order__client=client,
            product_id=obj.id
        ).exclude(order__status__code__in=[
            'new',
            'reserved', ]):
            if order_item.order_id not in orders:
                orders[order_item.order_id] = {
                    'order_id': order_item.order_id,
                    'order_price': order_item.price,
                    'sizes': defaultdict(int)
                }
            for ois in order_item.orderitemsize_set.all():
                orders[order_item.order_id]['sizes'][ois.size.code] += ois.quantity

        for order_id, sizes in defect_sizes.items():
            if order_id in orders:
                for size, quantity in sizes.items():
                    if size in orders[order_id]['sizes']:
                        orders[order_id]['sizes'][size] -= quantity
                        if orders[order_id]['sizes'][size] <= 0:
                            orders[order_id]['sizes'].pop(size)
                            if sum(orders[order_id]['sizes'].values()) <= 0:
                                orders.pop(order_id)
        for order in orders.values():
            order['sizes_list'] = [{
                'code': key,
                'count': order['sizes'][key]
            } for key in ['34','35','36','37','38','39','40','41','xs','s','m','l','xl','xxl','xxxl'] if key in order['sizes']]
        return orders.values()

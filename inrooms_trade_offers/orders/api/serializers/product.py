from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from products.models import Product
from products.services import RemainService


class ProductSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    color_detail = serializers.SerializerMethodField()
    remains = serializers.SerializerMethodField()
    is_new_product = serializers.SerializerMethodField()

    class Meta:
        model = Product

        fields = [
            'id',
            'article',
            'name',
            'image',
            'color',
            'color_detail',
            'price',
            'remains',
            'is_new_product',
        ]

    def get_image(self, obj):
        pi = obj.images.all().first()
        return self.resize_image(pi, obj)

    def resize_image(self, pi, obj):
        if pi:
            try:
                _200x200 = get_thumbnail(pi.image, '400x400', crop="center").url
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
            except IOError as e:
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1500 = request.build_absolute_uri(_950x1200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,
                    '950x1200': _950x1200}
        else:
            return None

    def get_color_detail(self, obj):
        return {'id': obj.color_id,
                'name': obj.color.name}

    def get_remains(self, obj):
        rs = RemainService([obj.id], warehouse_codes=['msk', 'showroom'])
        remains = rs.get_flat_remains()
        return remains.get(obj.id, 0)

    def get_is_new_product(self, obj):
        new_product_ids = self.context.get('new_product_ids', set())
        if obj.id in new_product_ids:
            return True
        else:
            return False


from rest_framework import serializers

from clients.api.serializers import ClientSerializer
from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import RefundAct


class RefundActSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    client_detail = serializers.SerializerMethodField()
    status_detail = serializers.SerializerMethodField()
    amount_full = serializers.SerializerMethodField()

    class Meta:
        model = RefundAct

        fields = [
            'id',
            'client',
            'client_detail',
            'status',
            'status_detail',
            'created',
            'modified',
            'amount_full',
            'category',
            'comment',
        ]

        extra_kwargs = {
            'client': {'required': False, },
            'category': {'required': False, 'allow_blank': True},
        }

    def get_client_detail(self, obj):
        return ClientSerializer(obj.client).data

    def get_amount_full(self, obj):
        return obj.get_amount_full()

    def get_status_detail(self, obj):
        return {'id': obj.status_id,
                'name': obj.status.name,
                'code': obj.status.code,
                'next_statuses': [{
                    'id': s.id,
                    'name': s.name,
                    'button_name': s.button_name,
                    'code': s.code,
                } for s in obj.status.next_status.all()]}

    def validate_category(self, data):
        if data == '':
            return None
        return data

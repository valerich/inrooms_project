from rest_framework import serializers

from ...models import DefectActStatus


class DefectActStatusSerializer(serializers.ModelSerializer):
    next_status_detail = serializers.SerializerMethodField()

    class Meta:
        model = DefectActStatus
        fields = [
            'id',
            'name',
            'code',
            'next_status_detail'
        ]

    def get_next_status_detail(self, obj):
        return [{'id': s.id,
                 'name': s.name,
                 'code': s.code} for s in obj.next_status.all()]

from rest_framework import serializers

from ...models import ScannerData


class ScannerDataSerializer(serializers.ModelSerializer):
    products_count = serializers.SerializerMethodField()

    class Meta:
        model = ScannerData

        fields = [
            'id',
            'created',
            'modified',
            'products_count',
        ]

    def get_products_count(self, obj):
        return obj.products.count()

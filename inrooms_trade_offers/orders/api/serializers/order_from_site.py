from rest_framework import serializers


from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import OrderFromSite, ClientFromSite, ShippingAddress

class CilentFromSiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientFromSite
        fields = (
            'email',
            'guest_email',
            'id_from_site'
        )

class ShippingAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShippingAddress
        fields = (
            'first_name',
            'last_name',
            'phone_number',
            'line1',
            'line2',
            'line3',
            'state',
            'city',
            'postcode'
        )



class OrderFromSiteSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    client_detail = serializers.SerializerMethodField()
    shipping_address = serializers.SerializerMethodField()

    class Meta:
        model = OrderFromSite

        fields = [
            'id',
            'created',
            'number',
            'client_detail',
            'total',
            'shipping_address',
        ]

    def get_client_detail(self, obj):
        return CilentFromSiteSerializer(obj.client).data

    def get_shipping_address(self, obj):
        return ShippingAddressSerializer(obj.shipping_address).data

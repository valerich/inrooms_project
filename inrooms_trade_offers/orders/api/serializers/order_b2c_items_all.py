from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers

from products.api.serializers import ProductSerializer
from products.models import Size
from stores.api.serializers.employee import EmployeeSerializer
from ...models import OrderB2CItem


class OrderB2CItemAllSerializer(serializers.ModelSerializer):
    product_detail = serializers.SerializerMethodField()
    order_detail = serializers.SerializerMethodField()
    cheque_1c_detail = serializers.SerializerMethodField()

    class Meta:
        model = OrderB2CItem
        fields = [
            'id',
            'price',
            'product_detail',
            'series_type',
            'order_detail',
            'cheque_1c_detail',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        size_fields = {}
        for size in Size.objects.all():
            size_fields['count_{}'.format(size.code)] = serializers.IntegerField(
                source='get_count_{}'.format(size.code),
                required=False,
            )
        self.fields.update(size_fields)

    def get_cheque_1c_detail(self, obj):
        if obj.cheque_1c_id:
            if obj.cheque_1c.buyer_card:
                buyer_card_detail = {
                    'id': obj.cheque_1c.buyer_card.id,
                    'number': obj.cheque_1c.buyer_card.number,
                }
                try:
                    buyer_card_detail['buyer_profile_id'] = obj.cheque_1c.buyer_card.buyerprofile.id
                except ObjectDoesNotExist:
                    buyer_card_detail['buyer_profile_id'] = None
            else:
                buyer_card_detail = None
            if obj.cheque_1c.seller_id:
                seller_detail = EmployeeSerializer(obj.cheque_1c.seller, fields=['id', 'first_name', 'last_name', 'middle_name']).data
            else:
                seller_detail = None
            return {
                'buyer_card_detail': buyer_card_detail,
                'number_1c': obj.cheque_1c.number_1c,
                'seller_detail': seller_detail
            }
        else:
            return None

    def get_product_detail(self, obj):
        if obj.product_id:
            return ProductSerializer(obj.product, fields=['id', 'name', 'article', 'color_detail', 'image']).data
        else:
            return None

    def get_order_detail(self, obj):
        return {
            'id': obj.order_id,
            'number': obj.order.number,
            'date': obj.order.date.strftime('%d.%m.%Y')
        }

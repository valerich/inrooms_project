from rest_framework import serializers

from clients.api.serializers.client import ClientSerializer
from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import OrderB2C


class OrderB2CSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    client_detail = serializers.SerializerMethodField()

    class Meta:
        model = OrderB2C

        fields = [
            'id',
            'date',
            'number',
            'client_detail',
            'amount_full',
            'amount_full_rur',
            'visits',
        ]

    def get_client_detail(self, obj):
        return ClientSerializer(obj.client).data

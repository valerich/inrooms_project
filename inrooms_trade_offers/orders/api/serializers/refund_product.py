from collections import OrderedDict, defaultdict

from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from products.models import Product
from products.services.product_remain_service import RemainService
from warehouses.models import Warehouse
from ...models import (
    OrderItem,
    RefundActItem,
)


class RefundProductSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    color_detail = serializers.SerializerMethodField()
    orders = serializers.SerializerMethodField()

    class Meta:
        model = Product

        fields = [
            'id',
            'article',
            'name',
            'image',
            'color',
            'color_detail',
            'orders',
        ]

    def get_image(self, obj):
        pi = obj.images.all().first()
        return self.resize_image(pi, obj)

    def resize_image(self, pi, obj):
        if pi:
            try:
                _200x200 = get_thumbnail(pi.image, '400x400', crop="center").url
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
            except IOError as e:
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1500 = request.build_absolute_uri(_950x1200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,
                    '950x1200': _950x1200}
        else:
            return None

    def get_color_detail(self, obj):
        return {'id': obj.color_id,
                'name': obj.color.name}

    def get_orders(self, obj):
        orders = OrderedDict()

        client = self.context['refund'].client

        rs = RemainService(product_ids=[obj.id, ],
                           warehouse_ids=Warehouse.objects.filter(client=client).values_list('id', flat=True))
        remains = rs.get_remains_by_size().get(obj.id, {})

        defect_sizes = defaultdict(lambda : defaultdict(int))
        for da in RefundActItem.objects.filter(refund_act__client=client, order_item__product=obj).exclude(refund_act__status__code='denial'):
            defect_sizes[da.order_item.order_id][da.size.code] += da.quantity
            if da.size.code in remains:
                remains[da.size.code] -= da.quantity

        for order_item in OrderItem.objects.filter(
            order__client=client,
            product_id=obj.id,
        ).exclude(order__status__code__in=[
            'new',
            'reserved',
        ]):
            if order_item.order_id not in orders:
                orders[order_item.order_id] = {
                    'order_id': order_item.order_id,
                    'order_price': order_item.price,
                    'sizes': defaultdict(int)
                }
            for ois in order_item.orderitemsize_set.filter(is_delivery_reserve=False):
                orders[order_item.order_id]['sizes'][ois.size.code] += ois.quantity

        for order_id, sizes in defect_sizes.items():
            if order_id in orders:
                for size, quantity in sizes.items():
                    if size in orders[order_id]['sizes']:
                        orders[order_id]['sizes'][size] -= quantity
                        if orders[order_id]['sizes'][size] <= 0:
                            orders[order_id]['sizes'].pop(size)
                            if sum(orders[order_id]['sizes'].values()) <= 0:
                                orders.pop(order_id)

        for order in orders.values():
            order['sizes_list'] = []
            for key in ['34','35','36','37','38','39','40','41','xs','s','m','l','xl','xxl','xxxl']:
                if key in order['sizes']:
                    count = order['sizes'][key]
                    remain_count = remains.get(key, 0)
                    order['sizes_list'].append(
                        {
                            'code': key,
                            'count': count,
                            'remain_count': remain_count if remain_count <= count else count
                        }
                    )

        return orders.values()

from rest_framework import serializers

from clients.api.serializers.client import ClientSerializer
from products.models import Size
from stores.api.serializers.employee import EmployeeSerializer
from ...models import OrderReturnB2C


class OrderReturnB2CSerializer(serializers.ModelSerializer):
    client_detail = serializers.SerializerMethodField()
    seller_detail = serializers.SerializerMethodField()

    class Meta:
        model = OrderReturnB2C

        fields = [
            'id',
            'date',
            'number',
            'seller',
            'seller_detail',
            'client_detail',
            'amount_full',
            'amount_full_rur',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        size_fields = {}
        for size in Size.objects.all():
            size_fields['count_{}'.format(size.code)] = serializers.IntegerField(
                source='get_count_{}'.format(size.code),
                required=False,
            )
        self.fields.update(size_fields)

    def get_client_detail(self, obj):
        return ClientSerializer(obj.client).data

    def get_seller_detail(self, obj):
        if obj.seller:
            return EmployeeSerializer(
                obj.seller,
                fields=[
                    'id',
                    'first_name',
                    'last_name',
                    'middle_name',
                ]
            ).data
        return None

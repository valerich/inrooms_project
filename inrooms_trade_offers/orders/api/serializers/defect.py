from rest_framework import serializers

from clients.api.serializers import ClientSerializer
from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import DefectAct


class DefectActSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    client_detail = serializers.SerializerMethodField()
    status_detail = serializers.SerializerMethodField()
    amount_full = serializers.SerializerMethodField()
    order_returns = serializers.SerializerMethodField()
    order_returns_is_good = serializers.SerializerMethodField()

    class Meta:
        model = DefectAct

        fields = [
            'id',
            'client',
            'client_detail',
            'status',
            'status_detail',
            'created',
            'modified',
            'amount_full',
            'comment',
            'category',
            'order_returns',
            'order_returns_is_good',
        ]

        extra_kwargs = {
            'client': {'required': False, },
            'category': {'required': False, 'allow_blank': True},
        }

    def get_client_detail(self, obj):
        return ClientSerializer(obj.client).data

    def get_status_detail(self, obj):
        return {'id': obj.status_id,
                'name': obj.status.name,
                'code': obj.status.code,
                'next_statuses': [{
                    'id': s.id,
                    'name': s.name,
                    'button_name': s.button_name,
                    'code': s.code,
                } for s in obj.status.next_status.all()]}

    def get_amount_full(self, obj):
        return obj.get_amount_full()

    def validate_category(self, data):
        if data == '':
            return None
        return data

    def get_order_returns(self, obj):
        data = []
        for orr in obj.orderreturn_set.all():
            data.append({
                'id': orr.id,
                'number': orr.number,
                'has_category': bool(orr.category)
            })
        return data

    def get_order_returns_is_good(self, obj):
        is_good = False
        for orr in obj.orderreturn_set.all():
            is_good = True
            if not(bool(orr.category)):
                return False
        return is_good

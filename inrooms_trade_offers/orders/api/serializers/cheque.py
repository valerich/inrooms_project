from decimal import Decimal
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from clients.api.serializers.client import ClientSerializer
from orders.models.cheques import Cheque, ChequeItem, ChequeItemSize
from stores.api.serializers.employee import EmployeeSerializer


class ChequeItemSizeSerializer(serializers.ModelSerializer):

    size = serializers.SlugRelatedField(slug_field='code', read_only=True)

    class Meta:
        model = ChequeItemSize

        fields = [
            'id',
            'size',
            'quantity',
        ]


class ChequeItemSerializer(serializers.ModelSerializer):
    sizes = ChequeItemSizeSerializer(many=True)
    product_detail = serializers.SerializerMethodField()
    discount_percent = serializers.SerializerMethodField()

    class Meta:
        model = ChequeItem

        fields = [
            'id',
            'product_detail',
            'price',
            'sizes',
            'price_discount',
            'discount_percent',
        ]

    def get_discount_percent(self, obj):
        if obj.price:
            return ((obj.price - obj.price_discount) / obj.price) * 100
        return 0

    def get_product_detail(self, obj):
        return {
            'id': obj.product_id,
            'name': obj.product.name,
            'article': obj.product.article,
            'color_detail': {
                'name': obj.product.color.name,
            },
            'image': self.resize_image(obj.product.images.all().first(), obj.product)
        }

    def resize_image(self, pi, obj):
        if pi:
            try:
                _200x200 = get_thumbnail(pi.image, '400x400', crop="center").url
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
            except IOError as e:
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1500 = request.build_absolute_uri(_950x1200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,
                    '950x1200': _950x1200}
        else:
            return None


class ChequeSerializer(serializers.ModelSerializer):
    items = ChequeItemSerializer(many=True, required=False)
    seller_detail = serializers.SerializerMethodField()
    buyer_card_detail = serializers.SerializerMethodField()
    amount_full = serializers.SerializerMethodField()
    client_detail = serializers.SerializerMethodField()
    kind_detail = serializers.SerializerMethodField()

    class Meta:
        model = Cheque
        fields = [
            'id',
            'created',
            'modified',
            'date',
            'buyer_card_detail',
            'number_1c',
            'seller_detail',
            'items',
            'amount_full',
            'client_detail',
            'kind',
            'kind_detail',
        ]

    def get_seller_detail(self, obj):
        if obj.seller_id:
            return EmployeeSerializer(obj.seller, fields=['id', 'first_name', 'last_name', 'middle_name']).data
        return None

    def get_buyer_card_detail(self, obj):
        if obj.buyer_card:
            data = {
                'id': obj.buyer_card.id,
                'number': obj.buyer_card.number,
            }
            try:
                data['buyer_profile_id'] = obj.buyer_card.buyerprofile.id
            except ObjectDoesNotExist:
                data['buyer_profile_id'] = None
            return data
        else:
            return None

    def get_amount_full(self, obj):
        amount_full = Decimal('0.00')
        for item in obj.items.all():
            amount_full += item.get_price_sum()
        return str(amount_full)

    def get_client_detail(self, obj):
        return ClientSerializer(obj.client, fields=['id', 'name']).data

    def get_kind_detail(self, obj):
        return {
            'code': obj.kind,
            'name': obj.get_kind_display(),
        }
from collections import defaultdict
from decimal import Decimal

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from clients.api.serializers import ClientSerializer
from clients.models import Client
from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import Order


class OrderCreateInvoiceSerializer(serializers.Serializer):
    provider = serializers.PrimaryKeyRelatedField(queryset=Client.objects.all(), required=False)

    def validate(self, data):
        provider = data.get('provider', None)
        if not provider:
            try:
                provider = Client.objects.get(code='ooo-bass-invest')
            except Client.DoesNotExist:
                raise ValidationError('Не передан поставщик')
            data['provider'] = provider
        return data


class OrderSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    status_detail = serializers.SerializerMethodField()
    client_detail = serializers.SerializerMethodField()
    amount_full = serializers.SerializerMethodField()
    main_collection_percentage = serializers.SerializerMethodField()
    items_count = serializers.SerializerMethodField()
    torg_12_category_1 = serializers.CharField(read_only=True)
    torg_12_category_2 = serializers.CharField(read_only=True)
    kind_detail = serializers.SerializerMethodField()
    can_edit = serializers.SerializerMethodField()

    class Meta:
        model = Order

        fields = [
            'id',
            'torg_12_category_1',
            'torg_12_category_2',
            'status',
            'status_detail',
            'created',
            'modified',
            'amount_full',
            'client',
            'client_detail',
            'series_count',
            'comment',
            'main_collection_percentage',
            'items_count',
            'date_shipped',
            'date_recived',
            'kind_detail',
            'kind',
            'can_edit',
            # 'items',
        ]

        extra_kwargs = {
            'client': {'required': False, },
        }

    def get_status_detail(self, obj):
        return {'id': obj.status_id,
                'name': obj.status.name,
                'code': obj.status.code,
                'next_statuses': [{
                    'id': s.id,
                    'name': s.name,
                    'button_name': s.button_name,
                    'code': s.code,
                } for s in obj.status.next_status.all()]}

    def get_client_detail(self, obj):
        if obj.client_id:
            return ClientSerializer(obj.client).data
        else:
            return None

    def get_amount_full(self, obj):
        return obj.get_amount_full()

    def get_main_collection_percentage(self, obj):
        data = "<table>"
        count = 0
        raw_data = defaultdict(int)
        for i in obj.items.all():
            c_name = i.product.main_collection.parent.name if i.product.main_collection.parent else i.product.main_collection.name
            c_count = i.get_quantity()
            count += c_count
            raw_data[c_name] += c_count
        if count:
            for key in sorted(raw_data.keys()):
                k_count = Decimal(raw_data[key] * 100 / count).quantize(Decimal('0.0'))
                data += '<tr style="font-size:12px;"><td><b>{}</b>:</td><td style="text-align:right;padding-left:5px;">{}%</td></tr>'.format(key, k_count)
        data += '</table>'
        return data

    def get_items_count(self, obj):
        return obj.items.count()

    def get_kind_detail(self, obj):
        return {
            'id': obj.kind,
            'name': obj.get_kind_display(),
        }

    def get_can_edit(self, obj):
        user = self.context['request'].user
        kind_map = {
            1: '',
            2: 'defect_',
            3: 'ten_percent_',
        }
        return user.has_perm('auth.can_order_{}edit_{}'.format(kind_map[obj.kind], obj.status.code))
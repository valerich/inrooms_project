from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import DefectImage


class DefectImageSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    thumb = serializers.SerializerMethodField()
    image = serializers.ImageField(read_only=True)

    def get_thumb(self, obj):
        if obj.image is None:
            return None
        try:
            t = get_thumbnail(obj.image, '200x200', crop='center')
        except IOError:
            return None
        if t:
            return t.url
        else:
            return None

    class Meta:
        model = DefectImage
        fields = [
            'id',
            'thumb',
            'image',
        ]

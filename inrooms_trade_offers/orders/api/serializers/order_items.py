from rest_framework import serializers

from products.api.serializers import ProductSerializer
from products.models import Size
from products.services import RemainService
from supply.helpers.supply import SupplyHelper
from ...models import OrderItem


class OrderItemSerializer(serializers.ModelSerializer):
    product_detail = serializers.SerializerMethodField()
    is_delivery_waiting = serializers.SerializerMethodField()
    supply_data = serializers.SerializerMethodField()
    all_remains_data = serializers.SerializerMethodField()

    class Meta:
        model = OrderItem
        fields = [
            'id',
            'price',
            'recommended_retail_price',
            'product',
            'product_detail',
            'series_type',
            'is_delivery_waiting',  # Ожидает поставку?
            'supply_data',
            'all_remains_data',
            'creator',
        ]
        extra_kwargs = {
            'price': {'required': False, },
            'series_type': {'required': False, },
            'creator': {'read_only': True, },
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        size_fields = {}
        for size in Size.objects.all():
            size_fields['count_{}'.format(size.code)] = serializers.IntegerField(
                source='get_count_{}'.format(size.code),
                required=False,
            )
        self.fields.update(size_fields)

    def get_product_detail(self, obj):
        if obj.product_id:
            return ProductSerializer(obj.product, context={'exclude_reserves_for_order_ids': [obj.order.id, ]}).data
        else:
            return None

    def get_supply_data(self, obj):
        if obj.product_id:
            return self.context['supply_data'][obj.product_id]
        return {}

    def get_all_remains_data(self, obj):
        if obj.product_id:
            return self.context['all_remains_data'][obj.product_id]
        return {}

    def get_is_delivery_waiting(self, obj):
        """Ожидаем ли поставки размеров по данному заказу?

        Используется для вывода иконки "ожидаем поставку"
        """
        return obj.orderitemsize_set.filter(is_delivery_reserve=True).exists()

from rest_framework import serializers

from clients.api.serializers.client import ClientSerializer
from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import OrderReturn


class OrderReturnSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    client_detail = serializers.SerializerMethodField()
    category_detail = serializers.SerializerMethodField()
    defect_act_detail = serializers.SerializerMethodField()
    refund_act_detail = serializers.SerializerMethodField()

    class Meta:
        model = OrderReturn

        fields = [
            'id',
            'date',
            'number',
            'seller',
            'client_detail',
            'amount_full',
            'category',
            'category_detail',
            'defect_act_detail',
            'refund_act_detail',
        ]

    def get_client_detail(self, obj):
        return ClientSerializer(obj.client).data

    def get_category_detail(self, obj):
        return {
            'id': obj.category,
            'name': obj.get_category_display(),
        }

    def get_defect_act_detail(self, obj):
        if obj.defect_act:
            return {
                'id': obj.defect_act.id,
            }
        return None

    def get_refund_act_detail(self, obj):
        if obj.refund_act:
            return {
                'id': obj.refund_act.id,
            }
        return None

from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from ...models import RefundActItem


class RefundActItemSerializer(serializers.ModelSerializer):
    order_item_detail = serializers.SerializerMethodField()
    size_detail = serializers.SerializerMethodField()

    class Meta:
        model = RefundActItem
        fields = [
            'id',
            'order_item_detail',
            'size_detail',
            'quantity',
            'comment',
        ]

    def get_order_item_detail(self, obj):
        data = {
            'id': obj.id,
            'order_detail': {
                'id': obj.order_item.order_id,
            },
            'product_detail': {
                'id': obj.order_item.product_id,
                'name': obj.order_item.product.name,
                'article': obj.order_item.product.article,
                'color': {
                    'id': obj.order_item.product.color_id,
                    'name': obj.order_item.product.color.name,
                },
                'image': self.get_image(obj.order_item.product)
            },
            'price': obj.order_item.price,
        }
        return data

    def get_size_detail(self, obj):
        return {
            'id': obj.size_id,
            'name': obj.size.name,
        }

    def get_image(self, obj):
        pi = obj.images.all().first()
        return self.resize_image(pi, obj)

    def resize_image(self, pi, obj):
        if pi:
            try:
                _200x200 = get_thumbnail(pi.image, '400x400', crop="center").url
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
            except IOError as e:
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1500 = request.build_absolute_uri(_950x1200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,
                    '950x1200': _950x1200}
        else:
            return None
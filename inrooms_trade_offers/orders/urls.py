from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^order/(?P<object_id>\d+)/export/$', views.OrderExport.as_view(), name='order-export'),
    url(r'^defect_act/(?P<object_id>\d+)/export/$', views.DefectActExport.as_view(), name='defect_act-export'),
    url(r'^refund_act/(?P<object_id>\d+)/export/$', views.RefundActExport.as_view(), name='refund_act-export'),
]

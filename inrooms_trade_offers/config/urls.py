# coding: utf-8
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic.base import TemplateView
from django.views.static import serve

urlpatterns = [
    url(r'^', include('extensions.authtools.urls')),
    url(r'^multimedia/', include('multimedia.urls')),
    url(r'^autocomplete/', include('autocomplete_light.urls')),

    # Admin URLs
    # url(r'^admin/rq/', include('extensions.django_rq.urls')),
    # url(r'^admin/rq/scheduler/', include('extensions.rq_scheduler.urls', namespace='rq_scheduler')),
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^offers/', include('offers.urls', namespace='offers')),
    url(r'^offer_requests/', include('offer_requests.urls', namespace='offer_requests')),
    url(r'^products/', include('products.urls', namespace='products')),
    url(r'^cart/', include('cart.urls', namespace='cart')),
    url(r'^orders/', include('orders.urls', namespace='orders')),
    url(r'^supply/', include('supply.urls', namespace='supply')),
    url(r'^invoices/', include('invoices.urls', namespace='invoices')),
    url(r'^manufacturing/', include('manufacturing.urls', namespace='manufacturing')),
    url(r'^config/', include(admin.site.urls)),
    url(r'^summernote/', include('django_summernote.urls')),
    url(r'^holders/', include('holders.urls', namespace='holders')),

    url(r'^public/api/', include('urls_public_api', namespace='public_api')),
    url(r'^api/', include('urls_api', namespace='api')),
]

admin.site.site_header = settings.PROJECT_NAME
admin.site.index_title = 'Базовые операции'

if settings.DEBUG:
    urlpatterns += [
        # Testing 404 and 500 error pages
        url(r'^404/$', TemplateView.as_view(template_name='404.html'), name='404'),
        url(r'^500/$', TemplateView.as_view(template_name='500.html'), name='500'),
        url(r'^favicon.ico$', serve, {'document_root': settings.STATIC_ROOT, 'path': "img/favicon.ico"}),
    ]

    try:
        from django.conf.urls.static import static
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

        import debug_toolbar
        urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls))]

    # Should only occur when debug mode is on for production testing
    except ImportError as e:
        import logging
        l = logging.getLogger(__name__)
        l.warning(e)

urlpatterns += [
    # Для всех остальных урлов отдаем пустой шаблон
    url(r'^', include('core.urls', namespace='core')),
]

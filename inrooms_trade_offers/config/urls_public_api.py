from django.conf.urls import include, url

urlpatterns = [
    url(r'^manufacturing/', include('manufacturing.api_public.urls', namespace='manufacturing')),
    url(r'^orders/', include('orders.api_public.urls', namespace='orders')),
    url(r'^payments/', include('payments.api_public.urls', namespace='payments')),
    url(r'^products/', include('products.api_public.urls', namespace='products')),
    url(r'^sms/', include('sms.api_public.urls', namespace='sms')),
    url(r'^stores/', include('stores.api_public.urls', namespace='stores')),
]

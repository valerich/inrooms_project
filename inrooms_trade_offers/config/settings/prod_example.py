from __future__ import absolute_import

from .base import *

DEBUG = False

ALLOWED_HOSTS = ['*',]

BROKER_HOST = "127.0.0.1"
BROKER_PORT = 5672   # default RabbitMQ listening port
BROKER_USER = "inrooms"
BROKER_PASSWORD = "inrooms"
BROKER_VHOST = "inrooms"
CELERY_BACKEND = "amqp"  # telling Celery to report the results back to RabbitMQ
CELERY_RESULT_DBURI = ""

########## DATABASE CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'inrooms',
        'USER': 'inrooms',
        'PASSWORD': 'inrooms',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
########## END DATABASE CONFIGURATION

STATIC_ROOT = '/www/static/'
MEDIA_ROOT = '/www/media/'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

RQ_QUEUES = {
    'default': {
        'USE_REDIS_CACHE': 'redis'
    },
    'high': {
        'USE_REDIS_CACHE': 'redis'
    },
    'low': {
        'USE_REDIS_CACHE': 'redis'
    }
}

RQ_SHOW_ADMIN_LINK = True
########## END REDIS QUEUE CONFIGURATION

INSTALLED_APPS += (
    'raven.contrib.django.raven_compat',
)

RAVEN_CONFIG = {
    'dsn': 'http://',
    'timeout': 5,
    'register_signals': True,
    # 'release': raven.fetch_git_sha(PROJECT_ROOT),
}

JS_DSN = ''

########## LOGGING CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#logging
LOGGERS = {
    # Log requests locally without [INFO] tag
    'werkzeug': {
        'handlers': ['default'],
        'level': 'DEBUG',
        'propagate': False,
    },
    # Log queue workers to console and file on development
    'rq.worker': {
        'handlers': ['default', 'file_log'],
        'level': 'DEBUG',
        'propagate': False,
    },
}

WKHTMLTOPDF_CMD = '/usr/local/bin/wkhtmltopdf.sh'

WKHTMLTOPDF_CMD_OPTIONS = {
    'quiet': True,
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            'tags': {'custom-tag': 'x'},
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}

SERVICE_1C = {
    'export_products': {
        'url': 'http://localhost?wsdl',
        'username': 'username',
        'password': 'password',
    }
}

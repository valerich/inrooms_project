"""
Base settings and globals.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

import sys
from datetime import timedelta
from os.path import abspath, basename, dirname, join, normpath
from sys import path

# Celery scheduler
# See: http://celery.readthedocs.org/en/latest/userguide/periodic-tasks.html
from celery.schedules import crontab

########## PATH CONFIGURATION
# Absolute filesystem path to the config directory:

CONFIG_ROOT = dirname(dirname(abspath(__file__)))

# Absolute filesystem path to the project directory:
PROJECT_ROOT = dirname(CONFIG_ROOT)

# Absolute filesystem path to the django repo directory:
DJANGO_ROOT = dirname(PROJECT_ROOT)

# Project name:
PROJECT_NAME = 'InroomsTO'

# Project folder:
PROJECT_FOLDER = basename(PROJECT_ROOT)

# Project domain:
PROJECT_DOMAIN = 'inrooms.net'

# Add our project to our pythonpath, this way we don't need to type our project
# name in our dotted import paths:
path.append(CONFIG_ROOT)
########## END PATH CONFIGURATION

USE_THOUSAND_SEPARATOR = True
THOUSAND_SEPARATOR = ' '


IS_TESTING = len(sys.argv) > 1 and sys.argv[1] == 'test'


########## EMAIL CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#email-subject-prefix
EMAIL_SUBJECT_PREFIX = '[%s] ' % PROJECT_NAME

# https://docs.djangoproject.com/en/dev/ref/settings/#server-email
SERVER_EMAIL = 'Serverbot <dev@%s>' % PROJECT_DOMAIN

DEFAULT_FROM_EMAIL = 'robot@inrooms.net'

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 587

########## END EMAIL CONFIGURATION


########## MANAGER CONFIGURATION
# See https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ('Yakov Istomin', 'Yakov Istomin <yakov@istomin.me>'),
)

# https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS
########## END MANAGER CONFIGURATION


########## APP CONFIGURATION
DJANGO_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'autocomplete_light',
    'django.contrib.admin',
    'django.contrib.admindocs',
)

THIRD_PARTY_APPS = (
    'authtools',
    'django_extensions',
    'django_summernote',
    'rest_framework',
    'rest_framework.authtoken',
    'sorl.thumbnail',
    'wkhtmltopdf',
    'prettyjson',
    'nested_inline',
    'phonenumber_field',
    'corsheaders',
)

PROJECT_APPS = (
    'accounts',
    'brands',
    'care',
    'cart',
    'clients',
    'client_cards',
    'comments',
    'consistency',
    'core',
    'documents',
    'factories',
    'history',
    'holders',
    'manufacturing',
    'sms',
    'multimedia',
    'offers',
    'offer_requests',
    'payments',
    'products',
    'purchases',
    'orders',
    'service_1c',
    'stores',
    'supply',
    'tasks',
    'warehouses',
    'prices',
    'providers',
    'invoices',
)

EXTENSION_APPS = (
    'extensions.authtools',
)

# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + PROJECT_APPS + EXTENSION_APPS
########## END APP CONFIGURATION

WKHTMLTOPDF_CMD_OPTIONS = {
    'quiet': True,
}

########## MIDDLEWARE CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware-classes
MIDDLEWARE_CLASSES = (
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'extensions.authtools.middlewares.LoginRequiredMiddleware',
)
########## END MIDDLEWARE CONFIGURATION


########## MIGRATIONS CONFIGURATION
MIGRATION_MODULES = {
    'sites': 'extensions.sites.migrations'
}
########## END MIGRATIONS CONFIGURATION


########## DEBUG CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = STAGING = False
########## END DEBUG CONFIGURATION


########## SECRET CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key should only be used for development and testing.
SECRET_KEY = r"lle*l7qn&!tog)$1n$=#op1rst%e!7k8t-k@wm&&v@msnuo6ud"
########## END SECRET CONFIGURATION


########## FIXTURE CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    # normpath(join(PROJECT_ROOT, 'fixtures')),
)
########## END FIXTURE CONFIGURATION


########## GENERAL CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = 'Europe/Moscow'

# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'ru-RU'

# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True
########## END GENERAL CONFIGURATION


########## TEMPLATE CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/templates/
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            normpath(join(PROJECT_ROOT, 'templates')),
            normpath(join(PROJECT_ROOT, 'extensions')),
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                'core.context_processors.base',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'string_if_invalid': 'NULL',
        },
    },
]
# TEMPLATE_LOADERS = (
#     'django.template.loaders.filesystem.load_template_source',
#     'django.template.loaders.app_directories.load_template_source',
# )
########## END TEMPLATE CONFIGURATION


########## MEDIA CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = normpath(join(PROJECT_ROOT, 'media'))

# https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'
########## END MEDIA CONFIGURATION


########## STATIC FILE CONFIGURATION

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'

# https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = normpath(join(PROJECT_ROOT, 'public'))

# https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

STATIC_DOCUMENT_ROOT = normpath(join(PROJECT_ROOT, 'static_doc/'))

# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    normpath(join(PROJECT_ROOT, 'static')),
    normpath(join(PROJECT_ROOT, '../front/dist/')),
)

STATICFILES_FINDERS_IGNORE = [
    '*.scss',
    '*.coffee',
    '*.map',
    '*.html',
    '*.txt',
    '*tests*',
    '*uncompressed*',
]

# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# STATICFILES_STORAGE = 'pipeline.storage.PipelineStorage'
########## END STATIC FILE CONFIGURATION


########## URL CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = 'config.urls'
########## END URL CONFIGURATION


########## LOGIN/LOGOUT CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#login-redirect-url
LOGIN_REDIRECT_URL = '/'

# https://docs.djangoproject.com/en/dev/ref/settings/#login-url
LOGIN_URL = '/login/'

# https://docs.djangoproject.com/en/dev/ref/settings/#logout-url
LOGOUT_URL = '/logout/'

LOGIN_EXEMPT_URLS = [
    'password_reset',
    'logout',
    'media',
    'public',
    'public/api/stores/test_s/',
]
########## END LOGIN/LOGOUT CONFIGURATION



CELERYBEAT_SCHEDULE = {
    'get-cyrrency-every-day': {
        'task': 'payments.get_currency',
        'schedule': crontab(hour=0, minute=1)
    },
    'parse-1s-remains-every-5-min': {
        'task': 'service_1c.sync_remains',
        'schedule': timedelta(minutes=1)
    },
    'parse-scanner-data-every-5-min': {
        'task': 'service_1c.sync_scanner_data',
        'schedule': timedelta(minutes=1)
    },
    'parse-1s-supply-status-every-5-min': {
        'task': 'service_1c.sync_supply_status',
        'schedule': timedelta(minutes=5)
    },
    'parse-1s-defect-act-status-every-5-min': {
        'task': 'service_1c.sync_defect_act_status',
        'schedule': timedelta(minutes=5)
    },
    'parse-1s-refund-act-status-every-5-min': {
        'task': 'service_1c.sync_refund_act_status',
        'schedule': timedelta(minutes=5)
    },
    'parse-1s-order-status-every-5-min': {
        'task': 'service_1c.sync_order_status',
        'schedule': timedelta(minutes=5)
    },
    'parse-1s-manufacturing-order-status-every-5-min': {
        'task': 'service_1c.sync_manufacturing_order_status',
        'schedule': timedelta(minutes=5)
    },
    'parse-1s-manufacturing-order-acceptance-every-5-min': {
        'task': 'service_1c.manufacturing_order_acceptance_import',
        'schedule': timedelta(minutes=5)
    },
    '1s-order-import-every-5-min': {
        'task': 'service_1c.order_import',
        'schedule': timedelta(minutes=5)
    },
    '1s-supply-import-every-5-min': {
        'task': 'service_1c.supply_import',
        'schedule': timedelta(minutes=5)
    },
    '1s-order-b2c-import-every-1-min': {
        'task': 'service_1c.order_b2c_import',
        'schedule': timedelta(minutes=1)
    },
    '1s-order-return-b2c-import-every-1-min': {
        'task': 'service_1c.order_return_b2c_import',
        'schedule': timedelta(minutes=1)
    },
    '1s-order-return-import-every-1-min': {
        'task': 'service_1c.order_return_import',
        'schedule': timedelta(minutes=1)
    },
    '1m-sms-send-messages': {
        'task': 'sms.send_messages',
        'schedule': timedelta(minutes=1)
    },
    '1m-sms-send-messages_sms': {
        'task': 'sms.send_messages_sms',
        'schedule': timedelta(minutes=1)
    },
    'set-is-new-collection-every-day': {
        'task': 'products.set_is_new_collection',
        'schedule': crontab(hour=4, minute=1)
    }
}

CELERY_ROUTES = {
    'payments.get_currency': {'queue': 'celery'},
    'products.set_is_new_collection': {'queue': 'celery'},
    'multimedia.convert_to_mp3': {'queue': 'celery'},
    'products.import_remain_history': {'queue': '1c_tasks'},
    'service_1c.sync_remains': {'queue': '1c_tasks'},
    'service_1c.sync_scanner_data': {'queue': '1c_tasks'},
    'service_1c.sync_supply_status': {'queue': '1c_tasks'},
    'service_1c.sync_defect_act_status': {'queue': '1c_tasks'},
    'service_1c.sync_refund_act_status': {'queue': '1c_tasks'},
    'service_1c.sync_manufacturing_order_status': {'queue': '1c_tasks'},
    'service_1c.order_import': {'queue': '1c_tasks'},
    'service_1c.supply_import': {'queue': '1c_tasks'},
    'service_1c.order_b2c_import': {'queue': '1c_tasks'},
    'service_1c.order_return_b2c_import': {'queue': '1c_tasks'},
    'service_1c.order_return_import': {'queue': '1c_tasks'},
    'service_1c.notify_1c_product_change': {'queue': '1c_tasks'},
    'service_1c.notify_1c_client_change': {'queue': '1c_tasks'},
    'service_1c.notify_1c_supplier_change': {'queue': '1c_tasks'},
    'service_1c.notify_1c_store_change': {'queue': '1c_tasks'},
    'service_1c.notify_1c_employee_change': {'queue': '1c_tasks'},
    'service_1c.notify_1c_offers_change': {'queue': '1c_tasks'},
    'service_1c.manufacturing_order_acceptance_import': {'queue': '1c_tasks'},
    'sms.create_template_message': {'queue': 'celery'},
    'sms.create_template_message_sms': {'queue': 'celery'},
    'sms.send_messages': {'queue': 'celery'},
    'sms.send_messages_sms': {'query': 'celery'},
}


########## WSGI CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'
########## END WSGI CONFIGURATION


########## USER MODEL CONFIGURATION
AUTH_USER_MODEL = 'accounts.User'
########## END USER MODEL CONFIGURATION


########## TESTING CONFIGURATION
# TEST_RUNNER = 'django.test.runner.DiscoverRunner'
TEST_RUNNER = 'rainbowtests.test.runner.RainbowDiscoverRunner'

########## END TESTING CONFIGURATION


DJANGORESIZED_DEFAULT_SIZE = [3072, 2479]
DJANGORESIZED_DEFAULT_QUALITY = 90
DJANGORESIZED_DEFAULT_KEEP_META = False


REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',),
    'DEFAULT_PAGINATION_CLASS': 'core.utils.paginator.MyApiPagination',
    'DATE_INPUT_FORMATS': ['%d.%m.%Y', '%Y-%m-%d', ],
    'DATETIME_INPUT_FORMATS': ('%d.%m.%Y %H:%M', '%d.%m.%Y', 'iso-8601'),
    'DATE_FORMAT': '%d.%m.%Y',
    'PAGE_SIZE': 50,
    'ORDERING_PARAM': 'sort_by',
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'core.auth.TokenAuthentication',
    ),
}

SUMMERNOTE_CONFIG = {
    'toolbar': [
        ["style", ["style"]],
        ["style", ["bold", "italic", "underline", "clear"]],
        ["fontsize", ["fontsize"]],
        ["color", ["color"]],
        ["para", ["ul", "ol", "paragraph"]],
        ["height", ["height"]],
        ["table", ["table"]],
        ['view', ['codeview']],
    ],
}


SERVICE_1C = {
    'inrooms_1c_wsdl': {
        'url': 'http://localhost?wsdl',
        'username': 'username',
        'password': 'password',
    },
    'op_inrooms_1c_wsdl': {
        'url': 'http://localhost?wsdl',
        'username': 'username',
        'password': 'password',
    }
}


########## LOGGING CONFIGURATION
# https://docs.djangoproject.com/en/dev/ref/settings/#logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'production_only': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'development_only': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'readable_sql': {
            '()': 'project_runpy.ReadableSqlFilter',
        },
    },
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(levelname)-8s [%(name)s:%(lineno)s] %(message)s',
            'datefmt': '%m/%d/%Y %H:%M:%S',
        },
        'simple': {
            'format': '%(levelname)-8s [%(name)s:%(lineno)s] %(message)s',
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'default': {
            'level': 'DEBUG',
            'class': 'config.lib.colorstreamhandler.ColorStreamHandler',
        },
        'console_dev': {
            'level': 'DEBUG',
            'filters': ['development_only'],
            'class': 'config.lib.colorstreamhandler.ColorStreamHandler',
            'formatter': 'simple',
        },
        'console_prod': {
            'level': 'INFO',
            'filters': ['production_only'],
            'class': 'config.lib.colorstreamhandler.ColorStreamHandler',
            'formatter': 'simple',
        },
        'file_log': {
            'level': 'DEBUG',
            'filters': ['development_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(DJANGO_ROOT, 'logs/log.log'),
            'maxBytes': 1024 * 1024,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'file_sql': {
            'level': 'DEBUG',
            'filters': ['development_only'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(DJANGO_ROOT, 'logs/sql.log'),
            'maxBytes': 1024 * 1024,
            'backupCount': 3,
            'formatter': 'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['production_only'],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
    },
    # Catch-all modules that use logging
    # Writes to console and file on development, only to console on production
    'root': {
        'handlers': ['console_dev', 'console_prod', 'file_log'],
        'level': 'DEBUG',
    },
    'loggers': {
        # Write all SQL queries to a file
        'django.db.backends': {
            'handlers': ['file_sql'],
            'filters': ['readable_sql'],
            'level': 'DEBUG',
            'propagate': False,
        },
        # Email admins when 500 error occurs
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
    }
}
########## END LOGGING CONFIGURATION

JS_DSN = 'test'

TELEBOT_TOKEN = ''
TELEGRAM_CHAT_ID = ''

# Константы для настройки библиотеки
SMSC_LOGIN = "login"			# логин клиента
SMSC_PASSWORD = "password"	# пароль или MD5-хеш пароля в нижнем регистре
SMSC_POST = False				# использовать метод POST
SMSC_HTTPS = False				# использовать HTTPS протокол
SMSC_CHARSET = "utf-8"			# кодировка сообщения (windows-1251 или koi8-r), по умолчанию используется utf-8
SMSC_DEBUG = False				# флаг отладки

# Константы для отправки SMS по SMTP
SMTP_FROM = "api@smsc.ru"		# e-mail адрес отправителя
SMTP_SERVER = "send.smsc.ru"	# адрес smtp сервера
SMTP_LOGIN = ""					# логин для smtp сервера
SMTP_PASSWORD = ""				# пароль для smtp сервера

SMS_SENDER_NAME = 'ZARACITY'

import factory
import factory.django
import factory.fuzzy

from accounts.factories import UserFactory
from .models import Comment


class CommentFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    body = factory.fuzzy.FuzzyText()

    class Meta:
        model = Comment

from django.contrib import admin

from .forms import CommentAdminForm
from .models import Comment


class CommentAdmin(admin.ModelAdmin):
    list_display = ('user', 'created', 'modified', 'content_object')
    form = CommentAdminForm


for model_or_iterable, admin_class in (
    (Comment, CommentAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

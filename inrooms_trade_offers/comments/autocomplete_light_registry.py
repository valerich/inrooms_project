from autocomplete_light import shortcuts as autocomplete_light

from offers.models import Offer
from products.models import Product


class AutocompleteCommentItems(autocomplete_light.AutocompleteGenericBase):
    choices = (
        Product.objects.all(),
        Offer.objects.all(),
    )

    search_fields = (
        ('name', 'article'),
        ('id', ),
    )

    attrs = {
        'data-autocomplete-minimum-characters': 0
    }


autocomplete_light.register(AutocompleteCommentItems)


# AUTOCOMPLETE_LIST = (
#     (ContentType, ContentTypeCommentAutocomplete),
# )
#
# for autocomplete_data in AUTOCOMPLETE_LIST:
#     autocomplete_light.register(*autocomplete_data)

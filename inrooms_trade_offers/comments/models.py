from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django_extensions.db.models import TimeStampedModel

from accounts.models import User


class Comment(TimeStampedModel):
    user = models.ForeignKey('accounts.User', verbose_name='Пользователь')
    body = models.TextField('Сообщение')
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    is_new = models.BooleanField('Новое', default=True)

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ('-modified', )

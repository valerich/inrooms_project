import django_filters
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import filters, status
from rest_framework.decorators import detail_route
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ..serializers import CommentSerializer
from ...models import Comment


class CommentFilterSet(django_filters.FilterSet):
    content_type__model = django_filters.CharFilter(name="content_type__model")
    content_type__app_label = django_filters.CharFilter(name="content_type__app_label")

    class Meta:
        model = Comment
        fields = [
            'modified',
            'created',
            'user',
            'content_type',
            'object_id',
            'is_new',
        ]


class CommentViewSet(ModelViewSet):
    serializer_class = CommentSerializer
    filter_class = CommentFilterSet
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,)
    search_fields = ('body', )
    ordering_fields = (
        'user',
        'created',
        'modified',
    )

    def get_queryset(self):
        qs = Comment.objects.order_by('-modified')
        qs = qs.select_related(
            'user'
        )
        return qs

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @detail_route(methods=['post'])
    def set_is_new(self, request, pk=None):
        action = request.data.get('action', None)
        is_new = None
        if action == 'read':
            is_new = False
        elif action == 'unread':
            is_new = True
        if isinstance(is_new, bool):
            comment = self.get_object()
            comment.is_new = is_new
            comment.save()
            return Response(CommentSerializer(comment).data)

        return Response(status=status.HTTP_400_BAD_REQUEST)


class CommentModelViewSet(CommentViewSet):
    http_method_names = ['get', 'post', ]

    def dispatch(self, request, app_label, model, object_id, *args, **kwargs):
        self.content_type = get_object_or_404(ContentType, app_label=app_label, model=model)
        try:
            self.content_object = self.content_type.get_object_for_this_type(id=object_id)
        except ObjectDoesNotExist:
            return Response(status=404)
        return super(CommentModelViewSet, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super(CommentModelViewSet, self).get_queryset()
        qs = qs.filter(content_type=self.content_type, object_id=self.content_object.id)
        return qs

    def perform_create(self, serializer):
        serializer.save(user=self.request.user,
                        content_object=self.content_object)

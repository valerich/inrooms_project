from django.conf.urls import include, url
from rest_framework import routers

from . import views

router = routers.SimpleRouter()

router.register(r'comment', views.CommentViewSet, base_name='comment')
router.register(r'comment/(?P<app_label>.*)/(?P<model>.*)/(?P<object_id>\d+)', views.CommentModelViewSet, base_name='comment_model')


urlpatterns = [
    url(r'^', include(router.urls)),
]

from django.utils.html import strip_tags
from rest_framework import serializers

from products.models import Product
from ...models import Comment


class CommentObjectRelatedField(serializers.RelatedField):
    """
    A custom field to use for the `tagged_object` generic relationship.
    """

    def to_representation(self, value):
        """
        Serialize tagged objects to a simple textual representation.
        """
        if isinstance(value, Product):
            return {
                'id': value.id,
                'name': value.name
            }
        raise Exception('Unexpected type of tagged object')


class CommentSerializer(serializers.ModelSerializer):
    user_detail = serializers.SerializerMethodField()
    created = serializers.DateTimeField(read_only=True)
    modified = serializers.DateTimeField(read_only=True)
    content_object_detail = serializers.SerializerMethodField()
    short_body = serializers.SerializerMethodField()
    can_change_is_new = serializers.SerializerMethodField()

    class Meta:
        model = Comment

        fields = [
            'id',
            'created',
            'modified',
            'body',
            'short_body',
            'user_detail',
            # 'content_type',
            'content_object_detail',
            'is_new',
            'can_change_is_new',
        ]

    def __init__(self, *args, **kwargs):
        if 'request_user' in kwargs:
            self.request_user = kwargs.pop('request_user')
        super(CommentSerializer, self).__init__(*args, **kwargs)

    def get_user_detail(self, obj):
        return {'id': obj.user_id,
                'name': obj.user.name}

    def get_content_object_detail(self, obj):
        if obj.content_type.app_label == 'orders' and obj.content_type.model == 'order':
            content_object_name = 'Заказ #{}'.format(obj.object_id)
        else:
            content_object_name = obj.content_object.name
        return {
            'object': {
                'id': obj.object_id,
                'name': content_object_name, },
            'content_type': {
                'id': obj.content_type_id,
                'app_label': obj.content_type.app_label,
                'model': obj.content_type.model, }}

    def get_short_body(self, obj):
        body = strip_tags(obj.body).replace('&nbsp;', ' ')
        if len(body) > 50:
            body = body[:47] + '...'
        return body

    def get_can_change_is_new(self, obj):
        if 'request' in self.context:
            return obj.user_id != self.context['request'].user.id
        elif hasattr(self, 'request_user'):
            return obj.user_id != self.request_user.id
        return False

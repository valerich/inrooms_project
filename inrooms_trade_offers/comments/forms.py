from autocomplete_light import shortcuts as autocomplete_light
from django_summernote.widgets import SummernoteWidget


class CommentAdminForm(autocomplete_light.ModelForm):
    content_object = autocomplete_light.GenericModelChoiceField('AutocompleteCommentItems')

    class Meta:
        widgets = {
            'body': SummernoteWidget(),
            'user': autocomplete_light.ChoiceWidget('UserAutocomplete'),
        }
        fields = [
            'body',
            'content_object',
            'user',
        ]

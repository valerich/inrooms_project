from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'comments'
    verbose_name = 'Комментарии'

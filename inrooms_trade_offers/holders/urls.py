from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^last_chance/$', views.LastChancePrintView.as_view(), name='last-chance-print'),
    url(r'^showcase/$', views.ShowcasePrintView.as_view(), name='showcase-print'),
    url(r'^mode/$', views.ModePrintView.as_view(), name='mode-print'),
]

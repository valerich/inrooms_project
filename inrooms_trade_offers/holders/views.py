import datetime
from rest_framework import serializers
from rest_framework import response
from django.shortcuts import get_object_or_404
from rest_framework.status import HTTP_400_BAD_REQUEST
from wkhtmltopdf.views import PDFTemplateView


class LastChancePrintView(PDFTemplateView):
    template_name = 'holders/last_chance.html'
    filename = 'last_chance.pdf'
    filename_docx = 'last_chance.docx'


class ShowCasePrintSerializer(serializers.Serializer):
    product1 = serializers.CharField(required=False)
    price1 = serializers.CharField(required=False)
    product2 = serializers.CharField(required=False)
    price2 = serializers.CharField(required=False)
    product3 = serializers.CharField(required=False)
    price3 = serializers.CharField(required=False)
    product4 = serializers.CharField(required=False)
    price4 = serializers.CharField(required=False)
    product5 = serializers.CharField(required=False)
    price5 = serializers.CharField(required=False)

    class Meta:
        fields = (
            'product1',
            'price1',
            'product2',
            'price2',
            'product3',
            'price3',
            'product4',
            'price4',
            'product5',
            'price5',
        )

class ShowcasePrintView(PDFTemplateView):
    template_name = 'holders/showcase.html'
    filename = 'showcase.pdf'

    def get(self, request, *args, **kwargs):
        self.serializer = ShowCasePrintSerializer(data=request.GET)
        if self.serializer.is_valid(False):
            return super().get(request, *args, **kwargs)
        else:
            return response.Response(status=HTTP_400_BAD_REQUEST, data=self.serializer.errors)

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(*args, **kwargs)
        ctx['product1'] = self.serializer.validated_data.get('product1', None)
        ctx['price1'] = self.serializer.validated_data.get('price1', None)
        ctx['product2'] = self.serializer.validated_data.get('product2', None)
        ctx['price2'] = self.serializer.validated_data.get('price2', None)
        ctx['product3'] = self.serializer.validated_data.get('product3', None)
        ctx['price3'] = self.serializer.validated_data.get('price3', None)
        ctx['product4'] = self.serializer.validated_data.get('product4', None)
        ctx['price4'] = self.serializer.validated_data.get('price4', None)
        ctx['product5'] = self.serializer.validated_data.get('product5', None)
        ctx['price5'] = self.serializer.validated_data.get('price5', None)
        ctx['product_count'] = self.get_product_count()
        return ctx

    def get_product_count(self):
        count = 0
        for i in range(1, 6):
            if self.serializer.validated_data.get('product{}'.format(i), None):
                count += 1
        return count


class ModePrintSerializer(serializers.Serializer):
    day_from = serializers.CharField(required=False)
    day_to = serializers.CharField(required=False)
    time_from = serializers.CharField(required=False)
    time_to = serializers.CharField(required=False)

    class Meta:
        fields = [
            'day_from',
            'day_to',
            'time_from',
            'time_to',
        ]


class ModePrintView(PDFTemplateView):
    template_name = 'holders/mode.html'
    filename = 'mode.pdf'

    def get(self, request, *args, **kwargs):
        self.serializer = ModePrintSerializer(data=request.GET)
        if self.serializer.is_valid(False):
            return super().get(request, *args, **kwargs)
        else:
            return response.Response(status=HTTP_400_BAD_REQUEST, data=self.serializer.errors)

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(*args, **kwargs)
        ctx['day_from'] = self.serializer.validated_data.get('day_from', '')
        ctx['day_to'] = self.serializer.validated_data.get('day_to', '')
        ctx['time_from'] = self.serializer.validated_data.get('time_from', '')
        ctx['time_to'] = self.serializer.validated_data.get('time_to', '')
        return ctx

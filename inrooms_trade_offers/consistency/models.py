from django.db import models

from model_utils.choices import Choices


CONSISTENCY_TYPE = Choices(
    ('polyester', 'POLIESTER - POLYESTER - POLIÉSTER - POLIESTERE - ΠΟΛYΕΣΤΕΡΑΣ - POLIÉZSTER'),
    ('modal', 'Modal, Moadal'),
    ('silk', 'Seta, Silk, Seide, Soie'),
    ('cotton', 'ALGODON - COTON - COTTON - ALGODÃO - KATOEN - COTONE - BAMBAKI - PAMUT - BAUMWOLLE - BAVLNA - BAWEŁNA - BOMULD - BOMBAŽ'),
    ('elastano', 'ELASTANO - ÉLASTHANNE - ELASTANE - ELASTAAN - EΛΑΣΤΙΝΗ - ELASTÁN - ELASTAN'),
    ('nylon', 'NAILON - NYLON - NYLON (So plo Brasil Poliamida) - NAIΛON - NEJLON - NAJLON'),
    ('acrylic', 'ACRILICO - ACRYLIQUE - ACRYLIC - ACRILICO - ACRYL - AKPYNKO - AKRYL - AKRIL - AKRILIK'),
    ('liocel', 'lYCOELL - LYOCELL - LIOCEL - SELAT SELULOSA'),
    ('poliuretan', 'POLIURETANO - POLYURETHANE - POLYURETHANE - POLYURETAAN - ΠOΛYOYPEOANIO - POLYURETHAN - POLIURETAN'),
    ('viscose', 'VISCOSA - VISCOSE - BIΣKOZH - VISZKOZ - VISKOSE - VISKOZA - WISKOZA - VISKOZA'),
    ('lino', 'LINO - LIN - LINEN - LINHO - LINNEN - ΛINO - LEINEN - HØR - LAN'),
    ('wool', 'LANA - LAINE - WOOL - LÃ - WOL - MAΛΛI - WOLLE - ULD - VOLNA'),
    ('down', 'Down'),
    ('aceteto', 'Acetato, Acetate, Acetat, Acetate'),
    ('fibra_metalizada', 'Fibra Metalizada'),
    ('otras_fibras', 'Otras Fibras'),
    ('mulberry_silce', 'Mulberry Silce'),
    ('poliamid', 'Naylon, Polyamide，nylon'),
    ('linen', 'Lino, Linen-Flax, Flachs, Linen, Lin'),
)


class ConsistencyItem(models.Model):
    name = models.CharField('Название', max_length=50)
    description = models.TextField('Описание', help_text='Выводится при печати этикеток например')
    code = models.CharField('Код', unique=True, help_text='Используется для переноса старых значений ухода', max_length=50)
    zara_code = models.CharField('Код zara', blank=True, max_length=100)

    class Meta:
        verbose_name = 'Элемент ухода'
        verbose_name_plural = 'Элементы ухода'
        ordering = ('name', )

    def __str__(self):
        return self.name


class ConsistencyMixin(models.Model):
    consistency_type1 = models.ForeignKey(ConsistencyItem, verbose_name='Уход 1', blank=True, null=True, related_name='%(class)s_consistency_type1', on_delete=models.PROTECT)
    consistency_value1 = models.PositiveSmallIntegerField(default=0)

    consistency_type2 = models.ForeignKey(ConsistencyItem, verbose_name='Уход 2', blank=True, null=True, related_name='%(class)s_consistency_type2', on_delete=models.PROTECT)
    consistency_value2 = models.PositiveSmallIntegerField(default=0)

    consistency_type3 = models.ForeignKey(ConsistencyItem, verbose_name='Уход 3', blank=True, null=True, related_name='%(class)s_consistency_type3', on_delete=models.PROTECT)
    consistency_value3 = models.PositiveSmallIntegerField(default=0)

    consistency_type4 = models.ForeignKey(ConsistencyItem, verbose_name='Уход 4', blank=True, null=True, related_name='%(class)s_consistency_type4', on_delete=models.PROTECT)
    consistency_value4 = models.PositiveSmallIntegerField(default=0)

    consistency_type5 = models.ForeignKey(ConsistencyItem, verbose_name='Уход 5', blank=True, null=True, related_name='%(class)s_consistency_type5', on_delete=models.PROTECT)
    consistency_value5 = models.PositiveSmallIntegerField(default=0)

    class Meta:
        abstract = True

    def get_full_consistency(self):
        return ', '.join(self.get_full_consistency_list())

    def get_full_consistency_list(self):
        full_consistency = []
        for ii in range(1, 5):
            if getattr(self, 'consistency_value{}'.format(ii)) and getattr(self, 'consistency_type{}'.format(ii)):
                full_consistency.append(
                    '{}% {}'.format(
                        getattr(self, 'consistency_value{}'.format(ii)),
                        getattr(self, 'consistency_type{}'.format(ii)).name
                    )
                )
        return full_consistency

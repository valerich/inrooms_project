from rest_framework import serializers

from ...models import ConsistencyItem


class ConsistencyItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = ConsistencyItem

        fields = [
            'id',
            'name',
            'description',
        ]

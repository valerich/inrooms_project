from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from ..serializers import ConsistencyItemSerializer
from ...models import ConsistencyItem


class ConsistencyItemViewSet(ModelViewSet):
    serializer_class = ConsistencyItemSerializer
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', )
    queryset = ConsistencyItem.objects.all()

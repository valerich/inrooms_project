from django.conf.urls import include, url
from rest_framework import routers

from . import views

router = routers.SimpleRouter()

router.register(r'consistency_item', views.ConsistencyItemViewSet, base_name='consistency_item')


urlpatterns = [
    url(r'^', include(router.urls)),
]

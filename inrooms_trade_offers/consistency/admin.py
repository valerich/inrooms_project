from django.contrib import admin
from sorl.thumbnail.shortcuts import get_thumbnail

from .models import ConsistencyItem


class ConsistencyItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description', 'zara_code')


for model_or_iterable, admin_class in (
    (ConsistencyItem, ConsistencyItemAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

from django.utils import timezone
from rest_framework import filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.filters import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ..serializers import TaskFileSerializer, TaskSerializer
from ...models import Task, TaskFile


class TaskViewSet(ModelViewSet):
    serializer_class = TaskSerializer
    filter_backends = (filters.SearchFilter, DjangoFilterBackend,)
    filter_fields = ('status', 'client')
    search_fields = ('name', '=id')
    queryset = Task.objects.all()
    ordering_fields = (
        'id',
        'name',
    )

    def get_queryset(self):
        qs = Task.objects.all()
        if not self.request.user.has_perm('auth.can_all_task_view'):
            if self.request.user.client:
                qs = qs.filter(client=self.request.user.client)
            else:
                qs = qs.none()
        return qs

    def perform_update(self, serializer):
        obj = serializer.instance
        old_obj = Task.objects.get(id=obj.id)
        if old_obj.status != serializer.validated_data['status'] and serializer.validated_data['status'] == Task.STATUS.complete:
            obj.date_fact = timezone.now().date()
            obj.save()
        super().perform_update(serializer)

    @detail_route(methods=['post'])
    def file_upload(self, request, pk=None):
        obj = self.get_object()
        file_obj = request.FILES['file']

        _file = TaskFile.objects.create(
            filename=file_obj.name,
            file=file_obj,
        )
        obj.files.add(_file)
        return Response({
            'status': 'ok'
        })

    @list_route(methods=['post'])
    def file_delete(self, request):
        file_id = request.data.get('file_id', None)

        try:
            _file = TaskFile.objects.get(
                id=file_id
            )
        except TaskFile.DoesNotExist:
            pass
        else:
            _file.delete()
        return Response({
            'status': 'ok'
        })

    @detail_route(methods=['get'])
    def files(self, request, pk=None):
        obj = self.get_object()
        return Response(TaskFileSerializer(obj.files.all(), many=True).data)

from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from ...models import TaskFile


class TaskFileSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = TaskFile
        fields = [
            'id',
            'filename',
            'file',
        ]

from django.utils import timezone
from rest_framework import serializers

from clients.api.serializers.client import ClientSerializer
from ...models import Task


class TaskSerializer(serializers.ModelSerializer):
    client_detail = serializers.SerializerMethodField()
    status_detail = serializers.SerializerMethodField()
    date_fact = serializers.SerializerMethodField()
    is_alarm = serializers.SerializerMethodField()

    class Meta:
        model = Task

        fields = [
            'id',
            'name',
            'status',
            'status_detail',
            'client',
            'client_detail',
            'description',
            'date_plan',
            'date_fact',
            'created',
            'modified',
            'is_alarm',
        ]

    def get_date_fact(self, obj):
        return obj.date_fact

    def get_client_detail(self, obj):
        return ClientSerializer(obj.client).data

    def get_status_detail(self, obj):
        return {
            'id': obj.status,
            'name': obj.get_status_display(),
        }

    def get_is_alarm(self, obj):
        if obj.status != obj.STATUS.complete and obj.date_plan < timezone.now().date():
            return True
        return False

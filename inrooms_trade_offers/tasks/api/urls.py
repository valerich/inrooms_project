from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import TaskViewSet

router = DefaultRouter()
router.register(r'task', TaskViewSet)


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
]

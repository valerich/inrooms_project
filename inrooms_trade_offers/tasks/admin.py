from django.contrib import admin

from tasks.models import Task


class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'client', 'status')
    list_filter = ('status', )
    search_fields = ('=id', 'name', 'client__name')


for model_or_iterable, admin_class in (
    (Task, TaskAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

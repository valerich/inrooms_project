import os
import uuid

from django.db import models
from model_utils.choices import Choices
from model_utils.models import TimeStampedModel


def generate_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'task/{}{}'.format(uuid.uuid4(), ext)


class TaskFile(TimeStampedModel):
    filename = models.CharField('Название файла', max_length=1000, blank=True)
    file = models.FileField('Файл', upload_to=generate_filename, max_length=755, blank=True)

    class Meta:
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'
        ordering = ('-id', )

    def get_file_name(self):
        file = self.file.name.split('/')[-1]
        return file[:file.rfind('.')]


STATUS_CHOICES = Choices(
    (1, 'new', 'Новая'),
    (2, 'complete', 'Выполнена'),
)


class Task(TimeStampedModel):
    STATUS = STATUS_CHOICES
    status = models.PositiveSmallIntegerField('Статус', choices=STATUS, default=STATUS.new)
    name = models.CharField('Название', max_length=100, blank=True)
    client = models.ForeignKey('clients.Client', verbose_name='Партнер')
    date_plan = models.DateField('Плановая дата исполнения')
    date_fact = models.DateField('Фактическая дата исполнения', blank=True, null=True)
    description = models.TextField('Описание', blank=True)

    files = models.ManyToManyField(TaskFile, verbose_name='Файлы клиента', blank=True)

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'
        ordering = ('-id', )

    def __str__(self):
        return 'id:{}, {} ({})'.format(self.id, self.name, self.client)

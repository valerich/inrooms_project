from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'tasks'
    verbose_name = 'Задачи'

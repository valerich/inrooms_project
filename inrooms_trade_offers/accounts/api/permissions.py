from rest_framework import permissions


class CanSetPasswordPermission(permissions.BasePermission):
    message = 'Не достаточно прав'

    def has_object_permission(self, request, view, obj):
        if request.user == obj:
            return True
        if request.user.has_perm('auth.can_set_all_passwords'):
            return True
        return False

from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.decorators import detail_route, list_route
from rest_framework.filters import DjangoFilterBackend, SearchFilter
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .permissions import CanSetPasswordPermission
from .serializers import UserSerializer, CurrentUserDetailSerializer, GroupSerializer
from ..models import User


class UserViewSet(ModelViewSet):
    model = User
    serializer_class = UserSerializer
    queryset = model.objects.all()
    filter_backends = (SearchFilter, DjangoFilterBackend,)
    search_fields = ('email', 'first_name', 'last_name', 'middle_name')
    filter_fields = ('is_active', 'groups')

    def get_queryset(self):
        return super(UserViewSet, self).get_queryset()

    @list_route()
    def current(self, request):
        data = CurrentUserDetailSerializer(request.user).data
        return Response(data)

    @detail_route(methods=['post'], permission_classes=[CanSetPasswordPermission])
    def set_password(self, request, pk=None):
        password = request.data.get('password', None)
        password2 = request.data.get('password2', None)
        if password != password2:
            return Response({'error': 'Пароли не совпадают'},
                            status=status.HTTP_400_BAD_REQUEST)
        obj = self.get_object()
        obj.set_password(password)
        obj.save()
        return Response(status=status.HTTP_200_OK)


class GroupViewSet(ModelViewSet):
    model = Group
    serializer_class = GroupSerializer
    queryset = model.objects.all().exclude(name__startswith='_').order_by('-name')

    filter_backends = (SearchFilter, )
    search_fields = ('name', )

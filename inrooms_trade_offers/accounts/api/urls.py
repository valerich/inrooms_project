from django.conf import urls
from rest_framework import routers

from .views import UserViewSet, GroupViewSet

router = routers.SimpleRouter()

router.register(r'user', UserViewSet, base_name='user')
router.register(r'group', GroupViewSet, base_name='group')


urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
]

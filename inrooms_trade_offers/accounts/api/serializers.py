from django.contrib.auth.models import Group
from rest_framework import serializers

from extensions.drf.serializers import DynamicFieldsSerializerMixin
from .. import models


class CurrentUserDetailSerializer(serializers.ModelSerializer):
    user_permissions = serializers.SerializerMethodField()
    client = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    is_client = serializers.SerializerMethodField()

    class Meta:
        model = models.User
        fields = (
            'id',
            'email',
            'first_name',
            'middle_name',
            'last_name',
            'name',
            # 'is_staff',
            # 'is_superuser',
            'user_permissions',
            'client',
            'is_client',
        )

    def get_is_client(self, obj):
        group_client = Group.objects.get_or_create(name='Партнеры')[0]
        return group_client in obj.groups.all()

    def get_user_permissions(self, obj):
        app_label_codename_list = [perm.split('.', 1) for perm in obj.get_all_permissions()]
        return [{'app_label': each[0], 'codename': each[1]} for each in app_label_codename_list]

    def get_client(self, obj):
        if obj.client:
            return {
                'id': obj.client.id,
                'name': obj.client.name,
                'order_b2c_currency': obj.client.order_b2c_currency,
            }
        else:
            return None

    def get_name(self, obj):
        return obj.get_full_name()


class GroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = Group

        fields = (
            'id',
            'name',
        )


class UserSerializer(DynamicFieldsSerializerMixin, serializers.ModelSerializer):
    groups_detail = GroupSerializer(source='groups', many=True, read_only=True)
    name = serializers.SerializerMethodField()

    class Meta:
        model = models.User

        fields = (
            'id',
            'is_active',
            'email',
            'name',
            'first_name',
            'middle_name',
            'last_name',
            'groups',
            'groups_detail',
            'description',
        )

    def get_name(self, obj):
        return obj.get_full_name()

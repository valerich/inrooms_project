from authtools.models import AbstractNamedUser, AbstractEmailUser
from django.db import models


class User(AbstractEmailUser):
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name']

    first_name = models.CharField('Имя', max_length=100, blank=True)
    middle_name = models.CharField('Отчество', max_length=100, blank=True)
    last_name = models.CharField('Фамилия', max_length=100, blank=True)

    client = models.ForeignKey('clients.Client', verbose_name='Партнер', blank=True, null=True,
                               on_delete=models.SET_NULL)
    clients = models.ManyToManyField('clients.Client', verbose_name='Партнеры', blank=True, related_name='users')

    last_login_date = models.DateField('Последняя дата работы в системе', blank=True, null=True)
    description = models.TextField('Описание', blank=True, default='')

    class Meta:
        ordering = ('-date_joined',)
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return '{name} <{email}>'.format(
            name=self.get_full_name(),
            email=self.email,
        )

    def get_full_name(self):
        data = []
        for field in ['first_name',
                  'middle_name',
                  'last_name', ]:
            part = getattr(self, field)
            if part:
                data.append(part)
        return ' '.join(data)

    def get_short_name(self):
        return self.get_full_name()

    def __getattr__(self, name):
        if name == 'name':
            return self.get_full_name()
        return super().__getattribute__(name)

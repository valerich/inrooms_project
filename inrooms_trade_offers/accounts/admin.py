from authtools.admin import (
    ADVANCED_PERMISSION_FIELDS,
    BASE_FIELDS,
    DATE_FIELDS,
    StrippedUserAdmin,
)
from django.contrib import admin

from .models import User


class UserAdmin(StrippedUserAdmin):
    list_display = ('is_active', 'email', 'first_name', 'last_name', 'middle_name', 'date_joined', 'last_login', 'is_superuser', 'is_staff')
    list_display_links = ('first_name', 'email')

    search_fields = ('first_name', 'middle_name', 'last_name', 'email')
    list_filter = ('is_active', 'is_superuser', 'is_staff')

    fieldsets = (
        BASE_FIELDS,
        DATE_FIELDS,
        ADVANCED_PERMISSION_FIELDS,
    )

admin.site.register(User, UserAdmin)

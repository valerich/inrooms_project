from django.contrib.auth.models import Group

from core.server_init import ServerInit
from ..models import User


class Setup(object):
    autosetup_methods = ()

    def __init__(self, autosetup=True):
        if autosetup:
            for method in self.autosetup_methods:
                method.__func__()


class UsersSetup(Setup):
    @staticmethod
    def default_users():
        si = ServerInit()
        si.create_all_data()
        group_admins = Group.objects.get(name=u'Администраторы')
        group_brand_manager = Group.objects.get(name='Бренд менеджер')
        group_process_administrator = Group.objects.get(name='Администратор процесса')
        group_client = Group.objects.get(name='Партнеры')

        user_admin, created = User.objects.get_or_create(email='admin@example.com')
        user_admin.is_staff = True
        user_admin.is_superuser = True
        user_admin.set_password('password')
        user_admin.save()
        user_admin.groups.add(group_admins)

        user_bm, created = User.objects.get_or_create(email='brandmanager@example.com')
        user_bm.set_password('password')
        user_bm.save()
        user_bm.groups.add(group_brand_manager)

        user_bm2, created = User.objects.get_or_create(email='brandmanager2@example.com')
        user_bm2.set_password('password')
        user_bm2.save()
        user_bm2.groups.add(group_brand_manager)

        user_pa, created = User.objects.get_or_create(email='processadministrator@example.com')
        user_pa.set_password('password')
        user_pa.save()
        user_pa.groups.add(group_process_administrator)

        user_cl, created = User.objects.get_or_create(email='client@example.com')
        user_cl.set_password('password')
        user_cl.save()
        user_cl.groups.add(group_client)


    autosetup_methods = (default_users, )

from django.contrib.auth.models import Permission
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from accounts.tests.setup import UsersSetup
from ..models import User


class UserViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(UserViewTestCase, cls).setUpClass()
        cls.user_list_url = reverse('api:accounts:user-list')
        UsersSetup()
        cls.user_bm = User.objects.get(email='brandmanager@example.com')
        cls.user_pa = User.objects.get(email='processadministrator@example.com')

    def setUp(self):
        self.client = APIClient()
        self.client.login(email='brandmanager@example.com', password='password')
        pass

    def tearDown(self):
        self.client.session.clear()

    def test_set_password(self):
        """Api изменения пароля"""

        change_password_url = reverse('api:accounts:user-set-password', args=[self.user_pa.id, ])

        # Даем пользователю права на смену паролей
        permission = Permission.objects.get(codename='can_set_all_passwords')
        self.user_bm.user_permissions.add(permission)

        # Проверяем, что post запросы обрабатываются
        response = self.client.post(change_password_url, {'password': 'test', 'password2': 'test'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Проверяем, что пароль меняется корректно
        client = APIClient()
        client.login(email='processadministrator@example.com', password='fail_password')
        response = client.get("/")
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        client.session.clear()
        client = APIClient()
        client.login(email='processadministrator@example.com', password='test')
        response = client.get("/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_set_fail_password(self):
        """ Попытка смены своего пароля """
        change_password_url = reverse('api:accounts:user-set-password', args=[self.user_bm.id, ])
        response = self.client.post(change_password_url, {'password': 'test', 'password2': 'fail'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_set_self_password(self):
        """ Попытка смены своего пароля """
        change_password_url = reverse('api:accounts:user-set-password', args=[self.user_bm.id, ])
        response = self.client.post(change_password_url, {'password': 'test', 'password2': 'test'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_set_other_password(self):
        """ Попытка смены не своего пароля (у пользователя нет прав на смену паролей)"""
        change_password_url = reverse('api:accounts:user-set-password', args=[self.user_pa.id, ])
        response = self.client.post(change_password_url, {'password': 'test', 'password2': 'test'})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

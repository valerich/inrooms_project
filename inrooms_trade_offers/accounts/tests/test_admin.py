from django.contrib import admin
from django.contrib.auth.models import Group
from django.test import TestCase

from ..models import User


class AccountsAdminTestCase(TestCase):
    def test_user_in_admin(self):
        self.assertTrue(User in admin.site._registry)

    def test_group_in_admin(self):
        self.assertTrue(Group in admin.site._registry)

from autocomplete_light import shortcuts as autocomplete_light

from .models import User


class UserAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields = ['name', 'email']
    attrs = {
        'placeholder': u'ФИО, email',
        'data-autocomplete-minimum-characters': 0,
    }


AUTOCOMPLETE_LIST = (
    (User, UserAutocomplete),
)

for autocomplete_data in AUTOCOMPLETE_LIST:
    autocomplete_light.register(*autocomplete_data)

from django.conf import urls

from .views import (
    ConsolidatedReportView,
    ConsolidatedProductsReportView,
    ConsolidatedRemainsView,
    EmployeeDayStatisticView,
    PartnerDayStatisticView,
    ShopSeasonReportView,
    RevenueReportView,
)


urlpatterns = [
    urls.url(r'^consolidated/$', ConsolidatedReportView.as_view(), name='consolidated'),
    urls.url(r'^consolidated/products/$', ConsolidatedProductsReportView.as_view(), name='consolidated_products'),
    urls.url(r'^consolidated/remains/(?P<product_id>\d+)/$', ConsolidatedRemainsView.as_view(), name='consolidate_remains'),
    urls.url(r'^shop_season/$', ShopSeasonReportView.as_view(), name='shop_season'),
    urls.url(r'^revenue/$', RevenueReportView.as_view(), name='revenue'),
    urls.url(r'^partner_day_statistic/$', PartnerDayStatisticView.as_view(), name='partner_day_statistic'),
    urls.url(r'^employee_day_statistic/$', EmployeeDayStatisticView.as_view(), name='employee_day_statistic'),
]

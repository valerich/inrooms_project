import datetime
import os
import tempfile
from collections import defaultdict
from io import StringIO

from django.db.models.aggregates import Sum
from django.http.response import HttpResponse
from rest_framework import serializers
from rest_framework.views import APIView

import pandas as pd

from clients.models import Client
from orders.models.order_b2c import OrderB2C, OrderReturnB2C


class PartnerDayStatisticSerializer(serializers.Serializer):
    date_from = serializers.DateField()
    date_to = serializers.DateField()
    client = serializers.PrimaryKeyRelatedField(queryset=Client.objects.all())




class PartnerDayStatisticView(APIView):

    def get(self, request, *args, **kwargs):
        self.serializer = PartnerDayStatisticSerializer(data=request.GET)
        self.serializer.is_valid(True)

        self.date_from = self.serializer.validated_data['date_from']
        self.date_to = self.serializer.validated_data['date_to']
        self.client = self.serializer.validated_data['client']

        dataframe = self.get_report_data()

        fd, fn = tempfile.mkstemp()
        os.close(fd)
        writer = pd.ExcelWriter(fn, engine='xlsxwriter')
        dataframe.to_excel(writer, 'Лист1')
        worksheet = writer.sheets['Лист1']
        for idx, col in enumerate(dataframe):
            series = dataframe[col]
            max_len = max((
                series.astype(str).map(len).max(),
                len(str(series.name))
            )) + 1
            worksheet.set_column(idx, idx, max_len)
        writer.save()

        fh = open(fn, 'rb')
        resp = fh.read()
        fh.close()
        response = HttpResponse(resp, content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=report.xlsx'
        return response

    def get_report_data(self):
        data = []

        order_b2c_data = self.get_order_b2c_data()
        order_return_b2c_data = self.get_order_return_b2c_data()

        date = self.date_from
        while date <= self.date_to:
            data.append({
                'date': date.strftime('%d.%m.%Y'),
                'order_b2c_amount_full': order_b2c_data[date]['amount_full'],
                'order_b2c_amount_full_rur': order_b2c_data[date]['amount_full_rur'],
                'order_return_b2c__amount_full': order_return_b2c_data[date]['amount_full'],
                'order_return_b2c__amount_full_rur': order_return_b2c_data[date]['amount_full_rur'],
                'revenue': order_b2c_data[date]['amount_full'] - order_return_b2c_data[date]['amount_full'],
                'revenue_rur': order_b2c_data[date]['amount_full_rur'] - order_return_b2c_data[date]['amount_full_rur'],
                'order_b2c_quantity': order_b2c_data[date]['quantity'],
                'order_return_b2c_quantity': order_return_b2c_data[date]['quantity'],
                'visits': order_b2c_data[date]['visits'],
            })
            date += datetime.timedelta(days=1)

        columns = [
            ['date', 'Дата'],
            ['revenue', 'Выручка в валюте'],
            ['revenue_rur', 'Выручка в рублях'],
            ['order_b2c_amount_full', 'Продажи в валюте'],
            ['order_b2c_amount_full_rur', 'Продажи в рублях'],
            ['order_b2c_quantity', 'Продано единиц'],
            ['order_return_b2c__amount_full', 'Возвраты в валюте'],
            ['order_return_b2c__amount_full_rur', 'Возвраты в рублях'],
            ['order_return_b2c_quantity', 'Вернули единиц'],
            ['visits', 'Посетители'],
        ]

        df = pd.DataFrame(
            data,
            columns=[c[0] for c in columns]
        )
        df.rename(columns={c[0]: c[1] for c in columns}, inplace=True)
        return df

    def get_order_b2c_data(self):
        qs = OrderB2C.objects.filter(date__gte=self.date_from,
                                     date__lte=self.date_to,
                                     client=self.client)
        qs = qs.values('date', 'amount_full', 'amount_full_rur', 'visits')
        qs = qs.order_by()
        qs = qs.annotate(
            Sum('items__orderb2citemsize__quantity'),
        )
        data = defaultdict(lambda: defaultdict(int))
        for i in qs:
            data[i['date']] = {
                'amount_full': i['amount_full'],
                'amount_full_rur': i['amount_full_rur'],
                'quantity': i['items__orderb2citemsize__quantity__sum'],
                'visits': i['visits'],
            }
        return data

    def get_order_return_b2c_data(self):
        qs = OrderReturnB2C.objects.filter(date__gte=self.date_from,
                                           date__lte=self.date_to,
                                           client=self.client)
        qs = qs.values('date')
        qs = qs.order_by()
        qs = qs.annotate(
            Sum('amount_full'),
            Sum('amount_full_rur'),
        )
        data = defaultdict(lambda: defaultdict(int))
        for i in qs:
            data[i['date']] = {
                'amount_full': i['amount_full__sum'],
                'amount_full_rur': i['amount_full_rur__sum'],
                'quantity': 0,
            }

        qs = OrderReturnB2C.objects.filter(date__gte=self.date_from,
                                           date__lte=self.date_to,
                                           client=self.client)
        qs = qs.values('date')
        qs = qs.order_by()
        qs = qs.annotate(
            Sum('items__orderreturnb2citemsize__quantity'),
        )
        for i in qs:
            data[i['date']]['quantity'] = i['items__orderreturnb2citemsize__quantity__sum']

        return data
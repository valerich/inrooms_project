from collections import OrderedDict

from django.db.models.query_utils import Q
from rest_framework.response import Response
from rest_framework.views import APIView

from clients.models import Client
from orders.models.order import OrderItemSize, OrderItem
from orders.models.order_b2c import OrderB2CItem, OrderReturnB2C, OrderReturnB2CItem, OrderReturnItem
from products.models import ProductCollection, Remain
from purchases.models import PurchaseItem
from reports.api.serializers.consolidated import ConsolidatedReportFormSerializer
from supply.models import SupplyItem


class ConsolidatedReportView(APIView):

    def get(self, request, *args, **kwargs):
        serializer = ConsolidatedReportFormSerializer(data=request.GET)
        serializer.is_valid(True)
        self.main_collection_parrent = serializer.validated_data.get('collection_parrent', None)
        self.main_collection = serializer.validated_data.get('collection', None)
        self.client = serializer.validated_data.get('client', None)
        self.set_clients()
        self.product_kind = serializer.validated_data['product__kind']
        self.has_moskow_remains = serializer.validated_data.get('has_moskow_remains', False)

        if self.has_moskow_remains:
            self.moskow_remains_product_ids = set(Remain.objects.filter(warehouse__code__in=['msk', 'showroom'], value__gt=0).values_list('product_id', flat=True).order_by().distinct())

        collection_data = []

        if not self.main_collection_parrent and not self.main_collection:
            self.report_kind = 1
            collection_data = self.get_collection_data()
        elif self.main_collection_parrent and not self.main_collection:
            self.report_kind = 2
            collection_data = self.get_collection_data()

        report_title = self.get_report_title()

        return Response(data={
            'report_title': report_title,
            'report_kind': self.report_kind,
            'collection_data': collection_data,
            'client': {
                'name': self.get_report_name()
            }
        })

    def get_report_name(self):
        if self.client:
            return self.client.name
        return 'Все партнеры'

    def set_clients(self):
        self.clients = Client.objects.all()
        if self.client:
            self.clients = self.clients.filter(id=self.client.id)
        else:
            self.clients = self.clients.filter(
                is_active=True,
                kind=Client.KIND.partner,
                store__isnull=False,
            )

    def get_report_title(self):
        if self.report_kind == 1:
            return 'По всем родительским коллекциям'
        elif self.report_kind == 2:
            return self.main_collection_parrent.name
        elif self.report_kind == 3:
            return str(self.main_collection)

    def get_product_collection_queryset(self):
        qs = ProductCollection.objects.all().order_by('-parent_id', '-order', 'name')
        if self.report_kind == 1:
            qs = qs.filter(parent__isnull=True)
        elif self.report_kind == 2:
            qs = qs.filter(Q(parent=self.main_collection_parrent) | Q(id=self.main_collection_parrent.id))
        return qs

    def get_order_b2c_item_queryset(self):
        qs = OrderB2CItem.objects.filter(order__client__in=self.clients).select_related('product__main_collection').prefetch_related('orderb2citemsize_set')
        if self.has_moskow_remains:
            qs = qs.filter(product_id__in=self.moskow_remains_product_ids)
        if self.report_kind == 2:
            qs = qs.filter(Q(product__main_collection=self.main_collection_parrent) | Q(product__main_collection__parent=self.main_collection_parrent))
        if self.product_kind:
            qs = qs.filter(product__kind=self.product_kind)
        return qs

    def get_order_return_b2c_queryset(self):
        qs = OrderReturnB2CItem.objects.filter(order__client__in=self.clients).select_related(
            'product__main_collection').prefetch_related('orderreturnb2citemsize_set')
        if self.has_moskow_remains:
            qs = qs.filter(product_id__in=self.moskow_remains_product_ids)
        if self.report_kind == 2:
            qs = qs.filter(Q(product__main_collection=self.main_collection_parrent) | Q(
                product__main_collection__parent=self.main_collection_parrent))
        if self.product_kind:
            qs = qs.filter(product__kind=self.product_kind)
        return qs

    def get_order_item_queryset(self):
        qs = OrderItem.objects.filter(order__status__code='received', order__client__in=self.clients).select_related('product__main_collection').prefetch_related('orderitemsize_set')
        if self.has_moskow_remains:
            qs = qs.filter(product_id__in=self.moskow_remains_product_ids)
        if self.report_kind == 2:
            qs = qs.filter(Q(product__main_collection=self.main_collection_parrent) | Q(product__main_collection__parent=self.main_collection_parrent))
        if self.product_kind:
            qs = qs.filter(product__kind=self.product_kind)
        return qs

    def get_order_return_queryset(self):
        qs = OrderReturnItem.objects.filter(order__client__in=self.clients).select_related(
            'product__main_collection').prefetch_related('orderreturnitemsize_set')
        if self.has_moskow_remains:
            qs = qs.filter(product_id__in=self.moskow_remains_product_ids)
        if self.report_kind == 2:
            qs = qs.filter(Q(product__main_collection=self.main_collection_parrent) | Q(
                product__main_collection__parent=self.main_collection_parrent))
        if self.product_kind:
            qs = qs.filter(product__kind=self.product_kind)
        return qs

    def get_remain_queryset(self):
        qs = Remain.objects.filter(warehouse__client__in=self.clients, value__gt=0).select_related('product__main_collection')
        if self.has_moskow_remains:
            qs = qs.filter(product_id__in=self.moskow_remains_product_ids)
        if self.report_kind == 2:
            qs = qs.filter(Q(product__main_collection=self.main_collection_parrent) | Q(product__main_collection__parent=self.main_collection_parrent))
        if self.product_kind:
            qs = qs.filter(product__kind=self.product_kind)
        return qs

    def get_collection_data(self):

        data = OrderedDict()
        all_sold = 0
        all_return = 0
        for pc in self.get_product_collection_queryset():
            data[pc.id] = {
                'product_collection': {
                    'id': pc.id,
                    'name': str(pc),
                    'parent_id': pc.parent_id
                },
                'supplied': 0,
                'sold': 0,
                'remain': 0,
                'return': 0,
                'sold_percent': 0,
                'return_percent': 0,
                'client_return': 0,
            }

        for order_b2c_item in self.get_order_b2c_item_queryset():
            if order_b2c_item.product.main_collection_id in data:
                item = data[order_b2c_item.product.main_collection_id]
            else:
                item = data[order_b2c_item.product.main_collection.parent_id]
            for ois in order_b2c_item.orderb2citemsize_set.all():
                item['sold'] += ois.quantity
                all_sold += ois.quantity

        for order_return_b2c_item in self.get_order_return_b2c_queryset():
            if order_return_b2c_item.product.main_collection_id in data:
                item = data[order_return_b2c_item.product.main_collection_id]
            else:
                item = data[order_return_b2c_item.product.main_collection.parent_id]
            for ois in order_return_b2c_item.orderreturnb2citemsize_set.all():
                item['return'] += ois.quantity
                all_return += ois.quantity

        for order_return_item in self.get_order_return_queryset():
            if order_return_item.product.main_collection_id in data:
                item = data[order_return_item.product.main_collection_id]
            else:
                item = data[order_return_item.product.main_collection.parent_id]
            for ois in order_return_item.orderreturnitemsize_set.all():
                item['client_return'] += ois.quantity

        for order_item in self.get_order_item_queryset():
            if order_item.product.main_collection_id in data:
                item = data[order_item.product.main_collection_id]
            else:
                item = data[order_item.product.main_collection.parent_id]
            for ois in order_item.orderitemsize_set.all():
                item['supplied'] += ois.quantity

        for r in self.get_remain_queryset():
            if r.product.main_collection_id in data:
                item = data[r.product.main_collection_id]
            else:
                item = data[r.product.main_collection.parent_id]
            item['remain'] += r.value
        for pc_id, pc_data in data.items():
            if pc_data['supplied']:
                pc_data['sold_percent'] = ((pc_data['sold'] - pc_data['return']) / pc_data['supplied']) * 100
            if all_sold:
                pc_data['all_sold_percent'] = ((pc_data['sold'] - pc_data['return']) / all_sold) * 100
            else:
                pc_data['all_sold_percent'] = 0
            if pc_data['sold']:
                pc_data['return_percent'] = (100 / pc_data['sold']) * pc_data['return']
        return [pc_data for pc_id, pc_data in data.items()]

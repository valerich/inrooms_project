import datetime

from collections import defaultdict
from django.db import models
from django.db.models.aggregates import Sum
from django.db.models.expressions import Func

from rest_framework.response import Response
from rest_framework.views import APIView

from clients.models import Client
from core.utils.date import date_range
from orders.models.order import Order
from orders.models.order_b2c import OrderB2C, OrderReturnB2C
from reports.api.serializers import RevenueSerializer


class RevenueReportView(APIView):

    def get(self, request, *args, **kwargs):
        serializer = RevenueSerializer(data=request.GET)
        serializer.is_valid(True)

        self.date_from = serializer.validated_data['date_from']
        self.date_to = serializer.validated_data['date_to']
        self.client = serializer.validated_data.get('client', None)
        self.clients = self.get_clients()

        self.set_dates()

        data = self.get_report_data()

        return Response(data=data)

    def get_clients(self):
        qs = Client.objects.filter(store__isnull=False)
        if self.client:
            qs = qs.filter(id=self.client.id)
        return qs

    def set_dates(self):
        self.dates = date_range(self.date_from, self.date_to)

    def get_report_data(self):
        data = []
        orders_data = self.get_orders_data()
        revenue_data = self.get_revenue_data()

        for date in self.dates:
            item = {
                'date': date.strftime('%Y-%m-%d'),
                'revenue': revenue_data[date],
                'orders': orders_data.get(date, None),
            }
            data.append(item)
        return data

    def get_orders_data(self):
        orders_data = {}
        qs = Order.objects.filter(
            client__in=self.clients,
            date_shipped__gte=self.date_from,
            date_shipped__lte=self.date_to + datetime.timedelta(days=1),
        )
        qs = qs.extra({'date_shipped' : "date(date_shipped)"})
        qs = qs.values('date_shipped')
        qs = qs.order_by()
        qs = qs.annotate(Sum('amount_full'))
        for i in qs:
            orders_data[i['date_shipped']] = int(i['amount_full__sum'])
        return orders_data

    def get_revenue_data(self):
        revenue_data = defaultdict(int)

        order_b2c_qs = OrderB2C.objects.filter(
            client__in=self.clients,
        )
        order_b2c_qs = order_b2c_qs.values('date')
        order_b2c_qs = order_b2c_qs.order_by()
        order_b2c_qs = order_b2c_qs.annotate(Sum('visits'), Sum('amount_full_rur'))
        for i in order_b2c_qs:
            revenue_data[i['date']] = int(i['amount_full_rur__sum'])

        order_return_b2c_qs = OrderReturnB2C.objects.filter(
            client__in=self.clients,
        )
        order_return_b2c_qs = order_return_b2c_qs.values('date')
        order_return_b2c_qs = order_return_b2c_qs.order_by()
        order_return_b2c_qs = order_return_b2c_qs.annotate(Sum('amount_full_rur'))
        for i in order_return_b2c_qs:
            revenue_data[i['date']] -= int(i['amount_full_rur__sum'])

        return revenue_data

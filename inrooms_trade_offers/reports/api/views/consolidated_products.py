from collections import defaultdict
import datetime

from django.db.models.aggregates import Sum, Count
from django.db.models.query_utils import Q
from rest_framework import filters
from rest_framework.generics import ListAPIView

from clients.models import Client
from core.utils.views.additional_data_pagination import AdditionalDataPaginatedViewMixin
from orders.models.order import OrderItemSize, OrderItem
from orders.models.order_b2c import OrderB2CItem, OrderB2CItemSize, OrderReturnB2CItemSize, OrderReturnB2CItem, \
    OrderReturnItemSize, OrderReturnItem
from products.models import Remain, Product, RemainHistory
from purchases.models import PurchaseItem
from reports.api.serializers import ConsolidatedReportFormSerializer, ConsolidatedProductsReportSerializer
from supply.models import SupplyItem
from warehouses.models import Warehouse


class ConsolidatedProductsReportView(AdditionalDataPaginatedViewMixin, ListAPIView):
    serializer_class = ConsolidatedProductsReportSerializer
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter)
    search_fields = ('^article', 'name', '=id')

    def get(self, request, *args, **kwargs):
        serializer = ConsolidatedReportFormSerializer(data=request.GET)
        serializer.is_valid(True)
        self.main_collection_parrent = serializer.validated_data.get('collection_parrent', None)
        self.main_collection = serializer.validated_data.get('collection', None)
        self.client = serializer.validated_data.get('client', None)
        self.set_clients()
        self.product_kind = serializer.validated_data['product__kind']
        self.has_moskow_remains = serializer.validated_data.get('has_moskow_remains', False)

        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        product_ids = set(OrderItem.objects.filter(order__status__code='received', order__client__in=self.clients).values_list('product_id', flat=True).order_by().distinct())
        product_ids.update(set(OrderB2CItem.objects.filter(order__client__in=self.clients).values_list('product_id', flat=True).order_by().distinct()))
        product_ids.update(set(OrderReturnB2CItem.objects.filter(order__client__in=self.clients).values_list('product_id', flat=True).order_by().distinct()))
        product_ids.update(set(OrderReturnItem.objects.filter(order__client__in=self.clients).values_list('product_id',
                                                                                                     flat=True).order_by().distinct()))
        product_ids.update(set(Remain.objects.filter(warehouse__client__in=self.clients, value__gt=0).values_list('product_id', flat=True).order_by().distinct()))

        if self.has_moskow_remains:
            moskow_remains_product_ids = set(
                Remain.objects.filter(warehouse__code__in=['msk', 'showroom'], value__gt=0).values_list('product_id',
                                                                                                        flat=True).order_by().distinct())
            product_ids = product_ids.intersection(moskow_remains_product_ids)
        qs = Product.objects.filter(id__in=product_ids).select_related('color')
        qs = qs.prefetch_related('images')
        if self.product_kind:
            qs = qs.filter(kind=self.product_kind)
        if self.main_collection:
            qs = qs.filter(main_collection=self.main_collection)
        return qs

    def get_serializer(self, page, *args, **kwargs):
        self.product_ids = [product.id for product in page]
        return super().get_serializer(page, *args, **kwargs)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['products_data'], context['sold_percent'], context['return_percent'] = self.get_products_data()
        context['order_b2c_quantity'] = self.get_order_b2c_quantity_data()
        context['remain_history'] = self.get_remain_history_data()
        return context

    def get_additional_data(self, queryset):
        return {
            'main_collection': {
                'name': str(self.main_collection) if self.main_collection else ''
            }
        }

    def set_clients(self):
        self.clients = Client.objects.all()
        if self.client:
            self.clients = self.clients.filter(id=self.client.id)
        else:
            self.clients = self.clients.filter(
                is_active=True,
                kind=Client.KIND.partner,
                store__isnull=False,
            )

    def get_order_b2c_quantity_data(self):
        order_b2c_quantity_qs = OrderB2CItemSize.objects.filter(
            order_item__order__client__in=self.clients,
            order_item__product_id__in=self.product_ids,
        ).values('order_item__product').order_by('order_item__product').annotate(sum_quantity=Sum('quantity'))

        order_b2c_quantity_data = defaultdict(int)

        for s in order_b2c_quantity_qs:
            order_b2c_quantity_data[s['order_item__product']] = s['sum_quantity']

        return order_b2c_quantity_data

    def get_remain_history_data(self):

        today = datetime.date.today()

        remain_history_qs = RemainHistory.objects.filter(
            warehouse__in=Warehouse.objects.filter(client__in=self.clients),
            product__in=Product.objects.filter(id__in=self.product_ids),
            value_avg__gt=0,
        ).exclude(
            Q(year__gt=today.year) | Q(year=today.year, month__gt=today.month)
        )
        remain_history_qs = remain_history_qs.values('product').order_by('product').annotate(
            sum_value_avg=Sum('value_avg'),
            count_value_avg=Count('value_avg'),
        )

        remain_history_data = defaultdict(lambda: defaultdict(int))
        for s in remain_history_qs:
            remain_history_data[s['product']] = {
                'sum_value_avg': s['sum_value_avg'],
                'count_value_avg': s['count_value_avg'],
            }
        return remain_history_data

    def get_order_b2c_item_size_queryset(self):
        qs = OrderB2CItemSize.objects.filter(order_item__order__client__in=self.clients, order_item__product_id__in=self.product_ids).select_related('order_item', 'size')
        return qs

    def get_order_return_b2c_item_size_queryset(self):
        qs = OrderReturnB2CItemSize.objects.filter(order_item__order__client__in=self.clients, order_item__product_id__in=self.product_ids).select_related('order_item', 'size')
        return qs

    def get_order_item_size_queryset(self):
        qs = OrderItemSize.objects.filter(order_item__order__status__code='received', order_item__order__client__in=self.clients, order_item__product_id__in=self.product_ids).select_related('order_item', 'size')
        return qs

    def get_order_return_item_size_queryset(self):
        qs = OrderReturnItemSize.objects.filter(order_item__order__client__in=self.clients, order_item__product_id__in=self.product_ids).select_related('order_item', 'size')
        return qs

    def get_remain_queryset(self):
        qs = Remain.objects.filter(warehouse__client__in=self.clients, product_id__in=self.product_ids, value__gt=0).select_related('size')
        return qs

    def get_products_data(self):
        data = defaultdict(lambda: defaultdict(int))
        sold = defaultdict(int)
        supplied = defaultdict(int)
        sold_percent = defaultdict(int)
        returns = defaultdict(int)
        return_percent = defaultdict(int)

        for order_item_size in self.get_order_item_size_queryset():
            data[(order_item_size.order_item.product_id, order_item_size.size.code)]['supplied'] += order_item_size.quantity
            supplied[order_item_size.order_item.product_id] += order_item_size.quantity

        for order_b2c_item_size in self.get_order_b2c_item_size_queryset():
            data[(order_b2c_item_size.order_item.product_id, order_b2c_item_size.size.code)]['sold'] += order_b2c_item_size.quantity
            sold[order_b2c_item_size.order_item.product_id] += order_b2c_item_size.quantity

        for order_return_b2c_item_size in self.get_order_return_b2c_item_size_queryset():
            data[(order_return_b2c_item_size.order_item.product_id, order_return_b2c_item_size.size.code)]['return'] += order_return_b2c_item_size.quantity
            returns[order_return_b2c_item_size.order_item.product_id] += order_return_b2c_item_size.quantity

        for order_return_item_size in self.get_order_return_item_size_queryset():
            data[(order_return_item_size.order_item.product_id, order_return_item_size.size.code)]['client_return'] += order_return_item_size.quantity

        for r in self.get_remain_queryset():
            if r.value:
                data[(r.product_id, r.size.code)]['remain'] += r.value
        for i in set(sold.keys()).union(set(supplied.keys())).union(set(returns.keys())):
            if supplied[i]:
                sold_percent[i] = ((sold[i] - returns[i]) / supplied[i]) * 100
            if sold[i]:
                return_percent[i] = (100 / sold[i]) * returns[i]

        return data, sold_percent, return_percent

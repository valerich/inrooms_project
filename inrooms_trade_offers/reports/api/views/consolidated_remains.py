from collections import defaultdict

from django.db.models.query_utils import Q
from rest_framework.response import Response
from rest_framework.status import HTTP_404_NOT_FOUND
from rest_framework.views import APIView

from products.models import Remain, Size, Product


class ConsolidatedRemainsView(APIView):

    def get(self, request, product_id, *args, **kwargs):
        try:
            product = Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            return Response(code=HTTP_404_NOT_FOUND, data={'error': 'Не найден товар с id {}'.format(product_id)})
        remain_qs = Remain.objects.filter(product_id=product_id, value__gt=0)
        remain_qs = remain_qs.filter((Q(warehouse__client__isnull=True) & Q(warehouse__is_active=True)) | Q(warehouse__client__is_active=True))
        remain_qs = remain_qs.exclude(warehouse__code__in=['chn', 'chn_defect', 'chn_label', ])
        remain_qs = remain_qs.order_by('warehouse__client__name', 'size__weight')
        remain_qs = remain_qs.select_related('warehouse', 'warehouse__client', 'size')

        data = defaultdict(lambda: defaultdict(int))
        for r in remain_qs:
            data[r.warehouse][r.size_id] = r.value

        sizes = Size.objects.filter(series_type=product.main_collection.series_type)

        r_data = []
        for warehouse, size_data in data.items():
            temp = {
                'warehouse': '{}{}'.format(
                    warehouse.name,
                    ' ({})'.format(warehouse.client.name) if warehouse.client else ''
                ),
                'sizes': []
            }

            for size in sizes:
                temp['sizes'].append(size_data[size.id])
            r_data.append(temp)

        r_data = sorted(r_data, key=lambda item: item['warehouse'])

        return Response(
            data={
                'id': product_id,
                'remains': r_data,
                'sizes': [s.name for s in sizes]
            })

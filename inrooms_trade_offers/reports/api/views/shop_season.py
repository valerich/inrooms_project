import datetime
from collections import defaultdict, OrderedDict

from decimal import Decimal
from django.db import models
from django.db.models.aggregates import Sum, Count
from django.db.models import Func
from django.db.models.expressions import F
from rest_framework.response import Response
from rest_framework.views import APIView
from sorl.thumbnail.shortcuts import get_thumbnail

from clients.models import Client
from core.utils.constants import DEPARTMENT_CHOICE
from invoices.models import Paying, Invoice
from offers.models import Offer
from orders.models.order_b2c import OrderB2C, OrderReturnB2C, OrderB2CItemSize
from orders.models.sales_plan import SalesPlan
from products.models import Remain, ProductCollection, SeasonProductCollectionPlan, SeasonProductCollectionPlanItem
from reports.api.serializers import ShopSeasonSerializer
from stores.models import Store


class Year(Func):
    function = 'EXTRACT'
    template = '%(function)s(YEAR from %(expressions)s)'
    output_field = models.IntegerField()


class Month(Func):
    function = 'EXTRACT'
    template = '%(function)s(MONTH from %(expressions)s)'
    output_field = models.IntegerField()


class ShopSeasonReportView(APIView):

    def get(self, request, *args, **kwargs):
        serializer = ShopSeasonSerializer(data=request.GET)
        serializer.is_valid(True)

        self.is_consolidated = serializer.validated_data.get('is_consolidated', False)

        self.set_season_dates(
            year=serializer.validated_data.get('year', None),
            season=serializer.validated_data.get('season', None)
        )

        self.set_spcp_data()

        self.stores = self.get_stores(serializer.validated_data.get('client', None))
        self.store_count = self.stores.count()
        self.product_collections_parrents = OrderedDict()
        for spcp in SeasonProductCollectionPlanItem.objects.filter(
                season_product_collection_plan__year=self.year,
                season_product_collection_plan__season=self.season,
            ):
            self.product_collections_parrents[spcp.product_collection.name] = spcp.plan
        self.product_collections = {pc.id: pc.parent.name if pc.parent else pc.name for pc in ProductCollection.objects.all().select_related('parent')}

        data = self.get_report_data()

        return Response(data=data)

    def set_season_dates(self, year=None, season=None):
        if year and season:
            self.year = year
            self.season = season
            month = 1 if season == 1 else 8
        else:
            today = datetime.date.today()
            self.year = today.year
            month = today.month
            self.season = 1 if month <= 7 else 2

        if month <= 7:
            self.date_from = datetime.date(self.year, 1, 1)
            self.date_to = datetime.date(self.year, 7, 31)
            self.season_name = 'весна/лето'
        else:
            self.date_from = datetime.date(self.year, 8, 1)
            self.date_to = datetime.date(self.year, 12, 31)
            self.season_name = 'осень/зима'
        self.months = [(self.date_from.year, i) for i in range(self.date_from.month, self.date_to.month + 1)]

    def set_spcp_data(self):
        try:
            spcp = SeasonProductCollectionPlan.objects.get(year=self.year, season=self.season)
        except SeasonProductCollectionPlan.DoesNotExist:
            self.units_per_s_m = 0
            self.avg_unit_cost = 0
        else:
            self.units_per_s_m = spcp.units_per_s_m
            self.avg_unit_cost = spcp.avg_unit_cost

    def get_stores(self, client=None):
        self.clients = Client.objects.filter(store__isnull=False, is_active=True, kind=1)
        if client:
            self.clients = self.clients.filter(id=client.id)
        if self.request.user.has_perm('auth.can_all_shop_season_report_view'):
            pass
        elif self.request.user.client:
            self.clients = self.clients.filter(id=self.request.user.client.id)
        else:
            self.clients = self.clients.none()

        return Store.objects.filter(client__in=self.clients).select_related('client')

    def __getattr__(self, name):
        if name in [
            'avg_cheque',
            'remains',
            'remainder_payable',
            'product_collections_data',
            'paying',
            'visitors',
            'orders_sum',
            'revenue',
            'sales_plan',
            'order_b2c_month_qs',
            'order_return_b2c_month_qs',
            'sales_plan_qs',
            'paying_month_qs',
            'remain_qs',
            'remain_product_collection_qs',
            'cheques',
            'avg_cheque_count',
            'cheques_quantity',
            'orders_sum_qs',
            'images',
            'marketing_balance',
            'marketing_invoice_qs',
            'marketing_paying_qs'
        ] and not name in self.__dict__:
            getattr(self, 'set_{}'.format(name))()
        return self.__getattribute__(name)

    def get_report_data(self):
        data = []
        items = [None, ] if self.is_consolidated else self.stores
        for store in items:
            trade_area = self.get_trade_area(store)
            data_part = {
                'store_name': self.get_store_name(store),                          # Название магазина
                'is_consolidated': self.is_consolidated,
                'year': self.year,
                'season': self.season,
                'date_from': self.date_from.strftime('%d.%m.%Y'),
                'date_to': self.date_to.strftime('%d.%m.%Y'),
                'client': self.get_client_id(store),
                'season_name': self.season_name,

                'trade_area': self.get_trade_area(store),                    # Торговая площадь магазина

                'plan_avg_cheque': self.get_plan_avg_cheque(store),          # Плановый средний чек

                'plan_conversion': self.get_plan_conversion(store),          # Плановая конверсия

                'plan_filling_items': self.units_per_s_m * trade_area,                       # План загрузки магазина в штуках (считается по
                                                                             # формуле (25 * торговая площадь). Почему? Х.з.

                'plan_filling_amount': self.avg_unit_cost * self.units_per_s_m * trade_area,               # План загрузки магазина в рублях (считается по
                                                                             # формуле (1500 * 25 * торговая площадь). Почему? Х.з.

                'filling_items': self.get_filling_items(store),    # Загрузка магазина в штуках (просто остатки)

                'filling_amount': self.get_filling_amount(store),  # Загрузка магазина в рублях
                                                                   # (остатки товаров умноженные на их цены)

                'avg_cheque': self.get_avg_cheque(store),          # Средний чек

                'avg_cheque_count': self.get_avg_cheque_count(store),

                'conversion': self.get_conversion(store),

                'product_collections': self.get_product_collections_data(store),

                'marketing_paying': self.get_marketing_paying(store),
            }

            data_part['season_data'] = []
            for month in self.months:
                if month[1] == 1:
                    plan_paying_month = (month[0] - 1, 12)
                else:
                    plan_paying_month = (month[0], month[1] - 1)

                data_part_month = {
                    'month': month[1],
                    'month_name': self.get_month_name(month[1])
                }

                data_part_month['visitors'] = self.get_visitors(store, month)            # Количество посетителей
                data_part_month['sales_plan'] = self.get_sales_plan(store, month)        # План продаж
                data_part_month['revenue'] = self.get_revenue(store, month)              # Выручка (продажи - возвраты)
                data_part_month['plan_paying'] = self.get_revenue(store, plan_paying_month) / 2          # Платежи план (выручка / 2) Потому что потому
                data_part_month['paying'] = self.get_paying(store, month)                # Платежи по факту
                data_part_month['plan_visitors'] = self.get_plan_visitors(store, month)  # План посещений

                data_part['season_data'].append(data_part_month)

            data_part['remainder_payable'] = self.get_remainder_payable(store)           # Остаток к оплате за сезон
            data_part['images'] = self.images
            data.append(data_part)
        return data

    def get_store_name(self, store=None):
        if self.is_consolidated:
            return 'По всем магазинам и партнерам'
        else:
            return store.name

    def get_client_id(self, store=None):
        if self.is_consolidated:
            return None
        else:
            return store.client_id

    def get_trade_area(self, store=None):
        if self.is_consolidated:
            return self.stores.aggregate(Sum('trade_area'))['trade_area__sum']
        else:
            return store.trade_area

    def get_plan_avg_cheque(self, store=None):
        if self.is_consolidated:
            if self.store_count:
                return self.stores.aggregate(Sum('plan_avg_cheque'))['plan_avg_cheque__sum'] / self.store_count
            else:
                return None
        else:
            return store.plan_avg_cheque

    def get_plan_conversion(self, store=None):
        if self.is_consolidated:
            if self.store_count:
                return self.stores.aggregate(Sum('plan_conversion'))['plan_conversion__sum'] / self.store_count
            else:
                return None
        else:
            return store.plan_conversion

    def get_filling_items(self, store=None):
        if self.is_consolidated:
            return self.remains['value__sum']
        else:
            return self.remains[store.client.id]['value__sum']

    def get_filling_amount(self, store=None):
        if self.is_consolidated:
            return self.remains['amount__sum']
        else:
            return self.remains[store.client.id]['amount__sum']

    def get_avg_cheque(self, store=None):
        if self.is_consolidated:
            return self.avg_cheque
        else:
            return self.avg_cheque[store.client.id]

    def get_avg_cheque_count(self, store=None):
        if self.is_consolidated:
            return self.avg_cheque_count
        else:
            return self.avg_cheque_count[store.client.id]

    def get_conversion(self, store=None):
        if self.is_consolidated:
            visitors = sum(
                [
                    value for d, value in self.visitors.items() if all([
                        d[0] == self.date_from.year,
                        self.date_from.month <= d[1] <= self.date_to.month])
                ]
            )
            cheques = len(self.cheques)
        else:
            visitors = sum(
                [
                    value for d, value in self.visitors[store.client.id].items() if all([
                    d[0] == self.date_from.year,
                    self.date_from.month <= d[1] <= self.date_to.month])
                    ]
            )
            cheques = len(self.cheques[store.client.id])

        if visitors:
            return (cheques / visitors) * 100
        else:
            return None

    def get_product_collections_data(self, store=None):
        if self.is_consolidated:
            data = self.product_collections_data
        else:
            data = self.product_collections_data[store.client.id]
        remains = 0
        for item in data:
            remains += item['remain']
        for item in data:
            item['plan_remain'] = round((Decimal((self.units_per_s_m * self.get_trade_area(store)) / 100)) * self.product_collections_parrents.get(item['collection_name'], 0))
            item['fact_percentage'] = round(item['remain'] * 100 / remains) if remains else None
        return data

    def get_marketing_paying(self, store=None):
        data = []
        if self.is_consolidated:
            balance = self.marketing_balance
        else:
            balance = self.marketing_balance[store.client.id]
        for date in self.months:
            if date in balance:
                data.append({
                    'month_name': self.get_month_name(date[1]),
                    'is_payed': balance[date],
                })
        return data

    def set_marketing_balance(self):
        if self.is_consolidated:
            data = defaultdict(Decimal)
            for item in self.marketing_invoice_qs:
                data[(item['payment_perion_year'], item['payment_perion_month'])] += item['total_price__sum']
            marketing_paying = self.marketing_paying_qs['amount__sum'] or 0
            for key in sorted(data.keys()):
                marketing_paying -= data[key]
                data[key] = marketing_paying >= 0
        else:
            data = defaultdict(lambda: defaultdict(Decimal))
            for item in self.marketing_invoice_qs:
                data[item['customer']][(item['payment_perion_year'], item['payment_perion_month'])] += item['total_price__sum']
            temp_marketing_paying = {item['invoice__customer']: item['amount__sum'] for item in self.marketing_paying_qs}
            for client_id in data.keys():
                marketing_paying = temp_marketing_paying.get(client_id, 0)
                for key in sorted(data[client_id].keys()):
                    marketing_paying -= data[client_id][key]
                    data[client_id][key] = marketing_paying >= 0
        self.marketing_balance = data

    def get_visitors(self, store, key):
        if self.is_consolidated:
            return self.visitors[key]
        else:
            return self.visitors[store.client.id][key]

    def get_sales_plan(self, store, key):
        if self.is_consolidated:
            return self.sales_plan[key]
        else:
            return self.sales_plan[store.id][key]

    def get_revenue(self, store, key):
        if self.is_consolidated:
            return self.revenue[key]
        else:
            return self.revenue[store.client.id][key]

    def get_paying(self, store, key):
        if self.is_consolidated:
            return self.paying[key]
        else:
            return self.paying[store.client.id][key]

    def get_plan_visitors(self, store, key):
        plan_avg_cheque = self.get_plan_avg_cheque(store)
        plan_conversion = self.get_plan_conversion(store)
        if self.is_consolidated:
            sales_plan = self.sales_plan[key]
        else:
            sales_plan = self.sales_plan[store.id][key]
        if plan_avg_cheque and plan_conversion:
            return round(((sales_plan / plan_avg_cheque) / plan_conversion) * 100)
        else:
            return None

    def get_remainder_payable(self, store):
        if self.is_consolidated:
            return self.remainder_payable
        else:
            return self.remainder_payable[store.client.id]

    def get_month_name(self, month):
        month_data = {
            1: 'Янв',
            2: 'Фев',
            3: 'Мар',
            4: 'Апр',
            5: 'Май',
            6: 'Июн',
            7: 'Июл',
            8: 'Авг',
            9: 'Сен',
            10: 'Окт',
            11: 'Ноя',
            12: 'Дек',
        }
        return month_data[month]

    # def get_orders_sum(self, store, key):
    #     return self.orders_sum[store.client.id][key]

    def set_order_b2c_month_qs(self):
        qs = OrderB2C.objects.filter(
            client__in=self.clients,
            # date__gte=self.date_from,
            # date__lte=self.date_to,
        )
        qs = qs.annotate(m=Month('date'), y=Year('date'))
        if self.is_consolidated:
            qs = qs.values('m', 'y')
        else:
            qs = qs.values('client', 'm', 'y')
        qs = qs.order_by()
        qs = qs.annotate(Sum('visits'), Sum('amount_full_rur'))
        self.order_b2c_month_qs = qs

    def set_order_return_b2c_month_qs(self):
        qs = OrderReturnB2C.objects.filter(
            client__in=self.clients,
            # date__gte=self.date_from,
            # date__lte=self.date_to,
        )
        qs = qs.annotate(m=Month('date'), y=Year('date'))
        if self.is_consolidated:
            qs = qs.values('m', 'y')
        else:
            qs = qs.values('client', 'm', 'y')
        qs = qs.order_by()
        qs = qs.annotate(Sum('amount_full_rur'))
        self.order_return_b2c_month_qs = qs

    def set_paying_month_qs(self):
        qs = Paying.objects.filter(
            invoice__customer__in=self.clients,
            invoice__kind=Invoice.KIND.default,
            include_to_office_plan=True,
            department=DEPARTMENT_CHOICE.escort,
            include_to_dashboard=True,
            # date__gte=self.date_from,
            # date__lte=self.date_to,
        )
        qs = qs.annotate(m=Month('date'), y=Year('date'))
        if self.is_consolidated:
            qs = qs.values('m', 'y')
        else:
            qs = qs.values('invoice__customer', 'm', 'y')
        qs = qs.order_by()
        qs = qs.annotate(Sum('amount'))
        self.paying_month_qs = qs

    def set_sales_plan_qs(self):
        qs = SalesPlan.objects.filter(
            store__in=self.stores,
            year=self.date_from.year,
            month__gte=self.date_from.month,
            month__lte=self.date_to.month,
        )
        if self.is_consolidated:
            qs = qs.values('month', 'year')
            qs = qs.order_by()
            qs = qs.annotate(plan=Sum('plan'))
        else:
            qs = qs.values('store', 'month', 'year', 'plan')
        self.sales_plan_qs = qs

    # def set_orders_sum_qs(self):
    #     qs = Order.objects.filter(
    #         client__in=self.clients,
    #         date_shipped__gte=self.date_from,
    #         date_shipped__lte=self.date_to + datetime.timedelta(days=1),
    #     )
    #     qs = qs.annotate(m=Month('date_shipped'), y=Year('date_shipped'))
    #     qs = qs.values('client', 'm', 'y')
    #     qs = qs.order_by()
    #     qs = qs.annotate(Sum('amount_full'))
    #     self.orders_sum_qs = qs

    def set_remain_qs(self):
        qs = Remain.objects.filter(
            warehouse__client__in=self.clients,
            value__gt=0,
        )
        qs = qs.exclude(product__main_collection__is_include_in_shop_season_report=False)
        if self.is_consolidated:
            qs = qs.aggregate(Sum('value'), amount__sum=Sum(F('value') * F('product__price'), output_field=models.DecimalField()))
        else:
            qs = qs.values('warehouse__client')
            qs = qs.order_by()
            qs = qs.annotate(Sum('value'), amount__sum=Sum(F('value') * F('product__price'), output_field=models.DecimalField()))
        self.remain_qs = qs

    def set_remain_product_collection_qs(self):
        qs = Remain.objects.filter(
            warehouse__client__in=self.clients,
            value__gt=0,
        )
        qs = qs.exclude(product__main_collection__is_include_in_shop_season_report=False)
        if self.is_consolidated:
            qs = qs.values('product__main_collection')
        else:
            qs = qs.values('warehouse__client', 'product__main_collection')
        qs = qs.order_by()
        qs = qs.annotate(
            Sum('value'),
            amount__sum=Sum(F('value') * F('product__price'), output_field=models.DecimalField()),
            product_count=Count('product', distinct=True)
        )
        self.remain_product_collection_qs = qs

    def set_marketing_invoice_qs(self):
        qs = Invoice.objects.filter(
            kind=Invoice.KIND.marketing,
            customer__in=self.clients,
        )
        if self.is_consolidated:
            qs = qs.values('payment_perion_month', 'payment_perion_year')
        else:
            qs = qs.values('customer', 'payment_perion_month', 'payment_perion_year')
        qs = qs.order_by()
        qs = qs.annotate(Sum('total_price'))
        self.marketing_invoice_qs = qs

    def set_marketing_paying_qs(self):
        qs = Paying.objects.filter(
            invoice__kind=Invoice.KIND.marketing,
            invoice__customer__in=self.clients,
        )
        if self.is_consolidated:
            qs = qs.aggregate(Sum('amount'))
        else:
            qs = qs.values('invoice__customer')
            qs = qs.order_by()
            qs = qs.annotate(Sum('amount'))
        self.marketing_paying_qs = qs

    def set_product_collections_data(self):
        if self.is_consolidated:
            temp = defaultdict(lambda: defaultdict(int))
            for r in self.remain_product_collection_qs:
                temp[self.product_collections[r['product__main_collection']]]['remain'] += r['value__sum']
                temp[self.product_collections[r['product__main_collection']]]['amount'] += r['amount__sum']
                temp[self.product_collections[r['product__main_collection']]]['product_count'] += r['product_count']

            self.product_collections_data = []
            collections_data = temp
            names = sorted(set(self.product_collections_parrents.keys()).union(set(collections_data.keys())))
            for collection_name in names:
                if self.product_collections_parrents.get(collection_name, 0) or collection_name in collections_data:
                    self.product_collections_data.append(
                        {
                            'collection_name': collection_name,
                            'remain': collections_data[collection_name]['remain'],
                            'amount': collections_data[collection_name]['amount'],
                            'product_count': collections_data[collection_name]['product_count'],
                            'plan_percentage': round(self.product_collections_parrents.get(collection_name, 0)),
                        }
                    )
        else:
            temp = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
            for r in self.remain_product_collection_qs:
                temp[r['warehouse__client']][self.product_collections[r['product__main_collection']]]['remain'] += r['value__sum']
                temp[r['warehouse__client']][self.product_collections[r['product__main_collection']]]['amount'] += r['amount__sum']
                temp[r['warehouse__client']][self.product_collections[r['product__main_collection']]]['product_count'] += r['product_count']

            self.product_collections_data = defaultdict(list)
            for client in self.clients:
                collections_data = temp[client.id]
                names = sorted(set(self.product_collections_parrents.keys()).union(set(collections_data.keys())))
                for collection_name in names:
                    if self.product_collections_parrents.get(collection_name, 0) or collection_name in collections_data:
                        self.product_collections_data[client.id].append(
                            {
                                'collection_name': collection_name,
                                'remain': collections_data[collection_name]['remain'],
                                'amount': collections_data[collection_name]['amount'],
                                'product_count': collections_data[collection_name]['product_count'],
                                'plan_percentage': round(self.product_collections_parrents.get(collection_name, 0)),
                            }
                        )

    def set_visitors(self):
        if self.is_consolidated:
            self.visitors = defaultdict(int)
            for item in self.order_b2c_month_qs:
                self.visitors[(item['y'], item['m'])] += item['visits__sum']
        else:
            self.visitors = defaultdict(lambda: defaultdict(int))
            for item in self.order_b2c_month_qs:
                self.visitors[item['client']][(item['y'], item['m'])] += item['visits__sum']

    def set_revenue(self):
        if self.is_consolidated:
            self.revenue = defaultdict(int)
            for item in self.order_b2c_month_qs:
                self.revenue[(item['y'], item['m'])] += item['amount_full_rur__sum']
            for item in self.order_return_b2c_month_qs:
                self.revenue[(item['y'], item['m'])] -= item['amount_full_rur__sum']
        else:
            self.revenue = defaultdict(lambda: defaultdict(int))
            for item in self.order_b2c_month_qs:
                self.revenue[item['client']][(item['y'], item['m'])] += item['amount_full_rur__sum']
            for item in self.order_return_b2c_month_qs:
                self.revenue[item['client']][(item['y'], item['m'])] -= item['amount_full_rur__sum']

    def set_paying(self):
        if self.is_consolidated:
            self.paying = defaultdict(Decimal)
            for item in self.paying_month_qs:
                self.paying[(item['y'], item['m'])] += item['amount__sum']
        else:
            self.paying = defaultdict(lambda: defaultdict(Decimal))
            for item in self.paying_month_qs:
                self.paying[item['invoice__customer']][(item['y'], item['m'])] += item['amount__sum']

    def set_sales_plan(self):
        if self.is_consolidated:
            self.sales_plan = defaultdict(int)
            for item in self.sales_plan_qs:
                self.sales_plan[(item['year'], item['month'])] += item['plan']
        else:
            self.sales_plan = defaultdict(lambda: defaultdict(int))
            for item in self.sales_plan_qs:
                self.sales_plan[item['store']][(item['year'], item['month'])] += item['plan']

    def set_cheques(self):
        qs = OrderB2CItemSize.objects.filter(
            order_item__order__client__in=self.clients,
            order_item__order__date__gte=self.date_from,
            order_item__order__date__lte=self.date_to,
        )
        qs = qs.values('order_item__order__client', 'order_item__order', 'order_item__cheque_1c')
        qs = qs.order_by()
        qs = qs.annotate(amount__sum=Sum(F('quantity') * F('order_item__price_rur'), output_field=models.DecimalField()))
        if self.is_consolidated:
            self.cheques = []
            for item in qs:
                self.cheques.append(item['amount__sum'])
        else:
            self.cheques = defaultdict(list)
            for item in qs:
                self.cheques[item['order_item__order__client']].append(item['amount__sum'])

    def set_cheques_quantity(self):
        qs = OrderB2CItemSize.objects.filter(
            order_item__order__client__in=self.clients,
            order_item__order__date__gte=self.date_from,
            order_item__order__date__lte=self.date_to,
        )
        qs = qs.values('order_item__order__client', 'order_item__order', 'order_item__cheque_1c')
        qs = qs.order_by()
        qs = qs.annotate(Sum('quantity'))
        if self.is_consolidated:
            self.cheques_quantity = []
            for item in qs:
                self.cheques_quantity.append(item['quantity__sum'])
        else:
            self.cheques_quantity = defaultdict(list)
            for item in qs:
                self.cheques_quantity[item['order_item__order__client']].append(item['quantity__sum'])

    def set_avg_cheque(self):
        if self.is_consolidated:
            self.avg_cheque = Decimal('0.00')
            if self.cheques:
                self.avg_cheque = sum(self.cheques) / len(self.cheques)
        else:
            self.avg_cheque = defaultdict(Decimal)
            for client, data in self.cheques.items():
                if data:
                    self.avg_cheque[client] = sum(data) / len(data)

    def set_avg_cheque_count(self):
        if self.is_consolidated:
            self.avg_cheque_count = Decimal('0.00')
            if self.cheques_quantity:
                self.avg_cheque_count = sum(self.cheques_quantity) / len(self.cheques_quantity)
        else:
            self.avg_cheque_count = defaultdict(Decimal)
            for client, data in self.cheques_quantity.items():
                if data:
                    self.avg_cheque_count[client] = sum(data) / len(data)

    def set_remainder_payable(self):
        if self.is_consolidated:
            self.remainder_payable = Decimal(sum(self.revenue.values())) / 2
            self.remainder_payable -= sum(self.paying.values())
        else:
            self.remainder_payable = defaultdict(int)
            for client_id, data in self.revenue.items():
                self.remainder_payable[client_id] = Decimal(sum(data.values())) / 2
            for client_id, data in self.paying.items():
                self.remainder_payable[client_id] -= sum(data.values())

    def set_remains(self):
        if self.is_consolidated:
            self.remains = self.remain_qs
        else:
            self.remains = defaultdict(lambda: defaultdict(int))
            for item in self.remain_qs:
                self.remains[item['warehouse__client']]['value__sum'] = item['value__sum']
                self.remains[item['warehouse__client']]['amount__sum'] = item['amount__sum']

    def set_images(self):
        self.images = []
        for offer in Offer.objects.filter(to_aggreement=True, images__isnull=False).prefetch_related('images').order_by('?')[:10]:
            try:
                t = get_thumbnail(offer.main_im, '950x1200', crop="center")
            except Exception:
                pass
            else:
                self.images.append(t.url)
                if len(self.images) >= 3:
                    break

import datetime
import os
import tempfile
from collections import defaultdict
from io import StringIO

from django.db import models
from django.db.models.aggregates import Sum
from django.db.models.expressions import F
from django.http.response import HttpResponse
from rest_framework import serializers
from rest_framework.views import APIView

import pandas as pd

from clients.models import Client
from orders.models.order_b2c import OrderB2C, OrderReturnB2C, OrderB2CItem
from stores.models import Employee


class EmployeeDayStatisticSerializer(serializers.Serializer):
    date_from = serializers.DateField()
    date_to = serializers.DateField()
    client = serializers.PrimaryKeyRelatedField(queryset=Client.objects.all())




class EmployeeDayStatisticView(APIView):

    def get(self, request, *args, **kwargs):
        self.serializer = EmployeeDayStatisticSerializer(data=request.GET)
        self.serializer.is_valid(True)

        self.date_from = self.serializer.validated_data['date_from']
        self.date_to = self.serializer.validated_data['date_to']
        self.client = self.serializer.validated_data['client']

        self.employee_ids = set()

        dataframe = self.get_report_data()

        fd, fn = tempfile.mkstemp()
        os.close(fd)
        writer = pd.ExcelWriter(fn, engine='xlsxwriter')
        dataframe.to_excel(writer, 'Лист1')
        worksheet = writer.sheets['Лист1']
        for idx, col in enumerate(dataframe):
            series = dataframe[col]
            max_len = max((
                series.astype(str).map(len).max(),
                len(str(series.name))
            )) + 1
            worksheet.set_column(idx, idx, max_len)
        writer.save()

        fh = open(fn, 'rb')
        resp = fh.read()
        fh.close()
        response = HttpResponse(resp, content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=report.xlsx'
        return response

    def get_report_data(self):
        data = []

        order_b2c_data = self.get_order_b2c_data()
        order_return_b2c_data = self.get_order_return_b2c_data()
        employees = self.get_employees()

        date = self.date_from
        while date <= self.date_to:
            for employee in employees:
                item = {
                    'date': date.strftime('%d.%m.%Y'),
                    'employee': employee.get_full_name(),
                    'revenue': 0,
                    'revenue_rur': 0,
                }
                if employee.id in order_b2c_data[date]:
                    item['revenue'] += order_b2c_data[date][employee.id]['amount_full']
                    item['revenue_rur'] += order_b2c_data[date][employee.id]['amount_full_rur']
                if employee.id in order_return_b2c_data[date]:
                    item['revenue'] -= order_return_b2c_data[date][employee.id]['amount_full']
                    item['revenue_rur'] -= order_return_b2c_data[date][employee.id]['amount_full_rur']
                if item['revenue'] or item['revenue_rur']:
                    data.append(item)

            item = {
                'date': date.strftime('%d.%m.%Y'),
                'employee': 'Без продавца',
                'revenue': 0,
                'revenue_rur': 0,
            }
            item['revenue'] += order_b2c_data[date][None]['amount_full']
            item['revenue_rur'] += order_b2c_data[date][None]['amount_full_rur']
            item['revenue'] -= order_return_b2c_data[date][None]['amount_full']
            item['revenue_rur'] -= order_return_b2c_data[date][None]['amount_full_rur']
            data.append(item)

            date += datetime.timedelta(days=1)

        columns = [
            ['date', 'Дата'],
            ['employee', 'Продавец'],
            ['revenue', 'Выручка в валюте'],
            ['revenue_rur', 'Выручка в рублях'],
        ]

        df = pd.DataFrame(
            data,
            columns=[c[0] for c in columns]
        )
        df.rename(columns={c[0]: c[1] for c in columns}, inplace=True)
        return df

    def get_employees(self):
        return Employee.objects.filter(id__in=self.employee_ids)

    def get_order_b2c_data(self):
        qs = OrderB2CItem.objects.filter(order__date__gte=self.date_from,
                                         order__date__lte=self.date_to,
                                         order__client=self.client)
        qs = qs.values('order__date', 'cheque_1c__seller')
        qs = qs.order_by()
        qs = qs.annotate(
            amount__sum=Sum(F('orderb2citemsize__quantity') * F('price'), output_field=models.DecimalField()),
            amount_rur__sum=Sum(F('orderb2citemsize__quantity') * F('price_rur'), output_field=models.DecimalField()),
        )
        data = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
        for i in qs:
            self.employee_ids.add(i['cheque_1c__seller'])
            data[i['order__date']][i['cheque_1c__seller']] = {
                'amount_full': i['amount__sum'],
                'amount_full_rur': i['amount_rur__sum'],
            }
        return data

    def get_order_return_b2c_data(self):
        qs = OrderReturnB2C.objects.filter(date__gte=self.date_from,
                                           date__lte=self.date_to,
                                           client=self.client)
        qs = qs.values('date', 'seller')
        qs = qs.order_by()
        qs = qs.annotate(
            Sum('amount_full'),
            Sum('amount_full_rur'),
        )
        data = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
        for i in qs:
            self.employee_ids.add(i['seller'])
            data[i['date']][i['seller']] = {
                'amount_full': i['amount_full__sum'],
                'amount_full_rur': i['amount_full_rur__sum'],
            }
        return data
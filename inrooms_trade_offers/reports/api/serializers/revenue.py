from rest_framework import serializers

from clients.models import Client


class RevenueSerializer(serializers.Serializer):
    client = serializers.PrimaryKeyRelatedField(queryset=Client.objects.all(), required=False)
    date_from = serializers.DateField()
    date_to = serializers.DateField()

    class Meta:
        fields = (
            'client',
            'date_from',
            'date_to',
        )

from rest_framework import serializers

from clients.models import Client
from products.models import ProductCollection, PRODUCT_KIND


class ConsolidatedReportFormSerializer(serializers.Serializer):
    collection_parrent = serializers.PrimaryKeyRelatedField(queryset=ProductCollection.objects.filter(parent__isnull=True), required=False)
    collection = serializers.PrimaryKeyRelatedField(queryset=ProductCollection.objects.all(), required=False)
    client = serializers.PrimaryKeyRelatedField(queryset=Client.objects.all(), required=False)
    product__kind = serializers.ChoiceField(choices=PRODUCT_KIND, required=False, allow_blank=True)
    has_moskow_remains = serializers.BooleanField(required=False)

    class Meta:
        fields = (
            'collection_parrent',
            'collection',
            'client',
            'has_moskow_remains',
        )

from rest_framework import serializers

from clients.models import Client


class ShopSeasonSerializer(serializers.Serializer):
    client = serializers.PrimaryKeyRelatedField(queryset=Client.objects.all(), required=False)
    year = serializers.IntegerField(min_value=2014, required=False)
    season = serializers.IntegerField(min_value=1, max_value=2, required=False)
    is_consolidated = serializers.BooleanField(required=False)

    class Meta:
        fields = (
            'client',
            'year',
            'season',
            'is_consolidated',
        )

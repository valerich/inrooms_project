from rest_framework import serializers
from sorl.thumbnail.shortcuts import get_thumbnail

from products.models import ProductCollection, Product, SIZES


class ConsolidatedProductsReportSerializer(serializers.ModelSerializer):
    color_detail = serializers.SerializerMethodField()
    size_data = serializers.SerializerMethodField()
    sold_percent = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    return_percent = serializers.SerializerMethodField()
    turnaround = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'article',
            'color_detail',
            'size_data',
            'sold_percent',
            'return_percent',
            'image',
            'turnaround',
        )

    def get_color_detail(self, obj):
        return {
            'name': obj.color.name
        }

    def get_size_data(self, obj):
        data = []
        for size_code in SIZES:
            key = (obj.id, size_code)
            if key in self.context['products_data']:
                item = self.context['products_data'][key]
                data.append({
                    'size': size_code,
                    'supplied': item['supplied'],
                    'sold': item['sold'],
                    'remain': item['remain'],
                    'return': item['return'],
                    'client_return': item['client_return'],
                })
        return data

    def get_sold_percent(self, obj):
        return self.context['sold_percent'][obj.id]

    def get_return_percent(self, obj):
        return self.context['return_percent'][obj.id]

    def get_image(self, obj):
        pi = obj.images.all().first()
        return self.resize_image(pi, obj)

    def resize_image(self, pi, obj):
        if pi:
            try:
                _200x200 = get_thumbnail(pi.image, '400x400', crop="center").url
                _950x1200 = get_thumbnail(pi.image, '950x1200', crop="center").url
            except IOError as e:
                logger.warning('Проблемы с изображением {} продукта {}: {}'.format(pi.image, obj, e))
                return None
            request = self.context.get('request', None)
            src = pi.image.url
            if request is not None:
                _200x200 = request.build_absolute_uri(_200x200)
                _950x1500 = request.build_absolute_uri(_950x1200)
                src = request.build_absolute_uri(src)
            return {'src': src,
                    '200x200': _200x200,
                    '950x1200': _950x1200}
        else:
            return None

    def get_turnaround(self, obj):
        order_b2c_quantity = self.context['order_b2c_quantity'][obj.id]
        remain_history = self.context['remain_history'][obj.id]
        data = None

        if remain_history['count_value_avg']:
            remain_avg = remain_history['sum_value_avg'] / remain_history['count_value_avg']
            if order_b2c_quantity:
                data = int((remain_avg * remain_history['count_value_avg'] * 30) / order_b2c_quantity)

        return data
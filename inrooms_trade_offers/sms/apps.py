from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'sms'
    verbose_name = 'Сообщения'

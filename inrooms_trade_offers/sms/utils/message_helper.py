from django.core.mail import send_mail
from django.conf import settings
from django.db import transaction
from django.template import Context, Template
from django.utils import timezone

from bot.utils import send_telegram_message
from core.utils.smsc_api import SMSC
from ..models import MessageTemplate, Message, MessageSMS


class MessageHelper():

    def create_template_message(self, template_id, email, template_context):
        try:
            template = MessageTemplate.objects.get(id=template_id)
        except MessageTemplate.DoesNotExist as e:
            send_telegram_message(message='Не удалось создать сообщение для шаблона {}\nТакого шаблона не существует'.format(template_id))
            raise e

        ctx = Context(template_context)
        try:
            title = Template('{% autoescape off %}' + template.title + '{% endautoescape %}').render(ctx)
            text = Template('{% autoescape off %}' + template.text + '{% endautoescape %}').render(ctx)
        except Exception as e:
            send_telegram_message(message='Не удалось создать сообщение для шаблона {}.\n{}'.format(template_id, str(e)))
            raise e

        try:
            message = Message.objects.create(
                template=template,
                title=title,
                text=text,
                email=email,
            )
        except Exception as e:
            send_telegram_message(message='Не удалось создать сообщение для шаблона {}.\n{}'.format(template_id, str(e)))
            raise e

        return message

    def send_messages(self, message_ids=None):
        with transaction.atomic():
            qs = Message.objects.filter(status=Message.STATUS.new)
            if message_ids:
                qs = qs.filter(id__in=message_ids)
            messages = list(qs)
            qs.update(status=Message.STATUS.process)

        for message in messages:
            try:
                send_mail(
                    message.title,
                    message.text,
                    settings.DEFAULT_FROM_EMAIL,
                    [message.email],
                    html_message=message.text
                )
            except Exception as e:
                send_telegram_message(
                    message='Не удалось отправить сообщение.{}\n{}'.format(
                        message.id,
                        str(e)
                    ),
                    url=message.get_absolute_url()
                )
                message.status = message.STATUS.error,
                message.result = str(e)
                message.save()
                raise e
            else:
                message.status = message.STATUS.sent
                message.result = 'Отправлено в {}'.format(str(timezone.now()))
                message.save()


class MessageSMSHelper(MessageHelper):

    def create_template_message(self, template_id, phone, template_context):
        try:
            template = MessageTemplate.objects.get(id=template_id)
        except MessageTemplate.DoesNotExist as e:
            send_telegram_message(message='Не удалось создать смс для шаблона {}\nТакого шаблона не существует'.format(template_id))
            raise e

        ctx = Context(template_context)
        try:
            text = Template('{% autoescape off %}' + template.text + '{% endautoescape %}').render(ctx)
        except Exception as e:
            send_telegram_message(message='Не удалось создать смс для шаблона {}.\n{}'.format(template_id, str(e)))
            raise e

        try:
            message = MessageSMS.objects.create(
                template=template,
                text=text,
                phone=phone,
            )
        except Exception as e:
            send_telegram_message(message='Не удалось создать смс для шаблона {}.\n{}'.format(template_id, str(e)))
            raise e

        return message

    def send_messages(self, message_ids=None):
        with transaction.atomic():
            qs = MessageSMS.objects.filter(status=Message.STATUS.new)
            if message_ids:
                qs = qs.filter(id__in=message_ids)
            messages = list(qs)
            qs.update(status=Message.STATUS.process)

        for message in messages:
            try:
                smsc = SMSC()
                smsc.send_sms(message.phone, message.text, sender=settings.SMS_SENDER_NAME)
            except Exception as e:
                send_telegram_message(
                    message='Не удалось отправить sms.{}\n{}'.format(
                        message.id,
                        str(e)
                    ),
                    url=message.get_absolute_url()
                )
                message.status = message.STATUS.error,
                message.result = str(e)
                message.save()
                raise e
            else:
                message.status = message.STATUS.sent
                message.result = 'Отправлено в {}'.format(str(timezone.now()))
                message.save()

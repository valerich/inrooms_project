from config.celery import app

from .utils import MessageHelper, MessageSMSHelper


@app.task(name='sms.create_template_message')
def create_template_message(template_id, email, context):
    """Создание сообщения на основании шаблона
    """
    rp = MessageHelper()
    rp.create_template_message(template_id, email, context)


@app.task(name='sms.create_template_message_sms')
def create_template_message_sms(template_id, phone, context):
    """Создание сообщения смс на основании шаблона
    """
    rp = MessageSMSHelper()
    rp.create_template_message(template_id, phone, context)


@app.task(name='sms.send_messages')
def send_messages(message_ids=None):
    """Отправка сообщений
    """
    rp = MessageHelper()
    rp.send_messages(message_ids)


@app.task(name='sms.send_messages_sms')
def send_messages_sms(message_ids=None):
    """Отправка смс
    """
    rp = MessageSMSHelper()
    rp.send_messages(message_ids)

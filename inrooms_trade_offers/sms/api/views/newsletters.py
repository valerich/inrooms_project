import datetime
import django_filters
from dateutil.relativedelta import relativedelta
from django.db import transaction
from django.db.models.query_utils import Q
from rest_framework import filters, status
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from client_cards.models import BuyerProfile
from sms.models import MessageSMS
from ..serializers import NewsletterSerializer
from ...models import Newsletter


class NewsletterFilterSet(django_filters.FilterSet):

    class Meta:
        model = Newsletter
        fields = [
            'status',
        ]


class NewsletterViewSet(ModelViewSet):
    serializer_class = NewsletterSerializer
    model = Newsletter
    filter_class = NewsletterFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', 'name', )
    ordering_fields = (
        'id',
        'created',
        'modified',
    )

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)

    def get_queryset(self):
        qs = Newsletter.objects.all()
        return qs

    @detail_route(methods=['post'])
    @transaction.atomic()
    def send(self, request, pk=None):
        newsletter = self.get_object()
        if newsletter.status == newsletter.STATUS.new:
            if not newsletter.text:
                return Response({'error': 'Для отправки рассылки необходимо заполнить текст'},
                                status=status.HTTP_400_BAD_REQUEST)
            buyer_profiles = BuyerProfile.objects.filter(
                Q(card__client__in=newsletter.clients.all()) | Q(id__in=newsletter.buyer_profiles.all().values_list('id', flat=True))
            )
            if not buyer_profiles.exists():
                return Response({'error': 'Для отправки рассылки необходимо заполнить получателей'},
                                status=status.HTTP_400_BAD_REQUEST)
            check_date_from = datetime.datetime.now() - relativedelta(days=1)
            for buyer_profile in buyer_profiles:
                if not MessageSMS.objects.filter(check_previous_messages=True, phone=buyer_profile.phone, created__gt=check_date_from).exists():
                    MessageSMS.objects.create(
                        newsletter=newsletter,
                        check_previous_messages=True,
                        phone=buyer_profile.phone,
                        text=newsletter.text,
                        buyer_profile=buyer_profile,
                    )
            newsletter.status = newsletter.STATUS.sent
            newsletter.save()

        return Response(status=status.HTTP_200_OK)


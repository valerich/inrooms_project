import django_filters
from rest_framework import filters
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from sms.api.serializers.subscriber import SubscriberSerializer
from sms.models import Subscriber


class SubscriberFilterSet(django_filters.FilterSet):

    class Meta:
        model = Subscriber
        fields = (
            'sources',
        )


class SubscriberViewSet(RetrieveModelMixin,
                        ListModelMixin,
                        GenericViewSet):
    model = Subscriber
    serializer_class = SubscriberSerializer
    queryset = Subscriber.objects.all()
    filter_class = SubscriberFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', 'email',)
    ordering_fields = (
        'id',
        'created',
        'modified',
        'email',
    )

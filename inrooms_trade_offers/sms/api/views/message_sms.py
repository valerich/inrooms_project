import django_filters
from rest_framework import filters
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin
from rest_framework.viewsets import GenericViewSet

from ..serializers import MessageSMSSerializer
from ...models import MessageSMS


class MessageSMSFilterSet(django_filters.FilterSet):

    class Meta:
        model = MessageSMS
        fields = [
            'status',
            'buyer_profile',
            'newsletter',
        ]


class MessageSMSViewSet(RetrieveModelMixin,
                        ListModelMixin,
                        GenericViewSet):
    serializer_class = MessageSMSSerializer
    model = MessageSMS
    filter_class = MessageSMSFilterSet
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, filters.DjangoFilterBackend,)
    search_fields = ('=id', 'phone', )
    ordering_fields = (
        'id',
        'created',
        'modified',
    )

    def get_queryset(self):
        qs = MessageSMS.objects.all()
        return qs

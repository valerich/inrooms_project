from django.conf import urls
from rest_framework.routers import DefaultRouter

from .views import MessageSMSViewSet, NewsletterViewSet, SubscriberViewSet

router = DefaultRouter()
router.register(r'message_sms', MessageSMSViewSet, base_name='message_sms')
router.register(r'newsletter', NewsletterViewSet, base_name='newsletter')
router.register(r'subscriber', SubscriberViewSet, base_name='subscriber')

urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),

]

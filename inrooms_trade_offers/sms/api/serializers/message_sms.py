from rest_framework import serializers

from ...models import MessageSMS


class MessageSMSSerializer(serializers.ModelSerializer):
    status_detail = serializers.SerializerMethodField()
    buyer_profile_detail = serializers.SerializerMethodField()

    class Meta:
        model = MessageSMS

        fields = [
            'id',
            'status',
            'status_detail',
            'created',
            'modified',
            'text',
            'phone',
            'result',
            'buyer_profile_detail',
        ]

    def get_status_detail(self, obj):
        return {
            'id': obj.status,
            'name': obj.get_status_display()
        }

    def get_buyer_profile_detail(self, obj):
        if obj.buyer_profile:
            return {
                'id': obj.buyer_profile.id,
                'name': str(obj.buyer_profile)
            }
        return None

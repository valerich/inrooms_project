from rest_framework import serializers

from accounts.api.serializers import UserSerializer
from ...models import Newsletter


class NewsletterSerializer(serializers.ModelSerializer):
    status_detail = serializers.SerializerMethodField()
    creator_detail = serializers.SerializerMethodField()
    clients_detail = serializers.SerializerMethodField()
    buyer_profiles_detail = serializers.SerializerMethodField()

    class Meta:
        model = Newsletter

        fields = [
            'id',
            'status',
            'status_detail',
            'created',
            'modified',
            'name',
            'text',
            'creator_detail',
            'clients',
            'clients_detail',
            'buyer_profiles',
            'buyer_profiles_detail',
        ]

    def get_status_detail(self, obj):
        return {
            'id': obj.status,
            'name': obj.get_status_display()
        }

    def get_creator_detail(self, obj):
        return UserSerializer(obj.creator).data

    def get_clients_detail(self, obj):
        return [{
            'id': i.id,
            'name': i.name,
        } for i in obj.clients.all()]

    def get_buyer_profiles_detail(self, obj):
        return [{
            'id': i.id,
            'name': str(i),
        } for i in obj.buyer_profiles.all()]
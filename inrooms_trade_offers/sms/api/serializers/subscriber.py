from rest_framework import serializers

from sms.models import Subscriber


class SubscriberSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subscriber
        fields = (
            'id',
            'email',
            'sources',
        )

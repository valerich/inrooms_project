from django.contrib import admin

from .models import MessageTemplate, Message, MessageSMS, Newsletter, SubscriptionSource, Subscriber


class NewsletterAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'creator', 'created')


class MessageTemplateAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', )


class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'status', 'template', 'title', 'email', )
    list_filter = ('status', 'template')


class MessageSMSAdmin(admin.ModelAdmin):
    list_display = ('id', 'status', 'template', 'phone')
    list_filter = ('status', 'template')


class SubscriptionSourceAdmin(admin.ModelAdmin):
    list_display = ('name', 'code')


class SubscriberAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'get_sources')
    list_filter = ('sources', )

    def get_sources(self, obj):
        return "\n".join([s.name for s in obj.sources.all()])


for model_or_iterable, admin_class in (
    (MessageTemplate, MessageTemplateAdmin),
    (Message, MessageAdmin),
    (MessageSMS, MessageSMSAdmin),
    (Newsletter, NewsletterAdmin),
    (SubscriptionSource, SubscriptionSourceAdmin),
    (Subscriber, SubscriberAdmin),
):
    admin.site.register(model_or_iterable, admin_class)

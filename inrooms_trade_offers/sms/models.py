from django.core.urlresolvers import reverse
from django.db import models

from model_utils.choices import Choices
from model_utils.models import TimeStampedModel

from accounts.models import User
from client_cards.models import BuyerProfile
from clients.models import Client


NEWSLETTER_CHOICES = Choices(
    (1, 'new', 'Новая'),
    (1000, 'sent', 'Отправлена'),
    (-100, 'error', 'Ошибка'),
)


class Newsletter(TimeStampedModel):
    STATUS = NEWSLETTER_CHOICES
    creator = models.ForeignKey(User, verbose_name='Создатель')
    clients = models.ManyToManyField(Client, verbose_name='Партнеры', blank=True)
    buyer_profiles = models.ManyToManyField(BuyerProfile, verbose_name='Покупатели', blank=True)
    status = models.PositiveSmallIntegerField('Статус', choices=STATUS, default=STATUS.new)
    name = models.CharField('Название', max_length=100)
    text = models.TextField('Текст', blank=True)
    result = models.TextField('Результат', blank=True)

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'
        ordering = ('-id', )

    def __str__(self):
        return self.name


class MessageTemplate(models.Model):
    title = models.TextField('Заголовок')
    text = models.TextField('Текст')

    class Meta:
        verbose_name = "Шаблон сообщения"
        verbose_name_plural = "Шаблоны сообщений"

    def __str__(self):
        return "{}. {}".format(self.id, self.title)


STATUS_CHOICES = Choices(
    (0, 'new', 'Новое'),
    (10, 'process', 'Обрабатывается'),
    (100, 'sent', 'Отправлено'),
    (-100, 'error', 'Ошибка'),
)


class Message(TimeStampedModel):
    STATUS = STATUS_CHOICES
    status = models.SmallIntegerField('Статус', choices=STATUS, default=STATUS.new)
    template = models.ForeignKey(MessageTemplate, verbose_name='На основании шаблона', blank=True, null=True)
    title = models.TextField('Заголовок', blank=True, null=True)
    text = models.TextField('Текст')
    email = models.EmailField('Email')
    result = models.TextField('Результат', blank=True)

    class Meta:
        verbose_name = "Сообщениe Email"
        verbose_name_plural = "Сообщения Email"

    def __str__(self):
        return "{}. {}".format(self.id, self.get_status_display())

    def get_absolute_url(self):
        return reverse('admin:sms_message_change', args=(self.id,))


class MessageSMS(TimeStampedModel):
    STATUS = STATUS_CHOICES
    status = models.SmallIntegerField('Статус', choices=STATUS, default=STATUS.new)
    template = models.ForeignKey(MessageTemplate, verbose_name='На основании шаблона', blank=True, null=True)
    text = models.TextField('Текст')
    phone = models.CharField('Телефон', max_length=12)
    result = models.TextField('Результат', blank=True)
    check_previous_messages = models.BooleanField('Проверять предыдущие сообщения?', default=False,
                                                  help_text='Устанавливаем "да" случае, если запрещаем отправлять больше чем одно сообщение в сутки')
    newsletter = models.ForeignKey(Newsletter, verbose_name='Отправлено по рассылке', blank=True, null=True)
    buyer_profile = models.ForeignKey(BuyerProfile, verbose_name='Покупатель', blank=True, null=True)

    class Meta:
        verbose_name = "СМС"
        verbose_name_plural = "СМС"

    def __str__(self):
        return "{}. {}".format(self.id, self.get_status_display())

    def get_absolute_url(self):
        return reverse('admin:sms_messagesms_change', args=(self.id,))


class SubscriptionSource(TimeStampedModel):
    name = models.CharField('Название', max_length=255)
    code = models.CharField('Код', max_length=255, unique=True)

    class Meta:
        verbose_name = 'Источник подписок'
        verbose_name_plural = 'Источники подписок'
        ordering = ('name', 'code')

    def __str__(self):
        return self.name


class Subscriber(TimeStampedModel):
    email = models.EmailField('email', unique=True)
    sources = models.ManyToManyField(SubscriptionSource, verbose_name='Источник подписок', related_name='subscribers')

    class Meta:
        verbose_name = 'Подписчик'
        verbose_name_plural = 'Подписчики'
        ordering = ('email', )

    def __str__(self):
        return self.email

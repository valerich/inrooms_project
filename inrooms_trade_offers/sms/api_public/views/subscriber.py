from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView

from sms.models import SubscriptionSource, Subscriber
from ..serializers import SubscriberAddSerializer


class SubscriberAddView(APIView):

    def post(self, request, *args, **kwargs):
        serializer = SubscriberAddSerializer(data=request.data)
        serializer.is_valid(True)
        source, created = SubscriptionSource.objects.get_or_create(
            code=serializer.validated_data['source'],
            defaults={
                'name': serializer.validated_data['source']
            }
        )

        subscriber, created = Subscriber.objects.get_or_create(
            email=serializer.validated_data['email']
        )
        subscriber.sources.add(source)
        return Response(status=HTTP_200_OK)
from rest_framework import serializers


class SubscriberAddSerializer(serializers.Serializer):
    email = serializers.EmailField()
    source = serializers.CharField()

    class Meta:
        fields = (
            'email',
            'source',
        )

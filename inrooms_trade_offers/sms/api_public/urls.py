from django.conf import urls

from .views import (
    SubscriberAddView,
)


urlpatterns = [
    urls.url(r'^subscriber_add/$', SubscriberAddView.as_view(), name='subscriber_add'),
]
